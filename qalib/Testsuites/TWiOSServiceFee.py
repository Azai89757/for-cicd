import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class TWiOSServiceFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWiOSServiceFee Precondition =="
        dumplogger.info("== Setup TWiOSServiceFee Precondition ==")

        ##Get device id and device t type
        Config._DeviceID_, device_type = GetiOSDeviceId()
        print "Device id : %s" % (Config._DeviceID_)
        print "Device type :", device_type

        ##Get device info by device id
        if Config._DeviceID_:
            #Config._MobileDeviceInfo_[Config._DeviceID_] = {}
            #Config._MobileDeviceInfo_[Config._DeviceID_]["device_name"] = device_type
            ##Build WDA to iOS device
            iOSBuildWDAToDevice({"device_type":device_type, "uuid":Config._DeviceID_})

            ##Call function from iOSBaseUICore
            iOSInitialDriver()
        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWiOSServiceFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSServiceFee-01.xml")

    def test_TWiOSServiceFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSServiceFee-02.xml")

    def test_TWiOSServiceFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSServiceFee-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWiOSServiceFee Post condition =="
        dumplogger.info("== Setup TWiOSServiceFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
