import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerVoucher Precondition =="
        dumplogger.info("== Setup TWHttpSellerVoucher Precondition ==")

    def test_TWHttpSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucher-01.xml")

    def test_TWHttpSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucher-02.xml")

    def test_TWHttpSellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucher-03.xml")

    def test_TWHttpSellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerVoucher Post condition =="
        dumplogger.info("== Setup TWHttpSellerVoucher Post condition ==")
