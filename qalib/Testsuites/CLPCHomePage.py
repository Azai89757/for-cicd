import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCHomePage Precondition =="
        dumplogger.info("== Setup CLPCHomePage Precondition ==")

    def test_CLPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCHomePage-01.xml")

    def test_CLPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCHomePage-02.xml")

    '''def test_CLPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCHomePage Post condition =="
        dumplogger.info("== Setup CLPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
