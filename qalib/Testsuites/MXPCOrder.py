import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCOrder Precondition =="
        dumplogger.info("== Setup MXPCOrder Precondition ==")

    # def test_MXPCOrder01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCOrder-01.xml")

    def test_MXPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCOrder-02.xml")

    def test_MXPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCOrder-03.xml")

    def test_MXPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCOrder-04.xml")

    def test_MXPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCOrder-05.xml")

    def test_MXPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCOrder-06.xml")

    # def test_MXPCOrder07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCOrder-07.xml")

    # def test_MXPCOrder08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCOrder-08.xml")

    # def test_MXPCOrder09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCOrder-09.xml")

    # def test_MXPCOrder10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCOrder-10.xml")

    # def test_MXPCOrder11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCOrder-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCOrder Post condition =="
        dumplogger.info("== Setup MXPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
