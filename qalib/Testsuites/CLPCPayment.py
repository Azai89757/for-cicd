import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCPayment Precondition =="
        dumplogger.info("== Setup CLPCPayment Precondition ==")

    def test_CLPCPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCPayment-01.xml")

    def test_CLPCPayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCPayment-02.xml")

    def test_CLPCPayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCPayment-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCPayment Post condition =="
        dumplogger.info("== Setup CLPCPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
