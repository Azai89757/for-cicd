import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCLogOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCLogOut Precondition =="
        dumplogger.info("== Setup VNPCLogOut Precondition ==")

    def test_VNPCLogOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCLogOut-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCLogOut Post condition =="
        dumplogger.info("== Setup VNPCLogOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
