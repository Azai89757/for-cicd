import unittest
from Config import dumplogger
from FrameWorkBase import *


class ESAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup ESAPIRestoreHomePopupBanner Precondition ==")

    def test_ESAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/ESAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup ESAPIRestoreHomePopupBanner Post condition ==")
