import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCLogisticPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCLogisticPromotion Precondition =="
        dumplogger.info("== Setup ARPCLogisticPromotion Precondition ==")

    def test_ARPCLogisticPromotion01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCLogisticPromotion-01.xml")

    def test_ARPCLogisticPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCLogisticPromotion-02.xml")

    def test_ARPCLogisticPromotion03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCLogisticPromotion-03.xml")

    def test_ARPCLogisticPromotion04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCLogisticPromotion-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCLogisticPromotion Post condition =="
        dumplogger.info("== Setup ARPCLogisticPromotion Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
