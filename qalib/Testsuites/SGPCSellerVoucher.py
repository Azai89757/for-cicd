import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSellerVoucher Precondition =="
        dumplogger.info("== Setup SGPCSellerVoucher Precondition ==")

    def test_SGPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCSellerVoucher-01.xml")

    def test_SGPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCSellerVoucher-02.xml")

    # def test_SGPCSellerVoucher03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCSellerVoucher-03.xml")

    # def test_SGPCSellerVoucher04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSellerVoucher Post condition =="
        dumplogger.info("== Setup SGPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
