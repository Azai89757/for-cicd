import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSellerDiscount Precondition =="
        dumplogger.info("== Setup SGPCSellerDiscount Precondition ==")

    def test_SGPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCSellerDiscount-01.xml")

    def test_SGPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCSellerDiscount-02.xml")

    def test_SGPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCSellerDiscount-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSellerDiscount Post condition =="
        dumplogger.info("== Setup SGPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
