#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 i18NJsonAPIMethod.py: The def of this file called by other function.
'''

##Import system library
import requests
import json
import time

##Import Framework common library
from Config import dumplogger

##Import api library
import HttpApiHelper


class i18NJsonController(HttpApiHelper.APIController):

    @staticmethod
    def GetLocalizeJson(url):
        '''
        GetLocalizeJson : Start proxy server before case start
                Input argu :
                    url - url of api request
                Return code :
                    response - response status and text
        '''
        dumplogger.info("Enter GetLocalizeJson")
        dumplogger.info("GetLocalizeJson to got => %s" % (url))

        ##Intial API Controller
        AP = HttpApiHelper.APIController()
        ##Get response from API
        response = AP.SendAPIPacket("get", url)

        return response["status"], response["text"]

    @staticmethod
    def GetSpaceIOToken():
        '''
        GetSpaceIOToken : Get space shoopee io token which will be used to get collection id
                Input argu : N/A
                Return code :
                    response - response status and text
        '''
        dumplogger.info("Enter GetSpaceIOToken")

        url = "https://space.shopee.io/v1/sessions/"
        dumplogger.info("GetSpaceIOToken to got => %s" % (url))

        headers = {
            'Authorization': 'Basic dHdxYV9ib3Q6U2hvcGVldHdxYTEyMw=='
        }

        ##Intial API Controller
        AP = HttpApiHelper.APIController()

        response = None

        ##Handle space connection error in four times
        for retry_count in range(4):
            try:
                ##Get response from API
                response = AP.SendAPIPacket("post", url, headers=headers)
                dumplogger.info("Connect successfully and get data in %s times" % (retry_count+1))
                break

            ##Get refused from server, sleep 10 seconds and request again
            except requests.exceptions.RequestException:
                dumplogger.exception("Encounter connection refused in %s times, sleep and go to retry" % (retry_count+1))
                time.sleep(3)

            except:
                dumplogger.exception("Encounter unknown connection error in %s times, sleep and go to retry" % (retry_count+1))
                time.sleep(3)

        return response

    @staticmethod
    def GetCollectionID(token, collection_len=1):
        '''
        GetCollectionID : Get all collection id of transify key
                Input argu :
                    token - api token from space
                Return code :
                    response - response status and text
        '''

        url = "https://space.shopee.io/api/webfe/transify/collections/list"

        dumplogger.info("Enter GetCollectionID")
        dumplogger.info("GetCollectionID to got => %s" % (url))

        ##Intial API Controller
        AP = HttpApiHelper.APIController()

        ##Authorization must included in headers
        headers = {
            'Authorization': 'Bearer ' + token
        }

        ##user-agent must included in payload
        payload = {
            'project_id': 4,
            'asc': 0,
            'offset': 1,
            'len': collection_len
        }

        ##change payload to data format
        payload = json.dumps(payload)

        ##Get response from API
        response = AP.SendAPIPacket("post", url, headers=headers, payload=payload)

        return response
