import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCPayment Precondition =="
        dumplogger.info("== Setup PLPCPayment Precondition ==")

    def test_PLPCPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCPayment-01.xml")

    def test_PLPCPayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCPayment-02.xml")

    def test_PLPCPayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCPayment-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCPayment Post condition =="
        dumplogger.info("== Setup PLPCPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
