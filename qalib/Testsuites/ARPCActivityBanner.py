import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class ARPCActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCActivityBanner Precondition =="
        dumplogger.info("== Setup ARPCActivityBanner Precondition ==")

    def test_ARPCActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-01.xml")

    def test_ARPCActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-02.xml")

    def test_ARPCActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-03.xml")

    def test_ARPCActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-04.xml")

    def test_ARPCActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-05.xml")

    def test_ARPCActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-06.xml")

    def test_ARPCActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-07.xml")

    def test_ARPCActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-08.xml")

    def test_ARPCActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCActivityBanner-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCActivityBanner Post condition =="
        dumplogger.info("== Setup ARPCActivityBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
