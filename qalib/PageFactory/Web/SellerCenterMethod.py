﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SellerCenterMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import BaseUICore
import BaseUILogic
import XtFunc
import DecoratorHelper


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def SCLogout(arg):
    '''
    SCLogout : log out account from seller center
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    ret = 1

    ##Check tips elemnet is exist or not, if exist, click tips confirm which is under user name
    xpath = Util.GetXpath({"locate":"tips"})
    if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click tips under user name", "result": "1"})

    ##log out account from seller center
    xpath1 = Util.GetXpath({"locate":"seller_user_info_drop_down"})
    xpath2 = Util.GetXpath({"locate":"seller_logout"})
    BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath1, "locatehidden":xpath2, "message":"Go to my account page", "result": "1"})

    OK(ret, int(arg['result']), 'SCLogout')

@DecoratorHelper.FuncRecorder
def SCLogin(arg):
    '''
    SCLogin : log in account from seller center
            Input argu :
                account - user account
                password - user password
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    account = arg["account"]
    password = arg["password"]
    ret = 1

    ##Input account
    xpath = Util.GetXpath({"locate":"sc_login_account"})
    BaseUICore.Input({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

    ##Input password
    xpath = Util.GetXpath({"locate":"sc_login_password"})
    BaseUICore.Input({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

    ##Click login button
    xpath = Util.GetXpath({"locate":"sc_login_loginbtn"})
    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click login button", "result": "1"})

    OK(ret, int(arg['result']), 'SCLogin')

class SellerCenterPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisputeReasonOption(arg):
        '''
        SelectDisputeReasonOption : Select dispute reason option
                Input argu :
                    reason_option - shipped_the_item_and_have_proof
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason_option = arg["reason_option"]

        ##Click dispute reason drop down menu
        xpath = Util.GetXpath({"locate":"dispute_drop_down"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dispute reason drop down menu", "result": "1"})

        ##Click dispute reason option
        xpath = Util.GetXpath({"locate":reason_option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dispute reason option => %s" % (reason_option), "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterPage.SelectDisputeReasonOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadEvidence(arg):
        '''
        UploadEvidence : Upload evidence
                Input argu :
                    file_name - image file neme
                    file_type - image file type
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click on upload evidence
        xpath = Util.GetXpath({"locate":"upload_evidence"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click on upload evidence", "result": "1"})

        ##Upload evidence image
        XtFunc.UploadFileWithWindowsAutoIt({"action":"image", "file_name":file_name, "file_type":file_type, "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterPage.UploadEvidence')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImage(arg):
        '''
        UploadImage : Upload image
                Input argu :
                    file_name - image file neme
                    file_type - image file type
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click on upload image icon
        xpath = Util.GetXpath({"locate":"image_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click on upload image icon", "result": "1"})

        ##Upload image
        XtFunc.UploadFileWithWindowsAutoIt({"action":"image", "file_name":file_name, "file_type":file_type, "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterPage.UploadImage')

class SellerCenterComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NativgateToPage(arg):
        '''
        NativgateToPage : Go to a category of seller center
                Input argu :
                    page_type - my_products / my_catagories / my_sales / marketing_centre / my_income / my_wallet / shop_settings / notification / homepage / help_center / my_orders / return_refund
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        BaseUICore.PageHasLoaded({"result": "1"})
        locate_catagory = Util.GetXpath({"locate":page_type})
        Back_to_view_with_picture_btn = Util.GetXpath({"locate":"Back_to_view_with_picture"})

        ##Go to a category
        BaseUICore.Click({"method":"xpath", "locate":locate_catagory, "message":"Go to a category", "result": "1"})

        ##Click switch view button due to new design on SC
        if page_type == "my_products":
            time.sleep(4)
            BaseUICore.Click({"method":"xpath", "locate":Back_to_view_with_picture_btn, "message":"Go back to view my product with pictures", "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterComponent.NativgateToPage -> ' + page_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
                Input argu :
                    button_type - type of button which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
                Input argu :
                    column_type - type of column
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterComponent.InputToColumn')


class MySellerProducts:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UpdateProductStatus(arg):
        ''' UpdateProductStatus : Update product status
                Input argu :
                    product_name - product name to be changed
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        product_name = arg["product_name"]
        ret = 1

        ##Click update this product
        xpath = Util.GetXpath({"locate":"update_product"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click update this product", "result": "1"})

        try:
            ##Try to click ok if there is a reminder
            xpath = Util.GetXpath({"locate":"reminder_alert"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok in reminder", "result": "1"})
        except:
            dumplogger.info("Failed to click ok in reminder, maybe it didn't showed up.")

        ##Modify product name
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"modify_prodcut_name"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click input column", "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_name, "result": "1"})

        ##Click update
        xpath = Util.GetXpath({"locate":"update_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click update", "result": "1"})
        time.sleep(3)

        try:
            ##Check current products display mode, if listing mode, switch to picture mode
            check_element_xpath = Util.GetXpath({"locate":"check_product_display_mode"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":check_element_xpath, "passok": "0", "result": "1"}):
                ##Switch view mode to listing of pictures
                Switch_view_mode = Util.GetXpath({"locate":"switch_view_mode"})
                BaseUICore.Click({"method":"xpath", "locate":Switch_view_mode, "message":"Click update", "result": "1"})
        except:
            dumplogger.info("No need to switch view product mode")

        OK(ret, int(arg['result']), 'MySellerProducts.UpdateProductStatus')


class SC_MySale:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchTab(arg):
        ''' SwitchTab : Switch tab in seller center
                Input argu : tabs/unpaid/toship/shipping/completed/cancelled/refund/to_process
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        time.sleep(2)

        ##Click tab
        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":type}), "message":"click button", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.SwitchTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EnterOrderDetail(arg):
        ''' EnterOrderDetail : Click order detail in MySale page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"order_detail"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click title to enter order detail", "result": "1"})

        ##Call SwitchBrowserWindow to switch handle to order detail
        BaseUILogic.SwitchBrowserWindow({"result": "1"})
        OK(ret, int(arg['result']), 'SC_MySale.EnterOrderDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenTrackNumber(arg):
        ''' ClickGenTrackNumber : Click button of generate track numer in MySale page
                Input argu :
                    shipping : 7-11 / ok
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        shipping = arg["shipping"]

        ret = 1

        if shipping == "7-11":
            xpath = "(//*[@class='shopee-button shopee-button--inactive shopee-button--primary ember-view'])[1]"
        elif shipping == "ok":
            xpath = "//*[@class='shopee-button shopee-button--inactive shopee-button--primary shopee-button--medium ember-view']"

        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click generate track number", "result": "1"})
        time.sleep(6)

        OK(ret, int(arg['result']), 'SC_MySale.ClickGenTrackNumber -> ' + shipping)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ApplyTrackNumber(arg):
        ''' ApplyTrackNumber : Click button of generate track numer in MySale page
                Input argu :
                    name - seller name
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        name = arg["name"]
        ret = 1

        xpath = "//*[@placeholder='你的真實姓名']"
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        xpath = "//*[@class='shopee-button shopee-button--inactive shopee-button--test shopee-button--medium shopee-button--primary ember-view']"
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click generate track number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'SC_MySale.ApplyTrackNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExpandReceipt(arg):
        ''' ExpandReceipt : Get more receipt detail info in order detail page.
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        time.sleep(2)
        xpath = Util.GetXpath({"locate":"buyer_payment_brief"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click order content", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.SelecExpandReceipttOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrangePickUpIntegration(arg):
        ''' ClickArrangePickUpIntegration : Click button of arrange pick up(安排取件)
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Call Click to click arrange pickup item
        xpath = Util.GetXpath({"locate":"pick_up_btn"})
        BaseUICore.CheckButtonClickable({"message":"Wait for page fully loaded", "method":"xpath", "locate":xpath, "state":"enable", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickArrangePickUpIntegration button", "result": "1"})
        time.sleep(5)

        ##For VN region, you can choose that bring your goods to the poskt by yourself or postman will go to your address to pick up goods.
        ##For default setting, we choose postman will go to your address to pick up goods.
        if Config._TestCaseRegion_ == "VN":
            dumplogger.info("Need to choose send goods method.")
            xpath = Util.GetXpath({"locate":"choose_method_confirm_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm choose send goods method button", "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.CheckButtonClickable({"message":"Wait for page fully loaded", "method":"xpath", "locate":xpath, "state":"enable", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'SC_MySale.ClickArrangePickUpIntegration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrangePickUpNonIntegration(arg):
        ''' ClickArrangePickUpNonIntegration : Click button of arrange shipment
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Call Click to click arrange pickup item
        xpath = Util.GetXpath({"locate":"arrange_pickup_item"})
        BaseUICore.CheckButtonClickable({"message":"Wait for page fully loaded", "method":"xpath", "locate":xpath, "state":"enable", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickArrangePickUp button", "result": "1"})
        time.sleep(5)

        ##Click confirm
        xpath = Util.GetXpath({"locate":"deliver_product"})
        BaseUICore.CheckButtonClickable({"message":"Wait for page fully loaded", "method":"xpath", "locate":xpath, "state":"enable", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'SC_MySale.ClickArrangePickUpNonIntegration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReturnRefundReason(arg):
        ''' ReturnRefundReason : Send reason and your email address for request return refund
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        ##Send reason of return/refund
        elem_reason = BaseUICore._WebDriver_.find_element_by_xpath(Util.GetXpath({"locate":"reason_input_column"}))
        action_r = ActionChains(BaseUICore._WebDriver_)
        action_r.move_to_element(elem_reason).click().send_keys('for test').perform()

        ##Send your email address
        elem_address = BaseUICore._WebDriver_.find_element_by_xpath(Util.GetXpath({"locate":"email_input_column"}))
        action_a = ActionChains(BaseUICore._WebDriver_)
        action_a.move_to_element(elem_address).click().send_keys('alex.hou@shopee.com').perform()

        ##Click complete button
        xpath = Util.GetXpath({"locate":"complete_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click complete button", "result": "1"})
        OK(ret, int(arg['result']), 'SC_MySale.ReturnRefundReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefundButton(arg):
        ''' ClickRefundButton : Click refund button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"refund_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click refund button", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.ClickRefundButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfrimInReturnRefundPopup(arg):
        ''' ClickConfrimInReturnRefundPopup : Click confirm button in return/refund pop up window
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = "//*[@class='shopee-button shopee-button--medium shopee-button--primary']"
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.ClickConfrimInReturnRefundPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelInReturnRefundPopup(arg):
        ''' ClickCancelInReturnRefundPopup : Click cancel button in return/refund pop up window
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"cancel_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.ClickCancelInReturnRefundPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisputeButton(arg):
        ''' ClickDisputeButton : Click dispute button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"dispute_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dispute button", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.ClickDisputeButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseButtonIn711(arg):
        ''' ClickCloseButtonIn711 : Click close button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        time.sleep(2)
        xpath = "//*[@class='shopee-button shopee-button--medium']"
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click close button", "result": "1"})

        OK(ret, int(arg['result']), 'SC_MySale.ClickCloseButtonIn711')


class SC_FlashSale:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyPromotion(arg):
        ''' ClickMyPromotion : Click my promotion
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"my_promotion_icon"}), "message":"Click my promotion button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_FlashSale.ClickMyPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSale(arg):
        ''' ClickFlashSale : Click flash sale
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"flash_sale_icon"}), "message":"Click flash sale button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_FlashSale.ClickFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRegisterNow(arg):
        ''' ClickRegisterNow : Click register now
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"registe_now_btn"}), "message":"Click register now button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_FlashSale.ClickRegisterNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddProduct(arg):
        ''' ClickAddProduct : Click add product
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click add product button
        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"add_product"}), "message":"Click add product button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_FlashSale.ClickAddProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddProduct(arg):
        ''' AddProduct : Add product
                Input argu :
                            all_product : 1 - not select all product
                                       0 - select all product
                            product_name : select product by name
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check page had redirect to choose nomination product page
        xpath = Util.GetXpath({"locate":"check_product_name"})
        BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "result": "1"})

        ##if want to select all product then don't need product name
        if "all_product" in arg:
            all_product = arg["all_product"]
        else:
            all_product = "0"

        if all_product == "1":
            ##Click selet all product
            BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"choose_all_product"}), "message":"Click add product button", "result": "1"})
        elif all_product == "0":
            product_name = arg["product_name"]

            ##add product
            product_xpath = "//div[@class='name'][text()='"+product_name+"']"
            BaseUICore.Click({"method":"xpath", "locate":product_xpath, "message":"Click add product button", "result": "1"})

        ##Click confirm button
        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"confirm_btn"}), "message":"Click confirm button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_FlashSale.AddProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FillFlashSaleProductInfo(arg):
        ''' FillFlashSaleProductInfo : fill flash sale product info and product upload image
                Input argu : quantity - quantity of product(s) need to fill
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##count xpath number
        counter = 1

        ##Due to our flash sale need least 7 products, thus, we only upload 7 products.
        for product_amount in range(1, 8):
            ##Select new image
            xpath = Util.GetXpath({"locate":"add_promotion_pic"}) + "[" + str(product_amount) + "]"
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add promotion picture button", "result": "1"})

            XtFunc.UploadFileWithWindowsAutoIt({"action":"image", "file_name":"pd1", "file_type":"png", "result": "1"})

            ##Input product name
            BaseUICore.Input({"method": "xpath", "locate":"//*/div[2]/table/tbody/tr[" + str(product_amount * 2) + "]/td[1]/div[2]/div[2]/textarea", "message":"Input product name", "string":"product" + str(i), "result": "1"})

            ##Input buy limit
            xpath = Util.GetXpath({"locate":"buy_limit"}) + "[" + str(counter) + "]"
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "20", "message": "Input buy limit number", "result": "1"})
            counter = counter + 4

        ##Click save
        BaseUILogic.MouseScrollEvent({"x": "0", "y": "-500", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": Util.GetXpath({"locate":"save_push_btn"}), "message": "Click save button", "result": "1"})

        time.sleep(30)

        OK(ret, int(arg['result']), 'SC_FlashSale.FillFlashSaleProductInfo')


class SC_ShippingFeePromotion:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectPromotionPeriod(arg):
        '''
        SelectPromotionPeriod : Select promotion period
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"select_promotion_period"}), "message":"Select promotion period", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.SelectPromotionPeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectNoTimeLimit(arg):
        '''
        SelectNoTimeLimit : Select no time limit
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"select_no_time_limit"}), "message":"Select no time limit", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.SelectNoTimeLimit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShippingFeePromotionName(arg):
        '''
        InputShippingFeePromotionName : Input shipping fee promotion name
                Input argu :
                    shipping_fee_promotion_name - shipping fee promotion name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping_fee_promotion_name = arg["shipping_fee_promotion_name"]

        xpath = Util.GetXpath({"locate": "shipping_fee_promotion_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shipping_fee_promotion_name, "result": "1"})

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.InputShippingFeePromotionName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShippingChannel(arg):
        '''
        SelectShippingChannel : Select shipping channel (West Malaysia)
                Input argu :
                    channel_name - channel name you want to select
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel_name = arg['channel_name']
        xpath = Util.GetXpath({"locate":"shipping_channel"})
        xpath = xpath.replace("replace_channel_name", channel_name)

        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select shipping channel", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.SelectShippingChannel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMinimumBasketPrice(arg):
        '''
        InputMinimumBasketPrice : Input minimum basket price
                Input argu :
                    minimum_basket_price - minimum basket price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        minimum_basket_price = arg["minimum_basket_price"]

        xpath = Util.GetXpath({"locate": "minimum_basket_price"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": minimum_basket_price, "result": "1"})

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.InputMinimumBasketPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShippingFee(arg):
        '''
        InputShippingFee : Input shipping fee
                Input argu :
                    shipping_fee - shipping fee
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping_fee = arg["shipping_fee"]

        xpath = Util.GetXpath({"locate": "shipping_fee"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shipping_fee, "result": "1"})

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.InputShippingFee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"confirm_btn"}), "message":"Click confirm btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupConfirm(arg):
        '''
        ClickPopupConfirm : Click popup confirm btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"confirm_btn"}), "message":"Click popup confirm btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.ClickPopupConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit btn
                Input argu :
                    rule_name - rule name you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_name = arg['rule_name']
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("replace_rule_name", rule_name)

        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click edit btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicate(arg):
        '''
        ClickDuplicate : Click duplicate btn
                Input argu :
                    rule_name - rule name you want to duplicate
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_name = arg['rule_name']
        xpath = Util.GetXpath({"locate":"duplicate_btn"})
        xpath = xpath.replace("replace_rule_name", rule_name)

        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click duplicate btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.ClickDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnd(arg):
        '''
        ClickEnd : Click end btn
                Input argu :
                    rule_name - rule name you want to end
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_name = arg['rule_name']
        xpath = Util.GetXpath({"locate":"end_btn"})
        xpath = xpath.replace("replace_rule_name", rule_name)

        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click end btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.ClickEnd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        BaseUICore.Click({"method":"javascript", "locate":Util.GetXpath({"locate":"delete_btn"}), "message":"Click delete btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'SC_ShippingFeePromotion.ClickDelete')


class SCShippingSettingButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChannelToggle(arg):
        '''
        ClickChannelToggle : Click channel toggle button
                Input argu :
                    channel_name - channel name to be clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        channel_name = arg['channel_name']
        ret = 1

        ##Click toggle by channel_name
        xpath = Util.GetXpath({"locate":"toggle_btn"})
        xpath = xpath.replace("string_to_be_replaced", channel_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click channel toggle", "result": "1"})

        OK(ret, int(arg['result']), 'SCShippingSettingButton.ClickChannelToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : After clicking channel toggle, click confirm button in popup window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'SCShippingSettingButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : After clicking channel toggle, click cancel button in popup window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'SCShippingSettingButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandChannel(arg):
        '''
        ClickExpandChannel : Expand channel
                Input argu :
                    channel_name - channel name to be expanded
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        channel_name = arg['channel_name']
        ret = 1

        ##Click icon for expanding by channel_name
        xpath = Util.GetXpath({"locate":"icon"})
        xpath = xpath.replace("string_to_be_replaced", channel_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Expand channel", "result": "1"})

        OK(ret, int(arg['result']), 'SCShippingSettingButton.ClickExpandChannel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableCODToggle(arg):
        '''
        ClickEnableCODToggle : Toggle on/off enable COD
                Input argu :
                    channel_name - enable COD of channel name to be clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        channel_name = arg['channel_name']
        ret = 1

        ##Click enable COD toggle by channel_name
        xpath = Util.GetXpath({"locate":"COD_toggle_btn"})
        xpath = xpath.replace("string_to_be_replaced", channel_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Toggle on/off COD", "result": "1"})

        OK(ret, int(arg['result']), 'SCShippingSettingButton.ClickEnableCODToggle')
