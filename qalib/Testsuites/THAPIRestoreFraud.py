import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THAPIRestoreFraud(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIRestoreFraud Precondition =="
        dumplogger.info("== Setup THAPIRestoreFraud Post condition")

    def test_THAPIRestoreFraud001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/THAPIRestoreFraud-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIRestoreFraud Post condition =="
        dumplogger.info("== Setup THAPIRestoreFraud Post condition")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
