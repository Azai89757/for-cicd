#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminDesignTools.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchDesignAdmin(arg):
    '''
    LaunchDesignAdmin : Launch design admin and Login with google account
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    db_file = "admin_cookie_design"
    design_admin_cookie = {}

    ##Determine country
    url = "https://design.staging.shopee.io/"

    ##Go to BE
    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(15)

    ##Click login btn
    xpath = Util.GetXpath({"locate": "login_btn"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login btn", "result": "1"})

    ##Get current env and country
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

    if 'design' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "","assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        design_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        design_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        design_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['design'] = [design_admin_cookie]

    ##Go to design admin
    BaseUICore.SetBrowserCookie({"storage_type": "design", "result": "1"})
    ##Go to url
    time.sleep(10)
    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(10)

    OK(ret, int(arg['result']), 'LaunchDesignAdmin')


class AdminDesignToolsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBackgroundRemoverImg(arg):
        '''
        UploadBackgroundRemoverImg : Upload image for background remover
                Input argu :
                    img_locate - image locate
                    file_name - image name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        img_locate = arg["img_locate"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload image for background remover
        locate = Util.GetXpath({"locate": img_locate})
        BaseUICore.UploadFileWithCSS({"locate": locate, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.UploadBackgroundRemoverImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseRegion(arg):
        '''
        ChooseRegion : Choose region
                Input argu :
                    region - which region you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        region = arg["region"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath,"message": "Click dropdown list", "result": "1"})

        ##Choose region
        xpath = Util.GetXpath({"locate": region})
        BaseUICore.Click({"method": "xpath", "locate": xpath,"message": "Click dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseRegion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBranding(arg):
        '''
        ChooseBranding : Choose branding
                Input argu :
                    category - which category you want to choose
                    brand_kit - which brand kit you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category = arg["category"]
        brand_kit = arg["brand_kit"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "category"})
        xpath = xpath.replace("category_to_be_replaced", category)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category", "result": "1"})

        ##Choose region
        xpath = Util.GetXpath({"locate": "brand_kit"})
        xpath = xpath.replace("name_to_be_replaced", brand_kit)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click brand kit", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseBranding')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseTemplate(arg):
        '''
        ChooseTemplate : Choose template
                Input argu :
                    template - which template you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        template = arg["template"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": "template_search"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": template, "result": "1"})

        ##Click search
        xpath = Util.GetXpath({"locate": "search"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search", "result": "1"})

        ##Click template
        xpath = Util.GetXpath({"locate": "template"})
        xpath = xpath.replace("name_to_be_replaced", template)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click template", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseElement(arg):
        '''
        ChooseElement : Choose element
                Input argu :
                    template - which element you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element = arg["element"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": "element_search"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": element, "result": "1"})

        ##Click search
        xpath = Util.GetXpath({"locate": "search"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search", "result": "1"})

        ##Click template
        xpath = Util.GetXpath({"locate": "element"})
        xpath = xpath.replace("name_to_be_replaced", element)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click element", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseElement')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseText(arg):
        '''
        ChooseText : Choose text
                Input argu :
                    text_combination - which text combination you want to choose
                    text_type - which text type you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text_combination = arg["text_combination"]
        text_type = arg["text_type"]

        ##Click text combination
        xpath = Util.GetXpath({"locate": "text_combination"})
        xpath = xpath.replace("name_to_be_replaced", text_combination)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click text combination", "result": "1"})

        ##Click text type
        xpath = Util.GetXpath({"locate": "text_type"})
        xpath = xpath.replace("name_to_be_replaced", text_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click text type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImgLayer(arg):
        '''
        UploadImgLayer : Upload image layer
                Input argu :
                    file_name - image name
                    file_type - file type
                    action - action
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        action = arg["action"]

        ##Upload image for background remover
        locate = Util.GetXpath({"locate": "img_locate"})
        BaseUICore.UploadFileWithCSS({"locate": locate, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.UploadImgLayer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseShopeeProductLinkImg(arg):
        '''
        ChooseShopeeProductLinkImg : Choose shopee product link img
                Input argu :
                    img_index - which img you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        img_index = arg["img_index"]

        ##Click img
        xpath = Util.GetXpath({"locate": "img_locate"})
        xpath = xpath.replace("name_to_be_replaced", img_index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click img", "result": "1"})

        ##Click ok
        AdminDesignToolsComponent.ClickOnButton({"button_type": "ok", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseShopeeProductLinkImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBorderStyle(arg):
        '''
        ChooseBorderStyle : Choose border style
                Input argu :
                    style - which border style you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        style = arg["style"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose region
        xpath = Util.GetXpath({"locate": style})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseBorderStyle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadTicketAttachment(arg):
        '''
        UploadTicketAttachment : Upload attachment for create ticket
                Input argu :
                    img_locate - image locate
                    file_name - image name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        img_locate = arg["img_locate"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click choose file button
        xpath = Util.GetXpath({"locate": img_locate})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})

        ##Upload user csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.UploadTicketAttachment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBirthDay(arg):
        '''
        ChooseBirthDay : Choose birthday
                Input argu :
                    birthday - birthday
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        birthday = arg["birthday"]

        ##Get current date time and add minutes on it
        time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(birthday), 0)

        ##Modify birthday
        xpath = Util.GetXpath({"locate": "birthday_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click birthday dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "birthday_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseBirthDay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseUserSelector(arg):
        '''
        ChooseUserSelector : Choose user selector
                Input argu :
                    user_selector - which user you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_selector = arg["user_selector"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": "user_selector_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_selector, "result": "1"})

        ##Choose user
        xpath = Util.GetXpath({"locate": "user_selector"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.ChooseUserSelector')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchTicket(arg):
        '''
        SearchTicket : Search ticket by id / name
                Input argu :
                    search_type - which ticket type you want to search (ticket_id / ticket_name)
                    ticket_data - which ticket data you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        ticket_data = arg["ticket_data"]

        ##Click search icon
        xpath = Util.GetXpath({"locate": search_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search icon", "result": "1"})

        ##Input something to column
        xpath = Util.GetXpath({"locate": search_type + "_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ticket_data, "result": "1"})

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsPage.SearchTicket')


class AdminDesignToolsComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click add new banner button
                Input argu :
                    button_type - type of button which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSection(arg):
        '''
        ClickOnSection : Click any type of section with well defined locator
            Input argu :
                section_type - type of section which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]

        ##Click on section
        xpath = Util.GetXpath({"locate": section_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click section: %s, which xpath is %s" % (section_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsComponent.ClickOnSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnTab(arg):
        '''
        ClickOnTab : Click any type of section with well defined locator
            Input argu :
                tab_type - type of section which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click on section
        xpath = Util.GetXpath({"locate": tab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath,"message": "Click section: %s, which xpath is %s" % (tab_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsComponent.ClickOnTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
            Input argu :
                checkbox_type - type of checkbox which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        ##Click on checkbox
        xpath = Util.GetXpath({"locate": checkbox_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath,"message": "Click check box: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column (for example, shopee_product_link)
                input_content - whicj content you want to input
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NavigateToPage(arg):
        '''
        NavigateToPage : Navigate to a page based on page_type
            Input argu :
                page_type - type of page to be navigate to
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]
        templates_name = arg["templates_name"]

        ##Click and navigate to page
        xpath = Util.GetXpath({"locate": page_type})
        xpath = xpath.replace("name_to_be_replaced", templates_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Navigate to Page: %s, which xpath is %s" % (page_type, xpath),"result": "1"})

        OK(ret, int(arg['result']), 'AdminDesignToolsComponent.NavigateToPage')
