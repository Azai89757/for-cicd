import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCListing Precondition =="
        dumplogger.info("== Setup INPCListing Precondition ==")

    def test_INPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCListing-01.xml")

    def test_INPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCListing-02.xml")

    def test_INPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCListing Post condition =="
        dumplogger.info("== Setup INPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
