import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSignup Precondition =="
        dumplogger.info("== Setup BRPCSignup Precondition ==")

    def test_BRPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSignup Post condition =="
        dumplogger.info("== Setup BRPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
