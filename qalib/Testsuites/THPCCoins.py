import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class THPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCoins Precondition =="
        dumplogger.info("== Setup THPCCoins Precondition ==")

    def test_THPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-01.xml")

    def test_THPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-02.xml")

    def test_THPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-03.xml")

    def test_THPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-04.xml")

    def test_THPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-05.xml")

    def test_THPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-06.xml")

    def test_THPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-07.xml")

    def test_THPCCoins08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-08.xml")

    def test_THPCCoins09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-09.xml")

    def test_THPCCoins10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-10.xml")

    def test_THPCCoins11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCoins-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCoins Post condition =="
        dumplogger.info("== Setup THPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
