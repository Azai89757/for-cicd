import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCPageTemplates(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPageTemplates Precondition =="
        dumplogger.info("== Setup IDPCPageTemplates Precondition ==")

    def test_IDPCPageTemplates01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPageTemplates-01.xml")

    def test_IDPCPageTemplates02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPageTemplates-02.xml")

    def test_IDPCPageTemplates03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPageTemplates-03.xml")

    def test_IDPCPageTemplates04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPageTemplates-04.xml")

    def test_IDPCPageTemplates05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPageTemplates-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPageTemplates Post condition =="
        dumplogger.info("== Setup IDPCPageTemplates Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
