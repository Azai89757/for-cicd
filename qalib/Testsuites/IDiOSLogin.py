import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class IDiOSLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDiOSLogin Precondition =="
        dumplogger.info("== Setup IDiOSLogin Precondition ==")

        ##Get device id
        if Config._DeviceID_ == "":
            Config._DeviceID_ = GetiOSDeviceId("iOS")
        else:
            dumplogger.info("Already have device id.")
        print Config._DeviceID_

        #Get device info by device id
        if Config._DeviceID_:

            ##Start Appium
            iOSStartAppium({"result": "1"})

            ##Get iOS version of this device
            device_version = GetiOSDeviceInfo()

            ##Call function from iOSBaseUICore
            print "Device connection check: Pass"
            print "Get device version: " + device_version

            ##Assign device platform version & device udid to global var
            Config._iOSDesiredCaps_['udid'] = Config._DeviceID_

            ##Call function from iOSBaseUICore
            iOSInitialDriver()

        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_IDiOSLogin001(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDiOSLogin-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDiOSLogin Post condition =="
        dumplogger.info("== Setup IDiOSLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
