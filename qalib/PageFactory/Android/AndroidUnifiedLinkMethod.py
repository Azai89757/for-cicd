#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidUnifiedLinkMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidUnifiedLink:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LaunchUrlWithChrome(arg):
        '''
        LaunchUrlWithChrome : Launch url with chrome
                Input argu :
                    url - website url
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        url = arg["url"]

        ##Open chrome app
        chrome_pkg = "com.android.chrome/com.google.android.apps.chrome.Main"
        AndroidBaseUICore.SendADBCommand({"command_type":"LaunchAPP", "adb_arg":chrome_pkg, "is_return": "0", "result": "1"})

        ##Clear chrome app storage
        chrome_app = "com.android.chrome"
        AndroidBaseUICore.SendADBCommand({"command_type":"CleanAppData", "adb_arg":chrome_app, "is_return": "0", "result": "1"})

        ##Open chrome app
        chrome_pkg = "com.android.chrome/com.google.android.apps.chrome.Main"
        AndroidBaseUICore.SendADBCommand({"command_type":"LaunchAPP", "adb_arg":chrome_pkg, "is_return": "0", "result": "1"})

        ##Click accept and continue in chorme
        xpath = Util.GetXpath({"locate":"accept_continue"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click accept & continue button", "result": "1"})

        ##Click no thanks in sign page
        xpath = Util.GetXpath({"locate":"no_thanks"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click no thanks button", "result": "1"})

        # Go to url
        xpath = Util.GetXpath({"locate": "search_bar"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":url, "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidUnifiedLink.LaunchUrlWithChrome')
