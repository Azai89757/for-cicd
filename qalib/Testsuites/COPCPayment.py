import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCPayment Precondition =="
        dumplogger.info("== Setup COPCPayment Precondition ==")

    def test_COPCPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCPayment-01.xml")

    def test_COPCPayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCPayment-02.xml")

    def test_COPCPayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCPayment-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCPayment Post condition =="
        dumplogger.info("== Setup COPCPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
