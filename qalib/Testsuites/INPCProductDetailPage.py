import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCProductDetailPage Precondition =="
        dumplogger.info("== Setup INPCProductDetailPage Precondition ==")

    def test_INPCProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductDetailPage-01.xml")

    def test_INPCProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductDetailPage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCProductDetailPage Post condition =="
        dumplogger.info("== Setup INPCProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
