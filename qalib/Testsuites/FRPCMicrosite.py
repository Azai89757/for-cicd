import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCMicrosite Precondition =="
        dumplogger.info("== Setup FRPCMicrosite Precondition ==")

    def test_FRPCMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-01.xml")

    def test_FRPCMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-02.xml")

    def test_FRPCMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-03.xml")

    def test_FRPCMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-04.xml")

    def test_FRPCMicrosite05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-05.xml")

    def test_FRPCMicrosite06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-06.xml")

    def test_FRPCMicrosite07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-07.xml")

    def test_FRPCMicrosite08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-08.xml")

    def test_FRPCMicrosite09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMicrosite-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCMicrosite Post condition =="
        dumplogger.info("== Setup FRPCMicrosite Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
