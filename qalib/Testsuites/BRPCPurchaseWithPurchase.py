import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCPurchaseWithPurchase(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCPurchaseWithPurchase Precondition =="
        dumplogger.info("== Setup BRPCPurchaseWithPurchase Precondition ==")

    def test_BRPCPurchaseWithPurchase01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCPurchaseWithPurchase-01.xml")

    def test_BRPCPurchaseWithPurchase02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCPurchaseWithPurchase-02.xml")

    def test_BRPCPurchaseWithPurchase03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCPurchaseWithPurchase-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCPurchaseWithPurchase Post condition =="
        dumplogger.info("== Setup BRPCPurchaseWithPurchase Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
