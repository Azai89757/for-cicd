#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 NotificationMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class NotificationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRedirect(arg):
        '''
        CheckRedirect : Check can redirect to target page correctly in sub notification folder
                Input argu :
                    redirect - myProducts / shoppingCart / view_details
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        redirect = arg["redirect"]

        if redirect == "myProducts":
            xpath = Util.GetXpath({"locate": "label_btn_view_product_detail"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message":"Go to myProducts page", "result": "1"})
            time.sleep(4)

            ##Switch windows handling to second page
            BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "0", "result": "1"})

            ##Switch view mode to listing of pictures
            ##Try to click ok if there is a reminder
            xpath = Util.GetXpath({"locate": "Back_to_view_with_picture"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in reminder", "result": "1"})

            else:
                dumplogger.info("Failed to click ok in reminder, maybe it didn't showed up.")

        else:

            ##Click in notification content
            xpath = Util.GetXpath({"locate": redirect})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Go to " + redirect + " page", "result": "1"})

        OK(ret, int(arg['result']), 'NotificationPage.CheckRedirect -> ' + redirect)


class NotificationButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectSubNotiffolder(arg):
        '''
        SelectSubNotiffolder : Go to selected sub notification folder in notification page
                Input argu :
                    folder - Promotions / Listing / Activity / Rating / Wallet / ShopeeUpdate
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        folder = arg["folder"]

        ##Go to selected sub notification folder
        xpath = Util.GetXpath({"locate": folder})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Go to selected sub notification folder", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'NotificationButton.SelectSubNotiffolder -> ' + folder)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDown(arg):
        '''
        ClickDropDown : Click drop down button in notification page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click drop down button in notification page
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down button in notification page", "result": "1"})

        OK(ret, int(arg['result']), 'NotificationButton.ClickDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollowInSellerStore(arg):
        '''
        ClickFollowInSellerStore : Click follow button in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click follow button in order detail page
        xpath = Util.GetXpath({"locate": "label_follow"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Go to selected sub notification folder", "result": "1"})

        OK(ret, int(arg['result']), 'NotificationButton.ClickFollowInSellerStore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstNotification(arg):
        '''
        ClickFirstNotification : Click first notification
                Input argu :
                    notification_folder - order_updates / promotions / wallet_updates / activity / rating_updates / shopee_updates
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        notification_folder = arg["notification_folder"]

        ##Click first notification
        xpath = Util.GetXpath({"locate": notification_folder})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first notification", "result": "1"})

        OK(ret, int(arg['result']), 'NotificationButton.ClickFirstNotification')
