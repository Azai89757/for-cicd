import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSellerDiscount Precondition =="
        dumplogger.info("== Setup PHPCSellerDiscount Precondition ==")

    def test_PHPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-01.xml")

    def test_PHPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-02.xml")

    def test_PHPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-03.xml")

    def test_PHPCSellerDiscount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-04.xml")

    def test_PHPCSellerDiscount05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-05.xml")

    def test_PHPCSellerDiscount06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-06.xml")

    def test_PHPCSellerDiscount07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerDiscount-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSellerDiscount Post condition =="
        dumplogger.info("== Setup PHPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
