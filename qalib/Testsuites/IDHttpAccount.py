import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class IDHttpAccount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpAccount Precondition =="
        dumplogger.info("== Setup IDHttpAccount Precondition ==")

    def test_IDHttpAccount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-01.xml")

    def test_IDHttpAccount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-02.xml")

    def test_IDHttpAccount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-03.xml")

    def test_IDHttpAccount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-04.xml")

    def test_IDHttpAccount05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-05.xml")

    def test_IDHttpAccount06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-06.xml")

    def test_IDHttpAccount07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-07.xml")

    def test_IDHttpAccount08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpAccount-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpAccount Post condition =="
        dumplogger.info("== Setup IDHttpAccount Post condition ==")
