import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCListing Precondition =="
        dumplogger.info("== Setup CLPCListing Precondition ==")

    def test_CLPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCListing-01.xml")

    def test_CLPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCListing-02.xml")

    def test_CLPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCListing Post condition =="
        dumplogger.info("== Setup CLPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
