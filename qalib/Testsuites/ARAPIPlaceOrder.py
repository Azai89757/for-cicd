import unittest
from Config import dumplogger
from FrameWorkBase import *


class ARAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup ARAPIPlaceOrder Precondition ==")

    def test_ARAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/ARAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup ARAPIPlaceOrder Post condition ==")
