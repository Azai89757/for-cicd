import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCCommissionFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCommissionFee Precondition =="
        dumplogger.info("== Setup IDPCCommissionFee Precondition ==")

    def test_IDPCCommissionFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-01.xml")

    def test_IDPCCommissionFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-02.xml")

    def test_IDPCCommissionFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-03.xml")

    def test_IDPCCommissionFee04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-04.xml")

    def test_IDPCCommissionFee05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-05.xml")

    def test_IDPCCommissionFee06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-06.xml")

    def test_IDPCCommissionFee07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-07.xml")

    def test_IDPCCommissionFee08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-08.xml")

    def test_IDPCCommissionFee09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-09.xml")

    def test_IDPCCommissionFee10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-10.xml")

    def test_IDPCCommissionFee11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCommissionFee-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCommissionFee Post condition =="
        dumplogger.info("== Setup IDPCCommissionFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
