import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCLogin Precondition =="
        dumplogger.info("== Setup VNPCLogin Precondition ==")

    def test_VNPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCLogin-01.xml")

    def test_VNPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCLogin Post condition =="
        dumplogger.info("== Setup VNPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
