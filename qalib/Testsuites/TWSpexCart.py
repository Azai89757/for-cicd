import unittest
from Config import dumplogger
from FrameWorkBase import *
from api.spex import SpexAPICore
import Config


class TWSpexCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexCart Precondition =="
        dumplogger.info("== Setup TWSpexCart Precondition ==")

        ##Tail log file to grep "Local connections to /run/spex/spex.sock:-2 forwarded to remote address /run/spex/spex.sock"
        ##If Spex InitResult is True, will not call RegisterToSpex() again
        if SpexAPICore.InspectSpexConnection():
            ##Register to Spex with specific type of spex configuration for the first time
            if not Config._SpexRegResult_:
                if SpexAPICore.GetSpexConfig('HashtagSearch_Test'):
                    Config._SpexRegResult_ = SpexAPICore.RegisterToSpex()

                    ##Check outcome after register
                    if Config._SpexRegResult_:
                        print "== Setup TWSpexCart Register Success =="
                        dumplogger.info("== Setup TWSpexCart Register Success ==")

                    else:
                        print ("Spex Register Failed!!!")
                        dumplogger.error("Spex Register Failed")
                        ##Directly Leave Setup
                        self.skipTest("")
                else:
                    dumplogger.error("Get Spex config from ini failed!!!")
                    self.skipTest("")
            else:
                dumplogger.info("Spex is already registered.")

        else:
            print "== Setup TWSpexCart Precondition Fail by No Spex Agent Connection =="
            dumplogger.error("== Setup TWSpexCart Precondition Fail by No Spex Agent Connection ==")
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWSpexCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCart-01.xml")

    def test_TWSpexCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCart-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexCart Post condition =="
        dumplogger.info("== Setup TWSpexCart Post condition ==")
