import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCSignup Precondition =="
        dumplogger.info("== Setup TWPCSignup Precondition ==")

    def test_TWPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCSignup-01.xml")

    def test_TWPCSignup02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCSignup-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCSignup Post condition =="
        dumplogger.info("== Setup TWPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
