import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class FRPCProductCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCProductCollection Precondition =="
        dumplogger.info("== Setup FRPCProductCollection Precondition ==")

    def test_FRPCProductCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-01.xml")

    def test_FRPCProductCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-02.xml")

    def test_FRPCProductCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-03.xml")

    def test_FRPCProductCollection04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-04.xml")

    def test_FRPCProductCollection05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-05.xml")

    def test_FRPCProductCollection06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-06.xml")

    def test_FRPCProductCollection07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCollection-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCProductCollection Post condition =="
        dumplogger.info("== Setup FRPCProductCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
