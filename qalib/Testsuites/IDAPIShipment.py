import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIShipment Precondition =="
        dumplogger.info("== Setup IDAPIShipment Precondition ==")

    def test_IDAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIShipment Post condition =="
        dumplogger.info("== Setup IDAPIShipment Post condition ==")
