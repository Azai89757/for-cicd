import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCMePage Precondition =="
        dumplogger.info("== Setup MXPCMePage Precondition ==")

    def test_MXPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCMePage Post condition =="
        dumplogger.info("== Setup MXPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
