import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpReceipt(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpReceipt Precondition =="
        dumplogger.info("== Setup TWHttpReceipt Precondition ==")

    def test_TWHttpReceipt01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReceipt-01.xml")

    def test_TWHttpReceipt02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReceipt-02.xml")

    def test_TWHttpReceipt03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReceipt-03.xml")

    def test_TWHttpReceipt04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReceipt-04.xml")

    def test_TWHttpReceipt05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReceipt-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpReceipt Post condition =="
        dumplogger.info("== Setup TWHttpReceipt Post condition ==")
