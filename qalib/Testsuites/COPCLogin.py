import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCLogin Precondition =="
        dumplogger.info("== Setup COPCLogin Precondition ==")

    def test_COPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCLogin-01.xml")

    def test_COPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCLogin Post condition =="
        dumplogger.info("== Setup COPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
