import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCUserPortal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCUserPortal Precondition =="
        dumplogger.info("== Setup IDPCUserPortal Precondition ==")

    def test_IDPCUserPortal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCUserPortal-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCUserPortal Post condition =="
        dumplogger.info("== Setup IDPCUserPortal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
