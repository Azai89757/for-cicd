import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCListing Precondition =="
        dumplogger.info("== Setup PLPCListing Precondition ==")

    def test_PLPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCListing-01.xml")

    def test_PLPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCListing-02.xml")

    def test_PLPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCListing Post condition =="
        dumplogger.info("== Setup PLPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
