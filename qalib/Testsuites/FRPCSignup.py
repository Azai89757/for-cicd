import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCSignup Precondition =="
        dumplogger.info("== Setup FRPCSignup Precondition ==")

    def test_FRPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCSignup Post condition =="
        dumplogger.info("== Setup FRPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
