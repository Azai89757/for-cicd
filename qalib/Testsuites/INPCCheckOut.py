import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCCheckOut Precondition =="
        dumplogger.info("== Setup INPCCheckOut Precondition ==")

    def test_INPCCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-01.xml")

    # def test_INPCCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/IN/INPCCheckOut-02.xml")

    def test_INPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-03.xml")

    def test_INPCCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-04.xml")

    def test_INPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-05.xml")

    def test_INPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-06.xml")

    def test_INPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-07.xml")

    def test_INPCCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-08.xml")

    # def test_INPCCheckOut09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/IN/INPCCheckOut-09.xml")

    def test_INPCCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-10.xml")

    def test_INPCCheckOut11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCheckOut-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCCheckOut Post condition =="
        dumplogger.info("== Setup INPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
