import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCForm(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCForm Precondition =="
        dumplogger.info("== Setup IDPCForm Precondition ==")

    def test_IDPCForm01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCForm-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCForm Post condition =="
        dumplogger.info("== Setup IDPCForm Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
