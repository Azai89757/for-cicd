import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCRefundCalculationAndStateMachine(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCRefundCalculationAndStateMachine Precondition =="
        dumplogger.info("== Setup BRPCRefundCalculationAndStateMachine Precondition ==")

    def test_BRPCRefundCalculationAndStateMachine01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-01.xml")

    def test_BRPCRefundCalculationAndStateMachine02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-02.xml")

    def test_BRPCRefundCalculationAndStateMachine03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-03.xml")

    def test_BRPCRefundCalculationAndStateMachine04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-04.xml")

    def test_BRPCRefundCalculationAndStateMachine05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-05.xml")

    def test_BRPCRefundCalculationAndStateMachine06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-06.xml")

    def test_BRPCRefundCalculationAndStateMachine07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRefundCalculationAndStateMachine-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCRefundCalculationAndStateMachine Post condition =="
        dumplogger.info("== Setup BRPCRefundCalculationAndStateMachine Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
