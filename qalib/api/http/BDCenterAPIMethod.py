#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 BDCenterAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import json

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class BDCenterAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCBOption(arg):
        '''
        SetCBOption : Change cb label of seller information
                Input argu :
                    userid - user id which want to change the cb label
                    cb_option - 1 (yes) / 0 (no)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        userid = int(arg['userid'])
        cb_option = int(arg['cb_option'])

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/BDCenter/set_cb_option", "result": "1"})

        ##Load Api data to be sent
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_cb_option", "result": "1"})

        ##Assign region, userid and cb_option from argument to global
        GlobalAdapter.CommonVar._DynamicCaseData_["region"] = Config._TestCaseRegion_.upper()
        GlobalAdapter.CommonVar._DynamicCaseData_["userid"] = userid
        GlobalAdapter.CommonVar._DynamicCaseData_["cb_option"] = cb_option

        ##Set payload value from global
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "region", "target": "region", "time_deviation": "", "result": "1"})
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "userid", "target": "userid", "time_deviation": "", "result": "1"})
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "cb_option", "target": "cb_option", "time_deviation": "", "result": "1"})
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "response_text", "source": "region", "target": "region", "time_deviation": "", "result": "1"})
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "response_text", "source": "userid", "target": "userid", "time_deviation": "", "result": "1"})
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "response_text", "source": "cb_option", "target": "cb_option", "time_deviation": "", "result": "1"})

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send http request
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Deinitial api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'BDCenterAPI.SetCBOption')
