import unittest
from Config import dumplogger
from FrameWorkBase import *


class PHAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup PHAPIRestoreHomePopupBanner Precondition ==")

    def test_PHAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/PHAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup PHAPIRestoreHomePopupBanner Post condition ==")
