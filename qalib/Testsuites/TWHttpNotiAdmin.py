import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpNotiAdmin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpNotiAdmin Precondition =="
        dumplogger.info("== Setup TWHttpNotiAdmin Precondition ==")

    def test_TWHttpNotiAdmin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpNotiAdmin-01.xml")

    def test_TWHttpNotiAdmin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpNotiAdmin-02.xml")

    def test_TWHttpNotiAdmin03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpNotiAdmin-03.xml")

    def test_TWHttpNotiAdmin04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpNotiAdmin-04.xml")

    def test_TWHttpNotiAdmin05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpNotiAdmin-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpNotiAdmin Post condition =="
        dumplogger.info("== Setup TWHttpNotiAdmin Post condition ==")
