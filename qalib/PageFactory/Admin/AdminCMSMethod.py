﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCMSMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminOfficialShopsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewButton(arg):
        '''
        ClickAddNewButton : Click add new banner button for official and shop banner
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new banner button
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new banner button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.ClickAddNewButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveMallShop(arg):
        '''
        ClickSaveMallShop : Click save btn of mall shop
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save mall shop.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.ClickSaveMallShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateHomepageMallShop(arg):
        '''
        CreateHomepageMallShop : Create mall shop in Shopee homepage
                Input argu :
                    shop_id - shop id
                    shop_name - mall shop name
                    EN_text - English promotion text(English only)
                    local_text - local language promotion text
                    visibility_type - visible / invisible
                    is_sold_spot - 1 (yes) / 0 (no)
                    landing_page - direct to which url
                    is_upload - 1 (yes) / 0 (no)
                    adjust_end_time - modify end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        country = Config._TestCaseRegion_.lower()

        shop_id = arg["shop_id"]
        shop_name = arg["shop_name"]
        EN_text = arg["EN_text"]
        local_text = arg["local_text"]
        visibility_type = arg["visibility_type"]
        is_sold_spot = arg["is_sold_spot"]
        landing_page = arg["landing_page"]
        is_upload = arg["is_upload"]
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]

        ##Enter mall shop id, if shop_id == "", then maintain current status
        if shop_id:
            xpath = Util.GetXpath({"locate": "shop_id"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        ##Enter mall shop name, if shop_name == "", then maintain current status
        if shop_name:
            xpath = Util.GetXpath({"locate": "shop_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_name, "result": "1"})

        ##Enter English promotion text, if EN_text == "", then maintain current status
        if EN_text:
            xpath = Util.GetXpath({"locate": "EN_text"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": EN_text, "result": "1"})

        ##Enter local-language promotion text, if local_text == "", then maintain current status
        if local_text and Config._TestCaseRegion_ != "PH":
            xpath = Util.GetXpath({"locate": country})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":local_text, "result": "1"})

        ##Choose if Visible
        if visibility_type:
            xpath = Util.GetXpath({"locate": visibility_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility", "result": "1"})

        ##Choose if sold spot
        if int(is_sold_spot):
            xpath = Util.GetXpath({"locate": "is_sold_spot"})
        else:
            xpath = Util.GetXpath({"locate": "not_sold_spot"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose sold spot", "result": "1"})

        ##Enter landing page url
        if landing_page:
            xpath = Util.GetXpath({"locate": "landing_page"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": landing_page, "result": "1"})

        ##Input End Time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(adjust_start_time), 0)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start, "result": "1"})

        ##Input End Time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(adjust_end_time), 0)
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end, "result": "1"})

        ##Upload mall shop banner image
        if int(is_upload):
            ##Choose a pic from library
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-image-to-add-0", "element_type": "id", "project": "HomePage", "action": "image", "file_name": "MallShop_1", "file_type": "jpg", "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": "remove_image_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Remove current image", "result": "1"})
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        time.sleep(20)

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.CreateHomepageMallShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditMallShopButton(arg):
        '''
        ClickEditMallShopButton : Click edit btn of mall shop
                Input argu :
                    mall_shop_name - mall shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        mall_shop_name = arg["mall_shop_name"]
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("name_to_be_replaced", mall_shop_name)

        ##Check if mall shop name exist
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose sold spot", "result": "1"})
        else:
            dumplogger.info("Unable to find xpath!!!")
            ret = 0

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.ClickEditMallShopButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteHomepageMallShopByName(arg):
        '''
        DeleteHomepageMallShopByName : Delete all mall shop in Shopee homepage with specific name
                Input argu :
                    mall_shop_name - mall shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mall_shop_name = arg["mall_shop_name"]

        ##Check if specific btn exists
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("name_to_be_replaced", mall_shop_name)

        for retry_times in range(5):

            ##If exist, disable the shop first and then delete it
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):

                ##Make mall shop invisible first
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit mall shop", "result": "1"})
                xpath = Util.GetXpath({"locate": "non_visible"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Change visible status to invisible", "result": "1"})
                time.sleep(3)
                xpath = Util.GetXpath({"locate": "save_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
                time.sleep(3)

                ##Delete mall shop
                xpath = Util.GetXpath({"locate": "delete_btn"})
                xpath = xpath.replace("name_to_be_replaced", mall_shop_name)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete btn", "result": "1"})

                ##Click confirm btn
                xpath = Util.GetXpath({"locate": "confirm_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})

                ##Refresh in case previous state still exist
                time.sleep(5)
                BaseUILogic.BrowserRefresh({"message": "Refresh page", "result": "1"})
                time.sleep(5)

            else:
                dumplogger.info("There is no shop to delete.")

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.DeleteHomepageMallShopByName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchMallShopByName(arg):
        '''
        SearchMallShopByName : Search specific mall shop by name
                Input argu :
                    mall_shop_name - mall shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        mall_shop_name = arg["mall_shop_name"]

        ##Enter reason
        xpath = Util.GetXpath({"locate": "mall_shop_search_bar"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": mall_shop_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.SearchMallShopByName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeMallShopOrder(arg):
        '''
        ChangeMallShopOrder : Change target mall shop to the top of list
                Input argu: N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Choose display 100 per page
        xpath = Util.GetXpath({"locate": "display_per_page_select"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change display order", "result": "1"})
        xpath = Util.GetXpath({"locate": "display_100_per_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click display 100 order per page", "result": "1"})
        time.sleep(5)

        ##Click change display order
        xpath = Util.GetXpath({"locate": "change_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change display order", "result": "1"})

        ##Get order length
        xpath = Util.GetXpath({"locate": "row"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
        order_count = GlobalAdapter.GeneralE2EVar._Count_

        ##Move last order to first(top 1 row is title)
        for orders in range(order_count, 2, -1):
            ##Get last element
            xpath = Util.GetXpath({"locate": "banner_order"}).replace('name_to_be_replace', shop_name)
            ##Get target element(former order, but need move crossing former order so -2)
            xpath_target = Util.GetXpath({"locate": "row_with_order"}).replace('order_to_be_replace', str(orders - 2))
            ##Drag element
            BaseUICore.DragAndDrop({"drag_elem": xpath, "target_elem": xpath_target, "result": "1"})

            ##Move to current order element
            xpath = Util.GetXpath({"locate": "banner_order"}).replace('name_to_be_replace', shop_name)
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
            time.sleep(2)

        ##Click save
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to save order", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopsPage.ChangeMallShopOrder')


class AdminMeSellingPage:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditMePage(arg):
        '''
        ClickEditMePage : Click edit mepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mepage edit button
        xpath = Util.GetXpath({"locate": "mepage_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mepage edit button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickEditMePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeFeatureOrder(arg):
        '''
        ChangeFeatureOrder : Change miscellaneous feature order of mepage
                Input argu:
                    drag_feature : feature to drag
                    drop_feature : feature to drop

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drag_feature = arg["drag_feature"]
        drop_feature = arg["drop_feature"]

        ##Click change feature order
        xpath = Util.GetXpath({"locate": "change_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change feature order", "result": "1"})
        time.sleep(5)

        ##Get drag element
        drag_xpath = Util.GetXpath({"locate": "drag_element"}).replace('FEATURE', drag_feature)
        ##Get drop element
        drop_xpath = Util.GetXpath({"locate": "drop_element"}).replace('FEATURE', drop_feature)

        ##Drag element and drop
        BaseUICore.DragAndDrop({"drag_elem": drag_xpath, "target_elem": drop_xpath, "result": "1"})
        time.sleep(3)

        ##Click save
        AdminMeSellingPage.ClickSaveOrderButton({"type": "miscellaneous", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminMeSellingPage.ChangeFeatureOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFeatureEditButton(arg):
        '''
        ClickFeatureEditButton : Click feature edit
                Input argu :
                    feature_name - feature name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        feature_name = arg["feature_name"]

        ##Click mepage feature edit button
        xpath = Util.GetXpath({"locate": "feature_edit_btn"})
        xpath = xpath.replace("feature_name_to_be_replace", feature_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mepage feature edit button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickFeatureEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFeatureDelete(arg):
        '''
        ClickFeatureDelete : Click feature delete
                Input argu :
                    feature_name - feature name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        feature_name = arg["feature_name"]

        ##Click mepage feature delete button
        xpath = Util.GetXpath({"locate": "feature_delete_btn"})
        xpath = xpath.replace("feature_name_to_be_replace", feature_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mepage feature delete button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickFeatureDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFeatureDeleteConfirmButton(arg):
        '''
        ClickFeatureDeleteConfirmButton : Click delete confirm button
                Input argu :
                    type - cancel / delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click delete confirm button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickFeatureDeleteConfirmButton->' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCircleSlot(arg):
        '''
        SetCircleSlot : Circles slot setting
                Input argu :
                    circle_item - circle item
                    circle_sys_name - set circle system name
                    circle_display_name_eng - set circle english display name
                    circle_display_name_region - set circle local language display name
                    circle_redirect - circle redirect url
                    start_time - circle item start time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_item = arg["circle_item"]
        circle_sys_name = arg["circle_sys_name"]
        circle_display_name_eng = arg["circle_display_name_eng"]
        circle_display_name_region = arg["circle_display_name_region"]
        circle_redirect = arg["circle_redirect"]
        start_time = arg["start_time"]

        ##Input circle system name
        if circle_sys_name:
            xpath = Util.GetXpath({"locate":"sys_name"})
            xpath = xpath.replace("string_to_be_replace", "subfeature-system-name-" + circle_item)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":circle_sys_name, "message":"Input circle system name", "result": "1"})

        ##Input circle english display name
        if circle_display_name_eng:
            xpath = Util.GetXpath({"locate":"display_name_english"})
            xpath = xpath.replace("string_to_be_replace", "subfeature-display-name-display_en-" + circle_item)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":circle_display_name_eng, "message":"Input circle english display name", "result": "1"})

        ##Input circle local display name
        if circle_display_name_region:
            xpath = Util.GetXpath({"locate":"display_name_region"})
            if Config._TestCaseRegion_ == "TW":
                xpath = xpath.replace("string_to_be_replace", "subfeature-display-name-display_zh_" + Config._TestCaseRegion_.lower() + '-' + circle_item)
            else:
                xpath = xpath.replace("string_to_be_replace", "subfeature-display-name-display_" + Config._TestCaseRegion_.lower() + '-' + circle_item)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":circle_display_name_region, "message":"Input circle local display name", "result": "1"})

        ##Input circle redirection url
        if circle_redirect:
            xpath = Util.GetXpath({"locate":"redirection_url"})
            xpath = xpath.replace("string_to_be_replace", "subfeature-redirection-url-" + circle_item)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":circle_redirect, "message":"Input circle redirection url", "result": "1"})

        ##Input start time
        if start_time:
            icon_start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
            xpath = Util.GetXpath({"locate": "start_time"})
            xpath = xpath.replace("string_to_be_replace", "start-subfeature-segment-" + circle_item)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": icon_start_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetCircleSlot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCircleSlotIcon(arg):
        '''
        SetCircleSlotIcon : Circles slot icon setting
                Input argu :
                    circle_item - circle item
                    circle_image_file - circle image
                    circle_image_type - circle image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_item = arg["circle_item"]
        circle_image_file = arg["circle_image_file"]
        circle_image_type = arg["circle_image_type"]

        BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-image-to-add-" + circle_item, "element_type": "id", "project": "MePage", "action": "image", "file_name": circle_image_file, "file_type": circle_image_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetCircleSlotIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteCircleButton(arg):
        '''
        ClickDeleteCircleButton : Circles delete circle button
                Input argu :
                    circle_item - circle item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_item = arg["circle_item"]

        ##Click delete circle button
        xpath = Util.GetXpath({"locate":"delete_button"})
        xpath = xpath.replace("string_to_be_replace", "delete-subfeature-segment-" + circle_item)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete circle button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickDeleteCircleButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateCircleButton(arg):
        '''
        ClickCreateCircleButton : Circles create circle button
                Input argu :
                    circle_item - circle item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_item = arg["circle_item"]

        ##Click create circle button
        xpath = Util.GetXpath({"locate":"create_button"})
        xpath = xpath.replace("string_to_be_replace", "create-subfeature-segment-" + circle_item)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click create circle button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickCreateCircleButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetFeatureBasicAttributes(arg):
        '''
        SetFeatureBasicAttributes : Set feature basic attributes
                Input argu :
                    system_feature - Click
                    group - Order / Activity
                    start_time - Feature start time
                    end_time - Feature end time
                    system_name - system name
                    display_name - display name
                    display_name_region - display name region
                    redirection_URL - redirection URL
                    access_to_unauthenticated - 1 for enable, 0 for disable
                    show_feature - 1 for enable, 0 for disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        system_feature = arg["system_feature"]
        group = arg["group"]
        system_name = arg["system_name"]
        display_name = arg["display_name"]
        display_name_region = arg["display_name_region"]
        redirection_URL = arg["redirection_URL"]
        access_to_unauthenticated = arg["access_to_unauthenticated"]
        show_feature = arg["show_feature"]
        start_time = int(arg["start_time"])
        end_time = int(arg["end_time"])

        ##Click system_feature
        if system_feature:
            xpath = Util.GetXpath({"locate": "system_feature"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click system_feature", "result": "1"})

        ##Set start time
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=start_time, is_tw_time = 1)
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Set end time
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=end_time, is_tw_time = 1)
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        ##Select group and click group item
        if group:
            xpath = Util.GetXpath({"locate": "group_select"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group dropdown select", "result": "1"})
            xpath = Util.GetXpath({"locate": "group_item"})
            xpath = xpath.replace("group_name_to_be_replace", group)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group dropdown item", "result": "1"})

        ##Input system name
        if system_name:
            xpath = Util.GetXpath({"locate": "system_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": system_name, "message": "Input system name", "result": "1"})

        ##Input display name(en)
        if display_name:
            xpath = Util.GetXpath({"locate": "display_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "message": "Input display name", "result": "1"})

        ##Input display name(region)
        if display_name_region:
            xpath = Util.GetXpath({"locate": "display_name_region"})
            if Config._TestCaseRegion_ == "TW":
                xpath = xpath.replace("display_name_region_to_be_replace", "display_zh_" + Config._TestCaseRegion_.lower())
            else:
                xpath = xpath.replace("display_name_region_to_be_replace", "display_" + Config._TestCaseRegion_.lower())
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name_region, "message": "Input display name(region)", "result": "1"})

        ##Input redirection url
        if redirection_URL:
            xpath = Util.GetXpath({"locate": "redirection_url"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirection_URL, "message": "Input redirection url", "result": "1"})

        ##Click access to unauthenticated users
        if int(access_to_unauthenticated):
            xpath = Util.GetXpath({"locate": "access_to_unauthenticated"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click access to unauthenticated users check box", "result": "1"})

        ##Click visibility
        if int(show_feature):
            xpath = Util.GetXpath({"locate": "show_feature"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show feature check box", "result": "1"})

        ##Set Minimum version requirements
        xpath = Util.GetXpath({"locate": "ios_min_version"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "1.00.00", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetFeatureBasicAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetFeatureIconImage(arg):
        '''
        SetFeatureIconImage : Set feature icon image
                Input argu :
                    icon_image_file - icon image file
                    icon_image_type - icon image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_image_file = arg["icon_image_file"]
        icon_image_type = arg["icon_image_type"]

        ##Upload icon image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-image-to-add-0", "element_type": "id", "project": "MePage", "action": "image", "file_name": icon_image_file, "file_type": icon_image_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetFeatureIconImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetFeatureNewBadge(arg):
        '''
        SetFeatureNewBadge : Set feature new badge
                Input argu :
                    new_badge_datetime - new badge datetime (optional)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        new_badge_datetime = arg["new_badge_datetime"]

        ##Click new badge set new badge datetime
        xpath = Util.GetXpath({"locate": "new_badge"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new badge check box", "result": "1"})

        ##Input badge_datetime
        if new_badge_datetime:
            datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(new_badge_datetime), 0, 1)
            xpath = Util.GetXpath({"locate": "new_badge_datetime"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": datetime, "message": "Input new badge datetime", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetFeatureNewBadge')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetFeatureUserScope(arg):
        '''
        SetFeatureUserScope : Set feature user scope
                Input argu :
                    user_scope - All / Whitelist / Percentage(Grayscale) / Rule Set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_scope = arg["user_scope"]

        ##Click user scope dropdown select
        xpath = Util.GetXpath({"locate": "user_scope_select"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown select", "result": "1"})

        ##Click user scope dropdown item
        xpath = Util.GetXpath({"locate": "user_scope_dropdown_item"})
        xpath = xpath.replace("user_scope_name_to_be_replace", user_scope)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetFeatureUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectRuleSet(arg):
        '''
        SelectRuleSet : Select rule set
                Input argu :
                    rule_set_item - rule set item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_set_item = arg["rule_set_item"]

        ##Click select rule set
        xpath = Util.GetXpath({"locate": "rule_set_select"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select rule set", "result": "1"})

        ##Click rule set item
        xpath = Util.GetXpath({"locate": "rule_set_item"})
        xpath = xpath.replace("rule_set_item_to_be_replace", rule_set_item)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set item", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SelectRuleSet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UplaodWhitelist(arg):
        '''
        UplaodWhitelist : Uplaod whitelist
                Input argu :
                    whitelist_file - whitelist csv
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        whitelist_file = arg["whitelist_file"]

        ##Click choose file button
        xpath = Util.GetXpath({"locate": "choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})
        XtFunc.WindowsKeyboardEvent(action="esc")
        time.sleep(3)

        ##Click and upload file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-upload-to-add-0", "element_type": "id", "project": "MePage", "action": "file", "file_name": whitelist_file, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})
        time.sleep(5)

        ##Click awesome button
        xpath = Util.GetXpath({"locate": "awesome_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click awesome button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.UplaodWhitelist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGrayscaleTime(arg):
        '''
        InputGrayscaleTime : Input grayscale time
                Input argu :
                    grayscale_start_time - grayscale start time
                    grayscale_end_time - grayscale end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        grayscale_start_time = arg["grayscale_start_time"]
        grayscale_end_time = arg["grayscale_end_time"]

        ##Input start time
        xpath = Util.GetXpath({"locate": "grayscale_start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": grayscale_start_time, "message": "Input grayscale start time", "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "grayscale_end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": grayscale_end_time, "message": "Input grayscale end time", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.InputGrayscaleTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetFeatureBannerBundle(arg):
        '''
        SetFeatureBannerBundle : Set feature banner bundle
                Input argu :
                    bundle_type - system_banner_bundle / user_banner_bunder
                    bundle_item_type - system_banner_bundle_item / user_banner_bundle_item
                    banner_bundle - banner bundle name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bundle_type = arg["bundle_type"]
        bundle_item_type = arg["bundle_item_type"]
        banner_bundle = arg["banner_bundle"]

        ##Select banner bundle
        xpath = Util.GetXpath({"locate": bundle_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner bundle dropdown select", "result": "1"})
        xpath = Util.GetXpath({"locate": bundle_item_type})
        xpath = xpath.replace("banner_bundle_to_be_replace", banner_bundle)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner_bundle dropdown item", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetFeatureBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdateFeatureButton(arg):
        '''
        ClickUpdateFeatureButton : Click update feature
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click update feature button
        xpath = Util.GetXpath({"locate": "update_feature_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update feature button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickUpdateFeatureButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDiscardChangesButton(arg):
        '''
        ClickDiscardChangesButton : Click discard change button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click discard change button
        xpath = Util.GetXpath({"locate": "discard_change_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click discard change button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickDiscardChangesButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDiscardChangesConfirmButton(arg):
        '''
        ClickDiscardChangesConfirmButton : Click discard change confirm button
                Input argu :
                    type - cancel / discard
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click discard change button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickDiscardChangesConfirmButton ->' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReorderGroupsButton(arg):
        '''
        ClickReorderGroupsButton : Click reorder groups
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reorder groups button
        xpath = Util.GetXpath({"locate": "reorder_groups_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reorder groups button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickReorderGroupsButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewFeatureButton(arg):
        '''
        ClickAddNewFeatureButton : Click add new feature
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new feature button
        xpath = Util.GetXpath({"locate": "new_feature_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new feature button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickAddNewFeatureButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateFeatureButton(arg):
        '''
        ClickCreateFeatureButton : Click create feature
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create feature button
        xpath = Util.GetXpath({"locate": "create_feature_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create feature button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickCreateFeatureButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateConfirmButton(arg):
        '''
        ClickCreateConfirmButton : Click create confirm
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create confirm button
        xpath = Util.GetXpath({"locate": "create_confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create confirm button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickCreateConfirmButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReorderGroupToLastPosition(arg):
        '''
        ReorderGroupToLastPosition : Reorder group to last position
                Input argu :
                    group_name - group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_name = arg["group_name"]

        ##Move one of groups to last group position
        xpath = Util.GetXpath({"locate": "reorder_group"})
        xpath = xpath.replace("group_name_to_be_replace", group_name)
        xpath_target_group = Util.GetXpath({"locate": "last_group"})
        BaseUICore.DragAndDrop({"drag_elem": xpath, "target_elem": xpath_target_group, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ReorderGroupToLastPosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveOrderButton(arg):
        '''
        ClickSaveOrderButton : Click save order
                Input argu :
                    type - group/ order/ activity/ miscellaneous
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Set up xpath for different save btn
        xpath = Util.GetXpath({"locate": type})

        ##Click save order button
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save order button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickSaveOrderButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadButton(arg):
        '''
        ClickDownloadButton : Click download button
                Input argu :
                    feature_name - feature name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        feature_name = arg['feature_name']
        ret = 1

        ##Click download button
        xpath = Util.GetXpath({"locate":"download_button"})
        xpath = xpath.replace("string_to_be_replaced", feature_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click download button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickDownloadButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableCircle(arg):
        '''
        ClickEnableCircle : Click Enable circles
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Enable circles
        xpath = Util.GetXpath({"locate":"enabled_circle"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enabled circle checkbox", "result": "1"})

        ##Click Enable redirection for main feature
        xpath = Util.GetXpath({"locate":"enabled_redirection"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enabled redirection checkbox", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickEnableCircle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCircleAttributes(arg):
        '''
        SetCircleAttributes : Set circle attributes
                Input argu :
                    redirection_url - redirection url
                    subtext_en - feature subtext (English)
                    subtext_region - feature subtext (region)
                    circle_slot_item - circle slot item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        redirection_url = arg['redirection_url']
        subtext_en = arg['subtext_en']
        subtext_region = arg['subtext_region']
        circle_slot_item = arg['circle_slot_item']

        if redirection_url:
            ##Input Main feature redirection URL
            xpath = Util.GetXpath({"locate":"redirection_url"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":redirection_url, "message":"Input redirection url", "result": "1"})

        if subtext_en:
            ##Input feature subtext (English)
            xpath = Util.GetXpath({"locate":"subtext_en"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":subtext_en, "message":"Input feature subtext (English)", "result": "1"})

        if subtext_region:
            ##Input feature subtext (region)
            xpath = Util.GetXpath({"locate":"subtext_region"})
            if Config._TestCaseRegion_ == "TW":
                xpath = xpath.replace("REGION", "zh_" + Config._TestCaseRegion_.lower())
            else:
                xpath = xpath.replace("REGION", Config._TestCaseRegion_.lower())
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":subtext_region, "message":"Input feature subtext (region)", "result": "1"})

        if circle_slot_item:
            ##Click Circle slot
            xpath = Util.GetXpath({"locate":"circle_slot_select"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click circle slot select", "result": "1"})
            xpath = Util.GetXpath({"locate":"circle_slot_item"})
            xpath = xpath.replace("string_to_be_replace", circle_slot_item)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click circle slot item", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.SetCircleAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCircleTab(arg):
        '''
        ClickCircleTab : Circles circle tab
                Input argu :
                    circle_item - circle item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_item = arg["circle_item"]

        ##Click circle tab
        xpath = Util.GetXpath({"locate":"circle_tab"})
        xpath = xpath.replace("string_to_be_replace", "tab-subfeature-segment-" + circle_item)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click circle tab", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.ClickCircleTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreFeatureList(arg):
        '''
        RestoreFeatureList : Delete feature
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get all delete button contains "AUTO"
        xpath = Util.GetXpath({"locate":"delete_buttons"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"multi", "result": "1"})

        ##Delete all "AUTO" feature
        for number in range(len(GlobalAdapter.CommonVar._PageAttributesList_)):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
            AdminMeSellingPage.ClickFeatureDeleteConfirmButton({"type": "delete", "result": "1"})
            time.sleep(3)
        else:
            dumplogger.info("There is no feature to delete.")

        OK(ret, int(arg["result"]), 'AdminMeSellingPage.RestoreFeatureList')


class AdminHomePageCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu :
                    flag - home_square / home_square_background / wallet_bar_background
                    order - element order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        flag = arg["flag"]
        order = arg["order"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": flag + "_edit_btn"})
        xpath = xpath.replace("string_to_be_replaced", order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminHomePageCommon.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu :
                    flag - home_square / home_square_background / wallet_bar_background /scheduled_icon / campaign_caption
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        flag = arg["flag"]

        ##Click add new homesquare / homesquare background
        if flag == "home_square" or flag == "home_square_background":
            xpath_add_new = Util.GetXpath({"locate":"add_new"})
            xpath_flag = Util.GetXpath({"locate":"add_new_" + flag})
            BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath_add_new, "locatehidden":xpath_flag, "message":"Click add new: " + flag, "result": "1"})

        else:
            ##Click add new
            xpath = Util.GetXpath({"locate":"add_new"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditStartEndTime(arg):
        '''
        EditStartEndTime : Edit start / end time
                Input argu :
                    adjust_start_time - Start time that the component started display
                    adjust_end_time - End time that the component ended display
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]

        ##Get current date time and add minutes on it
        time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_start_time), 0)
        time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_start_time), 0)

        ##Modify start date
        xpath = Util.GetXpath({"locate": "start_date_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "start_date_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Modify start time
        xpath = Util.GetXpath({"locate": "start_time_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "start_time_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Get current date time and add minutes on it
        time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_end_time), 0)
        time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_end_time), 0)

        ##Modify end date
        xpath = Util.GetXpath({"locate": "end_date_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "end_date_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Modify end time
        xpath = Util.GetXpath({"locate": "end_time_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "end_time_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.EditStartEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditVisible(arg):
        '''
        EditVisible : Edit visible
                Input argu :
                    visible - visible / invisible
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        visible = arg["visible"]

        ##Edit visible
        xpath = Util.GetXpath({"locate":visible})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select visible: " + visible, "result":"1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.EditVisible')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditUrl(arg):
        '''
        EditUrl : Edit redirect url
                Input argu :
                    url - url content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        url = arg["url"]

        ##Enter url
        xpath = Util.GetXpath({"locate": "url"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminHomePageCommon.EditUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditImage(arg):
        '''
        EditImage : Edit image
                Input argu :
                    flag - home_square / home_square_background / wallet_bar_background / scheduled_icon
                    upload_type - file / image
                    file_name - file name
                    file_type - file type
                    project - image file project path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        flag = arg["flag"]
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        project = arg["project"]

        for try_time in range(5):
            ##Upload banner image with input and handle error image upload exception
            locate = Util.GetXpath({"locate": flag + "_image_field"})
            BaseUICore.UploadFileWithCSS({"locate": locate, "action": upload_type, "file_name": file_name, "file_type": file_type, "result": "1"})
            time.sleep(15)
            dumplogger.info("Try upload image for %d time." % (try_time))

            image_xpath = Util.GetXpath({"locate": "banner_img"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": image_xpath, "passok": "0", "result": "1"}):
                ret = 1
                break
            else:
                time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHomePageCommon.EditImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectScopeType(arg):
        '''
        SelectScopeType : select scope type
            Input argu :
                scope_type - no_scope / user_list / rule_set
                user_list_mode - white_list / non_black_list
                action - action name
                project - file project path
                file_name - file name
                file_type - file type
                rule_set_id - rule set id
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope_type = arg["scope_type"]
        user_list_mode = arg["user_list_mode"]
        action = arg["action"]
        project = arg["project"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        rule_set_id = arg["rule_set_id"]

        ##Select scope type
        xpath = Util.GetXpath({"locate": scope_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select scope type: " + scope_type, "result": "1"})
        time.sleep(5)

        if scope_type == "user_list":
            ##Select user list mode
            xpath = Util.GetXpath({"locate": "user_list_drop_down_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user list mode drop down box", "result": "1"})
            xpath = Util.GetXpath({"locate": user_list_mode})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select user list mode: " + user_list_mode, "result": "1"})

            if action:
                ##Upload user file
                xpath = Util.GetXpath({"locate": "action"})
                xpath = xpath.replace("name_to_be_replaced", action)
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name":file_name, "file_type":file_type, "result":"1"})
                time.sleep(2)

        elif scope_type == "rule_set":
            ##Input rule set id
            xpath = Util.GetXpath({"locate": "rule_set_drop_down_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set drop down box", "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": rule_set_id, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.SelectScopeType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditUserScope(arg):
        '''
        EditUserScope : Edit user scope
                Input argu :
                    scope - by_wallet_status / by_coins_status / by_rule_set
                    users - all_users / activated_users / non_activated_users / users_with_existing_coins / users_with_no_existing_coins
                    rule_set - rule set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope = arg["scope"]
        users = arg["users"]
        rule_set = arg["rule_set"]

        ##Select user scope
        xpath = Util.GetXpath({"locate": scope})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select user scope: " + scope, "result":"1"})

        if scope == "by_wallet_status":
            ##Select wallet status dropdown box
            xpath = Util.GetXpath({"locate": "wallet_status_dropdown_box"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select wallet status dropdown box", "result":"1"})

            ##Select users
            xpath = Util.GetXpath({"locate": users})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select " + users, "result":"1"})

        elif scope == "by_coins_status":
            ##Select coins status dropdown box
            xpath = Util.GetXpath({"locate": "coins_status_dropdown_box"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select coins status dropdown box", "result":"1"})

            ##Select users
            xpath = Util.GetXpath({"locate": users})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select " + users, "result":"1"})

        elif scope == "by_rule_set":
            ##Select rule set dropdown box
            xpath = Util.GetXpath({"locate": "rule_set_dropdown_box"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select rule set dropdown box", "result":"1"})

            ##Select rule set
            xpath = Util.GetXpath({"locate": "rule_set"})
            xpath = xpath.replace("name_to_be_replaced", rule_set)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select " + rule_set, "result":"1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.EditUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete
                Input argu :
                    action - yes / cancel
                    flag - home_square / home_square_background / wallet_bar_background / scheduled_icon
                    order - order of the component you want to delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        flag = arg["flag"]
        order = arg['order']

        ##Click delete component with specific order
        xpath = Util.GetXpath({"locate":flag + "_delete_btn"})
        xpath = xpath.replace('string_to_be_replaced', order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
        time.sleep(5)

        ##Click confirm
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose action -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomePageCommon.ClickDelete')


class AdminHomeSquarePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateHomeSquare(arg):
        '''
        CreateHomeSquare : Create new home square
                Input argu :
                    adjust_start_time - Start time that home square started display
                    adjust_end_time - End time that home square ended display
                    visible - Home square visible (invisible / visible)
                    mobile_display - Whether home squares display on mobile app or not (0: don't display ; 1: display)
                    pc_display - Whether home squares display on pc web or not (0: don't display ; 1: display)
                    url - url the home square redirect to
                    title_en - title that display on English env
                    title_local - title that display on local env
                    image_name - upload image name
                    image_type - upload image type
                    project - image file project path
                    mobile_order - mobile order
                    pc_order - pc order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        visible = arg["visible"]
        mobile_display = arg["mobile_display"]
        pc_display = arg["pc_display"]
        url = arg["url"]
        title_en = arg["title_en"]
        title_local = arg["title_local"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]
        project = arg["project"]
        mobile_order = arg["mobile_order"]
        pc_order = arg["pc_order"]

        ##Click add new square button
        AdminHomePageCommon.ClickAddNew({"flag":"home_square", "result": "1"})
        time.sleep(10)

        ##Input start time and end time
        AdminHomePageCommon.EditStartEndTime({"adjust_start_time":adjust_start_time, "adjust_end_time":adjust_end_time, "result": "1"})

        ##Select visible
        AdminHomePageCommon.EditVisible({"visible":visible, "result": "1"})

        ##Determine whether display on mobile/pc or not
        time.sleep(3)
        AdminHomeSquarePage.EditDisplayPlatform({"mobile_display":mobile_display, "pc_display":pc_display, "result": "1"})

        ##Input mobile order
        AdminHomeSquarePage.EditMobileOrder({"mobile_order": mobile_order, "result": "1"})
        AdminHomeSquarePage.EditPCOrder({"pc_order": pc_order, "result": "1"})

        ##Input url
        AdminHomePageCommon.EditUrl({"url":url, "result": "1"})

        ##Input eng title
        AdminHomeSquareComponent.InputToColumn({"column_type": "channel_eng_title", "input_content": title_en, "result": "1"})

        ##Input local lang title
        AdminHomeSquareComponent.InputToColumn({"column_type": "local_title", "input_content": title_local, "result": "1"})

        ##Input chinese title (Only MY have 3 language input)
        if Config._TestCaseRegion_ in ("MY", "SG", "TW"):
            title_chinese = arg["title_chinese"]
            AdminHomeSquareComponent.InputToColumn({"column_type": "title_chinese", "input_content": title_chinese, "result": "1"})

        ##Upload static image for home square
        AdminHomePageCommon.EditImage({"flag":"home_square", "upload_type":"image", "file_name":image_name, "file_type":image_type, "project":project, "result": "1"})
        time.sleep(15)

        ##Click save
        AdminHomePageCommon.ClickSave({"result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHomeSquarePage.CreateHomeSquare' + ' -> '+title_en)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditDisplayPlatform(arg):
        '''
        EditDisplayPlatform : Edit home square display platform
                Input argu :
                    mobile_display - Whether home squares display on mobile app or not (0: don't display ; 1: display)
                    pc_display - Whether home squares display on pc web or not (0: don't display ; 1: display)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mobile_display = arg["mobile_display"]
        pc_display = arg["pc_display"]

        ##Determine whether display on mobile/pc or not
        if int(mobile_display):
            xpath = Util.GetXpath({"locate": "display_on_mobile_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click display on mobile checkbox", "result": "1"})
            time.sleep(2)

        if int(pc_display):
            xpath = Util.GetXpath({"locate": "display_on_pc_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click display on pc checkbox", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminHomeSquarePage.EditDisplayPlatform')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanHomeSquare(arg):
        '''
        CleanHomeSquare : Delete all home squares from Admin
                Input argu :
                    platform - pc/mobile
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        platform = arg["platform"]

        ##Choose specific platform
        xpath = Util.GetXpath({"locate":platform})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click switch platform", "result": "1"})
        time.sleep(5)

        ##Get home square number
        xpath = Util.GetXpath({"locate": "homesquare_delete_btn"})
        BaseUILogic.GetAndReserveElements({"locate": xpath, "reservetype": "count", "result": "1"})
        component_count = GlobalAdapter.GeneralE2EVar._Count_
        dumplogger.info("Homesquare component count = %s" % (component_count))

        ##Start delete all home square
        for delete_time in range(component_count):

            ##Click delete home square
            xpath = Util.GetXpath({"locate":"delete_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
            time.sleep(3)

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
            time.sleep(5)

        ##Check if the no-home-square label exists, if not, means didn't clean all of them
        xpath = Util.GetXpath({"locate": "banner"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ret = 0
            dumplogger.info("Clean homesquare process fail !!!")

        OK(ret, int(arg['result']), 'AdminHomeSquarePage.CleanHomeSquare->' + 'Delete ' + str(component_count) + ' homesquare(s)')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePlatform(arg):
        '''
        ChoosePlatform : Choose specific platform
                Input argu :
                    platform - pc/mobile
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        platform = arg["platform"]

        ##Choose specific platform
        time.sleep(10)
        xpath = Util.GetXpath({"locate": platform})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click switch platform", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHomeSquarePage.ChoosePlatform')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeHomeSquarePosition(arg):
        '''
        ChangeHomeSquarePosition : Change specific home square position
                Input argu :
                    element - home square you want to change
                    input_position - input position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element = arg["element"]
        input_position = arg["input_position"]

        ##Change specific home square position
        xpath = Util.GetXpath({"locate": "edit_box"})
        xpath = xpath.replace('string_to_be_replaced', element)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_position, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHomeSquarePage.ChangeHomeSquarePosition')


class AdminHomeSquareComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column (for example, channel_eng_title / local_title / title_chinese / pc_order_field)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 0
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        country = {
            "mx": "es_mx",
            "my": "ms_my",
            "br": "pt_BR",
            "sg": "ms_my",
            "co": "es_co",
            "cl": "es_cl",
            "pl": "pl",
            "es": "es_es",
            "in": "hi",
            "fr": "fr",
            "vn": "vi",
            "ph": "zhHans",
            "th": "th",
            "ar": "es_ar",
            "tw": "zhHans"
        }

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        if column_type == "local_title":
            xpath = xpath.replace("name_to_be_replaced", country[Config._TestCaseRegion_.lower()])
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomeSquareComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 0
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath,"message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomeSquareComponent.ClickOnButton')


class AdminHomeSquareBackgroundPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateHomeSquareBackground(arg):
        '''
        CreateHomeSquareBackground : Create new home square background
                Input argu :
                    adjust_start_time - Start time that home square background started display
                    adjust_end_time - End time that home square background ended display
                    visible - Home square background visible (invisible / visible)
                    text_color - text color
                    carousel_bar_color - carousel bar color
                    image_name - upload image name
                    image_type - upload image type
                    project - image file project path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        visible = arg["visible"]
        text_color = arg["text_color"]
        carousel_bar_color = arg["carousel_bar_color"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]
        project = arg["project"]

        ##Click add new square background button
        AdminHomePageCommon.ClickAddNew({"flag":"home_square_background", "result": "1"})
        time.sleep(5)

        ##Input start time and end time
        AdminHomePageCommon.EditStartEndTime({"adjust_start_time":adjust_start_time, "adjust_end_time":adjust_end_time, "result": "1"})

        ##Select visible
        AdminHomePageCommon.EditVisible({"visible":visible, "result": "1"})

        ##Input text color
        AdminHomeSquareBackgroundPage.EditTextColor({"text_color":text_color, "result": "1"})

        ##Input carousel bar color
        AdminHomeSquareBackgroundPage.EditCarouselBarColor({"carousel_bar_color":carousel_bar_color, "result": "1"})

        ##Upload static image for home square background
        AdminHomePageCommon.EditImage({"flag":"home_square_background", "upload_type":"image", "file_name":image_name, "file_type":image_type, "project":project, "result": "1"})

        ##Click save
        time.sleep(5)
        AdminHomePageCommon.ClickSave({"result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHomeSquareBackgroundPage.CreateHomeSquareBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditTextColor(arg):
        '''
        EditTextColor : Edit text color
                Input argu :
                    text_color - text color
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text_color = arg["text_color"]

        ##Edit text color
        xpath = Util.GetXpath({"locate": "text_color"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_color, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomeSquareBackgroundPage.EditTextColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCarouselBarColor(arg):
        '''
        EditCarouselBarColor : Edit carousel bar color
                Input argu :
                    carousel_bar_color - carousel bar color
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        carousel_bar_color = arg["carousel_bar_color"]

        ##Edit carousel bar color
        xpath = Util.GetXpath({"locate": "carousel_bar_color"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": carousel_bar_color, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomeSquareBackgroundPage.EditCarouselBarColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanHomeSquareBackground(arg):
        '''
        CleanHomeSquareBackground : Delete all home square background from Admin
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get home square background number
        xpath = Util.GetXpath({"locate": "homesquare_background_delete_btn"})
        BaseUILogic.GetAndReserveElements({"locate": xpath, "reservetype": "count", "result": "1"})
        component_count = GlobalAdapter.GeneralE2EVar._Count_

        ##Start delete all home square background
        for delete_time in range(component_count):

            ##Click delete home square background
            xpath = Util.GetXpath({"locate":"delete_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
            time.sleep(3)

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
            time.sleep(5)

        ##Check if the no-home-square background label exists, if not, means didn't clean all of them
        xpath = Util.GetXpath({"locate": "no_home_square_background"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ret = 0
            dumplogger.info("Clean homesquare background process fail !!!")

        OK(ret, int(arg['result']), 'AdminHomeSquareBackgroundPage.CleanHomeSquareBackground->' + 'Delete ' + str(component_count) + ' homesquare background(s)')

class AdminCampaignCaptionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateCampaignCaption(arg):
        '''
        CreateCampaignCaption : Create campaign caption
                Input argu :
                    adjust_start_time - Start time that campaign caption started display
                    adjust_end_time - End time that campaign caption ended display
                    visible - campaign caption visible (invisible / visible)
                    url - url
                    display_text - display text
                    caption - caption
                    scope - by_wallet_status / by_coins_status / by_rule_set
                    users - all_users / activated_users / non_activated_users / users_with_existing_coins / users_with_no_existing_coins
                    rule_set - rule set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        visible = arg["visible"]
        url = arg["url"]
        display_text = arg["display_text"]
        caption = arg["caption"]
        scope = arg["scope"]
        users = arg["users"]
        rule_set = arg["rule_set"]

        ##Click add new text button
        AdminHomePageCommon.ClickAddNew({"flag":"campaign_caption", "result": "1"})
        time.sleep(5)

        ##Input start time and end time
        AdminHomePageCommon.EditStartEndTime({"adjust_start_time":adjust_start_time, "adjust_end_time":adjust_end_time, "result": "1"})

        ##Select visible
        AdminHomePageCommon.EditVisible({"visible":visible, "result": "1"})

        ##Input url
        AdminHomePageCommon.EditUrl({"url":url, "result": "1"})

        ##Input display text
        AdminCampaignCaptionPage.EditDisplayText({"display_text":display_text, "result": "1"})

        ##Input caption
        AdminCampaignCaptionPage.EditCaption({"caption":caption, "result": "1"})

        ##Select user scope
        AdminHomePageCommon.EditUserScope({"scope":scope, "users":users, "rule_set":rule_set, "result": "1"})

        ##Click save
        AdminHomePageCommon.ClickSave({"result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.CreateCampaignCaption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCampaignCaption(arg):
        '''
        ClickEditCampaignCaption : Click edit campaign caption
                Input argu :
                    name - caption name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click edit campaign caption
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit caption", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.ClickEditCampaignCaption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditDisplayText(arg):
        '''
        EditDisplayText : Edit display text
                Input argu :
                    display_text - display text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        display_text = arg["display_text"]

        ##Edit display text
        xpath = Util.GetXpath({"locate": "display_text"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_text, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.EditDisplayText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCaption(arg):
        '''
        EditCaption : Edit caption
                Input argu :
                    caption - caption
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        caption = arg["caption"]

        ##Edit caption
        xpath = Util.GetXpath({"locate": "caption"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": caption, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.EditCaption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteCampaignCaption(arg):
        '''
        DeleteCampaignCaption : Delete campaign caption
                Input argu :
                    campaign_text - campaign text
                    action - yes / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign_text = arg["campaign_text"]
        action = arg["action"]

        ##Delete specific component if it exists
        xpath = Util.GetXpath({"locate": "campaign"})
        xpath = xpath.replace("string_to_be_replaced", campaign_text)

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click delete
            xpath = Util.GetXpath({"locate":"delete"})
            xpath = xpath.replace("text_to_be_replaced", campaign_text)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete", "result":"1"})

            ##Click yes
            xpath = Util.GetXpath({"locate":action})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click yes", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.DeleteCampaignCaption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditWalletBackground(arg):
        '''
        EditWalletBackground : Edit wallet caption
                Input argu :
                    order - wallet bar order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]

        ##Edit wallet bar
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("order_to_be_replaced", order)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit wallet background", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.EditWalletBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteWalletBackground(arg):
        '''
        DeleteWalletBackground : Delete wallet caption
                Input argu :
                    order - wallet bar order
                    action - yes / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]
        action = arg["action"]

        ##Delete wallet bar
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace("order_to_be_replaced", order)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete wallet background", "result":"1"})
        time.sleep(2)

        ##Choose action
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose action -> " + action, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignCaptionPage.DeleteWalletBackground')

class AdminEditDefaultCaptionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDefaultText(arg):
        '''
        InputDefaultText : Input default text
                Input argu :
                    field_name - field name
                    text - default text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field_name = arg["field_name"]
        text = arg["text"]

        ##Clear default text field
        xpath = Util.GetXpath({"locate": "default_text_field"})
        xpath = xpath.replace("name_to_be_replaced", field_name)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})

        ##Input default text
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminEditDefaultCaptionPage.InputDefaultText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDefaultUrl(arg):
        '''
        InputDefaultUrl : Input default url
                Input argu :
                    field_name - field name
                    url - default url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field_name = arg["field_name"]
        url = arg["url"]

        ##Clear default url field
        xpath = Util.GetXpath({"locate": "default_url_field"})
        xpath = xpath.replace("name_to_be_replaced", field_name)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})

        ##Input default url
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminEditDefaultCaptionPage.InputDefaultUrl')

class AdminDefaultIconsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadIcon(arg):
        '''
        UploadIcon : Upload icon
                Input argu :
                    icon_type - qr_scan_icon / wallet_icon / coins_icon
                    upload_type - file / image
                    file_name - file name
                    file_type - file type
                    project - image file project path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        project = arg["project"]

        ##Upload icon
        xpath = Util.GetXpath({"locate": icon_type})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": upload_type, "file_name":file_name, "file_type":file_type, "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminDefaultIconsPage.UploadIcon')

class AdminScheduledIconPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateScheduledIcon(arg):
        '''
        CreateScheduledIcon : Create Scheduled Icon
                Input argu :
                    adjust_start_time - Start time that Scheduled Icon started display
                    adjust_end_time - End time that Scheduled Icon ended display
                    image_name - image name
                    image_type - image type
                    project - image file project path
                    scope - by_wallet_status / by_coins_status / by_rule_set
                    users - all_users / activated_users / non_activated_users / users_with_existing_coins / users_with_no_existing_coins
                    rule_set - rule set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]
        project = arg["project"]
        scope = arg["scope"]
        users = arg["users"]
        rule_set = arg["rule_set"]

        ##Click add new icon button
        AdminHomePageCommon.ClickAddNew({"flag":"scheduled_icon", "result": "1"})
        time.sleep(5)

        ##Input start time and end time
        AdminHomePageCommon.EditStartEndTime({"adjust_start_time":adjust_start_time, "adjust_end_time":adjust_end_time, "result": "1"})

        ##Upload Scheduled scan icon image
        AdminHomePageCommon.EditImage({"flag":"scheduled_icon", "upload_type":"image", "file_name":image_name, "file_type":image_type, "project":project, "result": "1"})

        ##Select user scope
        AdminHomePageCommon.EditUserScope({"scope":scope, "users":users, "rule_set":rule_set, "result": "1"})

        ##Click save
        AdminHomePageCommon.ClickSave({"result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminScheduledIconPage.CreateScheduledIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditScheduledIcon(arg):
        '''
        EditScheduledIcon : Edit Scheduled Icon
                Input argu :
                    order - icon order
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]
        image_hash = arg["image_hash"]

        ##Click edit scheduled icon
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("order_to_be_replaced", order)
        xpath = xpath.replace("hash_to_be_replaced", image_hash)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit", "result":"1"})

        OK(ret, int(arg['result']), 'AdminScheduledIconPage.EditScheduledIcon')

class AdminWalletBarBackgroundPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateWalletBarBackground(arg):
        '''
        CreateWalletBarBackground : Create wallet bar background
                Input argu :
                    adjust_start_time - Start time that Scheduled Icon started display
                    adjust_end_time - End time that Scheduled Icon ended display
                    visible - visible / invisible
                    image_name - image name
                    image_type - image type
                    project - image file project path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        visible = arg["visible"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]
        project = arg["project"]

        ##Click add new background
        AdminHomePageCommon.ClickAddNew({"flag":"wallet_bar_background", "result": "1"})
        time.sleep(5)

        ##Input start time and end time
        AdminHomePageCommon.EditStartEndTime({"adjust_start_time":adjust_start_time, "adjust_end_time":adjust_end_time, "result": "1"})

        ##Edit visible
        AdminHomePageCommon.EditVisible({"visible":visible, "result": "1"})

        ##Upload background banner image
        AdminHomePageCommon.EditImage({"flag":"wallet_bar_background", "upload_type":"image", "file_name":image_name, "file_type":image_type, "project":project, "result": "1"})

        ##Click save
        AdminHomePageCommon.ClickSave({"result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminWalletBarBackgroundPage.CreateWalletBarBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditSpecificStartEndTime(arg):
        '''
        EditSpecificStartEndTime : Edit background banner specific start / end time
                Input argu :
                    specify_start_date - Specify start date
                    specify_start_time -Specify start time
                    specify_end_date - Specify end date
                    specify_end_time - Specify end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        specify_start_date = arg["specify_start_date"]
        specify_start_time = arg["specify_start_time"]
        specify_end_date = arg["specify_end_date"]
        specify_end_time = arg["specify_end_time"]

        ##Check whether the start date field is empty
        xpath = Util.GetXpath({"locate":"empty_start_date"})
        if not BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Clear start date box
            xpath = Util.GetXpath({"locate":"start_date_box"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result":"1"})
            xpath = Util.GetXpath({"locate":"start_date_close_icon"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start date close icon", "result":"1"})

        ##Input the specified start date
        time_stamp_start_date = specify_start_date
        xpath = Util.GetXpath({"locate":"start_date_box"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start date box", "result":"1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": time_stamp_start_date, "result": "1"})
        xpath = Util.GetXpath({"locate":"date_select"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select start date", "result":"1"})

        ##Input the specified start time
        time_stamp_start_time = specify_start_time
        xpath = Util.GetXpath({"locate":"start_time_box"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time box", "result":"1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": time_stamp_start_time, "result": "1"})

        ##Check whether the end date field is empty
        xpath = Util.GetXpath({"locate":"empty_end_date"})
        if not BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Clear end date box
            xpath = Util.GetXpath({"locate":"end_date_box"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result":"1"})
            xpath = Util.GetXpath({"locate":"end_date_close_icon"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click close icon", "result":"1"})

        ##Input the specified end date
        time_stamp_end_date = specify_end_date
        xpath = Util.GetXpath({"locate":"end_date_box"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end date box", "result":"1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": time_stamp_end_date, "result": "1"})
        xpath = Util.GetXpath({"locate":"date_select"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select end date", "result":"1"})

        ##Input the specified end time
        time_stamp_end_time = specify_end_time
        xpath = Util.GetXpath({"locate":"end_time_box"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time box", "result":"1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": time_stamp_end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminWalletBarBackgroundPage.EditSpecificStartEndTime')

class AdminBannerBundlePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBannerBundle(arg):
        '''
        CreateBannerBundle : Create a banner bundle
                Input argu :
                    banner_type - banner type (Category / Landing Page / Group Banner / Homepage Mall Banner / Home Square / Official Shop / Home Popup / Mall Popup / Microsite / Floating Banner / Skinny Banner)
                    bundle_name - name of bundle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        bundle_name = arg["bundle_name"]

        ##Click create banner bundle
        xpath = Util.GetXpath({"locate":"create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create banner bundle", "result": "1"})
        time.sleep(2)

        ##Click banner type dropdown
        xpath = Util.GetXpath({"locate":"banner_type_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner type dropdown", "result": "1"})
        time.sleep(2)

        ##Choose banner type
        xpath = Util.GetXpath({"locate":"banner_type"})
        xpath = xpath.replace("name_to_be_replaced", banner_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner type dropdown", "result": "1"})
        time.sleep(2)

        ##Input bundle name
        xpath = Util.GetXpath({"locate": "bundle_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": bundle_name, "result": "1"})
        time.sleep(2)

        ##Click save
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerBundlePage.CreateBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditBannerBundle(arg):
        '''
        ClickEditBannerBundle : Click edit a specific banner bundle
                Input argu :
                    bundle_name - name of bundle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bundle_name = arg["bundle_name"]

        ##Click edit banner bundle
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("name_to_be_replaced", bundle_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit banner bundle -> " + bundle_name, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBannerBundlePage.ClickEditBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteBannerBundle(arg):
        '''
        ClickDeleteBannerBundle : Click delete a specific banner bundle
                Input argu :
                    bundle_name - name of bundle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bundle_name = arg["bundle_name"]

        ##Click delete banner bundle
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace("name_to_be_replaced", bundle_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete banner bundle -> " + bundle_name, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBannerBundlePage.ClickDeleteBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBannerID(arg):
        '''
        ClickBannerID : Click banner ID in edit bundle content page
                Input argu :
                    type - type of bundle (banner/homesquare)

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click banner id
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click banner id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerBundlePage.ClickBannerID')


class AdminCMSCategoriesPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CategoryImageUpload(arg):
        '''
        CategoryImageUpload : Upload image for category
                Input argu :
                    name - category name
                    image_name - image name
                    image_type - image type

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]

        ##Click image upload btn
        xpath = Util.GetXpath({"locate":"image_upload_button"})
        xpath = xpath.replace("string_to_be_replaced", name)

        ##If upload btn exist, then upload image
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "HomePage", "action": "image", "file_name": image_name, "file_type": image_type, "element":"cat_img_upload", "element_type":"class", "result": "1"})
        else:
            dumplogger.info("Not able to find this xpath!!!")
            ret = 0

        OK(ret, int(arg['result']), 'AdminCMSCategoriesPage.CategoryImageUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCategoryDisplayName(arg):
        '''
        ModifyCategoryDisplayName : Modify category display name
                Input argu :
                    name - category name

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        original_name = arg["original_name"]
        display_name = arg["display_name"]

        ##Input display name to insert bar
        xpath = Util.GetXpath({"locate":"insert_bar"})
        xpath = xpath.replace("string_to_be_replaced", original_name)

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "result": "1"})
            time.sleep(5)
        else:
            dumplogger.info("Not able to find this xpath!!!")
            ret = 0

        OK(ret, int(arg['result']), 'AdminCMSCategoriesPage.ModifyCategoryDisplayName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveCategorySetting(arg):
        '''
        SaveCategorySetting : Save setting after edit category setting
                Input argu : N/A

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCMSCategoriesPage.SaveCategorySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCategoryExist(arg):
        '''
        CheckCategoryExist : Check category name exists (if not exist, block the delete category step)
                Input argu :
                    name -  category name

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Input display name to insert bar
        xpath = Util.GetXpath({"locate":"category_title"})
        xpath = xpath.replace("string_to_be_replaced", name)

        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Category does not exist")
            ret = 0

        OK(ret, int(arg['result']), 'AdminCMSCategoriesPage.CheckCategoryExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchGlobalCategoryToggle(arg):
        '''
        SwitchGlobalCategoryToggle : Switch global category toggle
                Input argu :
                    action - on / off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Switch global category toggle
        xpath = Util.GetXpath({"locate": action})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Switch global category toggle -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCMSCategoriesPage.SwitchGlobalCategoryToggle')

class AdminOfficialShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchOfficialShopCategory(arg):
        '''
        SearchOfficialShopCategory : Input search keyword for finding official shop category
                Input argu :
                    keyword - search keyword
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        keyword = arg["keyword"]

        ##input search keyword to search bar
        xpath = Util.GetXpath({"locate":"search_bar"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":keyword, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopPage.SearchOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteOfficialShopCategory(arg):
        '''
        DeleteOfficialShopCategory : Delete specific official shop category
                Input argu :
                    shopid - shop id
                    category - category where official shop is in
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        category = arg["category"]

        ##Parameter cannot be null, otherwise xpath is unavailable
        if not shopid or not category:
            dumplogger.info("Argument cannot be null")
            ret = 0

        else:
            ##Locate and click delete btn
            xpath = Util.GetXpath({"locate":"delete_btn"})
            xpath = xpath.replace("shopid_to_be_replaced", shopid)
            xpath = xpath.replace("category_to_be_replaced", category)

            ##Count current exist official shop numbers
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            delete_icon_count = GlobalAdapter.GeneralE2EVar._Count_

            ##If current exist banner number more than one, start delete each banner, if not, skip process
            if delete_icon_count != 0:
                for icon_count in range(delete_icon_count):
                    ##Click to delete banner
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete official shop category", "result": "1"})
                    dumplogger.info("Deleting %d official category" % (icon_count))
                    BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
                    time.sleep(30)

            dumplogger.info("Clean all official shop category succeed.")

        OK(ret, int(arg['result']), 'AdminOfficialShopPage.DeleteOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddOfficialShopCategory(arg):
        '''
        AddOfficialShopCategory : Add a official shop category
                Input argu :
                    category - category wanted to be added
                    shop - shop name
                    brand_name - brand name
                    index - index
                    sorting_name - sorting name
                    app_logo_name - image name for app logo
                    app_logo_type - image type for app logo
                    pc_logo_name - image name for pc logo
                    pc_logo_type - image type for pc logo
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category = arg["category"]
        shop = arg["shop"]
        brand_name = arg["brand_name"]
        index = arg["index"]
        sorting_name = arg["sorting_name"]
        app_logo_name = arg["app_logo_name"]
        app_logo_type = arg["app_logo_type"]
        pc_logo_name = arg["pc_logo_name"]
        pc_logo_type = arg["pc_logo_type"]

        ##Click add new official category
        xpath = Util.GetXpath({"locate":"add_new_category_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new button", "result": "1"})
        time.sleep(5)

        ##Click select category dropdown
        xpath = Util.GetXpath({"locate":"select_category_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click select category dropdown", "result": "1"})
        time.sleep(3)

        ##Choose category
        xpath = Util.GetXpath({"locate":"select_category"})
        xpath = xpath.replace("category_to_be_replaced", category)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose category", "result": "1"})

        ##Reclick category dropdown
        xpath = Util.GetXpath({"locate":"active_category_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click select category dropdown", "result": "1"})

        ##Search shop
        xpath = Util.GetXpath({"locate":"shop_search_bar"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":shop, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        ##Brand name
        xpath = Util.GetXpath({"locate":"brand_name_bar"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":brand_name, "result": "1"})

        ##Search index
        xpath = Util.GetXpath({"locate":"index_bar"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click index input dropdown", "result": "1"})
        time.sleep(3)
        xpath = Util.GetXpath({"locate":"chosen_index"})
        xpath = xpath.replace("index_to_be_replaced", index)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose index", "result": "1"})

        ##Sorting name
        xpath = Util.GetXpath({"locate":"sorting_name_bar"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":sorting_name, "result": "1"})

        ##Upload app logo image
        for try_time in range(5):
            ##Upload app logo image with input and handle error image upload exception
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-image-to-add-0", "element_type": "id", "project": "ActivityBanner", "action": "image", "file_name": app_logo_name, "file_type": app_logo_type, "result": "1"})
            dumplogger.info("Try upload image for %d time." % (try_time))

            ##If there's popup window showing up, close it and re-upload image
            if BaseUILogic.DetectPopupWindow():
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
                ret = 0
                time.sleep(10)
            else:
                dumplogger.info("No upload error image.")
                ret = 1
                break

        ##Upload pc logo image
        for try_time in range(5):
            ##Upload pc logo image with input and handle error image upload exception
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-image-to-add-1", "element_type": "id", "project": "ActivityBanner", "action": "image", "file_name": pc_logo_name, "file_type": pc_logo_type, "result": "1"})
            dumplogger.info("Try upload image for %d time." % (try_time))

            ##If there's popup window showing up, close it and re-upload image
            if BaseUILogic.DetectPopupWindow():
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
                ret = 0
                time.sleep(10)
            else:
                dumplogger.info("No upload error image.")
                ret = 1
                break

        ##Click save btn
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopPage.AddOfficialShopCategory')


class AdminBannerRuleSetPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerRuleSet(arg):
        '''
        EditBannerRuleSet : Edit exist banner rule set
                Input argu :
                    name - bundle name

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Check and click edit btn
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("string_to_be_replaced", name)

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})
        else:
            dumplogger.info("Not able to find this xpath!!!")
            ret = 0

        OK(ret, int(arg['result']), 'AdminBannerRuleSetPage.EditBannerRuleSet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveBannerRuleSet(arg):
        '''
        SaveBannerRuleSet : Save editted banner rule set
                Input argu : N/A

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerRuleSetPage.SaveBannerRuleSet')


class AdminHomepageComponentsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyTwoRowImageFET(arg):
        '''
        ModifyTwoRowImageFET : Do image FET on two-row image section
                Input argu :
                    platform - pc / mobile
                    upload_type - image / file
                    two_row_image_name - 2-row image name
                    two_row_image_type - 2-row image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        platform = arg["platform"]
        upload_type = arg["upload_type"]
        two_row_image_name = arg["two_row_image_name"]
        two_row_image_type = arg["two_row_image_type"]

        ##Upload 2-row image
        xpath = Util.GetXpath({"locate": platform})
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate": xpath, "element": "two-row-background-image", "element_type": "id", "project": "ActivityBanner", "action": upload_type, "file_name": two_row_image_name, "file_type": two_row_image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomepageComponentsPage.ModifyTwoRowImageFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyOneRowImageFET(arg):
        '''
        ModifyOneRowImageFET : Do image FET on one-row image
                Input argu :
                    upload_type - image / file
                    one_row_image_name - 1-row image name
                    one_row_image_type - 1-row image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        one_row_image_name = arg["one_row_image_name"]
        one_row_image_type = arg["one_row_image_type"]

        ##Upload 1-row image
        xpath = Util.GetXpath({"locate":"one_row_image_input"})
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate": xpath, "element": "one-row-background-image", "element_type": "id", "project": "ActivityBanner", "action": upload_type, "file_name": one_row_image_name, "file_type": one_row_image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomepageComponentsPage.ModifyOneRowImageFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadComponentImage(arg):
        '''
        UploadComponentImage : Upload homepage component image with error popup handling
                Input argu :
                    locate - image xpath
                    element - element content
                    element_type - element type (class / name / id)
                    project - frome which project the file from
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg["locate"]
        element = arg["element"]
        element_type = arg["element_type"]
        project = arg["project"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Handle popup error for 5 times
        for try_time in range(5):
            ##Upload visual image with input and handle error image upload exception
            BaseUILogic.UploadFileWithPath({"method": "xpath", "locate": locate, "element": element, "element_type": element_type, "project": project, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})
            dumplogger.info("Try upload image for %d time." % (try_time))

            ##If there's popup window showing up, close it and re-upload image
            if BaseUILogic.DetectPopupWindow():
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
                ret = 0
                time.sleep(10)
            else:
                dumplogger.info("No upload error image.")
                ret = 1
                break

        OK(ret, int(arg['result']), 'AdminHomepageComponentsPage.UploadComponentImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeOrderButton(arg):
        '''
        ClickChangeOrderButton : Click change order button of homepage component
                Input argu :
                    component_type - component type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]

        ##Click change component order btn
        xpath = Util.GetXpath({"locate":component_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click change order btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomepageComponentsPage.ClickChangeOrderButton ->' + component_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveOrderButton(arg):
        '''
        ClickSaveOrderButton : Click change order save button of homepage component
                Input argu :
                    component_type - component type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]

        ##Click save component order btn
        xpath = Util.GetXpath({"locate":component_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save order btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHomepageComponentsPage.ClickSaveOrderButton ->' + component_type)


class AdminPageTemplatePage:

    def __IsForTest__(self):
        pass

class AdminNewUserZonePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateNewUserZoneComponent(arg):
        '''
        CreateNewUserZoneComponent : Create new user zone component
                Input argu :
                    component_type - component type (top_visual / welcome_items / banner)
                    title - new user zone title
                    visibility_type - visible / invisible
                    start_time - component start time
                    end_time - component end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        title = arg["title"]
        visibility_type = arg["visibility_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Click create component
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create component", "result": "1"})
        time.sleep(5)

        ##Choose component type
        if component_type:
            ##Click component type dropdown
            xpath = Util.GetXpath({"locate": "component_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click component type dropdown", "result": "1"})
            time.sleep(1)

            ##Click choose component type
            xpath = Util.GetXpath({"locate": component_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose component type -> " + component_type, "result": "1"})
            time.sleep(5)

        ##Input title
        if title:
            xpath = Util.GetXpath({"locate":"title_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":title, "result": "1"})

        ##Choose visibility type
        if visibility_type:
            ##Click visibility type dropdown
            xpath = Util.GetXpath({"locate": "visibility_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visibility type dropdown", "result": "1"})
            time.sleep(1)

            ##Click choose visibility type
            xpath = Util.GetXpath({"locate": visibility_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility type -> " + visibility_type, "result": "1"})
            time.sleep(5)

        ##Adjust start time and end time
        ##Input start time
        if start_time:
            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)

            ##Input promotion start date
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "display_start_time"}), "string": time_stamp_start, "result": "1"})

        ##Input end time
        if end_time:
            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)

            ##Input promotion end date
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "display_end_time"}), "string": time_stamp_end, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.CreateNewUserZoneComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeNewUserComponentOrder(arg):
        '''
        ChangeNewUserComponentOrder : Change new user component order to top
                Input argu :
                    component_type - component type
                    component_name - component name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        component_name = arg["component_name"]

        ##Click change display order
        xpath = Util.GetXpath({"locate": component_type + "_change_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change display order", "result": "1"})

        ##Get order length
        xpath = Util.GetXpath({"locate": component_type + "_row"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
        order_count = GlobalAdapter.GeneralE2EVar._Count_

        ##Move last order to first
        for orders in range(order_count, 1, -1):
            ##Get last element
            xpath = Util.GetXpath({"locate": component_type + "_order"}).replace('name_to_be_replaced', component_name)
            ##Get target element(former order, but need move crossing former order so -2)
            xpath_target = Util.GetXpath({"locate": component_type + "_row_with_order"}).replace('order_to_be_replaced', str(orders - 1))
            ##Drag element
            BaseUICore.DragAndDrop({"drag_elem": xpath, "target_elem": xpath_target, "result": "1"})

            ##Move to current order element
            xpath = Util.GetXpath({"locate": component_type + "_order"}).replace('name_to_be_replaced', component_name)
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
            time.sleep(2)

        ##Click save
        xpath = Util.GetXpath({"locate": component_type + "_save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to save order", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.ChangeNewUserComponentOrder')


class AdminCampaignSitePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchMicrosite(arg):
        '''
        SearchMicrosite : Search microsite
                Input argu :
                    name - microsite name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "microsite_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Search microsite
        xpath = Util.GetXpath({"locate": "input_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        xpath = Util.GetXpath({"locate": "search_result"}).replace("microsite_name_to_be_replace", name)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignSitePage.SearchMicrosite')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditMicrositeButton(arg):
        '''
        ClickEditMicrositeButton : Edit microsite basic info or view or copy
                Input argu :
                    type - info, view, copy
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click microsite edit btn
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignSitePage.ClickEditMicrositeButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchMicrositeByPageId(arg):
        '''
        SearchMicrositeByPageId : Search microsite by Page Id
                Input argu :
                    id - microsite id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        id = arg["id"]

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "microsite_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Search microsite by id
        xpath = Util.GetXpath({"locate": "input_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": id, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignSitePage.SearchMicrositeByPageId')


class AdminCampaignMicrositeSettingsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteAllComponent(arg):
        '''
        DeleteAllComponent : Delete all component
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get delete btns
        xpath = Util.GetXpath({"locate":"delete_btn"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"multi", "result": "1"})
        if len(GlobalAdapter.CommonVar._PageElements_) != 0:
            for click_times in range(len(GlobalAdapter.CommonVar._PageElements_)):
                time.sleep(15)
                xpath = Util.GetXpath({"locate":"delete_btn"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete btn", "result": "1"})
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
                dumplogger.info("Click delete button %d times" % (click_times))
        else:
            dumplogger.info("There is no component to delete.")

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.DeleteAllComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabularMenuToAddComponent(arg):
        '''
        ClickTabularMenuToAddComponent : Click add new component on the left side component list
                Input argu :
                    tab - tab
                    component - component name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]
        component = arg["component"]

        ##Click component list tab
        xpath = Util.GetXpath({"locate":tab})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click component list tab -> %s" % tab, "result": "1"})

        ##Add component to layout
        xpath = Util.GetXpath({"locate":component})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Add component %s to layout" % component, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickTabularMenuToAddComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAnchorInfo(arg):
        '''
        InputAnchorInfo : Input anchor info
                Input argu :
                    component_type - component type
                    anchor_title - anchor title
                    anchor_id - anchor id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg['component_type']
        anchor_title = arg["anchor_title"]
        anchor_id = arg["anchor_id"]

        ##Click anchor checkbox if anchor checkbox is not enable
        xpath = Util.GetXpath({"locate": "ancher_inputbox_is_disable"})
        xpath = xpath.replace('replaced_text', component_type)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click anchor checkbox
            xpath = Util.GetXpath({"locate":"anchor_checkbox"})
            xpath = xpath.replace('replaced_text', component_type)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to enable anchor", "result": "1"})

        ##Input anchor info
        xpath = Util.GetXpath({"locate":"anchor_title"})
        xpath = xpath.replace('replaced_text', component_type)

        ##Anchor title and anchor id are required field, it will show in same time
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Input anchor title
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":anchor_title, "result": "1"})

            ##Input anchor id
            xpath = Util.GetXpath({"locate":"anchor_id"})
            xpath = xpath.replace('replaced_text', component_type)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":anchor_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputAnchorInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputName(arg):
        '''
        InputName : Input name
                Input argu :
                    component_name - component name
                    string - string you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg['component_name']
        string = arg['string']

        ##Input name
        xpath = Util.GetXpath({"locate":"name"})
        xpath = xpath.replace('replaced_text', component_name)
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":string, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageRatio(arg):
        '''
        InputImageRatio : Input image ratio
                Input argu :
                    image_ratio - image ratio
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_ratio = arg['image_ratio']

        ##Input image ratio
        xpath = Util.GetXpath({"locate":"image_ratio_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":image_ratio, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputImageRatio')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOneImageInfo(arg):
        '''
        InputOneImageInfo : Input 1 image info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "MicroSite" + slash + "image" + slash + "share_image.jpg"

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"1 Image", "string": "1 Image", "result": "1"})

        ##upload image
        xpath = Util.GetXpath({"locate":"input_image"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":file_path, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputOneImageInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputThreeProductInfo(arg):
        '''
        InputThreeProductInfo : Input 3 product info
                Input argu :
                    shopid - shop id
                    itemid_1 - item id
                    itemid_2 - item id
                    itemid_3 - item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid_1 = arg["itemid_1"]
        itemid_2 = arg["itemid_2"]
        itemid_3 = arg["itemid_3"]

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"3 Products", "anchor_title":"3 Products", "anchor_id":"3 Products", "result": "1"})

        ##backgorund color
        xpath = Util.GetXpath({"locate":"background_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[4].value = '#666666'", "result": "1"})

        ##Click customised style radio btn
        xpath = Util.GetXpath({"locate":"customized"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click customised style", "result": "1"})

        ##Price Color
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__item_price__color')[4].value = '#341f85'", "result": "1"})

        ##Click red text cart icon
        xpath = Util.GetXpath({"locate":"red_text_cart_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click red text cart", "result": "1"})

        ##Input Name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"3 Products", "string": "3 Products", "result": "1"})

        ##Input shopid 1 item id 1
        xpath = Util.GetXpath({"locate":"shopid1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": shopid, "result": "1"})
        xpath = Util.GetXpath({"locate":"itemid1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": itemid_1, "result": "1"})

        ##Input shopid 2 item id 2
        xpath = Util.GetXpath({"locate":"shopid2"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": shopid, "result": "1"})
        xpath = Util.GetXpath({"locate":"itemid2"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": itemid_2, "result": "1"})

        ##Input shopid 3 item id 3
        xpath = Util.GetXpath({"locate":"shopid3"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": shopid, "result": "1"})
        xpath = Util.GetXpath({"locate":"itemid3"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": itemid_3, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputThreeProductInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductGridInfo(arg):
        '''
        InputProductGridInfo : Input product grid info
                Input argu :
                    shopid - shop id
                    itemid - item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]

        ##Input shop id
        xpath = Util.GetXpath({"locate":"shopid_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": shopid, "result":"1"})

        ##Input item id
        xpath = Util.GetXpath({"locate":"itemid_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": itemid, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputProductGridInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductGridDisplayCustomised(arg):
        '''
        ChooseProductGridDisplayCustomised : Choose product grid display customised
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click customised
        xpath = Util.GetXpath({"locate":"customised_radio"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click customised radio", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ChooseProductGridDisplayCustomised')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductGridAddNewProduct(arg):
        '''
        ClickProductGridAddNewProduct : Click product grid add new product
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new product
        xpath = Util.GetXpath({"locate":"add_new_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new btn", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickProductGridAddNewProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductGridCsv(arg):
        '''
        UploadProductGridCsv : Upload product grid csv
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "MicroSite" + slash + "file" + slash + file_name + "." + file_type

        BaseUICore.ExecuteScript({"script":"document.getElementById('product-grid-csv-file-upload').removeAttribute('style', 'display:block')", "result":"1"})

        ##Upload file
        xpath = Util.GetXpath({"locate":"file_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": file_path, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.UploadProductGridCsv')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderInfo(arg):
        '''
        InputHeaderInfo : Input header info
                Input argu :
                    title - anchor title
                    name - header name
                    content - header text content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        name = arg["name"]
        content = arg["content"]

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type": "Header", "anchor_title":title, "anchor_id":title, "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name": "Header", "string":name, "result": "1"})

        ##Input header text content
        xpath = Util.GetXpath({"locate":"header_content"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputHeaderInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderInfoWithImage(arg):
        '''
        InputHeaderInfoWithImage : Input header info with image
                Input argu :
                    title - anchor title
                    name - header name
                    content - header content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        name = arg["name"]
        content = arg["content"]

        slash = Config.dict_systemslash[Config._platform_]

        ##Click make this component an anchor checkbox
        xpath = Util.GetXpath({"locate":"anchor_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to enable anchor", "result": "1"})

        ##Input anchor title
        xpath = Util.GetXpath({"locate":"anchor_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":title, "result": "1"})

        ##Input anchor id
        xpath = Util.GetXpath({"locate":"anchor_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":title, "result": "1"})

        ##Input name
        xpath = Util.GetXpath({"locate":"name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        ##Input header content
        xpath = Util.GetXpath({"locate":"header_content"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": content, "result": "1"})

        ##Upload anchor image
        xpath = Util.GetXpath({"locate":"get_anchor_image"})
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "MicroSite" + slash + "image" + slash + title + ".jpg"
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":file_path, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputHeaderInfoWithImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHorizontalScrollShopInfo(arg):
        '''
        InputHorizontalScrollShopInfo : Input horizontal scroll shop info
                Input argu :
                    assets_group_id - assets_group_id
                    description_text - description_text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        assets_group_id = arg["assets_group_id"]
        description_text = arg["description_text"]

        ##Input assets group id
        xpath = Util.GetXpath({"locate":"assets_group_id_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":assets_group_id, "result": "1"})

        ##Input description text
        xpath = Util.GetXpath({"locate":"description_text_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": description_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputHorizontalScrollShopInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHorizontalScrollCollectionInfo(arg):
        '''
        InputHorizontalScrollCollectionInfo : Input Horizontal Scroll Collection settings Info
                Input argu :
                    header_text - header_text
                    number_of_item_display - number_of_item_display
                    product_collection_id - product_collection_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        header_text = arg['header_text']
        number_of_item_display = arg['number_of_item_display']
        product_collection_id = arg['product_collection_id']

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Horizontal Scroll Collection", "anchor_title":"Horizontal Scroll Collection", "anchor_id":"Horizontal Scroll Collection", "result": "1"})

        ##Enable display component header
        xpath = Util.GetXpath({"locate":"header_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Display Component Header checkbox", "result": "1"})
        xpath = Util.GetXpath({"locate":"text_content"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":header_text, "result": "1"})

        ##Enable display see all to the right in header
        xpath = Util.GetXpath({"locate":"see_all_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Display See All to the Right in Header checkbox", "result": "1"})

        ##number of item display
        xpath = Util.GetXpath({"locate":"number_of_item_display"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number_of_item_display, "result": "1"})

        ##background color
        xpath = Util.GetXpath({"locate":"background_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[7].value = '#FFFFFF'", "result": "1"})

        ##Click customized layout
        xpath = Util.GetXpath({"locate":"customized"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})

        ##Click show progress bar checkbox
        xpath = Util.GetXpath({"locate":"show_progress_bar"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click show progress bar", "result": "1"})

        ##Click hide price checkbox
        xpath = Util.GetXpath({"locate":"hide_price"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click hide price", "result": "1"})

        ##Click show original price checkbox
        xpath = Util.GetXpath({"locate":"show_original_price"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click show original price", "result": "1"})

        ##Click orange cart icon
        xpath = Util.GetXpath({"locate":"orange_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click small orange cart", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Horizontal Scroll Collection", "string":"Horizontal Scroll Collection", "result": "1"})

        ##Input collection id
        xpath = Util.GetXpath({"locate":"product_collection_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_collection_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputHorizontalScrollCollectionInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFreeShippingVoucherInfo(arg):
        '''
        InputFreeShippingVoucherInfo : Input free shipping voucher info
                Input argu :
                    component_name - component_name
                    voucher_name - voucher_name
                    promotion_id - promotion_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg['component_name']
        voucher_name = arg['voucher_name']
        promotion_id = arg['promotion_id']

        ##background color
        xpath = Util.GetXpath({"locate":"background_color_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[5].value = '#666666'", "result": "1"})

        ##title color
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__voucher_title__color')[0].value = '#123456'", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__voucher_description__color')[0].value = '#654321'", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__voucher_right__color')[0].value = '#999999'", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__voucher_left__background_color')[0].value = '#948794'", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__voucher_right__background_color')[0].value = '#556678'", "result": "1"})

        ##Click white voucher icon
        xpath = Util.GetXpath({"locate":"voucher_icon_white"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click white voucher icon", "result": "1"})

        ##Click green voucher icon
        xpath = Util.GetXpath({"locate":"voucher_icon_green"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click white voucher icon", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Free Shipping Voucher", "string":component_name, "result": "1"})

        ##Input promotion id
        xpath = Util.GetXpath({"locate":"promotion_id"})
        if promotion_id:
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":promotion_id, "result": "1"})
        else:
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[0]["id"], "result": "1"})

        ##Input voucher name
        xpath = Util.GetXpath({"locate":"voucher_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":voucher_name, "result": "1"})

        ##Input voucher description
        xpath = Util.GetXpath({"locate":"voucher_description"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"twqa auto", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputFreeShippingVoucherInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAspectRatio(arg):
        '''
        InputAspectRatio : Input aspect ratio width and height
                Input argu :
                    aspect_radio_width - aspect radio width
                    aspect_radio_height - aspect radio height
                    component - component type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component = arg['component']
        aspect_radio_width = arg['aspect_radio_width']
        aspect_radio_height = arg['aspect_radio_height']

        ##Input aspect ratio width
        xpath = Util.GetXpath({"locate":"aspect_radio_width"})
        xpath = xpath.replace('replaced_text', component)
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":aspect_radio_width, "result": "1"})

        ##Input aspect ratio height
        xpath = Util.GetXpath({"locate":"aspect_radio_height"})
        xpath = xpath.replace('replaced_text', component)
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":aspect_radio_height, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputAspectRatio')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEmbedCode(arg):
        '''
        InputEmbedCode : Input HTML Embed Code
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Embed code
        xpath = Util.GetXpath({"locate":"html_embed_code"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":'<iframe width="1280" height="720" src="https://www.youtube.com/embed/xRcGOgKpXtQ" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture"></iframe>', "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputEmbedCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopGridInfo(arg):
        '''
        InputShopGridInfo : Input Shop Grid Info
                Input argu :
                    number_of_shops_display - number of shops display
                    number_of_shops_in_a_row - number of shops in a row
                    asset_group_id - asset group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number_of_shops_display = arg["number_of_shops_display"]
        number_of_shops_in_a_row = arg["number_of_shops_in_a_row"]
        asset_group_id = arg["asset_group_id"]

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Shop Grid", "anchor_title":"Shop Grid", "anchor_id":"Shop Grid","result": "1"})

        ##Enable background color
        xpath = Util.GetXpath({"locate":"background_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[15].value = '#666666'", "result": "1"})

        ##number of shops to display
        xpath = Util.GetXpath({"locate":"number_of_shops_to_display"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number_of_shops_display, "result": "1"})

        ##number of shops in a row
        if number_of_shops_in_a_row:
            xpath = Util.GetXpath({"locate":"number_of_shops_in_a_row"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number_of_shops_in_a_row, "result": "1"})

        ##Display shop name on all shop cards
        xpath = Util.GetXpath({"locate":"display_shopname_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click display shop name check box", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Shop Grid", "string":"Shop Grid", "result": "1"})

        ##Input shop assets group id
        xpath = Util.GetXpath({"locate":"asset_group_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":asset_group_id, "result": "1"})

        ##Input shop description
        xpath = Util.GetXpath({"locate":"shop_description"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"TWQA Shop Grid", "result": "1"})

        ##click select shop to display btn
        xpath = Util.GetXpath({"locate":"select_shops_to_display_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click select shop to display btn", "result": "1"})

        ##input shop id and click select btn
        for number in range(int(number_of_shops_display)):
            shopid = arg["shopid_" + str(number+1)]

            xpath = Util.GetXpath({"locate":"input_shop_id"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":shopid, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            time.sleep(5)

            xpath = Util.GetXpath({"locate":"select_btn"})
            xpath = xpath.replace('replaced_text', shopid)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click select btn", "result": "1"})
            time.sleep(5)

        ##click done btn
        xpath = Util.GetXpath({"locate":"done_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click done btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputShopGridInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseTopProductsType(arg):
        '''
        ChooseTopProductsType : Choose top products type
                Input argu :
                    top_product_type - manual/rcmd
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        top_product_type = arg["type"]

        ##Click recommendation
        xpath = Util.GetXpath({"locate":top_product_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click top product type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ChooseTopProductsType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddTopProductsManualSetRow(arg):
        '''
        AddTopProductsManualSetRow : Add top products manual set row
                Input argu :
                    click_times - click times
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_times = arg["click_times"]

        for click in click_times:
            ##Click add row
            xpath = Util.GetXpath({"locate":"add_row_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add row", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.AddTopProductsManualSetRow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTopProductsInfo(arg):
        '''
        InputTopProductsInfo : Input top products info
                Input argu :
                    order - input order
                    category_id - category_id
                    name - name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]
        category_id = arg["category_id"]
        name = arg["name"]

        ##Input category id and name data
        xpath = Util.GetXpath({"locate":"category_id"}).replace("order_to_be_replace", order)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":category_id, "result": "1"})

        xpath = Util.GetXpath({"locate":"name"}).replace("order_to_be_replace", order)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputTopProductsInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditTopProductsInfo(arg):
        '''
        EditTopProductsInfo : Edit top products info
                Input argu :
                    top_product_type - manual/rcmd
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_id1 = arg['category_id1']
        category_id2 = arg['category_id2']
        category_id3 = arg['category_id3']
        name1 = arg['name1']
        name2 = arg['name2']
        name3 = arg['name3']

        ##Click manual setup
        xpath = Util.GetXpath({"locate":"manual_setup"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click manual setup", "result": "1"})

        ##Input category id 1 data
        xpath = Util.GetXpath({"locate":"category_id1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":category_id1, "result": "1"})

        xpath = Util.GetXpath({"locate":"name1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name1, "result": "1"})

        ##Input category id 2 data
        xpath = Util.GetXpath({"locate":"category_id2"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":category_id2, "result": "1"})

        xpath = Util.GetXpath({"locate":"name2"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name2, "result": "1"})

        ##Input category id 3 data
        xpath = Util.GetXpath({"locate":"category_id3"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":category_id3, "result": "1"})

        xpath = Util.GetXpath({"locate":"name3"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name3, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputTopProductsInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBannerCarouselInfo(arg):
        '''
        InputBannerCarouselInfo : Input banner carousel info
                Input argu :
                    group_id - group_id
                    display_format - carousel / horizontal_scroll_1.5 / horizontal_scroll_2.5
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_id = arg['group_id']
        display_format = arg['display_format']

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Banner", "anchor_title":"Banner", "anchor_id":"Banner", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Banner", "string":"Banner", "result": "1"})

        ##Input group id
        xpath = Util.GetXpath({"locate":"group_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":group_id, "result": "1"})

        ##Select display format
        xpath = Util.GetXpath({"locate": display_format})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select carousel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputBannerCarouselInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBackToTopInfo(arg):
        '''
        InputBackToTopInfo : Input back to top info
                Input argu :
                    margin_bottom - margin_bottom
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        margin_bottom = arg['margin_bottom']

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Back To Top", "string": "Back To Top", "result": "1"})

        ##Input bottom
        xpath = Util.GetXpath({"locate":"margin_bottom"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":margin_bottom, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputBackToTopInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGapInfo(arg):
        '''
        InputGapInfo : Input Gap info
                Input argu :
                    gap_height - gap_height
                    platform_hide - platform_hide
                    user_scope - user_scope
                    bundle_name - bundle_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        gap_height = arg["gap_height"]
        platform_hide = arg["platform_hide"]
        user_scope = arg["user_scope"]
        bundle_name = arg["bundle_name"]

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Gap", "string":"Gap", "result": "1"})

        ##Select gap color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__color')[1].value = '#048cb5'", "result": "1"})

        ##Choose gap height
        xpath = Util.GetXpath({"locate":"gap_height"}).replace('height_to_be_replace', gap_height)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose gap height", "result": "1"})

        ##Select platform hide
        if platform_hide:
            xpath = Util.GetXpath({"locate": platform_hide})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle name select", "result": "1"})
        else:
            dumplogger.info("Don't need to click platform hide.")

        ##Select bundle name
        if bundle_name:
            xpath = Util.GetXpath({"locate": "bundle_name_dropdown_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle name select", "result": "1"})
            time.sleep(10)
            xpath = Util.GetXpath({"locate": "select_bundle_name"})
            xpath_replace = xpath.replace('value_to_be_replace', bundle_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath_replace, "message": "select bundle name", "result": "1"})
        else:
            dumplogger.info("Don't need to select bundle.")

        ##Select user scope
        if user_scope:
            xpath = Util.GetXpath({"locate": "user_scope_dropdown_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope select", "result": "1"})
            time.sleep(10)
            xpath = Util.GetXpath({"locate": "select_user_scope"})
            xpath_replace = xpath.replace('value_to_be_replace', user_scope)
            BaseUICore.Click({"method": "xpath", "locate": xpath_replace, "message": "select user scope", "result": "1"})
        else:
            dumplogger.info("Don't need to select user scope.")

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputGapInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAnchorTabBarInfo(arg):
        '''
        InputAnchorTabBarInfo : Input anchor tab bar info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Anchor Tab Bar", "string":"Anchor Tab Bar", "result": "1"})

        ##Input color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__tab_bar__background_color')[0].value='#357891'", "result": "1"})
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__tab_bar__color')[0].value='#ffffff'", "result": "1"})
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__tab_bar_highlight__color')[0].value='#357891'", "result": "1"})
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__tab_bar_highlight__background_color')[0].value='#ffffff'", "result": "1"})
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__dropdown_menu_highlight__color')[0].value='#ff006a'", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputAnchorTabBarInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAnchorTabBarImageInfo(arg):
        '''
        InputAnchorTabBarImageInfo : Input anchor tab bar image info
                Input argu :
                    image_ratio - image_ratio
                    shadow - light, dark
                    use_icon_grid - enable, disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_ratio = arg["image_ratio"]
        shadow = arg["shadow"]
        use_icon_grid = arg["use_icon_grid"]

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "MicroSite" + slash + "image" + slash + "share_image.jpg"

        ##Select "Image Tab Bar"
        xpath = Util.GetXpath({"locate":"image_tab_bar_radio"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select Image Tab Bar", "result": "1"})
        dumplogger.info("Success select Image Tab Bar.")

        ##Input tab image ratio
        AdminCampaignMicrositeSettingsPage.InputImageRatio({"image_ratio":image_ratio, "result": "1"})
        dumplogger.info("Success Input image ratio -> " + image_ratio)

        ##Select "Shadow"
        xpath = Util.GetXpath({"locate":"shadow_" + shadow})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select shadow -> " + shadow, "result": "1"})
        dumplogger.info("Success select shadow -> " + shadow)

        ##Select "Use Icon Grid" toggle, if enable
        if use_icon_grid == "enable":
            xpath = Util.GetXpath({"locate":"icon_grid_checkbox"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select use icon grid toggle", "result": "1"})
            dumplogger.info("Success select use icon grid.")

            ##Upload image
            xpath = Util.GetXpath({"locate":"input_image"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":file_path, "result": "1"})
            dumplogger.info("Success upload image.")
        else:
            dumplogger.info("Not use icon grid.")

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputAnchorTabBarImageInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCollectionGridInfo(arg):
        '''
        InputCollectionGridInfo : Input collection grid info
                Input argu :
                    header_text - header_text
                    number_of_item_display - number_of_item_display
                    product_collection_id - product_collection_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ret = 1
        header_text = arg['header_text']
        number_of_item_display = arg['number_of_item_display']
        product_collection_id = arg['product_collection_id']

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Collection Grid With Custom Tabs","anchor_title":"Collection Grid With Custom Tabs", "anchor_id":"Collection Grid With Custom Tabs", "result": "1"})

        ##Enable display component header
        xpath = Util.GetXpath({"locate":"header_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Display Component Header checkbox", "result": "1"})
        xpath = Util.GetXpath({"locate":"text_content"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":header_text, "result": "1"})
        ##Header color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__header__caption__text_color')[0].value = '#ffffff'", "result": "1"})

        ##Click customized layout
        xpath = Util.GetXpath({"locate":"customized"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})

        ##Enable display see all in header
        xpath = Util.GetXpath({"locate":"see_all_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Display see all in Header checkbox", "result": "1"})
        xpath = Util.GetXpath({"locate":"redirect_url"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"https://google.com", "result": "1"})

        ##number of item display
        xpath = Util.GetXpath({"locate":"number_of_item_per_tab"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number_of_item_display, "result": "1"})
        xpath = Util.GetXpath({"locate":"number_of_item_per_row"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number_of_item_display, "result": "1"})

        ##background color
        xpath = Util.GetXpath({"locate":"background_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})
        BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[0].value = '#000000'", "result": "1"})

        ##Price color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__item_price__color')[0].value = '#34ebe5'", "result": "1"})

        ##Click hide price checkbox
        xpath = Util.GetXpath({"locate":"hide_price"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click hide price", "result": "1"})

        ##Click original price checkbox
        xpath = Util.GetXpath({"locate":"show_original_price"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click show original price", "result": "1"})

        ##Click orange cart icon
        xpath = Util.GetXpath({"locate":"red_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click small orange cart", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Collection Grid With Custom Tabs", "string":"Collection Grid With Custom Tabs", "result": "1"})

        ##Input collection id
        xpath = Util.GetXpath({"locate":"product_collection_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_collection_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputCollectionGridInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCountdownTimerInfo(arg):
        '''
        InputCountdownTimerInfo : input countdown timer info
                Input argu :
                    background - background
                    padding_pixel_top - padding pixel top
                    padding pixel bottom - padding pixel bottom
                    timer_size - small, medium, large
                    time_number_background_color - time number background color
                    text_color - text color
                    text_before_timer - text before timer
                    millisecond_toggle - millisecond toggle
                    time_unit_toggle - time unit toggle
                    end_time_minu - end time minu
                    end_time - end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        background = arg["background"]
        padding_pixel_top = arg["padding_pixel_top"]
        padding_pixel_bottom = arg["padding_pixel_bottom"]
        timer_size = arg['timer_size']
        time_number_background_color = arg['time_number_background_color']
        time_number_color = arg['time_number_color']
        text_color = arg["text_color"]
        text_before_timer = arg['text_before_timer']
        millisecond_toggle = arg['millisecond_toggle']
        time_unit_toggle = arg['time_unit_toggle']
        end_time_minu = arg['end_time_minu']
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time_minu), 0)

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "Microsite" + slash + "image" + slash + "countdown_timer.jpg"

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Countdown Timer", "anchor_title":"Countdown Timer", "anchor_id":"Countdown Timer","result": "1"})

        if background == "image":
            ##Using background image
            xpath = Util.GetXpath({"locate":"background_image"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":file_path, "result": "1"})

        elif not background:
            ##background transparent
            xpath = Util.GetXpath({"locate":"background_transparent_radio"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click background transparent radio", "result": "1"})

        else:
            ##Using background color
            ##Click color radio
            xpath = Util.GetXpath({"locate":"background_color_radio"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click background color radio", "result": "1"})
            BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[11].value = '#" + background + "'", "result": "1"})

        ##Select timer size
        xpath = Util.GetXpath({"locate":"timer_" + timer_size})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click timer size -> " + timer_size, "result": "1"})

        ##Only image background can setting customized position
        if background == "image":
            ##Select Timer Position on the background
            xpath = Util.GetXpath({"locate":"timer_position_customized"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click customized timer position", "result": "1"})

            xpath = Util.GetXpath({"locate":"position_right"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"10", "result": "1"})

            xpath = Util.GetXpath({"locate":"position_top"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"10", "result": "1"})
        ##If not using image, setting padding pixel
        else:
            ##Setting padding pixel
            xpath = Util.GetXpath({"locate":"padding_pixel_top"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":padding_pixel_top, "result": "1"})

            xpath = Util.GetXpath({"locate":"padding_pixel_bottom"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":padding_pixel_bottom, "result": "1"})

        ##Change time number background color
        if time_number_background_color:
            ##Click color radio
            xpath = Util.GetXpath({"locate":"time_number_background_color_radio"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click time number background color radio", "result": "1"})

            BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__number__background_color')[0].value = '#" + time_number_background_color + "'", "result": "1"})
        else:
            dumplogger.info("Time number background color using transparent background")

        ##Change time number color
        if time_number_color:
            BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__number__color')[0].value = '#" + time_number_color + "'", "result": "1"})
        else:
            dumplogger.info("Time number color using transparent background")

        ##Input text before timer
        xpath = Util.GetXpath({"locate":"text_before_timer"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":text_before_timer, "result": "1"})

        if millisecond_toggle == "on":
            ##Toggle on millisecond
            xpath = Util.GetXpath({"locate":"millisecond_on"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click millisecond toggle to on", "result": "1"})
        else:
            dumplogger.info("Don't need to click millisecond toggle to on.")

        if time_unit_toggle == "on":
            ##Toggle on time unit
            xpath = Util.GetXpath({"locate":"time_unit_on"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click time unit toggle to on", "result": "1"})
        else:
            dumplogger.info("Don't need to click time unit toggle to on.")

        ##Change text color
        if text_color:
            BaseUICore.ExecuteScript({"script": "document.getElementsByName('children__0__style__text__color')[0].value = '#" + text_color + "'", "result": "1"})
        else:
            dumplogger.info("Text color using transparent background")

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Countdown Timer", "string":"Countdown Timer", "result": "1"})

        ##Input countdown end time
        xpath = Util.GetXpath({"locate":"countdown_end_time"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputCountdownTimerInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemRcmdInfo(arg):
        '''
        InputItemRcmdInfo : Input item rcmd info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_label = arg["product_label"]

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Item Recommendation", "anchor_title":"Item Recommendation", "anchor_id":"Item Recommendation", "result": "1"})

        ##Input number of items to display
        xpath = Util.GetXpath({"locate":"item_to_display"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"6", "result": "1"})

        ##Click customized layout
        xpath = Util.GetXpath({"locate":"customized"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})

        ##Set price color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__item_price__color')[6].value='#000000'", "result": "1"})

        ##Click hide price
        xpath = Util.GetXpath({"locate":"hide_price_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click hide price with ???", "result": "1"})

        ##Click show original price
        xpath = Util.GetXpath({"locate":"original_price_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click show original price", "result": "1"})

        ##Click orange text add to cart
        xpath = Util.GetXpath({"locate":"add_to_cart_orange"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click orange text add to cart image", "result": "1"})

        ##Click display product tags
        xpath = Util.GetXpath({"locate":"display_product_tag"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click display tag", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Item Recommendation", "string":"Item Recommendation", "result": "1"})

        ##Select product label
        xpath = Util.GetXpath({"locate":"product_label"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_label, "result": "1"})
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"product_label_value"}).replace("label_to_be_replace", product_label)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product label value", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputItemRcmdInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopRcmdInfo(arg):
        '''
        InputShopRcmdInfo : Input shop rcmd info
                Input argu :
                    asset_group_id - asset group id
                    product_label - product label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        asset_group_id = arg["asset_group_id"]
        product_label = arg["product_label"]

        ##Input anchor info
        AdminCampaignMicrositeSettingsPage.InputAnchorInfo({"component_type":"Shop Recommendation","anchor_title":"Shop Recommendation", "anchor_id":"Shop Recommendation", "result": "1"})

        ##Input number of shops to display
        xpath = Util.GetXpath({"locate":"shop_to_display"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"6", "result": "1"})

        ##Set price color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__shop_card__color')[0].value='#555666'", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Shop Recommendation", "string":"Shop Recommendation", "result": "1"})

        ##Select product label
        xpath = Util.GetXpath({"locate":"product_label"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_label, "result": "1"})
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"product_label_value"}).replace("label_to_be_replace", product_label)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product label value", "result": "1"})

        ##Input shop assets group id
        xpath = Util.GetXpath({"locate":"asset_group_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":asset_group_id, "result": "1"})

        ##Input shop default description
        xpath = Util.GetXpath({"locate":"shop_description"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"TWQA Microsite rcmd", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputShopRcmdInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherGridInfo(arg):
        '''
        InputVoucherGridInfo : Input voucher grid info
                Input argu :
                    component_name - Microsite voucher grid component name
                    vouchers_per_row - Number of vouchers per row
                    vouchers_display_number - Number of vouchers to display
                    ui_type - Component ui type
                    number_of_vouchers - Number of vouchers add in compoment
                    voucher_type - shopee_voucher / seller_voucher / free_shipping_voucher
                    promotion_ids - Voucher promotion ids add in compoment
                    shop_assets_group_id - Compoment shop assets group id
                Retrun code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg['component_name']
        vouchers_per_row = arg['vouchers_per_row']
        vouchers_display_number = arg['vouchers_display_number']
        ui_type = arg['ui_type']
        number_of_vouchers = arg['number_of_vouchers']
        voucher_type = arg['voucher_type']
        promotion_ids = arg['promotion_ids']
        shop_assets_group_id = arg['shop_assets_group_id']

        ##Background color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('style__background_color')[12].value = '#876542'", "result": "1"})

        ##Input number of vouchers per row
        xpath = Util.GetXpath({"locate":"vouchers_per_row"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":vouchers_per_row, "result": "1"})

        ##Input number of vouchers to display
        xpath = Util.GetXpath({"locate":"vouchers_display_number"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":vouchers_display_number, "result": "1"})

        ##Click ui type
        xpath = Util.GetXpath({"locate":"ui_type" + ui_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ui type" + ui_type, "result": "1"})

        ##voucher left Background color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__left_background_color')[0].value = '#ffeeff'", "result": "1"})

        ##voucher left Background color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__right_background_color')[0].value = '#ffeeff'", "result": "1"})

        ##voucher Bold description text color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__bold_description_color')[0].value = '#002255'", "result": "1"})

        ##voucher Light description text color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__light_description_color')[0].value = '#152555'", "result": "1"})

        ##voucher name text color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__logo_name_color')[0].value = '#382c00'", "result": "1"})

        ##voucher round Background color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__logo_background_color')[0].value = '#fae38e'", "result": "1"})

        ##voucher claim button color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__claim__button_color')[0].value = '#fae38e'", "result": "1"})

        ##voucher claim text color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__claim__button_text')[0].value = '#bfbfbf'", "result": "1"})

        ##Click Claimed and fully redeemed button color
        xpath = Util.GetXpath({"locate":"dark"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dark color", "result": "1"})
        xpath = Util.GetXpath({"locate":"light"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click light color", "result": "1"})

        ##Click guide user to shop for seller voucher
        xpath = Util.GetXpath({"locate":"guide_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click guide user to shop checkbox", "result": "1"})
        time.sleep(2)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click guide user to shop checkbox", "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Voucher Grid", "string":component_name, "result": "1"})

        ##Input shop assets group id
        xpath = Util.GetXpath({"locate":"shop_assets_group_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":shop_assets_group_id, "result": "1"})

        ##Select voucher displayed order
        xpath = Util.GetXpath({"locate":"customised_voucher_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click customised voucher button", "result": "1"})

        ##Prepare promotion ids
        if promotion_ids:
            ##Split promotion ids string into promotion_ids
            promotion_ids = promotion_ids.split(',')
        elif voucher_type == "shopee_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_
        elif voucher_type == "seller_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._SellerVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._SellerVoucherList_
        elif voucher_type == "free_shipping_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
        else:
            dumplogger.error("Voucher type string is not correct, please use shopee_voucher / seller_voucher / free_shipping_voucher string!!!")

        ##Log all promotion ids
        dumplogger.info("All promotion ids => %s" % (promotion_ids))

        ##Click add new voucher btn
        for click_times in range(int(number_of_vouchers)):
            xpath = Util.GetXpath({"locate":"add_new_voucher"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new voucher btn", "result": "1"})
            dumplogger.info("Click add new voucher times: %d" % (click_times))
            time.sleep(2)

            ##Input promotion id
            xpath = Util.GetXpath({"locate":"promotion_id"})
            xpath_promotion_id_field = xpath.replace("times_to_be_replace", str(click_times))
            BaseUICore.Input({"method":"xpath", "locate":xpath_promotion_id_field, "string":promotion_ids[click_times]["id"], "result": "1"})

            ##Input redirect url
            xpath = Util.GetXpath({"locate":"redirect_url"})
            xpath_redirect_url_field = xpath.replace("times_to_be_replace", str(click_times))
            BaseUICore.Input({"method":"xpath", "locate":xpath_redirect_url_field, "string":"https://google.com", "result": "1"})

            ##Input ribbon text
            xpath = Util.GetXpath({"locate":"ribbon_text"})
            xpath_ribbon_text_field = xpath.replace("times_to_be_replace", str(click_times))
            BaseUICore.Input({"method":"xpath", "locate":xpath_ribbon_text_field, "string":"Voucher Diaplay", "result": "1"})

            ##Input display name
            xpath = Util.GetXpath({"locate":"display_name"})
            xpath_display_name_field = xpath.replace("times_to_be_replace", str(click_times))
            BaseUICore.Input({"method":"xpath", "locate":xpath_display_name_field, "string":"AUTO", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputVoucherGridInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTextComponentInfo(arg):
        '''
        InputTextComponentInfo : Input text component info
                Input argu :
                    text - text
                    padding_top - padding_top
                    padding_bottom - padding_bottom
                    padding_left - padding_left
                    padding_right - padding_right
                    background_color - background_color
                    text_color - text_color
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg['text']
        padding_top = arg['padding_top']
        padding_bottom = arg['padding_bottom']
        padding_left = arg['padding_left']
        padding_right = arg['padding_right']
        background_color = arg['background_color']
        text_color = arg['text_color']

        #Enable background color
        if background_color:
            xpath = Util.GetXpath({"locate":"background_checkbox"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bg color check box", "result": "1"})

            if Config._TestCaseRegion_ == "ID":
                BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[6].value = '#" + background_color + "'", "result": "1"})
            else:
                BaseUICore.ExecuteScript({"script": "document.getElementsByName('style__background_color')[17].value = '#" + background_color + "'", "result": "1"})

        else:
            dumplogger.info("Background need to be transparent.")

        ##Text color
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('children__0__style__text_color')[0].value = '#" + text_color + "'", "result": "1"})

        ##Input padding top
        xpath = Util.GetXpath({"locate":"padding_top"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":padding_top, "result": "1"})

        ##Input padding bottom
        xpath = Util.GetXpath({"locate":"padding_bottom"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":padding_bottom, "result": "1"})

        ##Input padding left
        xpath = Util.GetXpath({"locate":"padding_left"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":padding_left, "result": "1"})

        ##Input padding right
        xpath = Util.GetXpath({"locate":"padding_right"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":padding_right, "result": "1"})

        ##Input name
        AdminCampaignMicrositeSettingsPage.InputName({"component_name":"Text", "string":"Text", "result": "1"})

        ##Input Text
        xpath = Util.GetXpath({"locate":"input_text"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Input text", "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputTextComponentInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMarkdownForTextComponent(arg):
        '''
        InputMarkdownForTextComponent : Input markdown to text component
                Input argu :
                    text - text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg['text']

        ##Input Text
        xpath = Util.GetXpath({"locate":"input_text"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Input text", "result": "1"})

        for text_char in text:
            if text_char == "#":
                ##Need to release shift after type "#"
                BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": "#", "result": "1"})

            ##Using "\\" in xml to do enter
            elif text_char == "\\":
                BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

            elif text_char == "*":
                ##Need to release shift after type "*"
                BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": "*", "result": "1"})

            else:
                BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": text_char, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputMarkdownForTextComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteComponent(arg):
        '''
        DeleteComponent : Delete component in microsite
                Input argu :
                    component_to_delete - component to delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_to_delete = arg['component_to_delete']

        ##Click delete btn
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace('replaced_text', component_to_delete)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click" + component_to_delete + "delete btn", "result": "1"})
            ##Handle popup
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.DeleteComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentOrder(arg):
        '''
        ModifyComponentOrder : Modify component order
                Input argu :
                    component_to_drag
                    component_to_drop
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_to_drag = arg['component_to_drag']
        component_to_drop = arg['component_to_drop']

        ##Get move component
        drag_xpath = Util.GetXpath({"locate":"move_btn"})
        drag_xpath = drag_xpath.replace('replaced_text', component_to_drag)

        ##Get target component
        drop_xpath = Util.GetXpath({"locate":"move_btn"})
        drop_xpath = drop_xpath.replace('replaced_text', component_to_drop)

        ##Drag component
        BaseUICore.DragAndDrop({"drag_elem":drag_xpath, "target_elem":drop_xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ModifyComponentOrder -> %s to %s' % (component_to_drag, component_to_drop))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadAnchorImage(arg):
        '''
        UploadAnchorImage : upload anchor image
                Input argu :
                    component_name - header, 3products
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg['component_name']

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "MicroSite" + slash + "image" + slash + "anchor_img.png"

        ##Upload file
        xpath = Util.GetXpath({"locate": "anchor_img_input"}).replace("component_name_to_be_replace", component_name)
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":file_path, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.UploadAnchorImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToEditComponent(arg):
        '''
        ClickToEditComponent : Click to edit component
                Input argu :
                    component_name - component_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg['component_name']

        ##Click component
        xpath = Util.GetXpath({"locate":"component_name"}).replace("component_name_to_be_replace", component_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click component", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickToEditComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreview(arg):
        '''
        ClickPreview : Click preview button
                Input argu :
                    platform - pc, mobile
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click preview button
        xpath = Util.GetXpath({"locate":"preview_btn", })
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click preview btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickPreview')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewTab(arg):
        '''
        ClickViewTab : Click  view tab
                Input argu :
                    platform - pc, mobile
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        platform = arg['platform']

        ##Click platform tab
        xpath = Util.GetXpath({"locate":platform})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click" + platform + "View tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickViewTab ->' + platform)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAdd(arg):
        '''
        ClickAdd : Click add new layout btn
                Input argu :
                    button_type - button_type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        button_type = arg["button_type"]

        for retry_times in range(3):
            ##Click add new layout btn
            xpath = Util.GetXpath({"locate":button_type})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new layout btn", "result": "1"})

            ##Detect javascript error title message
            xpath = Util.GetXpath({"locate":"javascript_error_title"})

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                xpath = Util.GetXpath({"locate":"ok_btn"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok btn of javascript error message.", "result": "1"})
                time.sleep(5)

            else:
                ret = 1
                dumplogger.info("Not detect javascript!!!")
                break

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit hotspots button
                Input argu :
                    button_type - button_type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click edit hotspot btn
        xpath = Util.GetXpath({"locate":button_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit hotspots btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickComponent(arg):
        '''
        ClickComponent : Click component to show component config
                Input argu :
                    component_name - microsite component name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]

        ##Click edit hotspot btn
        xpath = Util.GetXpath({"locate":"component_section"})
        xpath_component_section = xpath.replace("component_name_to_be_replace", component_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_component_section, "message":"Click component to show component config", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save btn
        time.sleep(20)
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save btn", "result": "1"})

        time.sleep(10)
        ##Detect handle popup message
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        else:
            dumplogger.info("Not detect popup message!!!")

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        '''
        ClickNext : Click next button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next btn
        xpath = Util.GetXpath({"locate":"next_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click next btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickNext')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToMicrositeList(arg):
        '''
        ClickBackToMicrositeList : Click back to microsite list buttonon top left
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click microsite list btn on top left
        xpath = Util.GetXpath({"locate":"microsite_list_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back to microsite list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickBackToMicrositeList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublish(arg):
        '''
        ClickPublish : Click publish to site btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click publish btn
        xpath = Util.GetXpath({"locate":"publish_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click publish to site btn", "result": "1"})

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickPublish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyComponent(arg):
        '''
        ClickCopyComponent : Copy component
                Input argu :
                    order - coponent_to_copy
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg['order']

        ##Click copy btn
        xpath = Util.GetXpath({"locate":"copy_btn"}).replace('order_to_be_replace', order)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click copy btn", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickCopyComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDone(arg):
        '''
        ClickDone : Click done btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done btn
        xpath = Util.GetXpath({"locate":"done_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click done btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherClaim(arg):
        '''
        ClickVoucherClaim : Click voucher claim
                Input argu :
                    type - free_shipping_voucher / shopee_voucher
                    voucher_name - voucher name
                    index - several same voucher name index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']
        voucher_name = arg['voucher_name']
        index = arg['index']

        ##Click voucher claim
        xpath = Util.GetXpath({"locate":type})
        xpath_voucher_name = xpath.replace("voucher_name_to_be_replace", voucher_name)
        xpath_target_voucher_claim_btn = xpath_voucher_name.replace("index_to_be_replace", index)
        BaseUICore.Click({"method":"xpath", "locate":xpath_target_voucher_claim_btn, "message":"Click voucher claim", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickVoucherClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackUp(arg):
        '''
        ClickBackUp : Click back up button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back up button
        xpath = Util.GetXpath({"locate":"back_up"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back up button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickBackUp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click see more btn
        xpath = Util.GetXpath({"locate":"see_more"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see more btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLeftAddComponentTab(arg):
        '''
        ClickLeftAddComponentTab : Click left add component tab
                Input argu :
                    type - component type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]

        ##Click to add component
        xpath = Util.GetXpath({"locate":"left_tab"}).replace("component_type_to_be_replace", component_type)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher usage", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickLeftAddComponentTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCompontentOrder(arg):
        '''
        ChangeCompontentOrder : Change compontent order
                Input argu :
                    original_order - original order of compontent
                    target_order - target order of compontent
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        original_order = arg["original_order"]
        target_order = arg["target_order"]

        ##Get original order element
        xpath = Util.GetXpath({"locate": "change_btn"}).replace('order_to_be_replace', original_order)
        xpath_target = Util.GetXpath({"locate": "change_btn"}).replace('order_to_be_replace', target_order)

        ##Drag element
        BaseUICore.DragAndDrop({"drag_elem": xpath, "target_elem": xpath_target, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ChangeCompontentOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteCompontent(arg):
        '''
        ClickDeleteCompontent : Click delete compontent
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to delete first component
        xpath = Util.GetXpath({"locate":"delete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickDeleteCompontent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCompontentDuration(arg):
        '''
        ChangeCompontentDuration : Change compontent duration
                Input argu :
                    component_type - component_type
                    duration_type - always / duration
                    start_time - start time of duration
                    end_time - end time of duration
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        duration_type = arg["duration_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Click duration type
        xpath = Util.GetXpath({"locate":duration_type}).replace("compontent_to_be_replace", component_type)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click duration type", "result":"1"})

        if duration_type == "duration":
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)

            ##Input start time
            xpath = Util.GetXpath({"locate":"start_time_input"}).replace("compontent_to_be_replace", component_type)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_time, "result":"1"})

            ##Input end time
            xpath = Util.GetXpath({"locate":"end_time_input"}).replace("compontent_to_be_replace", component_type)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_time, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ChangeCompontentDuration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectComponentType(arg):
        '''
        SelectComponentType : Select component type
                Input argu :
                    type - type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg['type']

        #Select component drop down list
        xpath = Util.GetXpath({"locate":"component_type"})
        BaseUICore.SelectDropDownList({"locate":xpath, "selecttype":"text", "selectkey":component_type, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.SelectComponentType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassEdit(arg):
        '''
        ClickMassEdit : Click mass edit
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click mass edit btn
        xpath = Util.GetXpath({"locate":"mass_edit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mass edit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickMassEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLayoutAction(arg):
        '''
        ClickLayoutAction : Click layout action
                Input argu :
                    action - reorder / delete / done
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        #Click layout action btn
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + action + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickLayoutAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnsavedChangesOptions(arg):
        '''
        ClickUnsavedChangesOptions : Click unsaved changes options
                Input argu :
                    option - thanks_for_reminding / i_know
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        #Click unsaved changes options
        xpath = Util.GetXpath({"locate":option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickUnsavedChangesOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInfo(arg):
        '''
        ClickInfo : Click info btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click info btn
        xpath = Util.GetXpath({"locate":"info_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click info", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBundleDealInfo(arg):
        '''
        InputBundleDealInfo : Input Bundle Deal Info
                Input argu :
                    promo_id - number of shops display
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promo_id = arg["promo_id"]

        ##click select bundledeal to display btn
        xpath = Util.GetXpath({"locate":"select_local_bundledeal_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click select local bundledeal button", "result": "1"})

        xpath = Util.GetXpath({"locate":"search_promo_id"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath,"string": promo_id, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(5)

        xpath = Util.GetXpath({"locate":"select_btn"})
        xpath = xpath.replace('replaced_text', promo_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click select btn", "result": "1"})
        time.sleep(5)

        ##click done btn
        xpath = Util.GetXpath({"locate":"done_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click done btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputBundleDealInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyFromMobile(arg):
        '''
        ClickCopyFromMobile : Click Copy From Mobile
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Copy From Mobile
        xpath = Util.GetXpath({"locate":"copy_from_mobile_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Copy From Mobile", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickCopyFromMobile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCollectionsInCompenent(arg):
        '''
        InputCollectionsInCompenent : Input number of collections in component
                Input argu :
                    number - collections number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input number of collections in component
        xpath = Util.GetXpath({"locate":"component"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputCollectionsInCompenent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCollectionGroupID(arg):
        '''
        InputCollectionGroupID : Input collection group id
                Input argu :
                    collection_group_id - collection group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_group_id = arg["collection_group_id"]

        ##Input collection group id
        xpath = Util.GetXpath({"locate":"collection_group_id"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":collection_group_id, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputCollectionGroupID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBoostCollection(arg):
        '''
        ClickBoostCollection : Click boost collection btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click boost collection btn
        xpath = Util.GetXpath({"locate":"boost_collection_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click boostcollection", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickBoostCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionSelect(arg):
        '''
        ClickCollectionSelect : Click collection select
                Input argu :
                    collection_name - collection name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_name = arg["collection_name"]

        ##Click collection select
        xpath = Util.GetXpath({"locate":"select_btn"})
        xpath = xpath.replace("collection_to_be_replace", collection_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click collection select", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickCollectionSelect')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove button
                Input argu :
                    collection_group_id - collection group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_group_id = arg["collection_group_id"]

        ##Click remove button
        xpath = Util.GetXpath({"locate":"remove_btn"})
        xpath = xpath.replace("id_to_be_replace", collection_group_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click remove button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionDone(arg):
        '''
        ClickCollectionDone : Click done button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done button
        xpath = Util.GetXpath({"locate":"done_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click done button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickCollectionDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button
        xpath = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click OK button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDescription(arg):
        '''
        InputDescription : Input description
                Input argu :
                    description_text - description text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description_text = arg["description_text"]

        ##Input description
        xpath = Util.GetXpath({"locate":"description_text"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":description_text, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCollectionsInRow(arg):
        '''
        InputCollectionsInRow : Input number of collections in row
                Input argu :
                    number - collections number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input number of collections in row
        xpath = Util.GetXpath({"locate":"row"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCampaignMicrositeSettingsPage.InputCollectionsInRow')


class AdminMicrositeHotspotSettingsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHotspotAnchorID(arg):
        '''
        InputHotspotAnchorID : Input hotspot anchor id
                Input argu :
                    anchor_id - anchor id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        anchor_id = arg['anchor_id']

        ##Input hotspot anchor id
        xpath = Util.GetXpath({"locate":"anchor_id_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":anchor_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.InputHotspotAnchorID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHotspotLinkURL(arg):
        '''
        InputHotspotLinkURL : Input hotspot link url
                Input argu :
                    url - url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        url = arg['url']

        ##Input hotspot url
        ##If feature is Form, then will input form url by region
        if Config._TestCaseFeature_ == "Form":
            xpath = Util.GetXpath({"locate":"url"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})

            url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/program/form/"
            dumplogger.info("url = %s" % (url))
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":url + GlobalAdapter.FormE2EVar._FormID_, "result": "1"})
        else:
            xpath = Util.GetXpath({"locate":"url"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.InputHotspotLinkURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHotspotReminderInfo(arg):
        '''
        InputHotspotReminderInfo : Input hotspot link url
                Input argu : title - title
                             popup_message - popup_message
                             url - url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 30, 0)
        title = arg['title']
        popup_message = arg['popup_message']
        url = arg['url']

        ##remove attribute readonly and insert end time
        BaseUICore.ExecuteScript({"script": "document.getElementsByClassName('ant-calendar-picker-input ant-input')[1].removeAttribute('readonly')", "result": "1"})
        xpath = Util.GetXpath({"locate":"end_time"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time", "result": "1"})
        xpath = Util.GetXpath({"locate":"input_end_time"})
        BaseUICore.SendKeyboardEvent({"locate":xpath, "sendtype": "select all", "result": "1"})
        BaseUICore.Input({"method":"xpath_no_clear", "locate":xpath, "string":end_time, "result": "1"})

        ##click submit to submit end time
        xpath = Util.GetXpath({"locate":"submit_end_time_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Submit end time", "result": "1"})

        ##Input title
        xpath = Util.GetXpath({"locate":"title"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":title, "result": "1"})

        ##Input popup msg
        xpath = Util.GetXpath({"locate":"popup_msg"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":popup_message, "result": "1"})

        ##Input url
        xpath = Util.GetXpath({"locate":"url"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.InputHotspotReminderInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHotspotAddToCart(arg):
        '''
        InputHotspotAddToCart : Input hotspot link url
                Input argu :
                    shop_id - shop_id
                    item_id - item_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg['shop_id']
        item_id = arg['item_id']

        ##Input shop id
        xpath = Util.GetXpath({"locate":"shop_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":shop_id, "result": "1"})

        ##Input item id
        xpath = Util.GetXpath({"locate":"item_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":item_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.InputHotspotAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHotspotAddToCartPlusReminderInfo(arg):
        '''
        InputHotspotAddToCartPlusReminderInfo : Input hotspot add to cart + reminder info
                Input argu :
                    title - title
                    popup_message - popup_message
                    shop_id - shop_id
                    item_id - item_id
                    start_time_minute - start time minute
                    end_time_minute - end time minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg['title']
        popup_message = arg['popup_message']
        shop_id = arg['shop_id']
        item_id = arg['item_id']
        start_time_minute = arg['start_time_minute']
        end_time_minute = arg['end_time_minute']

        start_time_minute = XtFunc.GetCurrentDateTime("%H:%M", 0, int(start_time_minute), 0)
        end_time_minute = XtFunc.GetCurrentDateTime("%H:%M", 0, int(end_time_minute), 0)

        ##Click start time input to open calendar input
        xpath = Util.GetXpath({"locate":"start_time_input"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time input", "result":"1"})

        ##Input start time
        xpath = Util.GetXpath({"locate":"time_calendar_input"})

        ##Delete time string to year, if delete all string will display error 500
        for back_time in range(0, 5):
            BaseUICore.SendKeyboardEvent({"locate":xpath, "sendtype": "back", "result":"1"})

        ##Can't input by input function, only can using keyboard event
        for text_char in start_time_minute:
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": text_char, "result": "1"})
            #XtFunc.WindowsKeyboardEvent(action="type_string", value=text_char, number=None, interval=0.01)

        ##Click OK on calendar
        xpath = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time input", "result":"1"})

        ##Click end time input to open calendar input
        xpath = Util.GetXpath({"locate":"end_time_input"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time input", "result":"1"})

        ##Input end time
        xpath = Util.GetXpath({"locate":"time_calendar_input"})

        ##Delete time string to year, if delete all string will display error 500
        for back_time in range(0, 5):
            BaseUICore.SendKeyboardEvent({"locate":xpath, "sendtype": "back", "result":"1"})

        ##Can't input by input function, only can using keyboard event
        for text_char in end_time_minute:
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": text_char, "result": "1"})
            #XtFunc.WindowsKeyboardEvent(action="type_string", value=text_char, number=None, interval=0.01)

        ##Input title
        xpath = Util.GetXpath({"locate":"title_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":title, "result":"1"})

        ##Input popup message
        xpath = Util.GetXpath({"locate":"popup_message_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":popup_message, "result":"1"})

        ##Input shop id
        xpath = Util.GetXpath({"locate":"shop_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":shop_id, "result":"1"})

        ##Input item id
        xpath = Util.GetXpath({"locate":"item_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":item_id, "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.InputHotspotAddToCartPlusReminderInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHotspotPopupMessage(arg):
        '''
        InputHotspotPopupMessage : Input hotspot popup message
                Input argu :
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        message = arg['message']

        ##Input hotspot popup msg
        xpath = Util.GetXpath({"locate":"message"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":message, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.InputHotspotPopupMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddHotspot(arg):
        '''
        ClickAddHotspot : Click hotspot add button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add button
        xpath = Util.GetXpath({"locate":"hotspot_add_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click hotspot add button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.ClickAddHotspot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDone(arg):
        '''
        ClickDone : Click done button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Click "Done" button
            xpath = Util.GetXpath({"locate":"done_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click done button", "result": "1"})

            ##Handle popup
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        except:
            dumplogger.info("Success edit hotspot.")

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.ClickDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopy(arg):
        '''
        ClickCopy : Copy component
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click copy btn
        xpath = Util.GetXpath({"locate":"copy_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click copy btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.ClickCopy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Delete component
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete btn
        xpath = Util.GetXpath({"locate":"delete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyHotspots(arg):
        '''
        ClickCopyHotspots : Click copy hotspots link, reminder or add to cart
                Input argu :
                    order - order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]

        ##Click copy btn of hotspots
        xpath = Util.GetXpath({"locate":"copy_btn"}).replace("order_to_be_replace", order)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click copy btn", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.ClickCopyHotspots')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteHotspots(arg):
        '''
        ClickDeleteHotspots : Click delete hotspots link, reminder or add to cart
                Input argu :
                    order - order
                    action - action
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]
        action = arg["action"]

        ##Click delete btn of hotspots
        xpath = Util.GetXpath({"locate":"delete_btn"}).replace("order_to_be_replace", order)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete btn", "result":"1"})

        ##Click action btn of hotspots
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click action btn -> %s" % (action), "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeHotspotSettingsPage.ClickDeleteHotspots')


class AdminMicrositePlaceHolderSettingsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPlaceholderKey(arg):
        '''
        InputPlaceholderKey : Input place holder key
                Input argu :
                    key - placeholders key
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        key = arg['key']

        ##Input placeholders key
        xpath = Util.GetXpath({"locate":"placeholders_key"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":key, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositePlaceHolderSettingsPage.InputPlaceholderKey')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPlaceholderDefaultText(arg):
        '''
        InputPlaceholderDefaultText : Input place holder default text
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input placeholders default text
        xpath = Util.GetXpath({"locate":"placeholders_default_text"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"twqa", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositePlaceHolderSettingsPage.InputPlaceholderDefaultText')


class AdminMicrositePreviewPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotPopup(arg):
        '''
        ClickHotspotPopup : Click hotspot popup in preview page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot popup
        xpath = Util.GetXpath({"locate":"hotspot_popup"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click hotspot popup btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositePreviewPage.ClickHotspotPopup')

class AdminMicrositeInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPageTitle(arg):
        '''
        InputPageTitle : Input page title
                Input argu :
                    page_title - page title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_title = arg['page_title']

        ##Input page title
        xpath = Util.GetXpath({"locate":"page_title_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":page_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputPageTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPageDescription(arg):
        '''
        InputPageDescription : Input page description
                Input argu :
                    page_description - page description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_description = arg['page_description']

        ##Input page description
        xpath = Util.GetXpath({"locate":"page_description_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":page_description, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputPageDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPageUrl(arg):
        '''
        InputPageUrl : Input page url
                Input argu :
                    page_url - page url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_url = arg['page_url']

        ##Input page url
        xpath = Util.GetXpath({"locate":"page_url_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":page_url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputPageUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShareTitle(arg):
        '''
        InputShareTitle : Input share title
                Input argu :
                    share_title - share title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        share_title = arg['share_title']

        ##Input share title
        xpath = Util.GetXpath({"locate":"share_title_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":share_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputShareTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShareDescription(arg):
        '''
        InputShareDescription : Input share description
                Input argu :
                    share_description - share description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        share_description = arg['share_description']

        ##Input share description
        xpath = Util.GetXpath({"locate":"share_description_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":share_description, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputShareDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShareImage(arg):
        '''
        UploadShareImage : Upload share image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "MicroSite" + slash + "image" + slash + "info_share_image.jpg"

        ##upload image
        xpath = Util.GetXpath({"locate":"share_image"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":file_path, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.UploadShareImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchSearchToggle(arg):
        '''
        SwitchSearchToggle : Switch search toggle
                Input argu :
                    option - on / off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        xpath = Util.GetXpath({"locate": "toggle_" + option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Switch on", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.SwitchSearchToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPlaceholder(arg):
        '''
        InputPlaceholder : Input placeholder id
                Input argu :
                    placeholder_id - placeholder id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        placeholder_id = arg['placeholder_id']

        ##Input placeholder id
        xpath = Util.GetXpath({"locate":"placeholder_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":placeholder_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputPlaceholder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSEOTitle(arg):
        '''
        InputSEOTitle : Input SEO title
                Input argu :
                    seo_title - SEO title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seo_title = arg['seo_title']

        ##Input SEO title
        xpath = Util.GetXpath({"locate":"seo_title_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":seo_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.InputSEOTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button in info page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save btn", "result": "1"})

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminMicrositeInfoPage.ClickSave')


class AdminMicrositeURLSchedulerSettingsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddURLScheduler(arg):
        '''
        ClickAddURLScheduler : Click add URL scheduler
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add
        xpath = Util.GetXpath({"locate":"add_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickAddURLScheduler')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainURL(arg):
        '''
        ClickMainURL : Click main URL
                Input argu :
                    main_url - main url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        main_url = arg["main_url"]

        ##Click main URL
        xpath = Util.GetXpath({"locate":"main_url"}).replace("main_url_to_be_replace", main_url)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click url", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickMainURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDefaultPageID(arg):
        '''
        ClickDefaultPageID : Click default page ID
                Input argu :
                    main_url - main url of default page ID
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        main_url = arg["main_url"]

        ##Click default page ID
        xpath = Util.GetXpath({"locate":"page_id_url"}).replace("main_url_to_be_replace", main_url)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Default Page ID url", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickDefaultPageID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOngoingPageID(arg):
        '''
        ClickOngoingPageID : Click ongoing page ID
                Input argu :
                    main_url - main url of on going page ID
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        main_url = arg["main_url"]

        ##Click ongoing page ID
        xpath = Util.GetXpath({"locate":"page_id_url"}).replace("main_url_to_be_replace", main_url)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Default Page ID url", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickOngoingPageID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisableOrEnableMainURL(arg):
        '''
        ClickDisableOrEnableMainURL : Click disable main URL
                Input argu :
                    main_url - main url of disable page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        main_url = arg["main_url"]

        ##Click disable
        xpath = Util.GetXpath({"locate":"btn"}).replace("main_url_to_be_replace", main_url)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click disable", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickDisableOrEnableMainURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainURLTab(arg):
        '''
        ClickMainURLTab : Click main URL tab
                Input argu :
                    tab - tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        tab = arg["tab"]

        ##Click tab
        js_script = """document.querySelector("[data-tab='""" + tab + """-url-scheduler']").click()"""

        BaseUICore.ExecuteScript({"script": js_script, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickMainURLTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditMainURL(arg):
        '''
        ClickEditMainURL : Click to edit main URL
                Input argu :
                    main_url - main url of edit page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        main_url = arg["main_url"]

        ##Click to to edit main URL
        xpath = Util.GetXpath({"locate":"edit"}).replace("url_to_be_replace", main_url)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickEditMainURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditSchedule(arg):
        '''
        ClickEditSchedule : Click to edit main schedule
                Input argu :
                    page_id - page id of main schedule
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page_id = arg["page_id"]

        ##Click to to edit main URL
        xpath = Util.GetXpath({"locate":"edit_btn"}).replace("page_id_to_be_replace", page_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickEditSchedule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMainAttributesURL(arg):
        '''
        InputMainAttributesURL : Input main attributes URL
                Input argu :
                    url - url of main attributes
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        url = arg["url"]

        ##Input end time
        xpath = Util.GetXpath({"locate":"main_url_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        ##Click save
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.InputMainAttributesURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDefaultPageID(arg):
        '''
        InputDefaultPageID : Input default page id
                Input argu :
                    page_id - page id of defaul page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page_id = arg["page_id"]

        ##Input end time
        xpath = Util.GetXpath({"locate":"page_id_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_id, "result": "1"})

        ##Click save
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.InputDefaultPageID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSchedulingPageID(arg):
        '''
        InputSchedulingPageID : Input scheduling page ID
                Input argu :
                    page_id - page_id
                    start_time - start time
                    end_time - end time
                    action - add / edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page_id = arg["page_id"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        action = arg["action"]

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(start_time), 0)
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0)

        ##Input page ID
        xpath = Util.GetXpath({"locate":action + "_page_id_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_id, "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate":action + "_start_time_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time", "result":"1"})

        ##Input end time
        xpath = Util.GetXpath({"locate":action + "_end_time_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time", "result":"1"})

        ##Click save
        xpath = Util.GetXpath({"locate":action + "_save"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.InputSchedulingPageID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteSchedulePageID(arg):
        '''
        ClickDeleteSchedulePageID : Click delete schedule page ID
                Input argu :
                    page_id - page id of delete schedule page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page_id = arg["page_id"]

        ##Click delete
        xpath = Util.GetXpath({"locate":"delete_btn"}).replace("pageid_to_be_replace", page_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMicrositeURLSchedulerSettingsPage.ClickDeleteSchedulePageID')


class AdminCollectionAssetsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCollectionAssets(arg):
        '''
        EditCollectionAssets : Edit collection assets
                Input argu :
                    name - collection asset name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Find specific collection by name and click its edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("string_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit collection assets.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCollectionAssetsPage.EditCollectionAssets')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCollectionAssetContent(arg):
        '''
        ModifyCollectionAssetContent : Modify collection asset contents
                Input argu :
                    group_name - group name (optional)
                    section - sections in collection assets (collection_id / collection_image / collection_text)
                    file_name - file name
                    file_type - file type
                    project - image file project path
                    action - action going to execute after uploading files
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        project = arg["project"]
        action = arg["action"]

        if "group_name" in arg:
            ##Modify group name (if needed)
            group_name = arg["group_name"]
            xpath = Util.GetXpath({"locate": "group_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_name, "result": "1"})

        ##Set file upload path
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + project + slash + "file" + slash + file_name + "." + file_type

        ##Upload file to specific section
        xpath = Util.GetXpath({"locate": section})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
        time.sleep(5)

        ##Execute different action for different section
        if action == "update":
            ##Since there are two kinds of update in different section
            if section == "collection_image":
                xpath = Util.GetXpath({"locate": "image_update"})

            elif section == "collection_text":
                xpath = Util.GetXpath({"locate": "text_update"})

        else:
            xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Action for " + section + ":" + action, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCollectionAssetsPage.ModifyCollectionAssetContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveCollectionAsset(arg):
        '''
        ClickSaveCollectionAsset : Save collection assets
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Find specific collection by name and click its edit btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Save collection assets.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCollectionAssetsPage.ClickSaveCollectionAsset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCreateGroupName(arg):
        '''
        InputCreateGroupName : Input group name when create a new group
                Input argu :
                    group_name - group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_name = arg["group_name"]

        xpath = Util.GetXpath({"locate": "group_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCollectionAssetsPage.InputCreateGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Save collection assets
                Input argu :
                    section - sections in collection assets (collection_id / collection_image / collection_text)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Find specific section and click its export btn
        xpath = Util.GetXpath({"locate": section + "_export_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCollectionAssetsPage.ClickExport')


class AdminShopCampaignPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateCampaignButton(arg):
        '''
        ClickCreateCampaignButton : Click create campaign btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create campaign btn
        xpath = Util.GetXpath({"locate": "create_campaign_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create campaign btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickCreateCampaignButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateShopCampaign(arg):
        '''
        CreateShopCampaign : Create shop campaign
                Input argu :
                    campaign_name - campaign name
                    adjust_nomination_start_time - nomination start time of the shop campaign
                    adjust_start_time - start time of the shop campaign
                    adjust_end_time - end time of the shop campaign
                    product_label - product label
                    main_site_url - to main site url
                    vouchers_number - numbers of voucher
                    entrance_title - entrance title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        campaign_name = arg["campaign_name"]
        adjust_nomination_start_time = arg["adjust_nomination_start_time"]
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        product_label = arg["product_label"]
        main_site_url = arg["main_site_url"]
        vouchers_number = arg["vouchers_number"]
        entrance_title = arg["entrance_title"]

        ret = 1

        ##Input campaign name
        xpath = Util.GetXpath({"locate": "campaign_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": campaign_name, "result": "1"})

        ##Adjust nomination start time, start time and end time
        ##Input nomination start time
        if adjust_nomination_start_time:
            ##Get current date time and add minutes on it
            time_stamp_nomination_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(adjust_nomination_start_time), 0)

            ##Input nomination start time
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "display_nomination_start_time"}), "string": time_stamp_nomination_start, "result": "1"})

            ##Click other place to close datetime picker
            xpath = Util.GetXpath({"locate": "edit_campaign_page_title"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit campaign page title.", "result": "1"})

        ##Input start time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(adjust_start_time), 0)

            ##Input start time
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "display_start_time"}), "string": time_stamp_start, "result": "1"})

            ##Click other place to close datetime picker
            xpath = Util.GetXpath({"locate": "edit_campaign_page_title"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit campaign page title.", "result": "1"})

        ##Input end time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(adjust_end_time), 0)

            ##Input nomination start time
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "display_end_time"}), "string": time_stamp_end, "result": "1"})

            ##Click other place to close datetime picker
            xpath = Util.GetXpath({"locate": "edit_campaign_page_title"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit campaign page title.", "result": "1"})

        ##Select product label dropdown label
        xpath = Util.GetXpath({"locate": "dropdown_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product label select", "result": "1"})

        ##Choose product label
        xpath = Util.GetXpath({"locate": "select_product_label"}).replace("label_to_be_replace", product_label)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product label", "result": "1"})

        ##Upload header background image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "header_background_image", "element_type": "data-name", "project": "ShopPage", "action": "image", "file_name": "header_background_img", "file_type": "png", "result": "1"})
        dumplogger.info("Success upload header background image.")

        ##Set background color
        BaseUICore.ExecuteScript({"script": "document.querySelector('.item > input[type=color]').value = '#ffc100'", "result": "1"})

        ##Upload tab bar campaign tab icon
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "campaign_tab_icon", "element_type": "data-name", "project": "ShopPage", "action": "image", "file_name": "campaign_tab_icon", "file_type": "png", "result": "1"})
        dumplogger.info("Success upload campaign tab icon.")

        ##Input main site url
        xpath = Util.GetXpath({"locate": "main_site_url"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": main_site_url, "result": "1"})

        ##Upload main site image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "image", "element_type": "data-name", "project": "ShopPage", "action": "image", "file_name": "main_site_image", "file_type": "jpg", "result": "1"})
        dumplogger.info("Success upload main site image.")

        ##Input vouchers number
        AdminShopCampaignPage.EditMaxNumberOfVouchers({"vouchers_number":vouchers_number, "result": "1"})

        ##Upload entrance image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "entrance_image", "element_type": "data-name", "project": "ShopPage", "action": "image", "file_name": "entrance_image", "file_type": "png", "result": "1"})
        dumplogger.info("Success upload entrance image.")

        ##Input entrance title
        xpath = Util.GetXpath({"locate": "entrance_title"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": entrance_title, "result": "1"})

        ##Input entrance description
        xpath = Util.GetXpath({"locate": "entrance_description"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": entrance_title, "result": "1"})

        ##Input entrance title (after published)
        xpath = Util.GetXpath({"locate": "entrance_title_after_published"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": entrance_title, "result": "1"})

        ##Input entrance button text
        xpath = Util.GetXpath({"locate": "entrance_button_text"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": entrance_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.CreateShopCampaign')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click shop campaign page save btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save btn
        xpath = Util.GetXpath({"locate": "shop_campaign_page_save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopListTab(arg):
        '''
        ClickShopListTab : Click shop list tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shop list tab
        xpath = Util.GetXpath({"locate": "shop_list_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop list tab.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickShopListTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassUploadContentTab(arg):
        '''
        ClickMassUploadContentTab : Click mass upload content tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mass upload content tab
        xpath = Util.GetXpath({"locate": "mass_upload_content_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass upload content tab.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickMassUploadContentTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShopWhitelist(arg):
        '''
        UploadShopWhitelist : Upload shop white list
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Upload shop white list
        xpath = Util.GetXpath({"locate":"upload_btn"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload shop white list file.")

        ##Click add button
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn.", "result": "1"})

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.UploadShopWhitelist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopListSectionEditButton(arg):
        '''
        ClickShopListSectionEditButton : Click shop list section edit btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shop list section edit btn
        xpath = Util.GetXpath({"locate": "shop_list_section_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop list section edit btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickShopListSectionEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublishButton(arg):
        '''
        ClickPublishButton : Click publish btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click publish btn
        xpath = Util.GetXpath({"locate": "publish_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click publish btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickPublishButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopCampaignPageEditButton(arg):
        '''
        ClickShopCampaignPageEditButton : Click shop campaign page edit btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shop campaign page edit btn
        xpath = Util.GetXpath({"locate": "shop_campaign_page_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop campaign page edit btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickShopCampaignPageEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditMaxNumberOfVouchers(arg):
        '''
        EditMaxNumberOfVouchers : Edit max number of vouchers
                Input argu :
                    vouchers_number - numbers of voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        vouchers_number = arg["vouchers_number"]

        ret = 1

        ##Edit max number of vouchers
        xpath = Util.GetXpath({"locate": "vouchers_number"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": vouchers_number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.EditMaxNumberOfVouchers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSellerVoucherFile(arg):
        '''
        UploadSellerVoucherFile : Upload seller voucher file
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Upload seller voucher file
        xpath = Util.GetXpath({"locate":"seller_voucher_file_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload seller voucher file.")
        time.sleep(5)

        ##Click add button
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn.", "result": "1"})
        time.sleep(5)

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.UploadSellerVoucherFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveSellerVoucherFile(arg):
        '''
        RemoveSellerVoucherFile : Remove seller voucher file
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Remove seller voucher file
        xpath = Util.GetXpath({"locate":"seller_voucher_file_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": "seller_vouchers", "file_type": "csv", "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload seller voucher file.")
        time.sleep(5)

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove btn.", "result": "1"})

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.RemoveSellerVoucherFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadFeaturedProductsFile(arg):
        '''
        UploadFeaturedProductsFile : Upload featured products file
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Upload featured products file
        xpath = Util.GetXpath({"locate":"featured_products_file_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload featured products file.")
        time.sleep(5)

        ##Click add button
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn.", "result": "1"})
        time.sleep(20)

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.UploadFeaturedProductsFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveFeaturedProductsFile(arg):
        '''
        RemoveFeaturedProductsFile : Remove featured products file
                Input argu :
                    product_amount - all / not_all
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        product_amount = arg["product_amount"]

        ret = 1

        if product_amount == "all":
            ##Upload featured products file
            xpath = Util.GetXpath({"locate":"featured_products_file_upload_button"})
            BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": "featured_products", "file_type": "csv", "element":"file", "element_type":"name", "result": "1"})
            dumplogger.info("Success upload featured products file.")
            time.sleep(10)

        elif product_amount == "not_all":
            ##Upload featured products file
            xpath = Util.GetXpath({"locate":"featured_products_file_upload_button"})
            BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": "featured_products_remove", "file_type": "csv", "element":"file", "element_type":"name", "result": "1"})
            dumplogger.info("Success upload featured products file.")
            time.sleep(10)

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove btn.", "result": "1"})

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.RemoveFeaturedProductsFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBannerFile(arg):
        '''
        UploadBannerFile : Upload banner file
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Upload banner file
        xpath = Util.GetXpath({"locate":"banner_file_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload banner file.")
        time.sleep(5)

        ##Click add button
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn.", "result": "1"})

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.UploadBannerFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveBannerFile(arg):
        '''
        RemoveBannerFile : Remove banner file
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Remove banner file
        xpath = Util.GetXpath({"locate":"banner_file_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "file", "file_name": "banner_file_remove", "file_type": "csv", "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload banner file.")
        time.sleep(5)

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove btn.", "result": "1"})

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.RemoveBannerFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDraftAllButton(arg):
        '''
        ClickDraftAllButton : Click draft all btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click draft all btn
        xpath = Util.GetXpath({"locate": "draft_all_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click draft all btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickDraftAllButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublishAllButton(arg):
        '''
        ClickPublishAllButton : Click publish all btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click publish all btn
        xpath = Util.GetXpath({"locate": "publish_all_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click publish all btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickPublishAllButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLockButton(arg):
        '''
        ClickLockButton : Click lock btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click lock btn
        xpath = Util.GetXpath({"locate": "lock_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click lock btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickLockButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnlockButton(arg):
        '''
        ClickUnlockButton : Click unlock btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click unlock btn
        xpath = Util.GetXpath({"locate": "unlock_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click unlock btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickUnlockButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDraftButton(arg):
        '''
        ClickDraftButton : Click draft btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click draft btn
        xpath = Util.GetXpath({"locate": "draft_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click draft btn.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.ClickDraftButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EndShopCampaign(arg):
        '''
        EndShopCampaign : End shop campaign
                Input argu :
                    adjust_end_time - end time of the shop campaign
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        adjust_end_time = arg["adjust_end_time"]

        ret = 1

        ##Get current date time and add minutes on it
        time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(adjust_end_time), 0)

        ##Input end time
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "display_end_time"}), "string": time_stamp_end, "result": "1"})

        ##Click other place to close datetime picker
        xpath = Util.GetXpath({"locate": "edit_campaign_page_title"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit campaign page title.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCampaignPage.EndShopCampaign')


class AdminSpecialShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickActionButton(arg):
        '''
        ClickActionButton : Click action button
                Input argu :
                    shopid - shop id
                    action - edit / delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        shopid = arg["shopid"]
        action = arg["action"]

        ret = 1

        ##Click edit button
        if action == "edit":
            xpath = Util.GetXpath({"locate": "edit_button"})
            xpath = xpath.replace('replaced_text', shopid)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button.", "result": "1"})

        ##Click delete button
        elif action == "delete":
            xpath = Util.GetXpath({"locate": "delete_button"})
            xpath = xpath.replace('replaced_text', shopid)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button.", "result": "1"})
            time.sleep(5)

            ##Click confirm button
            xpath = Util.GetXpath({"locate": "confirm_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickActionButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new btn
        xpath = Util.GetXpath({"locate": "add_new_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTemplate(arg):
        '''
        SelectTemplate : Select template
                Input argu :
                    options - template1 / template2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        options = arg["options"]

        ret = 1

        ##Select template
        xpath = Util.GetXpath({"locate": options})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select template.", "result": "1"})

        ##Click create button
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.SelectTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBasicInfo(arg):
        '''
        EditBasicInfo : Edit basic info
                Input argu :
                    title - title
                    shopid - shop id
                    username - username
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        title = arg["title"]
        shopid = arg["shopid"]
        username = arg["username"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Edit title
        xpath = Util.GetXpath({"locate": "title_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Edit shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "result": "1"})

        ##Edit username
        xpath = Util.GetXpath({"locate": "username_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": username, "result": "1"})

        ##Upload shop logo
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-shop-logo", "element_type": "id", "project": "ShopPage", "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})
        dumplogger.info("Success upload shop logo.")
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.EditBasicInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click tab
                Input argu :
                    tab_name - basic_info / carousel_banners / highlighted_categories / best_sellers / shop_collections / images_and_collections
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        tab_name = arg["tab_name"]

        ret = 1

        xpath = Util.GetXpath({"locate": tab_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tab.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewBanners(arg):
        '''
        ClickAddNewBanners : Add new banners
                Input argu :
                    platform - app / pc
                    title - title
                    url - url
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        platform = arg["platform"]
        title = arg["title"]
        url = arg["url"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Click add new button
        dumplogger.info("Add new banner.")
        xpath = Util.GetXpath({"locate": platform + "_add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button.", "result": "1"})

        ##Edit title
        xpath = Util.GetXpath({"locate": platform + "_title_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Edit url
        xpath = Util.GetXpath({"locate": platform + "_url_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        ##Upload banner
        xpath = Util.GetXpath({"locate": platform + "_banner_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "image", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload banner file.")
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickAddNewBanners')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewCategories(arg):
        '''
        ClickAddNewCategories : Add new categories
                Input argu :
                    title - title
                    shopid - shop id
                    collectionid - collection id
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        title = arg["title"]
        shopid = arg["shopid"]
        collectionid = arg["collectionid"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button.", "result": "1"})

        ##Edit title
        xpath = Util.GetXpath({"locate": "title_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Edit shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "result": "1"})

        ##Edit collection id
        xpath = Util.GetXpath({"locate": "collectionid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collectionid, "result": "1"})

        ##Upload image
        xpath = Util.GetXpath({"locate":"image_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "image", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload image.")

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickAddNewCategories')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBestSellersInfo(arg):
        '''
        EditBestSellersInfo : Edit best sellers info
                Input argu :
                    title - title
                    shopid - shop id
                    collectionid - collectionid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        title = arg["title"]
        shopid = arg["shopid"]
        collectionid = arg["collectionid"]

        ret = 1

        ##Edit title
        xpath = Util.GetXpath({"locate": "title_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Edit shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "result": "1"})

        ##Edit collection id
        xpath = Util.GetXpath({"locate": "collectionid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collectionid, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.EditBestSellersInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewCollections(arg):
        '''
        ClickAddNewCollections : Add new collections
                Input argu :
                    template - template1 / template2
                    platform - app / pc
                    title - title
                    shopid - shop id
                    collectionid - collection id
                    file_name - file name
                    file_type - file type
                    description - description
                    title_link - title link
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        template = arg["template"]
        platform = arg["platform"]
        title = arg["title"]
        shopid = arg["shopid"]
        collectionid = arg["collectionid"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        description = arg["description"]
        title_link = arg["title_link"]

        ret = 1

        if template == "template1":
            ##Click add new button
            xpath = Util.GetXpath({"locate": "add_new"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button.", "result": "1"})

            ##Edit description
            xpath = Util.GetXpath({"locate": template + "_description_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        elif template == "template2":
            ##Click add new collection button
            xpath = Util.GetXpath({"locate": platform + "_new_collection"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new collection button.", "result": "1"})

            ##Edit title link
            xpath = Util.GetXpath({"locate": template + "_title_link_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title_link, "result": "1"})

        ##Edit title
        xpath = Util.GetXpath({"locate": template + "_title_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Edit shop id
        xpath = Util.GetXpath({"locate": template + "_shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "result": "1"})

        ##Edit collection id
        xpath = Util.GetXpath({"locate": template + "_collectionid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collectionid, "result": "1"})

        ##Upload image
        xpath = Util.GetXpath({"locate": template + "_image_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "image", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload image.")

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickAddNewCollections')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewImage(arg):
        '''
        ClickAddNewImage : Click add new image
                Input argu :
                    platform - app / pc
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        platform = arg["platform"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ret = 1

        ##Click add new image button
        xpath = Util.GetXpath({"locate": platform + "_add_new_image"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new image button.", "result": "1"})
        time.sleep(5)

        ##Upload image
        xpath = Util.GetXpath({"locate":"image_upload_button"})
        BaseUILogic.UploadFileWithPath({"method":"xpath", "locate":xpath, "project": "ShopPage", "action": "image", "file_name": file_name, "file_type": file_type, "element":"file", "element_type":"name", "result": "1"})
        dumplogger.info("Success upload image.")

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickAddNewImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditNewHotspot(arg):
        '''
        EditNewHotspot : Edit new hotspot
                Input argu :
                    link - link
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        link = arg["link"]

        ret = 1

        ##Click on "+" button
        xpath = Util.GetXpath({"locate": "plus_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click plus button.", "result": "1"})

        ##Click add new hotspot button
        xpath = Util.GetXpath({"locate": "add_new_hotspot"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new hotspot button.", "result": "1"})

        ##Edit link
        xpath = Util.GetXpath({"locate": "link_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.EditNewHotspot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTemplateTab(arg):
        '''
        ClickTemplateTab : Click template tab
                Input argu :
                    template - template1 / template2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        template = arg["template"]

        ret = 1

        xpath = Util.GetXpath({"locate": template})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click template tab: " + template, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickTemplateTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublish(arg):
        '''
        ClickPublish : Click publish btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click publish button
        xpath = Util.GetXpath({"locate": "publish"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click publish button.", "result": "1"})
        time.sleep(5)

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickPublish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveAsDraft(arg):
        '''
        ClickSaveAsDraft : Click save as draft btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save as draft button
        xpath = Util.GetXpath({"locate": "draft"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save as draft button.", "result": "1"})
        time.sleep(5)

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopPage.ClickSaveAsDraft')


class AdminWelcomePackagePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewImageButton(arg):
        '''
        ClickAddNewImageButton : Click add new button in welcome package page
                Input argu :
                    banner_type : popup_banner / entrance_banner
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        banner_type = arg["banner_type"]
        ret = 1

        ##Click add new button to add image
        xpath = Util.GetXpath({"locate": banner_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn.", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminWelcomePackagePage.ClickAddNewImageButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBanner(arg):
        '''
        CreateBanner : Create Banner
                Input argu :
                    is_upload : 0 - upload
                    file_name : file name
                    file_type : file type
                    visibility_type : visible
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        is_upload = arg["is_upload"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        visibility_type = arg["visibility_type"]
        ret = 1

        ##Choose if Visible
        if visibility_type:
            xpath = Util.GetXpath({"locate": "visible_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility", "result": "1"})

        ##Upload popup banner
        if is_upload:
            AdminWelcomePackagePage.UploadBannerImg({"element": "banner-picker", "element_type": "id", "project": "WelcomePackage", "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminWelcomePackagePage.CreateBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBannerImg(arg):
        '''
        UploadBannerImg : Upload image for banner
                Input argu :
                    element - input element id or class
                    element_type - element type: id/name/class
                    project - your project name
                    action - image/file
                    image - image name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element = arg["element"]
        element_type = arg["element_type"]
        project = arg["project"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload banner image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": element, "element_type": element_type, "project": project, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        time.sleep(5)
        OK(ret, int(arg['result']), 'AdminWelcomePackagePage.UploadBannerImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminWelcomePackagePage.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageButton(arg):
        '''
        ClickPageButton : Click page button
                Input argu :
                    banner_type : popup_banner / entrance_banner
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]

        ##Click page button
        xpath = Util.GetXpath({"locate": banner_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminWelcomePackagePage.ClickPageButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchField(arg):
        '''
        InputSearchField : Search with operator
                Input argu :
                    banner_type : popup_banner / entrance_banner
                    keyword : keyword for search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        keyword = arg["keyword"]

        ##Search keyword
        xpath = Util.GetXpath({"locate": banner_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": keyword, "result": "1"})

        OK(ret, int(arg['result']), 'AdminWelcomePackagePage.InputSearchField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToggle(arg):
        '''
        ClickToggle : Click toggle checkbox
                Input argu :
                    banner_type : popup_banner / entrance_banner
                    operator: upload operator
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        operator = arg["operator"]

        ##Click toggle checkbox
        xpath = Util.GetXpath({"locate": banner_type})
        xpath = xpath.replace("string_to_be_replaced", operator)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminWelcomePackagePage.ClickToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteButton(arg):
        '''
        ClickDeleteButton : Click delete button
                Input argu :
                    banner_type : popup_banner / entrance_banner
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]

        ##Click delete button
        xpath = Util.GetXpath({"locate": banner_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        ##Detect handle popup message
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        else:
            dumplogger.info("Not detect popup message!!!")

        OK(ret, int(arg['result']), 'AdminWelcomePackagePage.ClickDeleteButton')


class AdminSharingContentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditButton(arg):
        '''
        ClickEditButton : Click edit button
                Input argu :
                    page_type - my_shop / others_product / my_product_discount / others_product_discount / my_shop / others_shop / referral / group_buy / microsite / feed / slash_price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace('replaced_text', page_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSharingContentPage.ClickEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSharingContentPage.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSharingContentField(arg):
        '''
        InputSharingContentField : Input text in sharing content field
                Input argu :
                    field_type - copy_link / share_text1 / share_text2 / share_url / share_title / share_text_facebook / share_description / email_title / email_body / quote / hashtag / attribution_link
                    input_text - sharing text content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        field_type = arg["field_type"]
        input_text = arg["input_text"]
        ret = 1

        ##Click copy link
        xpath = Util.GetXpath({"locate": field_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSharingContentPage.InputSharingContentField -> ' + field_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImage(arg):
        '''
        UploadImage : Upload image for sharing content
                Input argu :
                    image_shape - square-picker / rectangle-picker
                    file_name - image file neme
                    file_type - image file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        image_shape = arg["image_shape"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        ret = 1

        ##Upload square / rectangle image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": image_shape, "element_type": "id", "project": "SharingPanel", "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSharingContentPage.UploadImage')

class AdminItemCardDisplayPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemCardSet(arg):
        '''
        EditItemCardSet : Edit item card set
                Input argu :
                    name - item card type name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Find specific collection by name and click its edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"}).replace("item_card_name_to_be_replace", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit of item card set", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.EditItemCardSet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeOverlayImageOrder(arg):
        '''
        ChangeOverlayImageOrder : Change overlay image order
                Input argu :
                    card_type - item card type
                    image_name - image name
                    target_order - order need to change to
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_type = arg["card_type"]
        image_name = arg["image_name"]
        target_order = arg["target_order"]

        ##Click change order
        xpath = Util.GetXpath({"locate": "change_order_btn"}).replace("card_type_to_be_replace", card_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change order btn", "result": "1"})

        ##Drag and drop element
        xpath = Util.GetXpath({"locate": "image"}).replace("image_name_to_be_replace", image_name).replace("card_type_to_be_replace", card_type)
        xpath_target = Util.GetXpath({"locate": "label_order_target"}).replace("order_to_be_replace", target_order).replace("card_type_to_be_replace", card_type)
        BaseUICore.DragAndDrop({"drag_elem": xpath, "target_elem": xpath_target, "result": "1"})

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"}).replace("card_type_to_be_replace", card_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.ChangeOverlayImageOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemCardDisplayLabel(arg):
        '''
        EditItemCardDisplayLabel : Edit item card display label
                Input argu :
                    visibility - disabled / enabled / enabled_if_ongoing_flash_sale
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        visibility = arg["visibility"]

        ##Click visibility input block
        xpath = Util.GetXpath({"locate": "visibility_input"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visibility input block", "result": "1"})
        time.sleep(1)

        ##Choose visibility option
        xpath = Util.GetXpath({"locate": "visibility_option"}).replace("option_to_be_replaced", visibility.upper())
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visibility option", "result": "1"})
        time.sleep(1)

        ##Click save
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.EditItemCardDisplayLabel')


class AdminDeepDiscountPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToDeepDiscountEditPageByID(arg):
        '''
        GoToDeepDiscountEditPageByID : Go to Deep Discount edit page by id
                Input argu :
                    discount_id - deep discount id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        discount_id = arg["discount_id"]

        url = "https://admin." + GlobalAdapter.UrlVar._Domain_ + "/CMS/product/deep_discount/" + discount_id + "/"
        BaseUICore.GotoURL({"url": url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDeepDiscountPage.GoToDeepDiscountEditPageByID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveDeepDiscount(arg):
        '''
        ClickSaveDeepDiscount : Click Save button of deep discount setting
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Save button
        xpath = Util.GetXpath({"locate": "discount_save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDeepDiscountPage.ClickSaveDeepDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromotionID(arg):
        '''
        InputPromotionID : Input the reserved promotion id to deep discount setting
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Clear text field
        xpath = Util.GetXpath({"locate": "promotion_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})

        ##Get Promotion ID in form CLPR*** and reserve the numerical value
        promotion_id = GlobalAdapter.PromotionE2EVar._PromotionIDList_[0][4:]

        ##Input the reserved promotion id
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_id, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminDeepDiscountPage.InputPromotionID')


class AdminPreviewManagementPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadPreviewProduct(arg):
        '''
        UploadPreviewProduct : Upload preview product
                Input argu :
                    file_name - csv file with product
                    start_time - preview start time
                    end_time - preview end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        start_time = arg['start_time']
        end_time = arg['end_time']

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=int(end_time))

        ##Get the path of csv file and read the file
        slash = Config.dict_systemslash[Config._platform_]

        ##get the csv file from case file folder
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

        ##Read the file
        data_list = XtFunc.CsvReader(file_path)

        ##Modify preview start time
        XtFunc.CsvWritter({"input_row":"2", "input_column":"4", "value":start_time, "data_list":data_list, "file_path":file_path, "result":arg['result']})

        ##Read updated file again
        data_list = XtFunc.CsvReader(file_path)

        ##Modify preview end time
        XtFunc.CsvWritter({"input_row":"2", "input_column":"5", "value":end_time, "data_list":data_list, "file_path":file_path, "result":arg['result']})

        ##Upload file in input field
        xpath = Util.GetXpath({"locate": "input_file_path"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name":file_name, "file_type":"csv", "result":"1"})
        time.sleep(3)

        ##Click Upload button
        xpath = Util.GetXpath({"locate": "upload_csv_btn"})
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message":"Click Upload button", "result": "1"})

        BaseUILogic.HandleUnknownPopUpWindow({"expected_time": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPreviewManagementPage.UploadPreviewProduct -> preview start at ' + start_time + " and end at " + end_time)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeletePreviewProduct(arg):
        '''
        ClickDeletePreviewProduct : Click Delete Product in preview list
                Input argu :
                    item_id - item id want to delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']

        ##Click Delete button of the preview product
        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("item_id_to_be_replaced", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminPreviewManagementPage.ClickDeletePreviewProduct -> ' + item_id)
