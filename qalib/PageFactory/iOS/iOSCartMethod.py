﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSCartMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import FrameWorkBase
from Config import dumplogger
import Util
import DecoratorHelper

##Import ios library
import iOSBaseUICore
import iOSBaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class iOSCartPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckMakePurchaseStatus(arg):
        ''' CheckMakePurchaseStatus : Check made purchase status and retry 3 times if doesn't succeed
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        time.sleep(10)

        locate = Util.GetXpath({"locate":"order_detail_headline"})

        for retry_times in range(1, 4):
            if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"}):
                dumplogger.info("Make purchase successful!")
                break
            else:
                dumplogger.info("Make purchase unsuccessful, retry %s times" % (retry_times))
                iOSCartButton.ClickBuyNow({"result": "1"})
                if retry_times == 4:
                    ret = 0

        OK(ret, int(arg['result']), 'iOSCartPage.CheckMakePurchaseStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseSellerVoucher(arg):
        ''' ChooseSellerVoucher : Click use voucher btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click use btn
        xpath = Util.GetXpath({"locate":"use_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click use btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCartPage.ChooseSellerVoucher')


class iOSCartButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        ''' ClickBuyNow : Click buy now btn on Cart page
                Input argu :N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click buy now btn
        locate = Util.GetXpath({"locate":"purchase"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click purchase btn on cart page", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCartButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def TapsVoucherSection(arg):
        ''' TapsVoucherSection : Taps on voucher section in cart page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Taps on voucher section in cart page
        xpath = Util.GetXpath({"locate":"voucher_section"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click voucher section", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCartButton.TapsVoucherSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchShopeeCoinToggle(arg):
        ''' SwitchShopeeCoinToggle : Switch use shopee coin toggle
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Switch close/open for shopee coin toggle
        xpath = Util.GetXpath({"locate":"coin_toggle"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Switch close button to open", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCartButton.SwitchShopeeCoinToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAll(arg):
        ''' ClickSelectAll : In cart page click select all
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click select all checkbox
        xpath = Util.GetXpath({"locate":"select_all_checkbox"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click select all checkbox", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCartButton.ClickSelectAll')
