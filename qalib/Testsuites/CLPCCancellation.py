import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCCancellation Precondition =="
        dumplogger.info("== Setup CLPCCancellation Precondition ==")

    def test_CLPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCCancellation-01.xml")

    def test_CLPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCCancellation-02.xml")

    def test_CLPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCCancellation-03.xml")

    def test_CLPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCCancellation-04.xml")

    def test_CLPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCCancellation Post condition =="
        dumplogger.info("== Setup CLPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
