import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCForm(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCForm Precondition =="
        dumplogger.info("== Setup TWPCForm Precondition ==")

    def test_TWPCForm01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCForm-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCForm Post condition =="
        dumplogger.info("== Setup TWPCForm Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
