import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCFreeShippingVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCFreeShippingVoucher Precondition =="
        dumplogger.info("== Setup THPCFreeShippingVoucher Precondition ==")

    def test_THPCFreeShippingVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-01.xml")

    def test_THPCFreeShippingVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-02.xml")

    def test_THPCFreeShippingVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-03.xml")

    def test_THPCFreeShippingVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-04.xml")

    def test_THPCFreeShippingVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-05.xml")

    def test_THPCFreeShippingVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-06.xml")

    def test_THPCFreeShippingVoucher07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-07.xml")

    def test_THPCFreeShippingVoucher08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-08.xml")

    def test_THPCFreeShippingVoucher09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFreeShippingVoucher-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCFreeShippingVoucher Post condition =="
        dumplogger.info("== Setup THPCFreeShippingVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
