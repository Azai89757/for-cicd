import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCSignup Precondition =="
        dumplogger.info("== Setup ARPCSignup Precondition ==")

    def test_ARPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCSignup Post condition =="
        dumplogger.info("== Setup ARPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
