import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class MXPCTax(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCTax Precondition =="
        dumplogger.info("== Setup MXPCTax Precondition ==")

    def test_MXPCTax01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCTax-01.xml")

    def test_MXPCTax02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCTax-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCTax Post condition =="
        dumplogger.info("== Setup MXPCTax Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
