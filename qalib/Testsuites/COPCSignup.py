import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCSignup Precondition =="
        dumplogger.info("== Setup COPCSignup Precondition ==")

    def test_COPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCSignup Post condition =="
        dumplogger.info("== Setup COPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
