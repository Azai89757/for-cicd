import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class TWiOSOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWiOSOrder Precondition =="
        dumplogger.info("== Setup TWiOSOrder Precondition ==")

        platform = Config._TestCasePlatform_.lower()

        ##Get device id
        device_id = GetiOSDeviceId("iOS")
        print "Device id : %s" % (device_id)

        #Get device info by device id
        if device_id:
            ##Start Appium
            iOSStartAppium({"result": "1"})

            ##Get iOS version of this device
            device_version = GetiOSDeviceInfo()

            ##Call function from iOSBaseUICore
            print "Device connection check: Pass"
            print "Get device version: " + device_version

            ##Assign device platform version & device udid to global var
            Config._iOSDesiredCaps_['udid'] = device_id

            ##Call function from iOSBaseUICore
            iOSInitialDriver()

        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_TWiOSOrder001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-001.xml")

    def test_TWiOSOrder002(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-002.xml")

    def test_TWiOSOrder003(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-003.xml")

    def test_TWiOSOrder004(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-004.xml")

    def test_TWiOSOrder005(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-005.xml")

    def test_TWiOSOrder006(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-006.xml")

    def test_TWiOSOrder007(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-007.xml")

    def test_TWiOSOrder008(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-008.xml")

    def test_TWiOSOrder009(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSOrder-009.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWiOSOrder Post condition =="
        dumplogger.info("== Setup TWiOSOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
