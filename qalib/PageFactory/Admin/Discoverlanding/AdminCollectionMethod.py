#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCollectionMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminCampaignEntryPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateCampaignEntryAttribute(arg):
        '''
        CreateCampaignEntryAttribute : Create campaign entry attribute
                Input argu :
                    name - campaign entry collection name
                    description - campaign entry collection description
                    entry_type - calender / all
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        description = arg['description']
        entry_type = arg['entry_type']

        if name:
            ##Input collection name
            xpath = Util.GetXpath({"locate": "name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        if description:
            ##Input collection description
            xpath = Util.GetXpath({"locate": "description"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        if entry_type:
            ##Click choose collection type
            xpath = Util.GetXpath({"locate": entry_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose collection type -> " + entry_type, "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminCampaignEntryPage.CreateCampaignEntryAttribute')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerImage(arg):
        '''
        EditBannerImage : Edit banner image
                Input argu :
                    banner_stage - Schedule / Ongoing / Ended
                    image_type - banner /card
                    upload_type - file / image_hash
                    image_file - icon image file
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_stage = arg["banner_stage"]
        image_type = arg["image_type"]
        upload_type = arg["upload_type"]
        image_file = arg["image_file"]
        image_hash = arg["image_hash"]

        ##Upload icon image
        if upload_type == "upload_file":
            xpath = Util.GetXpath({"locate": image_type + "_file"})
            xpath = xpath.replace("string_to_be_replaced", banner_stage)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload button", "result": "1"})
            time.sleep(3)
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": image_file, "file_type": "png", "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":
            xpath = Util.GetXpath({"locate": image_type + "_hash"})
            xpath = xpath.replace("string_to_be_replaced", banner_stage)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
            dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminCampaignEntryPage.EditBannerImage')


class AdminAllCampaignConfigurationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateNewTab(arg):
        '''
        CreateNewTab : Create all campaign congiguration tab
                Input argu :
                    name - tab name
                    collection_id - campaign collection id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        collection_id = arg['collection_id']

        ##Click add new tab
        xpath = Util.GetXpath({"locate": "add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Add New button", "result": "1"})

        ##Input collection name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        ##Input collection id
        xpath = Util.GetXpath({"locate": "id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collection_id, "result": "1"})

        ##Click save
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAllCampaignConfigurationPage.CreateNewTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteTab(arg):
        '''
        DeleteTab : Delete all campaign congiguration tab
                Input argu :
                    name - tab name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        order_column = "1"

        ##Get and reserve campagin order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("name_to_be_replaced", name).replace("order_index_to_be_replaced", order_column)

        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to delete campagin
        xpath = Util.GetXpath({"locate": "delete_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete campagin", "result": "1"})
        time.sleep(2)

        ##Click double delete
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAllCampaignConfigurationPage.DeleteTab')


class AdminCustomizedCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPageBaseInfo(arg):
        '''
        EditPageBaseInfo : Edit customized collection landing page base information
                Input argu :
                    title -customized collection landing page title
                    description - customized collection landing page description
                    is_display_title - 1 / 0 / null(remain current status)
                    url - page url
                    group_id - collection group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg['title']
        description = arg['description']
        is_display_title = arg['is_display_title']
        url = arg['url']
        group_id = arg['group_id']

        if title:
            ##Input page title
            xpath = Util.GetXpath({"locate": "title"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        if description:
            ##Input page description
            xpath = Util.GetXpath({"locate": "description"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        if is_display_title:
            if int(is_display_title):
                xpath = Util.GetXpath({"locate": "is_display_title"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Enable display title", "result": "1"})
                    time.sleep(2)
            else:
                xpath = Util.GetXpath({"locate": "not_display_title"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Disable display title", "result": "1"})
                    time.sleep(2)

        if url:
            ##Input page url
            xpath = Util.GetXpath({"locate": "url"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        if group_id:
            ##Input collection group id
            xpath = Util.GetXpath({"locate": "group_id"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCustomizedCollectionPage.EditPageBaseInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditHeaderImage(arg):
        '''
        EditHeaderImage : Edit header image
                Input argu :
                    upload_type - upload image by file or image hash
                    image_file - header image file
                    file_type - image file type(eg.png,jpeg,gif)
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        image_file = arg["image_file"]
        file_type = arg["file_type"]
        image_hash = arg["image_hash"]

        ##Upload header image
        if upload_type == "upload_file":
            xpath = Util.GetXpath({"locate":"upload_file"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload button", "result": "1"})
            time.sleep(3)
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": image_file, "file_type": file_type, "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":
            xpath = Util.GetXpath({"locate": "upload_hash"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
            dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminCustomizedCollectionPage.EditHeaderImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddPageBaseInfo(arg):
        '''
        AddPageBaseInfo : New customized collection landing page base information
                Input argu :
                    title - customized collection landing page title
                    description - customized collection landing page description
                    is_display_title - 1 / 0 / null(remain current status)
                    collection_type - product / shop / campaign_entry / null(default product)
                    main_collection_id - main collection id
                    group_id - collection group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg['title']
        description = arg['description']
        is_display_title = arg['is_display_title']
        collection_type = arg['collection_type']
        main_collection_id = arg['main_collection_id']
        group_id = arg['group_id']

        ##Input page title
        xpath = Util.GetXpath({"locate": "title"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        if description:
            ##Input page description
            xpath = Util.GetXpath({"locate": "description"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        if is_display_title:
            if int(is_display_title):
                xpath = Util.GetXpath({"locate": "is_display_title"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Enable display title", "result": "1"})
                    time.sleep(2)
            else:
                xpath = Util.GetXpath({"locate": "not_display_title"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Disable display title", "result": "1"})
                    time.sleep(2)

        if collection_type:
            xpath = Util.GetXpath({"locate": collection_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Enable display title", "result": "1"})
            time.sleep(2)

        if main_collection_id:
            ##Input page main collection id
            xpath = Util.GetXpath({"locate": "main_collection_id"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": main_collection_id, "result": "1"})

        if group_id:
            ##Input collection group id
            xpath = Util.GetXpath({"locate": "group_id"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCustomizedCollectionPage.AddPageBaseInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteMultiTab(arg):
        '''
        DeleteMultiTab : Click button to delete multi tab
            Input argu :
                tab_order - input numver 1 ~ 10
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        tab_order = arg["tab_order"]

        ##Click on button
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace("order_to_be_replaced", tab_order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete multi tab: %s" % (tab_order), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCustomizedCollectionPage.DeleteMultiTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchMultiTab(arg):
        '''
        SwitchMultiTab : Click tab to switch multi tab
            Input argu :
                tab_order - input numver 1 ~ 10
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        tab_order = arg["tab_order"]

        ##Click on button
        xpath = Util.GetXpath({"locate": "tab"})
        xpath = xpath.replace("order_to_be_replaced", tab_order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Switch to  multi tab: %s" % (tab_order), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCustomizedCollectionPage.SwitchMultiTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditMultiTabImage(arg):
        '''
        EditMultiTabImage : Edit multi tab image
                Input argu :
                    image_type - icon / banner
                    tab_order - input numver 1 ~ 10
                    upload_type - upload_file / image_hash
                    image_file - header image file
                    file_type - image file type(eg.png,jpeg,gif)
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_type = arg["image_type"]
        upload_type = arg["upload_type"]
        image_file = arg["image_file"]
        file_type = arg["file_type"]
        image_hash = arg["image_hash"]
        tab_order = arg["tab_order"]

        ##Upload image file
        if upload_type == "upload_file":
            xpath = Util.GetXpath({"locate": image_type + "_file"})
            xpath = xpath.replace("order_to_be_replaced", str(int(tab_order)-1))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload button", "result": "1"})
            time.sleep(3)
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": image_file, "file_type": file_type, "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":
            xpath = Util.GetXpath({"locate": image_type})
            xpath = xpath.replace("order_to_be_replaced", tab_order)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
            dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminCustomizedCollectionPage.EditMultiTabImage')


class AdminCollectionComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCollectionComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column (for example, create_banner_name / create_banner_page_create_banner_time)
                input_content - input text
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCollectionComponent.InputToColumn')
