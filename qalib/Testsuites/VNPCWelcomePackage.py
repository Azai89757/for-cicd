import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCWelcomePackage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCWelcomePackage Precondition =="
        dumplogger.info("== Setup VNPCWelcomePackage Precondition ==")

    def test_VNPCWelcomePackage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCWelcomePackage-01.xml")

    def test_VNPCWelcomePackage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCWelcomePackage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCWelcomePackage Post condition =="
        dumplogger.info("== Setup VNPCWelcomePackage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
