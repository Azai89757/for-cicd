import unittest
from Config import dumplogger
from FrameWorkBase import *


class PLAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup PLAPIPlaceOrder Precondition ==")

    def test_PLAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/PLAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup PLAPIPlaceOrder Post condition ==")
