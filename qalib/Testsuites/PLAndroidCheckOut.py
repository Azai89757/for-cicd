import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class PLAndroidCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAndroidCheckOut Precondition =="
        dumplogger.info("== Setup PLAndroidCheckOut Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"pl"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    # def test_PLAndroidCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLAndroidCheckOut-01.xml")

    def test_PLAndroidCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLAndroidCheckOut-02.xml")

    def test_PLAndroidCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLAndroidCheckOut-03.xml")

    def test_PLAndroidCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLAndroidCheckOut-04.xml")

    def test_PLAndroidCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLAndroidCheckOut-05.xml")

    # def test_PLAndroidCheckOut06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLAndroidCheckOut-06.xml")

    def test_PLAndroidCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLAndroidCheckOut-07.xml")

    def test_PLAndroidCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLAndroidCheckOut-08.xml")

    # def test_PLAndroidCheckOut09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLAndroidCheckOut-09.xml")

    # def test_PLAndroidCheckOut10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLAndroidCheckOut-10.xml")

    # def test_PLAndroidCheckOut11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLAndroidCheckOut-11.xml")

    # def test_PLAndroidCheckOut12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLAndroidCheckOut-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAndroidCheckOut Post condition =="
        dumplogger.info("== Setup PLAndroidCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
