import unittest
from Config import dumplogger
from FrameWorkBase import *


class SGAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAPIShipment Precondition =="
        dumplogger.info("== Setup SGAPIShipment Precondition ==")

    def test_SGAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/SGAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAPIShipment Post condition =="
        dumplogger.info("== Setup SGAPIShipment Post condition ==")
