import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCMePage Precondition =="
        dumplogger.info("== Setup ESPCMePage Precondition ==")

    def test_ESPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCMePage Post condition =="
        dumplogger.info("== Setup ESPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
