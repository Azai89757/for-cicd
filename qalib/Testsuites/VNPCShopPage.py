import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCShopPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopPage Precondition =="
        dumplogger.info("== Setup VNPCShopPage Precondition ==")

    def test_VNPCShopPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopPage-01.xml")

    def test_VNPCShopPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopPage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopPage Post condition =="
        dumplogger.info("== Setup VNPCShopPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
