import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class MYAndroidMarketPlacePayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAndroidMarketPlacePayment Precondition =="
        dumplogger.info("== Setup MYAndroidMarketPlacePayment Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"my"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_MYAndroidMarketPlacePayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYAndroidMarketPlacePayment-01.xml")

    def test_MYAndroidMarketPlacePayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYAndroidMarketPlacePayment-02.xml")

    def test_MYAndroidMarketPlacePayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYAndroidMarketPlacePayment-03.xml")

    def test_MYAndroidMarketPlacePayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYAndroidMarketPlacePayment-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAndroidMarketPlacePayment Post condition =="
        dumplogger.info("== Setup MYAndroidMarketPlacePayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
