import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCLogin Precondition =="
        dumplogger.info("== Setup MXPCLogin Precondition ==")

    def test_MXPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCLogin-01.xml")

    def test_MXPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCLogin Post condition =="
        dumplogger.info("== Setup MXPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
