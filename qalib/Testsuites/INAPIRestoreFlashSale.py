import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INAPIRestoreFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INAPIRestoreFlashSale Precondition =="
        dumplogger.info("== Setup INAPIRestoreFlashSale Precondition ==")

    def test_INAPIRestoreFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/INAPIRestoreFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INAPIRestoreFlashSale Post condition =="
        dumplogger.info("== Setup INAPIRestoreFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
