import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCOrder Precondition =="
        dumplogger.info("== Setup PLPCOrder Precondition ==")

    def test_PLPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-01.xml")

    def test_PLPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-02.xml")

    def test_PLPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-03.xml")

    def test_PLPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-04.xml")

    def test_PLPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-05.xml")

    def test_PLPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-06.xml")

    def test_PLPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCOrder-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCOrder Post condition =="
        dumplogger.info("== Setup PLPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
