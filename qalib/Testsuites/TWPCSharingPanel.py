import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCSharingPanel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCSharingPanel Precondition =="
        dumplogger.info("== Setup TWPCSharingPanel Precondition ==")

    def test_TWPCSharingPanel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCSharingPanel-01.xml")

    def test_TWPCSharingPanel02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCSharingPanel-02.xml")

    def test_TWPCSharingPanel03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCSharingPanel-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCSharingPanel Post condition =="
        dumplogger.info("== Setup TWPCSharingPanel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
