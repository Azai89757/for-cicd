import unittest
from Config import dumplogger
from FrameWorkBase import *


class MYAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup MYAPIPlaceOrder Precondition ==")

    def test_MYAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MYAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup MYAPIPlaceOrder Post condition ==")
