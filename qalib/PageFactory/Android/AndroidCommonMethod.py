#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidCommonMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import framework common library
import Util
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidGoToHomePage(arg):
    '''
    AndroidGoToHomePage : Go to home page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Get Android shopee app home page url
    home_page_url = "https://" + GlobalAdapter.UrlVar._Domain_.replace(Config._EnvType_.lower() + ".", "") + " " + Config._DesiredCaps_['appPackage']
    dumplogger.info("home_page_url: %s" % (home_page_url))

    ##Use adb command send url to home page
    AndroidBaseUICore.SendADBCommand({"command_type":"IntentUrl", "adb_arg":home_page_url, "is_return": "0", "result": "1"})

    ##Check Home icon is shown
    xpath = Util.GetXpath({"locate":"home_title"})
    AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGoToHomePage')

@DecoratorHelper.FuncRecorder
def AndroidGoToPageByUrl(arg):
    '''
    AndroidGoToPageByUrl : Go to specific page by url
            Input argu :
                url - get url. NOTE: Please remove "staging" in front of url. EX: https://shopee.tw/Chat_Product_002-i.210045.1657212
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    url = arg["url"]
    ret = 1

    ##Use adb command to launch the product detail
    AndroidBaseUICore.SendADBCommand({"command_type":"IntentUrl", "adb_arg": url + ' ' + Config._DesiredCaps_['appPackage'] + '"', "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGoToPageByUrl')

@DecoratorHelper.FuncRecorder
def AndroidChangeDate(arg):
    '''
    AndroidChangeDate : Change date
            Input argu :
                year - year
                month - month
                day - day
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    year = arg["year"]
    month = arg["month"]
    day = arg["day"]
    month_table = {"1": "Jan", "2": "Feb", "3": "Mar", "4": "Apr", "5": "Mei", "6": "Jun", "7": "Jul", "8": "Agt", "9": "Sep", "10": "Okt", "11": "Nov", "12": "Des"}

    ##Input day (need to click the field both before and after input, to make sure input successfully)
    locate_day = Util.GetXpath({"locate":"locate_day"})
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_day, "message":"Click day", "result": "1"})
    AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate_day, "string":day, "result": "1"})

    ##Input month (need to click the field both before and after input, to make sure input successfully)
    locate_month = Util.GetXpath({"locate":"locate_month"})
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_month, "message":"Click month", "result": "1"})
    if Config._TestCaseRegion_ == "VN":
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate_month, "string":"thg " + month, "result": "1"})
    elif Config._TestCaseRegion_ == "ID":
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate_month, "string":month_table[month], "result": "1"})
    elif Config._TestCaseRegion_ == "TW":
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate_month, "string":month, "result": "1"})

    ##Input year (need to click the field both before and after input, to make sure input successfully)
    locate_year = Util.GetXpath({"locate":"locate_year"})
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_year, "message":"Click year", "result": "1"})
    AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate_year, "string":year, "result": "1"})
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_day, "message":"Click day", "result": "1"})

    ##Click finish button
    xpath = Util.GetXpath({"locate":"finish_button"})
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click finish button", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidChangeDate -> ' + year + '/' + month + '/' + day)

@DecoratorHelper.FuncRecorder
def AndroidClickProductCard(arg):
    '''
    AndroidClickProductCard : Click product name
            Input argu :
                product_name - product name
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    product_name = arg['product_name']

    ##Click product name
    xpath = Util.GetXpath({"locate":"product_name"})
    xpath = xpath.replace("product_name_to_be_replaced", product_name)
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click product name", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidClickProductCard')

@DecoratorHelper.FuncRecorder
def ReopenShopeeApp(arg):
    '''
    ReopenShopeeApp : Reopen app and refresh homepage
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Close APP and open
    time.sleep(5)
    AndroidBaseUICore.AndroidCloseShopeeAPP({"result": "1"})
    time.sleep(5)
    AndroidBaseUICore.AndroidOpenShopeeAPP({"result": "1"})
    time.sleep(30)
    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
    time.sleep(5)
    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
    time.sleep(10)

    OK(ret, int(arg['result']), 'ReopenShopeeApp')


class AndroidCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToPreviousPage(arg):
        '''
        ClickBackToPreviousPage : Click Back button
                Input argu :
                    page_type : Back button in specific page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Get xpath and click back button in specific page
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Back button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCommonButton.ClickBackToPreviousPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok button in popup (button text will be like: 'ĐỒNG Ý' ,'Đồng ý' ,'OK')
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button in popup message
        time.sleep(3)
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ok button in popup", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCommonButton.ClickOK')


class AndroidForbiddenZonePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NavigateUrl(arg):
        '''
        NavigateUrl : Navigate url to target page in Forbidden Zone
                Input argu :
                    url - Use full http url for web view or RN module name for RN page
                    NOTE: Please Add use full http url with "staging" for web view page or RN module name for RN page
                            http url: https://staging.shopee.vn/search?keyword=rcmd
                            RN module name : rn/@shopee-rn/seller-listing/PRODUCT_EDIT
                    page_type - web view or RN page. EX:rn_page or web_view
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        url = arg["url"]
        page_type = arg["page_type"]
        ret = 1

        ##Long press under bar
        xpath = Util.GetXpath({"locate":"long_press_btn"})
        AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})
        time.sleep(2)

        ##Scroll to navigate block
        xpath = Util.GetXpath({"locate":page_type + "_btn"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "result": "1"})
        AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": xpath, "ratio": "0.5", "result": "1"})

        ##input url to navigate
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":url, "result": "1"})

        ##Click navigate url
        xpath = Util.GetXpath({"locate":page_type + "_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click navigate url", "result": "1"})
        time.sleep(10)

        ##Handle unknown warning
        AndroidBaseUICore.AndroidHandleUnknownWarning({"result": "1"})

        OK(ret, int(arg['result']), 'AndroidForbiddenZonePage.NavigateUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToNativeFeaturesTest(arg):
        '''
        GoToNativeFeaturesTest : Go to native features test page in forbidden zone
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Long press under bar
        xpath = Util.GetXpath({"locate": "long_press_btn"})
        AndroidBaseUICore.AndroidLongPress({"locate": xpath, "sec": "3", "result": "1"})
        time.sleep(2)

        ##Scroll to Native Features Test block
        xpath = Util.GetXpath({"locate": "native_features_test_block"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "0", "result": "1"})

        ##Go to Native Features Test page
        xpath = Util.GetXpath({"locate": "native_features_test_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click native features test", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidForbiddenZonePage.GoToNativeFeaturesTest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToConfigFeatureToggle(arg):
        '''
        GoToConfigFeatureToggle : Go to config feature toggle page in native features test.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Scroll to Config Feature Toggle
        xpath = Util.GetXpath({"locate": "config_feature_toggle"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "0", "result": "1"})

        time.sleep(2)

        ##Scroll to above section
        xpath = Util.GetXpath({"locate": "log_history_section"})
        AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": xpath, "ratio": "0.3", "result": "1"})

        ##Go to Config Feature Toggle page
        xpath = Util.GetXpath({"locate": "config_feature_toggle"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click config feature toggle tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidForbiddenZonePage.GoToConfigFeatureToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetFeatureToggle(arg):
        '''
        SetFeatureToggle : Set feature toggle in config feature toggle page.
                Input argu :
                    toggle_name - which feature toggle you want to check open or close in config feature toggle page.
                    action - the toggle hope to open or close. EX: 1 will open and 0 will close.
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        toggle_name = arg["toggle_name"]
        action = arg["action"]

        ret = 1

        time.sleep(10)

        ##Click feature toggle seach bar
        xpath = Util.GetXpath({"locate": "toggle_search_bar"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click feature toggle seach bar", "result": "1"})

        ##input feature toggle
        xpath = Util.GetXpath({"locate": "toggle_search_bar_text"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": toggle_name, "result": "1"})

        ##check toggle status
        xpath = Util.GetXpath({"locate": "toggle_status_open"}).replace("toggle_name", toggle_name)

        ##if action not equals toggle now status, click toggle to change status
        if int(action) != AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "toggle_name_tab"}).replace("toggle_name", toggle_name)
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Change feature toggle status", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidForbiddenZonePage.SetFeatureToggle')
