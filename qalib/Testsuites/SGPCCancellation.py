import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCCancellation Precondition =="
        dumplogger.info("== Setup SGPCCancellation Precondition ==")

    def test_SGPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCancellation-01.xml")

    def test_SGPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCancellation-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCCancellation Post condition =="
        dumplogger.info("== Setup SGPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
