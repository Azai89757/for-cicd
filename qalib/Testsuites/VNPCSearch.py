import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCSearch(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSearch Precondition =="
        dumplogger.info("== Setup VNPCSearch Precondition ==")

    def test_VNPCSearch01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSearch-01.xml")

    def test_VNPCSearch02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSearch-02.xml")

    def test_VNPCSearch03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSearch-03.xml")

    def test_VNPCSearch04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSearch-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSearch Post condition =="
        dumplogger.info("== Setup VNPCSearch Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
