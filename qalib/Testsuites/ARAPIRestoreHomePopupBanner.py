import unittest
from Config import dumplogger
from FrameWorkBase import *


class ARAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup ARAPIRestoreHomePopupBanner Precondition ==")

    def test_ARAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/ARAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup ARAPIRestoreHomePopupBanner Post condition ==")
