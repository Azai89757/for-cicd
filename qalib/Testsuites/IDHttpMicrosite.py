import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpMicrosite Precondition =="
        dumplogger.info("== Setup IDHttpMicrosite Precondition ==")

    def test_IDHttpMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-01.xml")

    def test_IDHttpMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-02.xml")

    def test_IDHttpMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-03.xml")

    def test_IDHttpMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-04.xml")

    def test_IDHttpMicrosite05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-05.xml")

    def test_IDHttpMicrosite06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-06.xml")

    def test_IDHttpMicrosite07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-07.xml")

    def test_IDHttpMicrosite08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-08.xml")

    def test_IDHttpMicrosite09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-09.xml")

    def test_IDHttpMicrosite10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-10.xml")

    def test_IDHttpMicrosite11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-11.xml")

    def test_IDHttpMicrosite12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-12.xml")

    def test_IDHttpMicrosite13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-13.xml")

    def test_IDHttpMicrosite14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-14.xml")

    def test_IDHttpMicrosite15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-15.xml")

    def test_IDHttpMicrosite16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-16.xml")

    def test_IDHttpMicrosite17(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-17.xml")

    def test_IDHttpMicrosite18(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-18.xml")

    def test_IDHttpMicrosite19(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-19.xml")

    def test_IDHttpMicrosite20(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-20.xml")

    def test_IDHttpMicrosite21(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-21.xml")

    def test_IDHttpMicrosite22(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-22.xml")

    def test_IDHttpMicrosite23(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-23.xml")

    def test_IDHttpMicrosite24(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMicrosite-24.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpMicrosite Post condition =="
        dumplogger.info("== Setup IDHttpMicrosite Post condition ==")
