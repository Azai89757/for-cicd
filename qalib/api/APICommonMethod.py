#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 APICommonMethod.py: The def of this file called by other function.
'''
##Import system library
import json
import re

##Import common library
import FrameWorkBase
import Config
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def CheckAPIResponseCode(arg):
    ''' CheckAPIResponseCode : Check api response code to verify api result
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''

    ret = 1
    ##Store response status code spex api
    if "spex" in Config._TestCasePlatform_.lower():
        res_s = GlobalAdapter.APIVar._APIResponse_[0]
        message = "Verifing Spex API response code"
    ##Store response status cosde for http api
    else:
        res_s = GlobalAdapter.APIVar._APIResponse_["status"]
        message = "Verifing HTTP API response code"

    dumplogger.info("Expected result: %s" % (GlobalAdapter.APIVar._JsonCaseData_["response_status"]))
    dumplogger.info("Actual result: %s" % (res_s))

    ##Verify if status code matched
    if int(res_s) == int(GlobalAdapter.APIVar._JsonCaseData_["response_status"]):
        message += ": response code matched!!!"
    else:
        message += ": response code not matched!!!  %s != %s" % (res_s, GlobalAdapter.APIVar._JsonCaseData_["response_status"])
        ret = 0

    dumplogger.info(message)

    OK(ret, int(arg['result']), 'CheckAPIResponseCode->' + message)

def DisassembleDataWithAction(target_data, source_data, action, encoding="unicode", assign_value=""):
    ''' DisassembleDataWithAction : Disassemble data with different actions
            Input argu :
                target_data - the data provided to compare / replcae_data_type / assign_data / store_data
                source_data - for compare / store_data, it means api response of http / spex
                              for modify_data_type / assign_data, it means data defined in case .json file
                action - compare / modify_data_type / assign_data / store_data
                encoding - Optional, only when action == modify_data_type will be used, default value: unicode
                assign_value - Optional, only when action == assign_data will be used, default value: ""

            Return code :
                source_data - success for modify_data_type action
                1 - success for compare action
                0 - str / dict / list data not match : fail
                -1 - unexpected situation in data type / wrong data structure to match: fail
    '''
    source_type = type(source_data)
    target_type = type(target_data)
    dumplogger.info("Type of source_data for this layer: %s" % (source_type))
    dumplogger.info("target_data for this layer: %s" % (target_data))
    dumplogger.info("Type of target_data for this layer: %s" % (target_type))
    dumplogger.info("source_data for this layer: %s" % (source_data))

    ##If source_data is dictionary
    if source_type is dict:
        for key, value in source_data.iteritems():
            try:
                ##If target_data is not int or str, means it has more layer to disassemble -> to compare
                if target_type is dict:
                    dumplogger.info("Type of target_data == dict, means it has more layer to disassemble -> to compare")
                    ##If key in target_data, means they are in the same layer
                    dumplogger.info("Key of this layer : %s" % (key))
                    Config._HttpResponseTextKey_ = key
                    if key in target_data.iterkeys():
                        result = DisassembleDataWithAction(target_data[key], value, action, encoding, assign_value)
                        ##If compare failed or encounter exception, return failed result
                        if result != 1:
                            return result
                    else:
                        dumplogger.info("this means keys are not found in this layer of dict")
                        return 0

                ##If type of value in this layer is str/unicode and target_data equal to key, means it mathed the requirement, start to replace/modify/store/assign
                elif (type(value) is str or type(value) is unicode or type(value) is int or type(value) is long) and (target_data == key):
                    dumplogger.info("Type of value in this layer is str/unicode and target_data==key -> to replace/modify/store/assign")
                    ##Only modify_data_type in this section is valid
                    if action == "modify_data_type":
                        dumplogger.info("Only modify_data_type in this section is valid")
                        if encoding == "byte":
                            source_data[key] = str(value)
                        elif encoding == "unicode":
                            source_data[key] = source_data[key].decode('utf-8')

                        ##Return modified data
                        dumplogger.info("Return modified data: %s" % (source_data))
                        return source_data

                    ##Only store_data in this section is valid
                    elif action == "store_data":
                        dumplogger.info("Key matched!!! Store to global now...")
                        GlobalAdapter.CommonVar._DynamicCaseData_[key] = value
                        return 1

                    ##Only assign_data in this section is valid
                    elif action == "assign_data":
                        dumplogger.info("Only assign_data in this section is valid")

                        match_str = re.search("str_to_be_replaced", source_data[key], re.MULTILINE | re.IGNORECASE)
                        match_int = re.search("int_to_be_replaced", source_data[key], re.MULTILINE | re.IGNORECASE)
                        match_list_int = re.search("list_int_to_be_replaced", source_data[key], re.MULTILINE | re.IGNORECASE)
                        match_list_str = re.search("list_str_to_be_replaced", source_data[key], re.MULTILINE | re.IGNORECASE)
                        match_list_entire = re.search("list_entire_to_be_replaced", source_data[key], re.MULTILINE | re.IGNORECASE)
                        ##If source_data contains str_to_be_replaced or int_to_be_replaced or list_int_to_be_replaced or list_str_to_be_replaced pattern
                        if value == "str_to_be_replaced":
                            source_data[key] = str(assign_value)
                            dumplogger.info("Replacing str... %s" % (source_data[key]))
                            return source_data
                        elif value == "int_to_be_replaced":
                            ##Check assign value is int/long/string or not, if type is int(include 0), assign integer to data, or assign None to data
                            if type(assign_value) is int or type(assign_value) is long or type(assign_value) is str:
                                source_data[key] = int(assign_value)
                                dumplogger.info("Replacing int...%d" % (source_data[key]))
                            else:
                                source_data[key] = None
                                dumplogger.info("Replacing int to none")
                            return source_data
                        elif value == "list_int_to_be_replaced":
                            source_data[key] = []
                            source_data[key].append(int(assign_value))
                            dumplogger.info("Replacing list_int...%s" % (source_data[key]))
                            return source_data
                        elif value == "list_str_to_be_replaced":
                            source_data[key] = []
                            source_data[key].append(str(assign_value))
                            dumplogger.info("Replacing list_str...%s" % (source_data[key]))
                            return source_data
                        elif value == "list_entire_to_be_replaced":
                            source_data[key] = []
                            source_data[key].extend(assign_value)
                            dumplogger.info("Replacing list_str...%s" % (source_data[key]))
                            return source_data
                        if match_str:
                            source_data[key] = re.sub("str_to_be_replaced", "\"" + assign_value + "\"", source_data[key])
                            dumplogger.info("Replacing str... %s" % (source_data[key]))
                            return source_data
                        elif match_int:
                            source_data[key] = re.sub("int_to_be_replaced", assign_value, source_data[key])
                            dumplogger.info("Replacing int...%d" % (source_data[key]))
                            return source_data
                        elif match_list_int:
                            source_data[key] = re.sub("list_int_to_be_replaced", assign_value, source_data[key])
                            dumplogger.info("Replacing list_int...%s" % (source_data[key]))
                            return source_data
                        elif match_list_str:
                            source_data[key] = re.sub("list_str_to_be_replaced", assign_value, source_data[key])
                            dumplogger.info("Replacing list_str...%s" % (source_data[key]))
                            return source_data
                        elif match_list_entire:
                            source_data[key] = re.sub("list_entire_to_be_replaced", assign_value, source_data[key])
                            dumplogger.info("Replacing list_str...%s" % (source_data[key]))
                            return source_data
                        else:
                            dumplogger.info("Not the value to be expected, proceed.")

                    ##Unexpected scenario, return fail directly
                    else:
                        dumplogger.info("Unexpected scenario, return fail directly")
                        return 0

                ##If value of source_data is dict/list/tuple, means it has more layer to disassemble -> to replace data in deeper layers
                elif type(value) is list or type(value) is tuple or type(value) is dict:
                    ##If target_data matched the key, it means the data may inside in this layer, it's an special scenario
                    if (target_data == key) and (type(value) is list or type(value) is tuple) and (action == "store_data"):
                        dumplogger.info("Key matched!!! Store to global now...")
                        GlobalAdapter.CommonVar._DynamicCaseData_[key] = value[0]
                        return 1
                    else:
                        dumplogger.info("More layer to disassemble -> to replace data / assign data / store data in deeper layers")
                        result = DisassembleDataWithAction(target_data, value, action, encoding, assign_value)
                        dumplogger.info("result: %s" % (result))
                        ##If result of this layer is 1, means action success, will return 1 except for assign_data
                        if result == 1 and action != "assign_data":
                            return 1
                        ##If result of this layer is not integer, means it retured source_data, therefore source_data has been modified successfully
                        elif type(result) is not int:
                            return source_data

            ##Exception Handle
            except KeyError:
                dumplogger.exception("Encounter Key Error!!!")
                return -1
            except:
                dumplogger.exception("Encounter Other Error!!!")
                return -1

        ##Return pass if all dictionary for this layer is correct
        if action == "compare":
            dumplogger.info("All dictionary for this layer is correct")
            return 1
        ##Return fail if all dictionary for this layer has not found any matched keys
        else:
            dumplogger.info("All dictionary for this layer has not found any matched keys...")
            return 0

    ##If source_data is list or tuple
    elif source_type is list or source_type is tuple:
        #source_data = sorted(source_data)
        for list_index, list_item in enumerate(source_data):
            try:
                if target_type == source_type:
                    #target_data = sorted(target_data)
                    ##Compare list item first, if list item in this layer is wrong, then no need to go to next layer.
                    if list_item in target_data:
                        dumplogger.info("List data matched, go deeper in next layer.")
                        result = DisassembleDataWithAction(target_data[target_data.index(list_item)], list_item, action, encoding, assign_value)
                        ##If compare failed or encounter exception, return failed result
                        if result != 1:
                            return result

                    ##List data not match, but it's a dictionary in same index
                    elif list_item not in target_data and type(list_item) is dict and type(target_data[list_index]) is dict:
                        dumplogger.info("List data not match, but it's a dictionary in same index.")
                        result = DisassembleDataWithAction(target_data[list_index], list_item, action, encoding, assign_value)
                        ##If compare failed or encounter exception, return failed result
                        if result != 1:
                            return result

                    ##List data not match, it's a failed case.
                    else:
                        dumplogger.info("Data in list not match!")
                        dumplogger.info("Index of data in list: %d " % (list_index))
                        dumplogger.info("Data to be verified: %s " % (list_item))
                        Config._HttpResponseValue_ = "Compare failed. Index of data in list: [%s] and data to be verified: [%s]" % (list_index, list_item)
                        return 0

                ##Fore assign_data
                elif action == "assign_data":
                    result = DisassembleDataWithAction(target_data, list_item, action, encoding, assign_value)
                    if result != 0:
                        return result

                ##For not compared scenario, will go to next layer to see if there is more layer to dig up
                elif DisassembleDataWithAction(target_data, list_item, action, encoding, assign_value) == 1:
                    dumplogger.info("list layer not compared")
                    return 1

                ##Structure of target_data and source_data is not equal, return fail
                else:
                    dumplogger.info("Structure of target_data and source_data is not equal!")
                    return 0

            ##Exception Handle
            except IndexError:
                dumplogger.exception("Encounter Index Error!!!")
                return -1
            except:
                dumplogger.exception("Encounter Other Error!!!")
                return -1

        ##Return pass if all dictionary for this layer is correct for both compare / modify_data_type
        if action == "store_data":
            return 0
        else:
            return 1

    ##If source_data is string / integer / float, it means start comparing values
    elif source_type is str or source_type is unicode or source_type is int or source_type is float or source_type is bool or source_data is None or source_type is long:
        ##Only compare action enter this section is valid
        if action == "compare":
            dumplogger.info("Only compare in this section is valid")
            if target_data == source_data:
                dumplogger.info("Value match: %s = %s" % (source_data, target_data))
                return 1
            else:
                Config._HttpResponseValue_ = "%s != %s" % (source_data, target_data)
                dumplogger.info("Value not match: %s = %s" % (source_data, target_data))
                return 0

        ##Only assign_data in this section is valid
        elif action == "assign_data":
            dumplogger.info("Only assign_data in this section is valid")
            final_data = ""
            regex_str = r"(\"" + target_data + "\")(\W+)(str_to_be_replaced)"
            regex_int = r"(\"" + target_data + "\")(\W+)(int_to_be_replaced)"
            regex_simple_str = r"(str_to_be_replaced)"
            match_str = re.search(regex_str, source_data, re.MULTILINE | re.IGNORECASE)
            match_int = re.search(regex_int, source_data, re.MULTILINE | re.IGNORECASE)
            match_simple_str = re.search(regex_simple_str, source_data, re.MULTILINE | re.IGNORECASE)
            ##If source_data contains str_to_be_replaced or int_to_be_replaced pattern
            if match_str:
                final_data = "\"" + target_data + "\":\"" + assign_value + "\""
                source_data = re.sub(regex_str, final_data, source_data)
                return source_data
            elif match_int:
                final_data = "\"" + target_data + "\":" + assign_value
                source_data = re.sub(regex_int, final_data, source_data)
                return source_data
            elif match_simple_str:
                source_data = source_data.replace("str_to_be_replaced", str(assign_value))
                return source_data
            else:
                ##Unexpected data type to handle
                dumplogger.info("Data type: %s => Unexpected data to handle, will skip this layer..." % (source_data))
                return 1

        ##Unexpected scenario, return fail directly
        elif action == "modify_data_type":
            dumplogger.info("Please make sure if you have correct data structure and valid action type!!!")
            return 0

    else:
        ##Unexpected data type to handle
        dumplogger.info("Data type: %s => Unexpected data type to handle!!!" % (type(source_data)))
        return 0

@DecoratorHelper.FuncRecorder
def CheckAPIResponseText(arg):
    ''' CheckAPIResponseText : Check api response code to verify api result
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - str / dict / list data not match : fail
                    -1 - unexpected situation in data type / data structure: fail
    '''

    message = ""
    ret = 1
    ##Store response text for spex api
    if "spex" in Config._TestCasePlatform_.lower():
        res_t = GlobalAdapter.APIVar._APIResponse_[1]
    ##Store response text for http api
    else:
        try:
            ##transform str to dict
            res_t = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        except ValueError:
            ##If encounter value error, then means the response can't be transform correctly
            res_t = GlobalAdapter.APIVar._APIResponse_["text"]

    if res_t:
        dumplogger.info("ResponseText = %s" % (res_t))
        ret = DisassembleDataWithAction(res_t, GlobalAdapter.APIVar._JsonCaseData_["response_text"], "compare")
        if ret == 0:
            message = "Compare failed. Please check your json key [%s] => " % (Config._HttpResponseTextKey_) + Config._HttpResponseValue_
    elif not GlobalAdapter.APIVar._JsonCaseData_["response_text"]:
        dumplogger.info("Both API response and response_text defined in Json is emtpy, can skip this check!!!")
    else:
        ret = 0
        message = "There is no text in response, please check your content : %s" % str(GlobalAdapter.APIVar._APIResponse_)
        dumplogger.info(message)

    OK(ret, int(arg['result']), 'CheckAPIResponseText->' + message)

@DecoratorHelper.FuncRecorder
def ModifyPayloadDataType(arg):
    ''' ModifyPayloadDataType : Modify Payload data type to unicode or byte
                Input argu :
                    column - value of data need to be changed of it's type
                    encoding - unicode / byte, choose a encoding type of your column
                Return code :
                    1 - success
                    0 - wrong target type or failed to modify data type : fail
                    -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1
    column = arg["column"]
    encoding = arg["encoding"]

    ##Modify data of target column of spex api payload
    if "spex" in Config._TestCasePlatform_.lower():
        GlobalAdapter.APIVar._SpexReq_ = DisassembleDataWithAction(column, GlobalAdapter.APIVar._SpexReq_, "modify_data_type", encoding)
        if GlobalAdapter.APIVar._SpexReq_ == 1 or GlobalAdapter.APIVar._SpexReq_ == -1:
            ret = GlobalAdapter.APIVar._SpexReq_
        dumplogger.info("GlobalAdapter.APIVar._SpexReq_ = %s" % (GlobalAdapter.APIVar._SpexReq_))

    ##Modify data of target column of http api payload
    else:
        GlobalAdapter.APIVar._HttpPayload_ = DisassembleDataWithAction(column, GlobalAdapter.APIVar._HttpPayload_, "modify_data_type", encoding)
        if GlobalAdapter.APIVar._HttpPayload_ == 1 or GlobalAdapter.APIVar._HttpPayload_ == -1:
            ret = GlobalAdapter.APIVar._HttpPayload_
        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))

    OK(ret, int(arg['result']), 'ModifyPayloadDataType -> ' + column + " => To the string type: " + encoding)

@DecoratorHelper.FuncRecorder
def StoreDataFromAPIResponse(arg):
    ''' StoreDataFromAPIResponse : Store data from API response to global
                Input argu :
                    column - value of data need to be stored into global
                Return code :
                    1 - success
                    0 - wrong target type or failed to store data : fail
                    -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1
    column = arg["column"]

    ##Store response text for spex api
    if "spex" in Config._TestCasePlatform_.lower():
        res_t = GlobalAdapter.APIVar._APIResponse_[1]
    ##Store response text for http api
    else:
        res_t = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

    dumplogger.info(res_t)

    ret = DisassembleDataWithAction(column, res_t, "store_data")
    dumplogger.info("GlobalAdapter.CommonVar._DynamicCaseData_ = %s" % (GlobalAdapter.CommonVar._DynamicCaseData_))

    OK(ret, int(arg['result']), 'StoreDataFromAPIResponse -> ' + column + ' stored inside: ' + str(GlobalAdapter.CommonVar._DynamicCaseData_))

@DecoratorHelper.FuncRecorder
def AssignDataToAPIRequest(arg):
    ''' AssignDataToAPIRequest : Assign data to api request
                Input argu :
                    assign_to - headers / query_string_parameters / payload / req / response_text
                    source - value of which data already stored in global variable
                    target - value of data need to be assigned from global variable
                    time_deviation (Seconds) - time period interval to allow deviation,
                                     e.g. if time_deviation = "-60", then the time period to be assigned should be current time -60 seconds
                                          if time_deviation = "0", then the time period to be assigned should be current time +0 seconds
                                          if time_deviation = "", means you don't need to assign current time with deviation but from GlobalAdapter.CommonVar._DynamicCaseData_[source]
                Return code :
                    1 - success
                    0 - wrong target type or failed to assign data : fail
                    -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1
    assign_to = arg["assign_to"]
    source = arg["source"]
    target = arg["target"]
    time_deviation = arg["time_deviation"]

    if source == "cur_time":
        ##Assign global variable with fixed timestamp, please make sure to call GetCurrentTimeStamp first
        dumplogger.info("GlobalAdapter.TrackerVar._CurUnixTime_ = %s" % (GlobalAdapter.TrackerVar._CurUnixTime_))
        if target in ("auto_update_end_time", "start_time", "end_time", "svs_order_id", "source_id", "reason_code_id", "reason_code_ids", "task_name", "task_json_config_list", "start", "end"):
            ##Assign timestamp (sec) to request
            data_assigned = str(int(GlobalAdapter.TrackerVar._CurUnixTime_) + int(time_deviation))
        else:
            ##Assign timestamp (mill-sec) to request
            data_assigned = str(int(GlobalAdapter.TrackerVar._CurUnixTime_) + int(time_deviation)) + "000"
    else:
        ##Assign other data but not timestamp, to assign from global, please make sure to call StoreDataFromAPIResponse first
        data_assigned = GlobalAdapter.CommonVar._DynamicCaseData_[source]

    ##Assign data for headers
    if assign_to == "headers":
        GlobalAdapter.APIVar._HttpHeaders_ = DisassembleDataWithAction(target, GlobalAdapter.APIVar._HttpHeaders_, "assign_data", assign_value=data_assigned)
        dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_ = %s" % (GlobalAdapter.APIVar._HttpHeaders_))
        if GlobalAdapter.APIVar._HttpHeaders_ == 0 or GlobalAdapter.APIVar._HttpHeaders_ == -1:
            ret = GlobalAdapter.APIVar._HttpHeaders_

    ##Assign data for query_string_parameters
    elif assign_to == "query_string_parameters":
        GlobalAdapter.APIVar._HttpUrlParameter_ = DisassembleDataWithAction(target, GlobalAdapter.APIVar._HttpUrlParameter_, "assign_data", assign_value=data_assigned)
        dumplogger.info("GlobalAdapter.APIVar._HttpUrlParameter_ = %s" % (GlobalAdapter.APIVar._HttpUrlParameter_))
        if GlobalAdapter.APIVar._HttpUrlParameter_ == 0 or GlobalAdapter.APIVar._HttpUrlParameter_ == -1:
            ret = GlobalAdapter.APIVar._HttpUrlParameter_

    ##Assign data for payload
    elif assign_to == "payload":
        GlobalAdapter.APIVar._HttpPayload_ = DisassembleDataWithAction(target, GlobalAdapter.APIVar._HttpPayload_, "assign_data", assign_value=data_assigned)
        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))
        if GlobalAdapter.APIVar._HttpPayload_ == 0 or GlobalAdapter.APIVar._HttpPayload_ == -1:
            ret = GlobalAdapter.APIVar._HttpPayload_

    ##Assign data for req
    elif assign_to == "req":
        GlobalAdapter.APIVar._SpexReq_ = DisassembleDataWithAction(target, GlobalAdapter.APIVar._SpexReq_, "assign_data", assign_value=data_assigned)
        dumplogger.info("GlobalAdapter.APIVar._SpexReq_ = %s" % (GlobalAdapter.APIVar._SpexReq_))
        if GlobalAdapter.APIVar._SpexReq_ == 0 or GlobalAdapter.APIVar._SpexReq_ == -1:
            ret = GlobalAdapter.APIVar._SpexReq_

    ##Assign data for response text
    elif assign_to == "response_text":
        GlobalAdapter.APIVar._JsonCaseData_["response_text"] = DisassembleDataWithAction(target, GlobalAdapter.APIVar._JsonCaseData_["response_text"], "assign_data", assign_value=data_assigned)
        dumplogger.info("GlobalAdapter.APIVar._JsonCaseData_[\"response_text\"] = %s" % (GlobalAdapter.APIVar._JsonCaseData_["response_text"]))
        if GlobalAdapter.APIVar._JsonCaseData_["response_text"] == 0 or GlobalAdapter.APIVar._JsonCaseData_["response_text"] == -1:
            ret = GlobalAdapter.APIVar._JsonCaseData_["response_text"]

    else:
        dumplogger.info("Please assign correct assign_to!!!")
        ret = 0

    OK(ret, int(arg['result']), 'AssignDataToAPIRequest -> ' + target + ': ' + str(data_assigned))

@DecoratorHelper.FuncRecorder
def CheckAPITimeStamp(arg):
    ''' CheckAPITimeStamp : Check api time stamp of response code to verify api result
                Input argu :
                    column - target key and value to be verified
                    time_interval (Seconds) - time period interval to allow deviation,
                                    e.g. if time_interval = 60, then the time period to be verified should be +-60 seconds
                Return code :
                    1 - success
                    0 - str / dict / list data not match : fail
                    -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1
    column = arg["column"]
    time_interval = arg["time_interval"]
    api_time = GlobalAdapter.CommonVar._DynamicCaseData_[column]

    ##Modify timestamp format
    cur_time = str(GlobalAdapter.TrackerVar._CurUnixTime_) + "000"

    ##Get api timestamp from api request
    dumplogger.info("api time got from response: %d" % (api_time))
    if ret == 1:
        dumplogger.info("cur_time: " + cur_time)
        ##time_interval * 1000 means the unit is mini-seconds
        if int(api_time) >= int(cur_time) and int(api_time) < int(cur_time) + int(time_interval)*1000:
            dumplogger.info("Timestamp: %s matched in api response with deviation of %s seconds!!!" % (api_time, time_interval))
            ret = 1
        else:
            dumplogger.info("Timestamp: %s NOT matched in api response with deviation of %s seconds!!!" % (api_time, time_interval))
            ret = 0
    else:
        dumplogger.info("Get time stamp from api response failed...please check debug log !!!")

    OK(ret, int(arg['result']), 'CheckAPITimeStamp')

@DecoratorHelper.FuncRecorder
def ChangePayloadDictToStr(arg):
    ''' ChangePayloadDictToStr : change the payload type to json string
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - failed
                    -1 - TypeError when changing payload format / Others: fail
    '''
    ret = 1

    try:
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        dumplogger.info("json.dumps(GlobalAdapter.APIVar._HttpPayload_) success!")

    except TypeError:
        dumplogger.exception("Encounter TypeError when json.dumps(GlobalAdapter.APIVar._HttpPayload_)!!!")
        ret = -1

    except:
        dumplogger.exception("Encounter unknown Error when json.dumps(GlobalAdapter.APIVar._HttpPayload_)!!!")
        ret = -1

    OK(ret, int(arg['result']), 'ChangePayloadDictToStr')

@DecoratorHelper.FuncRecorder
def CalculateCountFromStoredData(arg):
    ''' CalculateCountFromStoredData : Calculate count data from stored data
                Input argu :
                    column - value of data has been stored into global
                    count_adjustment - value of adjustment according to response
                Return code :
                    1 - success
                    0 - wrong target type or failed to store data : fail
                    -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1
    column = arg["column"]
    count_adjustment = arg["count_adjustment"]

    ##Modify stored data
    GlobalAdapter.CommonVar._DynamicCaseData_[column] += int(count_adjustment)
    dumplogger.info("New GlobalAdapter.CommonVar._DynamicCaseData_ = %s" % (GlobalAdapter.CommonVar._DynamicCaseData_))

    OK(ret, int(arg['result']), 'ModifyStoreData -> ' + str(GlobalAdapter.CommonVar._DynamicCaseData_))

@DecoratorHelper.FuncRecorder
def CheckAPIResponseHeaders(arg):
    '''
    CheckAPIResponseHeaders : Check api response header to verify api result
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - wrong data
                    -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1

    res_h = GlobalAdapter.APIVar._APIResponse_["headers"]
    dumplogger.info("ResponseHeaders = %s" % (res_h))

    ret = DisassembleDataWithAction(res_h, GlobalAdapter.APIVar._JsonCaseData_["response_headers"], "compare")

    OK(ret, int(arg['result']), 'CheckAPIResponseHeaders')
