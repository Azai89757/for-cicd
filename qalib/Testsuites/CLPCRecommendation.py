import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class CLPCRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCRecommendation Precondition =="
        dumplogger.info("== Setup CLPCRecommendation Precondition ==")

    def test_CLPCRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCRecommendation-01.xml")

    def test_CLPCRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCRecommendation-02.xml")

    def test_CLPCRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCRecommendation-03.xml")

    def test_CLPCRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCRecommendation-04.xml")

    def test_CLPCRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCRecommendation-05.xml")

    def test_CLPCRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCRecommendation-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCRecommendation Post condition =="
        dumplogger.info("== Setup CLPCRecommendation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
