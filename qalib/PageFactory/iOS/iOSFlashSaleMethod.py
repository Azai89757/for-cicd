﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSFlashSaleMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import iOSBaseUICore
import iOSBaseUILogic
import DecoratorHelper

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class iOSMainPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoFlashSalePage(arg):
        ''' GotoFlashSalePage : Goto flash sale page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"view_all_flash_sale"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":xpath, "message":"Goto flash sale page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMainPage.GotoFlashSalePage')

class iOSFlashSalePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleProduct(arg):
        ''' ClickFlashSaleProduct : Click product on flash sale page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        xpath = Util.GetXpath({"locate":"product"}).replace("product_name_to_be_replace", product_name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click product on flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFlashSalePage.ClickFlashSaleProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareFlashSale(arg):
        ''' ClickShareFlashSale : Click share flash sale
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        iOSBaseUICore.iOSClickCoordinates({"x_cor":"296", "y_cor": "44", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFlashSalePage.ClickShareFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseSharePanel(arg):
        ''' ClickCloseSharePanel : Click share flash sale panel
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"confirm"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click close share panel on flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFlashSalePage.ClickCloseSharePanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemindMe(arg):
        ''' ClickRemindMe : Click remind me
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        iOSBaseUICore.iOSClickCoordinates({"x_cor":"262", "y_cor": "302", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFlashSalePage.ClickRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelRemindMe(arg):
        ''' CancelRemindMe : Cancel remind me
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"cancel_remind"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Cancel remind me flash sale product", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFlashSalePage.CancelRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        ''' ClickCategory : Click category
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"category"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click category", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFlashSalePage.ClickCategory')
