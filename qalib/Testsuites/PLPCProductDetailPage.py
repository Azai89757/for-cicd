import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCProductDetailPage Precondition =="
        dumplogger.info("== Setup PLPCProductDetailPage Precondition ==")

    def test_PLPCProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductDetailPage-01.xml")

    def test_PLPCProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductDetailPage-02.xml")

    def test_PLPCProductDetailPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductDetailPage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCProductDetailPage Post condition =="
        dumplogger.info("== Setup PLPCProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
