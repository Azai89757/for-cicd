
import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCancellation Precondition =="
        dumplogger.info("== Setup IDPCCancellation Precondition ==")

    def test_IDPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCancellation-01.xml")

    def test_IDPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCancellation-02.xml")

    def test_IDPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCancellation-03.xml")

    def test_IDPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCancellation-04.xml")

    def test_IDPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCancellation Post condition =="
        dumplogger.info("== Setup IDPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
