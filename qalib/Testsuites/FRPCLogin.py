import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCLogin Precondition =="
        dumplogger.info("== Setup FRPCLogin Precondition ==")

    def test_FRPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCLogin-01.xml")

    def test_FRPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCLogin Post condition =="
        dumplogger.info("== Setup FRPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
