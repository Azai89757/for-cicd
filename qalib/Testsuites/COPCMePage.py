import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCMePage Precondition =="
        dumplogger.info("== Setup COPCMePage Precondition ==")

    def test_COPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCMePage Post condition =="
        dumplogger.info("== Setup COPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
