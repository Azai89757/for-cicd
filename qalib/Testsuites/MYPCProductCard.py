import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class MYPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCProductCard Precondition =="
        dumplogger.info("== Setup MYPCProductCard Precondition ==")

    def test_MYPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCard-01.xml")

    def test_MYPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCard-02.xml")

    def test_MYPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCard-03.xml")

    def test_MYPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCard-04.xml")

    def test_MYPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCard-05.xml")

    def test_MYPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCProductCard Post condition =="
        dumplogger.info("== Setup MYPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
