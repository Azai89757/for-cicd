import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCHomePage Precondition =="
        dumplogger.info("== Setup FRPCHomePage Precondition ==")

    def test_FRPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCHomePage-01.xml")

    def test_FRPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCHomePage-02.xml")

    '''def test_FRPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCHomePage Post condition =="
        dumplogger.info("== Setup FRPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
