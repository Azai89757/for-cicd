#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminOfficialShopMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import os
import re
import time
import datetime

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import GlobalAdapter

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminOfficialShopCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeShopPositon(arg):
        '''
        ChangeShopPositon : Change official shop position
            Input argu :
                page_type - shop_category_detail / official_shop_summary
                category_name - category name
                shop_name - shop name
                position - position
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        page_type = arg["page_type"]
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]
        position = arg["position"]

        ##Click change position button
        AdminOfficialShopCommonButton.ClickChangePosition({"page_type":page_type, "category_name":category_name, "shop_name":shop_name, "result": "1"})

        ##Input position
        AdminOfficialShopCommonButton.InputPosition({"position":position, "result": "1"})

        ##Click confirm
        AdminOfficialShopCommonButton.ClickConfirm({"result": "1"})

        ##Click close
        AdminOfficialShopCommonButton.ClickClose({"result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommon.ChangeShopPositon')

class AdminOfficialShopCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePosition(arg):
        '''
        ClickChangePosition : Click change position
            Input argu :
                page_type - shop_category_detail / official_shop_summary
                category_name - category name
                shop_name - shop name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        page_type = arg["page_type"]
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]

        if page_type == "shop_category_detail":
            ##Get and reserve official shop order
            order_xpath = Util.GetXpath({"locate": "shop_icon_order"})
            order_xpath = order_xpath.replace("category_to_be_replaced", category_name).replace("shop_to_be_replaced", shop_name)
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        elif page_type == "official_shop_summary":
            ##Get and reserve official shop category order
            order_xpath = Util.GetXpath({"locate": "shop_category_icon_order"})
            order_xpath = order_xpath.replace("category_to_be_replaced", category_name)
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click change postion button
        xpath = Util.GetXpath({"locate": "change_position_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change position button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickChangePosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPosition(arg):
        '''
        InputPosition : Input position
            Input argu :
                position - position
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        position = arg["position"]

        ##Input position
        xpath = Util.GetXpath({"locate": "position_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": position, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.InputPosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click confirm
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click close
        xpath = Util.GetXpath({"locate": "close"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRedirect(arg):
        '''
        ClickRedirect : Click redirect
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click redirect
        xpath = Util.GetXpath({"locate": "redirect"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click redirect btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickRedirect')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit
        xpath = Util.GetXpath({"locate": "edit"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel
        xpath = Util.GetXpath({"locate": "cancel"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok
        xpath = Util.GetXpath({"locate": "ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click delete
        xpath = Util.GetXpath({"locate": "delete"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArchive(arg):
        '''
        ClickArchive : Click archive
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click archive
        xpath = Util.GetXpath({"locate": "archive"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click archive btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickArchive')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterPageNumberBar(arg):
        '''
        ClickFilterPageNumberBar : Click filter page number bar
                Input argu :
                    page_number - 10 / 20 / 50 / 100
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]

        ##Click filter page number bar
        xpath = Util.GetXpath({"locate": "page_number_filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number filter", "result": "1"})
        time.sleep(1)

        ##Click filter page number
        xpath = Util.GetXpath({"locate": "page_number"})
        xpath = xpath.replace("page_to_be_replaced", page_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickFilterPageNumberBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageNumber(arg):
        '''
        ClickPageNumber : Click page number
                Input argu :
                    page_number - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]

        ##Click page number
        xpath = Util.GetXpath({"locate": "page_number"})
        xpath = xpath.replace("page_to_be_replaced", page_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreviousPage(arg):
        '''
        ClickPreviousPage : Click previous page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click previous page
        xpath = Util.GetXpath({"locate": "previous_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click previous page", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickPreviousPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click next page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next page
        xpath = Util.GetXpath({"locate": "next_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSpin(arg):
        '''
        ClickSpin : Click spin button up or down
                Input argu :
                    spin - up /down
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        spin = arg["spin"]

        ##Click spin button
        xpath = Util.GetXpath({"locate": spin})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click spin button " + spin, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCommonButton.ClickSpin')

class AdminShopCategoryDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExpandOfficialShopCategoryPage(arg):
        '''
        ExpandOfficialShopCategoryPage : Expand official shop category page in order to see all official shop catgeory
            Input argu :
                version - shop category version (Old Version / New Version)
                category_name - category that needs to expand
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        version = arg["version"]
        category_name = arg["category_name"]

        ##Click version drop down box
        xpath = Util.GetXpath({"locate": "version_drop_down_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click version drop down box", "result": "1"})

        ##Select version
        xpath = Util.GetXpath({"locate": "version"})
        xpath = xpath.replace("string_to_be_replaced", version)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select version: " + version, "result": "1"})
        time.sleep(5)

        ##Click filter category
        xpath = Util.GetXpath({"locate": "category_filter_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category filter button", "result": "1"})
        time.sleep(1)

        ##Click filter category bar
        xpath = Util.GetXpath({"locate": "category_filter_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category filter bar", "result": "1"})
        time.sleep(1)

        ##Click choose category
        xpath = Util.GetXpath({"locate": "category_filter_name"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category -> " + category_name, "result": "1"})
        time.sleep(2)

        ##Click OK
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK btn", "result": "1"})
        time.sleep(2)

        ##Click filter page number bar
        xpath = Util.GetXpath({"locate": "page_number_filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number filter", "result": "1"})
        time.sleep(1)

        ##Click filter page number
        xpath = Util.GetXpath({"locate": "max_page_number"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose max page number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.ExpandOfficialShopCategoryPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteOfficialShopCategory(arg):
        '''
        DeleteOfficialShopCategory : Delete specific official shop category
            Input argu :
                category_name - category name
                shop_name - shop name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]

        ##Check if there is shop exist
        xpath = Util.GetXpath({"locate": "shop_exist"})
        xpath = xpath.replace("category_to_be_replaced", category_name).replace("shop_to_be_replaced", shop_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Get and reserve official shop order
            order_xpath = Util.GetXpath({"locate": "icon_order"})
            order_xpath = order_xpath.replace("category_to_be_replaced", category_name).replace("shop_to_be_replaced", shop_name)
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

            ##Click to delete banner
            xpath = Util.GetXpath({"locate": "delete_shop_icon"})
            xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete shop -> " + shop_name, "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.DeleteOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditOfficialShopCategory(arg):
        '''
        EditOfficialShopCategory : Edit specific official shop category
            Input argu :
                category_name - category name
                shop_name - shop name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]

        ##Get and reserve official shop order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("category_to_be_replaced", category_name).replace("shop_to_be_replaced", shop_name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to edit banner
        xpath = Util.GetXpath({"locate": "edit_shop_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit shop -> " + shop_name, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.EditOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateOfficialShopCategory(arg):
        '''
        CreateOfficialShopCategory : Create official shop category content
            Input argu :
                version - shop category version (old / new)
                category_name - category name
                shop_name - shop name
                brand_name - brand name
                index - index (A ~ Z , #)
                sorting_name - sorting name
                visibility_type - visible / invisible
                app_logo_name - app logo file name
                app_logo_type - app logo file type
                pc_logo_name - pc logo file name
                pc_logo_type - pc logo file type
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        version = arg["version"]
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]
        brand_name = arg["brand_name"]
        index = arg["index"]
        sorting_name = arg["sorting_name"]
        visibility_type = arg["visibility_type"]
        app_logo_name = arg["app_logo_name"]
        app_logo_type = arg["app_logo_type"]
        pc_logo_name = arg["pc_logo_name"]
        pc_logo_type = arg["pc_logo_type"]

        if category_name:
            ##Click category list bar
            xpath = Util.GetXpath({"locate": version + "_category_list_bar"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category list bar", "result": "1"})
            time.sleep(1)

            ##Input category name
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": category_name, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

            ##Click category title
            xpath = Util.GetXpath({"locate": version + "_category_title"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category title", "result": "1"})

        if shop_name:
            ##Click shop list bar
            xpath = Util.GetXpath({"locate": "shop_list_bar"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop list bar", "result": "1"})
            time.sleep(1)

            ##Input shop name
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": shop_name, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        if brand_name:
            ##Input brand name
            xpath = Util.GetXpath({"locate": "brand_name_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": brand_name, "result": "1"})

        if index:
            ##Click index bar
            xpath = Util.GetXpath({"locate": "index_list_bar"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click index list bar", "result": "1"})
            time.sleep(1)

            ##Input index
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": index, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        if sorting_name:
            ##Input sorting name
            xpath = Util.GetXpath({"locate": "sorting_name_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": sorting_name, "result": "1"})

        if visibility_type:
            ##Click choose visibility type
            xpath = Util.GetXpath({"locate": visibility_type})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility type -> " + visibility_type, "result": "1"})
                time.sleep(2)
            else:
                dumplogger.info("Visibility type is already the same.")

        ##Input app logo image
        if app_logo_name and app_logo_type:
            xpath = Util.GetXpath({"locate": "app_logo_input"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": app_logo_name, "file_type": app_logo_type, "result": "1"})
            time.sleep(8)

        ##Input pc logo image
        if pc_logo_name and pc_logo_type:
            xpath = Util.GetXpath({"locate": "pc_logo_input"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": pc_logo_name, "file_type": pc_logo_type, "result": "1"})
            time.sleep(8)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.CreateOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBEOfficialShopCategoryExist(arg):
        '''
        CheckBEOfficialShopCategoryExist : Check specific official shop category exists
            Input argu :
                category_name - category name
                shop_name - shop name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]

        ##Check if there is shop exist
        xpath = Util.GetXpath({"locate": "shop_exist"})
        xpath = xpath.replace("category_to_be_replaced", category_name).replace("shop_to_be_replaced", shop_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Official shop category exists.")
        else:
            ret = 0
            dumplogger.info("Official shop category not exist.")

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.CheckBEOfficialShopCategoryExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveSortingName(arg):
        '''
        RemoveSortingName : Remove sorting name
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Remove sorting name
        xpath = Util.GetXpath({"locate": "sorting_name_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.RemoveSortingName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCategory(arg):
        '''
         SelectCategory : Select category
            Input argu :
                category_name - category name
                filter_button - reset / ok
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        category_name = arg["category_name"]
        filter_button = arg["filter_button"]

        ##Click category filter
        xpath = Util.GetXpath({"locate": "category_filter_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category filter button", "result": "1"})
        time.sleep(1)

        if category_name:
            ##Click category filter drop down btn
            xpath = Util.GetXpath({"locate": "category_filter_drop_down"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category filter drop down btn", "result": "1"})
            time.sleep(1)

            ##Input category name
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": category_name, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
            time.sleep(1)

        if filter_button:
            xpath = Util.GetXpath({"locate": filter_button})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter button: " + filter_button, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.SelectCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchFilter(arg):
        '''
         InputSearchFilter : Input search filter
            Input argu :
                keywords - keywords
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        keywords = arg["keywords"]

        ##Click search filter
        xpath = Util.GetXpath({"locate": "search_filter_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search filter button", "result": "1"})
        time.sleep(1)

        ##Input search filter
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": keywords, "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.InputSearchFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditScheduling(arg):
        '''
         EditScheduling : Edit scheduling in change position window
                Input argu :
                    scheduling_time - scheduling time
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        scheduling_time = arg["scheduling_time"]

        ##Click enable scheduling
        xpath = Util.GetXpath({"locate":"enable_scheduling"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enable scheduling", "result":"1"})

        ##Input scheduling time
        scheduling_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(scheduling_time), 0)
        xpath = Util.GetXpath({"locate":"time_box"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click time box", "result":"1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":scheduling_time, "result":"1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.EditScheduling')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBrandBarAndList(arg):
        '''
        EditBrandBarAndList : Edit brand bar and list
            Input argu :
                category_name - category name
                shop_name - shop name
                action - disable_bar / enable_bar / disable_list / enable_list
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        category_name = arg["category_name"]
        shop_name = arg["shop_name"]
        action = arg["action"]

        ##Get and reserve official shop order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("category_to_be_replaced", category_name).replace("shop_to_be_replaced", shop_name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to brand bar and list button
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + action + " -> " + shop_name, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPage.EditBrandBarAndList')

class AdminShopCategoryDetailPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOfficialShopCategory(arg):
        '''
        ClickAddOfficialShopCategory : Click add official shop category btn
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        ##Click add btn
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add official shop btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickAddOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveOfficialShopCategory(arg):
        '''
        ClickSaveOfficialShopCategory : Click save official shop category btn
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save official shop btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickSaveOfficialShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShopCategoryVersion(arg):
        '''
        SelectShopCategoryVersion : Select shop category version
            Input argu :
                version - shop category version(Old Version / New Version)
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        version = arg["version"]

        ##Click version drop down box
        xpath = Util.GetXpath({"locate": "version_drop_down_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click version drop down box", "result": "1"})

        ##Select version
        xpath = Util.GetXpath({"locate": "version"})
        xpath = xpath.replace("string_to_be_replaced", version)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select version: " + version, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.SelectShopCategoryVersion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
         ClickNextPage : Click go to next page
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click go to next page button
        xpath = Util.GetXpath({"locate":"next_page"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click go to next page button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchFilterButton(arg):
        '''
         ClickSearchFilterButton : Click search filter button
            Input argu :
                filter_button - reset / search
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        filter_button = arg["filter_button"]

        ##Click search filter button
        xpath = Util.GetXpath({"locate": filter_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search filter button: " + filter_button, "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickSearchFilterButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReSyncToImageStock(arg):
        '''
         ClickReSyncToImageStock : Click Re-sync to image stock
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Re-sync to image stock
        xpath = Util.GetXpath({"locate":"resync_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Re-sync to image stock button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickReSyncToImageStock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassChangeDisplayOrder(arg):
        '''
         ClickMassChangeDisplayOrder : Click mass change display order
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click mass change display order
        xpath = Util.GetXpath({"locate":"mass_change_display_order"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mass change display order button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickMassChangeDisplayOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassAddDeleteOfficialShop(arg):
        '''
         ClickMassAddDeleteOfficialShop : Click mass add / delete official shop
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click mass add / delete official shop
        xpath = Util.GetXpath({"locate":"mass_add_delete_official_shop"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mass add / delete official shop button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickMassAddDeleteOfficialShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadCSV(arg):
        '''
         ClickDownloadCSV : Click download csv
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click download csv
        xpath = Util.GetXpath({"locate":"download_csv"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click download csv button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickDownloadCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
         ClickUpload : Click upload
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click upload
        xpath = Util.GetXpath({"locate":"upload"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminShopCategoryDetailPageButton.ClickUpload')

class AdminOfficialShopSummary:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExpandOfficialShopSummaryPage(arg):
        '''
        ExpandOfficialShopSummaryPage : Expand official shop summary page in order to see all official shop summary
            Input argu :
                version - shop category version (Old Version / New Version)
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        version = arg["version"]

        ##Click version drop down box
        xpath = Util.GetXpath({"locate": "version_drop_down_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click version drop down box", "result": "1"})

        ##Select version
        xpath = Util.GetXpath({"locate": "version"})
        xpath = xpath.replace("string_to_be_replaced", version)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select version: " + version, "result": "1"})
        time.sleep(5)

        ##Click filter page number bar
        xpath = Util.GetXpath({"locate": "page_number_filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number filter", "result": "1"})
        time.sleep(1)

        ##Click filter page number
        xpath = Util.GetXpath({"locate": "max_page_number"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose max page number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopSummary.ExpandOfficialShopSummaryPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditActions(arg):
        '''
        EditActions : Edit actions
            Input argu :
                category_name - category name
                action - hide / show
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        category_name = arg["category_name"]
        action = arg["action"]

        ##Get and reserve category order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("category_to_be_replaced", category_name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to actions button
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + action + " -> " + category_name, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopSummary.EditActions')

class AdminMassUploadShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCSV(arg):
        '''
         UploadCSV : Upload csv file
                Input argu :
                    upload_type - add_shops / delete_shops
                    action - file / image
                    file_name - file name
                    file_type - file type
                    project - file project path
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        project = arg["project"]

        ##Upload csv with collections
        xpath = Util.GetXpath({"locate":upload_type})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action":action, "file_name":file_name, "file_type":file_type, "result":"1"})

        OK(ret, int(arg['result']), 'AdminMassUploadShopPage.UploadCSV')

class AdminMassUploadShopPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadResultInfo(arg):
        '''
         ClickDownloadResultInfo : Click download result info
                Input argu :
                    upload_type - add / delete
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]

        ##Download specific result info file
        xpath = Util.GetXpath({"locate":"download_result_info"})
        xpath = xpath.replace("upload_type_to_be_replaced", upload_type)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMassUploadShopPageButton.ClickDownloadResultInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBack(arg):
        '''
         ClickBack : Click back
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click back
        xpath = Util.GetXpath({"locate":"back"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMassUploadShopPageButton.ClickBack')

class AdminBrandOfWeekPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBrandOfWeek(arg):
        '''
        CreateBrandOfWeek : Create brand of week
            Input argu :
                visibility_type - visible / invisible
                title - collection title
                is_display_title - 1 / 0 / null(remain current status)
                banner_image_name - banner image name
                banner_image_type - banner image type
                url - redirection url
                collection_type - product_collection / shop_category
                start_time - adjust start time
                is_end_time - 1 / 0 / null(remain current status)
                is_mall_image - 1 / 0/ null(remain current status)
            optional:
                mall_image_name - mall image name (if is_mall_image == "1")
                mall_image_type - mall image type (if is_mall_image == "1")
                end_time - adjust end time (if is_end_time == "1")
                collection_id - collection id (if collection_type == "product_collection")
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        visibility_type = arg["visibility_type"]
        title = arg["title"]
        is_display_title = arg["is_display_title"]
        banner_image_name = arg["banner_image_name"]
        banner_image_type = arg["banner_image_type"]
        url = arg["url"]
        collection_type = arg["collection_type"]
        start_time = arg["start_time"]
        is_end_time = arg["is_end_time"]
        is_mall_image = arg["is_mall_image"]

        ##Choose if Visible
        if visibility_type:
            xpath = Util.GetXpath({"locate": visibility_type})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility -> " + visibility_type, "result": "1"})
                time.sleep(2)

        ##Edit title
        if title:
            xpath = Util.GetXpath({"locate": "title_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})
            time.sleep(2)

        ##Edit display title or not
        if is_display_title:
            if int(is_display_title):
                xpath = Util.GetXpath({"locate": "is_display_title"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Enable display title", "result": "1"})
                    time.sleep(2)
            else:
                xpath = Util.GetXpath({"locate": "not_display_title"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Disable display title", "result": "1"})
                    time.sleep(2)

        ##Upload banner image
        if banner_image_name and banner_image_type:
            xpath = Util.GetXpath({"locate": "banner_image_input"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": banner_image_name, "file_type": banner_image_type, "result": "1"})
            time.sleep(8)

        ##Edit url
        if url:
            xpath = Util.GetXpath({"locate": "url_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})
            time.sleep(2)

        ##Edit collection_type
        if collection_type:
            xpath = Util.GetXpath({"locate": collection_type})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose collection type -> " + collection_type, "result": "1"})
                time.sleep(2)

            ##Input collection id if we choose product collection
            if collection_type == "product_collection":
                collection_id = arg["collection_id"]
                xpath = Util.GetXpath({"locate": "collection_id_input"})

                ##Clear field
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
                time.sleep(5)

                ##Input collection id
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collection_id, "result": "1"})
                time.sleep(2)

        ##Edit start time
        if start_time:
            ##Get current date time and add minutes on it
            time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(start_time), 0)
            time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "start_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

            ##Modify start time
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Check if end time needed to be limited or not
        if is_end_time:
            if int(is_end_time):
                xpath = Util.GetXpath({"locate": "is_end_time"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Enable end time", "result": "1"})
                    time.sleep(2)
                end_time = arg["end_time"]

                ##Get current date time and add minutes on it
                time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(end_time), 0)
                time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(end_time), 0)

                ##Modify end date
                xpath = Util.GetXpath({"locate": "end_date_dropdown"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
                time.sleep(2)
                xpath = Util.GetXpath({"locate": "end_date_input"})
                BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
                time.sleep(2)
                BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

                ##Modify end time
                xpath = Util.GetXpath({"locate": "end_time_dropdown"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
                time.sleep(2)
                xpath = Util.GetXpath({"locate": "end_time_input"})
                BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
                BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
            else:
                xpath = Util.GetXpath({"locate": "not_end_time"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Disable end time", "result": "1"})
                    time.sleep(2)

        ##Check if mall page image need to be added
        if is_mall_image:
            if int(is_mall_image):
                xpath = Util.GetXpath({"locate": "is_mall_image"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Enable mall image", "result": "1"})
                    time.sleep(2)

                ##Upload mall image
                mall_image_name = arg["mall_image_name"]
                mall_image_type = arg["mall_image_type"]
                xpath = Util.GetXpath({"locate": "mall_image_input"})
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": mall_image_name, "file_type": mall_image_type, "result": "1"})
                time.sleep(8)
            else:
                xpath = Util.GetXpath({"locate": "not_mall_image"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Disable mall image", "result": "1"})
                    time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPage.CreateBrandOfWeek')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBrandOfWeekPosition(arg):
        '''
        ChangeBrandOfWeekPosition : Change brand of week position
            Input argu :
                name - brand of week name
                position - new position number
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        name = arg["name"]
        position = arg["position"]
        order_column = "1"

        BaseUILogic.MouseScrollEvent({"x": "0", "y": "2000", "result": "1"})
        BaseUILogic.MouseScrollEvent({"x": "0", "y": "0", "result": "1"})
        time.sleep(5)

        ##Get and reserve brand of week order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("name_to_be_replaced", name).replace("order_index_to_be_replaced", order_column)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to change brand of week order
        xpath = Util.GetXpath({"locate": "change_order_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_[-2:])
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change order", "result": "1"})
        time.sleep(2)

        ##Input new position
        xpath = Util.GetXpath({"locate": "position_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": position, "result": "1"})
        time.sleep(2)

        ##Click confirm
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})
        time.sleep(5)

        ##Click close
        xpath = Util.GetXpath({"locate": "close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close", "result": "1"})
        time.sleep(5)

        ##Refresh browser
        BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPage.ChangeBrandOfWeekPosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBrandOfWeek(arg):
        '''
        DeleteBrandOfWeek : Delete specific brand of week (if exists)
            Input argu :
                name - brand of week name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        name = arg["name"]
        order_column = "1"

        ##Get and reserve brand of week order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("name_to_be_replaced", name).replace("order_index_to_be_replaced", order_column)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": order_xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

            ##Click to delete brand of week
            xpath = Util.GetXpath({"locate": "delete_icon"})
            xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_[-2:])
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete brand of week", "result": "1"})
            time.sleep(2)

            ##Click confirm
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})
            time.sleep(5)

            ##Refresh browser
            BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPage.DeleteBrandOfWeek')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBrandOfWeek(arg):
        '''
        EditBrandOfWeek : Edit specific brand of week
            Input argu :
                name - brand of week name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        name = arg["name"]
        order_column = "1"

        ##Get and reserve brand of week order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("name_to_be_replaced", name).replace("order_index_to_be_replaced", order_column)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to edit brand of week
        xpath = Util.GetXpath({"locate": "edit_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_[-2:])
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit brand of week", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPage.EditBrandOfWeek')

class AdminBrandOfWeekPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddBrandOfWeek(arg):
        '''
        ClickAddBrandOfWeek : Click add new brand of week btn
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click add btn
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new brand of week btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPageButton.ClickAddBrandOfWeek')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveBrandOfWeek(arg):
        '''
        ClickSaveBrandOfWeek : Click save brand of week btn
            Input argu :
                N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save brand of week btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPageButton.ClickSaveBrandOfWeek')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteImage(arg):
        '''
        ClickDeleteImage : Click delete image button
            Input argu :
                image_type - brand_of_week_banner / homepage_rectangle_image
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        image_type = arg["image_type"]

        ##Click delete image btn
        xpath = Util.GetXpath({"locate": image_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete image btn: " + image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBrandOfWeekPageButton.ClickDeleteImage')

class AdminOfficialShopCardsPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHereLink(arg):
        '''
        ClickHereLink : Click here link
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click here link
        xpath = Util.GetXpath({"locate": "here_link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click here link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickHereLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickActionsTab(arg):
        '''
        ClickActionsTab : Click actions tab
                Input argu :
                    tab - sold / reserved / archived
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click actions tab
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click actions tab: " + tab, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickActionsTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewOfficialShopsCard(arg):
        '''
        ClickAddNewOfficialShopsCard : Click add new official shops card btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new official shops card btn
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new official shops card btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickAddNewOfficialShopsCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSoldSpot(arg):
        '''
        ClickSoldSpot : Click sold spot
                Input argu :
                    option - yes / no
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click sold spot
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sold spot: " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickSoldSpot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisibility(arg):
        '''
        ClickVisibility : Click visibility
                Input argu :
                    option - visible / not_visible
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click visibility
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visibility: " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickVisibility')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteStartTime(arg):
        '''
        ClickDeleteStartTime : Click delete start time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Delete start time
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete start time", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickDeleteStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteEndTime(arg):
        '''
        ClickDeleteEndTime : Click delete end time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Delete end time
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete end time", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickDeleteEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNoEndingTime(arg):
        '''
        ClickNoEndingTime : Click no ending time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click no ending time
        xpath = Util.GetXpath({"locate": "no_ending_time"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click no ending time", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickNoEndingTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefresh(arg):
        '''
        ClickRefresh : Click refresh
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refresh
        xpath = Util.GetXpath({"locate": "refresh"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refresh", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickRefresh')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditOrderIcon(arg):
        '''
        ClickEditOrderIcon : Click edit order icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit order icon
        xpath = Util.GetXpath({"locate": "edit_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit order icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickEditOrderIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveOrderIcon(arg):
        '''
        ClickSaveOrderIcon : Click save order icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save order icon
        xpath = Util.GetXpath({"locate": "save_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save order icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickSaveOrderIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOrderIcon(arg):
        '''
        ClickCancelOrderIcon : Click cancel order icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel order icon
        xpath = Util.GetXpath({"locate": "cancel_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel order icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickCancelOrderIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopIDFilterIcon(arg):
        '''
         ClickShopIDFilterIcon : Click shop id filter button
                Input argu : N/A
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click shop id filter button
        xpath = Util.GetXpath({"locate": "search_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop id filter button", "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickShopIDFilterIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisibleFilterIcon(arg):
        '''
         ClickVisibleFilterIcon : Click visible filter button
                Input argu :
                    tab - sold / reserved
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click visible filter button
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visible filter button", "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickVisibleFilterIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopIDFilter(arg):
        '''
         ClickShopIDFilter : Click button to search or reset filter
                Input argu :
                    filter_button - reset / search
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        filter_button = arg["filter_button"]

        ##Click search filter button
        xpath = Util.GetXpath({"locate": filter_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Shop ID filter button: " + filter_button, "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickShopIDFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisibleFilter(arg):
        '''
         ClickVisibleFilter : Click button in visible filter panel
                Input argu :
                    filter_button - visible / not_visible / none / reset
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        filter_button = arg["filter_button"]

        ##Click button in visible filter panel
        xpath = Util.GetXpath({"locate": filter_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Visible filter button :" + filter_button, "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickVisibleFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreviewSoldSpots(arg):
        '''
         ClickPreviewSoldSpots : Click preview sold spots btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click preview sold spots btn
        xpath = Util.GetXpath({"locate": "preview_sold_spots_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click preview sold spots btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickPreviewSoldSpots')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click close
        xpath = Util.GetXpath({"locate": "close"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectTime(arg):
        '''
         ClickSelectTime : Click select time radio button in preview sold spots popup pandel
                Input argu :
                    radio_button - current_time / select_time
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        radio_button = arg["radio_button"]

        ##Click select time radio button in preview sold spots popup pandel
        xpath = Util.GetXpath({"locate": radio_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select time radio button :" + radio_button, "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickSelectTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteSelectTime(arg):
        '''
        ClickDeleteSelectTime : Click delete select time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Delete select time
        xpath = Util.GetXpath({"locate": "time_field"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete select time", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickDeleteSelectTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmitAndPreview(arg):
        '''
        ClickSubmitAndPreview : Click submit and preview button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit and preview button
        xpath = Util.GetXpath({"locate": "submit_preview_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit and preview button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickSubmitAndPreview')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemoveScheduling(arg):
        '''
        ClickRemoveScheduling : Click remove schedualing box
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click remove schedualing box
        xpath = Util.GetXpath({"locate": "check_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove schedualing box", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPageButton.ClickRemoveScheduling')


class AdminOfficialShopCardsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopID(arg):
        '''
        InputShopID : Input shop id
                Input argu :
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        xpath = Util.GetXpath({"locate": "shop_id"})

        ##Clear field
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        time.sleep(5)

        ##Input shop id
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.InputShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStartTime(arg):
        '''
        InputStartTime : Input start time
                Input argu :
                    specific_start_time - specific start time
                    duration_start_time - start time duration
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        specific_start_time = arg["specific_start_time"]
        duration_start_time = arg["duration_start_time"]

        ##Click select start time
        xpath = Util.GetXpath({"locate": "start_time_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time box", "result": "1"})
        time.sleep(2)

        ##Input start time
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        if specific_start_time:
            ##Input specific start time
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": specific_start_time, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        else:
            ##Input the start time duration
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(duration_start_time), 0)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.InputStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEndTime(arg):
        '''
        InputEndTime : Input end time
                Input argu :
                    specific_end_time - specific end time
                    duration_end_time - end time duration
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        specific_end_time = arg["specific_end_time"]
        duration_end_time = arg["duration_end_time"]

        ##Click select end time
        xpath = Util.GetXpath({"locate": "end_time_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time box", "result": "1"})
        time.sleep(2)

        ##Input end time
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        if specific_end_time:
            ##Input specific end time
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": specific_end_time, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        else:
            ##Input the end time duration
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(duration_end_time), 0)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.InputEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditOfficialShopsCardList(arg):
        '''
        EditOfficialShopsCardList : Edit official shops card list
                Input argu :
                    shop_name - shop name
                    action - enable / disable / edit / archive / delete / restore / schedule
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]
        action = arg["action"]

        ##Get and reserve official shop order
        order_xpath = Util.GetXpath({"locate": "icon_order"})
        order_xpath = order_xpath.replace("shop_to_be_replaced", shop_name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click list button
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + action + " -> " + shop_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.EditOfficialShopsCardList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteOfficialShopsCardList(arg):
        '''
        DeleteOfficialShopsCardList : Delete official shops card list
                Input argu :
                    shop_name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Switch to active(reserved) tab
        AdminOfficialShopCardsPageButton.ClickActionsTab({"tab": "reserved", "result": "1"})
        time.sleep(5)

        BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})
        time.sleep(5)
        AdminOfficialShopCommonButton.ClickFilterPageNumberBar({"page_number": "100","result": "1"})
        xpath = Util.GetXpath({"locate": "archived_button"})
        xpath = xpath.replace("name_to_be_replaced", shop_name)

        ##Official shops card is Active (Reserved)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):

            ##Click archived button
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click archived button on official shops card", "result": "1"})
            AdminOfficialShopCommonButton.ClickArchive({"result": "1"})

        ##Switch to archived tab
        AdminOfficialShopCardsPageButton.ClickActionsTab({"tab": "archived", "result": "1"})
        time.sleep(5)

        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("name_to_be_replaced", shop_name)

        ##Official shops card is Archived
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):

            ##Click delete button
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button on official shops card", "result": "1"})
            AdminOfficialShopCommonButton.ClickDelete({"result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.DeleteOfficialShopsCardList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeOfficialShopsCardPosition(arg):
        '''
        ChangeOfficialShopsCardPosition : Change official shops card position
                Input argu :
                    element - official shops card you want to change
                    input_position - input position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element = arg["element"]
        input_position = arg["input_position"]

        ##Change specific official shops card position
        xpath = Util.GetXpath({"locate": "edit_box"})
        xpath = xpath.replace('string_to_be_replaced', element)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_position, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.ChangeOfficialShopsCardPosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFilterShopID(arg):
        '''
        InputFilterShopID : Input shop id to filter
                Input argu :
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        xpath = Util.GetXpath({"locate": "shop_id"})

        ##Clear field
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        time.sleep(5)

        ##Input shop id
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.InputFilterShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSelectTime(arg):
        '''
        InputSelectTime : Input start time
                Input argu :
                    specific_start_time - specific start time
                    duration_start_time - start time duration
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        specific_start_time = arg["specific_start_time"]
        duration_start_time = arg["duration_start_time"]

        ##Click select time filed
        xpath = Util.GetXpath({"locate": "time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select time field", "result": "1"})
        time.sleep(2)

        ##Input select time field
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        if specific_start_time:
            ##Input specific start time
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": specific_start_time, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
        elif duration_start_time:
            ##Input the start time duration
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(duration_start_time), 0)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.InputSelectTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreAndCompareUpdateTime(arg):
        '''
        StoreAndCompareUpdateTime : Check weather the time is updated
                Input argu :
                    field - element to compare
                    compare - 1/0 (1:compare with previous stored updated time/0:store update time)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field = arg['field']
        compare = arg['compare']

        ##Get current update time
        xpath = Util.GetXpath({"locate": field})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute":"value", "mode":"single", "result": "1"})

        if GlobalAdapter.CommonVar._PageAttributes_ == '':
            ret = 0
            dumplogger.info("Compare time not found !")

        else:

            if int(compare):
                dumplogger.info("Comprare time : %s" % (GlobalAdapter.CommonVar._PageAttributes_))

                ##Check if update time has changed
                if GlobalAdapter.CommonVar._PageAttributes_ == GlobalAdapter.ProductCollectionE2EVar._UpdateTime_:
                    ret = 0
                    dumplogger.info("Compare time has not changed.")

                else:
                    dumplogger.info("Compare time has changed.")
                    dumplogger.info("Previous stored time: %s" % (GlobalAdapter.ProductCollectionE2EVar._UpdateTime_) + " not equal compare time : %s" % (GlobalAdapter.CommonVar._PageAttributes_))

            ##Store update time
            else:
                GlobalAdapter.ProductCollectionE2EVar._UpdateTime_ = GlobalAdapter.CommonVar._PageAttributes_
                dumplogger.info("Store time : %s" % (GlobalAdapter.ProductCollectionE2EVar._UpdateTime_))

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.StoreAndCompareUpdateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRepositionShopOrder(arg):
        '''
        InputRepositionShopOrder : Input order in reposition field
                Input argu :
                    order - order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]

        xpath = Util.GetXpath({"locate": "order"})

        ##Clear field
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        time.sleep(5)

        ##Input order
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": order, "result": "1"})
        time.sleep(2)
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopCardsPage.InputRepositionShopOrder')


class AdminPageConfigurationPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new page configuration button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new official shops card btn
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new page configuration button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToList(arg):
        '''
         ClickBackToList : Click back to list button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click back
        xpath = Util.GetXpath({"locate":"back_to_list_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back to list button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickBackToList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel in popup panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn in popup panel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLeave(arg):
        '''
        ClickLeave : Click leave in popup panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click leave
        xpath = Util.GetXpath({"locate": "leave_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click leave btn in popup panel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickLeave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveAndGoToPageBuild(arg):
        '''
        ClickSaveAndGoToPageBuild : Click save and go to page build btn
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click save and go to page build btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save and go to page build btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickSaveAndGoToPageBuild')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublish(arg):
        '''
        ClickPublished : Click publish button to confirm publish
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click publish
        xpath = Util.GetXpath({"locate": "publish_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click publish button to confirm publish", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickPublish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSavePageInfo(arg):
        '''
        ClickSavePageInfo : Click save page info button
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click save page info button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save page info button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickSavePageInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditView(arg):
        '''
        ClickEditView : Click edit view button
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click edit view button
        xpath = Util.GetXpath({"locate": "edit_view_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit view button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickEditView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveChange(arg):
        '''
        ClickSaveChange : Click save change
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click save change button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save change", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickSaveChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHeaderTextColor(arg):
        '''
        ClickHeaderTextColor : Click header text color
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Click header text color
        xpath = Util.GetXpath({"locate": "color_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click header text color", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPageButton.ClickHeaderTextColor')


class AdminPageConfigurationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreatePageConfiguration(arg):
        '''
        CreatePageConfiguration : Create page configuration
            Input argu :
                mall_category - category name
                page_title - page title
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        mall_category = arg["mall_category"]
        page_title = arg["page_title"]

        ##Input mall category
        if mall_category:
            xpath = Util.GetXpath({"locate": "mall_category_drawer"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose mall category -> " + mall_category, "result": "1"})
            xpath = Util.GetXpath({"locate": "category_name"})
            xpath = xpath.replace('string_to_be_replaced', mall_category)
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose mall category -> " + mall_category, "result": "1"})
                time.sleep(2)

        ##Input configuration name
        if page_title:
            xpath = Util.GetXpath({"locate": "title_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_title, "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.CreatePageConfiguration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeletePageTilte(arg):
        '''
        DeletePageTilte : Delete page tilte
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1

        ##Delete title
        xpath = Util.GetXpath({"locate": "title_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.DeletePageTilte')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickComponent(arg):
        '''
        ClickComponent : Click type of component
            Input argu :
                type - Widget / Image / Products / Promotions / Mall
                component - compoment name
            Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        ret = 1
        type = arg["type"]
        component = arg["component"]

        ##Click type of component
        xpath = Util.GetXpath({"locate": "left_menu"})
        xpath = xpath.replace("string_to_be_replaced", type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click component type -> " + type, "result": "1"})

        ##Click component
        xpath = Util.GetXpath({"locate": component})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click component -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.ClickComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderText(arg):
        '''
        InputHeaderText : Input header text
                Input argu :
                    language - header text language
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        language = arg["language"]
        text = arg["text"]

        ##Input text to header text input box
        xpath = Util.GetXpath({"locate": language})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.InputHeaderText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteHeaderText(arg):
        '''
        DeleteHeaderText : Delete header text
                Input argu :
                    language - header text language
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        language = arg["language"]

        ##Delete header text
        xpath = Util.GetXpath({"locate": language})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.DeleteHeaderText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectedComponent(arg):
        '''
        ClickSelectedComponent : Click on the selected component
                Input argu :
                    component_number - component order which has been selected
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_number = arg["component_number"]

        ##Click on the selected component
        xpath = Util.GetXpath({"locate": "selected_component"})
        xpath = xpath.replace("string_to_be_replaced", component_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click selected component", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.ClickSelectedComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteComponent(arg):
        '''
        DeleteComponent : Delete component
                Input argu :
                    component_number - component order which has been selected
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_number = arg["component_number"]

        ##Delete component
        xpath = Util.GetXpath({"locate": "selected_component"})
        xpath = xpath.replace("string_to_be_replaced", component_number)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete component", "result": "1"})
        time.sleep(2)
        xpath = Util.GetXpath({"locate": "OK"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.DeleteComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisibilityStatus(arg):
        '''
        ClickVisibilityStatus : Select published or draft your configuration page
                Input argu :
                    status - Published / Draft
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click btn to published or draft configuration page
        xpath = Util.GetXpath({"locate": "select_status"})
        xpath = xpath.replace("string_to_be_replaced", status)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click configuration visibilty status -> " + status, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminPageConfigurationPage.ClickVisibilityStatus')


class AdminOfficialShopComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column (for example, create_banner_name / create_banner_page_create_banner_time)
                input_content - input text
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminOfficialShopComponent.ClickOnButton')
