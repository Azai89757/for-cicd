import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCListing Precondition =="
        dumplogger.info("== Setup COPCListing Precondition ==")

    def test_COPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCListing-01.xml")

    def test_COPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCListing-02.xml")

    def test_COPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCListing Post condition =="
        dumplogger.info("== Setup COPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
