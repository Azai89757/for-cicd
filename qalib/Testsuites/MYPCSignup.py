import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCSignup Precondition =="
        dumplogger.info("== Setup MYPCSignup Precondition ==")

    def test_MYPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCSignup Post condition =="
        dumplogger.info("== Setup MYPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
