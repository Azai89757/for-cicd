import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCVoucherInShopeeMart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCVoucherInShopeeMart Precondition =="
        dumplogger.info("== Setup VNPCVoucherInShopeeMart Precondition ==")

    def test_VNPCVoucherInShopeeMart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCVoucherInShopeeMart-01.xml")

    def test_VNPCVoucherInShopeeMart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCVoucherInShopeeMart-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCVoucherInShopeeMart Post condition =="
        dumplogger.info("== Setup VNPCVoucherInShopeeMart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
