import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCTransparentBackgroundImage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCTransparentBackgroundImage Precondition =="
        dumplogger.info("== Setup VNPCTransparentBackgroundImage Precondition ==")

    def test_VNPCTransparentBackgroundImage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCTransparentBackgroundImage-01.xml")

    def test_VNPCTransparentBackgroundImage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCTransparentBackgroundImage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCTransparentBackgroundImage Post condition =="
        dumplogger.info("== Setup VNPCTransparentBackgroundImage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
