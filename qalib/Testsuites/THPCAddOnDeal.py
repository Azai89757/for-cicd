import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCAddOnDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCAddOnDeal Precondition =="
        dumplogger.info("== Setup THPCAddOnDeal Precondition ==")

    def test_THPCAddOnDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCAddOnDeal-01.xml")

    def test_THPCAddOnDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCAddOnDeal-02.xml")

    def test_THPCAddOnDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCAddOnDeal-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCAddOnDeal Post condition =="
        dumplogger.info("== Setup THPCAddOnDeal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
