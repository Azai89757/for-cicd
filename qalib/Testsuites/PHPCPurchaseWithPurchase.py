import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCPurchaseWithPurchase(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCPurchaseWithPurchase Precondition =="
        dumplogger.info("== Setup PHPCPurchaseWithPurchase Precondition ==")

    def test_PHPCPurchaseWithPurchase01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCPurchaseWithPurchase-01.xml")

    def test_PHPCPurchaseWithPurchase02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCPurchaseWithPurchase-02.xml")

    def test_PHPCPurchaseWithPurchase03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCPurchaseWithPurchase-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCPurchaseWithPurchase Post condition =="
        dumplogger.info("== Setup PHPCPurchaseWithPurchase Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
