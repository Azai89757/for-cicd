import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCIntermediatePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCIntermediatePage Precondition =="
        dumplogger.info("== Setup VNPCIntermediatePage Precondition ==")

    def test_VNPCIntermediatePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCIntermediatePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCIntermediatePage Post condition =="
        dumplogger.info("== Setup VNPCIntermediatePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
