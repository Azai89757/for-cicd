﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminShipmentMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminPromotionRulesPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBasicSetting(arg):
        '''
        InputBasicSetting : Input basic setting for promotion rule (description, status, start time, end time, priority, payment type, custom label)
                Input argu :
                    description - promotion rule description
                    status - enable / disable
                    start_time - start time delta base on datetime now
                    end_time - end time delta base on datetime now
                    priority - priority number
                    payment_type - normal / shopee_pay
                    first_custom_label - first custom label
                    second_custom_label - second custom label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]
        status = arg["status"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        priority = arg["priority"]
        payment_type = arg["payment_type"]
        first_custom_label = arg["first_custom_label"]
        second_custom_label = arg["second_custom_label"]

        if description:
            ##Input description
            xpath = Util.GetXpath({"locate": "description_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        if status:
            ##Click status radio button
            xpath = Util.GetXpath({"locate": status})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status radio button => " + status, "result": "1"})

        if priority:
            ##Input priority
            xpath = Util.GetXpath({"locate": "priority_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": priority, "result": "1"})

        ##Set up payment type
        if payment_type:
            ##Slide down to payment type section
            BaseUICore.ExecuteScript({"script": 'document.getElementById("article").scrollTo(0,2000)', "result": "1"})

            ##Click payment type drop down list
            xpath = Util.GetXpath({"locate": "payment_type_drop_down_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment type drop down list", "result": "1"})

            ##Click payment type drop down option
            xpath = Util.GetXpath({"locate": payment_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment type drop down option => " + payment_type, "result": "1"})

        ##Set up first custom label
        if first_custom_label:
            ##Click custom label
            xpath = Util.GetXpath({"locate": "custom_label"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom label", "result": "1"})

            ##Click first custom label checkbox
            xpath = Util.GetXpath({"locate": "first_label_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first custom label checkbox", "result": "1"})

            ##Input first custom label
            xpath = Util.GetXpath({"locate": "first_label_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": first_custom_label, "result": "1"})

        ##Set up second custom label
        elif second_custom_label:
            ##Click custom label
            xpath = Util.GetXpath({"locate": "custom_label"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom label", "result": "1"})

            ##Click second custom label checkbox
            xpath = Util.GetXpath({"locate": "second_label_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click second custom label checkbox", "result": "1"})

            ##Input second custom label
            xpath = Util.GetXpath({"locate": "second_label_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": second_custom_label, "result": "1"})

        ##Set up start time
        if start_time:
            ##Prepare start time string format to "yyyy-mm-dd HH:mm"
            start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))

            ##Input start time
            xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_string, "result": "1"})

        ##Set up end time
        if end_time:
            ##Prepare end time string format to "yyyy-mm-dd HH:mm"
            end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

            ##Input end time
            xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time_string, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.InputBasicSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDiscountTypeSetting(arg):
        '''
        InputDiscountTypeSetting : Input promotion rule discount type setting
                Input argu :
                    percent_discount - percent discount. Eg: 70 mean 70% shipping fee discount
                    percent_with_cap_discount_percent - percent with cap discount percent. Eg: 70 mean 70% shipping fee discount
                    percent_with_cap_discount_cap - percent with cap discount cap. Eg: 100 mean shipping fee is (X - 100), X is shipping channel shipping fee
                    flat_rate_discount - discount flat rate. Eg: 100 mean shipping fee is 100
                    cap_discount - discount capped. Eg: 100 mean shipping fee is (X - 100), X is shipping channel shipping fee
                    round_type - up / down / not
                    rounding_factor - round shipping fee to nearest factor
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        percent_discount = arg["percent_discount"]
        percent_with_cap_discount_percent = arg["percent_with_cap_discount_percent"]
        percent_with_cap_discount_cap = arg["percent_with_cap_discount_cap"]
        flat_rate_discount = arg["flat_rate_discount"]
        cap_discount = arg["cap_discount"]
        round_type = arg["round_type"]
        rounding_factor = arg["rounding_factor"]

        ##Set up percentage with cap
        if percent_with_cap_discount_percent and percent_with_cap_discount_cap:
            ##Click percentage with cap radio button
            xpath = Util.GetXpath({"locate": "percent_with_cap_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click percentage with cap radio button", "result": "1"})

            ##Input percentage in percentage with cap
            xpath = Util.GetXpath({"locate": "percent_field_in_percent_with_cap"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_with_cap_discount_percent, "result": "1"})

            ##Input cap in percentage with cap field
            xpath = Util.GetXpath({"locate": "cap_field_in_percent_with_cap"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_with_cap_discount_cap, "result": "1"})

        ##Set up percentage
        elif percent_discount:
            ##Click percentage radio button
            xpath = Util.GetXpath({"locate": "percent_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click percentage radio button", "result": "1"})

            ##Input percentage
            xpath = Util.GetXpath({"locate": "percentag_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_discount, "result": "1"})

        ##Set up flat rate
        elif flat_rate_discount:
            ##Click flat rate radio button
            xpath = Util.GetXpath({"locate": "flat_rate_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click flat rate radio button", "result": "1"})

            ##Input flat rate
            xpath = Util.GetXpath({"locate": "flat_rate_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": flat_rate_discount, "result": "1"})

        ##Set up capped
        elif cap_discount:
            ##Click capped radio button
            xpath = Util.GetXpath({"locate": "capped_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click capped radio button", "result": "1"})

            ##Input capped
            xpath = Util.GetXpath({"locate": "capped_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": cap_discount, "result": "1"})

        if round_type:
            ##Click round radio button
            xpath = Util.GetXpath({"locate": round_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click round type radio button => " + round_type, "result": "1"})

        ##Input rounding factor
        xpath = Util.GetXpath({"locate": "rounding_factor_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rounding_factor, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.InputDiscountTypeSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDiscountRuleSetting(arg):
        '''
        InputDiscountRuleSetting : Input promotion rule discount rule setting
                Input argu :
                    seller_scope - shop_whitelist / shop_blacklist / shop_type
                    seller_shop_type - seller shop types seperated with a comma (example: Mall,Prefer,Cross Border....)
                    buyer_scope - buyer_whitelist / buyer_blacklist
                    min_order_total - minimum order amount to enjoy discount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_scope = arg["seller_scope"]
        seller_shop_type = arg["seller_shop_type"]
        buyer_scope = arg["buyer_scope"]
        min_order_total = arg["min_order_total"]

        ##Click seller scope radio button
        xpath = Util.GetXpath({"locate": seller_scope})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller scope radio button => " + seller_scope, "result": "1"})

        ##Set up discount rule seller shop type
        if seller_shop_type:
            ##Prepare seller shop list from seller shop type split by comma
            seller_shop_type_list = seller_shop_type.split(',')

            ##Click all shop type in seller shop type list
            for seller_shop_type in seller_shop_type_list:
                ##Click seller shop type checkbox
                xpath = Util.GetXpath({"locate": "shop_type_checkbox"})
                xpath_shop_type_checkbox = xpath.replace("shop_type_to_be_replace", seller_shop_type)
                BaseUICore.Click({"method": "xpath", "locate": xpath_shop_type_checkbox, "message": "Click seller shop type checkbox => " + seller_shop_type, "result": "1"})

        if buyer_scope:
            ##Click buyer scope radio button
            xpath = Util.GetXpath({"locate": buyer_scope})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click buyer scope radio button => " + buyer_scope, "result": "1"})

        ##Set up discount rule min order total
        if min_order_total:
            ##Click min order total checkbox
            xpath = Util.GetXpath({"locate": "min_order_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click min order total checkbox", "result": "1"})

            ##Input min order total
            xpath = Util.GetXpath({"locate": "min_order_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_order_total, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.InputDiscountRuleSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRebateTypeSetting(arg):
        '''
        InputRebateTypeSetting : Input promotion rule rebate type setting
                Input argu :
                    percent_rebate - percent rebate. Eg: 70 mean 70% shipping fee rebate
                    percent_with_cap_rebate_percent - percent with cap rebate percent. Eg: 70 mean 70% shipping fee rebate
                    percent_with_cap_rebate_cap - percent with cap rebate cap. Eg: 100 mean shipping fee rebate is min(X, 100), X is shipping channel shipping fee rebate
                    percent_non_buyer_shipping_fee - percent non buyer shipping fee. Eg: 70 mean (ASF - BPSF) * 70% shipping fee rebate (ASF => Actual shipping fee, BPSF => Buyer paid shipping fee)
                    cap_rebate - rebate capped. Eg: 100 mean shipping fee rebate is min(X, 100), X is shipping channel shipping fee rebate
                    round_down_to - rebate round down shipping fee
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        percent_rebate = arg["percent_rebate"]
        percent_with_cap_rebate_percent = arg["percent_with_cap_rebate_percent"]
        percent_with_cap_rebate_cap = arg["percent_with_cap_rebate_cap"]
        percent_non_buyer_shipping_fee = arg["percent_non_buyer_shipping_fee"]
        cap_rebate = arg["cap_rebate"]
        round_down_to = arg["round_down_to"]

        ##Set up percentage with cap
        if percent_with_cap_rebate_percent and percent_with_cap_rebate_cap:
            ##Click percentage with cap radio button
            xpath = Util.GetXpath({"locate": "percent_with_cap_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click percentage with cap radio button", "result": "1"})

            ##Input percentage in percentage with cap
            xpath = Util.GetXpath({"locate": "percent_field_in_percent_with_cap"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_with_cap_rebate_percent, "result": "1"})

            ##Input cap in percentage with cap field
            xpath = Util.GetXpath({"locate": "cap_field_in_percent_with_cap"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_with_cap_rebate_cap, "result": "1"})

        ##Set up percentage
        elif percent_rebate:
            ##Click percentage radio button
            xpath = Util.GetXpath({"locate": "percent_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click percentage radio button", "result": "1"})

            ##Input percentage
            xpath = Util.GetXpath({"locate": "percentag_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_rebate, "result": "1"})

        ##Set up percentage non buyer shipping fee
        elif percent_non_buyer_shipping_fee:
            ##Click percentage non buyer shipping fee radio button
            xpath = Util.GetXpath({"locate": "percentage_non_buyer_shipping_fee_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click percentage non buyer shipping fee radio button", "result": "1"})

            ##Input percentage non buyer shipping fee
            xpath = Util.GetXpath({"locate": "percentage_non_buyer_shipping_fee_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent_non_buyer_shipping_fee, "result": "1"})

        ##Set up capped
        elif cap_rebate:
            ##Click capped radio button
            xpath = Util.GetXpath({"locate": "capped_radio_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click capped radio button", "result": "1"})

            ##Input capped
            xpath = Util.GetXpath({"locate": "capped_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": cap_rebate, "result": "1"})

        ##Set up round down to
        if round_down_to:
            ##Click round down to checkbox
            xpath = Util.GetXpath({"locate": "round_down_to_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click round down to checkbox", "result": "1"})

            ##Input round down to
            xpath = Util.GetXpath({"locate": "round_down_to_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": round_down_to, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.InputRebateTypeSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpRebateRule(arg):
        '''
        SetUpRebateRule : Set up rebate rule
                Input argu :
                    shipping_proof - Require shipping proof in rebate rule, input "1" to enable, or input "0" to disable
                    tracking_number - Require tracking number in rebate rule, input "1" to process, or input "0" to ignore
                    order_esf_cover_by_seller - Provide rebate when order ESF covered by seller, input "1" to process, or input "0" to ignore
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping_proof = arg["shipping_proof"]
        tracking_number = arg["tracking_number"]
        order_esf_cover_by_seller = arg["order_esf_cover_by_seller"]

        if int(shipping_proof):
            ##Click require shipping proof in rebate rule checkbox
            xpath = Util.GetXpath({"locate": "shipping_proof_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click require shipping proof in rebate rule checkbox", "result": "1"})

        if int(tracking_number):
            ##Click require tracking number in rebate rule checkbox
            xpath = Util.GetXpath({"locate": "tracking_number_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click require tracking number in rebate rule checkbox", "result": "1"})

        if int(order_esf_cover_by_seller):
            ##Click provide rebate when order ESF covered by seller checkbox
            xpath = Util.GetXpath({"locate": "order_esf_cover_by_seller_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click provide rebate when order ESF covered by seller checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.SetUpRebateRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpRuleAttribute(arg):
        '''
        SetUpRuleAttribute : Set up rule attribute
                Input argu :
                    hide_pdp_cart_display - Hide promotion rule completely in PDP and CART display, input "1" to process, or input "0" to ignore
                    hide_text_beside_green_truck - Hide promotion rule on text beside green truck in FE, input "1" to process, or input "0" to ignore
                    welcome_package - Welcome Package Shipping Promotion, input "1" to process, or input "0" to ignore
                    group_buy - Group Buy Shipping Promotion, input "1" to process, or input "0" to ignore
                    new_user - New User Shipping Promotion, input "1" to process, or input "0" to ignore
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        hide_pdp_cart_display = arg["hide_pdp_cart_display"]
        hide_text_beside_green_truck = arg["hide_text_beside_green_truck"]
        welcome_package = arg["welcome_package"]
        group_buy = arg["group_buy"]
        new_user = arg["new_user"]

        if int(hide_pdp_cart_display):
            ##Click hide promotion rule completely in PDP and cart display checkbox
            xpath = Util.GetXpath({"locate": "hide_pdp_cart_display_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hide promotion rule completely in PDP and CART display checkbox", "result": "1"})

        if int(hide_text_beside_green_truck):
            ##Click hide promotion rule on text beside green truck in FE checkbox
            xpath = Util.GetXpath({"locate": "hide_text_beside_green_truck_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Hide promotion rule on text beside green truck in FE checkbox", "result": "1"})

        if int(welcome_package):
            ##Click welcome package shipping promotion checkbox
            xpath = Util.GetXpath({"locate": "welcome_package_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click welcome package shipping promotion checkbox", "result": "1"})

        if int(group_buy):
            ##Click group buy shipping promotion checkbox
            xpath = Util.GetXpath({"locate": "group_buy_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group buy shipping promotion checkbox", "result": "1"})

        if int(new_user):
            ##Click New User Shipping Promotion checkbox
            xpath = Util.GetXpath({"locate": "new_user_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click New User Shipping Promotion checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.SetUpRuleAttribute')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpLogisticChannel(arg):
        '''
        SetUpLogisticChannel : Set up logistic channel
                Input argu :
                    logistics - logistics channels seperated with a comma (example: 7-11,全家,黑貓....)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        logistics = arg["logistics"]

        ##Prepare logistic channel list from logistics split by comma
        logistic_channel_list = logistics.split(',')

        ##Click all logistic channel in logistic channel list
        for logistic_channel in logistic_channel_list:
            ##Click logistic channel
            xpath = Util.GetXpath({"locate": "logistic_channel_btn"})
            xpath_logistic_channel_btn = xpath.replace("logistic_channel_to_be_replace", logistic_channel)
            BaseUICore.Click({"method": "xpath", "locate": xpath_logistic_channel_btn, "message": "Click logistic channel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.SetUpLogisticChannel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCsvFile(arg):
        '''
        UploadCsvFile : Upload promotion rule csv file
                Input argu :
                    type - create / edit
                    file_name - csv file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        file_name = arg["file_name"]

        ##Click upload button in promotion rule list
        xpath = Util.GetXpath({"locate": type + "_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button in promotion rule list => " + type, "result": "1"})
        time.sleep(5)

        ##Click choose file button
        xpath = Util.GetXpath({"locate": type + "_choose_file_btn"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        time.sleep(5)

        ##Upload promotion rule csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        ##Click choose file button
        xpath = Util.GetXpath({"locate": type + "_submit_btn"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.UploadCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExportCsvFile(arg):
        '''
        ExportCsvFile : Export promotion rule csv file
                Input argu :
                    status - active / expired / upcoming
                    start_time_from - start time range from (yyyy-mm-dd HH:mm)
                    start_time_to - start time range to (yyyy-mm-dd HH:mm)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]
        start_time_from = arg["start_time_from"]
        start_time_to = arg["start_time_to"]

        ##Click export button in promotion rule list
        xpath = Util.GetXpath({"locate": "export_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export button in promotion rule list", "result": "1"})

        ##Click status drop down list
        xpath = Util.GetXpath({"locate": "status_drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status drop down list", "result": "1"})

        ##Click status drop down option
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status drop down option => " + status, "result": "1"})

        ##Input start time range from
        xpath = Util.GetXpath({"locate": "start_time_range_from_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_from, "result": "1"})

        ##Input start time range to
        xpath = Util.GetXpath({"locate": "start_time_range_to_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_to, "result": "1"})

        ##Click status drop down list again to close date time popup window
        xpath = Util.GetXpath({"locate": "status_drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status drop down list", "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status drop down list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesPage.ExportCsvFile')


class AdminPromotionRulesButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNewPromotion(arg):
        '''
        ClickCreateNewPromotion : Click create new promotion button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create new promotion button
        xpath = Util.GetXpath({"locate": "create_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create new promotion button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesButton.ClickCreateNewPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpiredPromotion(arg):
        '''
        ClickExpiredPromotion : Click expired promotion button
                Input argu :
                    button_type - show / hide
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click expired promotion button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click expired promotion button => " + button_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesButton.ClickExpiredPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu :
                    promotion_rule_name - promotion rule name you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_rule_name = arg["promotion_rule_name"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": "promotion_rule_edit_btn"})
        xpath_promotion_rule_edit_btn = xpath.replace("promotion_rule_name_to_be_replace", promotion_rule_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_promotion_rule_edit_btn, "message": "Click edit button => " + promotion_rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionRulesButton.ClickEdit')


class AdminHolidayReminderRulesPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNewReminder(arg):
        '''
        ClickCreateNewReminder : Click create new holiday reminder button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create new holiday reminder button
        xpath = Util.GetXpath({"locate": "create_new_reminder_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create new holiday reminder button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHolidayReminderRulesPage.ClickCreateNewReminder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetHolidayReminderConfiguration(arg):
        '''
        SetHolidayReminderConfiguration : Set holiday reminder configuration
                Input argu :
                    affected_logistics - logistics channel will be affected by rule
                    start_time - rule start time, according to current time to assign
                    end_time - rule end time, according to current time to assign
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        affected_logistics = arg["affected_logistics"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        affected_region = ['en', 'zh-Hant', 'id', 'th', 'vi', 'zh-Hans', 'ms-MY', 'br']

        ##Click affected logistic channel
        xpath = Util.GetXpath({"locate": "logistic_channel"})
        xpath = xpath.replace("logistic_channel_replaced", affected_logistics)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click affected logistic channel", "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "rule_start_time"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0), "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "rule_end_time"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0), "result": "1"})

        ##Input en shipping reminder message
        for each_region in affected_region:
            xpath = Util.GetXpath({"locate": "shipping_reminder_message"})
            xpath = xpath.replace("region_replaced", each_region)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string": each_region + " shipping reminder message test", "result": "1"})

        ##Slide down to prepare to click save button
        BaseUICore.ExecuteScript({"script": 'document.getElementById("article").scrollTo(0,1500)', "result": "1"})

        ##Click save rule button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHolidayReminderRulesPage.SetHolidayReminderConfiguration')


class AdminLostParcelPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadOrderLostParcelCSV(arg):
        '''
        UploadOrderLostParcelCSV : Upload order lost parcel csv file in lost parcel page
                Input argu :
                        file_name - file name you want to upload
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Click choose file button
        xpath = Util.GetXpath({"locate": "choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})

        ##Upload order lost parcel csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(2)

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        ##Click confirm import button
        xpath = Util.GetXpath({"locate": "confirm_import_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm import button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminLostParcelPage.UploadOrderLostParcelCSV')
