#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCommonMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchShopeeAdmin(arg):
    '''
        LaunchShopeeAdmin : go to shopee admin page
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1
    db_file = "admin_cookie"
    env = Config._EnvType_.lower()
    admin_cookie = {}

    ##Get BE url and go to BE
    url = "https://admin." + GlobalAdapter.UrlVar._Domain_

    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(3)

    ##If cookie is not stored for the first time, get cookie from db
    if 'admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['admin'] = [admin_cookie]

    ##Go to Shopee Admin
    BaseUICore.SetBrowserCookie({"storage_type":"admin", "result": "1"})
    time.sleep(3)
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchShopeeAdmin')

@DecoratorHelper.FuncRecorder
def GoogleAccountLogin(arg):
    ''' GoogleAccountLogin : Login with google account
            Input argu :
                account - shopee account
                password - shopee password
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    ret = 1
    account = arg["account"]
    password = arg["password"]

    ##Enter account
    xpath = Util.GetXpath({"locate": "admin_account"})
    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
    time.sleep(3)

    ##Click continue button
    xpath = Util.GetXpath({"locate": "continue_button"})
    #BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click continue button", "result": "1"})
    BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
    time.sleep(5)

    ##Enter password
    xpath = Util.GetXpath({"locate": "admin_password"})
    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})
    time.sleep(3)

    ##Click continue button
    xpath = Util.GetXpath({"locate": "continue_button"})
    #BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click continue button", "result": "1"})
    BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
    time.sleep(5)

    OK(ret, int(arg['result']), 'GoogleAccountLogin')


class AdminTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserSubTab(arg):
        '''
        ClickUserSubTab : Click subtab in users tab
                Input argu :
                    subtab - address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click user subtab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in user tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickUserSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeVoucherSubTab(arg):
        '''ClickShopeeVoucherSubTab : Click a sub item in seller voucher tab
            Input argu : subtab - subtab name (add_seller_voucher/ mass_generate_seller_voucher)
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click shopee voucher subtab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in shopee voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickShopeeVoucherSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerVoucherSubTab(arg):
        '''ClickSellerVoucherSubTab : Click a sub item in seller voucher tab
            Input argu : subtab - subtab name (add_seller_voucher/ mass_generate_seller_voucher)
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click seller voucher subtab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in seller voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickSellerVoucherSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHomeSquareSubTab(arg):
        ''' ClickHomeSquareSubTab : Click a sub item in home square
                Input argu :
                    type - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        xpath = Util.GetXpath({"locate": subtab})

        ##Click a sub item in home square
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in home square", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickHomeSquareSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnLeftPanel(arg):
        ''' ClickTabOnLeftPanel : Click a tab at the left panel
                Input argu :
                    type - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Tabs mapping
        xpath = Util.GetXpath({"locate": type})
        ##Click a tab at the left panel
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        if type == "orders":
            # time.sleep(2)
            xpath = Util.GetXpath({"locate": "sub_orders"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to go in order", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickTabOnLeftPanel -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductsSubTab(arg):
        ''' ClickProductsSubTab : Click a sub item in product tab
                Input argu :
                    subtab - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in product tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in product tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickProductsSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickListingUploadControlSubTab(arg):
        ''' ClickListingUploadControlSubTab : Click a sub item in listing upload control
                Input argu :
                    subtab - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in listing upload control
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in listing upload control", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickListingUploadControlSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSystemSubTab(arg):
        ''' ClickSystemSubTab : Click a sub item in system tab
                Input argu :
                    subtab - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in system tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in system tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickSystemSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSSubTab(arg):
        ''' ClickCMSSubTab : Click a sub item in CMS tab
                Input argu :
                    subtab - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in CMS tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickCMSSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOfficialShopsSubTab(arg):
        ''' ClickOfficialShopsSubTab : Click a sub item in Official shops tab
                Input argu :
                    tab_name - banners, shops
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        tab_name = arg["tab_name"]

        ##Click Homepage Shopee Mall tab first to let layer three tab show up
        HomepageShopeeMallTab = Util.GetXpath({"locate": "HomepageShopeeMallTab"})
        BaseUICore.Click({"method": "xpath", "locate": HomepageShopeeMallTab, "message": "Click a sub item named Homepage Shopee Mall tab first", "result": "1"})
        time.sleep(3)

        ##Click a sub item in official shops tab
        xpath = Util.GetXpath({"locate": tab_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in official shops tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickOfficialShopsSubTab -> ' + tab_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentSubTab(arg):
        ''' ClickPaymentSubTab : Click a sub item in payment tab
                Input argu :
                    subtab - which tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in payment tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in payment tab", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminTab.ClickPaymentSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFraudSubTab(arg):
        ''' ClickFraudSubTab : Click a sub item in fraud tab
                Input argu :
                    subtab - sub tab name (logistics_fraud)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in fraud tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in fraud tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickFraudSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleSubTab(arg):
        ''' ClickFlashSaleSubTab : Click a sub item in flash sale tab
                Input argu :
                    subtab - sub tab name(settings/overview/categories/image_list)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click sub tab in flash sale tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickFlashSaleSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerCenterSubTab(arg):
        ''' ClickSellerCenterSubTab : Click a sub item in seller center tab
                Input argu :
                    subtab - sub tab name (flash_sale_whitelist/flash_sale_blacklist)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click white list tab to expand all sub tabs
        xpath = Util.GetXpath({"locate": "white_list_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white list sub tab", "result": "1"})

        ##Click third sub tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click third sub tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickSellerCenterSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionSubTab(arg):
        '''ClickPromotionSubTab : Click a sub item in promotion tab
                Input argu : subtab - subtab name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in product tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in promotion tab", "result": "1"})
        time.sleep(5)

        ##Check pop up window in promotion admin
        xpath = Util.GetXpath({"locate": "welcome_pop_up"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click OK button
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickPromotionSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupBuySubTab(arg):
        ''' ClickGroupBuySubTab : Click a sub item in group buy
            Input argu : subtab - subtab name (overview/categories/groups/settings/fraud_users)
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click sub tab in group buy tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in group buy", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickGroupBuySubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShipmentSubTab(arg):
        '''ClickShipmentSubTab : Click a sub tab in Shipment tab
                Input argu : subtab - subtab name
                Return code : 1 - success
                              0 - fail
                              -1 - error

        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in shipment tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in shipment tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickShipmentSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnSubTab(arg):
        '''ClickReturnSubTab : Click a sub item in return tab
            Input argu : subtab - subtab name
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click sub tab in return
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in return", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickReturnSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundRequestSubTab(arg):
        '''ClickReturnRefundRequestSubTab : Click a sub item in return -> return refund request tab
            Input argu : subtab - subtab name
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click sub tab in return refund request
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in return refund request", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickReturnRefundRequestSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickScreeningSubTab(arg):
        '''ClickScreeningSubTab : Click a sub tab in Product tab
                Input argu : subtab - subtab name
                Return code : 1 - success
                              0 - fail
                              -1 - error

        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in shipment tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in shipment tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickScreeningSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderSubTab(arg):
        '''ClickOrderSubTab : Click a sub tab in Order tab
                Input argu : subtab - subtab name
                Return code : 1 - success
                              0 - fail
                              -1 - error

        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in order tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in order tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickOrderSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickWelcomePackageSubTab(arg):
        '''ClickWelcomePackageSubTab : Click a sub tab in Welcome Package tab
                Input argu : subtab - subtab name
                Return code : 1 - success
                              0 - fail
                              -1 - error

        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in welcome package tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in welcome package tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickWelcomePackageSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGrowthSubTab(arg):
        '''ClickGrowthSubTab : Click a sub tab in Growth tab
                Input argu : subtab - subtab name
                Return code : 1 - success
                              0 - fail
                              -1 - error

        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in Growth tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in Growth tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickGrowthSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCampaignSiteSubTab(arg):
        '''ClickCampaignSiteSubTab : Click a sub tab in campaign site tab
            Input argu : subtab - subtab name (placeholder_set/ url_scheduler / microsite_label)
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Click seller voucher subtab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in campaign site", "result": "1"})

        OK(ret, int(arg['result']), 'AdminTab.ClickCampaignSiteSubTab')
