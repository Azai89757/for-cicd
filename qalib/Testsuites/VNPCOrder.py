import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCOrder Precondition =="
        dumplogger.info("== Setup VNPCOrder Precondition ==")

    def test_VNPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-01.xml")

    def test_VNPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-02.xml")

    def test_VNPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-03.xml")

    def test_VNPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-04.xml")

    def test_VNPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-05.xml")

    def test_VNPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-06.xml")

    def test_VNPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-07.xml")

    def test_VNPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-08.xml")

    def test_VNPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-09.xml")

    def test_VNPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-10.xml")

    def test_VNPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-11.xml")

    def test_VNPCOrder12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-12.xml")

    def test_VNPCOrder13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-13.xml")

    def test_VNPCOrder14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCOrder-14.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCOrder Post condition =="
        dumplogger.info("== Setup VNPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
