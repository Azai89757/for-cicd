import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCSellerVoucher Precondition =="
        dumplogger.info("== Setup MYPCSellerVoucher Precondition ==")

    def test_MYPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCSellerVoucher-01.xml")

    # def test_MYPCSellerVoucher02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MY/MYPCSellerVoucher-02.xml")

    # def test_MYPCSellerVoucher03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MY/MYPCSellerVoucher-03.xml")

    # def test_MYPCSellerVoucher04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MY/MYPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCSellerVoucher Post condition =="
        dumplogger.info("== Setup MYPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
