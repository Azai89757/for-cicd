import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCPageTemplates(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCPageTemplates Precondition =="
        dumplogger.info("== Setup VNPCPageTemplates Precondition ==")

    def test_VNPCPageTemplates01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCPageTemplates-01.xml")

    def test_VNPCPageTemplates02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCPageTemplates-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCPageTemplates Post condition =="
        dumplogger.info("== Setup VNPCPageTemplates Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
