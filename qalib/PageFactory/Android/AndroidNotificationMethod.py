﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidNotificationMethod.py: The def of this file called by XML mainly.
'''

##import python common library
import time

##import framework library
import Util
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidNotificationButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLoginSignup(arg):
        '''
        ClickLoginSignup : Click login/signup button in notification page without login status
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click login/signup button in motification page without login status
        xpath = Util.GetXpath({"locate":"login_signup_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click login/signup Icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickLoginSignup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToNotificationFolder(arg):
        '''
        GoToNotificationFolder : Goto Notification Folder
                Input argu :
                    folder - notification folder type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        folder = arg["folder"]

        ##CLick notification sub folder
        xpath = Util.GetXpath({"locate":folder})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "go to " + folder + " folder", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.GoToNotificationFolder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShoppingNow(arg):
        '''
        ClickGoShoppingNow : Go to shopping now
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go shopping now
        xpath = Util.GetXpath({"locate":"shopping_now"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "click go shopping now", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickGoShoppingNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstNotification(arg):
        '''
        ClickFirstNotification : Click first notification
                Input argu :
                    page - order_updates / promotions / social_updates / seller_updates / activity / wallet_updates / return_refund
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        ret = 1

        ##Click first wallet update
        xpath = Util.GetXpath({"locate": page})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click first %s Notification" % (page), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickFirstNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollowUser(arg):
        '''
        ClickFollowUser : followe user
                Input argu :
                    type - follow / unfollow
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click to follow/unfollow user
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click %s user" % (type), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickFollowUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReplyRating(arg):
        '''
        ClickReplyRating : Click reply rating
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reply rating
        xpath = Util.GetXpath({"locate":"reply"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click reply rating", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickReplyRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerNotificationSubTab(arg):
        '''
        ClickSellerNotificationSubTab : Go to seller notification's sub tab
                Input argu :
                    subtab - my_notifications / seller_updates
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click seller notification's sub tab
        xpath = Util.GetXpath({"locate":subtab})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click subtab-> " + subtab, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickSellerNotificationSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToSellerUpdatesSubFolder(arg):
        '''
        GoToSellerUpdatesSubFolder : Go to seller update tab sub folder
                Input argu :
                    folder - order_updates / seller_wallet / marketing_centre / listing / ratings / return_refund
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        folder = arg["folder"]

        ##Go to seller update tab sub folder
        xpath = Util.GetXpath({"locate":folder})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Go to folder-> " + folder, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.GoToSellerUpdatesSubFolder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteNotification(arg):
        '''
        DeleteNotification : Delete notification
                Input argu :
                    title - notification title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]

        ##Long press notification
        xpath = Util.GetXpath({"locate":"notif"})
        xpath = xpath.replace('replace_name', title)
        AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        ##Click delete button
        xpath = Util.GetXpath({"locate":"delete_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidNotificationButton.DeleteNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNotificationToggle(arg):
        '''
        ClickNotificationToggle : click notification's toggle icon
                Input argu :
                    notif_button - push_notification / notification_sound / order_updates / chats / shopee_promotions / feed / livestream / shopee_prizes / activities / wallet_updates
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        notif_button = arg["notif_button"]

        ##toggle notif button from setting
        xpath = Util.GetXpath({"locate": notif_button})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Toggle %s button" % (notif_button), "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickNotificationToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBack(arg):
        '''
        ClickBack : Click back button
                Input argu :
                    button_page - push_notif / notif_setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        button_page = arg['button_page']
        ret = 1

        ##Click back button
        xpath = Util.GetXpath({"locate": button_page})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationButton.ClickBack')

class AndroidNotificationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToNotificationSetting(arg):
        '''
        GoToNotificationSetting : Go to notification setting
                Input argu :
                    page - default(push notificaiton page) / ringtone / seller_push_noti
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        ret = 1

        ##Click setting button
        xpath = Util.GetXpath({"locate":"setting_icon"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction":"down", "isclick":"1", "result": "1"})
        time.sleep(2)

        ##Click notification setting button
        xpath = Util.GetXpath({"locate":"notif_setting_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click notification setting button", "result": "1"})
        time.sleep(2)

        ##Click push notification button
        xpath = Util.GetXpath({"locate":"push_notif_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click push notification button", "result": "1"})
        time.sleep(2)

        ##Get into sub-folder notification setting
        if page == "default":
            dumplogger.info("Push notification page already achieve!")
        else:
            xpath = Util.GetXpath({"locate": page + "_page_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click %s notification button" % (page), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationPage.GoToNotificationSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRandomMessage(arg):
        '''
        CheckRandomMessage : Check random message in notification page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check random message in notification page
        xpath = Util.GetXpath({"locate":"push_title"})
        xpath = xpath.replace("random_msg_to_be_replaced", GlobalAdapter.GeneralE2EVar._RandomString_)
        if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "message": "Check random text","passok": "0", "result": "1"}):
            dumplogger.info("Check push notification success.")
        else:
            dumplogger.info("Check push notification fail.")
            ret = 0

        OK(ret, int(arg['result']), 'AndroidNotificationPage.CheckRandomMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetRingtoneUpdateTime(arg):
        '''
        ResetRingtoneUpdateTime : Reset ringtone update time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Long press under bar
        xpath = Util.GetXpath({"locate":"long_press_btn"})
        AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        ##Relate down and click Native features test
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"5", "result": "1"})
        xpath = Util.GetXpath({"locate":"native_features_test"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click native features test", "result": "1"})

        ##Relative down
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"16", "result": "1"})

        for retry_times in range(5):
            ##Click reset ringtone update time
            xpath = Util.GetXpath({"locate":"ringtone_update"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click reset ringtone update time", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNotificationPage.ResetRingtoneUpdateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRedirect(arg):
        '''
        CheckRedirect : Check can redirect to target page correctly in sub notification folder
                Input argu :
                    folder - order_updates / seller_updates / promotions / activity / wallet_update
                    page - home
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        folder = arg["folder"]
        page = arg["page"]

        ##Check which folder will be click
        AndroidNotificationButton.ClickFirstNotification({"page": folder, "result": "1"})

        ##Select first redirect btn in promotion folder which redirect to home page
        xpath = Util.GetXpath({"locate": page})
        AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidNotificationPage.CheckRedirect -> ' + page)

class AndroidDeviceNotificationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPushNotification(arg):
        '''
        ClickPushNotification : Click push notification and get into redirect page
                Input argu :
                    pn_content - pn content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        pn_content = arg["pn_content"]

        xpath = Util.GetXpath({"locate":"expand_btn"})
        xpath = xpath.replace("replace_shopee_region", "Shopee " + Config._TestCaseRegion_)

        ##Device receive push notification more than two from same app will need expanding the notification
        if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):

            ##Click expand button to get separate push notification
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click expand button", "result": "1"})

        else:
            ##Expand button not exist, skip action
            dumplogger.info("Expand button is not exist!")

        ##Click push notification and get into the redirect page
        xpath = Util.GetXpath({"locate":"pn_content"})
        xpath = xpath.replace("replace_pn_content", pn_content)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click push notification", "result": "1"})

        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidDeviceNotificationPage.ClickPushNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExpandNotification(arg):
        '''
        ExpandNotification : Expand notification
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Use adb command to expand device notification page
        AndroidBaseUICore.SendADBCommand({"command_type":"ExpandNotification", "adb_arg":"", "is_return": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidDeviceNotificationPage.ExpandNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CollapseNotification(arg):
        '''
        CollapseNotification : Collapse notification
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Use adb command to collapse device notification page
        AndroidBaseUICore.SendADBCommand({"command_type":"CollapseNotification", "adb_arg":"", "is_return": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidDeviceNotificationPage.CollapseNotification')
