import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCShopCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCShopCollection Precondition =="
        dumplogger.info("== Setup COPCShopCollection Precondition ==")

    def test_COPCShopCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCShopCollection-01.xml")

    def test_COPCShopCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCShopCollection-02.xml")

    def test_COPCShopCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCShopCollection-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCShopCollection Post condition =="
        dumplogger.info("== Setup COPCShopCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
