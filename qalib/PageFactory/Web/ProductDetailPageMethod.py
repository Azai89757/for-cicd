﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ProductDetailPageMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import CartMethod
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class ProductDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveProductCardHorizontally(arg):
        '''
        MoveProductCardHorizontally : Move and Choose img in product detail page
                Input argu :
                    direction - left/right
                    times - how many times to move
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        direction = arg["direction"]
        times = arg["times"]

        ##Use for loop to move product card
        for move_times in range(int(times)):
            xpath = Util.GetXpath({"locate": "next_" + direction + "_img"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Move to next " + direction + " photo", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.MoveProductCardHorizontally')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchImageInFullScreen(arg):
        '''
        SwitchImageInFullScreen : Switch image in image full screen mode
                Input argu :
                    direction - switch direction
                    times - switch time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        direction = arg["direction"]
        times = arg["times"]

        ##Use for loop to move product card
        for move_times in range(int(times)):
            xpath = Util.GetXpath({"locate": "switch_" + direction})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Move to next " + direction + " photo", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.SwitchImageInFullScreen')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EnterVideoFullScreen(arg):
        '''
        EnterVideoFullScreen : Click video to enter video full screen mode
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click the video in order to enter video full screen mode
        xpath = Util.GetXpath({"locate": "video_image"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click video image", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.EnterVideoFullScreen')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyProductQuantity(arg):
        '''
        ModifyProductQuantity : Modify product quantity using add or minus
                Input argu :
                    type - plus / minus
                    click_times - click time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        click_times = arg["click_times"]

        ##Change product buy quantity (plus or minus click_times times)
        for times in range(int(click_times)):
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click video image", "result": "1"})

        time.sleep(8)

        OK(ret, int(arg['result']), 'ProductDetailPage.ModifyProductQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectModel(arg):
        '''
        SelectModel : Select model in PDP
                Input argu :
                    model - model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        model = arg["model"]

        ##Select model of product using model name
        xpath = Util.GetXpath({"locate": "model_name"})
        xpath = xpath.replace("name_to_be_replaced", model)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose model", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.SelectModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCartItemQuantity(arg):
        '''
        CheckCartItemQuantity : Check quantity of item in cart
                Input argu :
                    number - number of item in cart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Check number of item in cart in PDP page
        xpath = Util.GetXpath({"locate": "cart_quantity"})
        xpath = xpath.replace("string_to_be_replaced", number)

        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Quantity not correct !!!")
            ret = 0

        OK(ret, int(arg['result']), 'ProductDetailPage.CheckCartItemQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductPicture(arg):
        '''
        ClickProductPicture : Click product picture
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product picture
        xpath = Util.GetXpath({"locate": "pdp_product_picture"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product picture", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.ClickProductPicture')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealPicture(arg):
        '''
            ClickAddOnDealPicture : Click add on deal picture
                Input argu :
                    addon_product_name - addon product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        addon_product_name = arg["addon_product_name"]

        ##Click add on deal picture
        xpath = Util.GetXpath({"locate": "addon_product_picture"})
        xpath = xpath.replace("addon_product_name_to_be_replace", addon_product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal picture", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.ClickAddOnDealPicture')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductVariation(arg):
        '''
        SelectProductVariation : Goto product detail page to select product variation
                Input argu :
                    variation - product category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        variation = arg["variation"]

        ##Click product variation in product detail page
        xpath = Util.GetXpath({"locate": "variation"})
        xpath = xpath.replace("replaced_variation", variation)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click variation " + variation, "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.SelectProductVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRcmdProduct(arg):
        '''
        ClickRcmdProduct : Click rcmd product
                Input argu :
                    section - rcmd section
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg['section']
        product_name = arg['product_name']

        ##Click product in section
        xpath = Util.GetXpath({"locate": "item"})
        xpath = xpath.replace("replace_section_name", section)
        xpath = xpath.replace("replace_product_name", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rcmd item", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.ClickRcmdProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputQuantity(arg):
        '''
        InputQuantity : Goto product detail page and input quantity
                Input argu :
                    quantity - input your quantity
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        quantity = arg["quantity"]

        ##Input quantity
        xpath = Util.GetXpath({"locate": "input_qty"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": quantity, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'ProductDetailPage.InputQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToCart(arg):
        '''
        GoToCart : Click cart button on product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Cart icon
        xpath = Util.GetXpath({"locate": "cart_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart btn", "result": "1"})

        ##Retry refresh to confirm cart page is loaded
        CartMethod.CartPage.WaitCartLoadingIsComplete({"result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.GoToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseInstallmentBank(arg):
        '''
        ChooseInstallmentBank : Choose installment bank in PDP
                Input argu :
                    bank - citibank / hsbc / shinhan / standard_chartered / hdbank / scb / tpbank / sacombank
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg["bank"]

        ##Click bank
        xpath = Util.GetXpath({"locate": bank})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bank", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailPage.ChooseInstallmentBank')


class ProductDetailButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCart(arg):
        '''
        ClickCart : Click cart button on product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart btn
        xpath = Util.GetXpath({"locate": "cart_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cart btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLike(arg):
        '''
        ClickLike : Click Like Button in product detail page
                    Input argu : N/A
                    Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##click like button
        xpath = Util.GetXpath({"locate": "click_like_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click like button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickLike')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart button on product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add to cart btn
        xpath = Util.GetXpath({"locate": "add_to_cart_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAlready18y(arg):
        '''
        ClickAlready18y : Goto product detail page and click i am already 18 years old
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click already 18y button
        xpath = Util.GetXpath({"locate": "already_18y"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAlready18y')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNotAlready18y(arg):
        '''
        ClickNotAlready18y : Goto product detail page and click i am not already 18 years old
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click not already 18y button
        xpath = Util.GetXpath({"locate": "not_already_18y"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click not already 18 years button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickNotAlready18y')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Goto product detail page and click buy now
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy now button
        xpath = Util.GetXpath({"locate": "product_page_label_buy_now"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click buy now btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRCMDNextPage(arg):
        '''
        ClickRCMDNextPage : Click RCMD next arrow
                Input argu :
                    rcmd_type - label_from_same_shop / similar_products / you_may_also_like
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rcmd_type = arg["rcmd_type"]

        ##Click RCMD next arrow
        xpath = Util.GetXpath({"locate":rcmd_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click RCMD next arrow", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickRCMDNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSocialMedia(arg):
        '''
        ClickSocialMedia : Click Social Media in product detail page
                Input argu :
                    type - click Social Media share
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##click fb share button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click social media", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickSocialMedia')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingFeeDropDown(arg):
        '''
        ClickShippingFeeDropDown : Click shipping fee drop down in product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click shipping fee drop down
        xpath = Util.GetXpath({"locate": "ship_fee_drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click shipping fee drop down", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickShippingFeeDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseWholeSale(arg):
        '''
        ClickCloseWholeSale : Close pop windows in product detail page
                Input argu :
                    type - close Wholesale popup
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##close Wholesale popup
        xpath = Util.GetXpath({"locate": "click_Wholesale_close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close Pop Window", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickCloseWholeSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickWholeSale(arg):
        '''
        ClickWholeSale : Click Whole Sale in product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click wholesale button
        xpath = Util.GetXpath({"locate": "click_wholesale_common"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click whole sale common", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickWholeSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductVariation(arg):
        '''
        ClickProductVariation : Click product variation
                Input argu :
                    variation_name - product category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        variation_name = arg["variation_name"]

        ##Click product variation
        xpath = Util.GetXpath({"locate": "variation_btn"})
        xpath_variation_btn = xpath.replace("variation_name_to_be_replace", variation_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_variation_btn, "message": "Click product variation", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickProductVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealViewMore(arg):
        '''
        ClickAddOnDealViewMore : Click add on deal view more
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal view more
        xpath = Util.GetXpath({"locate": "aod_button_view_more"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal view more", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddOnDealViewMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealAddToCart(arg):
        '''
        ClickAddOnDealAddToCart : Click add on deal add to cart
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal add to cart
        xpath = Util.GetXpath({"locate": "aod_button_add_to_cart"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal add to cart", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddOnDealAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealGoToCart(arg):
        '''
        ClickAddOnDealGoToCart : Click add on deal go to cart
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal add to cart
        xpath = Util.GetXpath({"locate": "aod_button_go_to_cart"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal go to cart", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddOnDealGoToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetVoucher(arg):
        '''
        ClickGetVoucher : Click get voucher if there's any in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click get voucher button in PDP
        xpath = Util.GetXpath({"locate": "get_shop_voucher"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click get voucher button in PDP", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickGetVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealCheckBox(arg):
        '''
        ClickAddOnDealCheckBox : Click add on deal item checkbox in add on deal section
                Input argu :
                    product_name - add on deal item name which you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click add on deal item checkbox in add on deal section
        xpath = Util.GetXpath({"locate": "add_on_deal_checkbox"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddOnDealCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealVariationMenu(arg):
        '''
        ClickAddOnDealVariationMenu : Click add on deal product drop-down menu in product detail page
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click drop down
        xpath = Util.GetXpath({"locate": "addon_variation_drop_down"})
        xpath = xpath.replace('product_name_to_be_replace', product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddOnDealVariationMenu')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDealVariationMenuConfirm(arg):
        '''
        ClickAddOnDealVariationMenuConfirm : Click confirm in add on deal product drop-down menu in product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm
        xpath = Util.GetXpath({"locate": "aod_button_confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickAddOnDealVariationMenuConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleDealSeeMore(arg):
        '''
        ClickBundleDealSeeMore : Click bundle deal see more
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bundle deal see more
        xpath = Util.GetXpath({"locate": "bundle_deal_see_more"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle deal see more btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickBundleDealSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLowerRightChatBox(arg):
        '''
        ClickLowerRightChatBox : Click chat box at the lower right of the page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click chat box at lower right
        xpath = Util.GetXpath({"locate": "chat_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click chat box at lower right", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickLowerRightChatBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewShop(arg):
        '''
        ClickViewShop : Click view shop btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view shop btn
        xpath = Util.GetXpath({"locate": "view_shop_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view shop btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickViewShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreButton(arg):
        '''
        ClickSeeMoreButton : Click different type of see more button
                Input argu :
                    type - flashsale / bundle_deal / add_on_deal / voucher / purchase_with_gift
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click see more
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type + " see more", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickSeeMoreButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopTag(arg):
        '''
        ClickShopTag : Click shop tag in shop section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shop tag in shop section
        xpath = Util.GetXpath({"locate": "shop_tag"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop tag in shop section", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickShopTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeGuarantee(arg):
        '''
        ClickShopeeGuarantee : Click shopee guarantee btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shopee guarantee btn
        xpath = Util.GetXpath({"locate": "guarantee_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shopee guarantee btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickShopeeGuarantee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionPicture(arg):
        '''
        ClickPromotionPicture : Click promotion picture
                Input argu :
                    promotion_type - bundle_deal / purchase_with_gift
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg['promotion_type']

        ##Click promotion section picture
        xpath = Util.GetXpath({"locate": promotion_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion picture", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickPromotionPicture')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherShopAvatar(arg):
        '''
        ClickVoucherShopAvatar : Click voucher shop avatar in voucher drawer
                Input argu :
                    index - which voucher in voucher drawer
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Click voucher shop avatar in voucher drawer
        xpath = Util.GetXpath({"locate": "voucher_shop_avatar"})
        xpath = xpath.replace("voucher_index_to_be_replace", index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher shop avatar", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickVoucherShopAvatar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherText(arg):
        '''
        ClickVoucherText : Click voucher text in voucher drawer or voucher section
                Input argu :
                    index - which voucher text you want to click
                    type - voucher_drawer / voucher_section
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']
        type = arg['type']

        ##Click voucher text in voucher drawer or voucher section
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace("voucher_index_to_be_replace", index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher text", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickVoucherText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherClaim(arg):
        '''
        ClickVoucherClaim : Click voucher claim btn in voucher drawer or voucher section
                Input argu :
                    index - which voucher text you want to click
                    type - voucher_drawer / voucher_section
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']
        type = arg['type']

        ##Click voucher claim btn in voucher drawer or voucher section
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace("voucher_index_to_be_replace", index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher claim btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickVoucherClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExclusivePriceRightIcon(arg):
        '''
        ClickExclusivePriceRightIcon : Click exclusive price right icon in price section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exclusive price right icon in price section
        xpath = Util.GetXpath({"locate": "exclusive_price_right_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click exclusive price right icon", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickExclusivePriceRightIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserGroupUrl(arg):
        '''
        ClickUserGroupUrl : Click exclusive price user group url
                Input argu :
                    url_text - url text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        url_text = arg['url_text']

        ##Click exclusive price user group url
        xpath = Util.GetXpath({"locate": "group_url"})
        xpath = xpath.replace("text_to_be_replace", url_text)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click exclusive price user group url", "result": "1"})

        OK(ret, int(arg['result']), 'ProductDetailButton.ClickUserGroupUrl')
