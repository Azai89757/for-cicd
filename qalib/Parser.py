#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 Parser.py: For parser xml file and handle
'''

import sys
import inspect
import time
import os
import traceback
import Config
from Config import dumplogger, chrome_logger
import FrameWorkBase
import XtFunc
import Util
from Warn import warn
import datetime
import GlobalAdapter
import DecoratorHelper
import re
import LoadImports
import json

##Import Web
from PageFactory.Web import *

##Import Android
from PageFactory.Android import *

##Import iOS
from PageFactory.iOS import *

##Import Admin
from PageFactory.Admin import *
from PageFactory.Admin.Payment import *
from PageFactory.Admin.Order import *
from PageFactory.Admin.Discoverlanding import *
from PageFactory.Admin.ProductListing import *
from PageFactory.Admin.Growth import *
from PageFactory.Admin.Promotion import *
from PageFactory.Admin.Productlabel import *
from PageFactory.Admin.UserPortal import *

##Import API Related Scripts
from api import *
from api.http import *
from api.socket import *
from api.spex import *

##Import db Related Scripts
from db import DBCommonMethod
from db.Order import *

_CTX_ = {}
_CTX2_ = {}
_LINEFLAG_ = 0

##_CASE_['status'] 0: TestCase sucess / 1 or > 1 : not ok amouts
##_CASE_['txt'] not ok txt record during one case
##_CASE_['failskip'] -> Current test case will skip if test action fail
##_CASE_['funcname'] -> Function name from xml
##_CASE_['funcname_inline'] -> Function name from inline code
##_CASE_['funcname_cur'] -> Current function name for inline code faced special rule
_CASE_ = {'id':'', 'txt':'', 'counts':0, 'status':0, 'fcounts':0, 'flist':'', 're_flist':'', 'stime':0, 'oktime':0, 'failskip':0, 'funcname':''}

##Stored all of func for classmethod/staticmethod/decorator
_FuncListForXML_ = []

##Append functions(.py) called by XML dynamically
list_func_obj = []

##Get all the folders under qalib
for root, dirs, files in os.walk(os.path.dirname(os.path.abspath(__file__))):
    ##Skip .py in Testsuites
    if 'Testsuites' in dirs:
        dirs.remove('Testsuites')

    ##For all sub directory
    for sub_dir in dirs:
        dir_path = os.path.join(root, sub_dir)
        dumplogger.info("Sub directory path: %s" % (dir_path))

        ##Get all files under this directory
        files = os.listdir(dir_path)

        ##For each file in directory
        for each_file in files:
            file_name = each_file.split('.')
            if len(file_name) > 1:
                if file_name[1] == 'py' and file_name[0] != '__init__':
                    ##Get the format of package location
                    file_path = os.path.join(dir_path, file_name[0])
                    #dumplogger.info("File location: %s" % (file_path))

                    ##Replace file path from /QATest/qalib/*/* (linux or mac) QATest\\qalib\\*\\* (windows) to qalib.*.*
                    file_path = file_path.replace(re.findall(r'.*qalib.', file_path)[0], '')
                    file_path = file_path.replace(Config.dict_systemslash[Config._platform_], '.')
                    #dumplogger.info("Package location: %s" % (file_path))

                    if LoadImports.DynamicImportModule(file_path) != -1:
                        ##Append the module to list
                        list_func_obj.append(LoadImports.DynamicImportModule(file_path))

##Only the XtFunc.py under ./qalib will be called in xml, so append it specifically
list_func_obj.append(LoadImports.DynamicImportModule('XtFunc'))
dumplogger.info(list_func_obj)

dict_func_detail = {}
##Declare new function structure
for each_file in list_func_obj:
    try:
        file_str = each_file.__name__.split('.')[-1]
        dict_func_detail[each_file] = {}
        dict_func_detail[each_file]["name"] = file_str
        dict_func_detail[each_file]["obj"] = each_file
        dict_func_detail[each_file]["funcbase"] = None
        dict_func_detail[each_file]["funcdec"] = None
        dict_func_detail[each_file]["funcstatic"] = None
    except KeyError:
        #print "Encounter KeyError !!!!"
        dumplogger.exception("Encounter KeyError !!!!")
    except:
        #print "Encounter Other Error!!!!"
        dumplogger.exception("Encounter Other Error!!!!")

###############################################################

##Sequence to load func from list
for temp_fun in list_func_obj:

    ##If dict have key(function)
    #if dict_func_detail.has_key(temp_fun):
    if temp_fun in dict_func_detail:
        ##Get def from file(.py)
        dict_func_detail[temp_fun]["funcbase"] = [func for func, data in inspect.getmembers(temp_fun, inspect.isroutine)]
        ##Get staticmethod from file(.py)
        dict_func_detail[temp_fun]["funcstatic"] = [name + "." + temp for name, obj in inspect.getmembers(temp_fun) if inspect.isclass(obj) if "__IsForTest__" in dir(obj) for temp in dir(obj) if "__" not in temp]
        ##Get decorator from file(.py)
        dict_func_detail[temp_fun]["funcdec"] = [name for name, obj in inspect.getmembers(temp_fun, inspect.ismethod(object)) if isinstance(obj, DecoratorHelper.FuncRecorder)]
        ##Extend xml list
        _FuncListForXML_.extend(dict_func_detail[temp_fun]["funcbase"])
        _FuncListForXML_.extend(dict_func_detail[temp_fun]["funcstatic"])
        _FuncListForXML_.extend(dict_func_detail[temp_fun]["funcdec"])

    else:
        dumplogger.error("%s ->Please check your function declare !!!!" % (temp_fun))


def start_element(func, attrs):
    '''start_element: Filter start element in XML
    '''
    #print "Enter start_element"
    #print func
    #print attrs

    global _CTX_, _CTX2_, _CASE_
    _CTX_ = attrs

    if func == "TestCase":
        ##Save case start time
        Config._CaseStartTime_ = datetime.datetime.fromtimestamp(time.time()).strftime("%m-%d %H:%M:%S.000")

        ##Add debug log when test case Start
        #dumplogger.info("[%s]" %(_CASE_['id']))

        dumplogger.info(func)
        chrome_logger.info(func)
        #dumplogger.info(sys.argv[2])

        ##Stored test case id
        if Config._RerunList_:
            if _CTX_['id'] in Config._RerunList_:
                _CASE_['id'] = FrameWorkBase.FilterCaseID(_CTX_['id'])
            else:
                _CASE_['id'] = None
        else:
            _CASE_['id'] = FrameWorkBase.FilterCaseID(_CTX_['id'])
        dumplogger.info(_CASE_['id'])

        ##Stored test case status
        _CASE_['status'] = 0

        ##Reset failskip
        _CASE_['failskip'] = 0

        ##Set color to default color in cmd
        ##windows only -> FrameWorkBase.set_cmd_text_color(FrameWorkBase.FOREGROUND_RED | FrameWorkBase.FOREGROUND_GREEN | FrameWorkBase.FOREGROUND_BLUE)
        FrameWorkBase.set_cmd_text_color("white")

        if _CASE_['id']:

            if _CASE_['id'] in Config._CaseRelationShip_:
                ##If case is restore case, will keep to run.
                if "99" in _CASE_['id'].split("-")[1][2:4]:
                    dumplogger.info("This case %s is restore case, will keep to run this case!!!" % (_CASE_['id']))
                else:
                    dumplogger.info("This case %s is in Config._CaseRelationShip_: %s, will skip this case!!!" % (_CASE_['id'], Config._CaseRelationShip_))
                    _CASE_['id'] = None
                    return
            else:
                ##Reset case relationship list
                Config._CaseRelationShip_ = []

            ##Deal with LOGs
            if '-x' in FrameWorkBase._OPT_:
                sys.stdout = Config._NULL_
                sys.stderr = Config._NULL_
            elif '-l' in FrameWorkBase._OPT_:
                folder = Config._DIR_+"/"+Config._LogFolder_+"/"+_CASE_['id']
                fname = Config._DIR_+"/"+Config._LogFolder_+"/"+_CASE_['id']+"/"+_CASE_['id']+".log"
                if not os.path.exists(folder):
                    os.makedirs(folder)
                _LOG_ = open(fname, 'w')
                sys.stdout = _LOG_
                sys.stderr = _LOG_

            print "[" + _CASE_['id'] + "]"
            dumplogger.info("[" + _CASE_['id'] + "]")
            chrome_logger.info("[" + _CASE_['id'] + "]")

            ##Deal with TestCase XML attributes
            if 'os' in _CTX_:
                print "\tThe case only runs in " + _CTX_['os'] + "OS."
            if 'platform' in _CTX_:
                if Config._Platform_ != _CTX_['platform']:
                    print "\tThe case only runs in " + _CTX_['platform'] + " platform."
                    _CASE_['id'] = None

            if 'skip' in _CTX_:
                ##print  "\tskip="+_CTX_['skip']
                if _CTX_['skip'] == "1":
                    print "\tThe case %s is skipped." % _CASE_['id']
                    _CASE_['id'] = None

            ##If trigger case mode is HIGH, will execute include golden and high priority cases
            ##All case should be triggered by default if there is not assigning any trigger mode at the beginning
            if Config._TriggerCaseMode_ == "high":
                if 'priority' in _CTX_:
                    ##Priority GOLDEN and HIGH cases will be excuted
                    if _CTX_['priority'].lower() in ("golden", "high"):
                        dumplogger.info("The case %s is %s priority, will execute this case!!!" % (_CASE_['id'], _CTX_['priority']))
                    ##Case will not be triggered if prioity is not given
                    else:
                        dumplogger.info("The case %s is %s priority, will skip this case!!!" % (_CASE_['id'], _CTX_['priority']))
                        _CASE_['id'] = None

            ##If trigger case mode is GOLDEN/MEDIUM/LOW, will only execute the corresponding priority cases
            elif Config._TriggerCaseMode_ in ("medium", "low", "golden"):
                if 'priority' in _CTX_:
                    ##Only priority GOLDEN/MEDIUM/LOW cases will be excuted
                    if _CTX_['priority'].lower() == Config._TriggerCaseMode_:
                        dumplogger.info("The case %s is %s priority, will execute this case!!!" % (_CASE_['id'], _CTX_['priority']))
                    ##Case will not be triggered if prioity is not given
                    else:
                        dumplogger.info("The case %s is %s priority, will skip this case!!!" % (_CASE_['id'], _CTX_['priority']))
                        _CASE_['id'] = None

                else:
                    ##If there is no priority tag in xml, then this case will be considered as "LOW" prioity
                    _CTX_['priority'] = "LOW"

                    ##If no priority tag, this case will be triggered only when _TriggerCaseMode_ is "LOW"
                    if Config._TriggerCaseMode_ != "low":
                        dumplogger.info("Since there is no priority tag for this case, it will be assigned to low priority.")
                        dumplogger.info("The case %s is %s priority, will skip this case!!!" % (_CASE_['id'], _CTX_['priority']))
                        _CASE_['id'] = None
                    else:
                        dumplogger.info("The case %s is %s priority, will execute this case!!!" % (_CASE_['id'], _CTX_['priority']))
            else:
                dumplogger.info("Config._TriggerCaseMode_ not assigned, will execute all the case!!!")

            ##Renew test case count and stime
            if _CASE_['id']:
                _CASE_['stime'] = time.time()
                ##Rerun case won't need to count number.
                if not Config._RerunList_:
                    _CASE_['counts'] += 1

            ##Initial GlobalAdapter.CommonVar._FailedCaseInfo_ when start case
            GlobalAdapter.CommonVar._FailedCaseInfo_ = {
                "fail_time": Config._LogCurDateTime_,
                "line_number": 0,
                "fail_func": "",
                "step_dependency": [],
                "screenshot_path": "",
                "ok_msg": "",
                "exception_msg": "",
                "note": {},
                "log_file_path": "",
                "api_response": "",
                "api_url": ""
            }
            ##Initial GlobalAdapter.CommonVar when start case
            GlobalAdapter.CommonVar._Start_Img_ = []
            GlobalAdapter.CommonVar._Retry_Start_Img_ = []
            GlobalAdapter.CommonVar._Function_Name_ = []
            GlobalAdapter.CommonVar._Retry_Function_Name_ = []

    if not _CASE_['id']:
        return
#   if func=="Notify":
#       _CTX2_=attrs
#       print "\tNotifyStart()---%<---%<---"
#       _CASE_['oktime']=time.time()
#       XtFunc.NotifyStart(_CTX2_)

def end_element(func, attrs=""):
    '''end_element: Filter end element in XML
    '''

    global _CTX_, _CTX2_, _CASE_, _XtFuncs_, _SpFuncs_, _DcgsFuncs_
    _CTX_ = attrs

    #dumplogger.info("Enter end_element")
    #dumplogger.info(_CASE_)

    if _CASE_['id'] is None:
        return

    if func == "TestCase":

        #warn("end_element Enter TestCase")
        ##Fail case status
        if _CASE_['status'] != 0:

            if Config._RerunList_:
                ##Store failed case list for rerun mode
                if _CASE_['re_flist']:
                    _CASE_['re_flist'] = _CASE_['re_flist'] + "," + _CASE_['id']
                else:
                    _CASE_['re_flist'] = _CASE_['id']
            else:
                ##Store failed case list for usual mode
                if _CASE_['flist']:
                    _CASE_['flist'] = _CASE_['flist'] + "," + _CASE_['id']
                else:
                    _CASE_['flist'] = _CASE_['id']

                ##Only first time failed will count fail amount here
                _CASE_['fcounts'] += 1

            ##Fail case output to "QATest_Python_Failed.log"
            #flistfilepath = os.getcwd() + '\\QATest_Python_Failed.log'
            #file = open(flistfilepath, 'w')
            #file.write(_CASE_['flist'])
            #file.close()

            ##Load parameter to split test case type and id
            if sys.argv[2].find('-') > 0:
                input = sys.argv[2].split('-')
                testcase_type = input[0]
                testcase_id = input[1]
            else:
                testcase_type = sys.argv[1]

            '''
            ##Rerun fail case result output to "R_TestCaseType_QATest_Python_Failed.log"
            if _RERUN_ == '1':
                rerunflistfilepath = os.getcwd() + '\\R_' + testcase_type + '_QATest_Python_Failed.log'
                rerunfile = open(rerunflistfilepath, 'w')
                rerunfile.write(_CASE_['flist'])
                rerunfile.close()
            '''

            ##Prevent browser still exist If test action fail
            if _CASE_['failskip'] == 1:
                Config._CaseFailList_.append(_CASE_['id'])
                #print Config._CaseFailList_
                _CASE_['failskip'] = 0

                ##Get failed case relationship
                Config._CaseRelationShip_ = Config._RerunObj_.RRBotMain([_CASE_['id']])
                dumplogger.info("Config._CaseRelationShip_ = %s" % (Config._CaseRelationShip_))

                ##Split dump log file by each case when case fail, and save file path for SQTP data
                if Config._RerunList_:
                    ##File path could be different due to rerun
                    each_case_log_file_path = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + "_rerun" + Config.dict_systemslash[Config._platform_] + _CASE_['id'] + "_Dump.log"
                    each_case_chrome_log_file_path = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + "_rerun" + Config.dict_systemslash[Config._platform_] + _CASE_['id'] + "_chrome.log"
                else:
                    each_case_log_file_path = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_] + _CASE_['id'] + "_Dump.log"
                    each_case_chrome_log_file_path = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_] + _CASE_['id'] + "_chrome.log"
                ##Open Dump_QATest log file and split latest case log to new file
                with open(Config._OutputFolderPath_ + "/Dump_QATest.log", 'r') as all_dumplog_file:
                    all_dumplog = all_dumplog_file.read()
                    splited_dumplog = all_dumplog.rsplit("[start_element]|TestCase\n", 1)[1]
                    with open(each_case_log_file_path, "w+") as splited_dumplog_file:
                        splited_dumplog_file.write(splited_dumplog)
                ##Open Chrome_Recorder log file and split latest case log to new file
                with open(Config._OutputFolderPath_ + "/Chrome_Recorder.log", 'r') as chrome_log_file:
                    all_chrome_log = chrome_log_file.read()
                    splited_chrome_log = all_chrome_log.rsplit("[start_element]|TestCase\n", 1)[1]
                    with open(each_case_chrome_log_file_path, "w+") as splited_chrome_log_file:
                        splited_chrome_log_file.write(splited_chrome_log)
                ##APIlogs and Autologs file path are different
                if Config._TestCasePlatform_.lower() in ("http", "spex"):
                    GlobalAdapter.CommonVar._FailedCaseInfo_["log_file_path"] = "\\\\DS07\\1_Department\\Marketplace - QA\\APILogs" + each_case_log_file_path.split("QATest_Output")[1]
                else:
                    GlobalAdapter.CommonVar._FailedCaseInfo_["log_file_path"] = "\\\\DS07\\1_Department\\Marketplace - QA\\AutoLogs" + each_case_log_file_path.split("QATest_Output")[1]

                #########################
                ##Special coniditions - All functions reach below should not enter OK to prevent case jump out to teardown
                #########################
                ##Quit Android driver and Re-Initial android app if testcase type is Android
                if "android" in Config._TestCaseType_.lower():

                    try:
                        ##DeInitialDriver
                        AndroidBaseUICore.AndroidDeInitialDriver({})
                        dumplogger.info("Android Driver Error Handle to prevent handle still exist - Try")
                        ##Kill appium if case fail
                        AndroidBaseUICore.KillAppium({"isFail":"1"})
                        dumplogger.info("Kill Appium")

                    except:
                        dumplogger.exception("Android Driver Error Handle to prevent handle still exist - Except")

                    try:
                        ##Kill web driver to prevent browser exist
                        BaseUICore.DeInitialWebDriver({})
                        dumplogger.info("WebDriver Error Handle to prevent browser still exist - Try")
                    except:
                        dumplogger.exception("WebDriver Error Handle to prevent browser still exist - Except")

                    ##Re-Initial Appium
                    AndroidBaseUICore.StartAppium({})
                    ##Re-Initial Android driver
                    AndroidBaseUICore.AndroidInitialDriver({"env":"staging", "country":Config._TestCaseRegion_.lower()})
                    dumplogger.info("Re-Initial android app due to test case failure")

                ##Quit Android driver and Re-Initial android app if testcase type is Android
                elif "ios" in Config._TestCaseType_.lower():

                    try:
                        ##DeInitialDriver
                        iOSBaseUICore.iOSDeInitialDriver({})
                        dumplogger.info("iOS Driver Error Handle to prevent handle still exist - Try")
                        ##Kill appium if case fail
                        iOSBaseUICore.iOSKillXcodeBuild({"isFail":"1"})
                        dumplogger.info("Kill Appium")

                    except:
                        dumplogger.exception("iOS Driver Error Handle to prevent handle still exist - Except")

                    try:
                        ##Kill web driver to prevent browser exist
                        BaseUICore.DeInitialWebDriver({})
                        dumplogger.info("WebDriver Error Handle to prevent browser still exist - Try")
                    except:
                        dumplogger.exception("WebDriver Error Handle to prevent browser still exist - Except")

                    ##Re-Initial Appium
                    #iOSBaseUICore.iOSBuildWDAToDevice({"device_type":Config._MobileDeviceInfo_[Config._DeviceID_]["device_name"], "uuid":Config._DeviceID_})
                    ##Re-Initial iOS driver
                    time.sleep(5)
                    iOSBaseUICore.iOSInitialDriver()
                    dumplogger.info("Re-Initial ios driver due to test case failure")

                ##Quit Web driver
                elif "pc" in Config._TestCaseType_.lower():
                    try:
                        BaseUICore.DeInitialWebDriver({})
                        dumplogger.info("WebDriver Error Handle to prevent browser still exist - Try")
                    except:
                        dumplogger.exception("WebDriver Error Handle to prevent browser still exist - Except")

                dumplogger.info("_CASE_[failskip] = %s" % (_CASE_['failskip']))
                #########################

        ##If this case rerun and passed at the second time
        elif Config._RerunList_:

            ##If this passed case is fail in first time, fcounts should -1
            if _CASE_['id'] in Config._CaseFailList_:
                _CASE_['fcounts'] -= 1

        ##Delete "QATest_Python_Failed.log" file if rerun pass
        elif len(_CASE_['flist']) == 0:
            path = Config._CurDIR_ + '\\QATest_Python_Failed.log'
            if os.path.exists(path):
                #print "remove failed log"
                os.remove(path)

        elapse = time.time() - _CASE_['stime']
        print "[" + _CASE_['id'] + "] END...%.2f sec\n" % elapse

        ##Add debug log when test case END
        dumplogger.info("[" + _CASE_['id'] + "] END...%.2f sec\n" % elapse)

        try:
            ##Reset is_send_log_to_finetune
            is_send_log_to_finetune = 0

            ##If given build number contains PSID, then send log to finetuneDB instead of AutoLogDB
            if Config._BuildNo_.split(".")[1] in Config._PSIDForAutoFineTuneDB_:
                is_send_log_to_finetune = 1
                dumplogger.info("is_send_log_to_finetune -> %s" % (is_send_log_to_finetune))

            ##Check hostname is in white list for AutoLogDB
            if Config._HostName_ in Config._WLForAutoLogDB_:
                if is_send_log_to_finetune:
                    FrameWorkBase.SaveDailyResultToDB("AutoFineTune", _CASE_['id'], Config._LogCurDateTime_, "%.2f" % (elapse), Config._BuildNo_, _CASE_['status'], 1)
                    dumplogger.info("Enter _WLForAutoFineTuneDB_ -> %s for -> TWSP-%s" % (Config._HostName_, Config._BuildNo_.split(".")[1]))

                else:
                    ##Update daily run result in twqa_auto_db.auto_daily_report_tab
                    FrameWorkBase.SaveDailyResultToDB("InsertAutoLog", _CASE_['id'], Config._LogCurDateTime_, "%.2f" % (elapse), Config._BuildNo_, _CASE_['status'], 1)
                    dumplogger.info("Enter _WLForAutoLogDB_ -> %s" % (Config._HostName_))
                    if _CASE_['status'] or Config._RerunList_:
                        ##Update daily run fail log in twqa_auto_db.auto_daily_fail_log_collection_tab only when case fail
                        ##In rerun mode, update valid=0 of failed case and insert new one
                        FrameWorkBase.SaveDailyFailLogToDB(
                            "UpdateDailyFailLogCollection",
                            _CASE_['id'], GlobalAdapter.CommonVar._FailedCaseInfo_["fail_time"], Config._BuildNo_, _CASE_['status'], 1, Config._TestCaseRegion_, Config._TestCasePlatform_, Config._TestCaseFeature_,
                            GlobalAdapter.CommonVar._FailedCaseInfo_["line_number"], GlobalAdapter.CommonVar._FailedCaseInfo_["fail_func"], ",".join(GlobalAdapter.CommonVar._FailedCaseInfo_["step_dependency"]),
                            GlobalAdapter.CommonVar._FailedCaseInfo_["screenshot_path"], GlobalAdapter.CommonVar._FailedCaseInfo_["ok_msg"], GlobalAdapter.CommonVar._FailedCaseInfo_["exception_msg"],
                            json.dumps(GlobalAdapter.CommonVar._FailedCaseInfo_["note"]), GlobalAdapter.CommonVar._FailedCaseInfo_["log_file_path"], GlobalAdapter.CommonVar._FailedCaseInfo_["api_response"], GlobalAdapter.CommonVar._FailedCaseInfo_["api_url"])
                        dumplogger.info("Update fail log to DB successful!")

            ##If hostname belongs to white list for FinetuneDB, update auto_finetune_report_tab instead
            elif Config._HostName_ in Config._WLForAutoFineTuneDB_:
                if is_send_log_to_finetune:
                    FrameWorkBase.SaveDailyResultToDB("AutoFineTune", _CASE_['id'], Config._LogCurDateTime_, "%.2f" % (elapse), Config._BuildNo_, _CASE_['status'], 1)
                    dumplogger.info("Enter _WLForAutoFineTuneDB_ -> %s for -> TWSP-%s" % (Config._HostName_, Config._BuildNo_.split(".")[1]))
                else:
                    dumplogger.info("Won't send log to FineTune DB since specific build number is not given!!!")

            else:
                dumplogger.info("%s is not in the white list of GlobalAdapter.py, won't need to update DB record!! " % (Config._HostName_))
        except:
            dumplogger.exception("Insert automation log to DB fail, if you run QATest in local, please ignore...")

        ##Reset default value
        _CASE_['id'] = None
        _CASE_['txt'] = None
        GlobalAdapter.CommonVar._XMLStack_ = []

    ##If you would like add new library, please add in here.
    #elif func in _XtFuncs_ or func in _BaseUILogic_ or func in _BaseUICore_ or func in _PageFactory_ or func in _AndroidBaseUICore_ or func in _AndroidBaseUILogic_ or func in _AndroidOrderMethod_ or func in _AndroidPaymentMethod_:
    elif func in _FuncListForXML_:

        #warn("end_element Enter _XtFuncs_")
        #print func
        #print _CASE_

        ##Stored function name
        _CASE_["funcname"] = func
        dumplogger.info("_CASE_[funcname] = %s" % (_CASE_["funcname"]))

        ##############################################################################
        ##Reset function memory stack
        dumplogger.info("Reset GlobalAdapter.CommonVar._MemoryStack_")
        GlobalAdapter.CommonVar._MemoryStack_ = []
        dumplogger.info(GlobalAdapter.CommonVar._MemoryStack_)
        ##############################################################################

        try:
            _CASE_['oktime'] = time.time()

            ##Whether have setp command in argv
            if Config._Step_:
                print "\tReady to run " + func + "() ...."
                os.system("pause")

            #warn(_CASE_['failskip'])

            if _CASE_['failskip'] == 0:
                ##############################################################################
                ##For flag version in XML
                ##Extract xpath from json file
                if "locate" in attrs:
                    new_locate = Util.GetXpath(attrs)
                    attrs["locate"] = new_locate
                    dumplogger.info("attrs[locate] = %s" % (attrs["locate"]))

                ##############################################################################

                ##Dynamic to load all of function declare
                print "[Prepare enter %s]" % (func)
                for temp_fun in list_func_obj:
                    ##If dict have key(function)
                    #if dict_func_detail.has_key(temp_fun):
                    if temp_fun in dict_func_detail:
                        ##If current fun in list/staticmethod/decorator
                        if func in dict_func_detail[temp_fun]["funcbase"] or func in dict_func_detail[temp_fun]["funcdec"] or func in dict_func_detail[temp_fun]["funcstatic"]:
                            GlobalAdapter.CommonVar._FuncStack_ = []
                            GlobalAdapter.CommonVar._XMLStack_.append(dict_func_detail[temp_fun]["name"] + "." + func + "(" + str(_CTX_) + ")")
                            #dumplogger.info("Beginning test step... %s" % (func))
                            AndroidBaseUICore.AndroidGetAndStorgeCurrentImage({"img": GlobalAdapter.CommonVar._Start_Img_, "function_name": _CASE_['funcname'], "function_status": GlobalAdapter.CommonVar._Function_Name_})
                            eval(dict_func_detail[temp_fun]["name"] + "." + func + "(_CTX_)")

        ##This exception is "only" for FrameworkBase.OK. when executing step failed it will call quit() to raise SystemExit to stop function running.
        except SystemExit:
            dumplogger.exception("Getting system exit, will skip remaining step in this function and skip current running case!")
        except:
            print "\t",
            dumplogger.exception("Please check exception log for further infomation !!!")

            ##When result is not in xml key, terminate this test case
            try:
                _CTX_['result']
            except KeyError:
                print "Please remember to add result key in XML !!!"
                dumplogger.exception("Please remember to add result key in XML !!!")
            finally:
                error_message = str(func) + ": " + str(sys.exc_info()[1])
                FrameWorkBase.OK(Config._Exception_, 1, error_message)
                if '-d' in FrameWorkBase._OPT_:
                    print traceback.format_exc()
    else:
        ##If function name not found in xml tag, terminate this test case
        if str(func) != 'Description':
            error_message = "\t!!!!! There are no \"%s\" symbol in XML tag !!!!!" % str(func)
            print error_message
            print "\tQuitting..."
            dumplogger.error(error_message)
            FrameWorkBase.OK(Config._Exception_, 0, error_message)

def char_data(data):
    ''' Handle description text between XML tags '''
    global _CASE_, _LINEFLAG_

    #if _CASE_['id'] != None:
    if _CASE_['id']:
        new_string = data.strip()

        if len(new_string) != 0:
            if _LINEFLAG_ != 0:
                #print "Enter if"
                _CASE_['txt'] = data
            else:
                #print "Enter else"
                _LINEFLAG_ = 1  # Always pass first line due to Test suite description
            #print 'Character data:', repr(new_string)
            #os.system("pause")
        #else:
            #pass
            #print "len(new_string) == 0"
    return
