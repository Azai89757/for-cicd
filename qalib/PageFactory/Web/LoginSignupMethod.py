﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 LoginSignupMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def ShopeeLogin(arg):
    '''
    ShopeeLogin : Login shopee website
            Input argu :
                account - user account
                pwd - password
                logintype - account, email, phone, facebook, google
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    account = arg["account"]
    pwd = arg["password"]
    logintype = arg["logintype"]
    ret = 1

    ##Click login hyperlink
    xpath = Util.GetXpath({"locate": "label_login"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click login icon first", "result": "1"})

    if logintype in ("account", "email", "phone"):

        ##Input account
        xpath = Util.GetXpath({"locate": "login_account"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Input password
        xpath = Util.GetXpath({"locate": "login_password"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pwd, "result": "1"})

        ##Click login button
        LoginSignupButton.ClickLogin({"result": "1"})

    elif logintype == 'facebook':

        ##Change login method
        LoginSignupButton.ClickLoginMethod({"login_type": "facebook", "result": "1"})

        ##Switch to new window
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "0", "result": "1"})

        ##Input account
        xpath = Util.GetXpath({"locate": "facebook_login_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Input password
        xpath = Util.GetXpath({"locate": "facebook_login_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pwd, "result": "1"})

        ##Click login button
        xpath = Util.GetXpath({"locate": "facebook_login_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click button", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "facebook_confirm_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click confirm button", "result": "1"})

        ##Switch back to the first window
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "1", "result": "1"})

    elif logintype == 'google':

        ##Change login method
        LoginSignupButton.ClickLoginMethod({"login_type": "google", "result": "1"})

        ##Switch to new window
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "0", "result": "1"})

        ##Input account
        xpath = Util.GetXpath({"locate": "google_login_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Click continue button
        xpath = Util.GetXpath({"locate": "google_continue_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click button", "result": "1"})

        ##Input password
        xpath = Util.GetXpath({"locate": "google_login_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pwd, "result": "1"})

        ##Click continue button
        xpath = Util.GetXpath({"locate": "google_continue_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click button", "result": "1"})

        ##Switch back to the first window
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "1", "result": "1"})

    elif logintype == 'sms':

        ##Change login method SMS
        LoginSignupButton.ClickLoginMethod({"login_type": "sms", "result": "1"})

        ##Input account phone number
        LoginPage.InputPhoneNumber({"phone": account, "page_type":"sms_login_page", "result": "1"})

        ##Click next
        LoginSignupButton.ClickNext({"button_type":"label_login_signup_next", "result": "1"})

        ##Slider capthca was disabled by RE fraud
        ##Uncomment following code when slider captcha is enabled
        ##Input common capcha "123456"
        #LoginPage.InputCaptcha({"captcha": "123456", "result": "1"})

        ##Input capcha verify
        #LoginSignupButton.ClickCapchaVerify({"result": "1"})

        ##Sleep 5 second to wait Vcode page is loaded
        time.sleep(5)

        ##Input common Vcode "123456"
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": "123456", "result": "1"})

        ##Click login
        LoginSignupButton.ClickLogin({"result": "1"})

    ##After login wait web page has loaded
    BaseUICore.PageHasLoaded({"result": "1"})

    ##Close cookies message in specific region
    if Config._TestCaseRegion_ in ("PL", "ES"):
        xpath = Util.GetXpath({"locate": "cookies_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close cookies message", "result": "1"})

    ##Close PDPA popup in specific region
    if Config._TestCaseRegion_ in ("TH"):
        xpath = Util.GetXpath({"locate": "accept_pdpa_consent_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close PDPA consent", "result": "1"})
    OK(ret, int(arg['result']), 'ShopeeLogin -> ' + logintype)

@DecoratorHelper.FuncRecorder
def ShopeeLogOut(arg):
    '''
    ShopeeLogOut : Log out from shopee web
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Wait for page loading completed
    BaseUICore.PageHasLoaded({"result": "1"})

    ##Go to buyer home page
    xpath_username = Util.GetXpath({"locate": "shopee_username"})
    xpath_logout = Util.GetXpath({"locate": "logout_option"})
    BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath_username, "locatehidden": xpath_logout, "result": "1"})

    ##Wait for logout
    time.sleep(3)

    ##Redirect to shopee home page
    url = "https://" + GlobalAdapter.UrlVar._Domain_
    BaseUICore.GotoURL({"url": url, "result": "1"})

    OK(ret, int(arg['result']), 'ShopeeLogOut')


class LoginPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AccountLoginFET(arg):
        '''
        AccountLoginFET : FET test scenario of "Account" login method
                Input argu :
                    account - user account
                    pwd - password
                    scenario - to print/log what scenario is running
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        pwd = arg["password"]
        scenario = arg["scenario"]
        ret = 1

        ##Argument account input blank value and set random account
        if not account:
            account = XtFunc.GenerateRandomString(8, "characters")

        ##Input account
        xpath_account = Util.GetXpath({"locate": "login_account"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath_account, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath_account, "string": account, "result": "1"})

        ##Input password
        xpath_password = Util.GetXpath({"locate": "login_password"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath_password, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath_password, "string": pwd, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.AccountLoginFET ->' + scenario)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SMSLoginFET(arg):
        '''
        SMSLoginFET : FET test scenario of "SMS" login method
                Input argu :
                    account - user account
                    vcode - vcode received from cell phone text message
                    imgnumber - numbers in the integrety check image
                    scenario - to print/log what scenario is running
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        vcode = arg["vcode"]
        imgnumber = arg["imgnumber"]
        scenario = arg["scenario"]
        ret = 1

        ##If random string appeared in scenario, means to use random phone number started with 00
        if "random" in scenario:
            ##Generate phone number start with 0062
            phone = "0062" + XtFunc.GenerateRandomString(6, "numbers")
            dumplogger.info("Generated random phone: %s" % (phone))

        ##Input phone number
        LoginPage.InputPhoneNumber({"phone": account, "page_type":"sms_login_page", "result": "1"})

        if imgnumber:

            ##Click next btn
            LoginSignupButton.ClickNext({"button_type": "label_login_signup_next", "result": "1"})

            ##Input imgnumber
            LoginPage.InputCaptcha({"captcha": imgnumber, "result": "1"})

            ##Click verify btn
            LoginSignupButton.ClickCapchaVerify({"result": "1"})

        if vcode:

            ##Input Vcode "123456"
            time.sleep(3)
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": vcode, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.SMSLoginFET ' + scenario)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShopeeLoginFET(arg):
        '''
        ShopeeLoginFET : Login with random username
                Input argu :
                    password : password
                    logintype : account, email, phone, facebook, google
                    scenario : to print/log what scenario is running
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        password = arg["password"]
        logintype = arg["logintype"]
        scenario = arg["scenario"]
        ret = 1

        random_username = GlobalAdapter.GeneralE2EVar._RandomString_

        if "previous" in scenario:
            ##Use previous username to login shopee
            LoginPage.AccountLoginFET({"account": random_username, "password": password, "scenario": scenario, "result": "1"})

        else:
            ##Use Random username to login shopee
            ShopeeLogin({"account": random_username, "password": password, "logintype": logintype, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.ShopeeLoginFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        '''
        InputPassword : Input password to signup
                Input argu :
                    password - password you want to input
                    page_type - login_page / reset_password_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        password = arg["password"]
        page_type = arg["page_type"]

        ##Input password
        xpath = Util.GetXpath({"locate": page_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.InputPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhoneNumber(arg):
        '''
        InputPhoneNumber : Input phone number in SMS login page
                Input argu :
                    phone - phone number of account attached
                    page_type - sms_login_page / forget_password_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        phone = arg["phone"]
        page_type = arg["page_type"]

        ##Input phone number in SMS login page
        xpath = Util.GetXpath({"locate":page_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.InputPhoneNumber => %s' % (phone))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCaptcha(arg):
        '''
        InputCaptcha : Input captcha in captcha verification page
                Input argu :
                    captcha - input captcha for verification
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        captcha = arg["captcha"]

        ##Input captcha in captcha verification page
        xpath = Util.GetXpath({"locate":"sms_captcha_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":captcha, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.InputCaptcha => %s' % (captcha))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAccount(arg):
        '''
        InputAccount : Input account on login page
                Input argu :
                    account - user account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]

        ##Input account
        xpath = Util.GetXpath({"locate": "login_account"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        OK(ret, int(arg['result']), 'LoginPage.InputAccount')


class SignupPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PhoneSignupFET(arg):
        '''
        PhoneSignupFET : FET test scenario of "Phone number" signup method
                Input argu :
                    phone - phone number or random
                    vcode - verification code got from text message
                    imgnumber - numbers in the integrety check image
                    password - set password
                    scenario - to print/log what scenario is running
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        phone = arg["phone"]
        vcode = arg["textvcode"]
        imgnumber = arg["imgnumber"]
        password = arg["password"]
        scenario = arg["scenario"]
        ret = 1

        ##If random string appeared in scenario, means to use random phone number started with 0082
        if "random" in scenario:
            ##Reset phone number with 0082 + random phone number
            phone = "0082" + XtFunc.GenerateRandomString(6, "numbers")

        ##Input phone
        xpath = Util.GetXpath({"locate": "phone"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": phone, "result": "1"})

        time.sleep(2)

        ##Check icon is exist
        xpath = Util.GetXpath({"locate": "check_icon"})
        if phone and BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):

            ##Click next btn
            xpath = Util.GetXpath({"locate": "label_login_signup_next"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next btn", "result": "1"})

            ##Input imgnumber
            xpath = Util.GetXpath({"locate": "image_code"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": imgnumber, "result": "1"})

            ##Click next btn
            xpath = Util.GetXpath({"locate": "label_captcha_popup_verify"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next btn", "result": "1"})

        if vcode:

            ##Wait for page loading completed
            BaseUICore.PageHasLoaded({"result": "1"})

            ##Input Vcode
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": vcode, "result": "1"})

            time.sleep(2)

            ##Click verify btn
            xpath = Util.GetXpath({"locate": "label_verify"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click verify btn", "result": "1"})

        if password:
            ##Wait for page loading completed
            BaseUICore.PageHasLoaded({"result": "1"})

            ##Input password
            xpath = Util.GetXpath({"locate": "password_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

            ##Click signup btn
            xpath = Util.GetXpath({"locate": "signup_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click signup btn", "result": "1"})

            ##Get username in homepage
            xpath = Util.GetXpath({"locate":"account_name"})
            BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.GeneralE2EVar._RandomString_ = GlobalAdapter.CommonVar._PageAttributes_

        OK(ret, int(arg['result']), 'SignupPage.PhoneSignupFET -> ' + scenario)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhone(arg):
        '''
        InputPhone : Input phone with random numbers in signup page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input phone with random numbers
        xpath = Util.GetXpath({"locate": "phone_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "00" + XtFunc.GenerateRandomString(8, "numbers"), "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.InputPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        '''
        InputPassword : Input password to signup
                Input argu :
                    password : password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        password = arg["password"]
        ret = 1

        #Input password
        xpath = Util.GetXpath({"locate": "password"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.InputPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FacebookSignup(arg):
        '''
        FacebookSignup : Sign up by facebook
                Input argu :
                    account - facebook account
                    password - facebook password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        ret = 1

        ##Click facebook signup method
        LoginSignupButton.ClickSignupMethod({"signup_type": "facebook", "result": "1"})
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "0", "result": "1"})

        ##Input facebook account
        xpath = Util.GetXpath({"locate": "fb_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Input facebook passwords
        xpath = Util.GetXpath({"locate": "fb_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        ##Click log in
        xpath = Util.GetXpath({"locate": "fb_login"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Facebook login", "result": "1"})

        ##Confirm and go on as current log in user
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Facebook login confirm", "result": "1"})
            dumplogger.info("Comfirm user button.")

        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "1", "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.FacebookSignup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoogleSignup(arg):
        '''
        GoogleSignup : Sign up by google
                Input argu :
                    account - google account
                    password - google password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        ret = 1

        ##Click google signup method
        LoginSignupButton.ClickSignupMethod({"signup_type": "google", "result": "1"})
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "0", "result": "1"})

        ##Input google account
        xpath = Util.GetXpath({"locate": "google_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Click continue
        xpath = Util.GetXpath({"locate": "google_continue"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click continue btn", "result": "1"})

        ##Input google password
        xpath = Util.GetXpath({"locate": "google_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        ##Click continue
        xpath = Util.GetXpath({"locate": "google_continue"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click continue btn", "result": "1"})
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "1", "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.GoogleSignup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FacebookSignupFET(arg):
        '''
        FacebookSignupFET : FET case of Sign up by facebook
                Input argu :
                    account - shopee user account
                    email - shopee user email
                    scenario - print the scenario
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        email = arg["email"]
        scenario = arg["scenario"]
        ret = 1

        dumplogger.info(scenario)

        ##Input facebook account
        xpath = Util.GetXpath({"locate": "re_name"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Input password
        xpath = Util.GetXpath({"locate": "re_email"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": email, "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.FacebookSignupFET -> ' + scenario)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoogleSignupFET(arg):
        '''
        GoogleSignupFET : FET case of Sign up by google
                Input argu :
                    account - shopee user account
                    email - shopee user email
                    scenario - print the scenario
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        email = arg["email"]
        scenario = arg["scenario"]
        ret = 1

        dumplogger.info(scenario)

        ##Input google account
        xpath = Util.GetXpath({"locate": "re_name"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        ##Input password
        xpath = Util.GetXpath({"locate": "re_email"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": email, "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.GoogleSignupFET -> ' + scenario)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RenameUserNameAndEmail(arg):
        '''
        RenameUserNameAndEmail : Rename user name and email after facebook signup step
                Input argu :
                    username - username
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        username = arg["username"]
        ret = 1

        ##Delete previous username
        xpath = Util.GetXpath({"locate": "re_name"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input new username
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": username, "result": "1"})

        ##Delete email address
        xpath = Util.GetXpath({"locate": "re_email"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.RenameUserNameAndEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVcode(arg):
        '''
        InputVcode : Input vcode after phone number to signup
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        vcode = arg["vcode"]
        ret = 1

        ##Input vcode
        xpath = Util.GetXpath({"locate": "vcode"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": vcode, "result": "1"})

        OK(ret, int(arg['result']), 'SignupPage.InputVcode')

class LoginSignupButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu :
                    page_type - cancel button of which page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click cancel button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackIcon(arg):
        '''
        ClickBackIcon : Click back icon in forget password page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back icon
        xpath = Util.GetXpath({"locate": "back_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back icon", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickBackIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchTab(arg):
        '''
        SwitchTab : Switch tab in login/signup page
                Input argu :
                    tab - signup / login
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        tab = arg["tab"]
        ret = 1

        ##Click + icon in the conversation page
        xpath = Util.GetXpath({"locate": "switch_" + tab + "_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "SwitchTab", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.SwitchTab -> ' + tab.encode('big5'))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogin(arg):
        '''
        ClickLogin : Click login button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click login
        xpath = Util.GetXpath({"locate": "label_login_signup_login"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login btn", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSignUp(arg):
        '''
        ClickSignUp : Click signup button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click signup
        xpath = Util.GetXpath({"locate": "label_signup"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sign up", "result": "1"})

        ##Close PDPA consent dialog in TH
        if Config._TestCaseRegion_ == "TH":
            xpath = Util.GetXpath({"locate": "pdpa_accept_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close PDPA consent dialog", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickSignUp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click login/signup submit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click login/signup submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login/signup submit button", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        '''
        ClickNext : Click login/signup next button
                Input argu :
                    button_type - label_login_signup_next / label_reset_password_next
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click login/signup next button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login/signup next button => %s" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickNext')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCapchaVerify(arg):
        '''
        ClickCapchaVerify : Click capcha verify button in capcha page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click capcha verify button in capcha page
        xpath = Util.GetXpath({"locate": "label_captcha_popup_verify"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click capcha verify button in capcha page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickCapchaVerify')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendSMS(arg):
        '''
        ClickSendSMS : Click send sms by opencv in another verification page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send sms by opencv in another verification page
        xpath = Util.GetXpath({"locate":"send_sms_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click send sms by opencv in another verification page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickSendSMS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPhoneCall(arg):
        '''
        ClickPhoneCall : Click phone call button in another verification page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click phone call button in another verification page
        xpath = Util.GetXpath({"locate":"phone_call_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click phone call button in another verification page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickPhoneCall')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendVerification(arg):
        '''
        ClickSendVerification : Click send verification text link
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send verification
        xpath = Util.GetXpath({"locate": "label_require_otp_resend"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click send verification", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickSendVerification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAnotherVerifyMethod(arg):
        '''
        ClickAnotherVerifyMethod : Click another verification method text link
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click another verification method link
        xpath = Util.GetXpath({"locate": "label_require_otp_try_different_verification_method"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click send verification", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickAnotherVerifyMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLoginNow(arg):
        '''
        ClickLoginNow : Click login now
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click login now
        xpath = Util.GetXpath({"locate": "login_now_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login now", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickLoginNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTermsOfService(arg):
        '''
        ClickTermsOfService : Go to terms of service page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Go to terms of service page
        xpath = Util.GetXpath({"locate": "terms_of_service"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Go to terms of service page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickTermsOfService')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrivacyPolicy(arg):
        '''
        ClickPrivacyPolicy : Go to privacy policy page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Go to privacy policy page
        xpath = Util.GetXpath({"locate": "privacy_policy"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Go to privacy policy page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickPrivacyPolicy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNeedHelp(arg):
        '''
        ClickNeedHelp : Go to help page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Go to privacy policy page
        xpath = Util.GetXpath({"locate": "need_help"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Go to help page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickNeedHelp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLoginMethod(arg):
        '''
        ClickLoginMethod : Choose different login method
                Input argu :
                    login_type - login method
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        login_type = arg["login_type"]
        ret = 1

        ##Choose a login type
        xpath = Util.GetXpath({"locate": login_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Change login type: " + login_type, "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickLoginMethod' + login_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickForgetPassword(arg):
        '''
        ClickForgetPassword : Click forget password
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click forget password in the login page
        xpath = Util.GetXpath({"locate": "label_login_signup_forgot_password"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click forget password", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickForgetPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewPasswordIcon(arg):
        '''
        ClickViewPasswordIcon : Click view password icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view password icon
        xpath = Util.GetXpath({"locate": "view_password_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view password icon", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickViewPasswordIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSignupMethod(arg):
        '''
        ClickSignupMethod : Choose different login method
                Input argu :
                    signup_type - signup method
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        signup_type = arg["signup_type"]
        ret = 1

        ##Change singup type
        xpath = Util.GetXpath({"locate": signup_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Change singup type: " + signup_type, "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickSignupMethod -> ' + signup_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCaptchaIcon(arg):
        '''
        ClickCaptchaIcon : Click captcha icon to change captcha
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click captcha icon
        xpath = Util.GetXpath({"locate": "msg_captcha_popup_click_to_refresh"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click captcha icon to change captcha", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickCaptchaIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRegisterNow(arg):
        '''
        ClickRegisterNow : Click ok when popup register now
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok when popup register now
        xpath = Util.GetXpath({"locate": "label_login_signup_ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok when popup register now", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickRegisterNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLoginButtonInAlreadyRegistered(arg):
        '''
        ClickLoginButtonInAlreadyRegistered : Click login button for already registered account
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click login button
        xpath = Util.GetXpath({"locate": "login_btn_user_already_registered"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click log in btn to make registered user login", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickLoginButtonInAlreadyRegistered')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVerify(arg):
        '''
        ClickVerify : Click verify button in OTP page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click verify button in OTP page
        xpath = Util.GetXpath({"locate": "label_login_signup_verify"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click verify button in OTP page", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickVerify')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToShopee(arg):
        '''
        ClickBackToShopee : Click back to shopee
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to shopee
        xpath = Util.GetXpath({"locate": "label_login_signup_back_to_shopee"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to shopee", "result": "1"})

        OK(ret, int(arg['result']), 'LoginSignupButton.ClickBackToShopee')
