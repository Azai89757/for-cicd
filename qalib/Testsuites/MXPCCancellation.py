import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCancellation Precondition =="
        dumplogger.info("== Setup MXPCCancellation Precondition ==")

    def test_MXPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCancellation-01.xml")

    def test_MXPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCancellation-02.xml")

    def test_MXPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCancellation-03.xml")

    def test_MXPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCancellation-04.xml")

    def test_MXPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCancellation Post condition =="
        dumplogger.info("== Setup MXPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
