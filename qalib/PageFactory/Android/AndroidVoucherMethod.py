#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidVoucherMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import common library
import Util
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AndroidVoucherButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in voucher page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClaim(arg):
        '''
        ClickClaim : Click voucher claim button
                Input argu :
                    voucher_discount - if page_type is pdp or voucher_drawer, use this argument to locate element
                    minimum_basket_size - if page_type is pdp or voucher_drawer, use this argument to locate element
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_discount = arg["voucher_discount"]
        minimum_basket_size = arg["minimum_basket_size"]

        ##Click voucher claim button
        xpath = Util.GetXpath({"locate":"claim_btn"})
        xpath = xpath.replace("title_1_to_be_replace", voucher_discount)
        xpath = xpath.replace("title_2_to_be_replace", minimum_basket_size)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click voucher claim button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnTopPanel(arg):
        '''
        ClickTabOnTopPanel : Click voucher tab button on top panel
                Input argu :
                    type - valid / used / invalid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click voucher tab button
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click voucher tab button => " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickTabOnTopPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHistory(arg):
        '''
        ClickHistory : Click history button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click history button
        xpath = Util.GetXpath({"locate":"history_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click history button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickHistory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInputPromoCode(arg):
        '''
        ClickInputPromoCode : Click input promo code button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click input promo code button
        xpath = Util.GetXpath({"locate":"input_promo_code_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click input promo code button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickInputPromoCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucher(arg):
        '''
        ClickVoucher : Click target voucher name
                Input argu :
                    voucher_name - voucher name you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]

        ##Click voucher name
        xpath = Util.GetXpath({"locate":"voucher_name"})
        xpath_voucher_name = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_voucher_name, "message":"Click voucher name", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirmVoucher(arg):
        '''
        ClickConfirmVoucher : Click confirm voucher button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm voucher button
        xpath = Util.GetXpath({"locate":"voucher_confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickConfirmVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close popup button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close popup button
        xpath = Util.GetXpath({"locate":"close_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click close popup button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTermCondition(arg):
        '''
        ClickTermCondition : Click vouhcer terms and conditions button
                Input argu :
                    voucher_name - voucher name you want to click it terms and conditions button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click vouhcer terms and conditions button
        xpath = Util.GetXpath({"locate":"voucher_tc_btn"})
        xpath_voucher_tnc = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_voucher_tnc, "message":"Click vouhcer terms and conditions button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickTermCondition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUse(arg):
        '''
        ClickUse : Click use voucher button
                Input argu :
                    voucher_name - voucher name you want to use it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]

        ##Click use voucher button
        xpath = Util.GetXpath({"locate":"use_btn"})
        xpath_use_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_use_btn, "message":"Click use voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickUse')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTCPageUse(arg):
        '''
        ClickTCPageUse : Click T&C use voucher button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click use voucher button
        xpath = Util.GetXpath({"locate":"use_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click use T&C page voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickTCPageUse')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApply(arg):
        '''
        ClickApply : Click apply voucher button
                Input argu :
                    voucher_name - voucher name you want to apply it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]

        ##Click use voucher button
        xpath = Util.GetXpath({"locate":"apply_btn"})
        xpath_apply_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_apply_btn, "message":"Click apply voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickApply')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove voucher button
                Input argu :
                    voucher_name - voucher name you want to remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click remove voucher button
        xpath = Util.GetXpath({"locate":"remove_btn"})
        xpath_remove_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_remove_btn, "message":"Click remove voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLink(arg):
        '''
        ClickLink : Click voucher link button
                Input argu :
                    link_name - voucher link name button you want to click it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_name = arg['link_name']

        ##Click voucher link button
        xpath = Util.GetXpath({"locate":"link_name_btn"})
        xpath_link_name_btn = xpath.replace("link_name_to_be_replace", link_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_link_name_btn, "message":"Click voucher link button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowMore(arg):
        '''
        ClickShowMore : Click show more button in voucher selection page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show more button
        xpath = Util.GetXpath({"locate":"show_more_fsv_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click show more button in voucher selection page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickShowMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShopping(arg):
        '''
        ClickGoShopping : Click go shopping button in voucher selection page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go shopping button
        xpath = Util.GetXpath({"locate":"go_shop_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click go shopping button in voucher selection page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickGoShopping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowLess(arg):
        '''
        ClickShowLess : Click show less button in voucher selection page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show less button
        xpath = Util.GetXpath({"locate":"show_less_fsv_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click show less button in voucher selection page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickShowLess')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyVoucherPageTab(arg):
        '''
        ClickMyVoucherPageTab : Click MyVoucherPage Tab
                Input argu :
                    tab_name - voucher tab you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg['tab_name']

        ##Click MyVoucherPageTab button
        xpath = Util.GetXpath({"locate":tab_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click my voucher page tab button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherButton.ClickMyVoucherPageTab')

class AndroidVoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherCode(arg):
        '''
        InputVoucherCode : Choose voucher in voucher selection page
                Input argu :
                    voucher_type - shopee_voucher / seller_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]

        ##Get voucher input field XPath
        xpath = Util.GetXpath({"locate":"voucher_input"})

        ##Input shopee voucher code
        if voucher_type == "shopee_voucher":
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["code"], "result": "1"})

        ##Input 2nd shopee voucher code
        elif voucher_type == "2nd_shopee_voucher":
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-2]["code"], "result": "1"})

        ##Input seller voucher code
        elif voucher_type == "seller_voucher":
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["code"], "result": "1"})

        else:
            ret = -1
            dumplogger.error("Voucher type string is not correct, please use shopee_voucher or seller_voucher string!!!")

        ##Sleep 10 second to wait voucher claim toast is disappear
        time.sleep(10)

        ##Click voucher to use
        xpath = Util.GetXpath({"locate":"voucher_use"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click voucher use", "result": "1"})
        time.sleep(15)

        OK(ret, int(arg['result']), 'AndroidVoucherPage.InputVoucherCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClaimVoucher(arg):
        '''
        ClaimVoucher : Input voucher code and click save to claim voucher
                Input argu :
                    voucher_type - shopee_voucher / seller_voucher
                    voucher_code - voucher code you want to input
                    vouchers_index - if you want to input suffix voucher code, you need to give index of list variable "_VoucherCode_" to get one of suffix voucher codes
                    part_of_voucher_code_length - part of voucher code you want to input
                    extra_voucher_code - extra voucher code you want to extra input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]
        voucher_code = arg['voucher_code']
        vouchers_index = arg['vouchers_index']
        part_of_voucher_code_length = arg['part_of_voucher_code_length']
        extra_voucher_code = arg['extra_voucher_code']

        ##If voucher_code argument is empty, get voucher code from global variable
        if not voucher_code:
            ##Get voucher code from global list variable "_ShopeeVoucherInfo_"
            if voucher_type == "shopee_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_SellerVoucherInfo_"
            elif voucher_type == "seller_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._SellerVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_FreeShippingVoucherList_"
            elif voucher_type == "free_shipping_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_DPVoucherList_"
            elif voucher_type == "dp_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._DPVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_OfflinePaymentVoucherList_"
            elif voucher_type == "offline_payment_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_[int(vouchers_index)]["code"]

            else:
                ret = -1
                dumplogger.error("Voucher type string is not correct, please use shopee_voucher or seller_voucher string!!!")

        ##Get part of voucher code
        if part_of_voucher_code_length:
            voucher_code = voucher_code[:int(part_of_voucher_code_length)]

        ##Add extra voucher code string in voucher_code
        voucher_code = voucher_code + extra_voucher_code

        ##Log voucher code
        dumplogger.info("Input voucher code => '%s' to claim voucher" % (voucher_code))

        ##Input voucher code
        xpath = Util.GetXpath({"locate":"voucher_code_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":voucher_code, "result": "1"})

        ##Click save voucher button
        xpath = Util.GetXpath({"locate":"save_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click save voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherPage.ClaimVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRandomVoucherLabelText(arg):
        '''
        CheckRandomVoucherLabelText : Check random voucher label text in any voucher page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check random voucher label text in any voucher page
        xpath = Util.GetXpath({"locate":"random_voucher_label_txt"})
        xpath = xpath.replace("random_text_to_be_replaced", GlobalAdapter.PromotionE2EVar._CustomisedLabel_)
        if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "message": "Check random text","passok": "0", "result": "1"}):
            dumplogger.info("Voucher label text match!")
        else:
            dumplogger.info("Voucher label text not match!")
            ret = 0

        OK(ret, int(arg['result']), 'AndroidVoucherPage.CheckRandomVoucherLabelText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ApplyPlatformVoucher(arg):
        '''
        ApplyPlatformVoucher : apply platform voucher
                Input argu :
                    voucher_name - voucher name you want to apply
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click voucher radio button
        xpath = Util.GetXpath({"locate":"voucher_radio_button"})
        xpath_radio_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_radio_btn, "message":"Click voucher radio button", "result": "1"})

        time.sleep(5)

        ##Click voucher section confirm
        xpath = Util.GetXpath({"locate":"voucher_section_confirm_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click voucher section confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVoucherPage.ApplyPlatformVoucher')
