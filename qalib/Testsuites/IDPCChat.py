import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class IDPCChat(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCChat Precondition =="
        dumplogger.info("== Setup IDPCChat Precondition ==")

    def test_IDPCChat01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCChat-01.xml")

    def test_IDPCChat02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCChat-02.xml")

    def test_IDPCChat03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCChat-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCChat Post condition =="
        dumplogger.info("== Setup IDPCChat Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
