﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 GlobalAdapter.py: The def of this file called by XML mainly.
'''

class CommonVar:
    _PageUrl_ = ""
    _PageElements_ = ""
    _PageAttributes_ = ""
    _ScreenshotFilename_ = {
        "android":"",
        "pc":"",
        "ios":""
    }
    _ProcessID_ = {
        "appium":"",
        "ios":"",
        "android_screenshot":"",
        "pc_screenshot":""
    }
    _Start_Img_ = []
    _Retry_Start_Img_ = []
    _Function_Name_ = []
    _Retry_Function_Name_ = []
    _PageAttributesList_ = []
    _MemoryStack_ = []
    _XMLStack_ = []
    _FuncStack_ = []
    _FailedCaseInfo_ = {}
    _JSONRawDataFromFile_ = ""
    _DynamicCaseData_ = {}
    _BrowserCookie_ = {
        "TW":{},
        "VN":{},
        "ID":{},
        "TH":{},
        "PH":{},
        "MX":{},
        "MY":{},
        "SG":{},
        "BR":{},
        "CO":{},
        "CL":{},
        "PL":{},
        "ES":{},
        "IN":{},
        "FR":{},
        "AR":{}
    }

class UrlVar:
    _Domain_ = ""

class GeneralE2EVar:
    _Count_ = ""
    _LocationX_ = ""
    _LocationY_ = ""
    _RandomString_ = ""
    _ModifyString_ = ""
    _AlertTextList_ = []
    _ElementSizeDict_ = {}

class BannerE2EVar:
    _SequenceID_ = ""
    _HomeSquareNumber_ = ""
    _BannerCampaignUnitIDList_ = []
    _BannerCampaignUnitList_ = ""
    _BannerList_ = ""
    _BannerIDList_ = []

class MePageE2EVar:
    _FeatureID_ = ""

class OrderE2EVar:
    _CheckOutID_ = ""
    _CancelLogID_ = ""
    _OrderID_ = ""
    _FOrderID_ = ""
    _RefundID_ = ""
    _ReturnID_ = ""
    _ReturnSN_ = ""
    _LogisticPrice_ = ""
    _RefundNumberList_ = []
    _ReturnProductNameList_ = []
    _OrderSNDict_ = {}
    _ShipByDate_ = ""
    _EstimatedDeliveryTime_ = ""
    _EscrowReleaseCreatedTime_ = ""
    _OFGID_ = ""
    _LogisticStatus_ = ""
    _OrderTimestampDict_ = {}

class AccountE2EVar:
    _UserID_ = ""
    _ShopID_ = ""
    _CaptchaList_ = []
    _SellerIDList_ = []
    _PersonalInfoList_ = []
    _LoginSignupTime_ = ""
    _FraudTagList_ = []

class NotiE2EVar:
    _NotificationID_ = ""

class PaymentE2EVar:
    _IndomaretID_ = ""
    _TransactionID_ = ""
    _StatementID_ = ""
    _TopUpID_ = ""
    _MaintenanceStartTime_ = ""
    _MaintenanceEndTime_ = ""
    _SPMPaymentID_ = ""
    _SPMChannelRef_ = ""
    _BankVANumber_ = ""
    _ATMRef1_ = ""
    _AirpayOrderID_ = ""
    _TransactionFeeRuleID_ = ""
    _TransactionFeeCSRuleID_ = ""
    _BuyerTxnFeeRuleID_ = ""
    _DragonPayRefNo_ = ""
    _PayableAmount_ = ""

class ServiceFeeE2EVar:
    _StartTime_ = ""
    _EndTime_ = ""
    _RuleID_ = ""

class SellerTxnFeeE2EVar:
    _RuleID_ = ""
    _GroupID_ = ""

class CommissionFeeE2EVar:
    _RuleID_ = ""

class CoinsE2EVar:
    _CoinsAmount_ = ""
    _RuleID_ = ""
    _OriginalShopeeCoins_ = ""
    _GetShopeeCoins_ = ""

class ProductE2EVar:
    _CategoryID_ = ""

class FormE2EVar:
    _FormID_ = ""
    _UnsubFormID_ = ""
    _StartTime_ = ""
    _EndTime_ = ""
    _QRCodeURL_ = ""

class FlashSaleE2EVar:
    _TimeSlotID_ = ""
    _CFSSessionID_ = ""
    _MFSSessionID_ = ""
    _PromotionID_ = ""
    _CFSClusterID_ = ""
    _MFSClusterID_ = ""
    _ProductIDList_ = []
    _UploadProductList_ = []
    _ExtraDiscountProductIDList_ = []

class SellerDiscountE2EVar:
    _PromotionID_ = ""

class ProductCollectionE2EVar:
    _OldCollectionName_ = ""
    _NewCollectionName_ = ""
    _CollectionID_ = ""
    _GoogleAccount_ = ""
    _RecordsCount_ = ""
    _UpdateTime_ = ""
    _TemplateList_ = []
    _ItemEditList_ = []
    _ShopMetricsList_ = []
    _BatchUploadList_ = []
    _AuditLogList_ = []

class PromotionE2EVar:
    _AuditDataInfo_ = ""
    _BundleDealIDList_ = []
    _CustomisedLabel_ = ""
    _FlashSaleIDList_ = []
    _BrandSaleIDList_ = []
    _InShopFlashSaleIDList_ = []
    _PromotionIDList_ = []
    _ShopeeVoucherList_ = []
    _FreeShippingVoucherList_ = []
    _SellerVoucherList_ = []
    _ShopeeVoucherList_ = []
    _ReviewingFlashSaleList_ = []
    _OngoingFlashSaleList_ = []
    _UpcomingFlashSaleList_ = []
    _DPVoucherList_ = []
    _OfflinePaymentVoucherList_ = []
    _PartnerVoucherList_ = []

class ListingE2EVar:
    _GroupID_ = ""
    _QCReason_ = {}
    _UploadedFilePath_ = ""

class ShopCollectionE2EVar:
    _RecordsCount_ = []
    _NewCollectionName_ = []
    _CollectionID_ = []
    _ItemsPerPage_ = ""
    _NumberOfPage_ = ""
    _RuleID_ = ""

class RegionalJson:
    _SpaceIOToken_ = ""
    _CollectionID_ = []
    _EmptyCollection_ = []
    _KeyCount_ = []
    _i18N_ = {
                "TW":{},
                "VN":{},
                "ID":{},
                "TH":{},
                "PH":{},
                "MX":{},
                "MY":{},
                "SG":{},
                "BR":{},
                "CO":{},
                "CL":{},
                "PL":{},
                "ES":{},
                "IN":{},
                "FR":{},
                "AR":{},
                "ADMIN":{}
    }

    _TransferKeyMapping_ = {
        "PH":
        {
            "en-pc":{},
            "en-android-rn":{},
        },
        "TW":
        {
            "zh-hant-pc":{},
            "zh-Hant-android-rn":{},
        },
        "TH":
        {
            "th-pc":{},
            "th-android-rn":{}
        },
        "VN":
        {
            "vi-pc":{},
            "vi-android-rn":{}
        },
        "ID":
        {
            "id-pc":{},
            "id-android-rn":{}
        },
        "SG":
        {
            "en-pc":{},
            "en-android-rn":{}
        },
        "MX":
        {
            "es-mx-pc":{},
            "es-MX-android-rn":{}
        },
        "MY":
        {
            "ms-my-pc":{},
            "ms-MY-android-rn":{}
        },
        "BR":
        {
            "pt-br-pc":{},
            "pt-BR-android-rn":{}
        },
        "CO":
        {
            "es-co-pc":{},
            "es-CO-android-rn":{}
        },
        "CL":
        {
            "es-cl-pc":{},
            "es-CL-android-rn":{}
        },
        "PL":
        {
            "pl-pc":{},
            "pl-android-rn":{}
        },
        "ES":
        {
            "es-es-pc":{},
            "es-es-android-rn":{}
        },
        "IN":
        {
            "hi-pc":{},
            "hi-android-rn":{}
        },
        "FR":
        {
            "fr-pc":{},
            "fr-android-rn":{}
        },
        "AR":
        {
            "es-ar-pc":{},
            "es-ar-android-rn":{}
        }
    }

class APIVar:
    _APIResponse_ = ""
    _BannerPayload_ = {}
    _JsonCaseData_ = {}
    _HttpSession_ = ""
    _HttpUrl_ = ""
    _HttpPayload_ = {}
    _HttpCookie_ = {}
    _HttpHeaders_ = {}
    _HttpUrlParameter_ = {}
    _HttpUploadFiles_ = {}
    _RequestTimestamp_ = ""
    _SpexApiCommand_ = {}
    _SpexReq_ = {}
    _SpexReqBuf_ = {}
    _SpexResBuf_ = {}
    _Csrftoken_ = ""
    _CreateOrderData_ = {
        "logistic_channel_id":"",
        "payment_channel_id":"",
        "spm_option_info":"",
        "spm_channel_id":"",
        "checkout_data":{},
        "order_id":"",
        "checkout_id":""
    }
    _InShopFlashSaleIDList_ = []

class DBVar:
    _DBResult_ = ""
    _DBTableTitle_ = []
    _SqlCmd_ = ""

class TrackerVar:
    _CurUnixTime_ = ""
