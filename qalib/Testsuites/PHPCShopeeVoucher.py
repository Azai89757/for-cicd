import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCShopeeVoucher Precondition =="
        dumplogger.info("== Setup PHPCShopeeVoucher Precondition ==")

    def test_PHPCShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-01.xml")

    def test_PHPCShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-02.xml")

    def test_PHPCShopeeVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-03.xml")

    def test_PHPCShopeeVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-04.xml")

    def test_PHPCShopeeVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-05.xml")

    def test_PHPCShopeeVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-06.xml")

    def test_PHPCShopeeVoucher07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-07.xml")

    def test_PHPCShopeeVoucher08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-08.xml")

    def test_PHPCShopeeVoucher09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopeeVoucher-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCShopeeVoucher Post condition =="
        dumplogger.info("== Setup PHPCShopeeVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
