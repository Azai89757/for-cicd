import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCSharingPanel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSharingPanel Precondition =="
        dumplogger.info("== Setup THPCSharingPanel Precondition ==")

    def test_THPCSharingPanel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSharingPanel-01.xml")

    def test_THPCSharingPanel02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSharingPanel-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSharingPanel Post condition =="
        dumplogger.info("== Setup THPCSharingPanel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
