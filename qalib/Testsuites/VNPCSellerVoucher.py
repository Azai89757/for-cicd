import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSellerVoucher Precondition =="
        dumplogger.info("== Setup VNPCSellerVoucher Precondition ==")

    def test_VNPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-01.xml")

    def test_VNPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-02.xml")

    def test_VNPCSellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-03.xml")

    def test_VNPCSellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-04.xml")

    def test_VNPCSellerVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-05.xml")

    def test_VNPCSellerVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-06.xml")

    def test_VNPCSellerVoucher07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-07.xml")

    # def test_VNPCSellerVoucher08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCSellerVoucher-08.xml")

    # def test_VNPCSellerVoucher09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCSellerVoucher-09.xml")

    # def test_VNPCSellerVoucher10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCSellerVoucher-10.xml")

    # def test_VNPCSellerVoucher11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCSellerVoucher-11.xml")

    def test_VNPCSellerVoucher12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-12.xml")

    def test_VNPCSellerVoucher13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-13.xml")

    # def test_VNPCSellerVoucher14(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCSellerVoucher-14.xml")

    def test_VNPCSellerVoucher15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-15.xml")

    def test_VNPCSellerVoucher16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerVoucher-16.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSellerVoucher Post condition =="
        dumplogger.info("== Setup VNPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
