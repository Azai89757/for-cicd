import unittest
from Config import dumplogger
from FrameWorkBase import *
from api.spex import SpexAPICore
import Config


class TWSpexNotiAdmin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexNotiAdmin Precondition =="
        dumplogger.info("== Setup TWSpexNotiAdmin Precondition ==")

        ##Tail log file to grep "Local connections to /run/spex/spex.sock:-2 forwarded to remote address /run/spex/spex.sock"
        ##If Spex InitResult is True, will not call RegisterToSpex() again
        if SpexAPICore.InspectSpexConnection():
            ##Register to Spex with specific type of spex configuration for the first time
            if not Config._SpexRegResult_:
                if SpexAPICore.GetSpexConfig('Api_Test_Staging_TW'):
                    Config._SpexRegResult_ = SpexAPICore.RegisterToSpex()

                    ##Check outcome after register
                    if Config._SpexRegResult_:
                        print "== Setup TWSpexNotiAdmin Register Success =="
                        dumplogger.info("== Setup TWSpexNotiAdmin Register Success ==")

                    else:
                        print ("Spex Register Failed!!!")
                        dumplogger.error("Spex Register Failed")
                        ##Directly Leave Setup
                        self.skipTest("")
                else:
                    dumplogger.error("Get Spex config from ini failed!!!")
                    self.skipTest("")
            else:
                dumplogger.info("Spex is already registered.")

        else:
            print "== Setup TWSpexNotiAdmin Precondition Fail by No Spex Agent Connection =="
            dumplogger.error("== Setup TWSpexNotiAdmin Precondition Fail by No Spex Agent Connection ==")
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWSpexNotiAdmin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-01.xml")

    def test_TWSpexNotiAdmin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-02.xml")

    def test_TWSpexNotiAdmin03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-03.xml")

    def test_TWSpexNotiAdmin04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-04.xml")

    def test_TWSpexNotiAdmin05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-05.xml")

    def test_TWSpexNotiAdmin06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-06.xml")

    def test_TWSpexNotiAdmin07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiAdmin-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexNotiAdmin Post condition =="
        dumplogger.info("== Setup TWSpexNotiAdmin Post condition ==")
