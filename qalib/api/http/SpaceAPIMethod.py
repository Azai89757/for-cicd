#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SpaceAPIMethod.py: The def of this file called by other function.
'''
##Import system library
import json

##Import Framework common library
import Config
import FrameWorkBase
import DecoratorHelper
import GlobalAdapter
from Config import dumplogger

##Import api library
from api import APICommonMethod
from api.http import i18NJsonAPIMethod
import HttpAPICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class SpaceCronjobAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    def RunCronjob(arg):
        '''RunCronjob : Run cronjob in space shopee io
            Input argu :
                    N/A
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Run cronjob on space shopee io
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "run_space_cronjob", "result": "1"})
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SpaceCronjobAPI.RunCronjob')

    @staticmethod
    def RunCODCronjob(arg):
        '''RunCODCronjob : Run cronjob in space shopee io
            Input argu :
                    N/A
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cod_job_id = {"vn": "1632987198916", "th": "1632986285899", "ph": "1632987299196", "mx": "1632987451692", "my": "1632987314429", "br": "1632987451692",
                      "co": "1632987451692", "cl": "1632987451692", "tw": "1632987238794", "id": "1632987265711", "in": "1636080051086"}

        ##Initial API Helper
        AP = i18NJsonAPIMethod.i18NJsonController()

        ##Get space io token back from API
        space_response = AP.GetSpaceIOToken()

        ##Run cronjob on space shopee io
        slash = Config.dict_systemslash[Config._platform_]
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common" + slash + "order" + slash + "space_cronjob", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "run_space_cod_cronjob", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["Authorization"] = GlobalAdapter.APIVar._HttpHeaders_["Authorization"].replace("token_to_be_replaced", json.loads(space_response["text"])["token"])

        ##Assign job id
        GlobalAdapter.APIVar._HttpPayload_["job_id"] = "ordercodscreening-orderchange-staging.v2." + cod_job_id[Config._TestCaseRegion_.lower()]

        ##Assign order id
        GlobalAdapter.APIVar._HttpPayload_["options"]["order_ids"] = GlobalAdapter.OrderE2EVar._OrderID_

        ##Assign country id
        GlobalAdapter.APIVar._HttpPayload_["cid"] = Config._TestCaseRegion_.lower()

        ##Convert json to string
        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Send http request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SpaceCronjobAPI.RunCODCronjob')

    @staticmethod
    def RunShipByDateCronjob(arg):
        '''RunShipByDateCronjob : Run ship by date in space shopee io
            Input argu :
                    N/A
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sbd_id = {"vn": "1630035761834", "th": "1630035760943", "ph": "1630035759541", "mx": "1630035757108", "my": "1630035758211", "br": "1630035754921",
                  "co": "1630035756012", "cl": "1630035755451", "sg": "1630035760466", "tw": "1630035761501", "id": "1630035756459", "pl": "1630035797984",
                  "es": "1630035798530", "in": "1630035799449", "fr": "1630035799059", "ar": "1630035797435"}

        if Config._TestCaseRegion_.lower() in sbd_id.keys():
            ##Initial API Helper
            AP = i18NJsonAPIMethod.i18NJsonController()

            ##Get space io token back from API
            space_response = AP.GetSpaceIOToken()

            ##Run cronjob on space shopee io
            ##HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common\\order\\space_cronjob", "result": "1"})
            slash = Config.dict_systemslash[Config._platform_]
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common" + slash + "order" + slash + "space_cronjob", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "run_space_sbd_cronjob", "result": "1"})
            GlobalAdapter.APIVar._HttpHeaders_["Authorization"] = GlobalAdapter.APIVar._HttpHeaders_["Authorization"].replace("token_to_be_replaced", json.loads(space_response["text"])["token"])

            ##Get order id
            GlobalAdapter.APIVar._HttpPayload_["options"]["order_ids"] = GlobalAdapter.APIVar._HttpPayload_["options"]["order_ids"].replace("order_id_to_be_replaced", GlobalAdapter.OrderE2EVar._OrderID_)
            GlobalAdapter.APIVar._HttpPayload_["job_id"] = GlobalAdapter.APIVar._HttpPayload_["job_id"].replace("id_to_be_replaced", sbd_id[Config._TestCaseRegion_.lower()])
            GlobalAdapter.APIVar._HttpPayload_["cid"] = Config._TestCaseRegion_.lower()

            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})
        else:
            dumplogger.info("Please check your region in SpaceCronjobAPI.RunShipByDateCronjob ")
            ret = 0

        OK(ret, int(arg['result']), 'SpaceCronjobAPI.RunShipByDateCronjob')

    @staticmethod
    def UpdateItemFlag(arg):
        '''UpdateItemFlag : Update item flag cronjob (path: shopee-promotion/Checkout/UpdateItemFlag/Correct Green Truck Icon for Items) in space shopee io
            Input argu :
                    N/A
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial API Helper
        AP = i18NJsonAPIMethod.i18NJsonController()

        ##Get space io token back from API
        space_response = AP.GetSpaceIOToken()

        ##Run cronjob on space shopee io
        slash = Config.dict_systemslash[Config._platform_]
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "update_item_flag", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["Authorization"] = GlobalAdapter.APIVar._HttpHeaders_["Authorization"].replace("token_to_be_replaced", json.loads(space_response["text"])["token"])

        ##Assign cid (region)
        GlobalAdapter.APIVar._HttpPayload_["cid"] = Config._TestCaseRegion_.lower()

        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SpaceCronjobAPI.UpdateItemFlag')

    @staticmethod
    def SendReceipt(arg):
        '''
        SendReceipt : Run space cronjob to send e-receipt
            Input argu :N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        ##Initial API Helper
        AP = i18NJsonAPIMethod.i18NJsonController()

        ##Get space io token back from API
        space_response = AP.GetSpaceIOToken()

        ##Run cronjob on space shopee io
        slash = Config.dict_systemslash[Config._platform_]
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common" + slash + "order" + slash + "space_cronjob", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "run_space_receipt_send_cronjob", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["Authorization"] = GlobalAdapter.APIVar._HttpHeaders_["Authorization"].replace("token_to_be_replaced", json.loads(space_response["text"])["token"])

        ##Send http request
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SpaceCronjobAPI.SendReceipt')

    @staticmethod
    def ReceiveReceipt(arg):
        '''
        ReceiptReceive : Run space cronjob to receive e-receipt
            Input argu :N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        ##Initial API Helper
        AP = i18NJsonAPIMethod.i18NJsonController()

        ##Get space io token back from API
        space_response = AP.GetSpaceIOToken()

        ##Run cronjob on space shopee io
        slash = Config.dict_systemslash[Config._platform_]
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common" + slash + "order" + slash + "space_cronjob", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "run_space_receipt_receive_cronjob", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["Authorization"] = GlobalAdapter.APIVar._HttpHeaders_["Authorization"].replace("token_to_be_replaced", json.loads(space_response["text"])["token"])

        ##Send http request
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SpaceCronjobAPI.ReceiveReceipt')
