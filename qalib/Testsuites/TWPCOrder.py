import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCOrder Precondition =="
        dumplogger.info("== Setup TWPCOrder Precondition ==")

    def test_TWPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-01.xml")

    def test_TWPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-02.xml")

    def test_TWPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-03.xml")

    def test_TWPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-04.xml")

    def test_TWPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-05.xml")

    def test_TWPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-06.xml")

    def test_TWPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-07.xml")

    def test_TWPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-08.xml")

    def test_TWPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCOrder-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCOrder Post condition =="
        dumplogger.info("== Setup TWPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
