import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNiOSPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSPayment Precondition =="
        dumplogger.info("== Setup VNiOSPayment Precondition ==")

        ##Get device id
        Config._DeviceID_ = GetiOSDeviceId("iOS")

        #Get device info by device id
        if Config._DeviceID_:

            ##Start Appium
            iOSStartAppium({"result": "1"})

            ##Get iOS version of this device
            device_version = GetiOSDeviceInfo()

            ##Call function from iOSBaseUICore
            print "Device connection check: Pass"
            print "Get device version: " + device_version

            ##Assign device platform version & device udid to global var
            Config._iOSDesiredCaps_['udid'] = Config._DeviceID_

            ##Call function from iOSBaseUICore
            iOSInitialDriver()

        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_VNiOSPayment001(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSPayment-001.xml")

    def test_VNiOSPayment002(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSPayment-002.xml")

    def test_VNiOSPayment003(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSPayment-003.xml")

    def test_VNiOSPayment004(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSPayment-004.xml")

    def test_VNiOSPayment005(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSPayment-005.xml")

    def test_VNiOSPayment006(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSPayment-006.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSPayment Post condition =="
        dumplogger.info("== Setup VNiOSPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
