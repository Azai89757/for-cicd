﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidBaseUILogic.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
import Config
from Config import dumplogger
from Config import _InstantWaitingPeriod_

##Import Android library
import AndroidBaseUICore

##Import selenium related library
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidGetAndCheckElements(arg):
    '''
    AndroidGetAndCheckElements : Get the elements and reserve to global variable
            Input argu :
                locate - path
                string - compare string
                isfuzzy - Fuzzy comparison or perfect comparison
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    string = arg["string"]
    locate = arg["locate"]
    isfuzzy = ""

    ##Error handle for variable
    try:
        isfuzzy = arg["isfuzzy"]
    except KeyError:
        isfuzzy = "0"
        dumplogger.exception('KeyError - fuzzy default is 0')
    except:
        isfuzzy = "0"
        dumplogger.exception('Other error - fuzzy default is 0')
    finally:
        dumplogger.info("isfuzzy = %s" % (isfuzzy))

    ##Get elements
    AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
    AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
    AndroidBaseUICore.AndroidParseElements({"keyword": string, "isfuzzy": isfuzzy, "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGetAndCheckElements')

@DecoratorHelper.FuncRecorder
def AndroidGetAndCheckElementAttribute(arg):
    '''
    AndroidGetAndCheckElementAttribute : Get the elements and reserve to global variable and check element attribute
            Input argu :
                method - xpath, id, name, classname, css
                locate - path
                string - attribute string to be compared
                attribute - attribute to be compared
                isfuzzy - Fuzzy comparison or perfect comparison
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    method = arg["method"]
    string = arg["string"]
    locate = arg["locate"]
    attribute = arg["attribute"]
    isfuzzy = ""

    ##Error handle for variable
    try:
        isfuzzy = arg["isfuzzy"]
    except KeyError:
        isfuzzy = "0"
        dumplogger.exception('KeyError - fuzzy default is 0')
    except:
        isfuzzy = "0"
        dumplogger.exception('Other error - fuzzy default is 0')
    finally:
        dumplogger.info("isfuzzy = %s" % (isfuzzy))

    ##Get elements
    AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
    AndroidBaseUICore.AndroidGetAttributes({"attrtype":"get_attribute", "attribute":attribute, "mode":"single", "result": "1"})
    AndroidBaseUICore.AndroidParseElements({"keyword": string, "isfuzzy": isfuzzy, "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGetAndCheckElementAttribute->')

def AndroidMoveAndCheckElement(arg):
    '''
    AndroidMoveAndCheckElement : Swipe screen until element appears
            Input argu :
                locate - element
                direction - left/right/up/down
                isclick : 1 (click locate and check) / 0 (won't click locate, just check)
                passok : 1 (enter OK) / 0 (return check bool)
                times - total swipe down time for check element
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    dumplogger.info("Enter AndroidMoveAndCheckElement")

    locate = arg["locate"]
    direction = arg["direction"]
    isclick = arg["isclick"]
    message = "Exist"
    ret = 1

    if "times" in arg:
        times = int(arg['times'])
    else:
        times = 15

    if "passok" in arg:
        passok = arg['passok']
    else:
        passok = "1"

    for count in range(times):
        try:
            if isclick == "1":
                time.sleep(3)
                WebDriverWait(AndroidBaseUICore._AndroidDriver_, _InstantWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).click()
            else:
                WebDriverWait(AndroidBaseUICore._AndroidDriver_, _InstantWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate))))
            dumplogger.info("Element exists at the current screen... will break the loop and go next")
            break
        except:
            ##Can not locate element, keep slide
            AndroidRelativeMove({"direction": direction, "times": "1", "result": "1"})
            dumplogger.info("Element doesn't exist at the current screen..start to swipe")

        if count == (times-1):
            message = "Fail to locate element after swipe. Element xpath => %s" % (locate)
            dumplogger.info(message)
            ret = 0

    if passok == "1":
        OK(ret, int(arg['result']), 'AndroidMoveAndCheckElement->' + message)
    else:
        return ret

@DecoratorHelper.FuncRecorder
def AndroidRelativeMove(arg):
    '''
    AndroidRelativeMove : Swipe screen until element appears
            Input argu :
                direction - left/right/up/down
                times - move times, default 1
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    direction = arg["direction"]
    window_size = AndroidBaseUICore._AndroidDriver_.get_window_size()
    message = ""
    ret = 1

    if "times" in arg:
        times = int(arg['times'])
    else:
        times = 1

    try:
        for key,value in window_size.iteritems():
            if key == 'width':
                start_x = int(value) * 0.6
                end_x = int(value) * 0.3
            elif key == 'height':
                start_y = int(value) * 0.6
                end_y = int(value) * 0.3

    except:
        message = "Encounter Error."
        dumplogger.exception(message)

    for swipe_times in range(times):
        if direction == "right":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":start_x, "locate_y":start_y, "movex":end_x, "movey":start_y, "result": "1"})
        if direction == "left":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":end_x, "locate_y":start_y, "movex":start_x, "movey":start_y, "result": "1"})
        if direction == "down":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":start_x, "locate_y":start_y, "movex":start_x, "movey":end_y, "result": "1"})
        if direction == "up":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":start_x, "locate_y":end_y, "movex":start_x, "movey":start_y, "result": "1"})
        dumplogger.info("Swipe direction: %s, %d times" % (direction, swipe_times))

    OK(ret, int(arg['result']), 'AndroidRelativeMove -> ' + message)

@DecoratorHelper.FuncRecorder
def AndroidDirectMove(arg):
    '''
    AndroidDirectMove : Direct swipe screen from an initial element position and you can swipe many times
            Input argu :
                locate - initial element for swipe
                direction - left/right/up/down
                times - how many times you want to swipe
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg["locate"]
    direction = arg["direction"]
    times = int(arg["times"])
    ret = 1

    ##Get initial element location for reference
    AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
    AndroidBaseUICore.AndroidGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
    locate_x = GlobalAdapter.CommonVar._PageAttributes_['x']
    locate_y = GlobalAdapter.CommonVar._PageAttributes_['y']

    ##Get element size
    AndroidBaseUICore.AndroidGetAttributes({"attrtype":"size", "mode":"single", "result": "1"})
    width = min(GlobalAdapter.CommonVar._PageAttributes_['width'], 1080)
    height = GlobalAdapter.CommonVar._PageAttributes_['height']

    ##Swipe element in given direction for certain times
    for swipe_times in range(int(times)):
        if direction == "right":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x + width * 0.8, "locate_y":locate_y + height * 0.5, "movex":locate_x + width * 0.2, "movey":locate_y + height * 0.5, "result": "1"})
        if direction == "left":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x + width * 0.2, "locate_y":locate_y + height * 0.5, "movex":locate_x + width * 0.8, "movey":locate_y + height * 0.5, "result": "1"})
        if direction == "down":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x + width * 0.5, "locate_y":locate_y + height * 0.8, "movex":locate_x + width * 0.5, "movey":locate_y + height * 0.2, "result": "1"})
        if direction == "up":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x + width * 0.5, "locate_y":locate_y + height * 0.2, "movex":locate_x + width * 0.5, "movey":locate_y + height * 0.8, "result": "1"})
        dumplogger.info("Swipe direction: %s, %d times" % (direction, swipe_times))

    OK(ret, int(arg['result']), 'AndroidDirectMove')

@DecoratorHelper.FuncRecorder
def AndroidGetAndReserveElements(arg):
    '''
    AndroidGetAndReserveElements : Get the elements and reserve to global list
            Input argu :
                locate - path
                reserve_type - which one you would like to reserve
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg["locate"]
    reservetype = arg["reservetype"]
    message = "Type - %s" % (reservetype)
    ret = 1

    try:
        ##Reserve elements
        if "ordersn" in reservetype:
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            ##Check key is in order sn dict
            if reservetype in GlobalAdapter.OrderE2EVar._OrderSNDict_:
                dumplogger.info("%s key already in ordersn dict" % (reservetype))
            else:
                ##Assign key
                GlobalAdapter.OrderE2EVar._OrderSNDict_[reservetype] = []
            ##Append order sn into list with key name xxx_ordersn
            GlobalAdapter.OrderE2EVar._OrderSNDict_[reservetype].append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.OrderE2EVar._OrderSNDict_)

        elif reservetype == "count":
            ##Get elements
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"multi", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"count", "mode":"multi", "result": "1"})
            GlobalAdapter.GeneralE2EVar._Count_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.GeneralE2EVar._Count_)

        elif reservetype == "indomaret_id":
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"get_attribute", "mode":"single", "attribute":"content-desc", "result": "1"})
            GlobalAdapter.PaymentE2EVar._IndomaretID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._IndomaretID_)

        elif reservetype == "topup_id":
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._TopUpID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._TopUpID_)

        elif reservetype == "bank_va_number":
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._BankVANumber_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._BankVANumber_)

        elif reservetype == "atm_ref1":
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._ATMRef1_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._ATMRef1_)

        elif reservetype == "wallet_original_shopee_coins":
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.CoinsE2EVar._OriginalShopeeCoins_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.CoinsE2EVar._OriginalShopeeCoins_)

        elif reservetype == "wallet_get_shopee_coins":
            ##Get elements and save to global
            AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
            AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.CoinsE2EVar._GetShopeeCoins_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.CoinsE2EVar._GetShopeeCoins_)

    except:
        message = "Other error - fuzzy default is 0"
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'AndroidGetAndReserveElements -> ' + message)

@DecoratorHelper.FuncRecorder
def AndroidMoveElementToPosition(arg):
    '''
    AndroidMoveElementToPosition : Move specific element to specific height of screen
            Input argu :
                locate - locate the element going to move
                ratio - move element to specific height ratio of the screen (0 ~ 1)
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    locate = arg["locate"]
    ratio = arg['ratio']
    message = "Move to ratio %s" % (ratio)

    ##Adjust component position until component is under screen view, also cannot swipe too fast
    AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
    AndroidBaseUICore.AndroidGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
    locate_y = GlobalAdapter.CommonVar._PageAttributes_['y']
    if Config._DesiredCaps_['automationName'] == "Espresso" and int(locate_y) >= 1700:
        temp_y = locate_y
        while(temp_y >= 2200):
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":6, "locate_y":1500, "movex":6, "movey":1000, "result": "1"})
            temp_y -= 500
            time.sleep(2)
        AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":6, "locate_y":1500, "movex":6, "movey":1500 - (temp_y - 1700), "result": "1"})

    ##Locate element's coordinates
    element = WebDriverWait(AndroidBaseUICore._AndroidDriver_, 15).until(EC.visibility_of_element_located((By.XPATH, (locate))))
    y_cor = int(element.location['y'])
    window_size = AndroidBaseUICore._AndroidDriver_.get_window_size()

    try:
        for key,value in window_size.iteritems():
            if key == 'height':
                end_y = int(value)*float(ratio)
    except:
        message = "Encounter Error."
        dumplogger.exception(message)
        ret = 0

    ##Move element to specific ratio of the screen
    AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":6, "locate_y":y_cor, "movex":6, "movey":end_y, "result": "1"})
    time.sleep(5)

    OK(ret, int(arg['result']), 'AndroidMoveElementToPosition -> ' + message)

@DecoratorHelper.FuncRecorder
def AndroidCheckToastMessages(arg):
    '''
    AndroidCheckToastMessages : Check toast messages in espresso driver
            Input argu :
                toast_text - the toast messages you want to check
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    toast_text = arg['toast_text']

    ##Check toast messages is visible
    ret = AndroidBaseUICore._AndroidDriver_.execute_script('mobile:isToastVisible', {'text': toast_text})

    OK(ret, int(arg['result']), 'AndroidCheckToastMessages')
