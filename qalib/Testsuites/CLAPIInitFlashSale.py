import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLAPIInitFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIInitFlashSale Precondition =="
        dumplogger.info("== Setup CLAPIInitFlashSale Precondition ==")

    def test_CLAPIInitFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/CLAPIInitFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIInitFlashSale Post condition =="
        dumplogger.info("== Setup CLAPIInitFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
