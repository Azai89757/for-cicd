#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminUsersMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import admin library
import AdminCommonMethod

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminUserInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToUserInfoSubTab(arg):
        '''
        GoToUserInfoSubTab : Go to user info subtab
                Input argu :
                    subtab - subtab name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error

        '''
        ret = 1
        subtab = arg['subtab']

        ##Click a sub item in user info page
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in user info page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.GoToUserInfoSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UserAccountRestoreStatus(arg):
        '''
        UserAccountRestoreStatus : Check and restore the account status to normal/deleted
                Input argu :
                    account - shopee account
                    status - status that account restore to (normal/deleted/frozen/banned)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        status = arg["status"]
        ret = 1

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "user_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "search_user"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Check target user
        xpath = Util.GetXpath({"locate": "check_target_user"})
        xpath_replace = xpath.replace("account_to_be_replaced", account)

        ##Click target user into detail page.
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_replace, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "click_target_user"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target table", "result": "1"})

            ##Check account status
            xpath = Util.GetXpath({"locate": "check_account_status"})
            BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            account_status = GlobalAdapter.CommonVar._PageAttributes_.split()[-1].lower()
            dumplogger.info("Account status is %s" % (account_status))

            if status == account_status:
                dumplogger.info("Account status is the same as desired status, skip the process")

            else:
                if status == "deleted":
                    ##Change account status to Deleted
                    AdminUserInfoPage.ChangeAccountStatusToDeleted({"status_tag": account_status, "result": "1"})

                elif status == 'normal':
                    ##Change account status to Normal
                    AdminUserInfoPage.ChangeAccountStatusToNormal({"status_tag": account_status, "result": "1"})

                elif status == "frozen":
                    ##Change account status to Frozen
                    AdminUserInfoPage.ChangeAccountStatusToFrozen({"status_tag": account_status, "result": "1"})

                elif status == "banned":
                    ##Change account status to Banned
                    AdminUserInfoPage.ChangeAccountStatusToBanned({"status_tag": account_status, "result": "1"})

        else:
            dumplogger.info("Search account in user table fail !")
            ret = 0

        OK(ret, int(arg['result']), 'AdminUserInfoPage.UserAccountRestoreStatus -> ' + account + ' status restores to ' + status)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAccountStatusToNormal(arg):
        '''
        ChangeAccountStatusToNormal : Change account status to normal
                Input argu :
                    status_tag : it should be normal/deleted/frozen/banned
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status_tag = arg['status_tag']

        ##Click status button
        xpath = Util.GetXpath({"locate": "status_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Status button", "result": "1"})

        ##Perform normal action
        xpath = Util.GetXpath({"locate": "normal"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click normal", "result": "1"})

        if status_tag in ('banned', 'frozen'):

            ##Remove invalid tag
            xpath = Util.GetXpath({"locate": "delete_invalid_tag"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete user fraud tag", "result": "1"})

            ##Click drop down reason
            xpath = Util.GetXpath({"locate": "reason_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown", "result": "1"})

            ##Choose system reason
            xpath = Util.GetXpath({"locate": "normal_reason"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CC Points confirmed", "result": "1"})

        ##Enter reason
        xpath = Util.GetXpath({"locate": "change_status_reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation Restore", "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Submit", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ChangeAccountStatusToNormal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAccountStatusToDeleted(arg):
        '''
        ChangeAccountStatusToDeleted : Change account status to deleted
                Input argu :
                    status_tag : it should be normal/deleted/frozen/banned
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status_tag = arg['status_tag']

        if status_tag in ('banned', 'frozen'):

            ##Change status to normal
            AdminUserInfoPage.ChangeAccountStatusToNormal({"status_tag": status_tag, "result": "1"})

        ##Click status button
        xpath = Util.GetXpath({"locate": "status_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Status button", "result": "1"})

        #Perform delete action
        xpath2 = Util.GetXpath({"locate": "delete_option"})
        BaseUICore.Click({"method": "xpath", "locate": xpath2, "message": "Click delete", "result": "1"})

        ##Enter delete reason
        xpath = Util.GetXpath({"locate": "change_status_reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation Restore", "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Submit", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ChangeAccountStatusToDeleted')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAccountStatusToFrozen(arg):
        '''
        ChangeAccountStatusToFrozen : Change account status to frozen
                Input argu :
                    status_tag : it should be normal/deleted/frozen/banned
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status_tag = arg['status_tag']

        if status_tag in ('banned', 'deleted'):

            ##Change status to normal
            AdminUserInfoPage.ChangeAccountStatusToNormal({"status_tag": status_tag, "result": "1"})

        ##Click status button
        xpath = Util.GetXpath({"locate": "status_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Status button", "result": "1"})

        ##Perform frozen action
        xpath2 = Util.GetXpath({"locate": "frozen_option"})
        BaseUICore.Click({"method": "xpath", "locate": xpath2, "message": "Click frozen", "result": "1"})

        ##Remove invalid tag
        xpath = Util.GetXpath({"locate": "delete_invalid_tag"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete user fraud tag", "result": "1"})

        ##Click drop down reason
        xpath = Util.GetXpath({"locate": "reason_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown", "result": "1"})

        ##Choose system reason
        xpath = Util.GetXpath({"locate": "frozen_reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "FOR_AUTOMATION_TEST_DON'T_DELETE", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Enter frozen reason
        xpath = Util.GetXpath({"locate": "change_status_reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation Restore", "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Submit", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ChangeAccountStatusToFrozen')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAccountStatusToBanned(arg):
        '''
        ChangeAccountStatusToBanned : Change account status to banned
                Input argu :
                    status_tag : it should be normal/deleted/frozen/banned
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status_tag = arg['status_tag']

        ##Click status button
        xpath = Util.GetXpath({"locate": "status_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Status button", "result": "1"})

        ##Perform frozen action
        xpath2 = Util.GetXpath({"locate": "banned_option"})
        BaseUICore.Click({"method": "xpath", "locate": xpath2, "message": "Click banned", "result": "1"})

        if status_tag in ('normal', 'deleted'):

            ##Remove invalid tag
            xpath = Util.GetXpath({"locate": "delete_invalid_tag"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete na tag", "result": "1"})

            ##Click drop down reason
            xpath = Util.GetXpath({"locate": "reason_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown", "result": "1"})

            ##Choose system reason
            xpath = Util.GetXpath({"locate": "banned_reason"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "FOR_AUTOMATION_TEST_DON'T_DELETE", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)

        ##Enter banned reason
        xpath = Util.GetXpath({"locate": "change_status_reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation Restore", "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Submit", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ChangeAccountStatusToBanned')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyAccountStatus(arg):
        '''
        ModifyAccountStatus : Modify account status directly without checking (normal/deleted/banned/frozen)
                Input argu :
                    account - shopee account
                    type - account status type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]
        type = arg["type"]

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "user_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number": "5", "result": "1"})

        ##Search User
        xpath = Util.GetXpath({"locate": "search_user"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Click target user into detail page.
        xpath = Util.GetXpath({"locate": "check_target_user"})
        xpath_replace = xpath.replace("account_to_be_replaced", account)

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_replace, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "click_target_user"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target table", "result": "1"})

            ##Click status button and perform delete action
            xpath = Util.GetXpath({"locate": "status_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Status button", "result": "1"})
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status button", "result": "1"})

            ##Click ban all bank account
            if type == "banned":
                xpath = Util.GetXpath({"locate": "unban_bank_account"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Unclick ban all bank accounts", "result": "1"})

            if type == "normal":
                ## Click reason dropdown
                xpath = Util.GetXpath({"locate": "reason_dropdown"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown", "result": "1"})

                ## Type = normal account need to add NA tag and delete other tag
                xpath = Util.GetXpath({"locate": "delete_cc_tag"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete cc tag", "result": "1"})

                xpath = Util.GetXpath({"locate": "NA_confirmed"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click NA confirmed", "result": "1"})

            elif type in ("banned", "frozen"):
                ## Click reason dropdown
                xpath = Util.GetXpath({"locate": "reason_dropdown"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown", "result": "1"})

                ## Type = banned and Type = frozen account need to add CC Points tag and delete NA tag, while Type = deleted don't need to do this
                xpath = Util.GetXpath({"locate": "delete_na_tag"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete na tag", "result": "1"})

                xpath = Util.GetXpath({"locate": "CC_Points_confirmed"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CC Points confirmed", "result": "1"})

            time.sleep(5)

            ##Enter reason
            xpath = Util.GetXpath({"locate": "reason"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Modify user status Test", "result": "1"})

            time.sleep(5)

            ##Click submit button
            xpath = Util.GetXpath({"locate": "submit_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Submit", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ModifyAccountStatus -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PurgeCSCache(arg):
        '''
        PurgeCSCache : Search for user account and purge cs cache
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to purge cs cache
        xpath = Util.GetXpath({"locate": "purge_cs_cache_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to purge cs cache", "result": "1"})

        ##Handle alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        ##Click to purge mall cache
        xpath = Util.GetXpath({"locate": "purge_mall_cache_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to purge mall cache", "result": "1"})

        ##Handle two alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        time.sleep(2)
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.PurgeCSCache')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchUser(arg):
        '''
        SearchUser : Search for user account
                Input argu :
                    account - shopee account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        ret = 1

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "user_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "username_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Click target user into detail page.
        xpath = Util.GetXpath({"locate": "target_username"})
        xpath = xpath.replace('replace_account', account)
        time.sleep(2)

        ##Wait before table reload before next step
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Click user id in target table
        xpath = Util.GetXpath({"locate": "user_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target table", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.SearchUser -> Search for user account: ' + account)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchUserByRandomUsername(arg):
        '''
        SearchUserByRandomUsername : Search for random username
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Search random username in admin page
        random_username = GlobalAdapter.GeneralE2EVar._RandomString_
        AdminUserInfoPage.SearchUser({"account": random_username, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.SearchUserByRandomUsername -> Search for random username: ' + random_username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeUserNameButton(arg):
        '''
        ClickChangeUserNameButton : Click change user name button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change user name button
        xpath = Util.GetXpath({"locate": "change_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change user name button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ClickChangeUserNameButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit user info button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit user info button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change user name button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNewUserName(arg):
        '''
        InputNewUserName : Input new user name + click check button in edit user name page
                Input argu :
                    new_user - new user name
                    reason - reason to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        new_user = arg["new_user"]
        reason = arg["reason"]
        ret = 1

        ##Input user name
        xpath = Util.GetXpath({"locate": "username"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": new_user, "result": "1"})

        ##Click check button
        xpath = Util.GetXpath({"locate": "check_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click check button", "result": "1"})

        ##Input reason
        if reason:
            xpath = Util.GetXpath({"locate": "reason"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": new_user, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.InputNewUserName ->' + new_user)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditUserInfo(arg):
        '''
        EditUserInfo : Input new user name + click check button in edit user name page
                Input argu :
                    shop - shop name
                    email - email address
                    phone - phone number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        shop = arg["shop"]
        email = arg["email"]
        phone = arg["phone"]
        ret = 1

        if shop:
            ##Change shop name
            xpath = Util.GetXpath({"locate": "shop_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop, "result": "1"})

        if email:
            ##Change email
            xpath = Util.GetXpath({"locate": "email"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": email, "result": "1"})

        ##Change phone number (If doesn't input phone number then input random number with 00+country code in front)
        xpath = Util.GetXpath({"locate": "phone"})
        if phone:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": phone, "result": "1"})
        elif Config._TestCaseRegion_ == "VN":
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "00849" + XtFunc.GenerateRandomString(8, "numbers"), "result": "1"})
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "00" + XtFunc.GenerateRandomString(10, "numbers"), "result": "1"})

        ##Click Save
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.EditUserInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetLoginSignupTime(arg):
        '''
        GetLoginSignupTime : Get register / last login time
                Input argu :
                    check_type - login, signup
                    platform - pc, app
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_type = arg['check_type']
        platform = arg['platform']

        ##Get last login/ register time
        xpath = Util.GetXpath({"locate": check_type + "_" + platform})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        GlobalAdapter.AccountE2EVar._LoginSignupTimeList_.append(GlobalAdapter.CommonVar._PageAttributes_)
        dumplogger.info(GlobalAdapter.AccountE2EVar._LoginSignupTimeList_)

        OK(ret, int(arg['result']), "AdminUserInfoPage.GetLoginSignupTime")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLoginSignupTime(arg):
        '''
        CheckLoginSignupTime : Check register / last login time
                Input argu :
                    check_type - login, signup
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_type = arg['check_type']

        if check_type == 'login':
            ##If didn't pass, time might not update. Refresh browser and check again.
            for check_times in range(10):

                ##Get last login time
                xpath = Util.GetXpath({"locate": Config._TestCasePlatform_.lower()})
                BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
                BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

                ##Get time after login
                after_date = GlobalAdapter.CommonVar._PageAttributes_.split()[0]
                after_time = GlobalAdapter.CommonVar._PageAttributes_.split()[1]
                dumplogger.info("after time: %s %s" % (after_date, after_time))

                ##Get time before login
                before_date = GlobalAdapter.AccountE2EVar._LoginSignupTimeList_[-1].split()[0]
                before_time = GlobalAdapter.AccountE2EVar._LoginSignupTimeList_[-1].split()[1]
                dumplogger.info("before time: %s %s" % (before_date, before_time))

                ##Get hour and min
                after_time_hr = after_time.split(':')[0]
                after_time_min = after_time.split(':')[1]
                before_time_hr = before_time.split(':')[0]
                before_time_min = before_time.split(':')[1]

                ##If date before login was not same as date after login, login successfully
                if after_date != before_date:
                    dumplogger.info("Compare time success")
                    break
                ##If login on the same date not at same time, compare the hours
                elif after_time_hr > before_time_hr:
                    dumplogger.info("Compare time success")
                    break
                ##If login on the same date same hour, compare minutes.
                elif after_time_hr == before_time_hr and after_time_min > before_time_min:
                    dumplogger.info("Compare time success")
                    break
                else:
                    dumplogger.error("Compare time failed")
                    ret = 0
                    time.sleep(30)
                    BaseUILogic.BrowserRefresh({"message":"Browser refresh", "result": "1"})

        elif check_type == 'signup':

            ##Get current date
            if Config._TestCaseRegion_ in ("VN", "ID", "TH"):
                current_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", 0, 0, 0)
            elif Config._TestCaseRegion_ == "TW":
                current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)
            elif Config._TestCaseRegion_ == "PH":
                current_time = XtFunc.GetCurrentDateTime("%m/%d/%Y %H:%M", 0, 0, 0)

            current_date = current_time.split()[0]
            dumplogger.info("Current date: %s" % (current_date))

            ##Get signup date
            signup_date = GlobalAdapter.AccountE2EVar._LoginSignupTimeList_[-1].split()[0]
            dumplogger.info("Signup date: %s" % (signup_date))

            ##compare signup date
            if signup_date == current_date:
                dumplogger.info("Compare time success")
            else:
                dumplogger.error("Compare time failed")
                ret = 0

        else:
            dumplogger.info("Please input login or signup as argument to check time")
            ret = -1

        OK(ret, int(arg['result']), "AdminUserInfoPage.CheckLoginSignupTime")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeUserPhoneNumber(arg):
        '''
        ChangeUserPhoneNumber : Change user phone number and clean user cache
                Input argu :
                    account - target account to change
                    phone - phone number to be modified
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]
        phone = arg["phone"]

        ##Go to admin user tab
        AdminCommonMethod.AdminTab.ClickTabOnLeftPanel({"type": "user", "result": "1"})

        ##Search user
        AdminUserInfoPage.SearchUser({"account": account, "result": "1"})

        ##Click edit button in user info page
        AdminUserInfoPage.ClickEdit({"result": "1"})

        ##Input data to edit user info
        AdminUserInfoPage.EditUserInfo({"shop": "", "email": "", "phone": phone, "result": "1"})

        ##Clean user cache
        AdminUserInfoPage.PurgeCSCache({"account": account, "result": "1"})

        ##Wait for change to be effective
        time.sleep(3)

        OK(ret, int(arg['result']), "AdminUserInfoPage.ChangeUserPhoneNumber for account : " + account)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPurgeDevicesButton(arg):
        '''
        ClickPurgeDevicesButton : Clcik purge devices button to abnormal login OTP verification
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click purge devices button
        xpath = Util.GetXpath({"locate": "purge_devices_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Clcik purge devices button", "result": "1"})

        #Click pruge button to confirm purge
        xpath2 = Util.GetXpath({"locate": "purge_confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath2, "message": "Click purge confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ClickPurgeDevicesButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelinkAllSocialMediaButton(arg):
        '''
        ClickDelinkAllSocialMediaButton : Click delink social media button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delink social media button if exists
        xpath = Util.GetXpath({"locate": "delink_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Clcik delink social media button", "result": "1"})
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
            time.sleep(5)
        else:
            dumplogger.info("Delink btn not clickable")

        OK(ret, int(arg['result']), 'AdminUserInfoPage.ClickDelinkAllSocialMediaButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreGoogleSignupAccount(arg):
        '''
        RestoreGoogleSignupAccount : Restore Google Signup Account (delink gmail)
                Input argu :
                    gmail - user signup gmail
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        gmail = arg["gmail"]

        ##Check user table has displayed
        xpath = Util.GetXpath({"locate": "user_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Input email to search
        xpath = Util.GetXpath({"locate": "email_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": gmail, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Get gmail account xpath
        xpath = Util.GetXpath({"locate": "target_account"})
        xpath = xpath.replace('gmail_to_be_replace', gmail)

        ##If this gmail account exists
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click user id to enter user detail page
            xpath = Util.GetXpath({"locate": "user_id"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click user id to enter user detail page", "result": "1"})
            time.sleep(3)

            ##Click edit button on user detail page
            AdminUserInfoPage.ClickEdit({"result": "1"})

            ##Change email and add phone number
            AdminUserInfoPage.EditUserInfo({"shop": "", "email": "restored_" + gmail, "phone": "", "result": "1"})
            time.sleep(3)

            ##Click delink social media button
            AdminUserInfoPage.ClickDelinkAllSocialMediaButton({"result": "1"})

            ##Change user status to deleted
            AdminUserInfoPage.ChangeAccountStatusToDeleted({"status_tag": "Normal", "result": "1"})
            time.sleep(3)
        ##There is no account to restore
        else:
            dumplogger.info("There is no account to restore with this email")

        OK(ret, int(arg['result']), 'AdminUserInfoPage.RestoreGoogleSignupAccount')


class AdminUserPaymentInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetDefaultBankAccount(arg):
        '''
        ClickSetDefaultBankAccount : Click set default button
                Input argu :
                    bank_account - bank account number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank_account = arg["bank_account"]

        ##Click set default button
        xpath = Util.GetXpath({"locate": "set_default_btn"})
        xpath = xpath.replace("bank_account_to_be_replaced", bank_account)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set default button", "result": "1"})

        ##Handle alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        BaseUILogic.CheckPopupWindowMessage({"expected_message": 'success', "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPaymentInfoPage.ClickSetDefaultBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UserBankStatusChange(arg):
        '''
        UserBankStatusChange : Search for user account and change the account state to "Deleted"
                Input argu :
                    account - shopee account
                    type - bank account status type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        account_number = arg["account_number"]
        change_type = arg["type"]
        ret = 1

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "user_name_input_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        time.sleep(5)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Click target user into detail page.
        xpath = Util.GetXpath({"locate": "target_user_account"})
        xpath_replace = xpath.replace('string_to_be_replaced', account)
        time.sleep(2)

        ##Wait before table reload before next step
        BaseUILogic.CheckElementWithRetry({"locate": xpath_replace, "total_retry_number":"5", "result": "1"})

        ##Click user id in target table
        xpath = Util.GetXpath({"locate": "target_user_column"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target table", "result": "1"})

        ##Click Payment Information page.
        xpath = Util.GetXpath({"locate": "payment_information"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Payment Information", "result": "1"})

        ##Action : rejected/banned/checked
        ##Click Edit button.
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace("account_to_be_replaced", account_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Edit button", "result": "1"})

        ##Switch bank status
        xpath = Util.GetXpath({"locate": "change_bank_status_button"})
        if change_type == 'rejected':
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "3", "result": "1"})
        elif change_type == 'banned':
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "5", "result": "1"})
        elif change_type == 'checked':
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "4", "result": "1"})

        ##Click Save
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Save", "result": "1"})
        xpath = Util.GetXpath({"locate": "confirm_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Submit", "result": "1"})

        ##Handle alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminUserPaymentInfoPage.UserBankStatusChange -> Change Status:' + change_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddBankAccount(arg):
        '''
        AddBankAccount : Search for user account and add a new bank account
                Input argu :
                    account - shopee account
                    fullname - full name of user
                    ic_number - ic number of user
                    company_id - company registerd id
                    account_name - account name of user
                    bank_name - name of bank
                    region - region of bank
                    branch_name - branch name of bank
                    account_number - account number of bank account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        fullname = arg["fullname"]
        ic_number = arg["ic_number"]
        company_id = arg["company_id"]
        account_name = arg["account_name"]
        bank_name = arg["bank_name"]
        region = arg["region"]
        branch_name = arg["branch_name"]
        account_number = arg["account_number"]

        ret = 1

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "user_name_input_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Click target user into detail page.
        xpath = Util.GetXpath({"locate": "target_user_account"})
        xpath_replace = xpath.replace('string_to_be_replaced', account)
        time.sleep(2)

        ##Wait before table reload before next step
        BaseUILogic.CheckElementWithRetry({"locate": xpath_replace, "total_retry_number":"5", "result": "1"})

        ##Click user id in target table
        xpath = Util.GetXpath({"locate": "target_user_column"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target table", "result": "1"})

        ##Click Payment Information page.
        xpath = Util.GetXpath({"locate": "payment_information"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Payment Information", "result": "1"})

        ##Click to add a bank account
        xpath = Util.GetXpath({"locate": "add_bank_account_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to add a bank account", "result": "1"})

        ##Input Full name
        if fullname:
            xpath = Util.GetXpath({"locate": "full_name_input_column"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": fullname, "result": "1"})

        ##Input IC number
        if ic_number:
            xpath = Util.GetXpath({"locate": "ic_number_input_column"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ic_number, "result": "1"})

        ##Input Company ID
        if company_id:
            xpath = Util.GetXpath({"locate": "company_id_input_column"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": company_id, "result": "1"})

        if Config._TestCaseRegion_ == 'TW':
            ##Input Birthday in TW only
            xpath = Util.GetXpath({"locate": "tw_birthday_input_column"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "1980-08-07", "result": "1"})
            BaseUILogic.KeyboardAction({"locate": xpath, "actiontype": "enter", "result": "1"})

            ##Choose City in TW only
            xpath = Util.GetXpath({"locate": "tw_city_select_field"})
            BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": "台北市", "result": "1"})
            time.sleep(4)

            ##Choose District in TW only
            xpath = Util.GetXpath({"locate": "tw_district_select_field"})
            BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": "大安區", "result": "1"})

            ##Inpout Address in TW only
            xpath = Util.GetXpath({"locate": "tw_address_input_column"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "5566.st", "result": "1"})

        ##Inpout Bank Account Name
        xpath = Util.GetXpath({"locate": "account_name_input_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account_name, "result": "1"})

        ##Choose Bank Name
        xpath = Util.GetXpath({"locate": "bank_name_select_field"})
        BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": bank_name, "result": "1"})

        ##Choose Bank Region
        if Config._TestCaseRegion_ == "ID":
            xpath = Util.GetXpath({"locate":"region_select_field"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":region, "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": "region_select_field"})
            BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": region, "result": "1"})
        time.sleep(4)

        if Config._TestCaseRegion_ == 'TW':
            ##Choose Bank Branch Name
            xpath = Util.GetXpath({"locate": "branch_name_select_field"})
            BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": branch_name, "result": "1"})
        elif Config._TestCaseRegion_ in ("VN", "ID"):
            xpath = Util.GetXpath({"locate":"branch_name_select_field"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":branch_name, "result": "1"})

        ##Input Bank Account Number
        xpath = Util.GetXpath({"locate": "account_number_input_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account_number, "result": "1"})

        time.sleep(2)

        ##Click Submit btn
        xpath = Util.GetXpath({"locate": "submit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Submit btn", "result": "1"})
        time.sleep(2)

        ##Handle alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        BaseUILogic.CheckPopupWindowMessage({"expected_message": '{"success":true}', "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPaymentInfoPage.AddBankAccount')


class AdminUserCoinsLogPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderSN(arg):
        '''
        GetOrderSN : Get order SN in coins transaction log page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Reset order SN in web element
        XtFunc.ResetWebElementsList({"reset_element": "ordersn", "result": "1"})

        ##Get order sn
        xpath = Util.GetXpath({"locate":"order_sn"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"ordersn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserCoinsLogPage.GetOrderSN -> %s' % (GlobalAdapter.OrderE2EVar._OrderSNDict_))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchOrderSN(arg):
        '''
        SearchOrderSN : Search order SN in coins transaction log page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input order sn
        xpath = Util.GetXpath({"locate":"order_sn_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderSNDict_["ordersn"][-1], "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminUserCoinsLogPage.SearchOrderSN')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderStatus(arg):
        '''
        ClickOrderStatus : Click order status
                Input argu :
                    order_status - order_status
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        order_status = arg['order_status']

        xpath = Util.GetXpath({"locate": "drop_down_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list", "result": "1"})
        time.sleep(5)

        ##Click order status
        xpath = Util.GetXpath({"locate": order_status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion status:" + order_status, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminUserCoinsLogPage.ClickOrderStatus')


class AdminUserCoinsLogButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderIdLink(arg):
        '''
        ClickOrderIdLink : Click order id link
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        ##Click order id link
        xpath = Util.GetXpath({"locate": "order_id_link"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click order id link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserCoinsLogButton.ClickOrderIdLink')
