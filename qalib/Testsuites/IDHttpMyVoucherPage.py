import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpMyVoucherPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print ("== Setup IDHttpMyVoucherPage Precondition ==")
        dumplogger.info("== Setup IDHttpMyVoucherPage Precondition ==")

    def test_IDHttpMyVoucherPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMyVoucherPage-01.xml")

    def test_IDHttpMyVoucherPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMyVoucherPage-02.xml")

    def test_IDHttpMyVoucherPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpMyVoucherPage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print ("== Setup IDHttpMyVoucherPage Post condition ==")
        dumplogger.info("== Setup IDHttpMyVoucherPage Post condition ==")
