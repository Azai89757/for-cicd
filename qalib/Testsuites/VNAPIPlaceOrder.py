import unittest
from Config import dumplogger
from FrameWorkBase import *


class VNAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup VNAPIPlaceOrder Precondition ==")

    def test_VNAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup VNAPIPlaceOrder Post condition ==")
