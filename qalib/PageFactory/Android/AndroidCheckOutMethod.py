#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidCheckOutMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidCheckOutButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckOut(arg):
        '''
        ClickCheckOut : Click make order btn on Check out page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click make order
        xpath = Util.GetXpath({"locate":"make_order_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click make order btn on Check out page", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickCheckOut')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExpandShipType(arg):
        '''
        ExpandShipType : Expand a shipping type on checkout page
                Input argu :
                    ship_type : express / standard / economy / non-integrated / integrated / other
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ship_type = arg['ship_type']

        ##Click confirm btn
        xpath = Util.GetXpath({"locate":ship_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click expand element of ship type => " + ship_type, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ExpandShipType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseShippingType(arg):
        '''
        ChooseShippingType : ChooseShippingType
                Input argu :
                    shipping: shipping method name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping = arg['shipping']

        ##Check shipping mode exist in check out page
        xpath = Util.GetXpath({"locate":shipping})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result": "1"})

        if Config._TestCaseRegion_ == 'VN':
            ##Check if delivery time panel exist
            delivery_time_panel_xpath = Util.GetXpath({"locate": "delivery_time_panel"})

            ##Click delivery time option
            if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": delivery_time_panel_xpath, "message": "Check delivery time option is exist","passok": "0", "result": "1"}):
                time.sleep(3)
                delivery_time_option_xpath = Util.GetXpath({"locate": "delivery_time_option"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": delivery_time_option_xpath, "message": "Click delivery time option", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ChooseShippingType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm shipping button
                Input argu :
                    button_type: airpay_confirm / creditcard_confirm / shipping_confirm / wallet_confirm / payment_confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click confirm btn
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm btn on checkout page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button in checkout page
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel button in checkout page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetSellerVoucher(arg):
        '''
        ClickGetSellerVoucher : Click get seller voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click get seller voucher
        xpath = Util.GetXpath({"locate":"get_seller_voucher_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click get seller voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickGetSellerVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok button of popup warning message when choose unavailable payment method
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ok", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingMethod(arg):
        '''
        ClickShippingMethod : Click shipping method in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping_mode_position = arg['shipping_mode_position']

        ##Click shipping method
        xpath = Util.GetXpath({"locate":"shipping_mode"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath + '[' + shipping_mode_position + ']', "message": "Click change shipping method on checkout page", "result": "1"})

        if Config._TestCaseRegion_ == 'VN':
            delivery_time_panel_xpath = Util.GetXpath({"locate": "delivery_time_panel"})
            delivery_time_option_xpath = Util.GetXpath({"locate": "delivery_time_option"})

            ##Check if delivery time panel exist
            if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": delivery_time_panel_xpath, "message": "Check delivery time option is exist", "passok": "0", "result": "1"}):
                time.sleep(3)
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": delivery_time_option_xpath, "message": "Click delivery time option", "result": "1"})
                AndroidCheckOutButton.ClickConfirm({"button_type":"shipping_confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickShippingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseVoucher(arg):
        '''
        ClickChooseVoucher : Click choose voucher button in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose voucher button in checkout page
        xpath = Util.GetXpath({"locate":"choose_voucher_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose voucher button in checkout page", "result": "1"})
        ## Occur SPPT-18597 this Ticket need to sleep 3 sec for waiting choose voucher page loading
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickChooseVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentMethod(arg):
        '''
        ClickPaymentMethod : Click payment method in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click payment method
        xpath = Util.GetXpath({"locate":"payment_mode"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click change payment method on checkout page", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickPaymentMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropShippingMark(arg):
        '''
        ClickDropShippingMark : click the check mark to use drop shipping
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click the check mark to use drop shipping
        xpath = Util.GetXpath({"locate": "drop_check_mark"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click drop check mark", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickDropShippingMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BackToCheckoutPage(arg):
        '''
        BackToCheckoutPage : Click Back icon to back to checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        # Click Back icon to back to checkout page
        xpath = Util.GetXpath({"locate": "back_icon"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click back icon btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.BackToCheckoutPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPositive(arg):
        '''
        ClickPositive : Click positive button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click positive button
        locate = Util.GetXpath({"locate":"positive_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click positive button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickPositive')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNegative(arg):
        '''
        ClickNegative : Click negative button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click negative button
        locate = Util.GetXpath({"locate":"negative_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click negative button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickNegative')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCreditCard(arg):
        '''
        ClickAddCreditCard : Click add credit card button in choose payment method page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add btn
        xpath = Util.GetXpath({"locate":"add_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add new credit card btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickAddCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddInstallment(arg):
        '''
        ClickAddInstallment : Click add installment by credit card in choose payment method page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add btn
        xpath = Util.GetXpath({"locate":"add_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add installment by credit card btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.ClickAddInstallment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchShopeeCoinToggle(arg):
        '''
        SwitchShopeeCoinToggle : Click use shopee coin toggle
                Input argu :
                    switch_type - open/close
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        switch_type = arg['switch_type']

        ##Switch close/open for shopee coin toggle
        xpath = Util.GetXpath({"locate":switch_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Switch button to" + switch_type, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCheckOutButton.SwitchShopeeCoinToggle')


class AndroidCheckOutPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseShippingMethod(arg):
        '''
        ChooseShippingMethod : Choose a shipping method on checkout page
                Input argu :
                    shipping : blackcat - shipping by blackcat
                               711 - shipping by 711
                               family - shipping by family mart
                               hilife - shipping by hi-life
                               ok - shipping by ok mart
                               chinapost - shipping by china post
                               selfdeliver - shipping by self delivery
                               shipping_mode_position - Change shipping mode on which position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping = arg['shipping']
        shipping_mode_position = arg['shipping_mode_position']

        ##Click shipping method option
        AndroidCheckOutButton.ClickShippingMethod({"shipping_mode_position":shipping_mode_position, "result": "1"})

        ##Prevent ObjectStale error
        time.sleep(3)

        ##Choose shipping type
        AndroidCheckOutButton.ChooseShippingType({"shipping":shipping, "result": "1"})

        ##Waiting loading
        time.sleep(10)

        ##Click confirm btn
        AndroidCheckOutButton.ClickConfirm({"button_type":"shipping_confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.ChooseShippingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def WaitCheckoutLoadingIsComplete(arg):
        '''
        WaitCheckoutLoadingIsComplete : Wait checkout loading is complete
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"loading_popup"})

        ##Retry to check if loading image is exist
        for retry_times in range(10):
            if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Loading is not complete!, retry %d times" % (retry_times))
                time.sleep(2)
            else:
                dumplogger.info("Loading is complete!")
                break
        else:
            dumplogger.info("Loading is not complete!!!")
            ret = 0

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.WaitCheckoutLoadingIsComplete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMessage(arg):
        '''
        InputMessage: Input some Message in Checkout page
                Input argu :
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        message = arg['message']

        ##Input some message in message section
        xpath = Util.GetXpath({"locate": "message_section"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": message, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.InputMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRUTCode(arg):
        '''
        InputRUTCode : input RUT code
                Input argu :
                    rut - RUT code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rut = arg['rut']

        ##Input RUT code in RUT section
        xpath = Util.GetXpath({"locate": "rut_section"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": rut, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.InputRUTCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseAddress(arg):
        '''
        ChooseAddress : Choose specific address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Go to address page
        locate = Util.GetXpath({"locate":"address_field"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Choose specific address", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.ChooseAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTaxNotification(arg):
        '''
        ClickTaxNotification : Click tax notification section in checkoutpage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click tax notification section
        xpath = Util.GetXpath({"locate":"tax_notification"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click tax notification", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.ClickTaxNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseTaxNotification(arg):
        '''
        CloseTaxNotification : Click tax notification section in checkoutpage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click tax notification section
        xpath = Util.GetXpath({"locate":"close_tax_notification"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Close tax notification", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.CloseTaxNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDeliveryTime(arg):
        '''
        SelectDeliveryTime : Select delivery time in logistic page
                Input argu :
                    delivery_time - The time when you want delivery (anytime/worktime)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        delivery_time = arg['delivery_time']

        ##Click the time when you want delivery
        xpath = Util.GetXpath({"locate":delivery_time})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click the time you want delivery", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.SelectDeliveryTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectReceiptType(arg):
        '''
        SelectReceiptType : Select receipt type in order detail page
                Input argu :
                    receipt_type - donation_receipt
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        receipt_type = arg['receipt_type']

        ##Click dropdown icon
        xpath = Util.GetXpath({"locate":"select_recept_type"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click the drop down icon", "result": "1"})

        ##Click the receipt type when you want delivery
        xpath = Util.GetXpath({"locate":receipt_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click the receipt type", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutPage.SelectReceiptType')


class AndroidCheckOutComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        ##Click on button
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCheckOutComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column
                input_content - content to be inputed
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input RUT code in RUT section
        xpath = Util.GetXpath({"locate": column_type})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCheckOutComponent.InputToColumn')
