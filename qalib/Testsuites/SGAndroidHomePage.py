import sys
import os
import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *


class SGAndroidHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAndroidHomePage Precondition =="

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Disable wifi module by command only if proxy server is not used
            if not Config._EnableProxy_:
                ##Disable wifi module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc wifi disable"
                dumplogger.info("Disable emulator wifi module: %s" % (cmd))
                os.system(cmd)

                ##Enable mobile data module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc data enable"
                dumplogger.info("Enable emulator mobile data module: %s" % (cmd))
                os.system(cmd)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"sg", "result": "1"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_SGAndroidHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidHomePage-01.xml")

    def test_SGAndroidHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidHomePage-02.xml")

    '''def test_SGAndroidHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAndroidHomePage Post condition =="
        dumplogger.info("== Setup SGAndroidHomePage Post condition ==")
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
