import unittest
from Config import dumplogger
from FrameWorkBase import *


class MXAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup MXAPIPlaceOrder Precondition ==")

    def test_MXAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MXAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup MXAPIPlaceOrder Post condition ==")
