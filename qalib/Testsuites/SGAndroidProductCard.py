import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class SGAndroidProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAndroidProductCard Precondition =="
        dumplogger.info("== Setup SGAndroidProductCard Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"sg"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_SGAndroidProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidProductCard-01.xml")

    def test_SGAndroidProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidProductCard-02.xml")

    def test_SGAndroidProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidProductCard-03.xml")

    def test_SGAndroidProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidProductCard-04.xml")

    def test_SGAndroidProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidProductCard-05.xml")

    def test_SGAndroidProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGAndroidProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAndroidProductCard Post condition =="
        dumplogger.info("== Setup SGAndroidProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
