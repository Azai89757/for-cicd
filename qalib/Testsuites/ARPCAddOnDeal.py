import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCAddOnDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCAddOnDeal Precondition =="
        dumplogger.info("== Setup ARPCAddOnDeal Precondition ==")

    def test_ARPCAddOnDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCAddOnDeal-01.xml")

    def test_ARPCAddOnDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCAddOnDeal-02.xml")

    def test_ARPCAddOnDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCAddOnDeal-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCAddOnDeal Post condition =="
        dumplogger.info("== Setup ARPCAddOnDeal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
