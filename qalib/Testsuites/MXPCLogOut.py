import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCLogOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCLogOut Precondition =="
        dumplogger.info("== Setup MXPCLogOut Precondition ==")

    def test_MXPCLogOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCLogOut-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCLogOut Post condition =="
        dumplogger.info("== Setup MXPCLogOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
