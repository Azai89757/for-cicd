import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductCard Precondition =="
        dumplogger.info("== Setup TWHttpProductCard Precondition ==")

    def test_TWHttpProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductCard-01.xml")

    def test_TWHttpProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductCard-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductCard Post condition =="
        dumplogger.info("== Setup TWHttpProductCard Post condition ==")
