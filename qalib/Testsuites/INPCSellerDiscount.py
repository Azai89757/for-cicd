import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCSellerDiscount Precondition =="
        dumplogger.info("== Setup INPCSellerDiscount Precondition ==")

    def test_INPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerDiscount-01.xml")

    def test_INPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerDiscount-02.xml")

    def test_INPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerDiscount-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCSellerDiscount Post condition =="
        dumplogger.info("== Setup INPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
