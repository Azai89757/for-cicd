import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCHomePage Precondition =="
        dumplogger.info("== Setup PLPCHomePage Precondition ==")

    def test_PLPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCHomePage-01.xml")

    def test_PLPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCHomePage-02.xml")

    '''def test_PLPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCHomePage Post condition =="
        dumplogger.info("== Setup PLPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
