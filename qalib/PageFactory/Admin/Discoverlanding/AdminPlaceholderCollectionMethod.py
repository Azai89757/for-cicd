#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminPlaceholderCollectionMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminPlaceholderCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InpuActiveEndTime(arg):
        '''
        InpuActiveEndTime : Input active end time
                Input argu :
                    end_time - input end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = arg["end_time"]

        ##Already select time in time field
        xpath = Util.GetXpath({"locate": "delete_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click delete button to clear end time field
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        if end_time:
            ##Input active end time
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time", "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
            time.sleep(3)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPlaceholderCollectionPage.InpuActiveEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadFile(arg):
        '''
        UploadFile : Upload file
                Input argu :
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload file
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPlaceholderCollectionPage.UploadFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckFileStatusWithRefresh(arg):
        '''
        CheckFileStatusWithRefresh : Check the status of upload file
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]

        for refresh_times in range(5):

            #Find the status of upload file
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": file_name, "passok": "0", "result": "1"}):
                ret = 1
                break

            else:
                ret = 0
                dumplogger.info("Cannot find file status.")
                BaseUILogic.BrowserRefresh({"message": "Refresh browser.", "result": "1"})
                time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPlaceholderCollectionPage.CheckFileStatusWithRefresh')


class AdminPlaceholderCollectionComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminPlaceholderCollectionComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnIcon(arg):
        '''
        ClickOnIcon : Click any type of icon with well defined locator
            Input argu :
                icon_type - type of icon which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        ##Click on icon
        xpath = Util.GetXpath({"locate": icon_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon: %s, which xpath is %s" % (icon_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminPlaceholderCollectionComponent.ClickOnIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToTextField(arg):
        '''
        InputToTextField : Input any type of text field based on arguments
            Input argu :
                text_field_type - type of text field
                input_content - input content
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        text_field_type = arg["text_field_type"]
        input_content = arg["input_content"]

        ##Clear text field
        xpath = Util.GetXpath({"locate": text_field_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input something to text field
        if input_content:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPlaceholderCollectionComponent.InputToTextField')
