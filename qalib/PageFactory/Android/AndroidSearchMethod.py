#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidSearchMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidGlobalSearch(arg):
    '''
    AndroidGlobalSearch : Search product from home page
            Input argu :
                search_keyword - input key words to search product
                need_click - fill 1 for click search button to search item, fill 0 will go to pre-search page.
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    search_keyword = arg["search_keyword"]
    need_click = arg["need_click"]

    time.sleep(20)

    ##Check floating banner
    floating_xpath = Util.GetXpath({"locate":"floating_banner_xpath"})

    ##Get search bar's xpath in Top
    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":floating_xpath, "passok": "0", "result": "1"}):
        ##if floating banner exist, will go this xpath
        xpath = Util.GetXpath({"locate":"search_bar_with_floating"})
    else:
        xpath = Util.GetXpath({"locate":"global_search_bar"})

    ##Click search bar
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search bar", "result": "1"})
    time.sleep(10)

    ##Input keyword and send enter
    AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":Util.GetXpath({"locate":"typing_search_words"}), "string":search_keyword, "result": "1"})

    ##Check if or not need send enter
    if int(need_click):
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGlobalSearch')


class AndroidCategoryShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CategorySearch(arg):
        '''
        AndroidCategorySearch : Search target product in category shop page (only for android use)
                Input argu :
                    name - product name that you want to search for
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        ret = 1

        time.sleep(15)

        ##Click search bar
        xpath = Util.GetXpath({"locate":"category_search_bar"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search bar", "result": "1"})
        time.sleep(3)

        ##Input keyword and send enter
        xpath = Util.GetXpath({"locate":"typing_search_words"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":name, "result": "1"})
        time.sleep(3)
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCategoryShopPage.CategorySearch')


class AndroidMallShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MallSearch(arg):
        ''' AndroidMallSearch : Search target product in mall shop page (only for android use)
                Input argu :
                            name - product name that you want to search for
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        name = arg["name"]
        ret = 1

        time.sleep(15)

        ##Click search bar
        xpath = Util.GetXpath({"locate":"mall_search_bar"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search bar", "result": "1"})
        time.sleep(2)

        ##Input keyword and send enter
        xpath = Util.GetXpath({"locate":"typing_search_words"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":name, "result": "1"})
        time.sleep(3)

        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallShopPage.MallSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollowIcon(arg):
        '''
        ClickFollowIcon : Click follow icon in mall shop page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click follow icon in mall shop page
        xpath = Util.GetXpath({"locate":"follow_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click follow icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallShopPage.ClickFollowIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        '''
        ClickChatIcon : Click chat icon in mall shop page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click chat icon in mall shop page
        xpath = Util.GetXpath({"locate":"chat_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click chat icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallShopPage.ClickChatIcon')


class AndroidSearchButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSort(arg):
        '''
        ClickSort : Click sort button
                Input argu :
                    sort_type - label_relevance / label_latest / label_top_sales / label_price_lth / label_price_htl
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sort_type = arg["sort_type"]

        ##Click sort button
        xpath = Util.GetXpath({"locate":sort_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click sort button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchButton.ClickSort')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShortCutFilter(arg):
        '''
        ClickShortCutFilter : Click short cut button
                Input argu :
                    shortcut_name - Shopee Mall / Miễn phí vận chuyển / Yêu thích / Từ
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shortcut_name = arg["shortcut_name"]

        ##Click short cut button
        xpath = Util.GetXpath({"locate":"short_cut"})
        xpath = xpath.replace("string_to_replace", shortcut_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click short cut button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchButton.ClickShortCutFilter')


class AndroidSellerOwnShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProduct(arg):
        '''
        SearchProduct : Search target product in seller own shop page (only for android use)
                Input argu :
                    name - product name that you want to search for
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        ret = 1

        ##Click search bar
        xpath = Util.GetXpath({"locate":"search_bar"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search bar", "result": "1"})
        time.sleep(3)

        ##Input keyword and send enter
        xpath = Util.GetXpath({"locate":"typing_search_words"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":name, "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerOwnShopPage.SearchProduct')


class AndroidSearchBarPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputKeyword(arg):
        '''
        InputKeyword : input search keyword in search bar
                Input argu :
                    search_keyword - input key words to search product
                    need_click - fill 1 for click search button to search item, fill 0 will go to pre-search page.
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_keyword = arg["search_keyword"]
        need_click = arg["need_click"]

        ##Emulator can't using Sogou ime, using default ime and click coordinates
        ##Input keyword and send enter
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":Util.GetXpath({"locate":"typing_search_words"}), "string":search_keyword, "result": "1"})

        time.sleep(10)

        if int(need_click):
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchBarPage.InputKeyword')


class AndroidSearchBarButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchBar(arg):
        '''
        ClickSearchBar : Click search bar in Top
                Input argu :
                    page_type - search bar in page, ex: global / mall / collection / category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        time.sleep(15)

        if page_type == "global":
            ##Check floating banner exist brfore click global search bar in homepage
            floating_xpath = Util.GetXpath({"locate":"floating_banner_xpath"})

            ##Get search bar's xpath in global
            if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":floating_xpath, "passok": "0", "result": "1"}):
                xpath = Util.GetXpath({"locate":"search_bar_with_floating"})
            else:
                xpath = Util.GetXpath({"locate":page_type})

        else:
            ##Get search bar's xpath in which page
            xpath = Util.GetXpath({"locate":page_type})

        ##Click search bar in page
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search bar in Top", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchBarButton.ClickSearchBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelIcon(arg):
        '''
        ClickCancelIcon : Click cancel icon in search bar
                Input argu :
                    keyword - which keyword in seach bar
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        keyword = arg["keyword"]

        ##Click cancel icon in search bar
        xpath = Util.GetXpath({"locate":"cancel_icon"})
        xpath = xpath.replace("keyword_to_replace", keyword)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel icon in search bar", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchBarButton.ClickCancelIcon')


class AndroidPreSearchPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreHistory(arg):
        '''
        ClickSeeMoreHistory : Click see more history button in search history
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see more history button
        xpath = Util.GetXpath({"locate":"see_more_history"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see more history button in search history", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPreSearchPageButton.ClickSeeMoreHistory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCleanHistory(arg):
        '''
        ClickCleanHistory : Click clean history button in search history
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click clean history button
        xpath = Util.GetXpath({"locate":"clean_history"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click clean history button in search history", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPreSearchPageButton.ClickCleanHistory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        '''
        ClickCategory : Click recommendation category in pre-search page
                Input argu :
                    category_name - which category mock with recommendation ex.Điện Thoại & Phụ kiện/Túi Ví
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click recommendation category
        xpath = Util.GetXpath({"locate":"categories_slot"})
        xpath = xpath.replace("category_to_replace", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click recommendation category in pre-search page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPreSearchPageButton.ClickCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchDiscoveryKeyWord(arg):
        '''
        ClickSearchDiscoveryKeyWord : Click search discovery key word in pre-search page
                Input argu :
                    key_word - which Key Word with recommendation in pre-search page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        key_word = arg["key_word"]

        ##Click search discovery key word in pre-search page
        xpath = Util.GetXpath({"locate":"key_word_xpath"})
        xpath = xpath.replace("key_word_to_replace", key_word)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Search Discovery Key Word in pre-search page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPreSearchPageButton.ClickSearchDiscoveryKeyWord')


class AndroidSearchResultPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTryAgain(arg):
        '''
        ClickTryAgain : Click try again button in search result page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click try again button
        xpath = Util.GetXpath({"locate":"try_again"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click try again button in search result page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchResultPageButton.ClickTryAgain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchByImg(arg):
        '''
        ClickSearchByImg : Click search by img button in search result page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search by img button in search result page
        xpath = Util.GetXpath({"locate":"search_by_img_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Click search by img button in search result page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSearchResultPageButton.ClickSearchByImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRedirectToShop(arg):
        '''
        ClickRedirectToShop : Click redirect to shop in search result page
                Input argu :
                    shop_name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Click redirect to shop button
        xpath = Util.GetXpath({"locate":"redirect_btn"})
        xpath = xpath.replace("shop_name_to_replaced", shop_name)
        if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"3", "result": "1"}):
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click redirect to shop -> " + shop_name, "result": "1"})
        else:
            ret = 0
            dumplogger.info("Cannot find shop result !!!")

        OK(ret, int(arg['result']), 'AndroidSearchResultPageButton.ClickRedirectToShop')
