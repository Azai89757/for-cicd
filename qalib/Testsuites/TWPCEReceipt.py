import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCEReceipt(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup EReceipt Precondition =="
        dumplogger.info("== Setup TWPCEReceipt Precondition ==")

    def test_TWPCEReceipt01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCEReceipt-01.xml")

    def test_TWPCEReceipt02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCEReceipt-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCEReceipt Post condition =="
        dumplogger.info("== Setup TWPCEReceipt Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
