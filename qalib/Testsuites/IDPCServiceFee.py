import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCServiceFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCServiceFee Precondition =="
        dumplogger.info("== Setup IDPCServiceFee Precondition ==")

    def test_IDPCServiceFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCServiceFee-01.xml")

    def test_IDPCServiceFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCServiceFee-02.xml")

    def test_IDPCServiceFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCServiceFee-03.xml")

    def test_IDPCServiceFee04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCServiceFee-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCServiceFee Post condition =="
        dumplogger.info("== Setup IDPCServiceFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
