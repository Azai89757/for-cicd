import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCAddOnDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCAddOnDeal Precondition =="
        dumplogger.info("== Setup CLPCAddOnDeal Precondition ==")

    def test_CLPCAddOnDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCAddOnDeal-01.xml")

    def test_CLPCAddOnDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCAddOnDeal-02.xml")

    def test_CLPCAddOnDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCAddOnDeal-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCAddOnDeal Post condition =="
        dumplogger.info("== Setup CLPCAddOnDeal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
