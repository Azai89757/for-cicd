import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpProductCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductCollection Precondition =="
        dumplogger.info("== Setup TWHttpProductCollection Precondition ==")

    def test_TWHttpProductCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductCollection-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductCollection Post condition =="
        dumplogger.info("== Setup TWHttpProductCollection Post condition ==")
