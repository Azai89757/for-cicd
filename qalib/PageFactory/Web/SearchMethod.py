#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SearchMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def ClickSearchBar(arg):
    '''
    ClickSearchBar : Click search bar
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Click search bar
    xpath = Util.GetXpath({"locate":"search_bar"})
    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click search bar", "result": "1"})

    OK(ret, int(arg['result']), 'ClickSearchBar')

@DecoratorHelper.FuncRecorder
def ClickSearchIcon(arg):
    '''
    ClickSearchIcon : Click search icon
            Input argu :N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Click search icon
    xpath = Util.GetXpath({"locate":"search_icon"})
    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click search icon", "result": "1"})

    OK(ret, int(arg['result']), 'ClickSearchIcon')

@DecoratorHelper.FuncRecorder
def InputKeywordAndClickSearch(arg):
    '''
    InputKeywordAndClickSearch : input keyword to search product
            Input argu :
                search_keyword - input key words to search product
                need_click - fill 1 for click search button to search item, fill 0 will go to pre-search page.
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    search_keyword = arg["search_keyword"]
    need_click = arg['need_click']

    search_bar = Util.GetXpath({"locate":"search_bar"})
    search_button = Util.GetXpath({"locate":"search_button"})

    ##Enter keyword in search bar and go to search product page
    BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":search_bar, "result": "1"})
    BaseUICore.Input({"locate":search_bar, "string":search_keyword, "method":"xpath", "result": "1"})

    if int(need_click):
        BaseUICore.Click({"method":"xpath", "locate":search_button, "message":"click search button", "result": "1"})

    OK(ret, int(arg['result']), 'InputKeywordAndClickSearch')

class FilterPanelPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceRange(arg):
        '''
        InputPriceRange : input the lowest price and the highest price on the filter panel
                Input argu :
                    lowest_price - the lowest price
                    highest_price - the highest price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        lowest_price = arg["lowest_price"]
        highest_price = arg["highest_price"]

        lowest_price_box = Util.GetXpath({"locate":"lowest_price_box"})
        highst_price_box = Util.GetXpath({"locate":"highest_price_box"})
        apply_button = Util.GetXpath({"locate":"apply_button"})

        ##Enter the price range on the filter panel and click apply button
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":lowest_price_box, "result": "1"})
        BaseUICore.Input({"locate":lowest_price_box, "string":lowest_price, "method":"xpath", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":highst_price_box, "result": "1"})
        BaseUICore.Input({"locate":highst_price_box, "string":highest_price, "method":"xpath", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":apply_button, "message":"click apply button", "result": "1"})

        OK(ret, int(arg['result']), 'FilterPanelPage.InputPriceRange => Between ' + lowest_price + ' and ' + highest_price)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToFilterPanelByName(arg):
        '''
        MoveToFilterPanelByName : Move web scope to filter panel
                Input argu :
                    filter_name - filter panel text location
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        filter_name = arg["filter_name"]
        ret = 1

        ##Locate filter name and move to the location
        locate = Util.GetXpath({"locate":filter_name})

        ##If xpath exists
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":locate, "reservetype":"filter_panel", "result": "1"})
            BaseUILogic.MouseScrollEvent({"x":str(GlobalAdapter.GeneralE2EVar._LocationX_), "y":str(GlobalAdapter.GeneralE2EVar._LocationY_ - 500), "result": "1"})
        else:
            dumplogger.info("Unable to get product name xpath.")
            ret = 0

        OK(ret, int(arg['result']), 'FilterPanelPage.MoveToFilterPanelByName -->' + filter_name)


class FilterPanelPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreSaleLocation(arg):
        '''
        ClickSeeMoreSaleLocation : Click see more sale location in Filter Panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see more sale location
        xpath = Util.GetXpath({"locate":"see_more_location"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see more sale location", "result": "1"})

        OK(ret, int(arg['result']), 'FilterPanelPageButton.ClickSeeMoreSaleLocation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOtherSaleLocation(arg):
        '''
        ClickOtherSaleLocation : Click other sale location in Filter Panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click other location
        xpath = Util.GetXpath({"locate":"other_sale_location"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click other sale location", "result": "1"})

        OK(ret, int(arg['result']), 'FilterPanelPageButton.ClickOtherSaleLocation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCleanAll(arg):
        '''
        ClickCleanAll : Click Clean All in Filter Panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Clean All
        xpath = Util.GetXpath({"locate":"filter_clear_all_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Clean All", "result": "1"})

        OK(ret, int(arg['result']), 'FilterPanelPageButton.ClickCleanAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopType(arg):
        '''
        ClickShopType : Click shop type in Filter Panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_type = arg["shop_type"]

        ##Click shop type
        xpath = Util.GetXpath({"locate":"shop_type"})
        xpath = xpath.replace("shop_type_to_be_replaced", shop_type)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click shop type", "result": "1"})

        OK(ret, int(arg['result']), 'FilterPanelPageButton.ClickShopType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStatus(arg):
        '''
        ClickStatus : Click status in filter panel
                Input argu :
                    status - product status is new or used
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click status in filter panel
        xpath = Util.GetXpath({"locate":"status"})
        xpath = xpath.replace("status_to_be_replaced", status)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product status", "result": "1"})

        OK(ret, int(arg['result']), 'FilterPanelPageButton.ClickStatus')


class SearchHistoryPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickKeyWord(arg):
        '''
        ClickKeyWord : Click Key Word In Search History to search result page
                Input argu :
                    key_word - search key word in search history
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        key_word = arg["key_word"]

        ##Click search key word
        xpath = Util.GetXpath({"locate":"search_history_xpath"})
        xpath = xpath.replace("search_word_to_be_replace", key_word)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click KeyWord In Search History -->" + key_word, "result": "1"})

        OK(ret, int(arg['result']), 'SearchHistoryPage.ClickKeyWord')


class SearchResultPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToSortProduct(arg):
        '''
        ClickToSortProduct : Click sort button
                Input argu :
                    sort_type - label_relevance / label_latest / label_top_sales / label_price_lth / label_price_htl
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sort_type = arg["sort_type"]

        ##Click sort button
        xpath = Util.GetXpath({"locate":sort_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click sort button", "result": "1"})
        ##Click sort label to avoid hover mouse on product card
        xpath = Util.GetXpath({"locate":"label_sort_by"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click sort label", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPage.ClickToSortProduct with => ' + sort_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HoverOnSortPrice(arg):
        '''
        HoverOnSortPrice : Hover on sort price
                Input argu :
                    sort_price_type - label_price / label_price_lth / label_price_htl
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sort_price_type = arg["sort_price_type"]

        ##Hover mouse on price drop down element
        xpath = Util.GetXpath({"locate":sort_price_type})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPage.HoverOnSortPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MouseHoverProductCard(arg):
        '''
        MouseHoverProductCard : Hover a product card
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Hover a product card
        xpath = Util.GetXpath({"locate":"product_name"})
        xpath = xpath.replace("replace_product_name", product_name)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result":"1"})

        OK(ret, int(arg['result']), 'SearchResultPage.MouseHoverProductCard: ' + product_name)


class SearchResultPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollow(arg):
        '''
        ClickFollow :Click follow button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click follow button
        xpath = Util.GetXpath({"locate":"click_follow"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click follow button", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickFollow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTargetCategoryGrid(arg):
        '''
        ClickTargetCategoryGrid : Click category in search result page
                Input argu :
                    product_name : product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_grid_name = arg["category_grid_name"]

        ##Click category in search result page
        xpath = Util.GetXpath({"locate":"click_target_category"})
        xpath_target_product = xpath.replace("category_name_to_be_replace", category_grid_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_target_product, "message":"Click category in search result page", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickTargetCategoryGrid')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryInFilterPanel(arg):
        '''
        ClickCategoryInFilterPanel : Click category icon in filter panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Category icon
        xpath = Util.GetXpath({"locate":"click_category_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click category icon in filter panel", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickCategoryInFilterPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewShop(arg):
        '''
        ClickViewShop : Click view shop button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view shop button
        xpath = Util.GetXpath({"locate":"label_view_shop"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click view shop button", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickViewShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMiniPageController(arg):
        '''
        ClickMiniPageController : Click the prev/next to switch page in the sorting filter
                Input argu :
                    direction - prev/next to switch page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        direction = arg["direction"]

        ##Click direction
        xpath = Util.GetXpath({"locate":"controller_" + direction + "_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Mini Page Controller -->" + direction, "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickMiniPageController')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDigitalNumber(arg):
        '''
        ClickDigitalNumber : Click the digital number to switch page
                Input argu :
                    digital_number - digital number in search result page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        digital_number = arg["digital_number"]

        ##Click digital number
        xpath = Util.GetXpath({"locate":"digital_number_xpath"})
        xpath = xpath.replace("digital_number_to_be_replace", digital_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Digital Number -->" + digital_number, "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickDigitalNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRatingPageController(arg):
        '''
        ClickRatingPageController : Click the left/right to switch page beside the sorted panel
                Input argu :
                    direction - left/right to switch page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        direction = arg["direction"]

        ##Click direction
        xpath = Util.GetXpath({"locate":"controller_" + direction + "_arrow"})

        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ClickMiniPageController -->" + direction, "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickRatingPageController')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTargetProduct(arg):
        '''
        ClickTargetProduct : Click product in search result page
                Input argu :
                    product_name : product name display in search result page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click search product
        xpath = Util.GetXpath({"locate":"search_result_product"})
        xpath_target_product = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_target_product, "message":"Click product card in search result page", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickTargetProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserShop(arg):
        '''
        ClickUserShop : Click user shop
                Input argu :
                    user_name - user name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_name = arg["user_name"]

        ##Click user shop
        xpath = Util.GetXpath({"locate":"search_result_user"})
        xpath = xpath.replace("user_name_to_be_replace", user_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click user shop", "result": "1"})

        OK(ret, int(arg['result']), 'SearchResultPageButton.ClickUserShop')


class SearchBarButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelector(arg):
        '''
        ClickSelector : Click drop down menu search bar
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Selector
        xpath = Util.GetXpath({"locate":"search_bar_selector"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Selector", "result": "1"})

        OK(ret, int(arg['result']), 'SearchBarButton.ClickSelector')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectorByName(arg):
        '''
        ClickSelectorByName : Click Selector which display in search bar
                Input argu :
                    selector_name - selector name display in search bar
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        selector_name = arg["selector_name"]

        ##Click Selector by name
        xpath = Util.GetXpath({"locate":"selector_xpath"})
        xpath = xpath.replace("selector_name_to_be_replace", selector_name)
        search_bar_selector = Util.GetXpath({"locate":"search_bar_selector"})
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":search_bar_selector, "locatehidden":xpath, "message":"Click btn in search drop down list", "result": "1"})

        OK(ret, int(arg['result']), 'SearchBarButton.ClickSelectorByName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchShopIcon(arg):
        '''
        ClickSearchShopIcon : Click Search Shop Icon which display in Search History
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Search Shop Icon
        xpath = Util.GetXpath({"locate":"click_search_shop_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Search Shop Icon which display in Search History", "result": "1"})

        OK(ret, int(arg['result']), 'SearchBarButton.ClickSearchShopIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchPrefillIcon(arg):
        '''
        ClickSearchPrefillIcon : Click Search Prefill Icon which display in Search History
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Search Prefill Icon
        xpath = Util.GetXpath({"locate":"click_search_prefill"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Search Prefill Icon which display in Search History", "result": "1"})

        OK(ret, int(arg['result']), 'SearchBarButton.ClickSearchPrefillIcon')
