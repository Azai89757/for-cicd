import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCShopCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCShopCollection Precondition =="
        dumplogger.info("== Setup PHPCShopCollection Precondition ==")

    def test_PHPCShopCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopCollection-01.xml")

    def test_PHPCShopCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopCollection-02.xml")

    def test_PHPCShopCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCShopCollection-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCShopCollection Post condition =="
        dumplogger.info("== Setup PHPCShopCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
