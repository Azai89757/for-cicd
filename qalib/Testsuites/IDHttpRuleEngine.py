import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpRuleEngine(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpRuleEngine Precondition =="
        dumplogger.info("== Setup IDHttpRuleEngine Precondition ==")

    def test_IDHttpRuleEngine01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-01.xml")

    def test_IDHttpRuleEngine02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-02.xml")

    def test_IDHttpRuleEngine03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-03.xml")

    def test_IDHttpRuleEngine04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-04.xml")

    def test_IDHttpRuleEngine05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-05.xml")

    def test_IDHttpRuleEngine06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-06.xml")

    def test_IDHttpRuleEngine07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-07.xml")

    def test_IDHttpRuleEngine08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-08.xml")

    def test_IDHttpRuleEngine09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-09.xml")

    def test_IDHttpRuleEngine10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-10.xml")

    def test_IDHttpRuleEngine11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpRuleEngine-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpRuleEngine Post condition =="
        dumplogger.info("== Setup IDHttpRuleEngine Post condition ==")
