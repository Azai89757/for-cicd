#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCMTMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time

##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def LaunchCMTAdmin(arg):
    ''' LaunchCMTAdmin : go to CMT admin page
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    ret = 1
    cmt_admin_cookie = {}
    db_file = "cmt_cookie"

    ##Get current env and country
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
    GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

    if 'cmt_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        cmt_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        cmt_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        cmt_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['cmt_admin'] = [cmt_admin_cookie]

    ##Go to url
    url = "https://mkt-admin." + GlobalAdapter.UrlVar._Domain_
    BaseUICore.GotoURL({"url":url, "result": "1"})
    dumplogger.info("url = %s" % (url))
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"cmt_admin", "result": "1"})
    time.sleep(5)

    ##Go to CMT Admin
    url = "https://mkt-admin." + GlobalAdapter.UrlVar._Domain_ + "/mkt-portal"
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchCMTAdmin')

class AdminCMTComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnTab(arg):
        '''
        ClickOnTab : Click any tab on left panel in CMT admin page
                Input argu :
                    maintab_type - cmt / voucher / flash_sale
                    subtab_type - type of subtab which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        maintab_type = arg["maintab_type"]
        subtab_type = arg["subtab_type"]

        ##Click a main tab at the left panel
        xpath = Util.GetXpath({"locate": maintab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main tab: %s, which xpath is %s" % (maintab_type, xpath), "result": "1"})
        time.sleep(5)

        ##Click a sub tab at the left panel
        xpath = Util.GetXpath({"locate": subtab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab: %s, which xpath is %s" % (subtab_type, xpath), "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCMTComponent.ClickOnTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
                replace_text - type of argument which you want to replace
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        replace_text = arg["replace_text"]

        xpath = Util.GetXpath({"locate": button_type})
        if replace_text:
            xpath = xpath.replace("replaced_text", replace_text)

        ##Click on button
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCMTComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column
                input_content - input value
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCMTComponent.InputToColumn')

class AdminTimeSlotPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTimeSlot(arg):
        '''
        InputTimeSlot : Input start time or end time
            Input argu :
                time_type - start_time / end_time
                value - input time value
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        time_type = arg["time_type"]
        value = arg["value"]

        ##Click time column first to prepare to input value
        xpath = Util.GetXpath({"locate": time_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click time column", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(3)

        ##Input time value
        time_value = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(value), 0, is_tw_time=1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_value, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminTimeSlotPage.InputTimeSlot')

class AdminClusterPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCSVfile(arg):
        '''
        UploadCSVfile : Upload csv file to add flash sale item
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        ret = 1

        ##Upload file
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminClusterPage.UploadCSVfile')

class AdminReviewProductPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemCheckBox(arg):
        '''
        SelectItemCheckBox : Select item checkbox
                Input argu :
                    item_name - select item name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        item_name = arg["item_name"]
        ret = 1

        ##select item checkbox
        xpath = Util.GetXpath({"locate": "select_checkbox"})
        xpath = xpath.replace("replaced_text", item_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "select item check box", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReviewProductPage.SelectItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectRejectReason(arg):
        '''
        SelectRejectReason : Select reject item reason
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click template
        xpath = Util.GetXpath({"locate": "template"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click template", "result": "1"})

        ##Select template list
        xpath = Util.GetXpath({"locate": "template_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "select template list", "result": "1"})

        ##Click ok button
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReviewProductPage.SelectRejectReason')
