import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCancellation Precondition =="
        dumplogger.info("== Setup COPCCancellation Precondition ==")

    def test_COPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCancellation-01.xml")

    def test_COPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCancellation-02.xml")

    def test_COPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCancellation-03.xml")

    def test_COPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCancellation-04.xml")

    def test_COPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCancellation Post condition =="
        dumplogger.info("== Setup COPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
