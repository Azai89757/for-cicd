import unittest
from Config import dumplogger
from FrameWorkBase import *


class THAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIShipment Precondition =="
        dumplogger.info("== Setup THAPIShipment Precondition ==")

    def test_THAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/THAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIShipment Post condition =="
        dumplogger.info("== Setup THAPIShipment Post condition ==")
