import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCLogOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCLogOut Precondition =="
        dumplogger.info("== Setup IDPCLogOut Precondition ==")

    def test_IDPCLogOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCLogOut-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCLogOut Post condition =="
        dumplogger.info("== Setup IDPCLogOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
