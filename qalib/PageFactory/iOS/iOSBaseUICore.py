#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
iOSBaseUICore.py: The def of this file called by XML mainly.
'''


##Import system library
import os
import time
import shutil
import re
import traceback

##Import framework common library
import FrameWorkBase
import Util
import GlobalAdapter
import DecoratorHelper
import XtFunc
import Parser
import Config
from Config import dumplogger
from Config import _GeneralWaitingPeriod_, _AverageWaitingPeriod_, _ShortWaitingPeriod_, _InstantWaitingPeriod_

##import iOSDriver
from iOSDriver import iOSController
from iOSDriver import iOSLocator

##Import selenium related module
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

_iOSDriver_ = None

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def GetiOSDeviceInfo():
    ''' GetiOSDeviceInfo : Get iOS device name/platform version
            Input argu :
                N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''
    dumplogger.info("Enter GetiOSDeviceInfo")

    regex = r"(TWQA-Auto)(\s+\S+\s+)(\[)(\S+)(\])"
    cmd = "instruments -s devices"
    dumplogger.info("cmd = %s" % (cmd))

    ##Get cmd result
    output = os.popen(cmd).read()
    dumplogger.info("output = " + output.strip())

    ##Filter string and search for version number
    match = re.search(regex, output, re.MULTILINE)
    if match:
        final_output = match.group(2)
        dumplogger.info("Device version = %s", final_output)
    else:
        print "Device info not found.."
        dumplogger.info("Device version not found..")
        final_output = "not found"

    return final_output

def GetiOSDeviceId():
    ''' GetiOSDeviceId : Get iOS device id
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    dumplogger.info("Enter GetiOSDeviceId")

    regex = r"TWQA-Auto%s\s\S+\s\[(\S+)\]\s\S(Simulator)?" % (Config._DeviceNumber_)
    cmd = 'instruments -s devices'

    ##Get cmd result
    output = os.popen(cmd).read()
    dumplogger.info("output = " + output)

    ##Filter device list of testing machine only
    match = re.search(regex, output, re.MULTILINE)
    if match:
        dumplogger.info("deviceid = match.group(1) = " + match.group(1))
        if match.group(2):
            dumplogger.info("Using iOS Simulator")
            return match.group(1), "'iOS Simulator'"
        else:
            dumplogger.info("Using iOS real device")
            return match.group(1), "iOS"
    else:
        dumplogger.error("No matched device found")
        return None, None

@DecoratorHelper.FuncRecorder
def iOSBuildWDAToDevice(arg):
    ''' iOSBuildWDAToDevice : Build wda to target device
            Input argu :
                uuid - device uid
                device_type - device type of ios
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''
    uuid = arg['uuid']
    device_type = arg['device_type']

    ##Setting log file path
    try:
        xcode_log_file = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + "xcodebuild_log.log"

        ##Compiling cmd
        xcode_cmd = "xcodebuild >%s 2>%s build test -allowProvisioningUpdates -project %s/Desktop/WebDriverAgent/WebDriverAgent.xcodeproj -scheme WebDriverAgentRunner -destination platform=%s,id=%s &" % (xcode_log_file, xcode_log_file, os.path.expanduser("~"), device_type, uuid)
        #cmd = "osascript -e 'tell application \"terminal\" to do script \"%s\"'" % (xcode_cmd)

        ##Launch new command prompt for appium
        dumplogger.info("xcodebuild cmd: %s" % (xcode_cmd))
        os.system(xcode_cmd)

        ##Search running target device ip in xcodebuild log
        time.sleep(30)
        regex = r"(ServerURLHere->)(...+)(<-ServerURLHere)"
        with open(xcode_log_file, 'rb') as xcodebuild_log:
            for line in xcodebuild_log.readlines():
                match = re.search(regex, line)
                if match:
                    print "device_ip:", match.group(2)
                    #Config._MobileDeviceInfo_[uuid]["device_ip"] = match.group(2)

        ##Get process ID and save it
        xcode_cmd = xcode_cmd.replace("'", "")
        GlobalAdapter.CommonVar._ProcessID_["ios"] = FrameWorkBase.PNameQueryProcess(command_line=xcode_cmd)
        dumplogger.info("pid: %s" % (GlobalAdapter.CommonVar._ProcessID_["ios"]))
    except:
        print traceback.format_exc()
        print "Failed to build wda to device"
        dumplogger.exception("Failed to build wda to device")

    ##If not get process id, return error
    if GlobalAdapter.CommonVar._ProcessID_["ios"] == -1:
        print "Get xcodebuild process ID Failed!!!!!!!!"
        dumplogger.info("Get xcodebuild process ID Failed!!!!!!!!")

    ##Wait for xcode to build
    time.sleep(5)

@DecoratorHelper.FuncRecorder
def iOSKillXcodeBuild(arg):
    ''' iOSKillXcodeBuild : Kill xcodebuild process
            Input argu : isFail : To know whether it's intent by failure case or not.
                                 1 - default (Case not failed)
                                 0 - case failed
            Return code : N/A
            Note : None
    '''
    isFail = arg["isFail"]

    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Create log folder if not exist
    if os.path.exists(output_casename_folder):
        dumplogger.info(output_casename_folder + " : => path exist")
    else:
        ##Create folder through command
        command = "mkdir -p " + output_casename_folder
        os.system(command)
        dumplogger.info("command = " + command)

    xcodebuild_log_name_before = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + "xcodebuild_log.log"

    ##Kill process by process ID
    FrameWorkBase.KillProcessByPID(GlobalAdapter.CommonVar._ProcessID_["ios"])

    ##Rename xcodebuild log name if failed
    if isFail == '0':
        testcase_folder = output_casename_folder
        shutil.move(xcodebuild_log_name_before, testcase_folder)
        dumplogger.info("All cases are success.")

    else:
        ##Rename xcodebuild file to contain case id
        xcodebuild_log_name_after = output_casename_folder + Parser._CASE_['id'] + "_xcodebuild_log.log"

        try:
            ##Copy file into fail cases folder
            shutil.move(xcodebuild_log_name_before, xcodebuild_log_name_after)
            dumplogger.info("Rename xcodebuild log name->" + xcodebuild_log_name_after)
        except:
            dumplogger.exception("Move log file failed. Please check permission")
            print "Move log file failed. Please check permission"

    ##Reset CommonVar parameter
    GlobalAdapter.CommonVar._ProcessID_["ios"] = ""
    dumplogger.info('Clear GlobalAdapter.CommonVar._ProcessID_["ios"] list')

def iOSInitialDriver():
    ''' iOSInitialDriver : Initial iOS app driver
            Input argu : N/A
            Return code : N/A
            Note : None
    '''
    ##declare app bundle Id
    #if Config._MobileDeviceInfo_[Config._DeviceID_]["device_name"] == "'iOS Simulator'":
    #    bundle_id = "com.beeasy.shopee.sg"
    #else:
    #    bundle_id = 'com.beeasy.shopee.' + Config._TestCaseRegion_.lower() + '.enterprise'
    #Config._iOSDesiredCaps_["capabilities"]["alwaysMatch"]["bundleId"] = bundle_id

    try:
        driver = iOSController(Config._MobileDeviceInfo_[Config._DeviceID_]["device_ip"], Config._iOSDesiredCaps_)

        ##Set implicitly timeout
        driver.implicitly_wait(60)

        global _iOSDriver_
        _iOSDriver_ = driver

        ##Handle update message
        #iOSClickCancelOnUpdate({})

        ##Handle pop up banner
        iOSHandlePopupBanner({})

        ##Prevent app from crashing, bc we do too fast
        time.sleep(60)

        iOSSwitchBranch({"branch":"master-automation"})
    except KeyError:
        dumplogger.exception("iOSDriver cannot read device_ip in device info")
        print "iOSDriver cannot read device_ip in device info"

    except:
        dumplogger.exception("Encounter unknown error.")
        print "Encounter unknown error"

@DecoratorHelper.FuncRecorder
def iOSSwitchCountry(arg):
    ''' iOSSwitchCountry : Switch country in Forbidden Zone
            Input argu :
                country - vn, sg
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1
    country = arg['country']

    time.sleep(5)
    ##Check me icon is vn
    locate = Util.GetXpath({"locate":"me_icon"})
    is_exist = iOSCheckElementExist({"method":"id", "locate":locate, "passok": "0", "result": "1"})

    if not is_exist:
        ##Long press home icon
        locate = Util.GetXpath({"locate":"home"})
        iOSLongPress({"method":"xpath", "locate":locate, "sec":"5", "result": "1"})
        time.sleep(2)

        ##Click country option
        locate = Util.GetXpath({"locate":"country"})
        iOSClick({"method":"xpath", "locate":locate, "message":"Click country", "result": "1"})
        time.sleep(2)

        ##Select country
        locate = Util.GetXpath({"locate":country})
        iOSClick({"method":"id", "locate":locate, "message":"Click " + country, "result": "1"})

        ##Re-open Shopee APP to reload navigation bar
        iOSReopenApp({})

    else:
        dumplogger.info("Already in country: %s" % (country))

    OK(ret, int(arg['result']), 'iOSSwitchCountry')

@DecoratorHelper.FuncRecorder
def iOSSwitchBranch(arg):
    ''' iOSSwitchBranch : Switch branch in Forbidden Zone
            Input argu :
                branch - branch name
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    print "Enter iOSSwitchBranch"
    branch = arg['branch']

    ##Long press home icon
    iOSLongPressCoordinates({"x_cor":"40", "y_cor":"745", "sec":"5", "result":"1"})
    #locate = Util.GetXpath({"locate":"home"})
    #iOSLongPress({"method":"xpath", "locate":locate, "sec":"5", "result": "1"})
    time.sleep(5)

    ##Check is on branch now
    is_exist = iOSCheckElementExist({"method":"id", "locate":branch, "passok": "0", "result": "1"})

    if not is_exist:
        ##Click branch option
        locate = Util.GetXpath({"locate":"choose_branch"})
        iOSClick({"method":"xpath", "locate":locate, "message":"Click branch", "result": "1"})
        time.sleep(2)

        ##Input to search branch
        locate = Util.GetXpath({"locate":"search_bar"})
        iOSInput({"method":"xpath", "locate":locate, "string":branch, "result": "1"})
        time.sleep(2)

        ##Select branch
        locate = Util.GetXpath({"locate":"branch"})
        locate = locate.replace("branch_to_be_replace", branch)
        iOSClick({"method":"xpath", "locate":locate, "message":"Click branch:" + branch, "result": "1"})
        time.sleep(2)
        iOSClick({"method":"xpath", "locate":locate, "message":"Click branch:" + branch, "result": "1"})
        time.sleep(20)

        ##Re-open Shopee APP to reload navigation bar
        iOSCloseCurrentApp({})
        iOSOpenCurrentApp({})
        time.sleep(10)

        ##Handle update message and pop up
        iOSHandlePopupBanner({})
    else:
        dumplogger.info("Already in branch: %s" % (branch))

        ##Reopen app
        iOSReopenApp({})

@DecoratorHelper.FuncRecorder
def iOSDeInitialDriver(arg):
    ''' iOSDeInitialDriver : deinitial iOS driver
            Input argu : N/A
            Return code : N/A
            Note : None
    '''
    global _iOSDriver_

    try:
        _iOSDriver_.quit()
    except AttributeError:
        print "Please initial iOS driver first"
        dumplogger.info("Please initial iOS driver first")

    except:
        dumplogger.exception('Got exception error')

@DecoratorHelper.FuncRecorder
def iOSInput(arg):
    ''' Input : Input any strings in iOS app
            Input argu :
                locate - Position in iOS app
                string - Input strings
                                method - xpath, id
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    locate = arg['locate']
    string = arg['string']
    method = arg['method']
    ret = 1

    dumplogger.info("locate = %s" % (locate))
    dumplogger.info("string = %s" % (string))
    dumplogger.info("method = %s" % (method))

    ##Detect path to find element by method
    try:
        if method == "xpath":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).clear()
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).send_keys(string)

        elif method == "id":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((MobileBy.ACCESSIBILITY_ID, (locate)))).clear()
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((MobileBy.ACCESSIBILITY_ID, (locate)))).send_keys(string)

        elif method == "xpath_noclear":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).send_keys(string)

        elif method == "element":
            locate.send_keys(string)

        else:
            dumplogger.info("Please check method!!!")
            ret = 0
    except TimeoutException:
        dumplogger.exception('Cannot find element in 30 seconds')
        ret = -1

    OK(ret, int(arg['result']), 'iOSInput')

@DecoratorHelper.FuncRecorder
def iOSClick(arg):
    ''' iOSClick : Click any content in iOS
            Input argu :
                locate - Position in iOS
                string - Input strings
                message - output message
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    locate = arg['locate']
    method = arg['method']
    message = arg['message']
    ret = 1

    try:
        if method == "xpath":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).click()

        elif method == "id":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((MobileBy.ACCESSIBILITY_ID, (locate)))).click()

        elif method == "link_text":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.LINK_TEXT, (locate)))).click()

        elif method == "predicate":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((iOSLocator.PREDICATE, (locate)))).click()

        else:
            dumplogger.info("Please check method!!!")
            ret = 0

    except TimeoutException:
        dumplogger.exception('Cannot find element in 30 seconds')
        ret = 0

    OK(ret, int(arg['result']), 'iOSClick->' + message)

@DecoratorHelper.FuncRecorder
def iOSKeyboardAction(arg):
    ''' iOSKeyboardAction : Determine kinds of keyboard action used on iOS.
        Input argu :
            actiontype - which action on browser,  copy/cut/paste/delete/enter/$value_input$
            key - keys to press
        Return code : 1 - success
                      0 - fail
                     -1 - error
    '''
    actiontype = arg["actiontype"]
    key = arg["key"]
    ret = 1

    ##Click keys on keyboard
    locate = Util.GetXpath({"locate": actiontype})
    locate = locate.replace("key_to_be_replaced", unicode(key, 'utf-8'))
    iOSClick({"method":"xpath", "locate":locate, "message":"Click %s on keyboard", "result":"1"})

    OK(ret, int(arg['result']), 'iOSKeyboardAction->' + actiontype)

@DecoratorHelper.FuncRecorder
def iOSLongPressCoordinates(arg):
    ''' iOSLongPressCoordinates : Long press any position on iOS
            Input argu :
                x_cor - X coordinate
                y_cor - Y coordinate
                sec - seconds to press
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None  '''

    x_cor = arg['x_cor']
    y_cor = arg['y_cor']
    sec = arg['sec']
    ret = 1

    _iOSDriver_.long_press(x_cor, y_cor, sec)

    OK(ret, int(arg['result']), 'iOSLongPressCoordinates')

@DecoratorHelper.FuncRecorder
def iOSClickCoordinates(arg):
    ''' iOSClick : Click any content in iOS
            Input argu :
                x_cor - X coordinate
                y_cor - Y coordinate
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None  '''

    x_cor = arg['x_cor']
    y_cor = arg['y_cor']
    ret = 1

    _iOSDriver_.click_coordinate(x_cor, y_cor)

    OK(ret, int(arg['result']), 'iOSClickCoordinates')

@DecoratorHelper.FuncRecorder
def iOSGetElements(arg):
    ''' iOSGetElements : Get any elements in iOS
            Input argu :
                locate - Position in iOS
                method - xpath, css, id
                mode - single, multi
                message - output message
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    GlobalAdapter.CommonVar._PageElements_ = ""
    mode = None
    locate = arg["locate"]
    method = arg["method"]
    message = ""
    ret = 1

    try:
        mode = arg["mode"]
    except KeyError:
        mode = "single"
        dumplogger.info('KeyError - mode default is 0')
    except:
        mode = "single"
        dumplogger.info('Other error - mode default is 0')
    finally:
        dumplogger.info("mode = %s" % (mode))

    ##Get iOS elements by method
    try:
        if mode == "single":
            if method == "xpath":
                _iOSDriver_.implicitly_wait(30)
                GlobalAdapter.CommonVar._PageElements_ = _iOSDriver_.find_element_by_xpath(locate)

            elif method == "id":
                _iOSDriver_.implicitly_wait(30)
                GlobalAdapter.CommonVar._PageElements_ = _iOSDriver_.find_element_by_accessibility_id(locate)

        elif mode == "multi":
            if method == "xpath":
                _iOSDriver_.implicitly_wait(30)
                GlobalAdapter.CommonVar._PageElements_ = _iOSDriver_.find_elements_by_xpath(locate)
            elif method == "id":
                _iOSDriver_.implicitly_wait(30)
                GlobalAdapter.CommonVar._PageElements_ = _iOSDriver_.find_elements_by_accessibility_id(locate)

    except IndexError:
        message = "Index error"
        dumplogger.error("Index error")
        ret = 0

    except NoSuchElementException:
        message = "Get elements failed"
        dumplogger.exception("Get elements failed")
        ret = -1

    OK(ret, int(arg['result']), 'iOSGetElements->' + message)

@DecoratorHelper.FuncRecorder
def iOSParseElements(arg):
    ''' iOSParseElements : Parse any elements in iOS
            Input argu :
             comparison : 1 - fuzzy Comparison
                          0 - fully Comparison
                Keyword - the keyword want to check
                message - output message
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    dumplogger.info("Enter iOSParseElements")
    message = ""
    isfuzzy = arg["isfuzzy"]
    ret = 0

    if GlobalAdapter.CommonVar._PageAttributes_ is None:
        dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = None")
        message = "None"
        ret = 0

    elif "\n" in GlobalAdapter.CommonVar._PageAttributes_:
        dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = " + GlobalAdapter.CommonVar._PageAttributes_)

        if arg['keyword'] in GlobalAdapter.CommonVar._PageAttributes_:
            message = "In -> Success"
            ret = 1

        else:
            ret = 0
    else:
        ##Check string is match
        try:
            dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = " + GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info("keyword = " + arg['keyword'])

            '''if arg['keyword'] == _PageElements_.strip():
                message = "Equal -> Success"
                ret = 1
            else:
                ret = 0'''

            if arg['isfuzzy'] == "1":
                if arg['keyword'] in GlobalAdapter.CommonVar._PageAttributes_:
                    message = "In -> Success"
                    ret = 1
                else:
                    ret = 0
            elif arg['isfuzzy'] == "0":
                if arg['keyword'] == GlobalAdapter.CommonVar._PageAttributes_.strip():
                    message = "Equal -> Success"
                    ret = 1
                else:
                    ret = 0
            else:
                ret = 0
        except:
            dumplogger.exception("Please check error message.")
            ret = -1

    OK(ret, int(arg['result']), 'iOSParseElements->' + message)

@DecoratorHelper.FuncRecorder
def iOSRelativeSwipe(arg):
    ''' iOSRelativeSwipe : Swipe the page by corridinate
            Input argu :
                locate_x - initial cooridinate of x-axis
                locate_y - initial cooridinate of y-axis
                movex - degree of movement of x-axis
                movey - degree of movement of y-axis

            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    locate_x = arg["locate_x"]
    locate_y = arg["locate_y"]
    movex = arg["movex"]
    movey = arg["movey"]
    ret = 1

    try:
        _iOSDriver_.swipe(locate_x, locate_y, movex, movey, 0)
    except:
        dumplogger.exception("Please check error message.")
        ret = -1
    OK(ret, int(arg['result']), 'iOSRelativeSwipe')

@DecoratorHelper.FuncRecorder
def iOSGetAttributes(arg):
    ''' iOSGetAttributes : Get element attribute from _PageElements_
            Input argu :
                attrtype - text, location, count, get_attribute
                attribute - attribute for get_attribute
                mode - single, multi
                message - output message
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    GlobalAdapter.CommonVar._PageAttributes_ = ""
    GlobalAdapter.CommonVar._PageAttributesList_ = []
    attrtype = arg["attrtype"]
    message = ""
    ret = 1

    try:
        attribute = arg["attribute"]
    except KeyError:
        attribute = ""
        dumplogger.info('KeyError - attribute default is 0')
    except:
        attribute = ""
        dumplogger.info('Other error - attribute default is 0')
    finally:
        dumplogger.info("attribute = %s" % (attribute))

    try:
        mode = arg["mode"]
    except KeyError:
        mode = "single"
        dumplogger.info('KeyError - mode default is 0')
    except:
        mode = "single"
        dumplogger.info('Other error - mode default is 0')
    finally:
        dumplogger.info("mode = %s" % (mode))

    try:
        if mode == "single":
            if attrtype == 'text':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.text
            elif attrtype == 'location':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.location

            elif attrtype == 'get_attribute':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.get_attribute(attribute)

        elif mode == "multi":
            for tmp in GlobalAdapter.CommonVar._PageElements_:
                if attrtype == 'text':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.text)
                elif attrtype == 'location':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.location)
                elif attrtype == 'get_attribute':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.get_attribute(attribute))
            if attrtype == 'count':
                GlobalAdapter.CommonVar._PageAttributes_ = len(GlobalAdapter.CommonVar._PageElements_)

    except NoSuchElementException:
        message = "Get _PageAttributes_ failed"
        dumplogger.exception(message)
        ret = -1

    finally:
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributesList_)

    OK(ret, int(arg['result']), 'iOSGetAttributes->' + message)

def iOSCheckElementExist(arg):
    ''' Check the element exist or not
            Input argu :
                locate - Position in web
                message - output message
                method - xpath, css, id
                passok - 1 - will pass framework.OK (default)
                       - 0 - not pass framework.OK
                result - 0 - exist
                         1 - Nonexistent
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ##
    ## DO NOT use decoratorHelper on this function since this function will return value
    ##

    dumplogger.info("Enter iOSCheckElementExist")

    method = arg["method"]
    locate = arg["locate"]
    testresult = "Exist"
    ret = 1

    dumplogger.info("Locate method : %s", method)

    try:
        passok = arg["passok"]
    except KeyError:
        passok = "1"
        dumplogger.info('KeyError - passok default is 1')
    except:
        passok = "1"
        dumplogger.info('Other error - passok default is 1')
    finally:
        dumplogger.info("passok = %s" % (passok))

    try:
        if method == "xpath":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate))))
        elif method == "id":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((iOSLocator.ACCESSIBILITY_ID, (locate))))
        elif method == "link_text":
            WebDriverWait(_iOSDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.LINK_TEXT, (locate))))
    except NoSuchElementException:
        ret = 0
        testresult = "Nonexistent"
    except TimeoutException:
        ret = 0
        testresult = "Nonexistent"

    if passok == "1":
        OK(ret, int(arg['result']), 'iOSCheckElementExist->' + testresult)
    else:
        dumplogger.info("locate : %s => 0 for Nonexist, and 1 for Exist : %s" % (locate, ret))
        return ret

@DecoratorHelper.FuncRecorder
def iOSLongPress(arg):
    ''' iOSLongPress : Long press on an element
            Input argu :
                locate - location of the element
                sec - press for how long
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''
    locate = arg["locate"]
    sec = arg["sec"]
    method = arg["method"]
    ret = 1

    try:
        if method == "xpath":
            _iOSDriver_.find_element_by_xpath(locate).long_press(int(sec))

        elif method == "id":
            _iOSDriver_.find_element_by_accessibility_id(locate).long_press(int(sec))

    except NoSuchElementException:
        dumplogger.exception("Can't locate element after 30 secs!")
        ret = -1

    OK(ret, int(arg['result']), 'iOSLongPress')

@DecoratorHelper.FuncRecorder
def iOSClickCancelOnUpdate(arg):
    ''' iOSClickCancelOnUpdate : Click cancel on update when shopee app is up
            Input argu : N/A
            Return code : N/A
            Note : None
    '''
    print "Enter iOSClickCancelOnUpdate"

    try:
        time.sleep(5)
        ##Check if update popup message exist
        locate = Util.GetXpath({"locate":"new_build_available"})
        WebDriverWait(_iOSDriver_, _ShortWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate))))

        ##Click cancel button
        locate = Util.GetXpath({"locate":"cancel_button"})
        _iOSDriver_.find_element_by_accessibility_id(locate).click()
    except:
        dumplogger.info("Can't locate update pop up... proceed!")

@DecoratorHelper.FuncRecorder
def iOSHandlePopupBanner(arg):
    ''' iOSHandlePopupBanner : Click popup close btn to close popup banner
            Input argu : N/A
            Return code : N/A
            Note : None
    '''

    ##Banner close btn
    locate = Util.GetXpath({"locate":"close_btn"})

    if iOSCheckElementExist({"method":"xpath", "locate":locate, "passok":"0", "result":"1"}):
        ##Add banner pop handling

        iOSClick({"method":"xpath", "locate":locate, "message":"Click popup close btn to close popup banner", "result": "1"})

        ##Click metab coordinate
        iOSClickCoordinates({"x_cor":"340", "y_cor":"750", "result":"1"})

        ##Click home coordinate
        #time.sleep(3)
        #iOSClickCoordinates({"x_cor":"45", "y_cor":"750", "result":"1"})

        ##Click cancel
        #locate = Util.GetXpath({"locate":"cancel_icon"})
        #iOSClick({"method":"xpath", "locate":locate, "message":"Click cancel icon", "result": "1"})

        dumplogger.info("Popup Banner detected, and click anywhere to go on.")
    else:
        dumplogger.info("No Popup Banner detected!")

@DecoratorHelper.FuncRecorder
def iOSSetClipboard(arg):
    ''' iOSSetClipboard : Set clipboard on iOS
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    string = arg["string"]
    ret = 1

    ##Set clipboard
    _iOSDriver_.set_clipboard(string)

    OK(ret, int(arg['result']), 'iOSSetClipboard')

@DecoratorHelper.FuncRecorder
def iOSPasteClipboard(arg):
    ''' iOSPasteClipboard : Paste clipboard context on iOS
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
            Note : None
    '''

    ret = 1

    ##paste clipboard content
    locate = Util.GetXpath({"locate":"paste"})
    iOSClick({"method":"xpath", "locate":locate, "message":"click paste button", "result": "1"})
    time.sleep(3)

    OK(ret, int(arg['result']), 'iOSPasteClipboard')

@DecoratorHelper.FuncRecorder
def iOSDeactivateApp(arg):
    ''' iOSDeactivateApp : Deactivate app for seconds
            Input argu : seconds - deactivate app seconds
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 1
    seconds = arg['seconds']

    ##Deactivate app for seconds
    _iOSDriver_.deactivate_app(seconds)

    OK(ret, int(arg['result']), 'iOSDeactivateApp')

@DecoratorHelper.FuncRecorder
def iOSCloseCurrentApp(arg):
    ''' iOSCloseCurrentApp : Close current session app on iOS
            Input argu : N/A
            Return code : N/A
            Note : None
    '''
    ##Terminate current app
    _iOSDriver_.terminate_app(bundle_id=Config._iOSDesiredCaps_["capabilities"]["alwaysMatch"]["bundleId"])

@DecoratorHelper.FuncRecorder
def iOSLaunchApp(arg):
    ''' iOSLaunchApp : Launch app on iOS
        Input argu : bundle_id - app bundle id
        Return code : N/A
            Note : None
    '''
    bundle_id = arg['bundle_id']

    ##Set app id to config
    Config._iOSDesiredCaps_["capabilities"]["alwaysMatch"]["bundleId"] = bundle_id

    ##Launch app
    _iOSDriver_.launch_app(Config._iOSDesiredCaps_["capabilities"]["alwaysMatch"])

@DecoratorHelper.FuncRecorder
def iOSOpenCurrentApp(arg):
    ''' iOSOpenCurrentApp : Open current session app on iOS
            Input argu : N/A
            Return code : N/A
            Note : None
    '''

    _iOSDriver_.activate_app(bundle_id=Config._iOSDesiredCaps_["capabilities"]["alwaysMatch"]["bundleId"])


@DecoratorHelper.FuncRecorder
def iOSReopenApp(arg):
    ''' iOSReopenApp : Re-open shopee app on on iOS
            Input argu : N/A
            Return code : N/A
            Note : None  '''

    ##Close and open APP
    iOSCloseCurrentApp({})
    iOSOpenCurrentApp({})
    time.sleep(10)

    ##Click cancel and handle popup banner to avoid error
    #iOSClickCancelOnUpdate({})
    iOSHandlePopupBanner({})
