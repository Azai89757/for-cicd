import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCart Precondition =="
        dumplogger.info("== Setup VNPCCart Precondition ==")

    def test_VNPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-01.xml")

    def test_VNPCCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-02.xml")

    def test_VNPCCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-03.xml")

    def test_VNPCCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-04.xml")

    def test_VNPCCart05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-05.xml")

    def test_VNPCCart06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-06.xml")

    def test_VNPCCart07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-07.xml")

    def test_VNPCCart08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-08.xml")

    def test_VNPCCart09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-09.xml")

    def test_VNPCCart10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-10.xml")

    def test_VNPCCart11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCart-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCart Post condition =="
        dumplogger.info("== Setup VNPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
