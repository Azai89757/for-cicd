import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCheckOut Precondition =="
        dumplogger.info("== Setup MXPCCheckOut Precondition ==")

    # def test_MXPCCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCCheckOut-01.xml")

    # def test_MXPCCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCCheckOut-02.xml")

    # def test_MXPCCheckOut03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCCheckOut-03.xml")

    # def test_MXPCCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCCheckOut-04.xml")

    # def test_MXPCCheckOut05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXPCCheckOut-05.xml")

    def test_MXPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCheckOut-06.xml")

    def test_MXPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCheckOut-07.xml")

    def test_MXPCCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCheckOut-08.xml")

    def test_MXPCCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCheckOut-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCheckOut Post condition =="
        dumplogger.info("== Setup MXPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
