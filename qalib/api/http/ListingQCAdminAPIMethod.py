#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ListingQCAdminAPIMethod.py: The def of this file called by XML mainly.
 Listing Admin: Listing QC, Item Listing
'''
##Import system library
import json

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class LUCAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreLUCCookie(arg):
        '''
        StoreLUCCookie : Store LUC admin cookie
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]

        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_listingqc", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Bulid cookie for request header
        luc_cookie = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign cookie to API request header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = luc_cookie

        OK(ret, int(arg['result']), 'LUCAPI.StoreLUCCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SettingLUCRuleForItem(arg):
        '''
        SettingLUCRuleForItem : Setting listing upload control rule fpr item by API
                Input argu :
                    collection - collection name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection = arg["collection"]

        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        LUCAPI.StoreLUCCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'LUCAPI.SettingLUCRuleForItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteLUCGroup(arg):
        '''
        DeleteLUCGroup : Delete listing upload control group by API
                Input argu :
                    collection - collection name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection = arg["collection"]

        ##Delete group by API when have group id
        if GlobalAdapter.ListingE2EVar._GroupID_:
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
            LUCAPI.StoreLUCCookie({"result": "1"})
            GlobalAdapter.APIVar._HttpPayload_["group_id"] = int(GlobalAdapter.ListingE2EVar._GroupID_)
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
        else:
            dumplogger.info("No group ID need to delete.")

        OK(ret, int(arg['result']), 'LUCAPI.DeleteLUCGroup')


class ItemQCAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassEditItemQCStatus(arg):
        '''
        MassEditItemQCStatus : Mass update item qc status by listing QC mass edit function
                Input argu :
                    file_name - file to upload
                    option - mass_ban/mass_delete/mass_revert_ban/mass_revert_delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        option = arg["option"]

        country_id_mapping = {
            "AR": 12,
            "BR": 7,
            "CL": 11,
            "CO": 10,
            "ES": 14,
            "FR": 15,
            "ID": 1,
            "IN": 8,
            "MX": 9,
            "MY": 4,
            "PH": 5,
            "PL": 13,
            "SG": 0,
            "TH": 6,
            "TW": 2,
            "VN": 3,
        }

        ##Get qc reasons by test region
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/itemqc/mass_update_item_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_qc_reasons", "result": "1"})
        LUCAPI.StoreLUCCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["country"] = country_id_mapping[Config._TestCaseRegion_]
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "qc_reason", "result": "1"})

        ##Upload mass edit file
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/itemqc/mass_update_item_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "upload_mass_edit_file", "result": "1"})
        LUCAPI.StoreLUCCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["region"] = Config._TestCaseRegion_
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"
        GlobalAdapter.APIVar._HttpUploadFiles_ = {
            'file': open(file_path, 'rb'),
        }
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "file_path", "result": "1"})

        ##Create new mass edit task
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/itemqc/mass_update_item_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "create_new_task", "result": "1"})
        LUCAPI.StoreLUCCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["region"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpPayload_["file_path"] = GlobalAdapter.ListingE2EVar._UploadedFilePath_
        GlobalAdapter.APIVar._HttpPayload_["mass_edit_type"] = "item.qc." + option
        GlobalAdapter.APIVar._HttpPayload_["original_filename"] = file_name + ".csv"
        if option in ("mass_ban", "mass_delete"):
            if "id" in GlobalAdapter.ListingE2EVar._QCReason_:
                GlobalAdapter.APIVar._HttpPayload_["extra_data"] = "{\"reason\":\"" + GlobalAdapter.ListingE2EVar._QCReason_["id"] + "\",\"reason_description\":\"" + GlobalAdapter.ListingE2EVar._QCReason_["description"] + "\",\"reason_group\":2,\"pn_setting\":1}"
                dumplogger.info("Add extra_data to payload if option is ban/delete -> %s" % (GlobalAdapter.APIVar._HttpPayload_["extra_data"]))
            else:
                ret = -1
                dumplogger.error("Choose qc reason failed. Please check the qc reason setting at admin.")
        else:
            dumplogger.info("revert ban/revert delete do not need extra_data in payload")
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'ItemQCAPI.MassEditItemQCStatus')
