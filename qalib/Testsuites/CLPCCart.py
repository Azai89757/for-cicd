import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCCart Precondition =="
        dumplogger.info("== Setup CLPCCart Precondition ==")

    def test_CLPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCCart Post condition =="
        dumplogger.info("== Setup CLPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
