import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpLUC(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpLUC Precondition =="
        dumplogger.info("== Setup TWHttpLUC Precondition ==")

    def test_TWHttpLUC01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpLUC-01.xml")

    def test_TWHttpLUC02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpLUC-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpLUC Post condition =="
        dumplogger.info("== Setup TWHttpLUC Post condition ==")
