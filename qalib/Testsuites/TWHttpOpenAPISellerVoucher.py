import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpOpenAPISellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpOpenAPISellerVoucher Precondition =="
        dumplogger.info("== Setup TWHttpOpenAPISellerVoucher Precondition ==")

    def test_TWHttpOpenAPISellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPISellerVoucher-01.xml")

    def test_TWHttpOpenAPISellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPISellerVoucher-02.xml")

    def test_TWHttpOpenAPISellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPISellerVoucher-03.xml")

    def test_TWHttpOpenAPISellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPISellerVoucher-04.xml")

    def test_TWHttpOpenAPISellerVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPISellerVoucher-05.xml")

    def test_TWHttpOpenAPISellerVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPISellerVoucher-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpOpenAPISellerVoucher Post condition =="
        dumplogger.info("== Setup TWHttpOpenAPISellerVoucher Post condition ==")
