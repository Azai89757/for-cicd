import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *

class MXAndroidOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAndroidOrder Precondition =="
        dumplogger.info("== Setup MXAndroidOrder Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"mx"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_MXAndroidOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidOrder-01.xml")

    def test_MXAndroidOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidOrder-02.xml")

    # def test_MXAndroidOrder03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-03.xml")

    def test_MXAndroidOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidOrder-04.xml")

    def test_MXAndroidOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidOrder-05.xml")

    # def test_MXAndroidOrder06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-06.xml")

    # def test_MXAndroidOrder07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-07.xml")

    # def test_MXAndroidOrder08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-08.xml")

    # def test_MXAndroidOrder09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-09.xml")

    # def test_MXAndroidOrder10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-10.xml")

    # def test_MXAndroidOrder11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-11.xml")

    # def test_MXAndroidOrder12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-12.xml")

    # def test_MXAndroidOrder13(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidOrder-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAndroidOrder Post condition =="
        dumplogger.info("== Setup MXAndroidOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
