import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCLogisticPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCLogisticPromotion Precondition =="
        dumplogger.info("== Setup CLPCLogisticPromotion Precondition ==")

    def test_CLPCLogisticPromotion01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCLogisticPromotion-01.xml")

    def test_CLPCLogisticPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCLogisticPromotion-02.xml")

    def test_CLPCLogisticPromotion03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCLogisticPromotion-03.xml")

    def test_CLPCLogisticPromotion04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCLogisticPromotion-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCLogisticPromotion Post condition =="
        dumplogger.info("== Setup CLPCLogisticPromotion Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
