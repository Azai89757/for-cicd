import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCProductCard Precondition =="
        dumplogger.info("== Setup INPCProductCard Precondition ==")

    def test_INPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductCard-01.xml")

    def test_INPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductCard-02.xml")

    def test_INPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductCard-03.xml")

    def test_INPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductCard-04.xml")

    def test_INPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCProductCard-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCProductCard Post condition =="
        dumplogger.info("== Setup INPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
