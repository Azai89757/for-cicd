import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class FRPCRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCRecommendation Precondition =="
        dumplogger.info("== Setup FRPCRecommendation Precondition ==")

    def test_FRPCRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCRecommendation-01.xml")

    def test_FRPCRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCRecommendation-02.xml")

    def test_FRPCRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCRecommendation-03.xml")

    def test_FRPCRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCRecommendation-04.xml")

    def test_FRPCRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCRecommendation-05.xml")

    def test_FRPCRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCRecommendation-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCRecommendation Post condition =="
        dumplogger.info("== Setup FRPCRecommendation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
