﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 FlashSaleMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import CommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def CheckFlashSaleExist(arg):
    '''
    CheckFlashSaleExist : Check if flash sale exist
            Input argu :
                image - OpenCV project file
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    image = arg["image"]

    ##match_result = 1 : Sucessfully matched ; match_result = 0 : Fail to match
    match_result = 0

    for retry_count in range(1, 10):
        dumplogger.info("Start round: %s for checking homepage flash sale" % (retry_count))

        ##Close staging and open
        CommonMethod.ReopenShopeeWebsite({"result": "1"})

        ##Vertical move to flash sale
        CommonMethod.ScrollToSection({"section":"flash_sale_section", "result": "1"})

        ##Start to begin image compare by different file types
        ##Means to use Picture compare function
        match_result = XtFunc.MatchPictureImg({"mode":"web", "project": Config._TestCaseFeature_, "image": image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

        ##If match_result = 1, then match_result == ret -> sucessfully matched
        if match_result:
            dumplogger.info("Homepage flash sale matching successful!!!!")
            break
        else:
            dumplogger.info("Homepage flash sale matching failed at %d run..." % (retry_count))

    OK(ret, match_result, 'CheckFlashSaleExist')

class FlashSaleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickComingSoonFlashSale(arg):
        '''
        ClickComingSoonFlashSale : Click coming soon flash sale
                Input argu :
                    flashsale_order : next flash sale order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        flashsale_order = arg['flashsale_order']

        ##Click coming soon flash sale
        xpath = Util.GetXpath({"locate":"label_coming_soon"})
        xpath = xpath.replace("number_for_replace", flashsale_order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click coming soon flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickComingSoonFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProduct(arg):
        '''
        ClickProduct : Click product
                Input argu :
                    name : product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']

        ##Click product by name
        xpath = Util.GetXpath({"locate":"product"})
        xpath = xpath.replace('product_name', name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Goto flash sale product", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Click buy now
                Input argu :
                    product_name : product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        product_name = arg['product_name']

        ##Click buy now
        xpath = Util.GetXpath({"locate":"label_buy_now_flash_sale"})
        xpath_buy_now_btn = xpath.replace('product_name_to_be_replace', product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_buy_now_btn, "message":"Click buy now", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductPrice(arg):
        '''
        ClickProductPrice : Click product price in homepage flashsale section
                Input argu :
                    price : price string
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        price = arg['price']

        ##Click product price
        xpath = Util.GetXpath({"locate":"fs_price_home"})
        xpath_fs_price_home = xpath.replace('product_price_to_be_replace', price)
        BaseUICore.Click({"method":"xpath", "locate":xpath_fs_price_home, "message":"Click product price", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickProductPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrowRight(arg):
        '''
        ClickArrowRight : Click arrow right in homepage flashsale section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click arrow right
        xpath = Util.GetXpath({"locate":"home_fs_arrow_right_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click arrow right", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickArrowRight')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more button in landing page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see more button in landing page
        xpath = Util.GetXpath({"locate":"more_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see more button in landing page", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleBanner(arg):
        '''
        ClickFlashSaleBanner : Click flash sale banner
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click flash sale banner
        xpath = Util.GetXpath({"locate":"banner"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click flash sale banner", "result": "1"})

        OK(ret, int(arg['result']), 'FlashSaleButton.ClickFlashSaleBanner')
