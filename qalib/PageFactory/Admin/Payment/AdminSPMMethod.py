﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminSPMMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time
from datetime import datetime

##Import common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchSPMAdmin(arg):
    '''
    LaunchSPMAdmin : go to SPM admin page
        Input argu : N/A
        Return code :
            1 - success
            0 - fail
            -1 - error
    '''
    ret = 1
    spm_cookie = {}

    ##Get current env and country
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
    GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

    if 'spm' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "spm_admin_cookie", "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        spm_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        spm_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        spm_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['spm'] = [spm_cookie]

    url = "https://spm.i." + GlobalAdapter.UrlVar._Domain_ + "/admin/"
    dumplogger.info("url = %s" % (url))

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"spm", "result": "1"})
    time.sleep(5)

    ##Go to SPM Admin
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchSPMAdmin')


class SPMAdminTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnLeftPanel(arg):
        '''
        ClickTabOnLeftPanel : Click a tab at the left panel
                Input argu :
                    type - transaction / payment
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminTab.ClickTabOnLeftPanel -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentSubTab(arg):
        '''
        ClickPaymentSubTab : Click a sub item in payment tab
                Input argu :
                    subtab - payment_maintenance
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in payment tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in payment tab", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'SPMAdminTab.ClickPaymentSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentChannelItemSubTab(arg):
        '''
        ClickPaymentChannelItemSubTab : Click a sub item in payment channel item tab
                Input argu :
                    subtab - bank_bin_management / card_binding
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in payment channel item tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in payment channel item tab", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'SPMAdminTab.ClickPaymentChannelItemSubTab -> ' + subtab)


class SPMAdminTransactionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToTransactionDetailPage(arg):
        '''
        GoToTransactionDetailPage : Go to transaction detail page
                Input argu :
                    search_by - transaction_id / user_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_by = arg["search_by"]

        ##Search transaction by transaction id
        if search_by == "transaction_id":
            xpath = Util.GetXpath({"locate":"transaction_id_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PaymentE2EVar._TransactionID_, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            time.sleep(2)

        elif search_by == "user_id":
            xpath = Util.GetXpath({"locate":"user_id_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._OrderSNDict_, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            time.sleep(2)
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
            SPMAdminTransactionPage.GetTransactionID({"result": "1"})

        ##Click transaction
        xpath = Util.GetXpath({"locate":"first_transaction"})
        xpath = xpath.replace("transaction_id_to_be_replaced", GlobalAdapter.PaymentE2EVar._TransactionID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first transaction", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'SPMAdminTransactionPage.GoToTransactionDetailPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetPaymentID(arg):
        '''
        GetPaymentID : Get payment id in transaction detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get payment id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"spm_payment_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"spm_payment_id", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminTransactionPage.GetPaymentID -> ' + GlobalAdapter.PaymentE2EVar._SPMPaymentID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTransactionID(arg):
        '''
        GetTransactionID : Get first transaction id in spm transaction page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get transaction id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"transaction_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"transaction_id", "result":"1"})

        OK(ret, int(arg['result']), 'SPMAdminTransactionPage.GetTransactionID -> ' + GlobalAdapter.PaymentE2EVar._TransactionID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEntityID(arg):
        '''
        ClickEntityID : Click provision entity id to go to admin checkout detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click entity id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"spm_entity_id"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "click entity id", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminTransactionPage.ClickEntityID')


class SPMAdminPaymentButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClearCache(arg):
        '''
        ClickClearCache : Click clear cache button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click clear cache button
        xpath = Util.GetXpath({"locate": "clear_cache"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click clear cache button", "result": "1"})

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
        BaseUILogic.CheckPopupWindowMessage({"expected_message": 'Maintenance cache cleared!', "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickClearCache')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowPaymentChannelOption(arg):
        '''
        ClickShowPaymentChannelOption : Click show payment channel option button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show payment channel option button
        xpath = Util.GetXpath({"locate": "show_payment_channel_option"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show payment channel option button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickShowPaymentChannelOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowPaymentChannel(arg):
        '''
        ClickShowPaymentChannel : Click show payment channel button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show payment channel button
        xpath = Util.GetXpath({"locate": "show_payment_channel"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show payment channel button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickShowPaymentChannel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowExpiredSchedule(arg):
        '''
        ClickShowExpiredSchedule : Click show expired schedule button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show expired schedule button
        xpath = Util.GetXpath({"locate": "show_expired_schedule"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show expired schedule button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickShowExpiredSchedule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHideExpiredSchedule(arg):
        '''
        ClickHideExpiredSchedule : Click hide expired schedule button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hide expired schedule button
        xpath = Util.GetXpath({"locate": "hide_expired_schedule"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hide expired schedule button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickHideExpiredSchedule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNew(arg):
        '''
        ClickCreateNew : Click create new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create new button
        xpath = Util.GetXpath({"locate": "new_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create new button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickCreateNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click save button
                Input argu :
                    entity_id - entity id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        entity_id = arg['entity_id']

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_button"})
        xpath = xpath.replace('entity_id_to_be_replaced', entity_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentButton.ClickEdit')


class SPMAdminPaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPaymentMaintenanceInfo(arg):
        '''
        InputPaymentMaintenanceInfo : Input payment maintenance info
                Input argu :
                    status - enabled / disabled
                    desc - descripiton
                    start_time - maintenance start time
                    end_time - maintenance end time
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        status = arg["status"]
        desc = arg["desc"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        message = arg["message"]

        ##Choose status
        xpath = Util.GetXpath({"locate":"status_list"})
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": status, "result": "1"})

        ##Input desc
        xpath = Util.GetXpath({"locate":"desc_area"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":desc, "result": "1"})

        ##Input start time
        if start_time:
            ##Input Value could be a string like "test", and cannot transform into int type, therefore use exception to handle
            ##But whether time value or text value all need to input eventually.
            try:
                start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
            except ValueError:
                dumplogger.info("Input value cannot transform to int type.")
            xpath = Util.GetXpath({"locate":"start_time"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_time, "result": "1"})

        ##Input end time
        if end_time:
            ##Input Value could be a string like "test", and cannot transform into int type, therefore use exception to handle
            ##But whether time value or text value all need to input eventually.
            try:
                end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))
            except ValueError:
                dumplogger.info("Input value cannot transform to int type.")
            xpath = Util.GetXpath({"locate":"end_time"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_time, "result": "1"})

        ##Input message
        xpath = Util.GetXpath({"locate":"message_area"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":message, "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.InputPaymentMaintenanceInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePaymentChannelAndOption(arg):
        '''
        ChoosePaymentChannelAndOption : Choose payment channel and option
                Input argu :
                    category - Payment Channel/Payment Option
                    channel - Cybersource Installment/Cybersource (new)/Airpay Wallet V2/VN Airpay iBanking/Airpay GIRO
                    option - payment options
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        category = arg["category"]
        channel = arg["channel"]
        option = arg["option"]

        if category == "payment_channel":
            ##Choose category
            xpath = Util.GetXpath({"locate":"category_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "1", "result": "1"})
            time.sleep(5)

            ##Choose channel
            xpath = Util.GetXpath({"locate":"option_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": channel, "result": "1"})

        elif category == "payment_option":
            ##Choose category
            xpath = Util.GetXpath({"locate":"category_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "2", "result": "1"})
            time.sleep(5)

            ##Choose channel
            xpath = Util.GetXpath({"locate":"channel_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": channel, "result": "1"})
            time.sleep(5)

            ##Choose option
            xpath = Util.GetXpath({"locate":"option_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": option, "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.ChoosePaymentChannelAndOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreatePaymentMaintenance(arg):
        '''
        CreatePaymentMaintenance : Create payment maintenance
                Input argu :
                    category - Payment Channel/Payment Option
                    channel - Cybersource Installment/Cybersource (new)/Airpay Wallet V2/VN Airpay iBanking/Airpay GIRO
                    option - payment options
                    status - enabled / disabled
                    desc - descripiton
                    start time - maintenance start time
                    end time - maintenance end time
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        category = arg["category"]
        channel = arg["channel"]
        option = arg["option"]
        status = arg["status"]
        desc = arg["desc"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        message = arg["message"]

        ##Click create new button
        SPMAdminPaymentButton.ClickCreateNew({"result": "1"})

        ##Choose payment channel and option
        SPMAdminPaymentPage.ChoosePaymentChannelAndOption({"category":category, "channel":channel, "option":option, "result": "1"})

        ##Input payment maintenance info
        SPMAdminPaymentPage.InputPaymentMaintenanceInfo({"status":status, "desc":desc, "start_time":start_time, "end_time":end_time, "message":message, "result": "1"})

        ##Click save button
        SPMAdminPaymentButton.ClickSave({"result": "1"})

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
        BaseUILogic.CheckPopupWindowMessage({"expected_message": 'Success', "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.CreatePaymentMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPaymentMaintenance(arg):
        '''
        EditPaymentMaintenance : Edit payment maintenance info
                Input argu :
                    entity_id - entity id
                    status - enabled / disabled
                    desc - descripiton
                    start time - maintenance start time
                    end time - maintenance end time
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        entity_id = arg['entity_id']
        status = arg["status"]
        desc = arg["desc"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        message = arg["message"]

        ##Click edit button
        SPMAdminPaymentButton.ClickEdit({"entity_id":entity_id, "result": "1"})

        ##Input payment maintenance info
        SPMAdminPaymentPage.InputPaymentMaintenanceInfo({"status":status, "desc":desc, "start_time":start_time, "end_time":end_time, "message":message, "result": "1"})

        ##Click save button
        SPMAdminPaymentButton.ClickSave({"result": "1"})

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
        BaseUILogic.CheckPopupWindowMessage({"expected_message": 'Success', "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.EditPaymentMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreMaintenance(arg):
        '''
        RestoreMaintenance : Restore payment maintenance
                Input argu :
                        entity_id - entity id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        entity_id = arg['entity_id']

        ##Check if payment method is still under maintenance
        xpath = Util.GetXpath({"locate":"edit_button"})
        xpath = xpath.replace('entity_id_to_be_replaced', entity_id)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            SPMAdminPaymentPage.EditPaymentMaintenance({"entity_id":entity_id, "status":"ENABLED", "desc":"auto test", "message":"auto test", "start_time":"2", "end_time":"3", "result": "1"})
            SPMAdminTab.ClickPaymentSubTab({"subtab":"payment_maintenance", "result": "1"})
            SPMAdminPaymentButton.ClickClearCache({"result": "1"})
        else:
            dumplogger.info("Not under maintenance, no need to restore.")

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.RestoreMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReserveMaintenanceStartTime(arg):
        '''
        ReserveMaintenanceStartTime : Reserve date and time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Reserve date and time
        xpath = Util.GetXpath({"locate":"start_time"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"maintenance_starttime", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.ReserveMaintenanceStartTime -> ' + GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReserveMaintenanceEndTime(arg):
        '''
        ReserveMaintenanceEndTime : Reserve date and time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Reserve date and time
        xpath = Util.GetXpath({"locate":"end_time"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"maintenance_endtime", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.ReserveMaintenanceEndTime -> ' + GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompareDateAndTime(arg):
        '''
        CompareDateAndTime : Check if reserved date and time same as date and time that shows on FE
                Input argu :
                    time - start_time/end_time
                    compare - the location of text need to be compared (maintenance_list/new_changes)
                    entity_id - entity id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time = arg['time']
        compare = arg['compare']
        entity_id = arg['entity_id']
        start_time = GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_
        end_time = GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_

        if Config._TestCaseRegion_ == "ID":
            start_time = datetime.strftime(datetime.strptime(GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_,'%Y-%m-%d %H:%M'),'%d-%m-%Y %H:%M')
            end_time = datetime.strftime(datetime.strptime(GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_,'%Y-%m-%d %H:%M'),'%d-%m-%Y %H:%M')

        ##Set xpath
        if compare == "maintenance_list":
            xpath = Util.GetXpath({"locate": "maintenance_list_" + time})
            xpath = xpath.replace("entity_id_to_be_replaced", entity_id)
        elif compare == "new_changes":
            xpath = Util.GetXpath({"locate": "new_changes_" + time})

        ##Compare date time
        if time == "start_time":
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": start_time, "isfuzzy": "1", "result": "1"})
        elif time == "end_time":
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": end_time, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.CompareDateAndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOperateTime(arg):
        '''
        CheckOperateTime : Check operate time is current time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current time and avoid minute will be different
        if Config._TestCaseRegion_ == "ID":
            current_time = XtFunc.GetCurrentDateTime("%d-%m-%Y", 0, 0, 0)
        else:
            current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, 0, 0)

        ##Compare date time
        xpath = Util.GetXpath({"locate": "operate_time"})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": current_time, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.CheckOperateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPaymentDetailPage(arg):
        '''
        GoToPaymentDetailPage : Go to Payment detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Search payment by transaction id
        xpath = Util.GetXpath({"locate":"payment_id_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PaymentE2EVar._SPMPaymentID_, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(2)

        ##Click payment
        xpath = Util.GetXpath({"locate":"payment_id"})
        xpath = xpath.replace("payment_id_to_be_replaced", GlobalAdapter.PaymentE2EVar._SPMPaymentID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.GoToPaymentDetailPage -> Payment ID:' + GlobalAdapter.PaymentE2EVar._SPMPaymentID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetChannelRef(arg):
        '''
        GetChannelRef : Get channel ref in transaction detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get channel ref
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"spm_channel_ref"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"spm_channel_ref", "result":"1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.GetChannelRef -> ' + GlobalAdapter.PaymentE2EVar._SPMChannelRef_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetAirpayOrderID(arg):
        '''
        GetAirpayOrderID : Get airpay order id in spm payment page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get airpay order id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"airpay_order_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"airpay_order_id", "result":"1"})

        OK(ret, int(arg['result']), 'SPMAdminPaymentPage.GetAirpayOrderID -> ' + GlobalAdapter.PaymentE2EVar._AirpayOrderID_)


class SPMAdminProvisionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProvisionDetailPage(arg):
        '''
        GoToProvisionDetailPage : Go to provision detail page
                Input argu :
                    row_number - row number of provision data
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        row_number = arg["row_number"]

        ##Search provision by transaction id
        xpath = Util.GetXpath({"locate":"transaction_id_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PaymentE2EVar._TransactionID_, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(3)

        ##Click provision
        xpath = Util.GetXpath({"locate":"provision_id"})
        xpath = xpath.replace('replace_row_number', row_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click provision", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminProvisionPage.GoToProvisionDetailPage')


class SPMAdminCardBindingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByUser(arg):
        '''
        SearchByUser : Search card by user id at card binding page
                Input argu :
                    user_id - user id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        user_id = arg["user_id"]

        ##Search card by user id
        xpath = Util.GetXpath({"locate":"user_id_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":user_id, "result":"1"})
        time.sleep(2)
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminCardBindingPage.SearchByUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByCardNumber(arg):
        '''
        SearchByCardNumber : Search card by card number and expiry date at card binding page
                Input argu :
                    card_number - card number
                    expiry_date - card expiry date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        card_number = arg["card_number"]
        expiry_date = arg["expiry_date"]

        ##Search card by card number and expiry date
        xpath = Util.GetXpath({"locate":"card_number_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":card_number, "result":"1"})
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"expiry_date_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":expiry_date, "result":"1"})
        time.sleep(2)
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminCardBindingPage.SearchByCardNumber')


class SPMAdminBankBinManagementPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBankID(arg):
        '''
        ClickBankID : Click bank id in bank/bin management page
                Input argu :
                    bank_name - bank name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        bank_name = arg["bank_name"]

        ##Click bank id
        xpath = Util.GetXpath({"locate":"bank_id"})
        xpath = xpath.replace("bank_name_to_be_replaced", bank_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bank id", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminBankBinManagementPage.ClickBankID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddBin(arg):
        '''
        ClickAddBin : Click add bin in bank detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add bin
        xpath = Util.GetXpath({"locate":"add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminBankBinManagementPage.ClickAddBin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAddBinInfo(arg):
        '''
        InputAddBinInfo : Input BIN info in bank detail page
                Input argu :
                    bin_number - BIN number
                    card_type - card type
                    card_brand - card brand
                    extra_data - extra data
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bin_number = arg["bin_number"]
        card_type = arg["card_type"]
        card_brand = arg["card_brand"]
        extra_data = arg["extra_data"]

        ##Input BIN info
        xpath = Util.GetXpath({"locate":"bin_number_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":bin_number, "result":"1"})

        ##Select card type
        xpath = Util.GetXpath({"locate":"card_type_drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down icon", "result": "1"})
        xpath = Util.GetXpath({"locate":"card_type"})
        xpath = xpath.replace("card_type_to_be_replaced", card_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click card type", "result": "1"})

        ##Select card brand
        xpath = Util.GetXpath({"locate":"card_brand_drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down icon", "result": "1"})
        xpath = Util.GetXpath({"locate":"card_brand"})
        xpath = xpath.replace("card_brand_to_be_replaced", card_brand)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click card brand", "result": "1"})

        ##Input extra data
        xpath = Util.GetXpath({"locate":"extra_data_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":extra_data, "result":"1"})

        ##Click save bin
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminBankBinManagementPage.InputAddBinInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel bin in bank detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminBankBinManagementPage.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBinInfo(arg):
        '''
        EditBinInfo : edit BIN info in bank detail page
                Input argu :
                    original_bin - old bin number
                    new_bin - new bin number
                    extra_data - extra data
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        original_bin = arg["original_bin"]
        new_bin = arg["new_bin"]
        extra_data = arg["extra_data"]

        ##Click edit bin
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("bin_number_to_be_replaced", original_bin)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit bank", "result": "1"})

        ##Input BIN info
        xpath = Util.GetXpath({"locate":"bin_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_bin, "result":"1"})

        ##Input extra data
        if extra_data:
            xpath = Util.GetXpath({"locate":"extra_data_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":extra_data, "result":"1"})

        ##Click check icon
        xpath = Util.GetXpath({"locate":"check_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check icon", "result": "1"})

        ##Click save bin
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})

        OK(ret, int(arg['result']), 'SPMAdminBankBinManagementPage.EditBinInfo')
