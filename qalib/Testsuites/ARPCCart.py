import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCCart Precondition =="
        dumplogger.info("== Setup ARPCCart Precondition ==")

    def test_ARPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCCart Post condition =="
        dumplogger.info("== Setup ARPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
