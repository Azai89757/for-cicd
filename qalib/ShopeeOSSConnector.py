#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ShopeeOSSConnector.py: The def of this file called by other function.
'''
##Import system library
import boto3
from botocore import client
from botocore.exceptions import ClientError

##Import common library
import Util
import Config
import DecoratorHelper

class ShopeeOSSConnector(object):

    def __init__(self, env):
        ##Get config by env ShopeeOSSConnector.ini
        oss_config = Util.IniParser(Config._ShopeeOSSConnectorINI_)

        ##Bulid OSS connection
        self._host = oss_config[env]["host"]
        self._bucket_name = oss_config[env]["bucket_name"]

        self.session = boto3.session.Session(
            aws_access_key_id=oss_config[env]["access_key"],
            aws_secret_access_key=oss_config[env]["secret_key"]
        )
        self.resource = self.session.resource(
            service_name='s3',
            endpoint_url=self._host,
            config=client.Config(signature_version='s3'),
        )
        self._bucket = None

    @property
    def Bucket(self):
        ''' Bucket : connect to target bucket in OSS
                Input argu : N/A
                Return code :
                    self._bucket - bucket object
        '''
        if self._bucket is None:
            self._bucket = self.resource.Bucket(self._bucket_name)
        return self._bucket

    def DownloadFile(self, file_key, store_file_path):
        ''' DownloadFile : Download the file by given file_key from S3 to store_file_path
                Input argu :
                    file_key - the file key to download
                    store_file_path - the path for file to store in
                Return code :
                    True - download file successed
                    False - download file failed
        '''
        try:
            self.Bucket.download_file(file_key, store_file_path)
        except ClientError as e:
            if e.response['ResponseMetadata']['HTTPStatusCode'] == 404:
                return False
            else:
                raise
        else:
            return True

    def GetAllFiles(self, folder_path):
        ''' GetAllFiles : Get all files key in the folder sort by modified time
                Input argu :
                    folder_path - will query in given file path
                Return code :
                    sorted_files - sorted file object
                    False - download file failed
        '''
        try:
            all_files = self.Bucket.objects.filter(Prefix=folder_path)
        except ClientError as e:
            if e.response['ResponseMetadata']['HTTPStatusCode'] == 404:
                return False
            else:
                raise
        else:
            ##Sort files by last_modified time
            get_last_modified_time = lambda file_obj: file_obj.last_modified
            sorted_files = [file_obj.key for file_obj in sorted(all_files, key=get_last_modified_time, reverse=True)]
            return sorted_files

    def GetAllFilesMatchKeyWord(self, folder_path, pattern):
        ''' GetAllFilesMatchKeyWord : Get all file keys which match the given pattern
                Input argu :
                    folder_path - folder path need to be queried
                    pattern - the key word need to be matched
                Return code :
                    matched_file - matched file object
        '''
        ##Get all files in the folder
        all_files_keys = self.GetAllFiles(folder_path)

        ##Filter key by given pattern
        matched_file = []
        for key in all_files_keys:
            if pattern in key:
                matched_file.append(key)
        return matched_file
