import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCMediaStock(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCMediaStock Precondition =="
        dumplogger.info("== Setup THPCMediaStock Precondition ==")

    def test_THPCMediaStock01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMediaStock-01.xml")

    def test_THPCMediaStock02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMediaStock-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCMediaStock Post condition =="
        dumplogger.info("== Setup THPCMediaStock Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
