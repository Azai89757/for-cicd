import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCart Precondition =="
        dumplogger.info("== Setup COPCCart Precondition ==")

    def test_COPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCart Post condition =="
        dumplogger.info("== Setup COPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
