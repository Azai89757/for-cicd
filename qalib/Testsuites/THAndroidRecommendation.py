#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Config
import time
import unittest
import Util
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *


class THAndroidRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidRecommendation Precondition =="
        dumplogger.info("== Setup THAndroidRecommendation Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Disable wifi module by command only if proxy server is not used
            """
            if not Config._EnableProxy_:
                ##Disable wifi module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc wifi disable"
                dumplogger.info("Disable emulator wifi module: %s" % (cmd))
                os.system(cmd)
                ##Enable mobile data module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc data enable"
                dumplogger.info("Enable emulator mobile data module: %s" % (cmd))
                os.system(cmd)
            """

            ##Start Appium
            StartAppium({"result": "1"})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"th", "result": "1"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_THAndroidRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-01.xml")

    def test_THAndroidRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-02.xml")

    def test_THAndroidRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-03.xml")

    def test_THAndroidRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-04.xml")

    def test_THAndroidRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-05.xml")

    def test_THAndroidRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-06.xml")

    def test_THAndroidRecommendation07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-07.xml")

    def test_THAndroidRecommendation08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-08.xml")

    def test_THAndroidRecommendation09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-09.xml")

    def test_THAndroidRecommendation10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidRecommendation-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidRecommendation Post condition =="
        dumplogger.info("== Setup THAndroidRecommendation Post condition ==")

        AndroidDeInitialDriver({"result": "1"})

        ##Kill Appium
        KillAppium({"isFail":"0", "result": "1"})
