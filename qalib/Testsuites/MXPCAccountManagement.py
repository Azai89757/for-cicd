import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCAccountManagement(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCAccountManagement Precondition =="
        dumplogger.info("== Setup MXPCAccountManagement Precondition ==")

    def test_MXPCAccountManagement01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCAccountManagement-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCAccountManagement Post condition =="
        dumplogger.info("== Setup MXPCAccountManagement Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
