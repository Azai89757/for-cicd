#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminListingMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time

##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

##Import Admin library
from PageFactory.Admin import AdminCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchListingAdmin(arg):
    ''' LaunchListingAdmin : Go to admin page
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    dumplogger.info("Enter LaunchListingAdmin")
    ret = 1
    listing_admin_cookie = {}
    db_file = "admin_cookie_listing"

    ##Get current env and country
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

    if 'listing_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        listing_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        listing_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        listing_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['listing_admin'] = [listing_admin_cookie]

    ##listing admin url
    url = "https://admin.listing." + Config._EnvType_ + ".shopee.com/"
    dumplogger.info("url = %s" % (url))

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"listing_admin", "result": "1"})
    time.sleep(5)

    ##Go to Listing Admin
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchListingQCAdmin')

class ListingAdminCommon:

    def __IsForTest__(self):
        pass

    @DecoratorHelper.FuncRecorder
    def ChangeCountry(arg):
        ''' ChangeCountry : Change country
                Input argu :
                    country - vn / ph
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        country = arg["country"]

        ##Click country list
        xpath = Util.GetXpath({"locate": "country_list"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click country list", "result":"1"})
        time.sleep(1)

        ##Choose country
        xpath = Util.GetXpath({"locate": country})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose country -> " + country, "result":"1"})

        OK(ret, int(arg['result']), 'ListingAdminCommon.ChangeCountry')

    @DecoratorHelper.FuncRecorder
    def EnterProductListingPage(arg):
        ''' EnterProductListingPage : Enter Product Listing Page
                Input argu :
                    shop_id - shop id
                    product_id - product id
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        product_id = arg["product_id"]

        ##Go to product listing detail page
        listing_page_url = "https://admin.listing.staging.shopee.com/product/listing/detail?shopId=" + shop_id + "&itemId=" + product_id + "&region=" + Config._TestCaseRegion_
        BaseUICore.GotoURL({"url": listing_page_url, "result": "1"})

        OK(ret, int(arg['result']), 'ListingAdminCommon.EnterProductListingPage')

class ListingAdminTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExpandListingAdminSubTab(arg):
        ''' ExpandListingAdminSubTab : Expand a sub item in Listing Admin home page if it is not expanded
                Input argu :
                    subtab - sub tab under the page (fe_category)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Expand a sub item in listing admin home
        xpath = Util.GetXpath({"locate":subtab})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Expand a sub item in Listing Admin home page", "result":"1"})

        OK(ret, int(arg['result']), 'ListingAdminTab.ExpandListingAdminSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFECategorySubTab(arg):
        ''' ClickFECategorySubTab : Click a sub item under FE category section
                Input argu :
                    subtab - sub tab under the page (fe_category)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item under FE category section
        xpath = Util.GetXpath({"locate":subtab})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click a sub item under FE category page", "result":"1"})

        OK(ret, int(arg['result']), 'ListingAdminTab.ClickFECategorySubTab -> ' + subtab)

class AdminFrontEndCategoryPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EnterFrontEndCategoryTreeWithUrl(arg):
        ''' EnterFECategoryTreeWithUrl : Enter FE category tree with url
                Input argu :
                    country - vn / ph
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        country = arg["country"]
        tree_id = {
            "sg": "201",
            "my": "202",
            "th": "205",
            "vn": "206",
            "ph": "207",
            "br": "208",
            "mx": "209"
        }

        ##Go to specific country FE category tree with rul
        url = "https://admin.listing.staging.shopee.com/fecategory/treeinfo/category?tree_id=" + tree_id[country]
        BaseUICore.GotoURL({"url": url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFrontEndCategoryPage.EnterFrontEndCategoryTreeWithUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnterFrontEndCategoryTree(arg):
        ''' ClickEnterFrontEndCategoryTree : Click to enter FE category tree
                Input argu :
                    tree_name - tree name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        tree_name = arg["tree_name"]

        ##Click enter FE category tree
        xpath = Util.GetXpath({"locate": "enter_icon"})
        xpath = xpath.replace("name_to_be_replaced", tree_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enter category tree -> " + tree_name, "result":"1"})

        OK(ret, int(arg['result']), 'AdminFrontEndCategoryPage.ClickEnterFrontEndCategoryTree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddNewCategory(arg):
        ''' AddNewCategory : Add new category
                Input argu :
                    category_name - category name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click add new btn
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new category button", "result":"1"})
        time.sleep(2)

        ##Input new category name
        xpath = Util.GetXpath({"locate": "new_category_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": category_name, "result": "1"})
        time.sleep(2)

        ##Click OK btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click OK button", "result":"1"})

        OK(ret, int(arg['result']), 'AdminFrontEndCategoryPage.AddNewCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCategoryStatus(arg):
        ''' ChangeCategoryStatus : Change category status
                Input argu :
                    category_name - category name
                    status - disabled / activated
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]
        status = arg["status"]

        ##Click change status (if the original status is correct, then no need to change status)
        xpath = Util.GetXpath({"locate": status})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click change status", "result":"1"})

        OK(ret, int(arg['result']), 'AdminFrontEndCategoryPage.ChangeCategoryStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteFrontEndCategory(arg):
        ''' DeleteFrontEndCategory : Delete frontend category
                Input argu :
                    category_name - category name
                    action - confirm / cancel
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]
        action = arg["action"]

        ##Click delete category icon
        xpath = Util.GetXpath({"locate": "delete_icon"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete icon", "result":"1"})
            time.sleep(2)

            ##Click confirm btn
            xpath = Util.GetXpath({"locate": action})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm", "result":"1"})

        OK(ret, int(arg['result']), 'AdminFrontEndCategoryPage.DeleteFrontEndCategory')

class AdminListingDetailPage:

    def __IsForTest__(self):
        pass

    @DecoratorHelper.FuncRecorder
    def ClickProductEdit(arg):
        ''' ClickProductEdit : Click Edit button at top of product listing page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Edit button
        xpath = Util.GetXpath({"locate": "product_edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingDetailPage.ClickProductEdit')

    @DecoratorHelper.FuncRecorder
    def ClickProductSubmit(arg):
        ''' ClickProductSubmit : Click Submit button at top of product listing page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Submit button
        xpath = Util.GetXpath({"locate": "product_submit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingDetailPage.ClickProductSubmit')

    @DecoratorHelper.FuncRecorder
    def SetProductPublishStatus(arg):
        ''' SetProductPublishStatus : Set product to publish / unlist
                Input argu :
                    status - publish / unlist
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click Edit button
        AdminListingDetailPage.ClickProductEdit({"result": "1"})

        ##Change product status
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click publish button", "result": "1"})

        ##Click Submit button
        AdminListingDetailPage.ClickProductSubmit({"result": "1"})

        OK(ret, int(arg['result']), 'AdminListingDetailPage.SetProductPublishStatus')

    @DecoratorHelper.FuncRecorder
    def SetProductPriceStock(arg):
        ''' SetProductPriceStock : Set product price and stock (by model)
                Input argu :
                    model_id - model id
                    price - model price (if not necessary use empty value)
                    stock - model stock (if not necessary use empty value)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        model_id = arg["model_id"]
        price = arg["price"]
        stock = arg["stock"]

        ##Click Edit button
        AdminListingDetailPage.ClickProductEdit({"result": "1"})

        ##Enter product data according to input
        if price:
            xpath = Util.GetXpath({"locate": "model_price"})
            xpath = xpath.replace("id_to_be_replaced", model_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": price, "result": "1"})

        if stock:
            xpath = Util.GetXpath({"locate": "model_stock"})
            xpath = xpath.replace("id_to_be_replaced", model_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": stock, "result": "1"})

        ##Click Submit button
        AdminListingDetailPage.ClickProductSubmit({"result": "1"})

        OK(ret, int(arg['result']), 'AdminListingDetailPage.SetProductPriceStock')

    @DecoratorHelper.FuncRecorder
    def SetProductCondition(arg):
        ''' SetProductCondition : Set Product Condition
                Input argu :
                    condition - new / used
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        condition = arg["condition"]

        ##Click Edit button
        AdminListingDetailPage.ClickProductEdit({"result": "1"})

        ##Click product condition dropdown list
        xpath = Util.GetXpath({"locate": "condition_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click condition dropdown list", "result": "1"})

        ##Click product condition option
        xpath = Util.GetXpath({"locate": condition})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click condition option -> " + condition, "result": "1"})

        ##Click Submit button
        AdminListingDetailPage.ClickProductSubmit({"result": "1"})

        OK(ret, int(arg['result']), 'AdminListingDetailPage.SetProductCondition')
