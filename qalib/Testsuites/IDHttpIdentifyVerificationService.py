import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpIVS(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpIVS Precondition =="
        dumplogger.info("== Setup IDHttpIVS Precondition ==")

    def test_IDHttpIVS01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-01.xml")

    def test_IDHttpIVS02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-02.xml")

    def test_IDHttpIVS03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-03.xml")

    def test_IDHttpIVS04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-04.xml")

    def test_IDHttpIVS05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-05.xml")

    def test_IDHttpIVS06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-06.xml")

    def test_IDHttpIVS07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpIVS-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpIVS Post condition =="
        dumplogger.info("== Setup IDHttpIVS Post condition ==")
