import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCheckOut Precondition =="
        dumplogger.info("== Setup IDPCCheckOut Precondition ==")

    def test_IDPCCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-01.xml")

    def test_IDPCCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-02.xml")

    def test_IDPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-03.xml")

    def test_IDPCCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-04.xml")

    def test_IDPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-05.xml")

    def test_IDPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-06.xml")

    def test_IDPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-07.xml")

    def test_IDPCCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-08.xml")

    def test_IDPCCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-09.xml")

    def test_IDPCCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-10.xml")

    def test_IDPCCheckOut11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-11.xml")

    def test_IDPCCheckOut12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-12.xml")

    def test_IDPCCheckOut13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-13.xml")

    def test_IDPCCheckOut14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-14.xml")

    def test_IDPCCheckOut15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-15.xml")

    def test_IDPCCheckOut16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-16.xml")

    def test_IDPCCheckOut17(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-17.xml")

    def test_IDPCCheckOut18(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-18.xml")

    def test_IDPCCheckOut19(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCheckOut-19.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCheckOut Post condition =="
        dumplogger.info("== Setup IDPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
