#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidPageTemplatesMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import FrameWorkBase
import DecoratorHelper
import Util
from Config import dumplogger
import GlobalAdapter

##Import android library
import AndroidBaseUICore
import AndroidBaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidProductLabelPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceRange(arg):
        '''
        InputPriceRange : Input the min price or the max price on the filter panel
                Input argu :
                    type - min / max
                    price - the limit price you want to set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        price = arg["price"]

        ##Input max price
        xpath = Util.GetXpath({"locate": type})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":price, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelPage.InputPriceRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOutsideOfFilter(arg):
        '''
        ClickOutsideOfFilter : Click area out of the search filter panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click the gray part of the screen
        AndroidBaseUICore.AndroidClickCoordinates({"x_cor":"50", "y_cor":"800"})

        OK(ret, int(arg['result']), 'AndroidProductLabelPage.ClickOutsideOfFilter')


class AndroidPersonalizedCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceRange(arg):
        '''
        InputPriceRange : Input the min price or the max price on the filter panel
                Input argu :
                    type - min / max
                    price - the limit price you want to set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        price = arg["price"]

        ##Input max price
        xpath = Util.GetXpath({"locate": type})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":price, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPage.InputPriceRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOutsideOfFilter(arg):
        '''
        ClickOutsideOfFilter : Click area out of the search filter panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click the gray part of the screen
        AndroidBaseUICore.AndroidClickCoordinates({"x_cor":"50", "y_cor":"800"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPage.ClickOutsideOfFilter')


class AndroidPersonalizedCollectionPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click on filter button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on filter button
        xpath = Util.GetXpath({"locate":"filter_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPageButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset filter button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset
        xpath = Util.GetXpath({"locate":"reset_filter"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPageButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterOptions(arg):
        '''
        ClickFilterOptions : Click filter panel button
                Input argu :
                    button_name - the name of the button displayed on the filter panel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]

        ##Click filter panel button
        xpath = Util.GetXpath({"locate":button_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click filter panel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPageButton.ClickFilterOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToTop(arg):
        '''
        ClickBackToTop : Click back to top button in colleciton landing page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to top button in landing page
        xpath = Util.GetXpath({"locate":"back_top_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click back to top button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPageButton.ClickBackToTop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowMore(arg):
        '''
        ClickShowMore : Click filter panel show more at specific section
                Input argu :
                    section - filter section name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Click filter panel show more in specific section
        xpath = Util.GetXpath({"locate":section})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click %s Show more" % section, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPageButton.ClickShowMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowLess(arg):
        '''
        ClickShowLess : Click filter panel show less at specific section
                Input argu :
                    section - filter section name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Click filter panel show less in specific section
        xpath = Util.GetXpath({"locate":section})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click %s Show less" % section, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPersonalizedCollectionPageButton.ClickShowLess')


class AndroidProductLabelButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCart(arg):
        '''
        ClickCart : Go to cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Go to cart page
        xpath = Util.GetXpath({"locate":"cart"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Go to cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelButton.ClickCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click on filter button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on filter button
        xpath = Util.GetXpath({"locate":"filter_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset filter button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on filter button
        xpath = Util.GetXpath({"locate":"reset_filter"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterOptions(arg):
        '''
        ClickFilterOptions : Click filter panel button
                Input argu :
                    button_name - the name of the button displayed on the filter panel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]

        ##Click filter panel button
        xpath = Util.GetXpath({"locate":button_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click filter panel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelButton.ClickFilterOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowMore(arg):
        '''
        ClickShowMore : Click filter panel show more at specific section
                Input argu :
                    section - filter section name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Click filter panel show more in specific section
        xpath = Util.GetXpath({"locate":section})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click %s Show more" % section, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelButton.ClickShowMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowLess(arg):
        '''
        ClickShowLess : Click filter panel show less at specific section
                Input argu :
                    section - filter section name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Click filter panel show less in specific section
        xpath = Util.GetXpath({"locate":section})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click %s Show less" % section, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductLabelButton.ClickShowLess')


class AndroidAutoCollectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToTop(arg):
        '''
        ClickBackToTop : Click back to top button in auto colleciton page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to top button in auto colleciton page
        xpath = Util.GetXpath({"locate":"back_top_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click back to top button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAutoCollectionButton.ClickBackToTop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCart(arg):
        '''
        ClickCart : Go to cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Go to cart page
        xpath = Util.GetXpath({"locate":"cart_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Go to cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAutoCollectionButton.ClickCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChat(arg):
        '''
        ClickChat : Go to chat page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Go to chat page
        xpath = Util.GetXpath({"locate":"chat_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Go to chat page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAutoCollectionButton.ClickChat')


class AndroidShopCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopText(arg):
        '''
        ClickShopText : Click the shop collection card by shop text, please use the unique shop name on the shop collection page
                Input argu :
                    shop_text - the unique shop text on the shop collection page
                    layout - 1 or 2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_text = arg["shop_text"]
        layout = arg["layout"]

        ##Click collection card
        xpath = Util.GetXpath({"locate": "layout_" + layout})
        xpath = xpath.replace("string_to_be_replaced", shop_text)

        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click collection card", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopCollectionPage.ClickShopText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopName(arg):
        '''
        ClickShopName : Click the shop collection card by shop name, please use the unique shop name or shop text on the shop collection page
                Input argu :
                    shop_name - the unique shop name on the shop collection page
                    layout - 1 or 2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]
        layout = arg["layout"]

        ##Click collection card
        xpath = Util.GetXpath({"locate": "layout_" + layout})
        xpath = xpath.replace("string_to_be_replaced", shop_name)

        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click collection card", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopCollectionPage.ClickShopName')


class AndroidShopCollectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollow(arg):
        '''
        ClickFollow : Click follow shop button by card order
                Input argu :
                    shop_name - the unique shop name on the shop collection page
                    layout - 1 or 2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]
        layout = arg["layout"]

        ##Click shop follow button
        xpath = Util.GetXpath({"locate": "layout_" + layout})
        xpath = xpath.replace("string_to_be_replaced", shop_name)

        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click follow button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopCollectionButton.ClickFollow')


class AndroidCampaignEntryCollectionComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCampaignEntryCollectionComponent.ClickOnButton')


class AndroidAllCampaignPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCampaignTab(arg):
        '''
        ChangeCampaignTab : Change campaign tab
                Input argu :
                    tab_name - category name
                    swipe_time - swipe time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg["tab_name"]
        swipe_time = arg["swipe_time"]

        ##Scroll tab bar
        xpath = Util.GetXpath({"locate":"cateory_scroll"})
        if swipe_time:
            xpath = Util.GetXpath({"locate":"cateory_scroll"})
            AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times": swipe_time, "result": "1"})

        ##Click specific tab
        xpath = Util.GetXpath({"locate":"tab_name"})
        xpath = xpath.replace("string_to_be_replaced", tab_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click all campaign tabs -> " + tab_name, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAllCampaignPage.ChangeCampaignTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipePage(arg):
        '''
        SwipePage : Direct swipe screen from an initial element position to turn page
                Input argu :
                    locate - initial element for swipe
                    direction - left/right
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        locate = arg["locate"]
        direction = arg["direction"]
        ret = 1

        ##Get initial element location for reference
        AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
        locate_x = GlobalAdapter.CommonVar._PageAttributes_['x']
        locate_y = GlobalAdapter.CommonVar._PageAttributes_['y']

        ##Get element size
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"size", "mode":"single", "result": "1"})
        width = min(GlobalAdapter.CommonVar._PageAttributes_['width'], 1080)
        height = GlobalAdapter.CommonVar._PageAttributes_['height']

        ##Swipe element in given direction for certain times
        if direction == "right":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x + width * 0.9, "locate_y":locate_y + height * 0.5, "movex":locate_x + width * 0.1, "movey":locate_y + height * 0.5, "result": "1"})
        if direction == "left":
            AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x + width * 0.1, "locate_y":locate_y + height * 0.5, "movex":locate_x + width * 0.9, "movey":locate_y + height * 0.5, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAllCampaignPage.SwipePage')


class AndroidAllCampaignComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAllCampaignComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareIcon(arg):
        '''
        ClickShareIcon : Click share icon
            Input argu :
                campaign_name - campaign name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        campaign_name = arg["campaign_name"]

        ##Click on button
        xpath = Util.GetXpath({"locate": "share_icon"})
        xpath = xpath.replace("string_to_be_replaced", campaign_name)

        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click icon to share campaign: %s" % campaign_name, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAllCampaignComponent.ClickShareIcon')


class AndroidCustomizedCollectionComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCustomizedCollectionComponent.ClickOnButton')


class AndroidCustomizedCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCampaignTab(arg):
        '''
        ChangeCampaignTab : Change campaign tab
                Input argu :
                    tab_name - category name
                    swipe_time - swipe time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg["tab_name"]
        swipe_time = arg["swipe_time"]

        ##Scroll tab bar
        xpath = Util.GetXpath({"locate":"cateory_scroll"})
        if swipe_time:
            xpath = Util.GetXpath({"locate":"cateory_scroll"})
            AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times": swipe_time, "result": "1"})

        ##Click specific tab
        xpath = Util.GetXpath({"locate":"tab_name"})
        xpath = xpath.replace("string_to_be_replaced", tab_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click all campaign tabs -> " + tab_name, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCustomizedCollectionPage.ChangeCampaignTab')
