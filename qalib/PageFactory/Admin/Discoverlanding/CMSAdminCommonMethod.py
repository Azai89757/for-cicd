#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 CMSAdminCommonMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import os
import re
import time
import datetime

##Import framework common library
import XtFunc
import DecoratorHelper
import GlobalAdapter
import FrameWorkBase
import Config
from Config import dumplogger
import Util

##Inport Admin library
from PageFactory.Admin import AdminCommonMethod

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def LaunchCMSAdmin(arg):
    '''
    LaunchCMSAdmin : Go to admin page
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    env = Config._EnvType_.lower()
    db_file = "admin_cookie_dl"
    dl_admin_cookie = {}

    ##Determine country
    url = "https://dl-admin." + GlobalAdapter.UrlVar._Domain_

    ##Go to BE
    BaseUICore.GotoURL({"url":url, "result":"1"})
    time.sleep(5)

    ##If cookie is not stored for the first time, get cookie from db
    if 'dl_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Get current env and country
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        dl_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        dl_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        dl_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['dl_admin'] = [dl_admin_cookie]

    ##Go to Shopee Admin
    BaseUICore.SetBrowserCookie({"storage_type":"dl_admin", "result":"1"})
    time.sleep(3)
    BaseUICore.GotoURL({"url":url, "result":"1"})
    BaseUICore.PageHasLoaded({"result":"1"})

    ##Check if admin login success from cookies stored in db, if not, use testing account to login for alternative
    xpath = Util.GetXpath({"locate":"cms_admin_title"})
    if not BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
        ##Login with google account if needed
        AdminCommonMethod.GoogleAccountLogin({"account":"shopeetwqa", "password":"shopee@123!", "result":"1"})

        ##Get google logged in cookies
        BaseUICore.GetCurrentBrowserCookies({"storage_type":"dl_admin", "result":"1"})

    ##Click google account circle icon to get login account name
    xpath = Util.GetXpath({"locate":"google_account_icon"})
    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click google account icon", "result":"1"})
    time.sleep(3)

    ##Get google login account name if exist
    xpath = Util.GetXpath({"locate":"google_account_username"})
    if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"google_account_username", "result":"1"})
        dumplogger.info("Login google account name : {}".format(GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_))
        GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_ = GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_.split("@")[0]
    else:
        dumplogger.info("Google account name not found!!!!!!!!!!!!!")
        ret = 0

    xpath = Util.GetXpath({"locate":"google_account_icon"})
    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click google account icon", "result":"1"})

    OK(ret, int(arg['result']), 'LaunchCMSAdmin')

class CMSAdminTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminHomeSubTab(arg):
        '''
        ClickCMSAdminHomeSubTab : Click a sub item in CMS Admin home page
                Input argu :
                    subtab - sub tab under the page (campaign_tag / product_collection)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in home square
        xpath = Util.GetXpath({"locate":subtab})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click a sub item in CMS Admin home", "result":"1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminHomeSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminBannerSubTab(arg):
        '''
        ClickCMSAdminBannerSubTab : Click a sub item in CMS Admin under banner section
                Input argu :
                    subtab - sub tab under banner section (category / floating / homepage_mall / home_carousel / mall_carousel / mall_popup / home_popup / skinny / dp_carousel / dp_scrolling / home_square)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If banner tab not expanded, then click banner tab first
        xpath = Util.GetXpath({"locate": "banner_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "banner_tab_unexpanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner tab initially", "result": "1"})

        ##Click a sub tab under banner section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin home", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminBannerSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAdminCampaignSubTab(arg):
        '''
        ClickAdminCampaignSubTab : Click a sub item in BMS Admin under campaign section
                Input argu :
                    subtab - sub tab under banner section (category_carousel  / home_carousel / home_popup / skinny)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub tab under campaign section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in BMS Admin home", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickAdminCampaignSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminOfficialShopSubTab(arg):
        '''
        ClickCMSAdminOfficialShopSubTab : Click a sub item in CMS Admin under official shop section
                Input argu :
                    subtab - sub tab under official shop section (official_shop_summary / shop_category_detail / brand_of_the_week)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If official tab not expanded, then click official tab first
        xpath = Util.GetXpath({"locate": "official_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "official_tab_unexpanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click official tab initially", "result": "1"})

        ##Click a sub tab under official section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin official", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminOfficialShopSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminServiceSubTab(arg):
        '''
        ClickCMSAdminServiceSubTab : Click a sub item in CMS Admin under service section
                Input argu :
                    subtab - sub tab under service section (custom url / rule set / bundle)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If service tab not expanded, then click service tab first
        xpath = Util.GetXpath({"locate": "service_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "service_tab_expanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner tab initially", "result": "1"})

        ##Click a sub tab under sevice section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin home", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminServiceSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminPagesSubTab(arg):
        '''
        ClickCMSAdminPagesSubTab : Click a sub item in CMS Admin under pages section
                Input argu :
                    subtab - sub tab under pages section (wallet_campaign_caption / coins_campaign_caption / qr_scan / wallet / coins / background)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If pages tab not expanded, then click pages tab first
        xpath = Util.GetXpath({"locate": "pages_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "pages_tab_unexpanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click pages tab initially", "result": "1"})

        ##Click a sub tab under pages section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin pages", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminPagesSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminBannerTemplateSubTab(arg):
        '''
        ClickCMSAdminBannerTemplateSubTab : Click a sub item in CMS Admin under BannerTemplate section
                Input argu :
                    subtab - sub tab under pages section (template_registration / banner_tab_expanded / banner_tab_unexpanded)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If pages tab not expanded, then click pages tab first
        xpath = Util.GetXpath({"locate": "banner_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "banner_tab_unexpanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click pages tab initially", "result": "1"})

        ##Click a sub tab under pages section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin pages", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminBannerTemplateSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminMicrositeTab(arg):
        '''
        ClickCMSAdminMicrositeTab : Click a sub item in CMS Admin under microsite section
                Input argu :
                    subtab - sub tab under microsite section (campaign management)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If service tab not expanded, then click service tab first
        xpath = Util.GetXpath({"locate": "microsite_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "microsite_tab_expanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click microsite tab initially", "result": "1"})

        ##Click a sub tab under microsite section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin microsite", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminMicrositeTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSNewUserZoneTab(arg):
        '''
        ClickCMSNewUserZoneTab : Click a sub item in CMS Admin under new user zone section
                Input argu :
                    subtab - sub tab under new user zone section (homepage)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If new user zone tab not expanded, then click to expand new user zone tab first
        xpath = Util.GetXpath({"locate": "nuz_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "nuz_tab_expanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new user zone tab initially", "result": "1"})

        ##Click a sub tab under new user zone section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin new user zone", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSNewUserZoneTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminCategoryTab(arg):
        '''
        ClickCMSAdminCategoryTab : Click a sub item in CMS Admin under categories section
                Input argu :
                    subtab - sub tab under new user zone section (category_management / popular_collection_module)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##If new user zone tab not expanded, then click to expand new user zone tab first
        xpath = Util.GetXpath({"locate": "category_tab_expanded"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "category_tab_unexpanded"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new user zone tab initially", "result": "1"})

        ##Click a sub tab under new user zone section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in CMS Admin categories", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminCategoryTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSCampaignModuleTab(arg):
        '''
        ClickCMSCampaignModuleTab : Click campaign module section tab
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click campaign module section tab
        xpath = Util.GetXpath({"locate": "campaign_module_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click campaign module section tab", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSCampaignModuleTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminBannerSpaceSubTab(arg):
        '''
        ClickCMSAdminBannerSpaceSubTab : Click a sub item in CMS Admin under banner space section
                Input argu :
                    subtab - sub tab under the page (banner_space_group / banner_preview)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in banner space
        xpath = Util.GetXpath({"locate":subtab})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click a sub item in banner space", "result":"1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminBannerSpaceSubTab -> ' + subtab)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSTransparentBackgroundImageTab(arg):
        '''
        ClickCMSTransparentBackgroundImageTab : Click CMS transparent background image tab
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click CMS transparent background image tab
        xpath = Util.GetXpath({"locate":"tbi_tab"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click CMS transparent background image tab", "result":"1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSTransparentBackgroundImageTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSItemCardTab(arg):
        '''
        ClickCMSItemCardTab : Click CMS item card tab
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click CMS item card tab
        xpath = Util.GetXpath({"locate": "item_card_display"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CMS item card tab", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSItemCardTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminMediaStockSubTab(arg):
        '''
        ClickCMSAdminMediaStockSubTab : Click a sub item in CMS Admin under media stock section
                Input argu :
                    subtab - sub tab under the page (media_stock_attributes / media_stock_attributes_group / media_stock_images)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub item in media stock space
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in media stock space", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminMediaStockSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminAuditLogsTab(arg):
        '''
        ClickCMSAdminAuditLogsTab : Click CMS admin audit logs tab
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click CMS admin audit logs tab
        xpath = Util.GetXpath({"locate": "audit_logs_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CMS admin audit logs tab", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSAdminAuditLogsTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSShopeeMartSubTab(arg):
        '''
        ClickCMSShopeeMartSubTab : Click a sub item in CMS Admin under shopee supermarket section
                Input argu :
                    subtab - sub tab under the page (homepage_header / key_selling_points / category_management / homepage_categories_display / horizontal_collection / deals_tab_horizontal_collection / vouchers)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub tab in shopee supermarket section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in shopee supermarket section", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSShopeeMartSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSMessagingSubTab(arg):
        '''
        ClickCMSMessagingSubTab : Click a sub item in CMS Admin under Messaging section
                Input argu :
                    subtab - sub tab under the page (bottom_bar_message)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click a sub tab in messaging section
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a sub item in messaging section", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSMessagingSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSMissionTab(arg):
        '''
        ClickCMSMissionTab : Click CMS mission tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click CMS mission tab
        xpath = Util.GetXpath({"locate": "mission_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CMS mission tab", "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSMissionTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSModuleHeaderSubTab(arg):
        '''
        ClickCMSModuleHeaderSubTab : Click CMS module header sub tab
                Input argu :
                    subtab - festival_skin / header_text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Click CMS module header sub tab
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CMS module header sub tab -> " + subtab, "result": "1"})

        OK(ret, int(arg['result']), 'CMSAdminTab.ClickCMSModuleHeaderSubTab')
