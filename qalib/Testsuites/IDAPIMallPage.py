import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDAPIMallPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIMallPage Precondition =="
        dumplogger.info("== Setup IDAPIMallPage Precondition ==")

    def test_IDAPIMallPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIMallPage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIMallPage Post condition =="
        dumplogger.info("== Setup IDAPIMallPage Post condition ==")
