import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerDiscount Precondition =="
        dumplogger.info("== Setup TWHttpSellerDiscount Precondition ==")

    def test_TWHttpSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscount-01.xml")

    def test_TWHttpSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscount-02.xml")

    def test_TWHttpSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscount-03.xml")

    def test_TWHttpSellerDiscount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscount-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerDiscount Post condition =="
        dumplogger.info("== Setup TWHttpSellerDiscount Post condition ==")
