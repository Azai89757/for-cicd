#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 QATest.py: Main script for test FrameWorkBase
'''

import sys
sys.dont_write_bytecode = True
import os
import time
import unittest
sys.path.append("qalib")
import FrameWorkBase
import Config
from Config import dumplogger
from Logger import TerminalOutput
import Parser
from Util import *
import CaseDefine
import RerunCalculation

try:
    ##Load test case Type
    if sys.argv[1:]:
        for index in range(len(sys.argv[1:])):
            if "-T" in sys.argv[int(index)]:
                dumplogger.info("sys.argv[%s] = %s" % (index+1, sys.argv[index+1]))
                if "," in sys.argv[index+1]:
                    split_casetype = sys.argv[index+1].split(",")
                    for sub_casetype in split_casetype:
                        if "-" in sub_casetype:
                            Config._AllTestCaseType_.append(sub_casetype.split("-")[0])
                        else:
                            Config._AllTestCaseType_.append(sub_casetype)
                elif "-" in sys.argv[index+1]:
                    Config._AllTestCaseType_.append(sys.argv[index+1].split("-")[0])
                else:
                    Config._AllTestCaseType_.append(sys.argv[index+1])

        dumplogger.info("Config._AllTestCaseType_ = %s" % (Config._AllTestCaseType_))

    ##Load RERUN parameter
    ##0 -> Default mode
    ##1 -> Enable RAT Rerun
    ##2 -> Enable Automated Trigger bv  RAT
    # if sys.argv[3] == '0':
    #     Parser._RERUN_ = '0'
    # elif sys.argv[3] == '1':
    #     Parser._RERUN_ = '1'
    # elif sys.argv[3] == '2':
    #     Parser._RERUN_ = '2'
    # else:
    #     Parser._RERUN_ = 'Fail parameter'

except IndexError:
    Parser._RERUN_ = '0'


def TriggerTestCase(case_type):
    ''' TriggerTestCase : Trigger test case func
            Input argu :
                N/A
            Note : None
    '''

    ##Store test case type
    Config._TestCaseRegion_, Config._TestCasePlatform_, Config._TestCaseFeature_ = FrameWorkBase.FilterCaseRegionAndPlatform(case_type)

    ##Set shopee domain in to global variable
    AssembleShopeeDomain()

    ##Load test case
    suite = unittest.TestLoader().loadTestsFromTestCase(CaseDefine.ImportTestSuites()[case_type])

    ##Unit test command
    unittest.TextTestRunner(verbosity=0, stream=Config._NULL_).run(suite)


def RUN():
    ''' RUN : Trigger automation run
            Input argu :
                N/A
            Note : None
    '''

    ##For windows only to prevent system 1314 error
    if Config._platform_ == "windows":

        import win32security
        import win32process
        #import Util_Win

        ##Change windows terminal coding to utf-8
        os.system("chcp 65001")

        ##Enable SE_BACKUP_NAME and SE_RESTORE_NAME to prevent 1314 error
        ##1314 error : PRIVILEGE_NOT_HELD by RegSaveKeyEx()
        token = win32security.OpenProcessToken(win32process.GetCurrentProcess(), win32security.TOKEN_ADJUST_PRIVILEGES | win32security.TOKEN_QUERY)
        if token:
            for priv in (win32security.SE_BACKUP_NAME, win32security.SE_RESTORE_NAME):
                luid = win32security.LookupPrivilegeValue(None, priv)
                newState = [(luid, win32security.SE_PRIVILEGE_ENABLED)]
                try:
                    win32security.AdjustTokenPrivileges(token, 0, newState)
                except IOError:
                    print "Please check registry and permission !!!!"
                except:
                    print "Could not get requred priviledges !!!!!"

    ##Initial test case define and remove redundant test case type
    unique_alltestcasetype = list(set(Config._AllTestCaseType_))

    ##Sort by test case from sys.argv
    unique_alltestcasetype.sort(key=Config._AllTestCaseType_.index)

    ##Sequence to trigger test case
    for case_type in unique_alltestcasetype:
        TriggerTestCase(case_type)


def Rerun():
    ''' Rerun : Get rerun list and trigger rerun case
            Input argu :
                N/A
            Note : None
    '''
    rerun_alltestcasetype = []

    ##Get rerun list from script
    Config._RerunList_ = Config._RerunObj_.RRBotMain(Config._CaseFailList_)
    dumplogger.info("Config._RerunList_ = %s" % (Config._RerunList_))

    ##For Rerun only, Config._RerunList_ is a sequence of rerun test case
    if Config._RerunList_:
        ##Store rerun_alltestcasetype first
        for rerun_case in Config._RerunList_:
            rerun_case_type = rerun_case.split("-")[0]
            if rerun_case_type not in rerun_alltestcasetype:
                rerun_alltestcasetype.append(rerun_case_type)

        ##Sequence to trigger rerun test case
        for rerun_case_type in rerun_alltestcasetype:
            TriggerTestCase(rerun_case_type)


##Get start time
stime = time.time()

##Main
if __name__ == '__main__':
    if os.path.exists(Config._LogFile_):
        os.unlink(Config._LogFile_)

    sys.stdout = TerminalOutput()

    ##Initial proxy server if needed
    if Config._EnableProxy_ and Config._DeviceNumber_:
        cmd = StartProxyServer()
        proxy_pid = FrameWorkBase.PNameQueryProcess("cmd", cmd)
        dumplogger.info("PID of proxy server... %s" % (proxy_pid))

    ##If trigger emulator
    if '-E' in FrameWorkBase._OPT_:
        ##Start record android device performance
        StartRecordAndroidDevicePerformance()

    ##Load regional(country) json files for xpath define
    LoadRegionalJson()

    ##Initial rerun class obj here for further usage
    Config._RerunObj_ = RerunCalculation.RerunCalculate()

    ##Call main function to execute framework
    RUN()

    ##Rerun process only in daily automation trgger, for local, there will be no re-run
    if Config._HostName_ in Config._WLForAutoLogDB_:
        ##Reset case relation ship list to avoid error
        Config._CaseRelationShip_ = []
        Rerun()

##Use to handle reset automation client status.
#if Parser._RERUN_ == "2":
#    ResetRATClientStatus()

##Get duration
dtime = time.time() - stime

print "--::Summary::------------------------------------------------"
print "Output : " + Config._LogFile_

print "-------------------------------------------------------------"
print "Total " + str(Parser._CASE_['counts']) + " cases ...%.2f sec in %s (" % (dtime, Config._HostName_) + str(Parser._CASE_['fcounts']) + " cases failed)\n"
print "Fail case list:"
print Parser._CASE_['flist']
print "Fail rerun case list:"
print Parser._CASE_['re_flist']
print ""

##Fail Run cases&log
if '-f' in FrameWorkBase._OPT_:
    print "--::FailRun::------------------------------------------------"
    if Parser._CASE_['fcounts']:
        FrameWorkBase._OPT_['-T'] = Parser._CASE_['flist']
        Parser._CASE_['flist'] = ""
        Parser._CASE_['fcounts'] = 0
        Parser._CASE_['counts'] = 0
        Config._LogFolder_ = "log2"
        stime = time.time()
        RUN()
        dtime = time.time() - stime
        print "--::FailRun Summary::------------------------------------"
        print "Total " + str(Parser._CASE_['counts']) + " cases ...%.2f sec in %s (" % (dtime, Config._HostName_) + str(Parser._CASE_['fcounts']) + " cases failed)\n"
        print "FailRun case list:"
        print Parser._CASE_['flist']

##kill proxy server and split proxy log into failed case folder
if Config._EnableProxy_:
    ##Kill proxy process by pid
    FrameWorkBase.KillProcessByPID(proxy_pid)

    ##Write qatest_debuglog into ini file
    ini_path = Config._SystemPath_ + Config._OutputFolderName_ + Config.dict_systemslash[Config._platform_] + "proxy_config.ini"
    ini_data = {"debug_folder_info": {"qatest_debuglog": Config._OutputFolderPath_}}
    IniWriter(ini_path, ini_data)

    ##Set up device proxy network
    proxy_ip_port = "%s:%s" % (Config._DeviceHostIP_, Config._ProxyInfo_["port"] + int(Config._DeviceNumber_))
    command = "adb -s " + Config._DeviceID_ + " shell settings put global http_proxy " + proxy_ip_port
    os.system(command)

##If trigger emulator
if '-E' in FrameWorkBase._OPT_:
    ##Kill emulator
    command = "adb -s " + Config._DeviceID_ + " emu kill"
    os.system(command)

    ##End record android performance
    EndRecordAndroidDevicePerformance()
