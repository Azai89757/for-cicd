#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminOMSMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import time

##Import common library
import Util
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchOMSAdmin(arg):
    '''
    LaunchOMSAdmin : go to OMS admin page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    url = "http://athena.ssc.test.shopeemobile.com/redirect?email=tw.qa@shopee.com&name=tw.qa&token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcGVyYXRvciI6InRyb3VibGVsZXNzIiwiZXhwIjoxNjIyNjE3MTE1fQ.19moiVA7L4ZXTva0ARcK7J0afgN4b57_XzA9fbsQxIM#"
    dumplogger.info("url = %s" % (url))

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(10)

    OK(ret, int(arg['result']), 'LaunchOMSAdmin')


class AdminOMSPushStatus:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def OMSChangeShippingStatus(arg):
        '''
        OMSChangeShippingStatus : Change shipping status through OMS
                Input argu :
                    status - logistics_request_created / logistics_pickup_done / logistics_pickup_retry /
                             logistics_pickup_failed / logistics_delivery_done / logistics_delivery_failed /
                             logistics_request_cancelled / logistics_invalid

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        status = arg["status"]

        time.sleep(3)

        ##Launch ONS Admin
        LaunchOMSAdmin({"result": "1"})

        ##Go to push service
        BaseUICore.GotoURL({"url":"https://athena.ssc.test.shopeemobile.com/omsService/pushService", "result": "1"})

        ##Click env dropdown and select staging
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"env_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click update logistic info button", "result": "1"})
        xpath = Util.GetXpath({"locate":"envStaging"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click update logistic info button", "result": "1"})

        ##Input forder
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "input_forder"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._FOrderID_, "result": "1"})

        ##Choose a shipment status to update
        xpath = Util.GetXpath({"locate":"update_logistic_info_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click status dropdown", "result": "1"})
        time.sleep(2)
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose a shipment status to update", "result": "1"})

        ##Click update
        xpath = Util.GetXpath({"locate": "update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update", "result": "1"})

        ##Screen shot to make sure we have click submit and success message popup
        XtFunc.PrintScreen({"file_name": "oms_change_shipping", "mode":"web", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOMSPushStatus.OMSChangeShippingStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def OMSCheckArrangeShipmentStatus(arg):
        '''
        OMSCheckArrangeShipmentStatus : Check arrange shipment status through OMS
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Launch ONS Admin
        LaunchOMSAdmin({"result": "1"})

        ##Click env dropdown and select staging
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"env_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click environment dropdown", "result": "1"})
        xpath = Util.GetXpath({"locate":"envStaging"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select environment", "result": "1"})

        ##Input order id
        xpath = Util.GetXpath({"locate":"input_orderid"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

        ##Click search
        xpath = Util.GetXpath({"locate":"quickSubmit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click search button", "result": "1"})
        time.sleep(3)

        ##Screen shot to make sure we have click submit and success message popup
        XtFunc.PrintScreen({"file_name": "oms_arrange_shipment", "mode":"web", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOMSPushStatus.OMSCheckArrangeShipmentStatus')
