#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ListingAdminAPIMethod.py: The def of this file called by XML mainly.
 Listing Admin: Listing QC, Item Listing
'''
##Import system library
import json
from operator import itemgetter

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class ListingAdminAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCookie(arg):
        ''' AssignCookie : Assign listing admin cookie to API header
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]

        ##Send SQL to get listing admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_listing", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Bulid cookie for API header
        listing_admin_cookie = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign cookie to API header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = listing_admin_cookie

        OK(ret, int(arg['result']), 'ListingAdminAPI.AssignCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckProductStock(arg):
        ''' CheckProductStock : Check product stock by admin product listing API
                Input argu :
                    shopid: shop id
                    itemid: item id
                    check_type: order_cancel_restock / seller_soldout / compare_value
                    value_difference: the difference between old value and new value (needed when 'check_type' is compare_value)
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg['shopid']
        itemid = arg["itemid"]
        check_type = arg["check_type"]
        value_difference = int(arg["value_difference"])

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get and set Http Api data from collection get_stock_audits
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_stock_audits", "result": "1"})

        ##Assign shopid and itemid to API header
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("shop_id_to_be_replace", shopid)
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("item_id_to_be_replace", itemid)

        ##Assign itemid to API payload
        GlobalAdapter.APIVar._HttpPayload_["item_id"] = int(itemid)

        ##Change payload format to json string
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Assign listing admin cookie
        ListingAdminAPI.AssignCookie({"result": "1"})

        ##Send request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        ##Check API response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get API response
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##For loop each product audit log
        for each_product_audit in response["data"]["audits"]:

            ##Check type is "restock" and audit log order id is target check order id
            if check_type == "order_cancel_restock" and each_product_audit["ref_entity_id"] == GlobalAdapter.OrderE2EVar._OrderID_:
                ##Get product new stock and product old stock
                new_product_stock = int(each_product_audit["audit_deltas"][0]["new"])
                old_product_stock = int(each_product_audit["audit_deltas"][0]["old"])

                ##Check product stock (restock)
                if new_product_stock - old_product_stock > 0:
                    dumplogger.info("Check product restock pass!!! Old product stock is %s. New product stock is %s." % (old_product_stock, new_product_stock))
                else:
                    ret = 0
                    dumplogger.error("Check product restock fail!!! Old product stock is %s. New product stock is %s." % (old_product_stock, new_product_stock))

                break

            ##Check type is "soldout" and audit log operator is IIS service
            elif check_type == "seller_soldout" and each_product_audit["operator"] == "seller listing":
                ##Get product new stock and product old stock
                new_product_stock = int(each_product_audit["audit_deltas"][0]["new"])
                old_product_stock = int(each_product_audit["audit_deltas"][0]["old"])

                ##Check product stock (soldout)
                if old_product_stock - new_product_stock > 0 and new_product_stock == 0:
                    dumplogger.info("Check product soldout pass!!! Old product stock is %s. New product stock is %s." % (old_product_stock, new_product_stock))
                else:
                    ret = 0
                    dumplogger.error("Check product soldout fail!!! Old product stock is %s. New product stock is %s." % (old_product_stock, new_product_stock))

                break

            ##Check type is "compare_value"
            elif check_type == "compare_value" and each_product_audit["ref_entity_id"] == GlobalAdapter.OrderE2EVar._OrderID_:
                ##Get product new stock and product old stock
                new_product_stock = int(each_product_audit["audit_deltas"][0]["new"])
                old_product_stock = int(each_product_audit["audit_deltas"][0]["old"])

                ##Check product stock (soldout)
                if new_product_stock - old_product_stock == value_difference:
                    dumplogger.info("Compare stock value pass!!! Old product stock is %s. New product stock is %s." % (old_product_stock, new_product_stock))
                else:
                    ret = 0
                    dumplogger.error("Compare stock value fail!!! Old product stock is %s. New product stock is %s." % (old_product_stock, new_product_stock))

                break
        else:
            ret = 0
            dumplogger.error("Check product stock fail!!! Can't find target stock info from API response. Your check type is \"%s\"" % (check_type))

        ##DeInitial Http Api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'ListingAdminAPI.CheckProductStock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReorderAuditLogList(arg):
        ''' ReorderAuditLogList : Reorder audit log by audit type
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get API response
        res_t = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Get item audit log
        audit_log = res_t['data']['audits']

        ##Sort audit log list by key audit_type
        audit_log.sort(key=itemgetter('audit_type'), reverse=False)
        dumplogger.info("Sort the audit log %s" % (audit_log))

        GlobalAdapter.APIVar._APIResponse_["text"] = json.dumps(res_t)
        dumplogger.info("New GlobalAdapter.APIVar._APIResponse_ = %s" % (GlobalAdapter.APIVar._APIResponse_))

        OK(ret, int(arg['result']), 'ListingAdminAPI.ReorderAuditLogList')
