#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 NotificationAdminAPIMethod.py: The def of this file called by XML mainly.
 Notification Admin: Notification
'''
##Import system library
import json

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config

##Import api library
from api import APICommonMethod
import HttpAPICore

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class NotificationAdminAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCookie(arg):
        ''' AssignCookie : Assign notification admin cookie to API header
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]

        ##Send SQL to get notification admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_notification", "method":"select", "verify_result":"","assign_data_list":"", "store_data_list":store_list, "result": "1"})

        ##Bulid cookie for API header
        notification_admin_cookie = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign cookie to API header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = notification_admin_cookie

        OK(ret, int(arg['result']), 'NotificationAdminAPI.AssignCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SendPushNotification(arg):
        '''
        SendPushNotification : Send push notification
                Input argu :
                    collection - pnar / crm_queue
                    task_id - task id
                    user_id - user id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        collection = arg["collection"]
        task_id = arg["task_id"]
        user_id = arg["user_id"]
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get and set Http Api data from collection
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Assign task id to API header
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("replace_task_id", task_id)

        ##Assign task_id and user_id to API payload
        GlobalAdapter.APIVar._HttpPayload_["task_id"] = int(task_id)
        GlobalAdapter.APIVar._HttpPayload_["user_noti"]["user_id"] = int(user_id)

        ##Change payload format to json string
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Assign notification admin cookie
        NotificationAdminAPI.AssignCookie({"result": "1"})

        ##Send request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        ##Check API response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##DeInitial Http Api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'NotificationAdminAPI.SendPushNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SendSMSGateWay(arg):
        '''
        SendSMSGateWay : Send sms gateway
                Input argu :
                    collection - sms_gatway
                    task_id - task id
                    phone_number - phone number(Add 00 to the front end of the phone number)
                    gateway - gateway
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        collection = arg["collection"]
        task_id = arg["task_id"]
        phone_number = arg["phone_number"]
        gateway = arg["gateway"]
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get and set Http Api data from collection
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Assign task_id and phone_number to API payload
        GlobalAdapter.APIVar._HttpPayload_["task_id"] = int(task_id)
        GlobalAdapter.APIVar._HttpPayload_["sms_info"]["phone_number"] = phone_number
        GlobalAdapter.APIVar._HttpPayload_["sms_info"]["gateway"] = int(gateway)

        ##Change payload format to json string
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Assign notification admin cookie
        NotificationAdminAPI.AssignCookie({"result": "1"})

        ##Send request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        ##Check API response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##DeInitial Http Api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'NotificationAdminAPI.SendSMSGateWay')
