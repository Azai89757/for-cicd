import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCMePage Precondition =="
        dumplogger.info("== Setup SGPCMePage Precondition ==")

    def test_SGPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCMePage Post condition =="
        dumplogger.info("== Setup SGPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
