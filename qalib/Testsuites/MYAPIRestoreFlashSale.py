import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYAPIRestoreFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIRestoreFlashSale Precondition =="
        dumplogger.info("== Setup MYAPIRestoreFlashSale Precondition ==")

    def test_MYAPIRestoreFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MYAPIRestoreFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIRestoreFlashSale Post condition =="
        dumplogger.info("== Setup MYAPIRestoreFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
