import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNAPIHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIHomePage Precondition =="
        dumplogger.info("== Setup VNAPIHomePage Precondition ==")

    def test_VNAPIHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIHomePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIHomePage Post condition =="
        dumplogger.info("== Setup VNAPIHomePage Post condition ==")
