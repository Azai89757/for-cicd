﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidLoginSignUpMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time
import random
import string

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidHomePageMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidShopeeLogin(arg):
    '''
    AndroidShopeeLogin : Use for shopee app log in
            Input argu :
                account - user account
                password - user password
                logintype - account / google / sms
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    account = arg["account"]
    password = arg["password"]
    logintype = arg["logintype"]
    need_login = 1
    ret = 1

    ##Check device cpu usage is stable
    AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "70"})

    ##Click Me tab btn
    AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    ##Check device cpu usage is stable
    AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "70"})

    ##Move to the top of mepage
    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"3", "result": "1"})
    time.sleep(8)

    ##Check if login or not
    xpath = Util.GetXpath({"locate":"login_btn"})
    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate": xpath, "passok": "0", "result": "1"}):
        ##Get login button => In this case, it should be non-logged in scenario
        dumplogger.info("Can locate login button, current status should be non-logged in.")

    else:
        ##Can not get login button => In this case, it should be logged in scenario
        ##Get account name
        xpath = Util.GetXpath({"locate":"account_name"})
        AndroidBaseUICore.AndroidGetElements({"locate": xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        current_text = GlobalAdapter.CommonVar._PageAttributes_

        ##Compare if shop_name is same as account, if not, perform logout first.
        if current_text == account:
            dumplogger.info("This account: %s has logged in, not need to logout and re-login again", account)
            need_login = 0

        elif current_text != account:
            dumplogger.info("Prepare to logout!")
            AndroidShopeeLogout({"result": "1"})

            ##Click Me tab btn
            AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    if need_login:

        ##Click login btn
        AndroidLoginSignupButton.ClickLogin({"type":"login_button", "result": "1"})

        ##Check device cpu usage is stable
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "70"})

        if logintype == 'account':

            ##Input account and password
            time.sleep(2)
            AndroidLoginPage.InputAccount({"account": account, "result": "1"})
            AndroidLoginPage.InputPassword({"password": password, "page_type": "login_page", "result": "1"})

            ##Click login button
            AndroidLoginSignupButton.ClickConfirm({"page_type": "login_page", "result": "1"})

        elif logintype == 'google':

            ##Click google button
            AndroidLoginSignupButton.ClickLogin({"type":"google_login_button", "result": "1"})

            ##Click specific account tab
            xpath = Util.GetXpath({"locate":"google_account_select"})
            xpath = xpath.replace("replace_account", account)
            AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result": "1"})

        elif logintype == 'facebook_notinstalled_nologin':

            ##Kill FB
            AndroidBaseUICore.SendADBCommand({"command_type":"Uninstall", "adb_arg":"com.facebook.katana", "is_return": "0", "result": "1"})

            ##Click FB button
            AndroidLoginSignupButton.ClickLogin({"type":"fb_login_button", "result": "1"})

            ##Type FB account and password
            xpath = Util.GetXpath({"locate":"m_facebook_login_email"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})
            xpath2 = Util.GetXpath({"locate":"m_facebook_login_password"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath2, "string":password, "result": "1"})

            ##Click login button
            xpath = Util.GetXpath({"locate":"m_facebook_login_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click FB login button", "result": "1"})

            ##Click continue button
            xpath = Util.GetXpath({"locate":"m_facebook_continue_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click FB continue button", "result": "1"})

        elif logintype == 'sms':

            ##Click sms login text
            AndroidLoginSignupButton.ClickLogin({"type":"sms_login_button", "result": "1"})

            ##Input phone number
            AndroidLoginPage.InputPhoneNumber({"phone":account, "page_type":"sms_login_page", "result": "1"})

            ##Click next button
            AndroidLoginSignupButton.ClickNext({"page_type":"sms_login_page", "result": "1"})

            ##Slider capthca was disabled by RE fraud
            ##Uncomment following code when slider captcha is enabled
            ##Input captcha
            #AndroidLoginPage.InputCaptcha({"captcha":"123456", "result": "1"})

            ##Click confirm button
            #AndroidLoginSignupButton.ClickConfirm({"page_type": "captcha_page", "result": "1"})

            ##Input vcode
            AndroidLoginPage.InputVcode({"vcode":"123456", "result": "1"})
            time.sleep(5)

            ##Skip click next button step in specific region
            if Config._TestCaseRegion_ not in ("MX", "ID", "PH", "VN"):
                ##Click next button
                AndroidLoginSignupButton.ClickNext({"page_type":"vcode_page", "result": "1"})
                time.sleep(5)

        else:
            ret = -1
            dumplogger.info("Enter AndroidShopeeLogin but method not defined yet")

        ##Sleep 15s to wait loading popup appear
        time.sleep(15)

        ##Check device cpu usage is stable. Main purpose is wait loading popup disappear
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "60"})

        ##Close cookies message in specific region
        if Config._TestCaseRegion_ in ("PL", "ES"):
            AndroidBaseUICore.AndroidCloseCookiesPopUp({"result": "1"})

        ##Handle Close RNLogger
        AndroidBaseUICore.AndroidCloseRNLoggerPopUp()

    OK(ret, int(arg['result']), 'AndroidShopeeLogin')

@DecoratorHelper.FuncRecorder
def AndroidShopeeLogout(arg):
    '''
    AndroidShopeeLogout : Use for shopee app log out
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Click Me tab btn
    AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    ##Swipe to the top to be able to click setting btn
    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"2", "result": "1"})

    ##Check if current status is logged-in, if so, perform log out to prevent error
    locate = Util.GetXpath({"locate":"check_login_status"})
    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":locate, "message":"Check if seller tag exists", "passok": "0", "result": "1"}):

        ##Click My Account btn
        xpath = Util.GetXpath({"locate":"setting_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click setting icon", "result": "1"})

        ##Scroll account settings to logout btn
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"5", "result": "1"})

        ##Click log-out btn
        xpath = Util.GetXpath({"locate":"logout_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click logout btn", "result": "1"})

        ##Click confirm btn on popup
        xpath = Util.GetXpath({"locate":"confirm_logout_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click confirm btn", "result": "1"})

        ##Sleep 20s to wait homepage is loaded to avoid app crash
        time.sleep(20)

        ##Check device cpu usage is stable
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

        ##Click Me tab btn
        AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    else:

        ##Has been logout status, back to home page
        dumplogger.info("No account logged-in, go to Main page.")
        AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type":"home","result": "1"})

    OK(ret, int(arg['result']), 'AndroidShopeeLogout')


class AndroidLoginSignupButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchLoginSignup(arg):
        '''
        SwitchLoginSignup : To switch login/signup in metab
                Input argu :
                    switch_type : login_tab / signup_tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        switch_type = arg["switch_type"]

        ##Click order cancel button
        xpath = Util.GetXpath({"locate": switch_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Switch login/signup", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.SwitchLoginSignup -> ' + switch_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ForgetPassword(arg):
        '''
        ForgetPassword : Click Back button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order cancel button
        xpath = Util.GetXpath({"locate":"forget_password"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click forget password", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ForgetPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogin(arg):
        '''
        ClickLogin : Click Login button
                Input argu :
                    type - login_button / google_login_button / fb_login_button / apple_login_button / sms_login_button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click login button with retry 3 times
        xpath = Util.GetXpath({"locate": type})
        for retry_times in range(3):
            if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate": xpath, "message":"Check login button for %s times" % (retry_times+1), "passok": "0", "result": "1"}):
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Login button => %s" % (type), "result": "1"})
                time.sleep(5)
            else:
                break

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSignup(arg):
        '''
        ClickSignup : Click signup button
                Input argu :
                    type - signup_button / google_signup_button / fb_signup_button / apple_signup_button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click singup button
        xpath = Util.GetXpath({"locate": type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Signup button => %s" % (type), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickSignup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinue(arg):
        '''
        ClickContinue : Click Continue button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click button
        xpath = Util.GetXpath({"locate":"continue_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickContinue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTermsOfService(arg):
        '''
        ClickTermsOfService : Click terms of service in signup page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click terms of service in signup page
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":"terms_of_service", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickTermsOfService')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrivacyPolicy(arg):
        '''
        ClickPrivacyPolicy : Click privacy policy in signup page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click privacy policy by opencv in signup page
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":"privacy_policy", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickPrivacyPolicy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendSMS(arg):
        '''
        ClickSendSms : Click send sms by opencv in another verification page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send sms by opencv in another verification page
        xpath = Util.GetXpath({"locate":"send_sms_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click send sms by opencv in another verification page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickSendSMS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickResend(arg):
        '''
        ClickResend : Click resend text by opencv in verification code page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click resend text by opencv in verification code page
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":"resend_text", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickResend')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAnotherVerifyMethod(arg):
        '''
        ClickAnotherVerifyMethod : Click another verify method text
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Using openCV to click another verify method
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":"another_verify_method_text", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickAnotherVerifyMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click Confirm button
                Input argu :
                    page_type - ban_popup_login_page / captcha_page / login_page / not_registered_page / sms_login_page / another_vcode_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click button
        xpath = Util.GetXpath({"locate": page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAvatar(arg):
        '''
        ClickAvatar : Click avatar image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click avatar
        xpath = Util.GetXpath({"locate":"avatar_image"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Avatar Image", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickAvatar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHelp(arg):
        '''
        ClickHelp : Click help button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click help button
        xpath = Util.GetXpath({"locate":"help_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click help button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickHelp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        '''
        ClickNext : Click next button in any page
                Input argu :
                    page_type : next button in which page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click SMS login text
        xpath = Util.GetXpath({"locate": page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickNext')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefreshCaptureText(arg):
        '''
        ClickRefreshCaptureText : Click refresh captcha text in catpcha page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refresh captcha text in catpcha page
        xpath = Util.GetXpath({"locate":"refresh_captcha"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click refresh captcha text", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickRefreshCaptureText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in catpcha page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button in catpcha page
        xpath = Util.GetXpath({"locate":"cancel_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close button in login page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close button in login page
        xpath = Util.GetXpath({"locate":"close_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click close button in login page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRegister(arg):
        '''
        ClickRegister : Click register button in signup page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click register button in signup page
        xpath = Util.GetXpath({"locate":"register_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click register button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickRegister')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewPasswordIcon(arg):
        '''
        ClickViewPasswordIcon : Click view password icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view password icon
        xpath = Util.GetXpath({"locate": "view_password_icon"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click view password icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickViewPasswordIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDifferentVerifyMethod(arg):
        '''
        ClickDifferentVerifyMethod : Click different verify method text button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":"other_verification_method_redmi", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickDifferentVerifyMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPhoneCall(arg):
        '''
        ClickPhoneCall : Click phone call button in another verification page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click phone call button in another verification page
        xpath = Util.GetXpath({"locate":"phone_call_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click phone call button in another verification page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickPhoneCall')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMobileNumberChange(arg):
        '''
        ClickMobileNumberChange : Click mobile number change button in forgot password page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mobile number change button in forgot password page
        xpath = Util.GetXpath({"locate":"mobile_number_change"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click mobile number change button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickMobileNumberChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseShopeePayPopup(arg):
        '''
        ClickCloseShopeePayPopup : Click close shopee pay popup
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close shopee pay popup
        xpath = Util.GetXpath({"locate":"close_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click close shopee pay popup", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginSignupButton.ClickCloseShopeePayPopup')


class AndroidLoginPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AccountLoginFET(arg):
        '''
        AccountLoginFET : FET test scenario of "Account" login method
                Input argu :
                    account - user account
                    password - password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        ret = 1

        ##Enter account
        AndroidLoginPage.InputAccount({"account": account, "result": "1"})

        ##Enter password
        AndroidLoginPage.InputPassword({"password": password, "page_type": "login_page", "result": "1"})

        ##Click login button
        AndroidLoginSignupButton.ClickConfirm({"page_type": "login_page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginPage.AccountLoginFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVcode(arg):
        '''
        InputVcode : Input vcode
                Input argu :
                    vcode - Vcode
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        vcode = arg["vcode"]

        ##Check account vcode need or not
        xpath = Util.GetXpath({"locate":"verification_code"})

        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Input vcode in vcode page
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":vcode, "result": "1"})

        else:
            dumplogger.info("Dont need to vcode verify")

        OK(ret, int(arg['result']), 'AndroidLoginPage.InputVcode -> ' + vcode)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhoneNumber(arg):
        '''
        InputPhoneNumber : Input phone number in sms login page
                Input argu :
                    phone - phone number of account attached
                    page_type - input phone number field in which page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        phone = arg["phone"]
        page_type = arg["page_type"]

        ##Input phone number in sms login page
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginPage.InputPhoneNumber -> ' + phone)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCaptcha(arg):
        '''
        InputCaptcha : Input captcha in captcha verification page
                Input argu :
                    captcha - input captcha for verification
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        captcha = arg["captcha"]

        ##Input captcha in captcha verification page
        xpath = Util.GetXpath({"locate":"sms_captcha_column"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":captcha, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginPage.InputCaptcha -> ' + captcha)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAccount(arg):
        '''
        InputAccount : Input account in login page
                Input argu :
                    account - shopee account and it should be username or email or phone number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]

        ##Input account in login page
        xpath = Util.GetXpath({"locate":"input_account"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginPage.InputAccount -> ' + account)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        '''
        InputPassword : Input password in login page
                Input argu :
                    password - shopee account password
                    page_type - input password field in which page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        password = arg["password"]
        page_type = arg["page_type"]

        ##Input password in login page
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginPage.InputPassword -> ' + password)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShopeeLoginFET(arg):
        '''
        ShopeeLoginFET : Login with random username
                Input argu :
                    password - password
                    scenarios - using initial username or not (initial : using initial random username; modified: using modified username)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        password = arg["password"]
        scenarios = arg["scenarios"]
        ret = 1

        ##Decide using initial random username to login or modified one
        if scenarios == "initial":
            ##Use previous random username to login shopee
            AndroidLoginPage.AccountLoginFET({"account":GlobalAdapter.GeneralE2EVar._RandomString_, "password":password, "scenario":"Scenario: Input previous random username", "result": "1"})

        else:
            ##Use modified username to login shopee
            AndroidLoginPage.AccountLoginFET({"account":GlobalAdapter.GeneralE2EVar._ModifyString_, "password":password, "scenario":"Scenario: Input previous random username", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLoginPage.ShopeeLoginFET')


class AndroidSignUpPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserInfo(arg):
        '''
        InputUserInfo : Input data for sign up detail (Set email and account name)
                Input argu :
                    account - account
                    username - username
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]
        username = arg["username"]

        ##Rename the user name to avoid user name error
        xpath = Util.GetXpath({"locate":"username"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":username, "result": "1"})

        ##Remove email addresss to avoid the duplicate
        xpath = Util.GetXpath({"locate":"email"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSignUpPage.InputUserInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhone(arg):
        '''
        InputPhone : Input phone number to signup
                Input argu :
                    phone - phone number / if u want auto genrate, input "auto_generate" / null string
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        phone = arg["phone"]
        ret = 1

        ##Auto generate set to 1 and did not enter phone number
        if phone == "auto_generate":
            ##generate phone number with 0082 + random phone number
            phone = "00" + XtFunc.GenerateRandomString(8, "numbers")

        ##Input phone number
        xpath = Util.GetXpath({"locate":"phone_number"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSignUpPage.InputPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCaptcha(arg):
        '''
        InputCaptcha : Input captcha in captcha verification page
                Input argu :
                    captcha - captcha number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        captcha = arg["captcha"]
        ret = 1

        ##Input captcha
        xpath = Util.GetXpath({"locate":"captcha"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":captcha, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSignUpPage.InputCaptcha')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVcode(arg):
        '''
        InputVcode : Input vcode after phone number to signup
                Input argu :
                    vcode - vcode number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        vcode = arg["vcode"]
        ret = 1

        ##Input vcode
        xpath = Util.GetXpath({"locate":"vcode"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":vcode, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSignUpPage.InputVcode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        '''
        InputPassword : Input password to signup
                Input argu :
                    password - password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        password = arg["password"]
        ret = 1

        ##Input password
        xpath = Util.GetXpath({"locate":"password"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSignUpPage.InputPassword')


class AndroidGoogleSignupPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoogleSignup(arg):
        '''
        GoogleSignup : Sign up by google
                Input argu :
                    account - google account
                    password - google password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        ret = 1

        ##Click Google button
        xpath = Util.GetXpath({"locate":"google_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click Google button", "result": "1"})

        ##Google pop up is exist click add other user
        xpath = Util.GetXpath({"locate": "google_popup"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate":"google_select_another_account"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"select another account", "result": "1"})

        ##Type Google account
        xpath = Util.GetXpath({"locate":"google_login_email"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"google_continue_btn_in_account"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click google continue button", "result": "1"})
        time.sleep(5)

        ##Type Google password
        xpath = Util.GetXpath({"locate":"google_login_password"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"google_continue_btn_in_password"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click google continue button", "result": "1"})

        AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"3", "result": "1"})

        ##Click I agree button
        xpath = Util.GetXpath({"locate":"google_agree_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click google I agree button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidGoogleSignupPage.GoogleSignup->' + account)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoogleAccountRestore(arg):
        '''
        GoogleAccountRestore : Remove google account connected to android device.
                               Because once have logged in with google account in shopee APP, even if delete account in shopee admin,
                               the android device will still store the google account, so in order to avoid this problem,
                               this method can help remove google account from android device.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Send adb command to open device settings page
        AndroidBaseUICore.SendADBCommand({"command_type":"Intent", "adb_arg":"android.settings.SETTINGS", "is_return": "0", "result": "1"})

        ##Go to "sync" tab
        xpath = Util.GetXpath({"locate":"sync"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result": "1"})
        time.sleep(2)

        xpath = Util.GetXpath({"locate":"google_account"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click google tab
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click google tab", "result": "1"})

            ##Click account tab
            xpath = Util.GetXpath({"locate":"delete_google_account"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click account tab", "result": "1"})
            xpath = Util.GetXpath({"locate":"more_button"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click more button", "result": "1"})

            ##Click delete button
            xpath = Util.GetXpath({"locate":"remove_account"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click delete button", "result": "1"})
            time.sleep(2)

            ##Click confirm delete button
            xpath = Util.GetXpath({"locate":"confirm_remove_account"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click confirm button", "result": "1"})
            time.sleep(2)

        else:
            ##Not exist -> No google account connected
            dumplogger.info("No google account connected in this device")

        ##Close settings page
        AndroidBaseUICore.SendADBCommand({"command_type":"StopAPP", "adb_arg":"com.android.settings", "is_return": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidGoogleSignupPage.GoogleAccountRestore')
