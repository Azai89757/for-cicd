import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class CLPCProductCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCProductCollection Precondition =="
        dumplogger.info("== Setup CLPCProductCollection Precondition ==")

    def test_CLPCProductCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-01.xml")

    def test_CLPCProductCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-02.xml")

    def test_CLPCProductCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-03.xml")

    def test_CLPCProductCollection04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-04.xml")

    def test_CLPCProductCollection05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-05.xml")

    def test_CLPCProductCollection06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-06.xml")

    def test_CLPCProductCollection07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCollection-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCProductCollection Post condition =="
        dumplogger.info("== Setup CLPCProductCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
