import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCMyVoucherPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMyVoucherPage Precondition =="
        dumplogger.info("== Setup VNPCMyVoucherPage Precondition ==")

    def test_VNPCMyVoucherPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMyVoucherPage-01.xml")

    def test_VNPCMyVoucherPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMyVoucherPage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMyVoucherPage Post condition =="
        dumplogger.info("== Setup VNPCMyVoucherPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
