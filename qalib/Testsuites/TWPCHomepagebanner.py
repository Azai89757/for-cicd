import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCHomepagebanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCHomepagebanner Precondition =="
        dumplogger.info("== Setup TWPCHomepagebanner Precondition ==")

    def test_TWPCHomepagebanner001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-001.xml")

    def test_TWPCHomepagebanner002(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-002.xml")

    def test_TWPCHomepagebanner003(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-003.xml")

    def test_TWPCHomepagebanner004(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-004.xml")

    def test_TWPCHomepagebanner005(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-005.xml")

    def test_TWPCHomepagebanner006(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-006.xml")

    def test_TWPCHomepagebanner007(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-007.xml")

    def test_TWPCHomepagebanner008(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-008.xml")

    def test_TWPCHomepagebanner009(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomepagebanner-009.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCHomepagebanner Post condition =="
        dumplogger.info("== Setup TWPCHomepagebanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
