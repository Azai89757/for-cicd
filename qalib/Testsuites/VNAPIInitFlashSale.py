import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNAPIInitFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIInitFlashSale Precondition =="
        dumplogger.info("== Setup VNAPIInitFlashSale Precondition ==")

    def test_VNAPIInitFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIInitFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIInitFlashSale Post condition =="
        dumplogger.info("== Setup VNAPIInitFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
