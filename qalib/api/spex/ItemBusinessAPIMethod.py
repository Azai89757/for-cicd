#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ItemBusinessAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
from operator import itemgetter

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
from Config import dumplogger

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class ItemBusinessAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReorderModelsList(arg):
        ''' ReorderModelsList : Reorder models by model id and rule type
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get API response
        res_t = GlobalAdapter.APIVar._APIResponse_[1]

        ##Get item model list
        model_list = res_t['info'][0]['models']

        ##Reorder model list by model id
        model_list.sort(key=itemgetter('model_id'), reverse=False)

        ##Reorder ongoing_prices by rule type
        for item in model_list:
            ongoing_prices = item['price']['ongoing_prices']
            ongoing_prices.sort(key=itemgetter('rule_type'), reverse=False)

        GlobalAdapter.APIVar.APIResponse = (GlobalAdapter.APIVar._APIResponse_[0], res_t)
        dumplogger.info("New GlobalAdapter.APIVar._APIResponse_ = %s" % str(GlobalAdapter.APIVar._APIResponse_))

        OK(ret, int(arg['result']), 'ItemBusinessAPI.ReorderModelsList')
