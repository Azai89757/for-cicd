import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCNotification(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCNotification Precondition =="
        dumplogger.info("== Setup VNPCNotification Precondition ==")

    def test_VNPCNotification01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-01.xml")

    def test_VNPCNotification02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-02.xml")

    def test_VNPCNotification03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-03.xml")

    def test_VNPCNotification04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-04.xml")

    def test_VNPCNotification05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-05.xml")

    def test_VNPCNotification06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-06.xml")

    def test_VNPCNotification07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-07.xml")

    def test_VNPCNotification08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-08.xml")

    def test_VNPCNotification09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-09.xml")

    def test_VNPCNotification10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-10.xml")

    def test_VNPCNotification11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-11.xml")

    def test_VNPCNotification12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-12.xml")

    def test_VNPCNotification13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-13.xml")

    def test_VNPCNotification14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-14.xml")

    def test_VNPCNotification15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-15.xml")

    def test_VNPCNotification16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-16.xml")

    def test_VNPCNotification17(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-17.xml")

    def test_VNPCNotification18(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCNotification-18.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCNotification Post condition =="
        dumplogger.info("== Setup VNPCNotification Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
