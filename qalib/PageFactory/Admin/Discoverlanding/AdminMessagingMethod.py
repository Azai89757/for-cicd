#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminMediaStockMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import common library
import Config
import DecoratorHelper
import FrameWorkBase
import Util
import XtFunc

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminBottomBarPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPageButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "cancel"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPageButton.ClickCancel')


class AdminBottomBarPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentTime(arg):
        '''
        ModifyComponentTime : Do FET on homepage component time
                Input argu :
                    start_time - component start display time (if no input, then no action)
                    end_time - component end display time (if no input, then no action)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Do FET of start time and end time
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input Start Time
        if start_time:
            ##Get current date time and add minutes on it
            time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(start_time), 0)
            time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify start time
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
        else:
            Config.dumplogger.info("Do not setting start time.")

        ##Input End Time
        if end_time:
            ##Get current date time and add minutes on it
            time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(end_time), 0)
            time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(end_time), 0)

            ##Modify end date
            xpath = Util.GetXpath({"locate": "end_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify end time
            xpath = Util.GetXpath({"locate": "end_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
        else:
            Config.dumplogger.info("Do not setting end time.")

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifyComponentTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyTabBarType(arg):
        '''
        ModifyTabBarType : Modify tab bar type
                Input argu :
                    tab_bar_type - mall / live_streaming / feeds / video
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_bar_type = arg["tab_bar_type"]

        ##Click tab bar type dropdown
        xpath = Util.GetXpath({"locate": "dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tab bar type dropdown", "result": "1"})

        ##Click choose tab bar type
        xpath = Util.GetXpath({"locate": tab_bar_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose tab bar type -> " + tab_bar_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifyTabBarType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadTabBarImage(arg):
        '''
        UploadTabBarImage : Upload tab bar image
                Input argu :
                    upload_type - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload tab bar image
        xpath = Util.GetXpath({"locate": "upload_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": upload_type, "file_name":file_name, "file_type":file_type, "result":"1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.UploadTabBarImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyTabBarText(arg):
        '''
        ModifyTabBarText : Modify tab bar text
                Input argu :
                    text_en - text in English
                    text_zh - text in Simplified Chinese
                    text_others - text other than English and Simplified Chinese
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text_en = arg["text_en"]
        text_zh = arg["text_zh"]
        text_others = arg["text_others"]

        ##Enter English text if needed
        if text_en:
            xpath = Util.GetXpath({"locate": "text_en_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_en, "result": "1"})

        ##Enter English text if needed
        if text_zh:
            xpath = Util.GetXpath({"locate": "text_zh_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_zh, "result": "1"})

        ##Enter English text if needed
        if text_others:
            xpath = Util.GetXpath({"locate": "text_others_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_others, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifyTabBarText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyUrl(arg):
        '''
        ModifyUrl : Modify url
                Input argu :
                    url - url content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        url = arg["url"]

        ##Url input
        xpath = Util.GetXpath({"locate": "url_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifyUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyUserScope(arg):
        '''
        ModifyUserScope : Modify user scope
                Input argu :
                    ruleset_name - rule set name
                    bundle_name - bundle name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ruleset_name = arg["ruleset_name"]
        bundle_name = arg["bundle_name"]

        ##Click and choose ruleset
        if ruleset_name:
            ##Click ruleset dropdown
            xpath = Util.GetXpath({"locate": "ruleset_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ruleset dropdown", "result": "1"})

            ##Click choose ruleset
            xpath = Util.GetXpath({"locate": "ruleset"})
            xpath = xpath.replace("name_to_be_replaced", ruleset_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose ruleset", "result": "1"})

        ##Click and choose bundle
        if bundle_name:
            ##Click bundle dropdown
            xpath = Util.GetXpath({"locate": "bundle_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle dropdown", "result": "1"})

            ##Click choose bundle
            xpath = Util.GetXpath({"locate": "bundle"})
            xpath = xpath.replace("name_to_be_replaced", bundle_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose bundle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifyUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifySpecificSession(arg):
        '''
        ModifySpecificSession : Modify specific session (for Live Streaming)
                Input argu :
                    type - default / customized
                    user_id - user id
                    plan_id - plan id
                    live_tab_id - live_tab_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        user_id = arg["user_id"]
        plan_id = arg["plan_id"]
        live_tab_id = arg["live_tab_id"]

        ##Click choose session type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose session type -> " + type, "result": "1"})

        ##Input
        if user_id:
            xpath = Util.GetXpath({"locate": "user_id_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_id, "result": "1"})

        if plan_id:
            xpath = Util.GetXpath({"locate": "plan_id_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": plan_id, "result": "1"})

        if live_tab_id:
            xpath = Util.GetXpath({"locate": "live_tab_id_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": live_tab_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifySpecificSession')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyBottomBarMessage(arg):
        '''
        ModifyBottomBarMessage : Modify bottom bar message content
                Input argu :
                    start_time - start time
                    end_time - end time
                    tab_type - tab type
                    upload_type - upload type (file / image)
                    file_name - file name
                    file_type - file type
                    text_en - text in English
                    text_zh - text in Simplified Chinese
                    text_others - text other than English or Simplified Chinese
                    url - url
                    ruleset - ruleset
                    bundle - bundle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        tab_type = arg["tab_type"]
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        text_en = arg["text_en"]
        text_zh = arg["text_zh"]
        text_others = arg["text_others"]
        url = arg["url"]
        ruleset = arg["ruleset"]
        bundle = arg["bundle"]

        ##Input start time and end time
        if start_time or end_time:
            AdminBottomBarPage.ModifyComponentTime({"start_time": start_time, "end_time": end_time, "result": "1"})

        ##Input tab type
        if tab_type:
            AdminBottomBarPage.ModifyTabBarType({"tab_bar_type": tab_type, "result": "1"})

        ##Upload tab bar image
        if upload_type and file_name and file_type:
            AdminBottomBarPage.UploadTabBarImage({"upload_type": upload_type, "file_name": file_name, "file_type": file_type, "result": "1"})

        ##Input tab bar text
        if text_en or text_zh or text_others:
            AdminBottomBarPage.ModifyTabBarText({"text_en": text_en, "text_zh": text_zh, "text_others": text_others, "result": "1"})

        ##Input url
        if url:
            AdminBottomBarPage.ModifyUrl({"url": url, "result": "1"})

        ##Input ruleset and bundle
        if ruleset or bundle:
            AdminBottomBarPage.ModifyUserScope({"ruleset_name": ruleset, "bundle_name": bundle, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBottomBarPage.ModifyBottomBarMessage')
