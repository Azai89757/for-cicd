import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCSellerTxnFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSellerTxnFee Precondition =="
        dumplogger.info("== Setup PHPCSellerTxnFee Precondition ==")

    def test_PHPCSellerTxnFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerTxnFee-01.xml")

    def test_PHPCSellerTxnFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerTxnFee-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSellerTxnFee Post condition =="
        dumplogger.info("== Setup PHPCSellerTxnFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver({})
