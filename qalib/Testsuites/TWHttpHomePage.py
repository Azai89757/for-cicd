import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpHomePage Precondition =="
        dumplogger.info("== Setup TWHttpHomePage Precondition ==")

    def test_TWHttpHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpHomePage-01.xml")

    def test_TWHttpHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpHomePage-02.xml")

    def test_TWHttpHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpHomePage-03.xml")

    def test_TWHttpHomePage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpHomePage-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpHomePage Post condition =="
        dumplogger.info("== Setup TWHttpHomePage Post condition ==")
