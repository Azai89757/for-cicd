import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print("== Setup VNPCCancellation Precondition ==")
        dumplogger.info("== Setup VNPCCancellation Precondition ==")

    def test_VNPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-01.xml")

    def test_VNPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-02.xml")

    def test_VNPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-03.xml")

    def test_VNPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-04.xml")

    def test_VNPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-05.xml")

    # def test_VNPCCancellation06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCCancellation-06.xml")

    def test_VNPCCancellation07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-07.xml")

    def test_VNPCCancellation08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-08.xml")

    def test_VNPCCancellation09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-09.xml")

    def test_VNPCCancellation10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCancellation-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print("== Setup VNPCCancellation Post condition ==")
        dumplogger.info("== Setup VNPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
