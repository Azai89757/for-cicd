import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCheckOut Precondition =="
        dumplogger.info("== Setup THPCCheckOut Precondition ==")

    # def test_THPCCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-01.xml")

    # def test_THPCCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-02.xml")

    # def test_THPCCheckOut03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-03.xml")

    # def test_THPCCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-04.xml")

    # def test_THPCCheckOut05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-05.xml")

    def test_THPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-06.xml")

    def test_THPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-07.xml")

    def test_THPCCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-08.xml")

    def test_THPCCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-09.xml")

    def test_THPCCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-10.xml")

    # def test_THPCCheckOut11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-11.xml")

    # def test_THPCCheckOut12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-12.xml")

    # def test_THPCCheckOut13(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-13.xml")

    # def test_THPCCheckOut14(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-14.xml")

    # def test_THPCCheckOut15(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-15.xml")

    # def test_THPCCheckOut16(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-16.xml")

    # def test_THPCCheckOut17(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-17.xml")

    # def test_THPCCheckOut18(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-18.xml")

    # def test_THPCCheckOut19(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-19.xml")

    def test_THPCCheckOut20(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-20.xml")

    def test_THPCCheckOut21(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-21.xml")

    def test_THPCCheckOut22(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-22.xml")

    def test_THPCCheckOut23(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCheckOut-23.xml")

    # def test_THPCCheckOut24(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-24.xml")

    # def test_THPCCheckOut25(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THPCCheckOut-25.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCheckOut Post condition =="
        dumplogger.info("== Setup THPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
