import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCSignup Precondition =="
        dumplogger.info("== Setup MXPCSignup Precondition ==")

    def test_MXPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCSignup Post condition =="
        dumplogger.info("== Setup MXPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
