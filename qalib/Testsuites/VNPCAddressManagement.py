import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCAddressManagement(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCAddressManagement Precondition =="
        dumplogger.info("== Setup VNPCAddressManagement Precondition ==")

    def test_VNPCAddressManagement01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCAddressManagement-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCAddressManagement Post condition =="
        dumplogger.info("== Setup VNPCAddressManagement Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
