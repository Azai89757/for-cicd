#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 DLAdminAPIMethod.py: The def of this file called by XML mainly.
 DL Admin: Homepage
'''
##Import system library
import json

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class DLAdminAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreDLAdminCookie(arg):
        '''
        StoreDLAdminCookie : Assign DL Admin cookie to API header
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get current country
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "country"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]

        ##Send SQL to get DL admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_dl", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Bulid and Assign cookie to API header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'DLAdminAPI.StoreDLAdminCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadVouchersToShopeeMart(arg):
        '''
        UploadVouchersToShopeeMart : Upload Vouchers to Shopee Mart by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]

        if voucher_type == "seller_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._SellerVoucherList_
        elif voucher_type == "free_shipping_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
        elif voucher_type == "shopee_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_

        for voucher in voucher_list:
            ##Upload vouchers by DL API
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "upload_voucher_to_shopee_mart", "result": "1"})
            GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(voucher["id"])
            DLAdminAPI.StoreDLAdminCookie({"result": "1"})
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            APICommonMethod.CheckAPIResponseText({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'DLAdminAPI.UploadVouchersToShopeeMart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveVouchersFromShopeeMart(arg):
        '''
        RemoveVouchersFromShopeeMart : Remove Vouchers from Shopee Mart by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]

        if voucher_type == "seller_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._SellerVoucherList_
        elif voucher_type == "free_shipping_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
        elif voucher_type == "shopee_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_

        for voucher in voucher_list:
            ##Remove voucher by DL API
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "remove_voucher_from_shopee_mart", "result": "1"})
            GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(voucher["id"])
            DLAdminAPI.StoreDLAdminCookie({"result": "1"})
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'DLAdminAPI.RemoveVouchersFromShopeeMart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetComponentIDList(arg):
        '''
        GetComponentIDList : To store the component_id from Campaign module and new user zone of DL admin.
                Input argu :
                    var_name - the name of the variable which store the column value.
                Return code :
                    1 - success
        '''
        ret = 1
        var_name = arg["var_name"]
        id_list = []

        result = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Go to data > components layer of API response and collect component_id in all components section.
        for component in result["data"]["components"]:
            id_list.append(component["component_id"])

        dumplogger.info("The filtered data is %s" % (id_list))

        ##Store the original list for restoring the order in the end of the test.
        GlobalAdapter.CommonVar._DynamicCaseData_[var_name] = id_list

        OK(ret, int(arg['result']), 'DLAdminAPI.GetComponentIDList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveComponentIDToFirstOrder(arg):
        '''
        MoveComponentIDToFirstOrder : Move a component_id to the first order in campaign module and new user zone of DL admin. And the component_id is defined in request payload.
                Input argu :
                    source - the variable name of reorderd list.
                Return code :
                    1 - success
                    -1 - error
        '''
        ret = 1
        source = arg["source"]
        reordered_list = []
        data = GlobalAdapter.APIVar._HttpPayload_["component_ids"][0]

        ##Get the original order list from global variable.
        reordered_list.extend(GlobalAdapter.CommonVar._DynamicCaseData_[source])
        dumplogger.info("Reorder the list %s" % (reordered_list))

        try:
            ##Move the component_id to the first order of the list.
            reordered_list.remove(data)
            reordered_list.insert(0, data)

            ##Replace the original component_id with reordered one and set to request payload.
            GlobalAdapter.APIVar._HttpPayload_["component_ids"] = reordered_list
            dumplogger.info("Reordered the data into %s" % (GlobalAdapter.APIVar._HttpPayload_["component_ids"]))
        except ValueError:
            dumplogger.info("Encounter ValueError, please check the payload in json file.")
            ret = -1
        except:
            dumplogger.info("Encounter other error.")
            ret = -1

        OK(ret, int(arg['result']), 'DLAdminAPI.MoveComponentIDToFirstOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveComponentIDToLastOrder(arg):
        '''
        MoveComponentIDToLastOrder : Move a component_id to the last order in campaign module and new user zone of DL admin. And the component_id is defined in request payload.
                Input argu :
                    source - the variable name of reorderd list.
                Return code :
                    1 - success
                    -1 - error
        '''
        ret = 1
        source = arg["source"]
        reordered_list = []
        data = GlobalAdapter.APIVar._HttpPayload_["component_ids"][0]

        ##Get the original order list from global variable.
        reordered_list.extend(GlobalAdapter.CommonVar._DynamicCaseData_[source])
        dumplogger.info("Reorder the list %s" % (reordered_list))
        number_of_element = len(reordered_list)

        try:
            ##Move the component_id to the first order of the list.
            reordered_list.remove(data)
            reordered_list.insert(number_of_element - 1, data)

            ##Replace the original component_id with reordered one and set to request payload.
            GlobalAdapter.APIVar._HttpPayload_["component_ids"] = reordered_list
            dumplogger.info("Reordered the data into %s" % (GlobalAdapter.APIVar._HttpPayload_["component_ids"]))
        except ValueError:
            dumplogger.info("Encounter ValueError, please check the payload in json file.")
            ret = -1
        except:
            dumplogger.info("Encounter other error.")
            ret = -1

        OK(ret, int(arg['result']), 'DLAdminAPI.MoveComponentIDToFirstOrder')
