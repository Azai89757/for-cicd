import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCSellerVoucher Precondition =="
        dumplogger.info("== Setup TWPCSellerVoucher Precondition ==")

    def test_TWPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCSellerVoucher-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCSellerVoucher Post condition =="
        dumplogger.info("== Setup TWPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver({})
