import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMePage Precondition =="
        dumplogger.info("== Setup VNPCMePage Precondition ==")

    def test_VNPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMePage-01.xml")

    def test_VNPCMePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMePage-02.xml")

    def test_VNPCMePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMePage-03.xml")

    def test_VNPCMePage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMePage-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMePage Post condition =="
        dumplogger.info("== Setup VNPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
