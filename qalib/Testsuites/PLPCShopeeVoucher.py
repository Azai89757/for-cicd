import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCShopeeVoucher Precondition =="
        dumplogger.info("== Setup PLPCShopeeVoucher Precondition ==")

    def test_PLPCShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCShopeeVoucher-01.xml")

    def test_PLPCShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCShopeeVoucher-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCShopeeVoucher Post condition =="
        dumplogger.info("== Setup PLPCShopeeVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
