#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminShopeeMartMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminShopeeMartVoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyVoucherContent(arg):
        '''
        ModifyVoucherContent : Modify voucher content
                Input argu :
                    promotion_id - voucher promotion id
                    hide_based_on_user_scope - check the checkbox or not (1 / 0 / null)
                    hide_when_fully_redeemed - check the checkbox or not (1 / 0 / null)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_id = arg["promotion_id"]
        hide_based_on_user_scope = arg["hide_based_on_user_scope"]
        hide_when_fully_redeemed = arg["hide_when_fully_redeemed"]

        ##Input promotion id
        if promotion_id:
            xpath = Util.GetXpath({"locate": "promotion_id_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_id, "result": "1"})
            time.sleep(2)

        ##Check or uncheck voucher display mode (hide based on user scope)
        if hide_based_on_user_scope:
            if int(hide_based_on_user_scope):
                xpath = Util.GetXpath({"locate": "hide_based_on_user_scope_unchecked"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check hide based on user scope.", "result": "1"})
            else:
                xpath = Util.GetXpath({"locate": "hide_based_on_user_scope_checked"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click uncheck hide based on user scope.", "result": "1"})
            time.sleep(3)

        ##Check or uncheck voucher display mode (hide when fully redeemed)
        if hide_when_fully_redeemed:
            if int(hide_when_fully_redeemed):
                xpath = Util.GetXpath({"locate": "hide_when_fully_redeemed_unchecked"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check hide when fully redeemed.", "result": "1"})
            else:
                xpath = Util.GetXpath({"locate": "hide_when_fully_redeemed_checked"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click uncheck hide when fully redeemed.", "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopeeMartVoucherPage.ModifyVoucherContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteVoucher(arg):
        '''
        DeleteVoucher : Delete voucher (Expect voucher exist, if not, then error occurs)
                Input argu :
                    section - delete voucher section (active / archived)
                    promotion_id - voucher promotion id
                    post_action - action after click delete (confirm / cancel)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]
        promotion_id = arg["promotion_id"]
        post_action = arg["post_action"]

        ##Click choose section
        xpath = Util.GetXpath({"locate": section})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click section -> " + section, "result": "1"})

        ##Search voucher
        AdminShopeeMartVoucherPage.SearchVoucher({"section": section, "promotion_id": promotion_id, "result": "1"})

        ##Click delete
        xpath = Util.GetXpath({"locate": "delete"})
        xpath = xpath.replace("id_to_be_replaced", promotion_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete voucher -> " + promotion_id, "result": "1"})
        time.sleep(3)

        ##Click choose post action
        xpath = Util.GetXpath({"locate": post_action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose post action -> " + post_action, "result": "1"})
        time.sleep(5)
        BaseUILogic.BrowserRefresh({"message":"Refresh browser to retry", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopeeMartVoucherPage.DeleteVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchVoucher(arg):
        '''
        SearchVoucher : Search voucher
                Input argu :
                    section - voucher section (active / archived)
                    promotion_id - voucher promotion id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]
        promotion_id = arg["promotion_id"]

        ##Click choose section
        xpath = Util.GetXpath({"locate": section})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click section -> " + section, "result": "1"})

        ##Click promotion id search icon
        xpath = Util.GetXpath({"locate": "search_icon"})
        xpath = xpath.replace("section_to_be_replaced", section.capitalize())
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion id search icon", "result": "1"})

        ##Input promotion id
        xpath = Util.GetXpath({"locate": "search_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_id, "result": "1"})
        time.sleep(2)

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminShopeeMartVoucherPage.SearchVoucher')


class AdminShopeeMartCommonPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new
        xpath = Util.GetXpath({"locate": "add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeMartCommonPageButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeMartCommonPageButton.ClickSave')


class AdminHorizontalCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyVisibility(arg):
        '''
        ModifyVisibility : Modify horizontal collection visibility
                Input argu :
                    visibility - 1 / 0
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        visibility = arg["visibility"]

        ##Click toggle of visibility
        if int(visibility):
            xpath = Util.GetXpath({"locate": "visibility_toggle_off"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle on visibility.", "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": "visibility_toggle_on"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle off visibility.", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.ModifyVisibility')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCollectionTitle(arg):
        '''
        ModifyCollectionTitle : Modify horizontal collection title
                Input argu :
                    text_en - text in English
                    text_zh - text in Simplified Chinese
                    text_others - text in language other than English and Simplified Chinese
                    text_color_hex - text color hex
                    enable_display_title - enable display title (1 / 0 / null)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text_en = arg["text_en"]
        text_zh = arg["text_zh"]
        text_others = arg["text_others"]
        text_color_hex = arg["text_color_hex"]
        enable_display_title = arg["enable_display_title"]

        ##Input text in English
        if text_en:
            xpath = Util.GetXpath({"locate": "text_en_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_en, "result": "1"})
            time.sleep(2)

        ##Input text in English
        if text_zh:
            xpath = Util.GetXpath({"locate": "text_zh_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_zh, "result": "1"})
            time.sleep(2)

        ##Input text in English
        if text_others:
            xpath = Util.GetXpath({"locate": "text_others_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_others, "result": "1"})
            time.sleep(2)

        ##Input text color hex
        if text_color_hex:
            xpath = Util.GetXpath({"locate": "text_color_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_color_hex, "result": "1"})
            time.sleep(2)

        ##Enable display title or not
        if enable_display_title:
            if int(enable_display_title):
                xpath = Util.GetXpath({"locate": "display_title_toggle_off"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle on display title.", "result": "1"})
            else:
                xpath = Util.GetXpath({"locate": "display_title_toggle_on"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle off display title.", "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.ModifyCollectionTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyBannerInformation(arg):
        '''
        ModifyBannerInformation : Modify banner information
                Input argu :
                    collection_banner_name - collection banner name
                    collection_banner_type - collection banner type
                    banner_url - banner redirection url
                    landing_banner_name - landing banner name
                    landing_banner_type - landing banner type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_banner_name = arg["collection_banner_name"]
        collection_banner_type = arg["collection_banner_type"]
        banner_url = arg["banner_url"]
        landing_banner_name = arg["landing_banner_name"]
        landing_banner_type = arg["landing_banner_type"]

        ##Upload collection banner
        if collection_banner_name and collection_banner_type:
            xpath = Util.GetXpath({"locate": "collection_banner_upload"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action":"image", "file_name":collection_banner_name, "file_type":collection_banner_type, "result":"1"})
            time.sleep(5)

        ##Input banner url
        if banner_url:
            xpath = Util.GetXpath({"locate": "url_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": banner_url, "result": "1"})
            time.sleep(2)

        ##Upload landing banner
        if landing_banner_name and landing_banner_type:
            xpath = Util.GetXpath({"locate": "landing_banner_upload"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action":"image", "file_name":landing_banner_name, "file_type":landing_banner_type, "result":"1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.ModifyBannerInformation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCollectionType(arg):
        '''
        ModifyCollectionType : Modify collection type
                Input argu :
                    collection_type - collection_type (product_collection / shop_category)
                    collection_id - collection id (for product collection)
                    shop_name - shop name (for shop category)
                    shop_collection - shop collection (for shop category)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_type = arg["collection_type"]
        collection_id = arg["collection_id"]
        shop_name = arg["shop_name"]
        shop_collection = arg["shop_collection"]

        ##Choose collection type and name
        xpath = Util.GetXpath({"locate": collection_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle on visibility.", "result": "1"})
        if collection_type == "product_collection":
            ##Product collection need to input product collection id
            if collection_id:
                xpath = Util.GetXpath({"locate": "collection_id_input"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collection_id, "result": "1"})
                time.sleep(2)

        elif collection_type == "shop_category":
            ##Shop category need to choose shop name and shop collection
            if shop_name:
                xpath = Util.GetXpath({"locate": "shop_name_input"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_name, "result": "1"})
                xpath = Util.GetXpath({"locate": "shop_name_tab"})
                xpath = xpath.replace("name_to_be_replaced", shop_name)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose shop name -> " + shop_name, "result": "1"})
                time.sleep(2)

            if shop_collection:
                xpath = Util.GetXpath({"locate": "shop_collection_input"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_collection, "result": "1"})
                xpath = Util.GetXpath({"locate": "shop_collection_tab"})
                xpath = xpath.replace("name_to_be_replaced", shop_collection)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose shop collection -> " + shop_collection, "result": "1"})
                time.sleep(2)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.ModifyCollectionType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifySchedulingTime(arg):
        '''
        ModifySchedulingTime : Modify scheduling time
                Input argu :
                    adjust_start_time - start time adjust
                    no_end_time - no end time or not (1 / 0 / null)
                    adjust_end_time - end time adjust
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        no_end_time = arg["no_end_time"]
        adjust_end_time = arg["adjust_end_time"]

        ##Adjust start end time
        ##Input Start Time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_start_time), 0)
            time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify start time
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)

        ##Click no end time toggle
        if no_end_time:
            if int(no_end_time):
                xpath = Util.GetXpath({"locate": "no_end_time_toggle_off"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle on no end time.", "result": "1"})
            else:
                xpath = Util.GetXpath({"locate": "no_end_time_toggle_on"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle off no end time.", "result": "1"})
            time.sleep(3)

        ##Adjust end end time
        ##Input End Time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_end_time), 0)
            time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_end_time), 0)

            ##Modify end date
            xpath = Util.GetXpath({"locate": "end_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify end time
            xpath = Util.GetXpath({"locate": "end_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.ModifySchedulingTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCollectionPosition(arg):
        '''
        ChangeCollectionPosition : Change collection position
                Input argu :
                    name - collection name
                    new_position - collection new position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        new_position = arg["new_position"]

        ##Click change position
        xpath = Util.GetXpath({"locate": "change_position_btn"})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change position -> " + name, "result": "1"})
        time.sleep(2)

        ##Input position
        xpath = Util.GetXpath({"locate": "position_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": new_position, "result": "1"})
        time.sleep(2)

        ##Click confirm
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.ChangeCollectionPosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteHorizontalCollection(arg):
        '''
        DeleteHorizontalCollection : Delete horizontal collection
                Input argu :
                    section_type - section type (active / archived)
                    name - collection name
                    post_action - confirm / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]
        name = arg["name"]
        post_action = arg["post_action"]

        ##Click section
        xpath = Util.GetXpath({"locate": section_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change section -> " + section_type, "result": "1"})
        time.sleep(2)

        ##Input click delete btn
        xpath = Util.GetXpath({"locate": "delete"})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete -> " + name, "result": "1"})
        time.sleep(2)

        ##Click choose post action
        xpath = Util.GetXpath({"locate": post_action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose post action -> " + post_action, "result": "1"})
        time.sleep(5)
        BaseUILogic.BrowserRefresh({"message": "Refresh page", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminHorizontalCollectionPage.DeleteHorizontalCollection')
