#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 OrderAdminAPIMethod.py: The def of this file called by XML mainly.
 Order Admin: Update Fulfillment Status
'''
##Import system library
import json
import time

##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class OrderAdminAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreOrderAdminCookie(arg):
        '''
        StoreOrderAdminCookie : Assign Order Admin cookie to API header
                Input argu :
                        N/A
                Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get current country
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "country"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]

        ##Send SQL to get Order admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_order", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Assign cookie to API header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'OrderAdminAPI.StoreOrderAdminCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelReturnForRestore(arg):
        '''
        CancelReturnForRestore : Cancel return by order admin API. This main purpose is restore pending R/R order
                Input argu :
                    seller_name - which seller name you want to cancel return
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_name = arg["seller_name"]

        ##Search orders by seller name from order admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/restore_return_order", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "search_orders", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + "/api/gateway/v1/order_processing/order/query_order_list"
        GlobalAdapter.APIVar._HttpPayload_["seller_name"] = seller_name
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get order search API response
        order_search_api_response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##For loop all seller return search result
        for order_data in order_search_api_response["order_list"]:

            ##Log order id and order BE status
            dumplogger.info("Restore processing order id: %s" % (order_data["order_id"]))
            dumplogger.info("Restore processing BE status: %s" % (order_data["be_status"]["name"]))

            ##Check order BE status is "order_return_processing". BE Status "order_return_processing" means R/R status is pending request
            if order_data["be_status"]["name"] == "order_return_processing":

                ##Search returns by order id from order admin API
                HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/restore_return_order", "result": "1"})
                HttpAPICore.GetAndSetHttpAPIData({"collection": "search_returns", "result": "1"})
                GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + "/api/gateway/v1/return/return_data/get_return_refund_list"
                GlobalAdapter.APIVar._HttpPayload_["filter"]["order_id"] = str(order_data["order_id"])
                GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
                OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
                HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
                APICommonMethod.CheckAPIResponseCode({"result": "1"})

                ##Get return search API response
                return_search_api_response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

                ##Cancel return request by specific return id from order admin API
                HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/restore_return_order", "result": "1"})
                HttpAPICore.GetAndSetHttpAPIData({"collection": "cancel_return_refund_request", "result": "1"})
                GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + "/api/gateway/v1/return/batch_set_return_status"
                GlobalAdapter.APIVar._HttpPayload_["return_ids"].append(return_search_api_response["data"]["return_list"][0]["return_id"])
                GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
                OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
                HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
                APICommonMethod.CheckAPIResponseCode({"result": "1"})

                ##Log cancelled return id
                dumplogger.info("Successful cancel pending R/R order. Return id: %s" % (return_search_api_response["data"]["return_list"][0]["return_id"]))

        ##DeInitial Http Api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'OrderAdminAPI.CancelReturnForRestore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def TriggerLogisticMassCancellation(arg):
        '''
        TriggerLogisticMassCancellation : Trigger logistic mass cancellation in order admin
                Input argu :
                    cancel_type - mass_logistics_cancellation_invalid, mass_logistics_cancellation_request_cancelled
                    cancel_reason_code - cancel reason code you want to setting (802, 801, 204 .....)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cancel_type = arg["cancel_type"]
        cancel_reason_code = arg["cancel_reason_code"]

        ##Initial API and get API basic data from common json
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/logistic_mass_cancellation", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "upload_logistic_mass_cancellation_file", "result": "1"})

        ##Prepare upload logistic mass cancellation API url
        GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + "/api/gateway/v1/order_processing/mass_logistics_cancellation/upload"

        ##Prepare upload logistic mass cancellation API files
        upload_csv_file_data = ('file', ('auto_test_mass_cancel.csv', "orderid,reason_code,cancellation_reason\n%s,%s,auto_test" % (GlobalAdapter.OrderE2EVar._OrderID_, cancel_reason_code), 'text/csv'))
        GlobalAdapter.APIVar._HttpUploadFiles_.append(upload_csv_file_data)

        ##Upload logistic mass cancellation file by order admin API
        OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get upload logistic mass cancellation file API response
        api_response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Initial API and get API basic data from common json
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/logistic_mass_cancellation", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "create_mass_cancellation_job", "result": "1"})

        ##Prepare mass cancellation job API url
        GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + "/api/gateway/v1/order_processing/" + cancel_type + "/create_job"

        ##Prepare mass cancellation job API payload
        GlobalAdapter.APIVar._HttpPayload_["filename"] = api_response["data"]["filename"]
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Create mass cancellation job by order admin API
        OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'OrderAdminAPI.TriggerLogisticMassCancellation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddMaintenanceSchedule(arg):
        '''
        AddMaintenanceSchedule : Add payment maintenance schedule in order admin
                Input argu :
                    channel_id - payment channel id
                    custom_message - maintenance message
                    message_localized - maintenance message (localized)
                    comment - setting comment
                    start_time - start time
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel_id = arg["channel_id"]
        custom_message = arg["custom_message"]
        message_localized = arg["message_localized"]
        comment = arg["comment"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Initial API and get API basic data from common json
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/add_payment_maintenance", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "add_payment_maintenance", "result": "1"})

        ##Prepare add maintenenace API url
        GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + GlobalAdapter.APIVar._HttpUrl_

        ##Prepare add maintenenace API payload
        start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0, is_tw_time=1)
        start_timestamp = int(time.mktime(time.strptime(start_datetime, "%Y-%m-%d %H:%M")))
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["start_time"] = start_timestamp
        end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0, is_tw_time=1)
        end_timestamp = int(time.mktime(time.strptime(end_datetime, "%Y-%m-%d %H:%M")))
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["end_time"] = end_timestamp
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["channel_id"] = int(channel_id)
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["cm_message"] = custom_message
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["message_localized"] = message_localized
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["comment"] = comment
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Create add maintenenace by order admin API
        OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'OrderAdminAPI.AddMaintenanceSchedule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeactivateMaintenanceSchedule(arg):
        '''
        DeactivateMaintenanceSchedule : Deactivate payment maintenance schedule in order admin
                Input argu :
                    comment - setting comment
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        comment = arg["comment"]

        ##Get payment maintenenace by order admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/deactivate_payment_maintenance", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_payment_maintenance", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + GlobalAdapter.APIVar._HttpUrl_
        OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        APICommonMethod.StoreDataFromAPIResponse({"column": "id", "result": "1"})
        dumplogger.info("GlobalAdapter.CommonVar._DynamicCaseData_ = %s" % GlobalAdapter.CommonVar._DynamicCaseData_)

        ##Deactivate payment maintenenace by order admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/order/deactivate_payment_maintenance", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "deactivate_payment_maintenance", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = "https://order-admin." + GlobalAdapter.UrlVar._Domain_ + GlobalAdapter.APIVar._HttpUrl_
        OrderAdminAPI.StoreOrderAdminCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["id"] = GlobalAdapter.CommonVar._DynamicCaseData_["id"]
        GlobalAdapter.APIVar._HttpPayload_["schedule"]["comment"] = comment
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'OrderAdminAPI.DeactivateMaintenanceSchedule')
