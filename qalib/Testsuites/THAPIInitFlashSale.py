import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THAPIInitFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIInitFlashSale Precondition =="
        dumplogger.info("== Setup THAPIInitFlashSale Precondition ==")

    def test_THAPIInitFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/THAPIInitFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIInitFlashSale Post condition =="
        dumplogger.info("== Setup THAPIInitFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
