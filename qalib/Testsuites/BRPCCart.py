import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCCart Precondition =="
        dumplogger.info("== Setup BRPCCart Precondition ==")

    def test_BRPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCCart Post condition =="
        dumplogger.info("== Setup BRPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
