import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCFlashSale Precondition =="
        dumplogger.info("== Setup COPCFlashSale Precondition ==")

    def test_COPCFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCFlashSale-01.xml")

    def test_COPCFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCFlashSale-02.xml")

    def test_COPCFlashSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCFlashSale-03.xml")

    def test_COPCFlashSale04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCFlashSale-04.xml")

    def test_COPCFlashSale05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCFlashSale-05.xml")

    def test_COPCFlashSale06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCFlashSale-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCFlashSale Post condition =="
        dumplogger.info("== Setup COPCFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
