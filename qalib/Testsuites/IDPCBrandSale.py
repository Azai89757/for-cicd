import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCBrandSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCBrandSale Precondition =="
        dumplogger.info("== Setup IDPCBrandSale Precondition ==")

    def test_IDPCBrandSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCBrandSale-01.xml")

    def test_IDPCBrandSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCBrandSale-02.xml")

    def test_IDPCBrandSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCBrandSale-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCBrandSale Post condition =="
        dumplogger.info("== Setup IDPCBrandSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
