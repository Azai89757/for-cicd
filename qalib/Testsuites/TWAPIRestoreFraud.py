import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWAPIRestoreFraud(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIRestoreFraud Precondition =="
        dumplogger.info("== Setup TWAPIRestoreFraud Post condition")

    def test_TWAPIRestoreFraud001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWAPIRestoreFraud-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIRestoreFraud Post condition =="
        dumplogger.info("== Setup TWAPIRestoreFraud Post condition")
