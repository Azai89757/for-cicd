import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNAndroidShopeePublicVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidShopeePublicVoucher Precondition =="
        dumplogger.info("== Setup VNAndroidShopeePublicVoucher Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"vn"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_VNAndroidShopeePublicVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-01.xml")

    def test_VNAndroidShopeePublicVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-02.xml")

    def test_VNAndroidShopeePublicVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-03.xml")

    def test_VNAndroidShopeePublicVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-04.xml")

    def test_VNAndroidShopeePublicVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-05.xml")

    def test_VNAndroidShopeePublicVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-06.xml")

    def test_VNAndroidShopeePublicVoucher07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-07.xml")

    def test_VNAndroidShopeePublicVoucher08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-08.xml")

    def test_VNAndroidShopeePublicVoucher09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-09.xml")

    def test_VNAndroidShopeePublicVoucher10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-10.xml")

    def test_VNAndroidShopeePublicVoucher11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-11.xml")

    def test_VNAndroidShopeePublicVoucher12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidShopeePublicVoucher-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidShopeePublicVoucher Post condition =="
        dumplogger.info("== Setup VNAndroidShopeePublicVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        #Kill Appium
        KillAppium({"isFail":"0"})
