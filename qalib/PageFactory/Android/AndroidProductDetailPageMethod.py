#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidProductDetailPegeMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger
import AndroidLoginSignUpMethod

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidProductDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DirectLogin(arg):
        '''
        DirectLogin : Direct login when redirect to login page in PDP
                Input argu :
                    account - login account
                    password - login password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]
        password = arg["password"]

        ##Input account and password
        AndroidLoginSignUpMethod.AndroidLoginPage.InputAccount({"account": account, "result": "1"})
        AndroidLoginSignUpMethod.AndroidLoginPage.InputPassword({"password": password, "page_type": "login_page", "result": "1"})

        ##Click login button
        AndroidLoginSignUpMethod.AndroidLoginSignupButton.ClickConfirm({"page_type": "login_page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.DirectLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLikeProduct(arg):
        '''
        ClickLikeProduct : Click like product in product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"like_product"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click like product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.ClickLikeProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LongClickLikeProduct(arg):
        '''
        LongClickLikeProduct : Long click like product in product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"like_product"})
        AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.LongClickLikeProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddProductToCart(arg):
        '''
        AddProductToCart : Click add product to cart in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Swipe down to the button of the page
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"4", "result": "1"})

        ##Check if chat icon exists
        xpath = Util.GetXpath({"locate":"add_cart"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add to cart", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.AddProductToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeModelQuantity(arg):
        '''
        ChangeModelQuantity : Change increase/decrease quantity in pdp
                Input argu :
                    mode - increase / decrease
                    times - increase or decrease time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mode = arg["mode"]
        times = arg["times"]

        ##Increase or decrease quantity for some times
        for action_time in range(int(times)):
            xpath = Util.GetXpath({"locate": mode + "_quantity"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click " + mode + " icon", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.ChangeModelQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EnterVideoFullScreen(arg):
        '''
        EnterVideoFullScreen : Enter video full screen
                Input argu :N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click video two times (To enter full screen)
        xpath = Util.GetXpath({"locate":"video_tag"})

        ##Click for running video
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click video icon", "result": "1"})

        ##Click for expend video to full screen
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click video icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.EnterVideoFullScreen')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToFreeGiftSection(arg):
        '''
        MoveToFreeGiftSection : Move to free gift section
                Input argu :
                    free_gift_name - free gift name in PDP
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        free_gift_name = arg["free_gift_name"]

        ##Move to free gift section
        xpath = Util.GetXpath({"locate":"free_gift_text"})
        xpath = xpath.replace("name_to_be_replaced", free_gift_name)
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "result":"1"})
        AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":"6", "locate_y":"800", "movex":"6", "movey":"600", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.MoveToFreeGiftSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipeProductImage(arg):
        '''
        SwipeProductImage : Swipe between product images
                Input argu :
                    direction - right / left
                    times - swipe time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        direction = arg["direction"]
        times = arg["times"]

        ##Get product image block to swipe
        xpath = Util.GetXpath({"locate":"product_img"})
        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":direction, "times":times, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.SwipeProductImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClosePannel(arg):
        '''
        ClosePannel : Close pannel
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click grey section to close pannel when there is a pannel on product detail page
        AndroidBaseUICore.AndroidClickCoordinates({"x_cor":50, "y_cor":150})

        OK(ret, int(arg['result']), 'AndroidProductDetailPage.ClosePannel')


class AndroidAddOnDealSectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnItem(arg):
        '''
        ClickAddOnItem : Click add on deal section item and go to pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal section item
        xpath = Util.GetXpath({"locate": "add_on_item"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add on item", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionPage.ClickAddOnItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainItem(arg):
        '''
        ClickMainItem : Click add on deal section main item and go to pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal section main item
        xpath = Util.GetXpath({"locate": "main_item"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click main item", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionPage.ClickMainItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckbox(arg):
        '''
        ClickCheckbox : Click add on item checkbox
                Input argu :
                    product_name - product name which you will choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click add on item checkbox
        xpath = Util.GetXpath({"locate": "addon_item_checkbox"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add on item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionPage.ClickCheckbox')


class AndroidAddOnDealSectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDown(arg):
        '''
        ClickDropDown : Click add on deal drop down btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click drop down btn
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click drop down btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionButton.ClickDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductModelDropDown(arg):
        '''
        ClickProductModelDropDown : click product model drop down menu btn
                Input argu :
                    product_name - product name which you will click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product model drop down menu btn
        xpath = Util.GetXpath({"locate": "drop_down_menu_btn"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click drop down menu btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionButton.ClickProductModelDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseProductModelWindow(arg):
        '''
        ClickCloseProductModelWindow : Click to close product model window
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to close product model window
        xpath = Util.GetXpath({"locate": "close_window"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to close product model window", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionButton.ClickCloseProductModelWindow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewMore(arg):
        '''
        ClickViewMore : Click add on deal view more btn
                Input argu :
                    button_type - add_on_deal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Check view more btn
        time.sleep(5)
        xpath = Util.GetXpath({"locate": button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click view more btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionButton.ClickViewMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add on deal add to cart btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal add to cart btn
        xpath = Util.GetXpath({"locate": "add_to_cart_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add on deal add to cart btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealSectionButton.ClickAddToCart')


class AndroidModelSelectPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddProductToCart(arg):
        '''
        AddProductToCart : Add product to cart from model page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share link icon
        xpath = Util.GetXpath({"locate":"add_cart"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add to cart", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidModelSelectPage.AddProductToCart')


class AndroidVariationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickModifyVariationQuantity(arg):
        '''
        ClickModifyVariationQuantity : Click increase/decrease quantity in variation page
                Input argu :
                    action - increase / decrease
                    quantity - increase / decrease quantity
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        quantity = arg["quantity"]

        ##Do action for quantity of time
        for action_time in range(int(quantity)):
            xpath = Util.GetXpath({"locate": action})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click " + action + " quantity", "result": "1"})
            time.sleep(4)
            dumplogger.info("%s time : %s" % (action, action_time))

        OK(ret, int(arg['result']), 'AndroidVariationPage.ClickModifyVariationQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationQuantity(arg):
        '''
        InputVariationQuantity : Input quantity of variation page directly
                Input argu :
                    quantity - quantity of item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        quantity = arg["quantity"]

        ##Input quantity
        xpath = Util.GetXpath({"locate":"quantity_input"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string": quantity, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVariationPage.InputVariationQuantity')


class AndroidVariationPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Click buy now in variation page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy now
        xpath = Util.GetXpath({"locate":"buy_now_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click buy now", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVariationPageButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart in variation page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add to cart btn
        xpath = Util.GetXpath({"locate": "add_to_cart_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add to cart btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidVariationPageButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseVariation(arg):
        '''
        ClickChooseVariation : Click choose variation in variation page
                Input argu :
                    category - variation category
                    name - variation name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category = arg["category"]
        name = arg["name"]

        ##Click choose variation name
        xpath = Util.GetXpath({"locate":"variation_type"})
        xpath = xpath.replace("name_to_be_replaced", name)
        xpath = xpath.replace("category_to_be_replaced", category)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose variation -> " + name + " in " + category, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidVariationPageButton.ClickChooseVariation')


class AndroidProductDetailButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewSellerShop(arg):
        '''
        ClickViewSellerShop : Click to seller shop from pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to seller shop
        xpath = Util.GetXpath({"locate":"go_to_shop"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"method":"xpath", "locate": xpath, "direction":"down", "isclick":"0", "result": "1"})
        AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":"6", "locate_y":"800", "movex":"6", "movey":"600", "result": "1"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click go to seller shop", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickViewSellerShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewMyLikesPage(arg):
        '''
        ClickViewMyLikesPage : Click to My Likes page from pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to My Likes page
        xpath = Util.GetXpath({"locate":"view_my_likes"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to My Likes page", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickViewMyLikesPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareProductLink(arg):
        '''
        ClickShareProductLink : Click share link btn in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share link icon
        xpath = Util.GetXpath({"locate":"share_link_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click share link icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickShareProductLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToCart(arg):
        '''
        ClickToCart : Click cart icon in top bar of pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart icon in top bar
        xpath = Util.GetXpath({"locate":"cart_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cart icon in top bar", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDownMenu(arg):
        '''
        ClickDropDownMenu : Click menu icon in top bar of pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click menu icon in top bar
        xpath = Util.GetXpath({"locate":"menu_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click menu icon in top bar", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickDropDownMenu')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingFeeDropDown(arg):
        '''
        ClickShippingFeeDropDown : Click shipping fee drop down in product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click shipping fee drop down
        xpath = Util.GetXpath({"locate":"ship_fee_drop_down"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click shipping fee drop down", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickShippingFeeDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditProduct(arg):
        '''
        ClickEditProduct : Click menu icon in top bar of pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click menu icon in top bar
        xpath = Util.GetXpath({"locate":"edit_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit product icon in top bar", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickEditProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetVoucher(arg):
        '''
        ClickGetVoucher : Click get voucher if there's any in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click get voucher button in PDP
        xpath = Util.GetXpath({"locate":"get_shop_voucher"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click get voucher button in PDP", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickGetVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleDealSeeMore(arg):
        '''
        ClickBundleDealSeeMore : Click bundle deal see more btn in bundle deal section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bundle deal see more btn in bundle deal section
        xpath = Util.GetXpath({"locate": "bundle_deal_see_more"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickBundleDealSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogisticSection(arg):
        '''
        ClickLogisticSection : Click logistic section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click logistic section
        xpath = Util.GetXpath({"locate": "logistic_section"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click logistic section", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickLogisticSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemindMe(arg):
        '''
        ClickRemindMe : Click remind me btn in deep discount section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click remind me btn in deep discount section
        xpath = Util.GetXpath({"locate": "remind_me_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click remind me btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelRemindMe(arg):
        '''
        ClickCancelRemindMe : Click cancel remind me btn in deep discount section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel remind me btn in deep discount section
        xpath = Util.GetXpath({"locate": "cancel_remind_me_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cancel remind me btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickCancelRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Click direct buy in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy now button
        xpath = Util.GetXpath({"locate": "direct_buy_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click buy now button", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupBuyNow(arg):
        '''
        ClickPopupBuyNow : Click direct buy on pop up model message in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pop up buy now button
        xpath = Util.GetXpath({"locate": "pop_up_direct_buy_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click pop up buy now button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickPopupBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        '''
        ClickChatIcon : Click chat icon in pdp
                Input argu :
                    page_type - pdp / buyer_gallery
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click chat icon
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click chat icon -> " + page_type, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickChatIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart button on product detail page
                Input argu :N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add to cart button
        xpath = Util.GetXpath({"locate":"cart_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add to cart btn on product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductModel(arg):
        '''
        ChooseProductModel : Click to choose product mode on product detail page
                Input argu :
                    name - product model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click add to cart button
        xpath = Util.GetXpath({"locate":"product_mode"})
        xpath = xpath.replace("product_name_for_replace", name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose product mode on product detail page", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ChooseProductModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackButton(arg):
        '''
        ClickBackButton : Click back btn in top bar in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check if chat icon exists
        xpath = Util.GetXpath({"locate":"back_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click share link icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickBackButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleDealSection(arg):
        '''
        ClickBundleDealSection : Click bundle deal section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bundle deal section
        xpath = Util.GetXpath({"locate":"bd_section"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bundle deal section", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickBundleDealSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFreeGiftSeeMore(arg):
        '''
        ClickFreeGiftSeeMore : Click see more in free gift section
                Input argu :
                    free_gift_name - free gift name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        free_gift_name = arg["free_gift_name"]

        ##Click free gift see more btn
        xpath = Util.GetXpath({"locate":"free_gift_see_more"})
        xpath = xpath.replace("name_to_be_replaced", free_gift_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click free gift see more", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickFreeGiftSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickWholesale(arg):
        '''
        ClickWholesale : Click wholesale UI to wholesale detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click wholesale UI
        xpath = Util.GetXpath({"locate":"wholesale_section"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction":"down", "isclick":"1", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickWholesale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInstallmentSeeMore(arg):
        '''
        ClickInstallmentSeeMore : Click installment see more btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click installment see more btn
        xpath = Util.GetXpath({"locate":"installment_see_more"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click installment see more btn", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickInstallmentSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseAdultPopup(arg):
        '''
        ClickChooseAdultPopup : Click choose if user is over 18
                Input argu :
                    is_adult : 1 / 0
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        is_adult = arg["is_adult"]

        ##Click choose adult popup
        if int(is_adult):
            xpath = Util.GetXpath({"locate":"is_adult"})
        else:
            xpath = Util.GetXpath({"locate":"not_adult"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click if user is 18up", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickChooseAdultPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCartIcon(arg):
        '''
        ClickCartIcon : Click cart icon in buyer gallery page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart icon
        xpath = Util.GetXpath({"locate":"cart_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cart icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeLessContent(arg):
        '''
        ClickSeeLessContent : Click see less btn (buyer gallery content page)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see less icon
        xpath = Util.GetXpath({"locate":"see_less_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see less button.", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickSeeLessContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseMore(arg):
        '''
        ClickChooseMore : Click choose more btn (in more section popup)
                Input argu :
                    type - help / home / report
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click see less icon
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click more button -> " + type, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickChooseMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChoosePreferredPlusOption(arg):
        '''
        ClickChoosePreferredPlusOption : Click choose preferred plus popup option
                Input argu :
                    option - ok / read_more
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click choose preferred plus popup option
        xpath = Util.GetXpath({"locate":option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose preferred plus option -> " + option, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickChoosePreferredPlusOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyerGalleryCartIcon(arg):
        '''
        ClickBuyerGalleryCartIcon : Click buyer gallery cart icon in buyer gallery page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buyer gallery cart icon in buyer gallery page
        xpath = Util.GetXpath({"locate":"cart_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cart icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickBuyerGalleryCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more btn in product detail section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see more btn in product detail section
        xpath = Util.GetXpath({"locate": "see_more_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click see more btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserGroupUrl(arg):
        '''
        ClickUserGroupUrl : Click user group url
                Input argu :
                    url_text - url text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        url_text = arg["url_text"]

        ##Click user group url
        xpath = Util.GetXpath({"locate": "user_group_url"})
        xpath = xpath.replace("text_to_be_replaced", url_text)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click user group url", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickUserGroupUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok btn in wholesale detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button in whole sale detail page
        xpath = Util.GetXpath({"locate":"whole_sale_ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ok button in wholesale detail", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductDetailButton.ClickOK')
