import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCCancellation Precondition =="
        dumplogger.info("== Setup BRPCCancellation Precondition ==")

    def test_BRPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCancellation-01.xml")

    def test_BRPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCancellation-02.xml")

    def test_BRPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCancellation-03.xml")

    def test_BRPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCancellation-04.xml")

    def test_BRPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCCancellation Post condition =="
        dumplogger.info("== Setup BRPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
