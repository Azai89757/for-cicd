import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpMallPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpMallPage Precondition =="
        dumplogger.info("== Setup TWHttpMallPage Precondition ==")

    def test_TWHttpMallPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpMallPage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpMallPage Post condition =="
        dumplogger.info("== Setup TWHttpMallPage Post condition ==")
