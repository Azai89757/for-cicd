import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCCommissionFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCommissionFee Precondition =="
        dumplogger.info("== Setup THPCCommissionFee Precondition ==")

    def test_THPCCommissionFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCommissionFee-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCommissionFee Post condition =="
        dumplogger.info("== Setup THPCCommissionFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
