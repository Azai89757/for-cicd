import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCPurchaseWithPurchase(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCPurchaseWithPurchase Precondition =="
        dumplogger.info("== Setup MYPCPurchaseWithPurchase Precondition ==")

    def test_MYPCPurchaseWithPurchase01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCPurchaseWithPurchase-01.xml")

    def test_MYPCPurchaseWithPurchase02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCPurchaseWithPurchase-02.xml")

    def test_MYPCPurchaseWithPurchase03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCPurchaseWithPurchase-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCPurchaseWithPurchase Post condition =="
        dumplogger.info("== Setup MYPCPurchaseWithPurchase Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
