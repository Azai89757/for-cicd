import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCMission(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMission Precondition =="
        dumplogger.info("== Setup IDPCMission Precondition ==")

    def test_IDPCMission01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMission-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMission Post condition =="
        dumplogger.info("== Setup IDPCMission Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
