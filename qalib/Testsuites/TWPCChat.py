import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCChat(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCChat Precondition =="
        dumplogger.info("== Setup TWPCChat Precondition ==")

    def test_TWPCChat01(self):
        try:
            if isMyCase(self.id()) is False:
                return
            XMLParser("/xml/ui/TW/TWPCChat-01.xml")
        except:
            print traceback.print_exc()

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCChat Post condition =="
        dumplogger.info("== Setup TWPCChat Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
