#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SharingPanelMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class SharingPanelPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LaunchSocialMediaWebSite(arg):
        '''
        LaunchSocialMediaWebSite : Launch social media website
                Input argu :
                    sharepage - facebook / pinterest / twitter
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        sharepage = arg["sharepage"]
        ret = 1

        ##Define url to launch website
        url = "https://www." + sharepage + ".com/login"
        dumplogger.info("url = %s" % (url))

        ##Go to url
        BaseUICore.GotoURL({"url":url, "result": "1"})

        OK(ret, int(arg['result']), 'SharingPanelPage.LaunchSocialMediaWebSite')


class SharingPanelPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSocialMediaButton(arg):
        '''
        ClickSocialMediaButton : Click social media button
                Input argu :
                    social_media_type - messenger / facebook / pinterest / twitter
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        social_media_type = arg["social_media_type"]
        ret = 1

        ##Click social media button
        xpath = Util.GetXpath({"locate": social_media_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + social_media_type + " button.", "result": "1"})

        OK(ret, int(arg['result']), 'SharingPanelPageButton.ClickSocialMediaButton')


class FacebookSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FacebookLogin(arg):
        '''
        FacebookLogin : Login facebook
                Input argu :
                    account - facebook account
                    password - facebook password
                    page_type - share_page_login / website_login
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        page_type = arg["page_type"]
        ret = 1

        ##Input facebook account and password
        xpath = Util.GetXpath({"locate": "fb_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        xpath = Util.GetXpath({"locate": "fb_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        ##Click log in
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Facebook login", "result": "1"})

        OK(ret, int(arg['result']), 'FacebookSharingPage.FacebookLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareWithMessenger(arg):
        '''
        ClickShareWithMessenger : Click share with messenger
                Input argu :
                    receiver - receiver name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        receiver = arg["receiver"]
        ret = 1

        ##Input receiver
        xpath = Util.GetXpath({"locate": "receiver_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": receiver, "result": "1"})

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Click send
        xpath = Util.GetXpath({"locate": "send_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click send button.", "result": "1"})

        OK(ret, int(arg['result']), 'FacebookSharingPage.ClickShareWithMessenger')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareWithFacebook(arg):
        '''
        ClickShareWithFacebook : Click share with facebook
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share
        xpath = Util.GetXpath({"locate": "share_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click share button.", "result": "1"})

        ##Switch back to the first window
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "1", "result": "1"})

        OK(ret, int(arg['result']), 'FacebookSharingPage.ClickShareWithFacebook')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareLinkToEnterPDP(arg):
        '''
        ClickShareLinkToEnterPDP : Click share link to enter product detail page
                Input argu :
                    page_type - facebook / messenger
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click share link
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click share link", "result": "1"})

        OK(ret, int(arg['result']), 'FacebookSharingPage.ClickShareLinkToEnterPDP')

class FacebookSharingPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFacebook(arg):
        '''
        ClickFacebook : Click facebook icon to enter facebook page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click facebook icon
        xpath = Util.GetXpath({"locate": "facebook_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click facebook button", "result": "1"})

        OK(ret, int(arg['result']), 'FacebookSharingPageButton.ClickFacebook')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMePage(arg):
        '''
        ClickMePage : Click mepage in facebook
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mepage in facebook
        xpath = Util.GetXpath({"locate": "mepage_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mepage in facebook", "result": "1"})

        OK(ret, int(arg['result']), 'FacebookSharingPageButton.ClickMePage')

class PinterestSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PinterestLogin(arg):
        '''
        PinterestLogin : Login pinterest
                Input argu :
                    account - pinterest account
                    password - pinterest password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        ret = 1

        ##Click pinterest login homepage button
        xpath = Util.GetXpath({"locate": "pinterest_login_homepage"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click login btn", "result": "1"})

        ##Input pinterest account and password
        xpath = Util.GetXpath({"locate": "pinterest_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})
        xpath = Util.GetXpath({"locate": "pinterest_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        ##Click login button
        xpath = Util.GetXpath({"locate": "login_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login button", "result": "1"})

        OK(ret, int(arg['result']), 'PinterestSharingPage.PinterestLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareWithPinterest(arg):
        '''
        ClickShareWithPinterest : Click share with pinterest
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exist board to share
        xpath = Util.GetXpath({"locate": "board"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click profile button", "result": "1"})

        OK(ret, int(arg['result']), 'PinterestSharingPage.ClickShareWithPinterest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLinkToEnterPDP(arg):
        '''ClickLinkToEnterPDP : Click share link to enter product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click profile button
        xpath = Util.GetXpath({"locate": "profile_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click profile button", "result": "1"})

        ##Click board button
        xpath = Util.GetXpath({"locate": "board_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click board button", "result": "1"})

        ##Click share picture
        xpath = Util.GetXpath({"locate": "share_picture"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click share picture", "result": "1"})

        ##Click other space
        xpath = Util.GetXpath({"locate": "other_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click other space", "result": "1"})

        ##Click share link
        xpath = Util.GetXpath({"locate": "share_link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click share link", "result": "1"})

        OK(ret, int(arg['result']), 'PinterestSharingPage.ClickLinkToEnterPDP')


class TwitterSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def TwitterLogin(arg):
        '''
        TwitterLogin : Login twitter
                Input argu :
                    account - twitter account
                    password - twitter password
                    page_type - share_page_login / website_login
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        account = arg["account"]
        password = arg["password"]
        page_type = arg["page_type"]
        ret = 1

        ##Input twitter account
        xpath = Util.GetXpath({"locate": "twitter_email"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click twitter email textbox to focus", "result": "1"})
        xpath = Util.GetXpath({"locate": "twitter_email"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": account, "result": "1"})

        if page_type == "website_login":

            ##Click next step button
            xpath = Util.GetXpath({"locate": "next_step_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next step button", "result": "1"})

        ##Input twitter password
        xpath = Util.GetXpath({"locate": "twitter_password"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click twitter password to focus", "result": "1"})
        xpath = Util.GetXpath({"locate": "twitter_password"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": password, "result": "1"})

        ##Click twitter login button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login button", "result": "1"})

        OK(ret, int(arg['result']), 'TwitterSharingPage.TwitterLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareWithTwitter(arg):
        '''
        ClickShareWithTwitter : Click share with twitter
                Input argu :
                    message - share message content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message = arg["message"]
        ret = 1

        ##Input share message
        xpath = Util.GetXpath({"locate": "message_content"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": message, "result": "1"})

        ##Click tweet to share message
        xpath = Util.GetXpath({"locate": "share_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click share button.", "result": "1"})

        ##Switch back to the first window
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "1", "result": "1"})

        OK(ret, int(arg['result']), 'TwitterSharingPage.ClickShareWithTwitter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLinkToEnterPDP(arg):
        '''
        ClickLinkToEnterPDP : Click share link to enter product detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click profile button
        xpath = Util.GetXpath({"locate": "profile_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click profile button", "result": "1"})

        ##Click share link
        xpath = Util.GetXpath({"locate": "share_link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click share link", "result": "1"})

        OK(ret, int(arg['result']), 'TwitterSharingPage.ClickLinkToEnterPDP')


class TwitterSharingPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteButton(arg):
        '''
        ClickDeleteButton : Click delete button to delete share link
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click more button
        xpath = Util.GetXpath({"locate": "more_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click more button", "result": "1"})

        ##Click delete button
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        ##Click confirm delete button
        xpath = Util.GetXpath({"locate": "confirm_delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm delete button", "result": "1"})

        OK(ret, int(arg['result']), 'TwitterSharingPageButton.ClickDeleteButton')
