import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCCart Precondition =="
        dumplogger.info("== Setup PLPCCart Precondition ==")

    def test_PLPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCCart Post condition =="
        dumplogger.info("== Setup PLPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
