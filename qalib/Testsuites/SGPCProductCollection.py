import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCProductCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCProductCollection Precondition =="
        dumplogger.info("== Setup SGPCProductCollection Precondition ==")

    def test_SGPCProductCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-01.xml")

    def test_SGPCProductCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-02.xml")

    def test_SGPCProductCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-03.xml")

    def test_SGPCProductCollection04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-04.xml")

    def test_SGPCProductCollection05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-05.xml")

    def test_SGPCProductCollection06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-06.xml")

    def test_SGPCProductCollection07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCollection-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCProductCollection Post condition =="
        dumplogger.info("== Setup SGPCProductCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
