import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSellerDiscount Precondition =="
        dumplogger.info("== Setup BRPCSellerDiscount Precondition ==")

    def test_BRPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerDiscount-01.xml")

    def test_BRPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerDiscount-02.xml")

    def test_BRPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerDiscount-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSellerDiscount Post condition =="
        dumplogger.info("== Setup BRPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
