import unittest
from Config import dumplogger
from FrameWorkBase import *


class THAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup THAPIPlaceOrder Precondition ==")

    def test_THAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/THAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup THAPIPlaceOrder Post condition ==")
