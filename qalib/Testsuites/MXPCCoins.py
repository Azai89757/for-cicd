import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class MXPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCoins Precondition =="
        dumplogger.info("== Setup MXPCCoins Precondition ==")

    def test_MXPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-01.xml")

    def test_MXPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-02.xml")

    def test_MXPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-03.xml")

    def test_MXPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-04.xml")

    def test_MXPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-05.xml")

    def test_MXPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-06.xml")

    def test_MXPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCoins-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCoins Post condition =="
        dumplogger.info("== Setup MXPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
