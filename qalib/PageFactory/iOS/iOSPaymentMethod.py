﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSPaymentMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import DecoratorHelper

##Import ios library
import iOSBaseUICore
import iOSBaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class iOSPaymentButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' ClickConfirm : Click confirm delete account button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click edit
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result":"1"})

        OK(ret, int(arg['result']), 'iOSPaymentButton.ClickConfirm')


class iOSPaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectPaymentMethod(arg):
        '''SelectPaymentMethod : Select a payment method in payment method page
                Input argu : payment - payment type
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        payment = arg["payment"]

        ##Click payment method
        locate = Util.GetXpath({"locate":payment})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Select a payment method in payment method page", "result":"1"})
        time.sleep(10)

        ##If using credit card to pay, need to select one credit card
        if payment == 'credit_card':
            #card_number = "*0002"

            locate = Util.GetXpath({"locate":"credit_card_number"})
            #xpath = xpath.replace("card_number_to_be_replace", card_number)
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click credit card number", "result":"1"})
            time.sleep(2)

        elif payment == 'ibanking':
            ##Choose NCB
            locate = Util.GetXpath({"locate":"ncb"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Choose NCB", "result":"1"})

        elif payment == "cc_installment":
            ##Choose 6x installment
            locate = Util.GetXpath({"locate":"6x"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click a 6x installment on Checkout page.", "result":"1"})

        elif payment == "airpay":
            ##Check airpay option is expand
            locate = Util.GetXpath({"locate":"airpay_expand"})
            if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"}):
                dumplogger.info("airpay option is expanded")
            else:
                dumplogger.info("airpay option is not expanded, click to expand")
                locate = Util.GetXpath({"locate":"airpaywallet"})
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click card on Checkout page.", "result":"1"})

        elif payment == 'airpay_bt':
            ##Check airpay option is expand
            locate = Util.GetXpath({"locate":"airpay_expand"})
            if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"}):
                dumplogger.info("airpay option is expanded")
            else:
                dumplogger.info("airpay option is not expanded, click to expand")
                ##Choose CC card
                locate = Util.GetXpath({"locate":"airpaywallet_bk"})
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click card on Checkout page.", "result":"1"})

        OK(ret, int(arg['result']), 'iOSPaymentPage.SelectPaymentMethod')


class iOSChoosePaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        ''' ClickOk : Click ok on choose payment page
                Input argu :N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click pay btn on credit card 3PP page
        locate = Util.GetXpath({"locate":"ok_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click ok btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSChoosePaymentPage.ClickOk')


class iOSAirPayPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AirPayNavigation(arg):
        ''' AirPayNavigation : Click on a sub-feature in the airpay page
                Input argu :
                    type - which progress tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click sub-feature in the airpay page
        locate = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in airpay", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayPage.AirPayNavigation -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTopUpAmount(arg):
        ''' SelectTopUpAmount : Select top up amount
                Input argu :
                    amount - top up amount
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        amount = arg["amount"]

        ##Click amount
        locate = Util.GetXpath({"locate":"amount"}).replace("amount_to_be_replace", amount)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click top up amount in airpay", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayPage.SelectTopUpAmount -> ' + amount)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTopUpPayment(arg):
        ''' SelectTopUpPayment : Select top-up payment
                Input argu : type - ibanking, airpay
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        payment_type = arg["type"]

        ##Click to choose top up payment
        locate = Util.GetXpath({"locate":"payment"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click to choose payment in airpay", "result":"1"})

        if payment_type == "ibanking":
            ##Click iBanking
            locate = Util.GetXpath({"locate":"ibanking"})
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click iBanking", "result":"1"})

            ##Click NCB
            locate = Util.GetXpath({"locate":"ncb"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click NCB", "result":"1"})

        elif payment_type == "airpay":
            ##Click Airpay
            locate = Util.GetXpath({"locate":"airpay"})
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click iBanking", "result":"1"})

            ##Click NCB
            locate = Util.GetXpath({"locate":"bank"}).replace("last_4_num_to_be_replace", "4321").replace("bank_name_to_be_replace", "ABBANK")
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click bank", "result":"1"})

        ##Click confirm button
        locate = Util.GetXpath({"locate":"confirm"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click confirm button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayPage.SelectTopUpPayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPayNow(arg):
        ''' ClickPayNow : Click pay now
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click confirm button
        locate = Util.GetXpath({"locate":"pay_now"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click pay now button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayPage.ClickPayNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAirpayPassword(arg):
        ''' InputAirpayPassword : Input Airpay password
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click password 111111
        locate = Util.GetXpath({"locate":"1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click number 1", "result":"1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click number 1", "result":"1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click number 1", "result":"1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click number 1", "result":"1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click number 1", "result":"1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click number 1", "result":"1"})

        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSAirPayPage.InputAirpayPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAirpayBankVerfiy(arg):
        ''' InputAirpayBankVerfiy : Input Airpay bank verfiy
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Input password 111111
        locate = Util.GetXpath({"locate":"vcode_input"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":"111111", "result":"1"})

        ##Click Verfiy
        locate = Util.GetXpath({"locate":"verfiy"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click verfiy", "result":"1"})

        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSAirPayPage.InputAirpayBankVerfiy')


class iOSAirPayButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToOrderDetail(arg):
        ''' ClickGoToOrderDetail : Click go to order detail btn after payout by airpay wallet
                Input argu :
                            N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to order detail btn after payout by airpay wallet
        xpath = Util.GetXpath({"locate":"order_detail_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click order detail btn in airpay page", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayButton.ClickGoToOrderDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToHomePage(arg):
        '''ClickGoToHomePage : Click home page btn after payout by airpay wallet
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to home page after payout by airpay wallet
        locate = Util.GetXpath({"locate":"home_page_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click home page btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayButton.ClickGoToHomePage')


class iOSOrderSuccessfulPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyPurchase(arg):
        """ClickMyPurchase : Click my purchase in osp
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        """
        ret = 1

        ##Click my purchase btn
        locate = Util.GetXpath({"locate":"my_purchase_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click my purchase btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSOrderSuccessfulPageButton.ClickMyPurchase')

class iOSIntermediaryButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPay(arg):
        ''' ClickPay : Click pay button in payment intermediary page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click pay btn
        xpath = Util.GetXpath({"locate":"pay_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click pay button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickPay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNegative(arg):
        ''' ClickNegative : Click negative button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click negative button
        locate = Util.GetXpath({"locate":"negative_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click negative button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickNegative')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickForgetPassword(arg):
        ''' ClickForgetPassword : Click forget button in payment intermediary page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click forget btn
        xpath = Util.GetXpath({"locate":"forget_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click forget button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickForgetPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel button in payment intermediary page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        ''' ClickAgree : Click agree button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click agree button
        xpath = Util.GetXpath({"locate":"agree_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click agree button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPayAgain(arg):
        ''' ClickPayAgain : Click pay again button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click pay again button
        xpath = Util.GetXpath({"locate":"pay_again_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click pay again button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickPayAgain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLeave(arg):
        ''' CLickLeave : Click leave button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click go to previous page button
        xpath = Util.GetXpath({"locate":"leave_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click go to previous page button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickLeave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadLater(arg):
        ''' ClickUploadLater : Click "I do not have receipt. Upload later" button in bank transfer page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click upload later button
        xpath = Util.GetXpath({"locate":"upload_later_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click upload later button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSIntermediaryButton.ClickUploadLater')
