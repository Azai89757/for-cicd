import unittest
from Config import dumplogger
from FrameWorkBase import *


class CLAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIShipment Precondition =="
        dumplogger.info("== Setup CLAPIShipment Precondition ==")

    def test_CLAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/CLAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIShipment Post condition =="
        dumplogger.info("== Setup CLAPIShipment Post condition ==")
