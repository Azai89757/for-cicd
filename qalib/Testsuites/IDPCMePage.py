import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMePage Precondition =="
        dumplogger.info("== Setup IDPCMePage Precondition ==")

    def test_IDPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMePage-01.xml")

    def test_IDPCMePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMePage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMePage Post condition =="
        dumplogger.info("== Setup IDPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
