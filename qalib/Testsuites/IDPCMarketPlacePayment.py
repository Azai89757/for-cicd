import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCMarketPlacePayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMarketPlacePayment Precondition =="
        dumplogger.info("== Setup IDPCMarketPlacePayment Precondition ==")

    def test_IDPCMarketPlacePayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMarketPlacePayment-01.xml")

    def test_IDPCMarketPlacePayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMarketPlacePayment-02.xml")

    def test_IDPCMarketPlacePayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMarketPlacePayment-03.xml")

    def test_IDPCMarketPlacePayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMarketPlacePayment-04.xml")

    def test_IDPCMarketPlacePayment05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMarketPlacePayment-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMarketPlacePayment Post condition =="
        dumplogger.info("== Setup IDPCMarketPlacePayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
