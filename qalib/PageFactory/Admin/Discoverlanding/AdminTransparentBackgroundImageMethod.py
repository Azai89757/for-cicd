#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminTransparentBackgroundImageMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util
import XtFunc

##Inport Web library
from PageFactory.Web import BaseUICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminTransparentBackgroundImagePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductCSV(arg):
        '''
        UploadProductCSV : Upload transparent background image products
                input Argu :
                    file_name - product list file name
                    type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        type = arg['type']

        ##Upload transparent background image products
        locate = Util.GetXpath({"locate": "upload_csv"})
        BaseUICore.UploadFileWithCSS({"locate":locate, "action": "file", "file_name": file_name, "file_type": type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImagePage.UploadProductCSV')


class AdminTransparentBackgroundImageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstCheck(arg):
        '''
        ClickFirstCheck : Click first check button on cropping tool page
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click first check button on cropping tool page
        xpath = Util.GetXpath({"locate": "check_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first check button on cropping tool page","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickFirstCheck')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemId(arg):
        '''
        ClickItemId : Click item Id button on cropped image QC list
                input Argu :
                    item_id : input the item id which you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']

        ##Click item Id button on cropped image QC list
        xpath = Util.GetXpath({"locate": "item_id_btn"})
        xpath = xpath.replace("item_id_to_be_replaced", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item Id button on cropped image QC list","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickItemId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAccept(arg):
        '''
        ClickAccept : Click accept button on cropped image QC list
                input Argu :
                    item_id : which item you want to accept
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']

        ##Click accept button on cropped image QC list
        xpath = Util.GetXpath({"locate": "accept_btn"})
        xpath = xpath.replace("item_id_to_be_replaced", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click accept button on cropped image QC list","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickAccept')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReject(arg):
        '''
        ClickReject : Click reject button on cropped image QC list
                input Argu :
                    item_id : which item you want to reject
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']

        ##Click reject button on cropped image QC list
        xpath = Util.GetXpath({"locate": "reject_btn"})
        xpath = xpath.replace("item_id_to_be_replaced", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reject button on cropped image QC list","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickReject')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBacktoRequestList(arg):
        '''
        ClickBacktoRequestList : Click back to request list
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to request list
        xpath = Util.GetXpath({"locate": "back_to_request_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to request list","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickBacktoRequestList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefresh(arg):
        '''
        ClickRefresh : Click refresh
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refresh
        xpath = Util.GetXpath({"locate": "refresh_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refresh","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickRefresh')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageNumber(arg):
        '''
        ClickPageNumber : Click page number
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click page number
        xpath = Util.GetXpath({"locate": "page_number_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number","result": "1"})

        OK(ret, int(arg['result']), 'AdminTransparentBackgroundImageButton.ClickPageNumber')
