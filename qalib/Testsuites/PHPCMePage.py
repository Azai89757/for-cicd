import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCMePage Precondition =="
        dumplogger.info("== Setup PHPCMePage Precondition ==")

    def test_PHPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCMePage-01.xml")

    def test_PHPCMePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCMePage-02.xml")

    def test_PHPCMePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCMePage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCMePage Post condition =="
        dumplogger.info("== Setup PHPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
