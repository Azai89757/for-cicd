import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCPlaceholderCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPlaceholderCollection Precondition =="
        dumplogger.info("== Setup IDPCPlaceholderCollection Precondition ==")

    def test_IDPCPlaceholderCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPlaceholderCollection-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPlaceholderCollection Post condition =="
        dumplogger.info("== Setup IDPCPlaceholderCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
