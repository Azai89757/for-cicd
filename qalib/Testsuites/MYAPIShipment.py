import unittest
from Config import dumplogger
from FrameWorkBase import *


class MYAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIShipment Precondition =="
        dumplogger.info("== Setup MYAPIShipment Precondition ==")

    def test_MYAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MYAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIShipment Post condition =="
        dumplogger.info("== Setup MYAPIShipment Post condition ==")
