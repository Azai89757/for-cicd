import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpEscrow(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpEscrow Precondition =="
        dumplogger.info("== Setup TWHttpEscrow Precondition ==")

    def test_TWHttpEscrow01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-01.xml")

    def test_TWHttpEscrow02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-02.xml")

    def test_TWHttpEscrow03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-03.xml")

    def test_TWHttpEscrow04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-04.xml")

    def test_TWHttpEscrow05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-05.xml")

    def test_TWHttpEscrow06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-06.xml")

    def test_TWHttpEscrow07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-07.xml")

    def test_TWHttpEscrow08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpEscrow-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpEscrow Post condition =="
        dumplogger.info("== Setup TWHttpEscrow Post condition ==")
