import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCPHPCProductCard Precondition =="
        dumplogger.info("== Setup PHPCPHPCProductCard Precondition ==")

    def test_PHPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCProductCard-01.xml")

    def test_PHPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCProductCard-02.xml")

    def test_PHPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCProductCard-03.xml")

    def test_PHPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCProductCard-04.xml")

    def test_PHPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCProductCard-05.xml")

    def test_PHPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCProductCard Post condition =="
        dumplogger.info("== Setup PHPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
