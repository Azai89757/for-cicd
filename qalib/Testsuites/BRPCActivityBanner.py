import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class BRPCActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCActivityBanner Precondition =="
        dumplogger.info("== Setup BRPCActivityBanner Precondition ==")

    def test_BRPCActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-01.xml")

    def test_BRPCActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-02.xml")

    def test_BRPCActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-03.xml")

    def test_BRPCActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-04.xml")

    def test_BRPCActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-05.xml")

    def test_BRPCActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-06.xml")

    def test_BRPCActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-07.xml")

    def test_BRPCActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-08.xml")

    def test_BRPCActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCActivityBanner-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCActivityBanner Post condition =="
        dumplogger.info("== Setup BRPCActivityBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
