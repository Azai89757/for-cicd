import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCFlashSale Precondition =="
        dumplogger.info("== Setup TWPCFlashSale Precondition ==")

    def test_TWPCFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCFlashSale-01.xml")

    def test_TWPCFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCFlashSale-02.xml")

    def test_TWPCFlashSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCFlashSale-03.xml")

    def test_TWPCFlashSale04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCFlashSale-04.xml")

    def test_TWPCFlashSale05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCFlashSale-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCFlashSale Post condition =="
        dumplogger.info("== Setup TWPCFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
