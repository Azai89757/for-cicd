import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSellerVoucher Precondition =="
        dumplogger.info("== Setup PHPCSellerVoucher Precondition ==")

    def test_PHPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSellerVoucher-01.xml")

    # def test_PHPCSellerVoucher02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCSellerVoucher-02.xml")

    # def test_PHPCSellerVoucher03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCSellerVoucher-03.xml")

    # def test_PHPCSellerVoucher04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSellerVoucher Post condition =="
        dumplogger.info("== Setup PHPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
