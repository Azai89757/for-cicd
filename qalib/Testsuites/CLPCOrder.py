import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCOrder Precondition =="
        dumplogger.info("== Setup CLPCOrder Precondition ==")

    def test_CLPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-01.xml")

    def test_CLPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-02.xml")

    def test_CLPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-03.xml")

    def test_CLPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-04.xml")

    def test_CLPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-05.xml")

    def test_CLPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-06.xml")

    def test_CLPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCOrder-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCOrder Post condition =="
        dumplogger.info("== Setup CLPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
