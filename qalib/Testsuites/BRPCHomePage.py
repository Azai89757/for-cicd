import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCHomePage Precondition =="
        dumplogger.info("== Setup BRPCHomePage Precondition ==")

    def test_BRPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCHomePage-01.xml")

    def test_BRPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCHomePage-02.xml")

    '''def test_BRPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCHomePage Post condition =="
        dumplogger.info("== Setup BRPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
