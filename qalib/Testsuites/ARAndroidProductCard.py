import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class ARAndroidProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAndroidProductCard Precondition =="
        dumplogger.info("== Setup ARAndroidProductCard Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"ar"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_ARAndroidProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARAndroidProductCard-01.xml")

    def test_ARAndroidProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARAndroidProductCard-02.xml")

    def test_ARAndroidProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARAndroidProductCard-03.xml")

    def test_ARAndroidProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARAndroidProductCard-04.xml")

    def test_ARAndroidProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARAndroidProductCard-05.xml")

    def test_ARAndroidProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARAndroidProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAndroidProductCard Post condition =="
        dumplogger.info("== Setup ARAndroidProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
