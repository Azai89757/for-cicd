import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpReturnRefund Precondition =="
        dumplogger.info("== Setup TWHttpReturnRefund Precondition ==")

    def test_TWHttpReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReturnRefund-01.xml")

    def test_TWHttpReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReturnRefund-02.xml")

    def test_TWHttpReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReturnRefund-03.xml")

    def test_TWHttpReturnRefund04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReturnRefund-04.xml")

    def test_TWHttpReturnRefund05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReturnRefund-05.xml")

    def test_TWHttpReturnRefund06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpReturnRefund-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpReturnRefund Post condition =="
        dumplogger.info("== Setup TWHttpReturnRefund Post condition ==")
