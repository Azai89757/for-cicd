﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import LoadImports
sys.path.append("Testsuites/")
from Testsuites import *
from Config import dumplogger

##Define test case from .py in Testsuites dynamically
def ImportTestSuites():
    ''' ImportTestSuites : Define test case from .py in Testsuites dynamically
            Input argu :
                N/A
            Return code :
                case_type - testsuite class object return
            Note : None
    '''
    case_type = {}

    ##Iterate through the modules to mapping modules' classes
    for module_name in globals():
        try:
            ##Try to map class to dictionary of modules
            case_type[module_name] = getattr(LoadImports.DynamicImportModule('Testsuites.' + module_name), module_name)
            #dumplogger.info(case_type[module_name])
        except AttributeError:
            ##No need to import class and assign to case_type if no such module
            dumplogger.info("No need to import class and assign to case_type if no such module")
        except:
            dumplogger.exception("Please check your test case define file!!!")

    #dumplogger.info(case_type)
    return case_type
