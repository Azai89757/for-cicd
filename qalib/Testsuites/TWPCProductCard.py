import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCProductCard Precondition =="
        dumplogger.info("== Setup TWPCProductCard Precondition ==")

    def test_TWPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductCard-01.xml")

    def test_TWPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductCard-02.xml")

    def test_TWPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductCard-03.xml")

    def test_TWPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductCard-04.xml")

    def test_TWPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductCard-05.xml")

    def test_TWPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCProductCard Post condition =="
        dumplogger.info("== Setup TWPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
