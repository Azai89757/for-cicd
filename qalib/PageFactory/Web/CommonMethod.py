﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 CommonMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore
import BaseUILogic
import CartMethod
import ActivityBannerMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def SetPFB(arg):
    '''
    SetPFB : Set Browser cookie
            Input argu :
                SPC_PFB_MOBILEMALL_STATIC = Mobile static section
                SPC_PFB_MOBILEMALL_RENDER = Mobile static render
                SPC_PFB_MOBILEMALL_HYBRID = Mobile static hybrid
                SPC_PFB_MALL_API = Mall API
                SPC_PFB_PCMALL_RENDER = PCMall render
                SPC_PFB_PCMALL_STATIC = PCMall static
                SPC_PFB_FOO_PFB = FOO
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    dumplogger.info("Enter SetPFB")
    dumplogger.info(arg)
    try:
        ##Set browser cookie
        for keys, vals in arg.items():
            if vals:
                BaseUICore._WebDriver_.add_cookie({'name':keys, 'value':vals})
                dumplogger.info('cookie_name = %s and cookie_value = %s', keys, vals)
            else:
                dumplogger.info('cookie_name = %s value is empty!', keys)

        BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
        BaseUICore.PageHasLoaded({"result": "1"})
        return 1

    except KeyError:
        dumplogger.exception("Encounter Error!")
        return 0

@DecoratorHelper.FuncRecorder
def ShopeeLaunchWebSite(arg):
    '''
    ShopeeLaunchWebSite : Launch shopee website only
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Get pc platform name in corresponding region from GlobalAdapter
    for platform in GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_]:
        if 'pc' in platform:
            transify_key_name = platform

    ##Store i18N Json file name by test country for the first time
    if not GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][transify_key_name]:
        ##Store i18N Json file name by test country for the first time
        if transify_key_name not in Config._i18NJson_[Config._TestCaseRegion_]:
            Config._i18NJson_[Config._TestCaseRegion_].append(transify_key_name)

        ##Get Transify Key
        ret = Util.GetPCTransifyKey(Config._EnvType_.lower(), transify_key_name)
        dumplogger.info("Get PC transify key result: %d" % (ret))
    else:
        dumplogger.info("PC transify key has been loaded!!!")

    if ret == 1:
        ##Define url to launch
        url = "https://" + GlobalAdapter.UrlVar._Domain_
        dumplogger.info("url = %s" % (url))

        ##Go to url
        BaseUICore.GotoURL({"url":url, "result": "1"})

        ##Choose language when entering FE
        if Config._TestCaseRegion_ in ("TH", "MY", "IN"):
            xpath = Util.GetXpath({"locate":"choose_language"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose language", "result": "1"})
        elif Config._TestCaseRegion_ in ("PL", "ES", "FR"):
            xpath = Util.GetXpath({"locate": "cookies_btn"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close cookies message", "result": "1"})

        ##Refresh browser for possible banner showed up
        xpath = Util.GetXpath({"locate": "f5_page"})
        BaseUILogic.DetectPopupBanner({"locate": xpath, "result": "1"})
        BaseUICore.PageHasLoaded({"result": "1"})

        ##Switch PFB if needed
        if Config._SwitchEnv_:
            dumplogger.info("Start to switch PFB..")
            is_success_set_pfb = SetPFB(Config._PFB_["PC"])
            if is_success_set_pfb:
                dumplogger.info("Set PFB success!")
            else:
                dumplogger.info("Set PFB failed!")
                ret = -1
    else:
        dumplogger.info("Get PC Transify key failed...please check debug message!!!")

    OK(ret, int(arg['result']), 'ShopeeLaunchWebSite')

@DecoratorHelper.FuncRecorder
def ScrollToSection(arg):
    '''
    ScrollToSection : Scroll to target section
            Input argu :
                section - home_banner_section / category_section / flash_sale_section / skinny_banner_section /
                          mall_section / trending_search / top_product_section / rcmd_section / product_specifications_section
                          product_description_section / product_ratings_section
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    section = arg["section"]

    ##Check section is exist
    xpath = Util.GetXpath({"locate":section})
    if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

        ##Get section location
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"location", "mode":"single", "result": "1"})

        ##Get location y of section
        location_y_of_section = GlobalAdapter.CommonVar._PageAttributes_["y"]
        dumplogger.info("Section location y => " + str(location_y_of_section))

        ##Get top nav size
        xpath = Util.GetXpath({"locate":"home_top_nav"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"size", "mode":"single", "result": "1"})
        ##Get height of top nav
        height_of_top_nav = GlobalAdapter.CommonVar._PageAttributes_["height"]
        dumplogger.info("Top nav height => " + str(height_of_top_nav))

        ##section_location_y subtract by top_nav_height
        location_y_of_section = location_y_of_section - height_of_top_nav

        ##Scroll down by section location y
        BaseUICore._WebDriver_.execute_script("window.scrollBy(0, " + str(location_y_of_section) + ")")
        dumplogger.info("Scroll by vertical number => " + str(location_y_of_section))
    else:
        dumplogger.exception("Can't find this home page section => " + section)

    OK(ret, int(arg['result']), 'ScrollToSection -> ' + section)

@DecoratorHelper.FuncRecorder
def GotoProductDetail(arg):
    '''
    GotoProductDetail : Go to product detail page
            Input argu :
                shopid - shopid / sellerid
                productid - product id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    shopid = arg["shopid"]
    productid = arg["productid"]
    env = arg["env"]
    ret = 1

    url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/product/" + shopid + "/" + productid + "/"
    dumplogger.info("url = " + url)

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(15)

    ##Check PDP item name is loaded
    xpath = Util.GetXpath({"locate":"pdp_item_name"})
    BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "message":"Check PDP item name is loaded", "result": "1"})

    ##Sleep 5 second to wait PDP page is loaded to avoid "seller no login over 7 days"
    time.sleep(5)

    OK(ret, int(arg['result']), 'GotoProductDetail')

@DecoratorHelper.FuncRecorder
def GotoSellerStore(arg):
    '''
    GotoSellerStore : Go to seller store
            Input argu :
                shop_name - shop name which want to go
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    shop_name = arg["shop"]
    ret = 1

    url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/" + shop_name
    dumplogger.info("url = " + url)

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(2)

    OK(ret, int(arg['result']), 'GotoSellerStore')

@DecoratorHelper.FuncRecorder
def ClickShopeeLogoIcon(arg):
    '''
    ClickShopeeLogoIcon : Click shopee logo icon in login/signup page
            Input argu :
                page_type - shopee login of any page
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    page_type = arg["page_type"]

    ##Click shopee logo icon in login/signup page
    xpath = Util.GetXpath({"locate": page_type})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shopee logo icon", "result": "1"})

    OK(ret, int(arg['result']), 'ClickShopeeLogoIcon')

@DecoratorHelper.FuncRecorder
def ReopenShopeeWebsite(arg):
    '''
    ReopenShopeeWebsite : Reopen web page and launch website
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Close staging and open
    time.sleep(5)
    BaseUICore.DeInitialWebDriver({})
    BaseUICore.InitialWebDriver({"browser": "chrome", "result": "1"})
    ShopeeLaunchWebSite({"result": "1"})
    time.sleep(10)
    BaseUILogic.BrowserRefresh({"message": "Refresh category page", "result": "1"})
    time.sleep(3)
    BaseUILogic.BrowserRefresh({"message": "Refresh category page", "result": "1"})
    time.sleep(15)

    ##Close cookie btn
    if Config._TestCaseRegion_ in ("PL", "ES", "FR"):
        xpath = Util.GetXpath({"locate": "cookies_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close cookies message", "result": "1"})

    OK(ret, int(arg['result']), 'ReopenShopeeWebsite')

@DecoratorHelper.FuncRecorder
def CheckToastMessages(arg):
    '''
    CheckToastMessages : Check toast messages in webdriver
            Input argu :
                toast_text - the toast messages you want to check
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    toast_text = arg['toast_text']

    ##Check toast messages is visible
    xpath = Util.GetXpath({"locate": "toast_path"})
    xpath = xpath.replace("toast_text_to_be_replaced", toast_text)
    BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

    OK(ret, int(arg['result']), 'CheckToastMessages')

class HomePageNavigationBar:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLoginIcon(arg):
        ''' ClickLoginIcon : Click login icon
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click login icon
        xpath = Util.GetXpath({"locate":"Login_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click login icon", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.ClickLoginIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSignupIcon(arg):
        ''' ClickSignupIcon : Click signup icon
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click signup icon
        xpath = Util.GetXpath({"locate":"Signup_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click signup icon", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.ClickSignupIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToNotificationPage(arg):
        ''' GoToNotificationPage : Go to Notification page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"label_notifications"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Go to Notification page", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.GoToNotificationPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HoverToNotificationPage(arg):
        ''' HoverToNotificationPage : Go to Notification page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Move to element by its coordinate
        xpath = Util.GetXpath({"locate":"label_notifications"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.HoverToNotificationPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToCart(arg):
        ''' GoToCart : Go to Cart page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Cart icon
        xpath = Util.GetXpath({"locate":"cart_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Cart icon", "result": "1"})

        ##Retry refresh to confirm cart page is loaded
        CartMethod.CartPage.WaitCartLoadingIsComplete({"result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.GoToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCartIcon(arg):
        ''' ClickCartIcon : Click cart icon
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Cart icon
        xpath = Util.GetXpath({"locate":"cart_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Cart icon", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.ClickCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteItemInCartDropDownList(arg):
        '''DeleteItemInCartDropDownList : Delete item in cart dropdown list
                Input argu : product_name - product name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Get cart icon, delete btn
        cart_xpath = Util.GetXpath({"locate":"cart_icon"})
        delete_btn = Util.GetXpath({"locate":"delete_btn"})
        delete_btn = delete_btn.replace("replace_product_name", product_name)

        ##Click delete btn
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":cart_xpath, "locatehidden":delete_btn, "message":"Go delete in cart drop down list", "result": "1"})

        ##Move cursor to shopee logo
        time.sleep(5)
        shopee_logo = Util.GetXpath({"locate":"shopee_icon"})
        BaseUICore.MoveToElementCoordinate({"locate":shopee_logo, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.DeleteItemInCartDropDownList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToBuyingList(arg):
        ''' GoToBuyingList : Go to buying list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath1 = Util.GetXpath({"locate":"me_drop_down_list"})
        xpath2 = Util.GetXpath({"locate":"my_purchase"})

        ##Go to buying list page
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath1, "locatehidden":xpath2, "message":"Go to buying list page", "result": "1"})
        time.sleep(5)
        shopee_logo = Util.GetXpath({"locate":"shopee_icon"})
        BaseUICore.MoveToElementCoordinate({"locate":shopee_logo, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.GoToBuyingList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToMyGroup(arg):
        ''' GoToMyGroup : Go to MyGroup page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath1 = Util.GetXpath({"locate":"me_drop_down_list"})
        xpath2 = Util.GetXpath({"locate":"label_header_my_group"})

        ##Go to MyGroup page
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath1, "locatehidden":xpath2, "message":"Go to MyGroup page", "result": "1"})
        time.sleep(5)
        shopee_logo = Util.GetXpath({"locate":"shopee_icon"})
        BaseUICore.MoveToElementCoordinate({"locate":shopee_logo, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.GoToMyGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToMyAccount(arg):
        ''' GoToMyAccount : Go to my account page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath_drop_down = Util.GetXpath({"locate":"me_drop_down_list"})
        xpath_account = Util.GetXpath({"locate":"my_account"})

        ##Go to buying list page
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath_drop_down, "locatehidden":xpath_account, "message":"Go to my account page", "result": "1"})

        ##Check my account page is loaded
        xpath_my_account_page = Util.GetXpath({"locate":"label_user_profile_profile"})
        BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath_my_account_page, "message":"Check my account page is loaded", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.GoToMyAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserName(arg):
        ''' ClickUserName : Click username
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"user_name"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click username", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageNavigationBar.ClickUserName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeLanguage(arg):
        '''
        ChangeLanguage : Change language
                Input argu :
                    language - language
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        language = arg['language']

        ##Move to language setting and change language
        xpath = Util.GetXpath({"locate": "language_setting"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        xpath = Util.GetXpath({"locate": "target_language"})
        xpath = xpath.replace("language_type_to_replace", language)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Change language", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'HomePageNavigationBar.ChangeLanguage')
