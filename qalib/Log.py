def Dump(level, message, standalone=0):
    '''DebugLog: Use logging module to print detail of log
        Input argu : level - Logging level
                        INFO    :20
                        WARNING :30
                        ERROR   :40
                        CRITICAL:50
                     message - debug log message
        Return code:  1 -
                      0 -
                     -1 -
        Note: None'''

    def OutputCategoryLog():

        if level == 10:
            logging.debug(message)
        elif level == 20:
            logging.info(message)
        elif level == 30:
            logging.warning(message)
        elif level == 40:
            logging.error(message)
        elif level == 50:
            logging.critical(message)

    if standalone == 1:

        import logging
        import sys
        logging.basicConfig(format="%(asctime)s-[%(process)d][%(thread)d]|[%(levelname)s]|[%(filename)s:%(lineno)d][%(funcName)s]|%(message)s",
                            level=logging.DEBUG,
                            filename=r"C:\Dump",
                            filemode="w",
                            stream=sys.stdout
                            )

        OutputCategoryLog()

    elif standalone == 0:

        import Config

        if Config._DUMP_ == 1:

            import logging
            import sys
            logging.basicConfig(format="%(asctime)s-[%(process)d][%(thread)d]|[%(levelname)s]|[%(filename)s:%(lineno)d][%(funcName)s]|%(message)s",
                                level=logging.DEBUG,
                                filename=Config._DUMPPATH_,
                                filemode="w",
                                stream=sys.stdout
                                )

            OutputCategoryLog()
