import unittest
from Config import dumplogger
from FrameWorkBase import *
from api.spex import SpexAPICore
import Config


class TWSpexCollectionAdmin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexCollectionAdmin Precondition =="
        dumplogger.info("== Setup TWSpexCollectionAdmin Precondition ==")

        ##Tail log file to grep "Local connections to /run/spex/spex.sock:-2 forwarded to remote address /run/spex/spex.sock"
        ##If Spex InitResult is True, will not call RegisterToSpex() again
        if SpexAPICore.InspectSpexConnection():
            ##Register to Spex with specific type of spex configuration for the first time
            if not Config._SpexRegResult_:
                if SpexAPICore.GetSpexConfig('Api_Test_Staging_TW'):
                    Config._SpexRegResult_ = SpexAPICore.RegisterToSpex()

                    ##Check outcome after register
                    if Config._SpexRegResult_:
                        print "== Setup TWSpexCollectionAdmin Register Success =="
                        dumplogger.info("== Setup TWSpexCollectionAdmin Register Success ==")

                    else:
                        print ("Spex Register Failed!!!")
                        dumplogger.error("Spex Register Failed")
                        ##Directly Leave Setup
                        self.skipTest("")
                else:
                    dumplogger.error("Get Spex config from ini failed!!!")
                    self.skipTest("")
            else:
                dumplogger.info("Spex is already registered.")

        else:
            print "== Setup TWSpexCollectionAdmin Precondition Fail by No Spex Agent Connection =="
            dumplogger.error("== Setup TWSpexCollectionAdmin Precondition Fail by No Spex Agent Connection ==")
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWSpexCollectionAdmin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-01.xml")

    def test_TWSpexCollectionAdmin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-02.xml")

    def test_TWSpexCollectionAdmin03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-03.xml")

    def test_TWSpexCollectionAdmin04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-04.xml")

    def test_TWSpexCollectionAdmin05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-05.xml")

    def test_TWSpexCollectionAdmin06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-06.xml")

    def test_TWSpexCollectionAdmin07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-07.xml")

    def test_TWSpexCollectionAdmin08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionAdmin-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexCollectionAdmin Post condition =="
        dumplogger.info("== Setup TWSpexCollectionAdmin Post condition ==")
