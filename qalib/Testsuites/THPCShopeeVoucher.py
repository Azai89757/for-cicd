import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCShopeeVoucher Precondition =="
        dumplogger.info("== Setup THPCShopeeVoucher Precondition ==")

    def test_THPCShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeVoucher-01.xml")

    def test_THPCShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeVoucher-02.xml")

    def test_THPCShopeeVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeVoucher-03.xml")

    def test_THPCShopeeVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeVoucher-04.xml")

    def test_THPCShopeeVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeVoucher-05.xml")

    def test_THPCShopeeVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeVoucher-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCShopeeVoucher Post condition =="
        dumplogger.info("== Setup THPCShopeeVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
