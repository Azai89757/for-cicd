#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 CheckOutMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class CheckOutPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def WaitCheckOutPageFullyLoaded(arg):
        '''
        WaitCheckOutPageFullyLoaded : Wait checkout page fully loaded to avoid error
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Wait for loading popup disappear in 10 times
        for count in range(10):

            ##Check loading popup exist or not
            xpath = Util.GetXpath({"locate":"loading_popup"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

                ##Loading popup exist and sleep 5 second for next check
                dumplogger.info("Loading popup still exist...")
                time.sleep(5)
            else:

                ##Loading popup not exist and leave function
                ret = 1
                dumplogger.info("Loading popup is gone after waiting for %s times...", count)
                break

        OK(ret, int(arg['result']), 'CheckOutPage.WaitCheckOutPageFullyLoaded')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePaymentMethod(arg):
        '''
        ChangePaymentMethod : Change payment method in checkout page
                Input argu :
                    method - payment method to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method = arg["method"]

        ##Clcik change payment method btn if exists
        xpath = Util.GetXpath({"locate":"change_payment_method_btn"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click change payment method button", "result":"1"})

        ##Click payment method
        xpath = Util.GetXpath({"locate":method})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click a payment method", "result":"1"})
        time.sleep(2)

        ##Click sub options under each payment method
        if method == "ibanking":
            ##Click a bank account
            xpath = Util.GetXpath({"locate":"ibanking_bank_account"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click NCB bank account", "result":"1"})

        elif method == "airpay_bt":
            ##Click airpay payment
            xpath = Util.GetXpath({"locate":"airpay_wallet"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click a payment method", "result":"1"})
            time.sleep(2)

            ##Click airpay bt account
            xpath = Util.GetXpath({"locate":"airpay_bt"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click a bt account", "result":"1"})

        elif method == "airpay_wallet":
            ##Click airpay wallet options
            xpath = Util.GetXpath({"locate":"airpay_wallet_options"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click a bt account", "result":"1"})

        else:
            dumplogger.info("No sub options to choose for this payment method")

        OK(ret, int(arg['result']), 'CheckOutPage.ChangePaymentMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeShippingMethod(arg):
        '''
        ChangeShippingMethod : Change shipping method in checkout page
                Input argu :
                    position - edit button position
                    method - shipping method
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        position = arg['position']
        method = arg["method"]

        ##Click change shipping method button
        xpath = Util.GetXpath({"locate":"change_shipping_method_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath + "[" + position + "]", "message":"Click to change a shipping method", "result": "1"})

        ##Choose shipping method
        CheckOutPage.ChooseShippingMethod({"method":method, "result": "1"})

        ##Click submit
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click complete button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.ChangeShippingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeCoin(arg):
        '''
        ClickShopeeCoin : Click shopee coin checkbox to use shopee coin
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to use shopee coin checkbox
        xpath = Util.GetXpath({"locate": "shopee_coin_checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shopee coin checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.ClickShopeeCoin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PayOutByCreditCard(arg):
        '''
        PayOutByCreditCard : PayOut by credit card
                Input argu : N/A
                Retrun code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ## Input CVV code before click pay button (for ID, so far)
        if Config._TestCaseRegion_ in ("MX", "ID", "BR"):
            xpath = Util.GetXpath({"locate":"cvv_code"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"123", "result": "1"})

            ##Input E-mail in MX
            if Config._TestCaseRegion_ in ("MX", "BR"):
                xpath = Util.GetXpath({"locate":"email_field"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"autotest@shopee.com", "result": "1"})

            ##Click ensure credit card pay button
            xpath = Util.GetXpath({"locate":"pay_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click pay button", "result": "1"})

            ##Click ok btn
            time.sleep(5)
            xpath = Util.GetXpath({"locate":"ok_button"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok btn", "result": "1"})
                time.sleep(5)

        else:
            ##Click ensure credit card pay button
            xpath = Util.GetXpath({"locate":"pay_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click pay button", "result": "1"})
            time.sleep(6)

            ##The submit button in SG is inside a iframe
            if Config._TestCaseRegion_ in ("SG", "VN", "MY"):
                ##Switch to frame
                new_frame = Util.GetXpath({"locate":"new_frame"})
                BaseUICore.SwitchToFrame({"frame":new_frame, "result":"1"})

            ##Click ensure credit card pay button
            xpath = Util.GetXpath({"locate":"submit_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit btn", "result": "1"})

            ##Click ok btn
            time.sleep(5)
            xpath = Util.GetXpath({"locate":"ok_button"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.PayOutByCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseShippingMethod(arg):
        '''
        ChooseShippingMethod : Choose shipping method
                Input argu :
                    method - shipping method
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method = arg['method']

        ##Check sub collection in specific region
        if method == "self_delivery" and Config._TestCaseRegion_ == "VN":
            ##check non-integration exist
            xpath = Util.GetXpath({"locate":"check_non_integration_option"})

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ##open non-integration list
                xpath = Util.GetXpath({"locate":"open_non_integration"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select" + "method", "result": "1"})

        elif Config._TestCaseRegion_ == "ID":
            ##check non-integration exist for regular
            xpath = Util.GetXpath({"locate":"check_non_integration_option_for_regular"})

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ##open Reguler list
                xpath = Util.GetXpath({"locate":"open_regular_list"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select" + "method", "result": "1"})

        ##Select a shipping method
        xpath = Util.GetXpath({"locate":method})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select" + "method", "result": "1"})

        ##Check prefered delivery time exist or not
        if Config._TestCaseRegion_ in ("VN", "TH"):
            ##Select prefered delivery time
            prefered_delivery_xpath = Util.GetXpath({"locate":"prefered_delivery_time"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath + prefered_delivery_xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath + prefered_delivery_xpath, "message":"Choose a prefered delivery time", "result": "1"})
            else:
                dumplogger.info("No prefered delivery time options showed")

        OK(ret, int(arg['result']), 'CheckOutPage.ChooseShippingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMessageForSeller(arg):
        '''
        InputMessageForSeller : Input message for seller
                Input argu :
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        message = arg['message']

        ##Input message
        xpath = Util.GetXpath({"locate":"input_message"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":message, "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.InputMessageForSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRUTCode(arg):
        '''
        InputRUTCode :  input RUT code
                Input argu :
                    rut - RUT code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rut = arg['rut']

        ##Input RUT code in RUT section
        xpath = Util.GetXpath({"locate": "rut_section"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rut, "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.InputRUTCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCYBSInstallmentPlan(arg):
        '''
        SelectCYBSInstallmentPlan : Select CYBS installment plan
                Input argu :
                    bank - bank name
                    period - installment period
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg['bank']
        period = arg['period']

        ##Select CYBS installment bank
        xpath = Util.GetXpath({"locate":"bank_name"})
        xpath = xpath.replace("bank_name_to_be_replaced", bank)
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Select CYBS installment bank", "result": "1"})

        ##Select CYBS installment period
        xpath = Util.GetXpath({"locate":"period"})
        xpath = xpath.replace("period_to_be_replaced", period)
        BaseUICore.Click({"method": "javascript", "locate": xpath,"message": "Select CYBS installment period", "result": "1"})

        ##Click next button
        xpath = Util.GetXpath({"locate":"next_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.SelectCYBSInstallmentPlan')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeAddress(arg):
        '''
        ClickChangeAddress : click change pickup address btn in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change pickup address btn
        xpath = Util.GetXpath({"locate": "change_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change pickup address btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.ClickChangeAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputIdentificationCard(arg):
        '''
        InputIdentificationCard : Input national identification cart number
                Input argu :
                    id_number - national identification card number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        id_number = arg["id_number"]

        ##Input id card number
        xpath = Util.GetXpath({"locate": "id_section"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": id_number, "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.InputIdentificationCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDeliveryTime(arg):
        '''
        SelectDeliveryTime : Select delivery time in logistic page
                Input argu :
                    delivery_time - The time when you want delivery (anytime/worktime)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        delivery_time = arg['delivery_time']

        ##Input id card number
        xpath = Util.GetXpath({"locate": delivery_time})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click the time you want delivery", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.SelectDeliveryTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectReceiptType(arg):
        '''
        SelectReceiptType : Select receip type in checkout page
                Input argu :
                    receipt_type - The time when you want delivery (anytime/worktime)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        receipt_type = arg['receipt_type']

        ##Click dropdown icon
        xpath = Util.GetXpath({"locate": "dropdown_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown icon", "result": "1"})

        ##Select receipt type
        xpath = Util.GetXpath({"locate": receipt_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click the time you want delivery", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutPage.SelectReceiptType')


class CheckOutButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewAddress(arg):
        '''
        ClickAddNewAddress : click add new address btn in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new adress btn
        xpath = Util.GetXpath({"locate": "add_new_address_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new address btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickAddNewAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeShippingMethod(arg):
        '''
        ClickChangeShippingMethod : Click change shipping method btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change btn
        xpath = Util.GetXpath({"locate":"change_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click change shipping method btn", "result": "1"})

        ##Select prefered delivery time
        prefered_delivery_xpath = Util.GetXpath({"locate":"prefered_delivery_time"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath + prefered_delivery_xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath + prefered_delivery_xpath, "message":"Choose a prefered delivery time", "result": "1"})
        else:
            dumplogger.info("No prefered delivery time options showed")

        OK(ret, int(arg['result']), 'CheckOutButton.ClickChangeShippingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCreditCard(arg):
        '''
        SelectCreditCard : Select credit card
                Input argu :
                    card_number - credit card number you want to select
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']

        ##Click credit card
        xpath = Util.GetXpath({"locate":"credit_card_info"})
        xpath_credit_card_info = xpath.replace("card_number_to_be_replace", card_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath_credit_card_info, "message":"select credit card", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectBankAccount(arg):
        '''
        SelectBankAccount : Select bank account
                Input argu :
                    bankaccount - bank account name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bankaccount = arg["bankaccount"]

        ##Click bank account
        xpath = Util.GetXpath({"locate": "bank_account"})
        xpath = xpath.replace("bankaccount_to_be_replaced", bankaccount)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckOut(arg):
        '''
        ClickCheckOut : Goto check out page and check out
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click checkout button
        xpath = Util.GetXpath({"locate":"label_opc_place_order"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickCheckOut')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCBPopup(arg):
        '''
        ClickCBPopup : After click place order button, handle the case that page will popup tax alert window if product is cross border product.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check whether if popup cb product tax text
        xpath = Util.GetXpath({"locate":"cb_popup"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click cofirm button of popup window
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"handle cb product popup", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickCBPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandShipType(arg):
        '''
        ClickExpandShipType : Expand a shipping type on checkout page
                Input argu :
                    ship_type : express / standard / economy / non-integrated / integrated / other / regular
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ship_type = arg['ship_type']

        ##Click expand element
        xpath = Util.GetXpath({"locate":ship_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click expand element of ship type => " + ship_type, "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickExpandShipType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePayment(arg):
        '''
        ClickChangePayment : click change payment when default selected payment method
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change payment button first to show all payment method
        xpath = Util.GetXpath({"locate":"change_payment_method_btn"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click change payment method button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickChangePayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentMethod(arg):
        '''
        ClickPaymentMethod : click payment method in checkout page
                Input argu :
                    method - credit_card / cod / cc_installment / bank_transfer / ibanking / airpay
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method = arg['method']

        ##Click change payment method button
        xpath = Util.GetXpath({"locate":method})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s payment method" % method, "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickPaymentMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : click pop up button in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click pop up button in checkout page
        xpath = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOptionsInLogisticPage(arg):
        '''
        ClickOptionsInLogisticPage : Click btn in change shipping method
                Input argu :
                    option - cancel, submit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Click ok or cancel btn
        xpath = Util.GetXpath({"locate":option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickOptionsInLogisticPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCreditCard(arg):
        '''
        ClickAddCreditCard : A btn of adding credit card for payment when user doesn't create credit card before
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create a new credit card for payment btn
        xpath = Util.GetXpath({"locate":"label_opc_pay_with_new_card"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click add credit card btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickAddCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCYBSInstallment(arg):
        '''
        ClickAddCYBSInstallment : Click add CYBS installment button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add CYBS installment button
        xpath = Util.GetXpath({"locate":"add_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add CYBS installment button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickAddCYBSInstallment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCreditCard(arg):
        '''
        ClickAddCreditCard : Click edit button for new added credit card in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click edit credit card btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickEditCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCCInstallment(arg):
        '''
        SelectCCInstallment : Select credit card installment
                Input argu :
                    card_number - card number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']

        ##Select credit card installment
        xpath = Util.GetXpath({"locate":"credit_card_installment"})
        xpath = xpath.replace("card_number_to_be_replaced", card_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select credit card installment", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectCCInstallment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectInstallmentTenor(arg):
        '''
        SelectInstallmentTenor : Choose installment tenor
                Input argu :
                    tenor - installment tenor
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tenor = arg['tenor']

        ##Click installment tenor
        xpath = Util.GetXpath({"locate":"installment_tenor"})
        xpath = xpath.replace("tenor_to_be_replaced", tenor)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click installment tenor", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectInstallmentTenor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCreditCardInstallment(arg):
        '''
        ClickAddCreditCardInstallment : A btn of adding credit card installment for payment when user doesn't create credit card before
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create a new credit card installment for payment btn
        xpath = Util.GetXpath({"locate":"label_opc_add_cc_installment"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click add credit card installment btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickAddCreditCardInstallment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInstallmentSubmit(arg):
        '''
        ClickInstallmentSubmit : Click submit btn of selecting installment for payment
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"submit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click submit installment btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickInstallmentSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectIBankingBank(arg):
        '''
        SelectIBankingBank : Select ibanking bank
                Input argu :
                    bank - bank name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg['bank']

        ##Select ibanking bank
        xpath = Util.GetXpath({"locate":"ibanking_bank"})
        xpath = xpath.replace("bank_name_to_be_replaced", bank)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select ibanking bank", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectIBankingBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseVoucher(arg):
        '''
        ClickChooseVoucher : Click choose voucher button to open voucher popup window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose voucher button to open voucher popup window
        xpath = Util.GetXpath({"locate":"choose_voucher_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click choose voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickChooseVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetSellerVoucher(arg):
        '''
        ClickGetSellerVoucher : Click get seller voucher
                Input argu :
                    shop_name - shop name you want to get it seller voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Click get seller voucher
        xpath = Util.GetXpath({"locate":"get_seller_voucher_btn"})
        xpath_get_seller_voucher_btn = xpath.replace("shop_name_to_be_replace", shop_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_get_seller_voucher_btn, "message":"Click get seller voucher", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickGetSellerVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAkulakuPeriod(arg):
        '''
        SelectAkulakuPeriod : Choose akulaku installment period
                Input argu :
                    period - akulaku installment period 1/2/3/6/9/12
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        period = arg['period']

        ##Click akulaku installment
        xpath = Util.GetXpath({"locate":"akulaku_period"})
        xpath_period = xpath.replace("period_to_be_replace", period)
        BaseUICore.Click({"method":"xpath", "locate":xpath_period, "message":"click akulaku period", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectAkulakuPeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectKredivoOption(arg):
        '''
        SelectKredivoOption : Choose kredivo options
                Input argu :
                    option - Choose kredivo options
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Click kredivo option
        xpath = Util.GetXpath({"locate":"kredivo_option"})
        xpath_option = xpath.replace("option_to_be_replace", option)
        BaseUICore.Click({"method":"xpath", "locate":xpath_option, "message":"click kredivo option", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectKredivoOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectMolpayCashOption(arg):
        '''
        SelectMolpayCashOption : Choose molpay cash options
                Input argu :
                    option - 711 / kkmart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Click molpay cash option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click molpay cash option", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.SelectMolpayCashOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropShippingMark(arg):
        '''
        ClickDropShippingMark : click the check mark to use drop shipping
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click the check mark to use drop shipping
        xpath = Util.GetXpath({"locate": "drop_check_mark"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click drop check mark", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickDropShippingMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropShippingChange(arg):
        '''
        ClickDropShippingChange : click the change btn to change drop shipping address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click the check mark to use drop shipping
        xpath = Util.GetXpath({"locate": "drop_change_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click drop change btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickDropShippingChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropShippingSave(arg):
        '''
        ClickDropShippingSave : click the save btn to save drop shipping address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click the check mark to use drop shipping
        xpath = Util.GetXpath({"locate": "drop_save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click drop save btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickDropShippingSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChat(arg):
        '''
        ClickChat : click chat button in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click chat btn
        xpath = Util.GetXpath({"locate": "chat_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click chat btn", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickChat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseChat(arg):
        '''
        CloseChat : close chat button in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close chat btn
        xpath = Util.GetXpath({"locate": "close_chat_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close chat", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.CloseChat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinue(arg):
        '''
        ClickContinue : click continue button in checkout page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click continue button in checkout page
        xpath = Util.GetXpath({"locate":"continue_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutButton.ClickContinue')

class CheckOutComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
                button_text - text of button which to be replaced in xpath
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        button_text = arg["button_text"]

        xpath = Util.GetXpath({"locate": button_type})

        if button_text:
            xpath = xpath.replace("string_to_be_replace", button_text)

        ##Click on button
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
            Input argu :
                checkbox_type - details_page_all_product / details_page_option_product
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        xpath = Util.GetXpath({"locate": checkbox_type})

        ##Click on checkbox
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnLabel(arg):
        '''
        ClickOnLabel : Click any type of label with well defined locator
            Input argu :
                label_type - type of label which defined by caller
                label_text - text of label which to be replaced in xpath
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        label_type = arg["label_type"]
        label_text = arg["label_text"]

        xpath = Util.GetXpath({"locate": label_type})

        if label_text:
            xpath = xpath.replace("string_to_be_replace", label_text)

        ##Click on label
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click label: %s, which xpath is %s" % (label_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutComponent.ClickOnLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column
                input_content - content to be inputed
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'CheckOutComponent.InputToColumn')
