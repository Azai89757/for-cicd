import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXAPIRestoreFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIRestoreFlashSale Precondition =="
        dumplogger.info("== Setup MXAPIRestoreFlashSale Precondition ==")

    def test_MXAPIRestoreFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MXAPIRestoreFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIRestoreFlashSale Post condition =="
        dumplogger.info("== Setup MXAPIRestoreFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
