import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCFlashSale Precondition =="
        dumplogger.info("== Setup INPCFlashSale Precondition ==")

    def test_INPCFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCFlashSale-01.xml")

    def test_INPCFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCFlashSale-02.xml")

    def test_INPCFlashSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCFlashSale-03.xml")

    def test_INPCFlashSale04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCFlashSale-04.xml")

    def test_INPCFlashSale05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCFlashSale-05.xml")

    def test_INPCFlashSale06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCFlashSale-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCFlashSale Post condition =="
        dumplogger.info("== Setup INPCFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
