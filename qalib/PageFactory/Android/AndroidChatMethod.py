﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidChatMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import common library
import Util
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidChatListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteChat(arg):
        '''
        DeleteChat : Long press to delete chat if the name of the chat exists
                Input argu :
                    chat_name : chat to be deleted
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        chat_name = arg["chat_name"]
        ret = 1

        ##Long press chat to delete
        xpath = Util.GetXpath({"locate": "target_username"})
        xpath = xpath.replace('replace_name', chat_name)

        ##Delete chat if the specific chat name exists
        if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            AndroidBaseUICore.AndroidLongPress({"locate": xpath, "sec": "3", "result": "1"})

            ##Click delete
            xpath = Util.GetXpath({"locate": "delete_btn"})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

            ##Confirm delete
            xpath = Util.GetXpath({"locate": "confirm_delete"})

            ##Check confirm popup exist since sometimes chat will just be deleted directly
            if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ##Click confirm
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})
            else:
                dumplogger.info("Do not have check alert.")

        OK(ret, int(arg['result']), 'AndroidChatListPage.DeleteChat -> ' + chat_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectChatDisplayMode(arg):
        '''
        SelectChatDisplayMode : Select chat display drop down list
                Input argu :
                    select_type - all / unread / unreplied
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        select_type = arg["select_type"]
        ret = 1

        ##Click display drop down list
        xpath = Util.GetXpath({"locate": "display_drop_down_list"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click display drop down list", "result": "1"})

        if select_type:
            ##Click display mode
            xpath = Util.GetXpath({"locate": select_type})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click chat display mode -> " + select_type, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatListPage.SelectChatDisplayMode')


class AndroidChatListPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatWithUser(arg):
        '''
        ClickChatWithUser : Click chat with user
                Input argu :
                    chat_user : chat user
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        chat_user = arg["chat_user"]

        ##Click chat with name
        xpath = Util.GetXpath({"locate": "chat_user"})
        xpath = xpath.replace('replace_name', chat_user)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click chat", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatListPageButton.ClickChatWithUser -> ' + chat_user)


class AndroidChatConversationWindow:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BargainForProduct(arg):
        '''
        BargainForProduct : Bargain for product
                Input argu :
                    amount - amount of the product
                    price - price of the product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        amount = arg["amount"]
        price = arg["price"]
        ret = 1

        ##Input amount
        xpath = Util.GetXpath({"locate": "input_amount"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": amount, "result": "1"})

        ##Input price
        xpath = Util.GetXpath({"locate": "input_price"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": price, "result": "1"})

        ##Click submit
        xpath = Util.GetXpath({"locate": "submit_offer_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to make offer", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindow.BargainForProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductPrice(arg):
        '''
        InputProductPrice : Input product price
                Input argu :
                    price - price of the product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        price = arg["price"]
        ret = 1

        ##Input price
        xpath = Util.GetXpath({"locate": "input_price"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": price, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindow.InputProductPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductQuantity(arg):
        '''
        InputProductQuantity : Input product quantity
                Input argu :
                    amount - amount of the product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        amount = arg["amount"]
        ret = 1

        ##Input amount
        xpath = Util.GetXpath({"locate": "input_amount"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": amount, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindow.InputProductQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyProductQuantity(arg):
        '''
        ModifyProductQuantity : Modify product quantity in make offer pop up
                Input argu :
                    click_times - click times
                    type - plus / minus
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        type = arg["type"]
        click_times = arg["click_times"]

        for time in range(int(click_times)):
            ##Click type button
            xpath = Util.GetXpath({"locate": type})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click" + type + "btn", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidChatConversationWindow.ModifyProductQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyOffer(arg):
        '''
        ModifyOffer : Modify offer
                Input argu :
                    amount - amount of the product
                    price - price of the product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        amount = arg["amount"]
        price = arg["price"]
        ret = 1

        ##Modify offer
        xpath = Util.GetXpath({"locate": "modify_offer"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Modify offer", "result": "1"})
        AndroidChatConversationWindow.BargainForProduct({"amount": amount, "price": price, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindow.ModifyOffer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SendTextMessage(arg):
        '''
        SendTextMessage : Send text message in the conversation window
                Input argu :
                    string - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        string = arg["string"]
        ret = 1

        ##Send text message
        xpath = Util.GetXpath({"locate": "chat_edit"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": string, "result": "1"})

        ##Click send
        xpath = Util.GetXpath({"locate": "send_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindow.SendTextMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFrequentlyMessage(arg):
        '''
        InputFrequentlyMessage : Input frequently message in frequently used message page
                Input argu :
                    frequently_message - frequently_message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        frequently_message = arg["frequently_message"]
        ret = 1

        ##Input frequently used message
        xpath = Util.GetXpath({"locate": "frequently_message"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": frequently_message, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindow.InputFrequentlyMessage -> ' + frequently_message)


class AndroidChatConversationWindowButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMore(arg):
        '''
        ClickMore : Click + icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click + icon in the conversation page
        xpath = Util.GetXpath({"locate": "plus_icon"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click + Icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Click buy now
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy now
        xpath = Util.GetXpath({"locate": "buy_now_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click buy now", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMakeOffer(arg):
        '''
        ClickMakeOffer : Click Make offer
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Make offer (Bargain the item)
        xpath = Util.GetXpath({"locate": "make_offer_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click Make offer", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickMakeOffer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBargainDetail(arg):
        '''
        ClickBargainDetail : Click to go to bargain detail
                Input argu :
                    price - Choose a price of the bargain to go
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        price = arg["price"]
        ret = 1

        ##Go to bargain detail
        xpath = Util.GetXpath({"locate": "go_to_bargain_detail"})
        xpath = xpath.replace('price_replace', price)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to go to bargain detail", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickBargainDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRegretThisBargain(arg):
        '''
        ClickRegretThisBargain : Click to regret this bargain
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Regret this bargain
        xpath = Util.GetXpath({"locate": "regret_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Regret this bargain", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickRegretThisBargain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerTakeTheOffer(arg):
        '''
        ClickSellerTakeTheOffer : Seller take the offer
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Seller take the offer
        xpath = Util.GetXpath({"locate": "accept_offer"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Seller take the offer", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickSellerTakeTheOffer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerRejectTheOffer(arg):
        '''
        ClickSellerRejectTheOffer : Seller reject the offer
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Regret this bargain
        xpath = Util.GetXpath({"locate": "reject_offer"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Seller reject the offer", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickSellerRejectTheOffer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyerMakeAnNewOffer(arg):
        '''
        ClickBuyerMakeAnNewOffer : Buyer make a new offer offer
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Buyer make a new offer offer
        xpath = Util.GetXpath({"locate": "re_offer_again"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Buyer make a new offer offer", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickBuyerMakeAnNewOffer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLatestPicture(arg):
        '''
        ClickLatestPicture : Click latest picture in chat room
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click latest picture which count from bottom to top
        xpath = Util.GetXpath({"locate": "picture"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click picture", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickLatestPicture')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadImage(arg):
        '''
        ClickDownloadImage : Click download image to save picture
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download image to save picture
        xpath = Util.GetXpath({"locate": "save_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click download image button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickDownloadImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickActionIcon(arg):
        '''
        ClickActionIcon : Click action icon at upper right corner in chat room page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click action icon
        xpath = Util.GetXpath({"locate": "action_icon"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click action icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickActionIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseAction(arg):
        '''
        ClickChooseAction : Click choose action button after action icon(three dots at upper right corner)
                Input argu :
                    action - view_profile / back_homepage / search / report_user / block_user / unblock_user / need_help / delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action = arg["action"]
        ret = 1

        ##Click choose action button
        xpath = Util.GetXpath({"locate": action})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click action button -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickChooseAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseConfirmedOption(arg):
        '''
        ClickChooseConfirmedOption : Click yes or cancel button(can use in block user page and go to url message)
                Input argu :
                    action - yes / no
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action = arg["action"]
        ret = 1

        ##Click confirmed option button(yes or no)
        xpath = Util.GetXpath({"locate": action})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirmed option button -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickChooseConfirmedOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStickerIcon(arg):
        '''
        ClickStickerIcon : Click sticker icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click sticker icon
        xpath = Util.GetXpath({"locate": "sticker_icon"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click sticker icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickStickerIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickModifyFrequentlyMessagesIcon(arg):
        '''
        ClickModifyFrequentlyMessagesIcon : Click modify frequently messages icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click modify frequently messages icon
        xpath = Util.GetXpath({"locate": "edit_frequently_messages_icon"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click edit frequently messages icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickModifyFrequentlyMessagesIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFrequentlyMessages(arg):
        '''
        ClickFrequentlyMessages : Click frequently messages
                Input argu :
                    frequently_message - frequently message
                    page_type - chat / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        frequently_message = arg["frequently_message"]
        page_type = arg["page_type"]
        ret = 1

        ##Click frequently messages
        xpath = Util.GetXpath({"locate": page_type + "_frequently_message"})
        xpath = xpath.replace("replace_frequently_message", frequently_message)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click frequently messages -> " + frequently_message, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickFrequentlyMessages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit button in frequently used message page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFrequentlyMessagesToggle(arg):
        '''
        ClickFrequentlyMessagesToggle : Click frequently messages toggle
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click frequently messages toggle button
        xpath = Util.GetXpath({"locate": "toggle_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click frequently messages toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickFrequentlyMessagesToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMessageContent(arg):
        '''
        ClickMessageContent : Click messages content at conversation window
                Input argu :
                    content - content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        content = arg["content"]
        ret = 1

        ##Click message content
        xpath = Util.GetXpath({"locate": "content_message"})
        xpath = xpath.replace("replace_content_message", content)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click content messages -> " + content, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickMessageContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendMessage(arg):
        '''
        ClickSendMessage : Click send messages at conversation bar
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send message button
        xpath = Util.GetXpath({"locate": "send_message_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send messages button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatConversationWindowButton.ClickSendMessage')


class AndroidBargainDetailPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBargainHistory(arg):
        '''
        ClickBargainHistory : Click bargain history
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bargain history
        xpath = Util.GetXpath({"locate": "history_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to bargain history", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBargainDetailPageButton.ClickBargainHistory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToBargainItemList(arg):
        '''
        ClickGoToBargainItemList : Go to bargain item list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bargain item list
        xpath = Util.GetXpath({"locate": "item_list_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click bargain item list", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBargainDetailPageButton.ClickGoToBargainItemList')


class AndroidChatCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyPurchasesProduct(arg):
        '''
        ClickMyPurchasesProduct : Click product from my purchases page
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        product_name = arg["product_name"]
        ret = 1

        ##Click my purchases product info to get in order detail page
        xpath = Util.GetXpath({"locate": "purchase_product"})
        xpath = xpath.replace("replace_product_name", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click my purhcases product info", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatCommonButton.ClickMyPurchasesProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        '''
        ClickChatIcon : Click chat icon
                Input argu :
                    page_type - pdp / cart / shopee_live / my_purchases / my_shop / me / seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click chat icon
        xpath = Util.GetXpath({"locate": page_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click chat icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatCommonButton.ClickChatIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCartIcon(arg):
        '''
        ClickCartIcon : Click cart icon
                Input argu :
                    page_type - pdp
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click cart icon
        xpath = Util.GetXpath({"locate": page_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cart icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidChatCommonButton.ClickCartIcon')


class AndroidChatCommonPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToChatSetting(arg):
        '''
        GoToChatSetting : Go to chat setting
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click setting button
        xpath = Util.GetXpath({"locate": "setting_icon"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "1", "result": "1"})
        time.sleep(2)

        ##Click chat setting button
        xpath = Util.GetXpath({"locate": "chat_setting_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click chat setting button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidChatCommonPage.GoToChatSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SendPictureToChatRoom(arg):
        '''
        SendPictureToChatRoom : Send picture to chat room
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select picture
        xpath = Util.GetXpath({"locate": "select_picture"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Select picture", "result": "1"})
        time.sleep(2)

        ##Click pick up button
        xpath = Util.GetXpath({"locate": "pick_up_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click pick up next button", "result": "1"})
        time.sleep(2)

        ##Click pick up confirm button
        xpath = Util.GetXpath({"locate": "pick_up_confirm"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click pick up confirm button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidChatCommonPage.SendPictureToChatRoom')
