import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCTransparentBackgroundImage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCTransparentBackgroundImage Precondition =="
        dumplogger.info("== Setup MYPCTransparentBackgroundImage Precondition ==")

    def test_MYPCTransparentBackgroundImage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCTransparentBackgroundImage-01.xml")

    def test_MYPCTransparentBackgroundImage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCTransparentBackgroundImage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCTransparentBackgroundImage Post condition =="
        dumplogger.info("== Setup MYPCTransparentBackgroundImage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
