import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCSellerTxnFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSellerTxnFee Precondition =="
        dumplogger.info("== Setup IDPCSellerTxnFee Precondition ==")

    def test_IDPCSellerTxnFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerTxnFee-01.xml")

    def test_IDPCSellerTxnFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerTxnFee-02.xml")

    def test_IDPCSellerTxnFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerTxnFee-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSellerTxnFee Post condition =="
        dumplogger.info("== Setup IDPCSellerTxnFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
