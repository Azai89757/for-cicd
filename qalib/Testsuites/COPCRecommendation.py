import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class COPCRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCRecommendation Precondition =="
        dumplogger.info("== Setup COPCRecommendation Precondition ==")

    def test_COPCRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCRecommendation-01.xml")

    def test_COPCRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCRecommendation-02.xml")

    def test_COPCRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCRecommendation-03.xml")

    def test_COPCRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCRecommendation-04.xml")

    def test_COPCRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCRecommendation-05.xml")

    def test_COPCRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCRecommendation-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCRecommendation Post condition =="
        dumplogger.info("== Setup COPCRecommendation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
