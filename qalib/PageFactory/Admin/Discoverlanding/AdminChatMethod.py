#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminChatMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def LaunchChatAdmin(arg):
    '''
    LaunchChatAdmin : Go to chat admin page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    ChatAdmin_cookie = {}
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

    if 'ChatAdmin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:

        ##Using sql command to get cookie name and value in specific env and country
        assign_list = [{"column": "env", "value_type": "string"}]
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": "admin_cookie_chat", "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        ChatAdmin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        ChatAdmin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        ChatAdmin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['ChatAdmin'] = [ChatAdmin_cookie]

    ##chat admin url
    url = "https://admin.chat." + Config._EnvType_ + ".shopee.io"

    ##Go to url
    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type": "ChatAdmin", "result": "1"})
    time.sleep(5)

    ##Go to Chat Admin
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})
    time.sleep(10)

    ##Choose country
    ##Click drop down list
    xpath = Util.GetXpath({"locate": "drop_down_list"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click drop down list", "result": "1"})

    ##Click country
    xpath = Util.GetXpath({"locate": "country"})
    xpath = xpath.replace('replace_country', Config._TestCaseRegion_)
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click country", "result": "1"})
    time.sleep(10)

    OK(ret, int(arg['result']), 'LaunchChatAdmin')


class AdminChatCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnLeftPanel(arg):
        '''
        ClickTabOnLeftPanel : Click a tab at the left panel
                Input argu :
                    type - chat_conversations / all_stickers
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatCommonButton.ClickTabOnLeftPanel -> ' + type)


class AdminChatHistoryLogButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button
                Input argu :
                    page_type - chat_conversations / user_chatted
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click search button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset button
                Input argu :
                    page_type - chat_conversations / user_chatted / message_info
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click reset button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatLogWithUserName(arg):
        '''
        ClickChatLogWithUserName : Click chat log with user name
                Input argu :
                    username - username
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        username = arg["username"]

        ##Click chat log with user name
        xpath = Util.GetXpath({"locate": "chat_log"})
        xpath = xpath.replace("replace_username", username)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click chat log with user name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickChatLogWithUserName -> ' + username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadChat(arg):
        '''
        ClickDownloadChat : Click download chat
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download chat
        xpath = Util.GetXpath({"locate": "download_chat"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download_chat", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickDownloadChat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReloadMessage(arg):
        '''
        ClickReloadMessage : Click reload message
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reload message
        xpath = Util.GetXpath({"locate": "reload_message"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reload message", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickReloadMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click filter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter
        xpath = Util.GetXpath({"locate": "filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMessages(arg):
        '''
        ClickFilterMessages : Click filter messages button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter messages button
        xpath = Util.GetXpath({"locate": "filter_messages"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter messages button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickFilterMessages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClearFilters(arg):
        '''
        ClickClearFilters : Click clear filters button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click clear filters button
        xpath = Util.GetXpath({"locate": "clear_filters"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click clear filters button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickClearFilters')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConversationMessage(arg):
        '''
        ClickConversationMessage : Click conversation message
                Input argu :
                    message - message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message = arg["message"]
        ret = 1

        ##Click message
        xpath = Util.GetXpath({"locate": "message"})
        xpath = xpath.replace("replace_message", message)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click message", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickConversationMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConversationMedia(arg):
        '''
        ClickConversationMedia : Click conversation media(include sticker, image and video)
                Input argu :
                    action - image / video
                    src - src (ex: set '0002@2x.png')
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action = arg["action"]
        src = arg["src"]
        ret = 1

        ##Click image / sticker / video
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("replace_src", src)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickConversationMedia')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConversationProduct(arg):
        '''
        ClickConversationProduct : Click conversation product
                Input argu :
                    type - product / order / offer
                    id - id (product -> item id, order -> order id, offer -> offer id)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        id = arg["id"]
        ret = 1

        ##Click product with different type
        xpath = Util.GetXpath({"locate": "product"})
        xpath = xpath.replace("replace_type", type)
        xpath = xpath.replace("replace_id", id)
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click product type -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogButton.ClickConversationProduct')


class AdminChatHistoryLogPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserId(arg):
        '''
        InputUserId : Input user id
                Input argu :
                    page_type - chat_conversations / user_chatted
                    user_id - user_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        user_id = arg["user_id"]
        ret = 1

        ##Input user id
        xpath = Util.GetXpath({"locate": page_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogPage.InputUserId -> ' + user_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAttribute(arg):
        '''
        InputAttribute : Input attribute at message information
                Input argu :
                    attribute - attribute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        attribute = arg["attribute"]
        ret = 1

        ##Input attribute
        xpath = Util.GetXpath({"locate": "message_info_search_bar"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": attribute, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogPage.InputAttribute -> ' + attribute)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseFilterDate(arg):
        '''
        ChooseFilterDate : Choose filter date
                Input argu :
                    choose_time - ex: 2021-12
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        choose_time = arg["choose_time"]
        ret = 1

        ##Get local time
        local_time = XtFunc.GetCurrentDateTime("%Y-%m")
        local_year = int(local_time.split('-')[0])
        local_month = int(local_time.split('-')[1])

        ##Get target year and month
        target_year = int(choose_time.split('-')[0])
        target_month = int(choose_time.split('-')[1])

        ##To locate correct month page(ex: want to choose 2021-12 but page display 2022-02, then click back button twice)
        for times in range((local_year - target_year) * 12 + local_month - target_month):
            xpath = Util.GetXpath({"locate": "filter_month_back_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter month back button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogPage.ChooseFilterDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCalendarDate(arg):
        '''
        ChooseCalendarDate : Choose calendar date
                Input argu :
                    start_time - ex: 2021-12-12
                    end_time - ex: 2021-12-15
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        ret = 1

        ##Click start time
        xpath = Util.GetXpath({"locate": "calendar_start_time"})
        xpath = xpath.replace("replace_start_time", start_time)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click calendar start time -> " + start_time, "result": "1"})

        ##Click end time
        xpath = Util.GetXpath({"locate": "calendar_end_time"})
        xpath = xpath.replace("replace_end_time", end_time)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click calendar end time -> " + end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatHistoryLogPage.ChooseCalendarDate')


class AdminChatAllStickersButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStickersStatus(arg):
        '''
        ClickStickersStatus : Click stickers status
                Input argu :
                    status - all / published / pending_publication / pending / archived / drafts
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        status = arg["status"]
        ret = 1

        ##Click stickers status button
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status button -> " + status, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickStickersStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseAction(arg):
        '''
        ClickChooseAction : Click action button in all stickers menu page
                Input argu :
                    sticker_title - sticker title
                    action - review / download / edit / archive / delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        sticker_title = arg["sticker_title"]
        action = arg["action"]
        ret = 1

        ##Click stickers action button
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("replace_sticker_title", sticker_title)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status button -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickChooseAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickYes(arg):
        '''
        ClickYes : Click Yes button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Yes button
        xpath = Util.GetXpath({"locate": "yes_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Yes button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickYes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNo(arg):
        '''
        ClickNo : Click No button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click No button
        xpath = Util.GetXpath({"locate": "no_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click No button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickNo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddUploadNewStickers(arg):
        '''
        ClickAddUploadNewStickers : Click add / upload new stickers button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add / upload new stickers button
        xpath = Util.GetXpath({"locate": "add_upload_new_stickers"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add upload new stickers button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickAddUploadNewStickers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        '''
        ClickNext : Click next button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next button
        xpath = Util.GetXpath({"locate": "next_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickNext')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpiry(arg):
        '''
        ClickExpiry : Click expiry button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click expiry button
        xpath = Util.GetXpath({"locate": "expiry_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click expiry button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickExpiry')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
        ClickUpload : Click upload button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove button
                Input argu :
                    page_type - main / hidden
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click remove button
        xpath = Util.GetXpath({"locate": page_type})
        if page_type == "main":
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove button", "result": "1"})
        else:
            BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath, "locatehidden": xpath, "message": "Click remove button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveDraft(arg):
        '''
        ClickSaveDraft : Click save as draft button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save as draft button
        xpath = Util.GetXpath({"locate": "save_draft_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save as draft button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickSaveDraft')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu :
                    page_type - main / sub
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": page_type})

        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrevious(arg):
        '''
        ClickPrevious : Click previous button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click previous button
        xpath = Util.GetXpath({"locate": "previous_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click previous button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickPrevious')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackTemplate(arg):
        '''
        ClickBackTemplate : Click back template button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back template button
        xpath = Util.GetXpath({"locate": "back_template_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back template button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickBackTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLeave(arg):
        '''
        ClickLeave : Click leave button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click leave button
        xpath = Util.GetXpath({"locate": "leave_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click leave button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickLeave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClearAll(arg):
        '''
        ClickClearAll : Click clear all button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click clear all button
        xpath = Util.GetXpath({"locate": "clear_all_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click clear all button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickClearAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickView(arg):
        '''
        ClickView : Click view button at Upload Stickers and Subtitles page, need put the mouse on the image
                Input argu :
                    locate_num - the locate number of the picture (ex: 1)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        locate_num = arg["locate_num"]
        ret = 1

        ##Click view button
        xpath = Util.GetXpath({"locate": "view_button"})
        xpath = xpath.replace("replace_locate_num", locate_num)
        BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath, "locatehidden": xpath, "message": "Click view button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close button
        xpath = Util.GetXpath({"locate": "close_button"})
        BaseUICore.SendKeyboardEvent({"locate":xpath, "sendtype": "esc", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDownIcon(arg):
        '''
        ClickDropDownIcon : Click dropdown icon
                Input argu :
                    locate_num - the locate number of the dropdown icon (ex: 1)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        locate_num = arg["locate_num"]
        ret = 1

        ##Click dropdown icon
        xpath = Util.GetXpath({"locate": "dropdown_icon"})
        xpath = xpath.replace("replace_locate_num", locate_num)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickDropDownIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemTitle(arg):
        '''
        ClickItemTitle : Click item title(breadcrumbs) at the top side of the page
                Input argu :
                    item_title - item title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        item_title = arg["item_title"]
        ret = 1

        ##Click item title
        xpath = Util.GetXpath({"locate": "breadcrumbs"})
        xpath = xpath.replace("replace_item_title", item_title)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item title -> " + item_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickItemTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit button
                Input argu :
                    page_type - main / sub
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        ret = 1

        ##Click submit button
        xpath = Util.GetXpath({"locate": page_type})

        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersButton.ClickSubmit')


class AdminChatAllStickersPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStickerPackTitle(arg):
        '''
        InputStickerPackTitle : Input sticker pack title
                Input argu :
                    title - title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        title = arg["title"]
        ret = 1

        ##Input title
        xpath = Util.GetXpath({"locate": "input_title"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersPage.InputStickerPackTitle -> ' + title)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDateTime(arg):
        '''
        InputDateTime : Input date time
                Input argu :
                    page_type - live_date / expiry_date
                    time - time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_type = arg["page_type"]
        time = arg["time"]
        ret = 1

        ##Assemble date time
        assemble_time = XtFunc.GetCurrentDateTime("%d/%m/%Y %H:%M", 0, int(time), 0, 1)

        ##Click calander input field
        xpath = Util.GetXpath({"locate": page_type + "_calendar_selector"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set calander -> " + page_type, "result": "1"})

        ##Input date time
        xpath = Util.GetXpath({"locate": page_type + "_input_time"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": assemble_time, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersPage.InputDateTime -> ' + assemble_time)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSubtitle(arg):
        '''
        InputSubtitle : Input subtitile
                Input argu :
                    subtitle - subtitle
                    locate_num -  the locate number of the picture (ex: 1)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        locate_num = arg["locate_num"]
        subtitle = arg["subtitle"]
        ret = 1

        ##Input subtitle
        xpath = Util.GetXpath({"locate": "input_subtitle"})
        xpath = xpath.replace("replace_locate_num", locate_num)
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": subtitle, "result": "1"})

        OK(ret, int(arg['result']), 'AdminChatAllStickersPage.InputSubtitle -> ' + subtitle)
