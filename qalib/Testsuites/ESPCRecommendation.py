import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class ESPCRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCRecommendation Precondition =="
        dumplogger.info("== Setup ESPCRecommendation Precondition ==")

    def test_ESPCRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-01.xml")

    def test_ESPCRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-02.xml")

    def test_ESPCRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-03.xml")

    def test_ESPCRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-04.xml")

    def test_ESPCRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-05.xml")

    def test_ESPCRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-06.xml")

    def test_ESPCRecommendation08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCRecommendation-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCRecommendation Post condition =="
        dumplogger.info("== Setup ESPCRecommendation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
