import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDAPIMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIMicrosite Precondition =="
        dumplogger.info("== Setup IDAPIMicrosite Precondition ==")

    def test_IDAPIMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIMicrosite-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIMicrosite Post condition =="
        dumplogger.info("== Setup IDAPIMicrosite Post condition ==")
