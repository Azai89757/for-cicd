import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class VNPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCHomePage Precondition =="
        dumplogger.info("== Setup VNPCHomePage Precondition ==")

    def test_VNPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePage-01.xml")

    def test_VNPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePage-02.xml")

    def test_VNPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCHomePage Post condition =="
        dumplogger.info("== Setup VNPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
