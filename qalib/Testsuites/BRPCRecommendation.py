import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class BRPCRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCRecommendation Precondition =="
        dumplogger.info("== Setup BRPCRecommendation Precondition ==")

    def test_BRPCRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRecommendation-01.xml")

    def test_BRPCRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRecommendation-02.xml")

    def test_BRPCRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRecommendation-03.xml")

    def test_BRPCRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRecommendation-04.xml")

    def test_BRPCRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRecommendation-05.xml")

    def test_BRPCRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCRecommendation-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCRecommendation Post condition =="
        dumplogger.info("== Setup BRPCRecommendation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
