﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminGrowthMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AdminReorderItemPosition(arg):
    '''
    AdminReorderItemPosition : Reorder item  position
        Input argu :
            order - rule order
            reorder - which order want to change
        Return code :
            1 - success
            0 - fail
            -1 - error
    '''
    ret = 1
    order = arg["order"]
    reorder = arg["reorder"]

    ##Click change item order button
    xpath = Util.GetXpath({"locate": "change_item_order"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change item order button", "result": "1"})

    ##Get element original order
    xpath = Util.GetXpath({"locate": "order_item"})
    xpath = xpath.replace("order_item_to_be_replace", order)

    ##Get element target order
    xpath_target = Util.GetXpath({"locate": "order_item"})
    xpath_target = xpath_target.replace("order_item_to_be_replace", reorder)

    ##Drag element
    BaseUICore.DragAndDrop({"drag_elem": xpath, "target_elem": xpath_target, "result": "1"})

    ##Click save button
    xpath = Util.GetXpath({"locate": "save"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

    OK(ret, int(arg["result"]), 'AdminReorderItemPosition')


class AdminFreeGiftPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCsvFile(arg):
        '''
        UploadCsvFile : Upload csv file
                Input argu :
                    upload_type - replace_whole_table / append_new_rows
                    file_name - csv file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        upload_type = arg['upload_type']
        file_name = arg['file_name']
        ret = 1

        if upload_type == "replace_whole_table":

            ##Upload free gift csv file with replace whole table
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "free-gift-1", "element_type": "id", "project": "WelcomePackage", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        else:
            ##Upload free gift csv file with append new rows
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "free-gift-2", "element_type": "id", "project": "WelcomePackage", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": upload_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.UploadCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextButton(arg):
        '''
        ClickNextButton : Click next button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click page button
        xpath = Util.GetXpath({"locate": "page_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.ClickNextButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputInSearchField(arg):
        '''
        InputInSearchField : Input search keyword in search field
                Input argu :
                    field_type - shop_id / item_id / model_id / rebate / price
                    keyword - keyword for search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field_type = arg["field_type"]
        keyword = arg["keyword"]

        ##Search keyword
        xpath = Util.GetXpath({"locate": field_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": keyword, "result": "1"})

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.InputInSearchField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditFreeGiftButton(arg):
        '''
        ClickEditFreeGiftButton : Click edit free gift button
                Input argu :
                    item_id - product item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click edit free gift button
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace('replaced_text', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit free gift button", "result": "1"})

        ##Edit rebate
        xpath = Util.GetXpath({"locate": "rebate_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "20", "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.ClickEditFreeGiftButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteItemButton(arg):
        '''
        ClickDeleteItemButton : Click delete button
                Input argu :
                    item_id - product item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click delete button in free gift
        xpath = Util.GetXpath({"locate": "delete_button_in_free_gift"})
        xpath = xpath.replace('replaced_text', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button in free gift page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.ClickDeleteItemButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchDeleteButton(arg):
        '''
        ClickBatchDeleteButton : Click batch delete button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch delete button
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "batch_delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.ClickBatchDeleteButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCheckbox(arg):
        '''
        ClickItemCheckbox : Click item checkbox
                Input argu :
                    item_id - product item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click item checkbox
        xpath = Util.GetXpath({"locate": "item_checkbox_in_free_gift"})
        xpath = xpath.replace('replaced_text', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item checkbox in free gift", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeGiftPage.ClickItemCheckbox')


class AdminExclusiveDealsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCsvFile(arg):
        '''
        UploadCsvFile : Upload csv file
                Input argu :
                    upload_type - replace_whole_table / append_new_rows
                    file_name - csv file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        upload_type = arg['upload_type']
        file_name = arg['file_name']
        ret = 1

        if upload_type == "replace_whole_table":

            ##Upload exclusive deal csv file with replace whole table
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "exclusive-deal-1", "element_type": "id", "project": "WelcomePackage", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        else:
            ##Upload exclusive deal csv file with append new rows
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "exclusive-deal-2", "element_type": "id", "project": "WelcomePackage", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": upload_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.UploadCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputInSearchField(arg):
        '''
        InputInSearchField : Input search keyword in search field
                Input argu :
                    field_type - shop_id / item_id / model_id / rebate / promotion_price
                    keyword - keyword for search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field_type = arg["field_type"]
        keyword = arg["keyword"]

        ##Search keyword
        xpath = Util.GetXpath({"locate": field_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": keyword, "result": "1"})

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.InputInSearchField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditExclusiveDealButton(arg):
        '''
        ClickEditExclusiveDealButton : Click edit exclusive deal button
                Input argu :
                    item_id - product item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click edit exclusive deal button
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace('replaced_text', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit exclusive deal button", "result": "1"})

        ##Edit promotion price
        xpath = Util.GetXpath({"locate": "promotion_price_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "5", "result": "1"})

        ##Edit rebate
        xpath = Util.GetXpath({"locate": "rebate_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "20", "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.ClickEditExclusiveDealButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteItemButton(arg):
        '''
        ClickDeleteItemButton : Click delete button
                Input argu :
                    item_id - product item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click delete button in exclusive deal
        xpath = Util.GetXpath({"locate": "delete_button_in_exclusive_deal"})
        xpath = xpath.replace('replaced_text', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button in exclusive deal page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.ClickDeleteItemButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCheckbox(arg):
        '''
        ClickItemCheckbox : Click item checkbox
                Input argu :
                    item_id - product item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click item checkbox
        xpath = Util.GetXpath({"locate": "item_checkbox_in_exclusive_deal"})
        xpath = xpath.replace('replaced_text', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item checkbox in exclusive deal", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.ClickItemCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextButton(arg):
        '''
        ClickNextButton : Click next button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click page button
        xpath = Util.GetXpath({"locate": "page_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.ClickNextButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchDeleteButton(arg):
        '''
        ClickBatchDeleteButton : Click batch delete button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch delete button
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "batch_delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusiveDealsPage.ClickBatchDeleteButton')


class AdminCampaignPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewCampaign(arg):
        '''
        ClickAddNewCampaign : Click add new campaign button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new campaign button
        xpath = Util.GetXpath({"locate": "add_new_campaign_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new campaign button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.ClickAddNewCampaign')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButtonInCampaign(arg):
        '''
        ClickSaveButtonInCampaign : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.ClickSaveButtonInCampaign')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignNameField(arg):
        '''
        InputCampaignNameField : Input campaign name field
                Input argu :
                    campaign_name - campaign name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign_name = arg['campaign_name']

        ##Input campaign name
        xpath = Util.GetXpath({"locate":"campaign_name_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":campaign_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.InputCampaignNameField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignStartTime(arg):
        '''
        InputCampaignStartTime : Input start time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time_day = arg['start_time_day']
        start_time_min = arg['start_time_min']

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", int(start_time_day), int(start_time_min), 0)

        ##Input start time
        xpath = Util.GetXpath({"locate":"start_period"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.InputCampaignStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignNameSearchField(arg):
        '''
        InputCampaignSearchField : Input search keyword in campaign name search field
                Input argu :
                    keyword - keyword for search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        keyword = arg["keyword"]

        ##search campaign keyword
        xpath = Util.GetXpath({"locate": "name_search_field", "filename": Util.FileName(__file__)})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": keyword, "result": "1"})

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.InputCampaignSearchField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignEndTime(arg):
        '''
        InputCampaignEndTime : Input end time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time_day = arg['end_time_day']
        end_time_min = arg['end_time_min']

        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", int(end_time_day), int(end_time_min), 0)

        ##Input end time
        xpath = Util.GetXpath({"locate":"end_period"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.InputCampaignEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromotionId(arg):
        '''
        InputPromotionId : Input promotion id
                Input argu :
                    promotion_field_order - first / second / third
                    promotion_id - promotion id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_field_order = arg["promotion_field_order"]
        promotion_id = arg["promotion_id"]

        ##Input promotion id
        xpath = Util.GetXpath({"locate":promotion_field_order})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":promotion_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.InputPromotionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateGiftBanner(arg):
        '''
        CreateGiftBanner : Create gift banner
                Input argu :
                    file_name : file name
                    file_type : file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        ret = 1

        ##Upload gift banner
        AdminCampaignPage.UploadGiftBannerImg({"element": "banner-picker", "element_type": "id", "project": "WelcomePackage", "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminCampaignPage.CreateGiftBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadGiftBannerImg(arg):
        '''
        UploadGiftBannerImg : Upload image for gift banner
                Input argu :
                    element - input element id or class
                    element_type - element type: id/name/class
                    project - your project name
                    action - image/file
                    image - image name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element = arg["element"]
        element_type = arg["element_type"]
        project = arg["project"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload gift banner image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": element, "element_type": element_type, "project": project, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        time.sleep(5)
        OK(ret, int(arg['result']), 'AdminCampaignPage.UploadGiftBannerImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickActionButton(arg):
        '''
        ClickActionButton : Click action button
                Input argu :
                    action - enable / disable / edit / duplicate / delete
                    campaign_name - campaign name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        campaign_name = arg["campaign_name"]

        ##Click action button
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace('replaced_text', campaign_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.ClickActionButton -->' + action)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignIdSearchField(arg):
        '''
        InputCampaignIdSearchField : Input search keyword in campaign id search field
                Input argu :
                    keyword - keyword for search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        keyword = arg["keyword"]

        ##search campaign id
        xpath = Util.GetXpath({"locate": "id_search_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": keyword, "result": "1"})

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignPage.InputCampaignIdSearchField')


class AdminDisplayControlPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisplayControlToggle(arg):
        '''
        ClickDisplayControlToggle : Click display control toggle
                Input argu :
                    action_type - banner / voucher / item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg["action_type"]

        ##Click control toggle
        BaseUICore.ExecuteScript({"script": "document.getElementById('" + action_type + "').click()", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickDisplayControlToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetDisplayControlToggleOn(arg):
        '''
        ResetDisplayControlToggleOn : Reset display control toggle
                Input argu :
                    action_type - banner / voucher / item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg["action_type"]

        ##If user condition is not show, open the toogle
        xpath = Util.GetXpath({"locate": action_type})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Open control toggle
            BaseUICore.ExecuteScript({"script": "document.getElementById('" + action_type + "').click()", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ResetDisplayControlToggleOn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddDisplayControl(arg):
        '''
        ClickAddDisplayControl : Click add button
                Input argu :
                    action_type - banner / voucher / item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg["action_type"]

        ##Click banner add button
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner add button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickAddDisplayControl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveDisplayControl(arg):
        '''
        ClickSaveDisplayControl : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        ##Handle Pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickSaveDisplayControl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBannerCondition(arg):
        '''
        ClickBannerCondition : Click banner condition dropdownlist and select order count or registration time
                Input argu :
                    condition_type - order count / registration time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        condition_type = arg["condition_type"]

        ##Click banner condition
        xpath = Util.GetXpath({"locate": "click_condition"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner condition", "result": "1"})

        ##Select condition
        xpath = Util.GetXpath({"locate": "banner_condition"})
        xpath = xpath.replace('replaced_text', condition_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + condition_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickBannerCondition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBannerOperator(arg):
        '''
        ClickBannerOperator : Click operator dropdownlist and select =,≠,<,≤,>,≥
                Input argu :
                    operator_type - =,≠,<,≤,>,≥
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        operator_type = arg["operator_type"]

        ##Click operator
        xpath = Util.GetXpath({"locate": "click_operator"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner operator", "result": "1"})

        ##Select operator
        xpath = Util.GetXpath({"locate": "banner_operator"})
        xpath = xpath.replace('replaced_text', operator_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select operator", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickBannerOperator')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBannerValue(arg):
        '''
        ClickBannerValue : Click value dropdownlist and select 1,2,3,4
                Input argu :
                    value_type - 1,2,3,4
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value_type = arg["value_type"]

        ##Click value
        xpath = Util.GetXpath({"locate": "click_value"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click value", "result": "1"})

        ##Select value
        xpath = Util.GetXpath({"locate": "banner_value"})
        xpath = xpath.replace('replaced_text', value_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + value_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickBannerValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherCondition(arg):
        '''
        ClickVoucherCondition : Click voucher condition dropdownlist and select order count or registration time
                Input argu :
                    condition_type - order count / registration time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        condition_type = arg["condition_type"]

        ##Click voucher condition
        xpath = Util.GetXpath({"locate": "click_condition"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher condition", "result": "1"})

        ##Select condition
        xpath = Util.GetXpath({"locate": "voucher_condition"})
        xpath = xpath.replace('replaced_text', condition_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + condition_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickVoucherCondition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherOperator(arg):
        '''
        ClickVoucherOperator : Click operator dropdownlist and select =,≠,<,≤,>,≥
                Input argu :
                    operator_type - =,≠,<,≤,>,≥
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        operator_type = arg["operator_type"]

        ##Click voucher operator
        xpath = Util.GetXpath({"locate": "click_operator"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click operator", "result": "1"})

        ##Select operator
        xpath = Util.GetXpath({"locate": "voucher_operator"})
        xpath = xpath.replace('replaced_text', operator_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select operator", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickVoucherOperator')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherValue(arg):
        '''
        ClickVoucherValue : Click value dropdownlist and select 1,2,3,4
                Input argu :
                    value_type - 1,2,3,4
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value_type = arg["value_type"]

        ##Click voucher value
        xpath = Util.GetXpath({"locate": "click_value"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher value", "result": "1"})

        ##Select value
        xpath = Util.GetXpath({"locate": "voucher_value"})
        xpath = xpath.replace('replaced_text', value_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + value_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickVoucherValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCondition(arg):
        '''
        ClickItemCondition : Click item condition dropdownlist and select order count or registration time
                Input argu :
                    condition_type - order count / registration time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        condition_type = arg["condition_type"]

        ##Click item condition
        xpath = Util.GetXpath({"locate": "click_condition"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item condition", "result": "1"})

        ##Select condition
        xpath = Util.GetXpath({"locate": "item_condition"})
        xpath = xpath.replace('replaced_text', condition_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + condition_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickItemCondition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemOperator(arg):
        '''
        ClickItemOperator : Click item operator dropdownlist and select =,≠,<,≤,>,≥
                Input argu :
                    operator_type - =,≠,<,≤,>,≥
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        operator_type = arg["operator_type"]

        ##Click item operator
        xpath = Util.GetXpath({"locate": "click_operator"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item operator", "result": "1"})

        ##Select operator
        xpath = Util.GetXpath({"locate": "item_operator"})
        xpath = xpath.replace('replaced_text', operator_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select operator", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickItemOperator')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemValue(arg):
        '''
        ClickItemValue : Click value dropdownlist and select 1,2,3,4
                Input argu :
                    value_type - 1,2,3,4
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value_type = arg["value_type"]

        ##Click item value
        xpath = Util.GetXpath({"locate": "click_value"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item value", "result": "1"})

        ##Select value
        xpath = Util.GetXpath({"locate": "item_value"})
        xpath = xpath.replace('replaced_text', value_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + value_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickItemValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeletetag(arg):
        '''
        ClickDeletetag : Click delete tag
                Input argu :
                    tag_type - banner / voucher / item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tag_type = arg["tag_type"]

        ##Click delete tag
        xpath = Util.GetXpath({"locate": tag_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner delete tag", "result": "1"})

        OK(ret, int(arg['result']), 'AdminDisplayControlPage.ClickDeletetag')


class AdminFormManagementButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate":"add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu :
                    name - form name
                    form_type - subscribe/ unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        form_type = arg['form_type']

        ##Click edit button
        xpath = Util.GetXpath({"locate":form_type})
        xpath = xpath.replace("form_name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button
                Input argu :
                    name - form name
                    form_type - subscribe / unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        form_type = arg['form_type']

        ##Click delete button
        xpath = Util.GetXpath({"locate":form_type})
        xpath = xpath.replace("form_name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        ##Popup handle
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminFormManagementButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicate(arg):
        '''
        ClickDuplicate : Click duplicate button
                Input argu :
                    name - form name
                    form_type - subscribe / unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        form_type = arg['form_type']

        ##Search form by form name
        AdminFormManagementPage.SearchForm({"form_type": form_type, "search_by": "form_name", "name": name, "result": "1"})
        time.sleep(2)

        ##Click duplicate button
        xpath = Util.GetXpath({"locate":form_type})
        xpath = xpath.replace("form_name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click duplicate button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementButton.ClickDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewSubmissions(arg):
        '''
        ClickViewSubmissions : Click view submissions button
                Input argu :
                    name - form name
                    form_type - subscribe / unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        form_type = arg['form_type']

        xpath = Util.GetXpath({"locate":form_type})
        xpath = xpath.replace("form_name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view submissions button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementButton.ClickViewSubmissions')


class AdminFormDetailButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFormRuleCheckbox(arg):
        '''
        ClickFormRuleCheckbox : Click form rule checkbox
                Input argu :
                    checkbox_type - multiple_submission / edit_submission / submission_count_limit / allow_unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        ##Click form rule checkbox
        xpath = Util.GetXpath({"locate":checkbox_type})
        xpath = xpath.replace("checkbox_type_to_be_replaced", checkbox_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click form rule checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickFormRuleCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAdd(arg):
        '''
        ClickAdd : Click add question
                Input argu :
                    question_type - free/phone/email/address/date/checkbox/single_choice/multiple_choice/file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        question_type = arg["question_type"]

        ##Click add question
        xpath = Util.GetXpath({"locate":question_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add %s question" % question_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickQuestionAction(arg):
        '''
        ClickQuestionAction : Click question action for delete/copy/expand/collapse
                Input argu :
                    question_type - free/phone/email/address/date/checkbox/single_choice/multiple_choice/file
                    action - delete/copy/expand/collapse
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        question_type = arg["question_type"]
        action = arg["action"]

        ##Click question action
        xpath = Util.GetXpath({"locate":action + "_btn"})
        if question_type == "single_choice":
            xpath = xpath.replace("question_type_to_be_replaced", "SC")
        elif question_type == "multiple_choice":
            xpath = xpath.replace("question_type_to_be_replaced", "MC")
        else:
            question_type = question_type.capitalize()
            xpath = xpath.replace("question_type_to_be_replaced", question_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s question" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickQuestionAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRequiredCheckbox(arg):
        '''
        ClickRequiredCheckbox : Click required checkbox
                Input argu :
                    question_type - free/phone/email/address/date/checkbox/single_choice/multiple_choice
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        question_type = arg["question_type"]

        ##Get all question type in global list
        xpath = Util.GetXpath({"locate": "question_type"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"multi", "result": "1"})
        all_question_type = GlobalAdapter.CommonVar._PageAttributesList_

        ##Click required checkbox base on the question of the list index
        if question_type == "single_choice":
            BaseUICore.ExecuteScript({"script":"document.getElementsByName('mandatory')[" + str(all_question_type.index("SC Question")) + "].click()", "result": "1"})
        elif question_type == "multiple_choice":
            BaseUICore.ExecuteScript({"script":"document.getElementsByName('mandatory')[" + str(all_question_type.index("MC Question")) + "].click()", "result": "1"})
        else:
            question_type = question_type.capitalize()
            BaseUICore.ExecuteScript({"script":"document.getElementsByName('mandatory')[" + str(all_question_type.index(question_type + " Question")) + "].click()", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickRequiredCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemoveOption(arg):
        '''
        ClickRemoveOption : Click remove option button
                Input argu :
                    question_type - sc/mc
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        question_type = arg["question_type"]
        question_type = question_type.upper()

        ##Click remove option button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        xpath = xpath.replace("question_type_to_be_replaced", question_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove option button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickRemoveOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFileCheckbox(arg):
        '''
        ClickFileCheckbox : Click file_type checkbox
                Input argu :
                    file_type - PDF / Image
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_type = arg["file_type"]

        ##Click file checkbox
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('" + file_type + "')[0].click()", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailButton.ClickFileCheckbox')


class AdminFormSubmissionsButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export button to export submissions
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export button
        xpath = Util.GetXpath({"locate": "export_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormSubmissionsButton.ClickExport')


class AdminFormManagementPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchForm(arg):
        '''
        SearchForm : search form by name or id
                Input argu :
                    search_by - form_name/form_id
                    name - form name
                    form_type - subscribe/unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_by = arg["search_by"]
        name = arg["name"]
        form_type = arg["form_type"]

        ##Search form by name or id
        if search_by == "form_name":
            xpath = Util.GetXpath({"locate":"form_name_" + form_type})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        elif search_by == "form_id":
            xpath = Util.GetXpath({"locate":"form_id_" + form_type})
            if form_type == "subscribe":
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.FormE2EVar._FormID_, "result": "1"})
            else:
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.FormE2EVar._UnsubFormID_, "result": "1"})

        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminFormManagementPage.SearchForm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetFormID(arg):
        '''
        GetFormID : Get form id in admin form management page
                Input argu :
                    name - form name
                    form_type - subscribe / unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        form_type = arg['form_type']

        ##Get form id
        xpath = Util.GetXpath({"locate":form_type})
        xpath = xpath.replace("form_name_to_be_replaced", name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":form_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.GetFormID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def TagForm(arg):
        '''
        TagForm : Map/Unmap tag to form
                Input argu :
                    action - map/unmap
                    tag - tag name
                    days - remove tag after days
                    resubscribe - 1 : check Re-subscribe box
                                  0 : uncheck Re-subscribe box
                    form - to get form id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        form = arg["form"]
        tag = arg["tag"]
        days = arg["days"]
        resubscribe = arg["resubscribe"]

        ##Input form id
        if form:
            AdminFormManagementPage.GetFormID({"name": form, "form_type":"subscribe", "result": "1"})
            xpath = Util.GetXpath({"locate": "tag_form_id"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._FormID_, "result": "1"})

        ##Input remove tag after days
        if days:
            xpath = Util.GetXpath({"locate": "remove_tag_after_days"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": days, "result": "1"})

        ##Select "Add Tag when Re-subscribe" check-box
        if not int(resubscribe):
            BaseUICore.ExecuteScript({"script": "document.getElementsByName('allow_retag')[0].click()", "result": "1"})

        ##Input tag name
        xpath = Util.GetXpath({"locate": "tag_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": tag, "result": "1"})

        ##Click map/unmap action
        xpath = Util.GetXpath({"locate":action + "_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action button -> %s" % action, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFormManagementPage.TagForm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LinkForm(arg):
        '''
        LinkForm : Link/Unlink forms
                Input argu :
                    action - link/unlink
                    form - to get form id
                    unsubscribe_form - to get form id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        form = arg["form"]
        unsubscribe_form = arg["unsubscribe_form"]

        ##Input form id
        xpath = Util.GetXpath({"locate": "link_form"})
        ##If input something
        if form:
            ##Input form_id from global
            if form == "form_id":
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._FormID_, "result": "1"})
            ##Input non-exist form_id from arg
            else:
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": form, "result": "1"})
            time.sleep(3)

        ##Input unsubscribe form id
        xpath = Util.GetXpath({"locate": "unsubscribe_form"})
        ##If input something
        if unsubscribe_form:
            ##Input form_id from global
            if unsubscribe_form == "unsubform_id":
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._UnsubFormID_, "result": "1"})
            ##Input non-exist form_id from arg
            else:
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": unsubscribe_form, "result": "1"})
            time.sleep(3)

        ##Click link/unlink action
        xpath = Util.GetXpath({"locate": action + "_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action button -> %s" % action, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFormManagementPage.LinkForm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLinkedFormDisplay(arg):
        '''
        CheckLinkedFormDisplay : Check Linked Form Display correctlly
                Input argu :
                    form - to get form id
                    unsubscribe_form - to get form id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        form = arg["form"]
        unsubscribe_form = arg["unsubscribe_form"]

        ##Check unsubscribe form_id is in linked form block
        AdminFormManagementPage.GetFormID({"name": unsubscribe_form, "form_type":"unsubscribe", "result": "1"})
        xpath = Util.GetXpath({"locate": "form_id_table_1"})
        xpath = xpath.replace("form_name_to_be_replaced", form)
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._UnsubFormID_, "isfuzzy": "1", "result": "1"})

        ##Check form_id is in linked unsubscribe-form block
        AdminFormManagementPage.GetFormID({"name": form, "form_type":"subscribe", "result": "1"})
        xpath = Util.GetXpath({"locate": "form_id_table_2"})
        xpath = xpath.replace("form_name_to_be_replaced", unsubscribe_form)
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._FormID_, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFormManagementPage.CheckLinkedFormDisplay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeSubmissionLimitFromDB(arg):
        '''
        ChangeSubmissionLimitFromDB : Change Submission Limit From DB
                Input argu :
                    form_title - form name
                    limit_num - limit number of submit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        form_title = arg["form_title"]
        limit_num = arg["limit_num"]

        AdminFormManagementPage.GetFormID({"name": form_title, "form_type":"subscribe", "result": "1"})

        ##define assign_list
        assign_list = [{"column": "form_id", "value_type": "int"},{"column": "submission_count_limit", "value_type": "int"}]

        ##Assign each flash sale id to global first
        GlobalAdapter.CommonVar._DynamicCaseData_["form_id"] = GlobalAdapter.FormE2EVar._FormID_
        GlobalAdapter.CommonVar._DynamicCaseData_["submission_count_limit"] = limit_num

        ##Send SQL command and store category id
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_form", "file_name":"changeLimit", "method":"update", "verify_result":"1", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.ChangeSubmissionLimitFromDB')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreFormList(arg):
        '''
        RestoreFormList : Delete form
                Input argu :
                    form_type - subscribe/ unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        form_type = arg["form_type"]

        ##Get all delete button in form
        xpath = Util.GetXpath({"locate":form_type})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"multi", "result": "1"})

        ##Store all delete btn into global
        all_delete_btn = GlobalAdapter.CommonVar._PageAttributesList_

        ##Delete all form
        for number in range(len(all_delete_btn)):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
            time.sleep(3)
        else:
            dumplogger.info("There is no form to delete.")

        OK(ret, int(arg["result"]), 'AdminFormManagementPage.RestoreFormList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReserveFormTime(arg):
        '''
        ReserveFormTime : Reserve date and time
                Input argu :
                    time_type - start_time/end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time_type = arg["time_type"]

        ##Reserve date and time
        xpath = Util.GetXpath({"locate":time_type})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"form_" + time_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.ReserveFormTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReserveUnsubscribeFormTime(arg):
        '''
        ReserveUnsubscribeFormTime : Reserve date and time
                Input argu :
                    time_type - start_time/end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time_type = arg["time_type"]

        ##Reserve date and time
        xpath = Util.GetXpath({"locate":time_type})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"form_" + time_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.ReserveUnsubscribeFormTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompareDateAndTime(arg):
        '''
        CompareDateAndTime : Check if reserved date and time same as date and time that shows on FE
                Input argu :
                        time_type - start_time / end_time / unsubscribe_start_time / unsubscribe_end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time_type = arg['time_type']

        ##Compare date time
        xpath = Util.GetXpath({"locate": time_type})
        if "start_time" in time_type:
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._StartTime_, "isfuzzy": "1", "result": "1"})
        elif "end_time" in time_type:
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.FormE2EVar._EndTime_, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.CompareDateAndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOperateTime(arg):
        '''
        CheckOperateTime : Check operate time is current time
                Input argu :
                        form_type - subscribe/ unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        form_type = arg["form_type"]

        if Config._TestCaseRegion_ == "TW":
            current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0) + " +0800"
        elif Config._TestCaseRegion_ == "ID" or Config._TestCaseRegion_ == "VN":
            current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0) + " +0700"

        ##Compare date time
        xpath = Util.GetXpath({"locate": form_type})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": current_time, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.CheckOperateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckFormURL(arg):
        '''
        CheckFormURL : Check form url format
                Input argu :
                        form_type - subscribe/ unsubscribe
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        form_type = arg["form_type"]

        ##Check form url format
        url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/program/form/"
        dumplogger.info("url = %s" % (url))

        xpath = Util.GetXpath({"locate": form_type})
        if form_type == "subscribe":
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": url + GlobalAdapter.FormE2EVar._FormID_, "isfuzzy": "0", "result": "1"})
        else:
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": url + GlobalAdapter.FormE2EVar._UnsubFormID_, "isfuzzy": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormManagementPage.CheckFormURL')


class AdminFormDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFormBasicInfo(arg):
        '''
        InputFormBasicInfo : Input form basic info
                Input argu :
                    title - title
                    description - description
                    start_time - start time
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        description = arg["description"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input form title
        xpath = Util.GetXpath({"locate": "title"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Input form description
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Input form start time
        if start_time:
            start_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", minutes=int(start_time))
            xpath = Util.GetXpath({"locate": "start_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time + " +0800", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Input form end time
        if end_time:
            end_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", minutes=int(end_time))
            xpath = Util.GetXpath({"locate": "end_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time + " +0800", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailPage.InputFormBasicInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFormRedirectPageInfo(arg):
        '''
        InputFormRedirectPageInfo : Input form success/already-signed-up page info
                Input argu :
                    page_type - success/already_signed_up
                    title - title
                    description - description
                    button_text - button text
                    button_url - button url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]
        title = arg["title"]
        description = arg["description"]
        button_text = arg["button_text"]
        button_url = arg["button_url"]

        ##Input success page title
        xpath = Util.GetXpath({"locate": "title"})
        xpath = xpath.replace("page_type_to_be_replaced", page_type)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Input success page description
        xpath = Util.GetXpath({"locate": "description"})
        xpath = xpath.replace("page_type_to_be_replaced", page_type)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Input success page button text
        xpath = Util.GetXpath({"locate": "button_text"})
        xpath = xpath.replace("page_type_to_be_replaced", page_type)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": button_text, "result": "1"})

        ##Input success page button url
        xpath = Util.GetXpath({"locate": "button_url"})
        xpath = xpath.replace("page_type_to_be_replaced", page_type)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": button_url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailPage.InputFormRedirectPageInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputQuestionDetail(arg):
        '''
        InputQuestionDetail : Input question detail
                Input argu :
                    question_type - free/phone/email/address/date/checkbox/single_choice/multiple_choice/file
                    title - title
                    placeholder - placeholder
                    required - yes is click checkbox, or don't need to fill in this field
                    option_amount - option amount, only SC/MC need to fill in this field
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        question_type = arg["question_type"]
        title = arg["title"]
        placeholder = arg["placeholder"]
        option_amount = arg["option_amount"]
        required = arg["required"]

        if question_type == "single_choice":
            question_type = "SC"
        elif question_type == "multiple_choice":
            question_type = "MC"
        else:
            question_type = question_type.capitalize()

        ##Input question detail title
        xpath = Util.GetXpath({"locate": "title"})
        xpath = xpath.replace("question_type_to_be_replaced", question_type)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Input question detail placeholder
        if placeholder:
            xpath = Util.GetXpath({"locate": "placeholder"})
            xpath = xpath.replace("question_type_to_be_replaced", question_type)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": placeholder, "result": "1"})

        if question_type == "SC" or question_type == "MC":
            ##Click add option button
            for number in range(int(option_amount)-1):
                xpath = Util.GetXpath({"locate":"add_option"})
                xpath = xpath.replace("question_type_to_be_replaced", question_type)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add option button", "result": "1"})

            ##Input question detail option
            for number in range(int(option_amount)):
                xpath = Util.GetXpath({"locate": "option"})
                xpath = xpath.replace("question_type_to_be_replaced", question_type)
                xpath = xpath.replace("number_to_be_replaced", str(number+1))
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": question_type + " option " + str(number+1), "result": "1"})

        ##Click required checkbox
        if required:
            AdminFormDetailButton.ClickRequiredCheckbox({"question_type": question_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailPage.InputQuestionDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRespondentsLimit(arg):
        '''
        InputRespondentsLimit : Input form basic info
                Input argu :
                    number - limit number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input limit of respondents
        xpath = Util.GetXpath({"locate": "number_input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFormDetailPage.InputRespondentsLimit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeQuestionSequence(arg):
        '''
        ChangeQuestionSequence : Change question sequence
                Input argu :
                    question_type - free/phone/email/address/date/checkbox/single_choice/multiple_choice
                    x_offset - x offset
                    y_offset - y offset
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        question_type = arg["question_type"]
        x_offset = arg['x_offset']
        y_offset = arg['y_offset']

        ##Change question sequence
        xpath_to_move = Util.GetXpath({"locate": question_type})
        BaseUICore.DragAndDropByOffset({"drag_elem": xpath_to_move, "x_offset": x_offset, "y_offset": y_offset, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFormDetailPage.ChangeQuestionSequence')


class AdminFormSubmissionsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSubmissionTime(arg):
        '''
        InputSubmissionTime : Input submission time for export file
                Input argu :
                    start_time - start time
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time
        start_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", minutes=int(start_time))
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Click blank place
        xpath = Util.GetXpath({"locate": "blank_place"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click blank place", "result": "1"})

        ##Input end time
        end_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", minutes=int(end_time))
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFormSubmissionsPage.InputSubmissionTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchFormSubmissions(arg):
        '''
        SearchFormSubmissions : Search form submissions
                Input argu :
                    search_by - user_id/ user_name/ shop_id/ submit_time/ unsubscribe_time
                    keyword - input keyword to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_by = arg["search_by"]
        keyword = arg["keyword"]

        current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, -60, 0)

        ##Input search box
        if search_by == "submit_time":
            xpath = Util.GetXpath({"locate": "start_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", -1, 0, 0), "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "end_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", 1, 0, 0), "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        elif search_by == "unsubscribe_time":
            xpath = Util.GetXpath({"locate": "unsubscribe_start_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", -1, 0, 0), "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "unsubscribe_end_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", 1, 0, 0), "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": search_by})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": keyword, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFormSubmissionsPage.SearchFormSubmissions')
