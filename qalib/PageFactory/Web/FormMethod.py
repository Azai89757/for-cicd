#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 FormMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import FrameWorkBase
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter
import Config

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def ComposeFormQRCodeUrl(arg):
    ''' ComposeFormQRCodeUrl : compose URL by Form QRCode
            Input argu : NA
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 1

    GlobalAdapter.FormE2EVar._QRCodeURL_ = "https://" + GlobalAdapter.UrlVar._Domain_ + "/universal-link/program/form/" + GlobalAdapter.FormE2EVar._FormID_ + "?deep_and_deferred=1&smtt=9"
    dumplogger.info("QRCode URL : %s" % (GlobalAdapter.FormE2EVar._QRCodeURL_))

    OK(ret, int(arg['result']), 'ComposeFormQRCodeUrl')
