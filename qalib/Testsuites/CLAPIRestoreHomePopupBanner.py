import unittest
from Config import dumplogger
from FrameWorkBase import *


class CLAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup CLAPIRestoreHomePopupBanner Precondition ==")

    def test_CLAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/CLAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup CLAPIRestoreHomePopupBanner Post condition ==")
