﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidPaymentMethod.py: The def of this file called by XML mainly.
'''

##import system library
import time
from datetime import date
from datetime import timedelta

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidCheckOutMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidPaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectPaymentMethod(arg):
        '''
        SelectPaymentMethod : Select a payment method in payment method page
                Input argu :
                    type - airpay/airpaywallet/banktransfer/cc_installment/cod/creditcard/ibanking/shopeewallet
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click payment method
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Select a payment method in payment method page", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectPaymentMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCardNumber(arg):
        '''
        SelectCardNumber : Select card number
                Input argu :
                    card_number - Choose card number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']

        ##If using credit card to pay, need to select one credit card
        xpath = Util.GetXpath({"locate":"credit_card_number"})
        xpath = xpath.replace("card_number_to_be_replace", card_number)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click credit card number", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectCardNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectBank(arg):
        '''
        SelectBank : Select bank if using ID bank transfer
                Input argu :
                    bank - Choose specific bank if using ID bank transfer (no need in TW and VN)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg['bank']

        ##If using ID bank transfer, need to select specific bank
        xpath = Util.GetXpath({"locate":"bank"})
        xpath = xpath.replace("bank_to_be_replace", bank)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bank", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectMolpayCashOption(arg):
        '''
        SelectMolpayCashOption : Select molpay cash option (for MY)
                Input argu :
                    option - 711/kkmart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Select molpay cash option
        xpath = Util.GetXpath({"locate":option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click molpay cash option", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectMolpayCashOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAirpayBankAccount(arg):
        '''
        SelectAirpayBankAccount : Select airpay bank account if using airpay bt
                Input argu :
                    bank_account - Choose specific bank account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank_account = arg['bank_account']

        ##Choose airpay bank account
        xpath = Util.GetXpath({"locate":"bank_account"})
        xpath = xpath.replace("account_to_be_replaced", bank_account)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bank account-> %s" % bank_account, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectAirpayBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCCInstallmentBankAndPeriod(arg):
        '''
        SelectCCInstallmentBankAndPeriod : Select CC installment bank and period
                Input argu :
                    bank - bank name
                    period - 3/6/9/12 x @ 0%
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg["bank"]
        period = arg["period"]

        ##Select cc installment period
        xpath = Util.GetXpath({"locate":"cc_installment_period"})
        xpath = xpath.replace('bank_to_be_replaced', bank)
        xpath = xpath.replace('period_to_be_replaced', period)

        ##If the installment period is expanded then just click
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cc installment period -> %s" % (period), "result": "1"})
        ##If doesn't then click credit card first to expand the drawer and then choose installment
        else:
            xpath_bank = Util.GetXpath({"locate":"bank"})
            xpath_bank = xpath_bank.replace('bank_to_be_replaced', bank)
            AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath_bank, "direction":"down", "isclick":"1", "result": "1"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cc installment period -> %s" % (period), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectCCInstallmentBankAndPeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchIBankingBank(arg):
        '''
        SearchIBankingBank : Searchibanking bank
                Input argu :
                    keyword - search keyword
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        keyword = arg["keyword"]

        ##Search bank
        xpath = Util.GetXpath({"locate":"search_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":keyword, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SearchIBankingBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectIBankingAccount(arg):
        '''
        SelectIBankingAccount : Select ibanking bank
                Input argu :
                    bank - bank name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg["bank"]

        if Config._TestCaseRegion_ != "TH":

            ##Search bank
            AndroidPaymentPage.SearchIBankingBank({"keyword":bank, "result":"1"})

        ##Click the bank account
        xpath = Util.GetXpath({"locate":"ibanking_account"})
        xpath = xpath.replace('bank_to_be_replaced', bank)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bank account", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectIBankingAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAkulakuPeriod(arg):
        '''
        SelectAkulakuPeriod : Select Akulaku Period
                Input argu :
                    period - Choose specific installment period 1/2/3/6/9/12
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        period = arg['period']

        ##Choose akulaku period
        xpath = Util.GetXpath({"locate":"period"})
        xpath = xpath.replace("period_to_be_replaced", period)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click akukaku period-> %s" % period, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectAkulakuPeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectKredivoOption(arg):
        '''
        SelectKredivoOption : Select Kredivo Option
                Input argu :
                    option - Choose kredivo option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Choose Kredivo Option
        xpath = Util.GetXpath({"locate":"kredivo_option"})
        xpath = xpath.replace("option_to_be_replace", option)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click kredivo option -> %s" % option, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentPage.SelectKredivoOption')


class AndroidPaymentButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCreditCard(arg):
        '''
        ClickAddCreditCard : To click Add credit card in page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(3)
        ##Click Add card
        xpath = Util.GetXpath({"locate":"add_new_card"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Add a credit card", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickAddCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddBankAccount(arg):
        '''
        ClickAddBankAccount : To click Add bank account in page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(3)
        ##Click Add bank account
        xpath = Util.GetXpath({"locate":"add_new_account"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Add a bank accounts", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickAddBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDone(arg):
        '''
        ClickDone : Check bank info done and click done btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done
        xpath = Util.GetXpath({"locate":"done_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click done", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditAccount(arg):
        '''
        ClickEditAccount : Click edit account button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit
        xpath = Util.GetXpath({"locate":"edit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickEditAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm delete account button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPay(arg):
        '''
        ClickPay : Click pay button in payment confirm page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pay button in payment confirm page
        xpath = Util.GetXpath({"locate":"pay_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click pay button in payment confirm page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickPay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        '''
        ClickAgree : Click agree button for bank account/credit card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click agree button
        xpath = Util.GetXpath({"locate":"agree_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click agree button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinue(arg):
        '''
        ClickContinue : Click continue button if popup ID SIPP VA message
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click continue button
        xpath = Util.GetXpath({"locate":"continue_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickContinue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button for bank account/credit card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        if Config._TestCaseRegion_ == "TW":
            ##Click action button
            xpath = Util.GetXpath({"locate":"action_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click action button", "result": "1"})

        ##Click delete button
        xpath = Util.GetXpath({"locate":"delete_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentButton.ClickDelete')


class AndroidBankAccountListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetDefaultBank(arg):
        '''
        SetDefaultBank : Set this bank account as default
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set default button
        xpath = Util.GetXpath({"locate":"set_account_default_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Set the account as default", "result": "1"})
        dumplogger.info("Click set default button.")

        ##If show otp page then need to input otp
        AndroidBankAccountListPage.InputVerificationCode({"code": "123456", "result": "1"})
        time.sleep(3)

        ##Click back button
        xpath = Util.GetXpath({"locate":"home_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Back to previous page", "result": "1"})
        dumplogger.info("Click set default button.")

        OK(ret, int(arg['result']), 'AndroidBankAccountListPage.SetDefaultBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVerificationCode(arg):
        '''
        InputVerificationCode : Input verfication code for adding a bank account
                Input argu :
                    code - verfication code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        code = arg["code"]

        ##Click resend button
        xpath = Util.GetXpath({"locate":"resend_btn"})

        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Input verification code, it's the only edittext in the page
            if Config._TestCaseRegion_ != "TW":
                for number in code:
                    AndroidBaseUICore.AndroidKeyInNumbers({"number":number, "result": "1"})
            else:
                xpath = Util.GetXpath({"locate":"input_column"})
                AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":code, "result": "1"})

            ##Add sleep to prevent the next click being skipped
            time.sleep(2)

            ##Click Add card
            xpath = Util.GetXpath({"locate":"continue_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Continue button to proceed", "result": "1"})
        else:
            dumplogger.info("Don't need to input verification code")

        OK(ret, int(arg['result']), 'AndroidBankAccountListPage.InputVerificationCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserInfo(arg):
        '''
        InputUserInfo : Input name and ID for bank account
                Input argu :
                    name - account name
                    pid - personal id
                    birthday - normal / invalid(future date)
                    city - city
                    area - area
                    address - address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        name = arg['name']
        pid = arg['pid']
        birthday = arg['birthday']
        city = arg['city']
        area = arg['area']
        address = arg['address']

        ##Input name
        if name:
            xpath = Util.GetXpath({"locate":"name"})
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        ##Input ID
        if pid:
            xpath = Util.GetXpath({"locate":"id"})
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":pid, "result": "1"})

        ##Choose city
        if city:
            xpath = Util.GetXpath({"locate":"city_choose"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click city list", "result": "1"})
            xpath = Util.GetXpath({"locate":"city"})
            xpath = xpath.replace("city_to_be_replaced", city)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose city", "result": "1"})

        ##Choose area
        if area:
            xpath = Util.GetXpath({"locate":"area_choose"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click area list", "result": "1"})
            xpath = Util.GetXpath({"locate":"area"})
            xpath = xpath.replace("area_to_be_replaced", area)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose area", "result": "1"})

        ##Input address
        if address:
            xpath = Util.GetXpath({"locate":"address"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":address, "result": "1"})

        ##Input birthday
        if birthday:
            xpath = Util.GetXpath({"locate":"birthday"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":birthday, "result": "1"})

        ##Click next
        xpath = Util.GetXpath({"locate":"next_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankAccountListPage.InputUserInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBankInfo(arg):
        '''
        InputBankInfo : Input info for bank account
                Input argu :
                    account_name - account name
                    account_number - account number
                    branch_name - branch_name
                    region - region
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account_name = arg['account_name']
        account_number = arg['account_number']
        branch_name = arg['branch_name']
        region = arg['region']

        ##Select bank
        xpath = Util.GetXpath({"locate":"bank_choose"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bank list", "result": "1"})
        xpath = Util.GetXpath({"locate":"bank"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "1", "result": "1"})

        ##Select/Input region
        if Config._TestCaseRegion_ in ("VN", "TW"):
            xpath = Util.GetXpath({"locate":"region_choose"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click region list", "result": "1"})
            xpath = Util.GetXpath({"locate":"region"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose region", "result": "1"})
        elif Config._TestCaseRegion_ == "ID":
            xpath = Util.GetXpath({"locate":"region"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":region, "result": "1"})

        ##Select/Input branch
        if Config._TestCaseRegion_ == "TW":
            xpath = Util.GetXpath({"locate":"branch_choose"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click branch list", "result": "1"})
            xpath = Util.GetXpath({"locate":"branch"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose branch", "result": "1"})
        elif Config._TestCaseRegion_ in ("VN", "ID"):
            xpath = Util.GetXpath({"locate":"branch"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":branch_name, "result": "1"})

        ##Input account name
        xpath = Util.GetXpath({"locate":"account_name"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account_name, "result": "1"})

        ##Input account number
        xpath = Util.GetXpath({"locate":"account_number"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account_number, "result": "1"})

        ##Click Next
        xpath = Util.GetXpath({"locate":"next_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankAccountListPage.InputBankInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUPIBankInfo(arg):
        '''
        InputUPIBankInfo : Input info for UPI bank account
                Input argu :
                    account_name - account name
                    vpa - vpa
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account_name = arg['account_name']
        vpa = arg['vpa']

        ##Input account name
        xpath = Util.GetXpath({"locate":"account_name"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account_name, "result": "1"})

        ##Input account number
        xpath = Util.GetXpath({"locate":"upi_vpa"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":vpa, "result": "1"})

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"submit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankAccountListPage.InputUPIBankInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToAccountDetailPage(arg):
        '''
        GoToAccountDetailPage : Click to account setting page
                Input argu :
                    account - last 4 number from account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg['account']

        ##Click done
        xpath = Util.GetXpath({"locate":"account"})
        xpath = xpath.replace('replaced_text', account)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click account", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankAccountListPage.GoToAccountDetailPage')


class AndroidBankAccountDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBankAccount(arg):
        '''
        DeleteBankAccount : Click action btn and click delete
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        if Config._TestCaseRegion_ != "ID":
            ##Click action btn
            xpath = Util.GetXpath({"locate":"action_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click account", "result": "1"})

        ##Click delete
        xpath = Util.GetXpath({"locate":"delete_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete", "result": "1"})

        ##When delete last bank account, will pop up a notice message to confirm, if exist, need to click confirm btn first.
        notice_msg = Util.GetXpath({"locate":"notice_message"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":notice_msg, "passok": "0", "result": "1"}):
            notice_confirm_btn = Util.GetXpath({"locate":"notice_confirm_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":notice_confirm_btn, "message":"Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankAccountDetailPage.DeleteBankAccount')


class AndroidPaymentSafePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCreditCardCVV(arg):
        '''
        InputCreditCardCVV : Input credit card cvv field
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input credit card cvv field
        xpath = Util.GetXpath({"locate": "cc_cvv_input_box"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": "123", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafePage.InputCreditCardCVV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEmail(arg):
        '''
        InputEmail : Input credit card email field for MX
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input credit card Email field
        xpath = Util.GetXpath({"locate": "email_input_box"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": "aaa@abc.com", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafePage.InputEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectBank(arg):
        '''
        SelectBank : Select bank in safe page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bank selection bar
        xpath = Util.GetXpath({"locate": "drop_down"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bank selection bar", "result": "1"})
        time.sleep(3)

        ##Click the first bank
        xpath = Util.GetXpath({"locate": "first_bank"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click the first bank", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafePage.SelectBank')


class AndroidPaymentSafeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPay(arg):
        '''
        ClickPay : Click pay button in payment intermediary page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pay btn
        xpath = Util.GetXpath({"locate":"pay_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click pay button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickPay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNegative(arg):
        '''
        ClickNegative : Click negative button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click negative button
        locate = Util.GetXpath({"locate":"negative_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click negative button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickNegative')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in payment intermediary page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        '''
        ClickAgree : Click agree button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click agree button
        xpath = Util.GetXpath({"locate":"agree_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click agree button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click OK button
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPositive(arg):
        '''
        ClickPositive : Click positive button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click positive button
        xpath = Util.GetXpath({"locate":"positive_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click positive button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickPositive')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreviousPage(arg):
        '''
        ClickPreviousPage : Click go to previous page button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to previous page button
        xpath = Util.GetXpath({"locate":"previous_page_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click go to previous page button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickPreviousPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStay(arg):
        '''
        ClickStay : Click stay button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click stay button
        xpath = Util.GetXpath({"locate":"stay_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click stay button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickStay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadLater(arg):
        '''
        ClickUploadLater : Click "I do not have receipt. Upload later" button in bank transfer page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload later button
        xpath = Util.GetXpath({"locate":"upload_later_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload later button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickUploadLater')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadNow(arg):
        '''
        ClickUploadNow : Click "Upload now" button in bank transfer page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload later button
        xpath = Util.GetXpath({"locate":"upload_now_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload now button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickUploadNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDismiss(arg):
        '''
        ClickUploadLater : Click Dismiss button in bank transfer page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload later button
        xpath = Util.GetXpath({"locate":"dismiss_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload later button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickDismiss')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickIKnowIt(arg):
        '''
        ClickIKnowIt : Click I knew it btn after click place order when using bank transfer to pay
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click I knew it button
        xpath = Util.GetXpath({"locate":"i_knew_it_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click I knew it button", "result": "1"})
        time.sleep(2)

        ## Click OK for popup reminder (System confirmed payment of this order within 10 minutes)
        xpath = Util.GetXpath({"locate": "ok_popup_button"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click 'ok' button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickIKnowIt')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLeave(arg):
        '''
        ClickLeave : Click leave button in payment confirm page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click leave button in payment confirm page
        xpath = Util.GetXpath({"locate":"leave_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click leave button in payment confirm page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentSafeButton.ClickLeave')


class AndroidPaymentThirdPartyButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click credit card submit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"submit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click credit card cancel button
                Input argu :
                    page_type - credit_card / ibanking
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click cancel btn in specific page
        xpath = Util.GetXpath({"locate": page_type + "_cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        '''
        ClickAgree : Click agree button to close popup
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click agree button
        xpath = Util.GetXpath({"locate":"agree_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click agree button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreviousPage(arg):
        '''
        ClickPreviousPage : Click go to previous page button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to previous page button
        xpath = Util.GetXpath({"locate":"previous_page_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click go to previous page button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyButton.ClickPreviousPage')


class AndroidPaymentThirdPartyPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputIBankingInfo(arg):
        '''
        InputIBankingInfo : Input ibanking info in third party page
                Input argu :
                    card_number - NCB card number
                    card_date - card date
                    card_holder - card holder name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']
        card_date = arg['card_date']
        card_holder = arg['card_holder']

        ##Input card number
        xpath = Util.GetXpath({"locate":"card_number"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":card_number, "result":"1"})

        ##Input card date
        xpath = Util.GetXpath({"locate":"card_date"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":card_date, "result":"1"})

        ##Input card holder name
        xpath = Util.GetXpath({"locate":"card_holder_name"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":card_holder, "result":"1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate":"submit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyPage.InputIBankingInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputIBankingOTP(arg):
        '''
        InputIBankingOTP : Input ibanking otp in third party page
                Input argu :
                    otp - otp
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        otp = arg['otp']

        ##Input otp number
        xpath = Util.GetXpath({"locate":"otp_value"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":otp, "result":"1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate":"comfirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click comfirm btn", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyPage.InputIBankingOTP')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCardPassword(arg):
        '''
        InputCardPassword : Input password for add cc
                Input argu :
                    password - password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        password = arg['password']

        ##Input password
        xpath = Util.GetXpath({"locate":"password_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":password, "result":"1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate":"comfirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click comfirm btn", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidPaymentThirdPartyPage.InputCardPassword')


class AndroidCreditCardPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCreditCardInfo(arg):
        '''
        InputCreditCardInfo : Input credit card info
                Input argu :
                    name - owner name
                    card_number - card number
                    valid_time - valid time
                    cvc - card verification code
                    address - address
                    postal - region of postal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        card_number = arg['card_number']
        valid_time = arg['valid_time']
        cvc = arg['cvc']
        address = arg['address']
        city = arg['city']
        state = arg['state']
        postal = arg['postal']

        if name:
            ##Input name
            xpath = Util.GetXpath({"locate":"input_name"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        if card_number:
            ##Input card number
            xpath = Util.GetXpath({"locate":"input_card_number"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":card_number, "result": "1"})

            ##Input valid time
            xpath = Util.GetXpath({"locate":"input_valid_time"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":valid_time, "result": "1"})

            ##Input cvc
            xpath = Util.GetXpath({"locate":"input_cvc"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":cvc, "result": "1"})

        if address:
            ##Move to address field
            AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "2", "result": "1"})

            ##Input address
            xpath = Util.GetXpath({"locate":"input_address"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":address, "result": "1"})

        if city:
            ##Input city
            xpath = Util.GetXpath({"locate":"input_city"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":city, "result": "1"})

        if state:
            ##Input state
            xpath = Util.GetXpath({"locate":"input_state"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":state, "result": "1"})

        if postal:
            ##Input postal code
            xpath = Util.GetXpath({"locate":"input_postal"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":postal, "result": "1"})

        ##Click submit btn
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"submit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCreditCardPage.InputCreditCardInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStreetNumber(arg):
        '''
        InputStreetNumber : Input streetnumber
                Input argu :
                    street_number - street number of address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        street_number = arg['street']

        ##Input name
        xpath = Util.GetXpath({"locate":"input_street"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":street_number, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCreditCardPage.InputStreetNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetDefaultCreditCard(arg):
        '''
        SetDefaultCreditCard : Set this bank account as default
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set default button
        xpath = Util.GetXpath({"locate":"set_default_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click set default button", "result": "1"})
        time.sleep(5)

        ##Click back button
        xpath = Util.GetXpath({"locate":"home_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Back to previous page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCreditCardPage.SetDefaultCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToCreditCardDetailPage(arg):
        '''
        GoToCreditCardDetailPage : Click to credit card info page
                Input argu : card_number - last 4 number from card number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']

        ##Click credit card
        xpath = Util.GetXpath({"locate":"card_number"})
        xpath = xpath.replace('card_number_to_be_replaced', card_number)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click credit card", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCreditCardPage.GoToCreditCardDetailPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteCreditCard(arg):
        '''
        DeleteCreditCard : Delete Credit Card
                Input argu :
                    password - login password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        password = arg["password"]

        ##Click delete
        xpath = Util.GetXpath({"locate":"delete_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})

        ##Input login password
        xpath = Util.GetXpath({"locate":"input_password"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCreditCardPage.DeleteCreditCard')


class AndroidBankTransferPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReceiptInfo(arg):
        '''
        InputReceiptInfo : Input bank transfer receipt information if click upload receipt now
                Input argu :
                    name - user name/receipt name
                    from_bank - bank name bank transfer from
                    to_bank - bank name bank transfer to
                    account - account number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        from_bank = arg["from_bank"]
        to_bank = arg["to_bank"]
        account = arg["account"]
        ret = 1

        ##Input name
        if name:
            xpath = Util.GetXpath({"locate":"name"})
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        if from_bank:
            ##Input bank
            from_bank_xpath = Util.GetXpath({"locate":"from_bank"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":from_bank_xpath, "message":"Click bank drop down menu", "result": "1"})
            xpath = Util.GetXpath({"locate":"bank_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', from_bank)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_replace, "message":"Choose which bank", "result": "1"})
            time.sleep(1)

        if to_bank:
            ##Input bank
            to_bank_xpath = Util.GetXpath({"locate":"to_bank"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":to_bank_xpath, "message":"Click bank drop down menu", "result": "1"})
            xpath = Util.GetXpath({"locate":"bank_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', to_bank)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_replace, "message":"Choose which bank", "result": "1"})
            time.sleep(1)

        if Config._TestCaseRegion_ == "TH":
            ##Input date
            date_xpath = Util.GetXpath({"locate":"date"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":date_xpath, "message":"Click date", "result": "1"})
            time.sleep(3)
            ok_xpath = Util.GetXpath({"locate":"ok"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":ok_xpath, "message":"Click ok", "result": "1"})
            ##Input time
            time_xpath = Util.GetXpath({"locate":"time"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":time_xpath, "message":"Click time", "result": "1"})
            time.sleep(3)
            ok_xpath = Util.GetXpath({"locate":"ok"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":ok_xpath, "message":"Click ok", "result": "1"})

        ##Input account
        xpath = Util.GetXpath({"locate":"account"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankTransferPage.InputReceiptInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit after input bank transfer receipt information
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"submit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankTransferPage.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadReceiptImage(arg):
        '''
        UploadReceiptImage : Upload receipt image in bank transfer info page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(3)
        xpath = Util.GetXpath({"locate":"add_photo"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})
        time.sleep(3)

        ##Click to upload from album
        xpath = Util.GetXpath({"locate":"from_album"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})

        ##Click first image
        xpath = Util.GetXpath({"locate":"image"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBankTransferPage.UploadReceiptImage')


class AndroidWalletPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWalletVerificationCode(arg):
        '''
        InputWalletVerificationCode : Input wallet verification code
                Input argu :vcode - vcode to be input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        vcode = arg['vcode']

        ##Check if OTP page pop up
        xpath = Util.GetXpath({"locate": "send_vcode_alert"})

        if AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click confirm popup button
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm popup btn", "result": "1"})

            ##Enter vcode
            xpath = Util.GetXpath({"locate": "otp_input_area"})
            AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": vcode, "result": "1"})

            ##Click verify button
            xpath = Util.GetXpath({"locate": "verify_btn"})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click verify btn", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']),'AndroidWalletPage.InputWalletVerificationCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWalletPinCode(arg):
        '''
        InputWalletPinCode : Input wallet pin code
                Input argu :
                    pin_code - pin code to be input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        pin_code = arg['pin_code']

        ##Key in the numbers from the last digit of list
        for number in pin_code:
            ##PH using adb send keyevent won't work
            if Config._TestCaseRegion_ in ("PH", "VN"):
                xpath = Util.GetXpath({"locate":"passcode"})
                xpath = xpath.replace("passcode_to_replace", number)
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click passcode: " + number, "result":"1"})
            else:
                AndroidBaseUICore.AndroidKeyInNumbers({"number": number, "result": "1"})

        if Config._TestCaseRegion_ == "TW":
            ##Click comfirm button
            xpath = Util.GetXpath({"locate":"input_number_confirm"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidWalletPage.InputWalletPinCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopeePayPinCode(arg):
        '''
        InputShopeePayPinCode : Payout by ShopeePay
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input Shopee pay password default passsword : 013579 (only ID)
        ##ID Shopeepay pin like 111111 or 123456 is not allowed because is too simple and predictable
        ##Input 0
        xpath = Util.GetXpath({"locate": "shopee_pay_number"})
        xpath = xpath.replace('replace_number', "0")
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click number : 0", "result": "1"})

        for number in range(1, 10, 2):
            ##Input 1/3/5/7/9
            xpath = Util.GetXpath({"locate": "shopee_pay_number"})
            xpath = xpath.replace('replace_number', str(number))
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click number : "+str(number), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidWalletPage.InputShopeePayPinCode')


class AndroidOrderSuccessfulPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBack(arg):
        '''
        ClickBack : Click back button in order successful page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back button in order successful page
        xpath = Util.GetXpath({"locate":"back_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderSuccessfulPageButton.ClickBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHome(arg):
        '''
        ClickHome : Click home button in order successful page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click home button in order successful page
        xpath = Util.GetXpath({"locate":"home_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click home button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderSuccessfulPageButton.ClickHome')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyPurchase(arg):
        '''
        ClickMyPurchase : Click my purchase button in order successful page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Wait order successful page loading is complete
        AndroidCheckOutMethod.AndroidCheckOutPage.WaitCheckoutLoadingIsComplete({"result": "1"})

        ##Click my purchase button in order successful page
        xpath = Util.GetXpath({"locate":"mypurchase_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click my purchase button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderSuccessfulPageButton.ClickMyPurchase')
