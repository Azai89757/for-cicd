#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidMallPageMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import os
import re
import time
import datetime

##Import framework library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidCommonMethod
import AndroidMePageMethod
import AndroidLoginSignUpMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidMallPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOfficialShopCategoryExist(arg):
        '''
        CheckOfficialShopCategoryExist : Check official shop category exist
                Input argu :
                    page_type - page type (category / mall_page)
                    category_name - category name
                    image - image name
                    swipe_time - swipe time
                    is_click - click image or not
                    category_scroll_time - category section scroll time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        match_result = 1
        page_type = arg["page_type"]
        category_name = arg["category_name"]
        image = arg["image"]
        swipe_time = arg["swipe_time"]
        is_click = arg["is_click"]
        category_scroll_time = arg["category_scroll_time"]

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            if page_type == "mall_page":
                ##Move to official shop section and enter
                xpath = Util.GetXpath({"locate":"offical_shop_view_more"})
                if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"7", "result": "1"}):
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

                    ##Click official shop view more button
                    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click official shop view more button", "result": "1"})
                    time.sleep(8)

                    ##Close mall popup banner just in case
                    AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                    ##Click specific official shop category
                    time.sleep(8)
                    xpath = Util.GetXpath({"locate":"official_shop_category"})
                    xpath = xpath.replace("string_to_be_replaced", category_name)
                    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click official shop category -> " + category_name, "result": "1"})
                    time.sleep(10)

                    ##Close mall popup banner just in case
                    AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                    ##Refresh page
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
                    time.sleep(5)
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
                    time.sleep(10)

                    ##Close mall popup banner just in case
                    AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                    if swipe_time:
                        xpath = Util.GetXpath({"locate":"trending_shop_scroll"})
                        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
                            AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":swipe_time, "result": "1"})

            elif page_type == "category":
                ##Move to category section
                xpath = Util.GetXpath({"locate":"cateory_scroll"})
                if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"10", "result": "1"}):
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

                    ##Scroll category section
                    if category_scroll_time:
                        xpath = Util.GetXpath({"locate":"cateory_scroll"})
                        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times": category_scroll_time, "result": "1"})

                    ##Click specific category
                    xpath = Util.GetXpath({"locate":"category_name"})
                    xpath = xpath.replace("string_to_be_replaced", category_name)
                    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click homepage category -> " + category_name, "result": "1"})
                    time.sleep(8)

                    ##Refresh page
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"2", "result": "1"})
                    time.sleep(5)

                    if swipe_time:
                        xpath = Util.GetXpath({"locate":"category_shop_scroll"})
                        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":swipe_time, "result": "1"})

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("Android official shop category image/GIF matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.8", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(40)
                dumplogger.info("Android official shop category image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidMallPage.CheckOfficialShopCategoryExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTrendingShopImageExist(arg):
        '''
        CheckTrendingShopImageExist : Check trend shop image exists (Trending shop section must remain exists in shop category page)
                Input argu :
                    category_name - category name
                    image - image name
                    index - index
                    is_click - click image or not
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        match_result = 1
        category_name = arg["category_name"]
        image = arg["image"]
        index = arg["index"]
        is_click = arg["is_click"]

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Move to official shop section and enter
            xpath = Util.GetXpath({"locate":"offical_shop_view_more"})
            if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"7", "result": "1"}):
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

                ##Click official shop view more button
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click official shop view more button", "result": "1"})
                time.sleep(8)

                ##Close mall popup banner just in case
                AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                ##Click specific official shop category
                time.sleep(8)
                xpath = Util.GetXpath({"locate":"official_shop_category"})
                xpath = xpath.replace("string_to_be_replaced", category_name)
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click official shop category -> " + category_name, "result": "1"})
                time.sleep(5)

                ##Close mall popup banner just in case
                AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                ##Refresh page
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
                time.sleep(3)
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
                time.sleep(10)

                ##Close mall popup banner just in case
                AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                ##Click trending shop see more
                xpath = Util.GetXpath({"locate":"trending_shop_see_more"})
                if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
                    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click trending shop see more", "result": "1"})
                    time.sleep(5)

                    ##Click specific official shop category
                    time.sleep(8)
                    xpath = Util.GetXpath({"locate":"official_shop_category"})
                    xpath = xpath.replace("string_to_be_replaced", category_name)
                    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click official shop category -> " + category_name, "result": "1"})
                    time.sleep(5)

                    ##Refresh page
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"2", "result": "1"})
                    time.sleep(5)

                    ##Choose index
                    xpath = Util.GetXpath({"locate":"index_btn"})
                    xpath = xpath.replace("string_to_be_replaced", index)
                    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "message":"Check if seller tag exists", "passok": "0", "result": "1"}):
                        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click index -> " + index, "result": "1"})
                        time.sleep(5)

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("Android official shop category image/GIF matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.9", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(40)
                dumplogger.info("Android official shop category image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidMallPage.CheckTrendingShopImageExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBrandOfWeekExist(arg):
        '''
        CheckBrandOfWeekExist : Check brand of week exists
                Input argu :
                    is_top - 1 / 0 (if is top, then no need to swipe)
                    brand_title - brand of week title
                    image - image name
                    is_click - click image or not
                    swipe_time - scroll swipe time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        match_result = 1
        is_top = arg["is_top"]
        brand_title = arg["brand_title"]
        image = arg["image"]
        is_click = arg["is_click"]
        swipe_time = arg["swipe_time"]

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Move to official shop section and enter
            xpath = Util.GetXpath({"locate":"offical_shop_view_more"})
            if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"7", "result": "1"}):
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

                ##Click official shop view more button
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click official shop view more button", "result": "1"})
                time.sleep(8)

                ##Close mall popup banner just in case
                AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                ##Refresh page
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
                time.sleep(5)
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"1", "result": "1"})
                time.sleep(10)

                ##Close mall popup banner just in case
                AndroidBaseUICore.AndroidCloseBanner({"banner_type":"mall"})

                ##If there's brand title setting
                if brand_title:
                    xpath = Util.GetXpath({"locate":"brand_scroll_list"})
                    xpath = xpath.replace("title_to_be_replaced", brand_title)
                    if not int(is_top) and AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0","result": "1"}):
                        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

                    ##If need swipe
                    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}) and swipe_time:
                        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":swipe_time, "result": "1"})

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("Android brand of week matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.9", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(40)
                dumplogger.info("Android brand of week matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidMallPage.CheckBrandOfWeekExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAlphabet(arg):
        '''
        ClickAlphabet : Click the alphabet
                Input argu :
                    alphabet - alphabet
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        alphabet = arg["alphabet"]

        ##Click the alphabet
        xpath = Util.GetXpath({"locate":"alphabet_text"})
        xpath = xpath.replace("string_to_be_replaced", alphabet)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click alphabet: " + alphabet, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallPage.ClickAlphabet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryBarDropDown(arg):
        '''
        ClickCategoryBarDropDown : Click category bar drop down button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click drop down button
        xpath = Util.GetXpath({"locate":"drop_down_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click drop down button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallPage.ClickCategoryBarDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        '''
        ClickCategory : Click category
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click category
        xpath = Util.GetXpath({"locate":"category"})
        xpath = xpath.replace("string_to_be_replaced", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click category: " + category_name, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallPage.ClickCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToMallPage(arg):
        '''
        GoToMallPage : Go to mall page
                Input argu :
                    entry_point - see_more_button / see_more_card
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        entry_point = arg["entry_point"]

        ##Close APP and open
        AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

        ##Move to mall page section and enter
        xpath = Util.GetXpath({"locate":"see_more_button"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"7", "result": "1"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

        if entry_point == "see_more_card":
            ##Scroll to see more card
            xpath = Util.GetXpath({"locate":"mall_page_scroll"})
            AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":"5", "result": "1"})

        ##Click see more
        xpath = Util.GetXpath({"locate":entry_point})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see more", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallPage.GoToMallPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchBar(arg):
        '''
        ClickSearchBar : Click mall page search bar
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mall page search bar
        xpath = Util.GetXpath({"locate":"search_bar"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click mall page search bar", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMallPage.ClickSearchBar')
