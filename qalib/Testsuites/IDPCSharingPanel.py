import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCSharingPanel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSharingPanel Precondition =="
        dumplogger.info("== Setup IDPCSharingPanel Precondition ==")

    def test_IDPCSharingPanel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSharingPanel-01.xml")

    def test_IDPCSharingPanel02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSharingPanel-02.xml")

    def test_IDPCSharingPanel03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSharingPanel-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSharingPanel Post condition =="
        dumplogger.info("== Setup IDPCSharingPanel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
