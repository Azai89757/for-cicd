﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminScreeningMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util

##Inport Web library
from PageFactory.Web import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminProductScreeningButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStartQCProducts(arg):
        '''
        ClickStartQCProducts : Click start QC next 30 products
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ## Click start QC product button
        xpath = Util.GetXpath({"locate": "start_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductScreeningButton.ClickStartQCProducts')
