import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCCancellation Precondition =="
        dumplogger.info("== Setup ESPCCancellation Precondition ==")

    def test_ESPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCancellation-01.xml")

    def test_ESPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCancellation-02.xml")

    def test_ESPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCancellation-03.xml")

    def test_ESPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCancellation-04.xml")

    def test_ESPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCCancellation Post condition =="
        dumplogger.info("== Setup ESPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
