#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 STSAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time
import json
import zipfile
import os

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import XtFunc
import csv
import Config
from ShopeeOSSConnector import ShopeeOSSConnector

##Import api library
import HttpAPICore
from AdminAPIMethod import AdminCommonAPI

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class STSAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreSTScookie(arg):
        ''' StoreSTScookie : Store cookie
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        db_file = arg["db_file"]
        ret = 1
        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value"]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":"", "store_data_list":store_list, "result": "1"})

        ##Bulid cookie for request header
        sts_cookie = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign cookie to API request header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] += sts_cookie

        OK(ret, int(arg['result']), 'STSAPI.StoreSTScookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreStuckEvent(arg):
        ''' RestoreStuckEvent : restore stuck event
                Input argu : N/A
                Return code : 1 - success
        '''
        ret = 1
        ##Get processing event
        DBCommonMethod.InitialSQL({"db_name":"staging_sts", "result": "1"})
        DBCommonMethod.GetAndSetSQLData({"file_name":"get_processing_event", "result": "1"})
        DBCommonMethod.SendSQLCommandToDB({"method":"select", "result": "1"})

        ##If processing event exist, move it to finished
        if GlobalAdapter.DBVar._DBResult_:
            DBCommonMethod.StoreDataFromSQLResult({"column":"event_id", "result": "1"})
            assign_list = [{"column": "event_id", "value_type": "number"}]
            DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_sts", "file_name":"finish_event", "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        ##Deinitial
        DBCommonMethod.DeInitialSQL({"result": "1"})

        OK(ret, int(arg['result']), 'STSAPI.RestoreStuckEvent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignSearchPayoutTime(arg):
        ''' AssignSearchPayoutTime : get format timestamp to filter payout
                Input argu :
                        time_deviation - search time interval (minutes)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        time_deviation = arg['time_deviation']

        ##Bulid search time filter
        search_start_date = XtFunc.GetCurrentDateTime("%Y/%m/%d %H:%M:%S +0800", 0, -int(time_deviation), 0, is_tw_time = 1)
        search_end_date = XtFunc.GetCurrentDateTime("%Y/%m/%d %H:%M:%S +0800", 0, 0, 0, is_tw_time = 1)

        ##Assign search time to payload
        GlobalAdapter.APIVar._HttpPayload_['start_date'] = search_start_date
        GlobalAdapter.APIVar._HttpPayload_['end_date'] = search_end_date

        OK(ret, int(arg['result']), 'STSAPI.AssignSearchPayoutTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def WriteChangeStatusCsv(arg):
        ''' WriteChangeStatusCsv : Write change payout/item status csv
                Input argu :
                        payout_type - adjustment or escrow
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        ##Bulid file path
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._APICaseDataFilePath_ + slash + Config._TestCasePlatform_.lower() + slash + "common" + slash + Config._TestCaseFeature_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_.lower() + slash + "change_status.csv"

        ##Get payout type and payout id
        payout_type = arg['payout_type']
        payout_id = GlobalAdapter.CommonVar._DynamicCaseData_[payout_type + "_id"]

        ##Store change status data as data_list
        data_list = [[payout_id, payout_type]]

        ##Wirte to change_status file
        with open(file_path, 'wb') as openfile:
            writefile = csv.writer(openfile)
            writefile.writerows(data_list)

        OK(ret, int(arg['result']), 'STSAPI.WriteChangeStatusCsv')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetManuallyPayoutReportFromOSS(arg):
        ''' GetManuallyPayoutReportFromOSS : get generated payout file from oss server
                Input argu :
                            payout_channel - payoneer / pingpong / lianlian
                            report_type - safe_file / SAFE_file / payout_file / pre_safe
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        slash = Config.dict_systemslash[Config._platform_]
        payout_channel = arg['payout_channel']
        report_type = arg['report_type']

        ##Define store_file_path
        store_file_path = Config._CurDataDIR_ + slash + "sts_manually_payout_report"

        ##Define file_path in the OSS server
        current_month = XtFunc.GetCurrentDateTime("%Y%m")
        if payout_channel == "lianlian":
            report_file_path = "manually_payout/" + payout_channel + "/pre_safe/" + Config._EnvType_.lower() + "/" + Config._TestCaseRegion_.lower() + "/" + current_month
        else:
            report_file_path = "manually_payout/" + payout_channel + "/" + Config._EnvType_.lower() + "/" + Config._TestCaseRegion_.lower() + "/" + current_month

        ##Bulid OSS connector
        oss_client = ShopeeOSSConnector("staging_sts")

        ##Download report
        report_file_key = oss_client.GetAllFilesMatchKeyWord(report_file_path, report_type)
        if payout_channel == "payoneer" and report_type == "safe_file":
            ##Payoneer safe file will be .zip, need to unzip and change filename
            oss_client.DownloadFile(report_file_key[0], store_file_path + ".zip")
            with zipfile.ZipFile(store_file_path + ".zip", 'r') as zip_ref:
                payoneer_safe_filename = zip_ref.namelist()[0]
                zip_ref.extractall()
                os.rename(payoneer_safe_filename, "sts_manually_payout_report.json")

        elif payout_channel == "lianlian":
            ##For lianlian, sts only provide pre_safe file. Need to generate safe by upload pre_safe to admin
            oss_client.DownloadFile(report_file_key[0], store_file_path + ".csv")

            ##Login admin
            AdminCommonAPI.AdminLogin({"result":"1"})

            ##Initial and set create lianlian report api
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "create_order_level_detail", "result": "1"})
            AdminCommonAPI.AssignCsrfToken({"result":"1"})
            GlobalAdapter.APIVar._HttpUploadFiles_ = {
                'file': ("sts_manually_payout_report.csv", open("sts_manually_payout_report.csv",'rb'), 'text/csv')
            }

            ##Send request
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

            ##Store response as csv file for further check
            with open("sts_manually_payout_report.csv", 'w') as file:
                for line in GlobalAdapter.APIVar._APIResponse_["text"].encode('utf8'):
                    file.write(line.replace("|",","))

        else:
            oss_client.DownloadFile(report_file_key[0], store_file_path + ".csv")

        OK(ret, int(arg['result']), 'STSAPI.GetManuallyPayoutReportFromOSS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetFinancialReportFromOSS(arg):
        ''' GetFinancialReportFromOSS : get generated payout file from oss server
                Input argu :
                            report_name - financial report name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        slash = Config.dict_systemslash[Config._platform_]

        report_name = arg['report_name']
        report_file_path = "/data/shopee/sts/" + Config._EnvType_.lower() + "/" + Config._TestCaseRegion_.lower() + "/financial_report/"
        store_file_path = Config._CurDIR_ + slash

        ##Bulid OSS connector
        oss_client = ShopeeOSSConnector("staging_sts")

        ##Get financial report file key
        all_files = oss_client.GetAllFilesMatchKeyWord(report_file_path, report_name)

        ##Download the file
        oss_client.DownloadFile(all_files[0], store_file_path + "sts_financial_report.csv")

        OK(ret, int(arg['result']), 'STSAPI.GetFinancialReportFromOSS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignOrderInfo(arg):
        '''
        AssignOrderInfo : assign order info for update order status
                Input argu :
                        column - assign to Http payload, support order_id / ofg_id
                Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        column = arg['column']

        ##Get global order info and assign to payload
        if column == "order_id":
            GlobalAdapter.APIVar._HttpPayload_['order_id'] = int(GlobalAdapter.OrderE2EVar._OrderID_)
        elif column == "ofg_id":
            GlobalAdapter.APIVar._HttpPayload_['ofg_id'] = int(GlobalAdapter.OrderE2EVar._OFGID_)
        else:
            ret = 0

        OK(ret, int(arg['result']), 'STSAPI.AssignOrderInfo')
