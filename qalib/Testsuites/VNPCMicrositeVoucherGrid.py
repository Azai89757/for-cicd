import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCMicrositeVoucherGrid(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMicrositeVoucherGrid Precondition =="
        dumplogger.info("== Setup VNPCMicrositeVoucherGrid Precondition ==")

    def test_VNPCMicrositeVoucherGrid01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMicrositeVoucherGrid-01.xml")

    def test_VNPCMicrositeVoucherGrid02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMicrositeVoucherGrid-02.xml")

    def test_VNPCMicrositeVoucherGrid03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMicrositeVoucherGrid-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMicrositeVoucherGrid Post condition =="
        dumplogger.info("== Setup VNPCMicrositeVoucherGrid Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
