#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidHomePageMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidCategoryPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHomePageCategoryExist(arg):
        '''
        CheckHomePageCategoryExist : Check if category square in category exists
                Input argu :
                    image - OpenCV project file
                    swipe_direction - direction of scrolling the category list
                    swipe_time - time scrolling the category list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]
        swipe_direction = arg["swipe_direction"]
        swipe_time = arg["swipe_time"]

        ##Success: match_result = 0; Fail: match_result = 1
        match_result = 1

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking homepage category" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Scroll screen to category list
            xpath = Util.GetXpath({"locate":"category_list"})
            if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"9", "result": "1"}):
                AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "1", "result": "1"})
                time.sleep(3)
                AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": xpath, "ratio": "0.3", "result": "1"})
                time.sleep(5)

                ##Swipe the category list horizontally
                xpath = Util.GetXpath({"locate":"category_horiz_scroll"})
                AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":swipe_direction, "times":swipe_time, "result": "1"})

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode": "android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether homepage category is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("Homepage category matching successful!!!!")
                break
            else:
                dumplogger.info("Homepage category matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidCategoryPage.CheckHomePageCategoryExist')


class AndroidCampaignModule:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHomepageFeature(arg):
        '''
        CheckHomepageFeature : Check if homepage features exist
                Input argu :
                    title - feature title
                    image - OpenCV project file
                    swipe - swipe feature or not (1 / 0)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        image = arg["image"]
        swipe = arg["swipe"]
        match_result = 1

        ##Repeatly check if homepage feature shows up
        for retry_count in range(1,5):
            dumplogger.info("Start round: %s for checking homepage component" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Move to feature section
            xpath = Util.GetXpath({"locate":"feature_scroll"})
            xpath = xpath.replace("string_to_be_replaced", title)
            if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"5", "result": "1"}):
                AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": xpath, "ratio": "0.5", "result": "1"})
                time.sleep(5)

                ##Swipe the feature list if needed
                if int(swipe):
                    swipe_direction = arg["swipe_direction"]
                    swipe_time = arg["swipe_time"]
                    xpath = Util.GetXpath({"locate":"feature_scroll"})
                    xpath = xpath.replace("string_to_be_replaced", title)

                    ##If scroll row exist, swipe if needed
                    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":swipe_direction, "times":swipe_time, "result": "1"})
                    else:
                        dumplogger.info("Unable to find xpath!!!")
                        time.sleep(60)
                        continue

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether feature is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("App homepage component matching successfully at %d run!!!!" % (retry_count))
                break
            else:
                dumplogger.info("App homepage component matching failed at %d run..." % (retry_count))
                time.sleep(60)

        OK(ret, match_result, 'AndroidCampaignModule.CheckHomepageFeature')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHomepageVisual(arg):
        '''
        CheckHomepageVisual : Check if homepage features exist
                Input argu :
                    title - homepage feature title
                    type - feature type (top_visual / buttom_visual)
                    image - OpenCV project file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        type = arg["type"]
        image = arg["image"]
        match_result = 0

        ##Repeatly check if visual shows up
        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking homepage category" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            xpath = Util.GetXpath({"locate":"feature_scroll"})
            xpath = xpath.replace("string_to_be_replaced", title)
            if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"3", "result": "1"}):
                ##If bottom visual, swipe on more time
                if type == "bottom_visual":
                    AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether feature is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("App homepage visual matching successfully at %d run!!!!" % (retry_count))
                break
            else:
                dumplogger.info("App homepage visual matching failed at %d run..." % (retry_count))
                time.sleep(60)

        OK(ret, match_result, 'AndroidCampaignModule.CheckHomepageVisual')


class AndroidCampaignModuleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreComponent(arg):
        '''
        ClickSeeMoreComponent : Click component see more btn
                Input argu :
                    component_title - component title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_title = arg["component_title"]

        ##Click see more btn of specific component
        xpath = Util.GetXpath({"locate":"see_more_btn"})
        xpath = xpath.replace("string_to_be_replaced", component_title)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click component see more.", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCampaignModuleButton.ClickSeeMoreComponent')


class AndroidHomeSquare:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHomeSquareChange(arg):
        '''
        CheckHomeSquareChange : Check if home square change to latest status
                Input argu :
                    image - OpenCV project file
                    swipe_time - swipe time
                    is_click - click image or not
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]
        swipe_time = arg["swipe_time"]
        is_click = arg["is_click"]

        ##match_result = 0 : Sucessfully matched ; match_result = 1 : Fail to match
        match_result = 1

        for retry_count in range(1, 13):
            dumplogger.info("Start round: %s for checking home square" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            xpath = Util.GetXpath({"locate":"home_square_scroll"})
            if swipe_time and AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
                AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":swipe_time, "result": "1"})

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.8", "expected_result":arg['result']})

            ##Whether home square is exist or not, break the loop as long as the result is expected
            ##If match_result = 0, then match_result == ret -> sucessfully matched
            if match_result:
                dumplogger.info("Home square matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.8", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(25)
                dumplogger.info("Home square matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidHomeSquare.CheckHomeSquareChange')


class AndroidWalletBar:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckWalletBarChange(arg):
        '''
        CheckWalletBarChange : Check if wallet bar change to latest status
                Input argu :
                    env - staging / uat / test
                    is_click - click image or not
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]
        is_click = arg["is_click"]

        ##match_result = 0 : Sucessfully matched ; match_result = 1 : Fail to match
        match_result = 1

        for retry_count in range(1, 15):
            dumplogger.info("Start round: %s for checking wallet bar" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.8", "expected_result":arg['result']})

            ##Whether wallet bar is exist or not, break the loop as long as the result is expected
            ##If match_result = 0, then match_result == ret -> sucessfully matched
            if match_result:
                dumplogger.info("Wallet bar matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.8", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(60)
                dumplogger.info("Wallet bar matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidWalletBar.CheckWalletBarChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckWalletBarTextChange(arg):
        '''
        CheckWalletBarTextChange : Check if wallet bar text change to latest status
                Input argu :
                    text - text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        text = arg["text"]

        for retry_count in range(1, 15):
            dumplogger.info("Start round: %s for checking wallet bar text" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Check wallet bar text
            xpath = Util.GetXpath({"locate":"wallet_bar_text"})
            xpath = xpath.replace("text_to_be_replace", text)

            if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ret = 1
                dumplogger.info("Wallet bar text matching successful at %d run!!!!" % (retry_count))
                break
            else:
                time.sleep(60)
                dumplogger.info("Wallet bar text matching failed at %d run..." % (retry_count))

        OK(ret, int(arg['result']), 'AndroidWalletBar.CheckWalletBarTextChange')


class AndroidHomePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HomePageNavigation(arg):
        '''
        HomePageNavigation : Click on a icon at the buttom of the home page
                Input argu :
                    type - which progress tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Check device cpu usage is stable
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "70"})

        ##Check type is mall or not
        if type == 'mall':

            ##Back to homepage
            AndroidHomePage.HomePageNavigation({"type":"home", "result": "1"})

            ##Slide down and check for mall shop
            AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"mall"}), "direction":"down", "isclick":"1", "result": "1"})

        else:

            ##Go to different tab
            time.sleep(3)

            ##Click sub feature
            locate = Util.GetXpath({"locate":type})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in the me page", "result": "1"})

            ##Close cookies message in specific region
            if type == 'metab' and Config._TestCaseRegion_ in ("PL", "ES"):
                AndroidBaseUICore.AndroidCloseCookiesPopUp({"result": "1"})

            ##Close PDPA consent popup in specific region
            if type == 'home' and Config._TestCaseRegion_ in ("TH"):
                AndroidBaseUICore.AndroidClosePDPAPopup({"result": "1"})

        OK(ret, int(arg['result']), 'AndroidHomePage.HomePageNavigation -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRandomUsername(arg):
        '''
        GetRandomUsername : Get random username in homepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get random username in homepage
        xpath = Util.GetXpath({"locate":"account_name"})
        AndroidBaseUICore.AndroidGetElements({"locate":xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        GlobalAdapter.GeneralE2EVar._RandomString_ = GlobalAdapter.CommonVar._PageAttributes_

        OK(ret, int(arg['result']), 'AndroidHomePage.GetRandomUsername')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckNewUserZone(arg):
        '''
        CheckNewUserZone : Check new user zone
                Input argu :
                    image - image name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        match_result = 1
        image = arg["image"]

        for retry_count in range(1, 6):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether feature is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("App new user zone components matching successful at %d run !!!!" % (retry_count))
                break
            else:
                dumplogger.info("App new user zone components matching failed at %d run..." % (retry_count))
                time.sleep(60)

        OK(ret, match_result, 'AndroidHomePage.CheckNewUserZone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckMallShop(arg):
        '''
        CheckMallShop : Check mall shop
                Input argu :
                    image - image name
                    swipe_time - swipe time
                    is_click - click image or not
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        match_result = 1
        image = arg["image"]
        swipe_time = arg["swipe_time"]
        is_click = arg["is_click"]

        for retry_count in range(1, 11):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Move to official shop section and enter
            xpath = Util.GetXpath({"locate":"offical_shop_view_more"})
            if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"7", "result": "1"}):
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"2", "result": "1"})
                time.sleep(5)

                if swipe_time:
                    xpath = Util.GetXpath({"locate":"mall_shop_scroll"})
                    AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":swipe_time, "result": "1"})

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("Android mall shop image matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.8", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(30)
                dumplogger.info("Android mall shop image matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidHomePage.CheckMallShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipeDownToMallSection(arg):
        '''
        SwipeDownToMallSection : Swipe down to mall section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Swipe down to mall section
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"shopee_mall_title"}), "direction":"down", "isclick":"0", "result": "1"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidHomePage.SwipeDownToMallSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipeDownToCategorySection(arg):
        '''
        SwipeDownToCategorySection : Swipe down to mall section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Swipe down to category section
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"category_title"}), "direction":"down", "isclick":"0", "result": "1"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidHomePage.SwipeDownToCategorySection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipeDownToHomepageSection(arg):
        '''
        SwipeDownToHomepageSection : Swipe down to specific homepage section
                Input argu :
                    section - category / mall / daily_discover
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Swipe down to specific homepage section
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate": section}), "direction":"down", "isclick":"0", "result": "1"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidHomePage.SwipeDownToHomepageSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHomeDailyDiscoverIcon(arg):
        '''
        ClickHomeDailyDiscoverIcon : Click home tag (or daily discover tag)
                Input argu :
                    mode - home / daily_discover
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mode = arg["mode"]

        ##Click home icon
        xpath = Util.GetXpath({"locate":mode})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Home/DD icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidHomePage.ClickHomeDailyDiscoverIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLandingPageBanner(arg):
        '''
        ClickLandingPageBanner : Click landing page banner
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click landing page banner
        xpath = Util.GetXpath({"locate":"landing_page_banner"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click landing page banner", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidHomePage.ClickLandingPageBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompareShopeeCoins(arg):
        '''
        CompareShopeeCoins : Compare the shopee coins amount of increase
                Input argu :
                    expected_increased_coins - expected increased coins amount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        expected_increased_coins = arg["expected_increased_coins"]

        ##Original shopee coins
        original_coins = int((GlobalAdapter.CoinsE2EVar._OriginalShopeeCoins_).split(' Xu')[0])

        ##Current shopee coins
        get_coins = int((GlobalAdapter.CoinsE2EVar._GetShopeeCoins_).split(' Xu')[0])

        ##Calculate the increased coins
        increased_coins = get_coins - original_coins

        if int(increased_coins) == int(expected_increased_coins):
            dumplogger.info("Compare shopee coins success.")

        else:
            dumplogger.info("Compare shopee coins failed.")
            dumplogger.info("original shopee coins amount: " + str(original_coins))
            dumplogger.info("current shopee coins amount: " + str(get_coins))
            dumplogger.info("expected increased shopee coins amount: " + str(expected_increased_coins))
            ret = 0

        OK(ret, int(arg['result']), 'AndroidHomePage.CompareShopeeCoins')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetModifiedUsername(arg):
        '''
        GetModifiedUsername : Get modified username in homepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get random username in homepage
        xpath = Util.GetXpath({"locate":"account_name"})
        AndroidBaseUICore.AndroidGetElements({"locate":xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        GlobalAdapter.GeneralE2EVar._ModifyString_ = GlobalAdapter.CommonVar._PageAttributes_

        OK(ret, int(arg['result']), 'AndroidHomePage.GetModifiedUsername')


class AndroidNewUserZoneButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewUserZoneImage(arg):
        '''
        ClickNewUserZoneImage : Click new user zone product card img
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click new user zone product card img
        xpath = Util.GetXpath({"locate":"nuz_img"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click new user zone product card img", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNewUserZoneButton.ClickNewUserZoneImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExclusiveDealsTab(arg):
        '''
        ClickExclusiveDealsTab : Click exclusive deals tab
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click exclusive deals tab
        xpath = Util.GetXpath({"locate":"exclusive_deals_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click exclusive deals tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNewUserZoneButton.ClickExclusiveDealsTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFreeGiftsTab(arg):
        '''
        ClickFreeGiftsTab : Click free gifts tab
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click free gifts tab
        xpath = Util.GetXpath({"locate":"free_gifts_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click free gifts tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidNewUserZoneButton.ClickFreeGiftsTab')


class AndroidMissionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMissionCampaign(arg):
        '''
        ClickMissionCampaign : Click mission campaign
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click mission campaign
        xpath = Util.GetXpath({"locate":"mission_campaign"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click mission campaign", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMissionButton.ClickMissionCampaign')
