import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCOrder Precondition =="
        dumplogger.info("== Setup BRPCOrder Precondition ==")

    def test_BRPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-01.xml")

    def test_BRPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-02.xml")

    def test_BRPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-03.xml")

    def test_BRPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-04.xml")

    def test_BRPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-05.xml")

    def test_BRPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-06.xml")

    def test_BRPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-07.xml")

    def test_BRPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-08.xml")

    def test_BRPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-09.xml")

    def test_BRPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-10.xml")

    def test_BRPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCOrder-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCOrder Post condition =="
        dumplogger.info("== Setup BRPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
