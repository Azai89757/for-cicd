#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminAuditLogsMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util

##Inport Web library
from PageFactory.Web import BaseUICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminAuditLogsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByItemId(arg):
        '''
        SearchByItemId : Search result by item id
                input Argu :
                    item_id - item id ( if input multiple id,separated item id list by comma )
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']

        ##Click search item id icon
        AdminAuditLogsButton.ClickSearchItemId({"result": "1"})

        ##reset input value
        xpath = Util.GetXpath({"locate":"reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button","result": "1"})
        AdminAuditLogsButton.ClickSearchItemId({"result": "1"})

        ##click textarea, then go to input stage
        xpath = Util.GetXpath({"locate":"textarea_input"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click text area","result": "1"})

        ##input item id
        xpath = Util.GetXpath({"locate":"id_input"})
        BaseUICore.Input({"locate":xpath, "string":item_id, "method":"xpath", "result": "1"})

        ##click somewhere else to close the following search list
        AdminAuditLogsButton.ClickSourceFilter({"result": "1"})
        AdminAuditLogsButton.ClickSearchItemId({"result": "1"})

        ##click search
        xpath = Util.GetXpath({"locate":"search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button","result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogsPage.SearchByItemId')


class AdminAuditLogsButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchOperator(arg):
        '''
        ClickSearchOperator : Click search operator icon
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search operator icon
        xpath = Util.GetXpath({"locate": "search_operator_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search operator icon","result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogsButton.ClickSearchOperator')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDateAndTimeFilter(arg):
        '''
        ClickDateAndTimeFilter : Click date and time filter
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click date and time filter
        xpath = Util.GetXpath({"locate": "date_time_filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click date and time filter","result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogsButton.ClickDateAndTimeFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSourceFilter(arg):
        '''
        ClickSourceFilter : Click source filter
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click source filter
        xpath = Util.GetXpath({"locate": "source_filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click source filter","result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogsButton.ClickSourceFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAllCheckbox(arg):
        '''
        ClickSelectAllCheckbox : Click select all checkbox
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select all checkbox
        xpath = Util.GetXpath({"locate": "select_all_checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select all checkbox","result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogsButton.ClickSelectAllCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchItemId(arg):
        '''
        ClickSearchItemId : Click search item id icon
                input Argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search item id icon
        xpath = Util.GetXpath({"locate": "search_item_id_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search item id icon","result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogsButton.ClickSearchItemId')
