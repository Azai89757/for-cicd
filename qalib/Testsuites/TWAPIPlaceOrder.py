import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup TWAPIPlaceOrder Precondition ==")

    def test_TWAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup TWAPIPlaceOrder Post condition ==")
