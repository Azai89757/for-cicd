import sys
import os
import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class CLAndroidActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAndroidActivityBanner Precondition =="

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Disable wifi module by command only if proxy server is not used
            if not Config._EnableProxy_:
                ##Disable wifi module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc wifi disable"
                dumplogger.info("Disable emulator wifi module: %s" % (cmd))
                os.system(cmd)

                ##Enable mobile data module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc data enable"
                dumplogger.info("Enable emulator mobile data module: %s" % (cmd))
                os.system(cmd)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"cl", "result": "1"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_CLAndroidActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidActivityBanner-01.xml")

    def test_CLAndroidActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidActivityBanner-02.xml")

    def test_CLAndroidActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidActivityBanner-03.xml")

    ##Catgory not at the bottom of management page
    # def test_CLAndroidActivityBanner04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CL/CLAndroidActivityBanner-04.xml")

    def test_CLAndroidActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidActivityBanner-05.xml")

    def test_CLAndroidActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidActivityBanner-06.xml")

    def test_CLAndroidActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidActivityBanner-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAndroidActivityBanner Post condition =="
        dumplogger.info("== Setup CLAndroidActivityBanner Post condition ==")
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
