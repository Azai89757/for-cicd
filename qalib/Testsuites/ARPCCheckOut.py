import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCCheckOut Precondition =="
        dumplogger.info("== Setup ARPCCheckOut Precondition ==")

    def test_ARPCCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCCheckOut-01.xml")

    def test_ARPCCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCCheckOut-02.xml")

    # def test_ARPCCheckOut03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCheckOut-03.xml")

    # def test_ARPCCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCheckOut-04.xml")

    # def test_ARPCCheckOut05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCheckOut-05.xml")

    # def test_ARPCCheckOut06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCheckOut-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCCheckOut Post condition =="
        dumplogger.info("== Setup ARPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
