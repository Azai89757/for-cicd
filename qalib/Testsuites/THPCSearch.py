import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCSearch(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSearch Precondition =="
        dumplogger.info("== Setup THPCSearch Precondition ==")

    def test_THPCSearch01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSearch-01.xml")

    def test_THPCSearch02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSearch-02.xml")

    def test_THPCSearch03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSearch-03.xml")

    def test_THPCSearch04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSearch-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSearch Post condition =="
        dumplogger.info("== Setup THPCSearch Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
