#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidCoinsMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidCoinsButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyCoins(arg):
        '''
        ClickMyCoins : Click my coins in coin page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click my coins btn
        xpath = Util.GetXpath({"locate": "my_coins_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click my coins button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCoinsButton.ClickMyCoins')


class AndroidCoinsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoCoinSubTab(arg):
        '''
        GotoCoinSubTab : Go to coin sub tab from coin page
                Input argu :
                    type - all_history / earning / spending
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click my coins btn
        xpath = Util.GetXpath({"locate": type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click sub tab on coin page ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCoinsPage.GotoCoinSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCoinCreditDate(arg):
        '''
        CheckCoinCreditDate : Check coin credit date
                Input argu :
                    title - manual credit coins title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]

        xpath = Util.GetXpath({"locate":"credit_date"}).replace("title_to_be_replace", title)
        AndroidBaseUICore.AndroidGetElements({"locate":xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        credit_date = GlobalAdapter.CommonVar._PageAttributes_

        if Config._TestCaseRegion_ == "VN":
            current_time = XtFunc.GetCurrentDateTime("%H:%M %d-%m-%Y", 0, 0, 0)
            dumplogger.info("current time: %s" % (current_time))

        elif Config._TestCaseRegion_ == "TW":
            current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)
            dumplogger.info("current time: %s" % (current_time))

        ##Only compare date
        if credit_date[:10] == current_time[:10]:
            dumplogger.info("Credit date same with current date.")
        else:
            dumplogger.info("Credit date difference with current date.")
            ret = 0

        OK(ret, int(arg['result']), "AndroidCoinsPage.CheckCoinCreditDate")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoCoinExpiredPage(arg):
        '''
        GotoCoinExpiredPage : Go to coin expired page from coin page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click coins expired date
        xpath = Util.GetXpath({"locate": "coins_expired"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click coins expired date.", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCoinsPage.GotoCoinExpiredPage')
