#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidMicroSiteMethod.py: The def of this file called by XML mainly.
'''

##import python common library
import time

##Import framework library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidGoToMicrositePage(arg):
    '''
    AndroidGoToMicrositePage : Go to microsite url
            Input argu :
                microsite_url : go to microsite page
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    microsite_url = arg["microsite_url"]
    ret = 0

    for retry_times in range(3):
        ##Long press under bar
        xpath = Util.GetXpath({"locate":"long_press_btn"})
        AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        ##Click forbidden zone button
        xpath = Util.GetXpath({"locate":"forbidden_zone_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Forbidden Zone button", "result": "1"})
        time.sleep(10)

        ##Back to last page
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"back", "result": "1"})
        time.sleep(10)

        ##Click forbidden zone button
        xpath = Util.GetXpath({"locate":"forbidden_zone_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Forbidden Zone button", "result": "1"})
        time.sleep(10)

        ##Check navigator button
        xpath = Util.GetXpath({"locate":"navigator_btn"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Forbidden zone page load complete.")

            ##Click navigator button
            xpath = Util.GetXpath({"locate":"navigator_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Navigator button", "result": "1"})
            time.sleep(10)

            ##Input microsite url
            xpath = Util.GetXpath({"locate":"input_microsite_url"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":microsite_url, "result": "1"})
            time.sleep(3)
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})
            time.sleep(3)

            ##Click navigator button on the right
            xpath = Util.GetXpath({"locate":"navigator_right_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Navigator button on the right", "result": "1"})
            time.sleep(10)
            ret = 1
            break
        else:
            ##If forbidden zone page not load complete, go to forbidden page again
            dumplogger.info("Forbidden zone page not load complete.")

            ##Use adb command send url to home page
            home_page_url = "https://shopee." + Config._TestCaseRegion_.lower() + " " + Config._DesiredCaps_['appPackage']
            AndroidBaseUICore.SendADBCommand({"command_type":"IntentUrl", "adb_arg":home_page_url, "is_return": "0", "result": "1"})
            time.sleep(15)

    OK(ret, int(arg['result']), 'AndroidGoToMicrositePage -> ' + microsite_url)


class AndroidMicrositeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        '''
        ClickAgree : Click agree button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click agree button
        xpath = Util.GetXpath({"locate":"agree_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click agree button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSwipeDown(arg):
        '''
        ClickSwipeDown : Click swipe down
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click swipe down
        xpath = Util.GetXpath({"locate":"swipe_down_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "click swipe down", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickSwipeDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherClaim(arg):
        '''
        ClickVoucherClaim : Click voucher claim
                Input argu :
                    type - free_shipping_voucher / shopee_voucher
                    voucher_name - target voucher name you want to click in several voucher grid
                    index - several same voucher name index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']
        voucher_name = arg['voucher_name']
        index = arg['index']

        ##Click voucher claim
        xpath = Util.GetXpath({"locate":type})
        xpath_voucher_name = xpath.replace("voucher_name_to_be_replace", voucher_name)
        xpath_target_sv_claim_btn = xpath_voucher_name.replace("index_to_be_replace", index)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_target_sv_claim_btn, "message":"Click voucher claim", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickVoucherClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherGrid(arg):
        '''
        ClickVoucherGrid : Click voucher grid
                Input argu :
                    voucher_name - target voucher name you want to click in several voucher grid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click voucher grid
        xpath = Util.GetXpath({"locate":"voucher_grid"})
        xpath_voucher_grid = xpath.replace("voucher_name_to_be_replace", voucher_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_voucher_grid, "message":"Click voucher grid", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickVoucherGrid')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherTAndC(arg):
        '''
        ClickVoucherTAndC : Click Voucher T&C
                Input argu :
                    index - several voucher name index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Click voucher T&C
        xpath = Util.GetXpath({"locate":"t&c_btn"})
        xpath_t_and_c_btn = xpath.replace("index_to_be_replace", index)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_t_and_c_btn, "message":"Click voucher T&C", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickVoucherTAndC')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackUp(arg):
        '''
        ClickBackUp : Click back up button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back up button
        xpath = Util.GetXpath({"locate":"back_up_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click back up button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickBackUp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotPopUp(arg):
        '''
        ClickHotspotPopUp : Click hotspot popup
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot popup
        xpath = Util.GetXpath({"locate":"hotspot_popup"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click hotspot popup", "result": "1"})

        ##Click hotspot popup ok button
        xpath = Util.GetXpath({"locate":"hotspot_popup_ok"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click hotspot popup ok", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickHotspotPopUp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotReminder(arg):
        '''
        ClickHotspotReminder : Click hotspot reminder
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot reminder
        xpath = Util.GetXpath({"locate":"hotspot_reminder"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click hotspot reminder", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickHotspotReminder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionGridSeeAll(arg):
        '''
        ClickCollectionGridSeeAll : Click collection grid see all button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click see all btn
        xpath = Util.GetXpath({"locate":"see_all"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see all btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickCollectionGridSeeAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTopRightFunction(arg):
        '''
        ClickTopRightFunction : Click top right function
                Input argu :
                    option - share / back_to_homepage
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click top right function
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image": "top_right_function", "threshold":"0.9", "is_click":"yes", "result": "1"})

        ##Click option button
        xpath = Util.GetXpath({"locate":option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickTopRightFunction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSharingPanel(arg):
        '''
        ClickSharingPanel : Click sharing panel
                Input argu :
                    option - copy_link
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click sharing panel option
        xpath = Util.GetXpath({"locate":option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickSharingPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewMore(arg):
        '''
        ClickViewMore : Click bundle deal view more
                Input argu :
                    bundle_deal_name - bundle deal name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bundle_deal_name = arg["bundle_deal_name"]

        ##Click view more
        xpath = Util.GetXpath({"locate":"view_more"})
        xpath = xpath.replace("name_to_be_replaced", bundle_deal_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click view more", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickViewMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleItem(arg):
        '''
        ClickBundleItem : Click item in bundledeal component
                Input arg :
                    index - the order of items from left to right (value is from 1 to 3)
                    bundle_name - bundle name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        index = arg["index"]
        bundle_name = arg["bundle_name"]

        ##Click item
        xpath = Util.GetXpath({"locate":"bundle_item"})
        xpath = xpath.replace("bundle_name_to_be_replaced", bundle_name)
        xpath = xpath.replace("index_to_be_replaced", str(int(index) + 1))
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bundle item ", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeButton.ClickBundleItem')


class AndroidMicrositeComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
                replace_text - type of argument which you want to replace(optional)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        if "replace_text" in arg:
            replace_text = arg["replace_text"]
            xpath = Util.GetXpath({"locate": button_type})
            xpath = xpath.replace("replaced_text", replace_text)
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})
        else:
            ##Click on button
            xpath = Util.GetXpath({"locate": button_type})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnTextField(arg):
        '''
        ClickOnTextField : Click any type of text field with well defined locator
            Input argu :
                text_type - type of text field which defined by caller
                text_content - text to click
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        text_type = arg["text_type"]
        text_content = arg["text_content"]

        ##Click on text
        xpath = Util.GetXpath({"locate": text_type})
        xpath = xpath.replace("replaced_text", text_content)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click text field: %s, which xpath is %s" % (text_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeComponent.ClickOnTextField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnIcon(arg):
        '''
        ClickOnIcon : Click any type of icon with well defined locator
            Input argu :
                icon_type - type of icon which defined by caller
                replace_text - type of argument which you want to replace(optional)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        if "replace_text" in arg:
            replace_text = arg["replace_text"]
            xpath = Util.GetXpath({"locate": icon_type})
            xpath = xpath.replace("replaced_text", replace_text)
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click icon: %s, which xpath is %s" % (icon_type, xpath), "result": "1"})
        else:
            ##Click on icon
            xpath = Util.GetXpath({"locate": icon_type})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click icon: %s, which xpath is %s" % (icon_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeComponent.ClickOnIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSection(arg):
        '''
        ClickOnSection : Click any type of section with well defined locator
            Input argu :
                section_type - type of section which defined by caller
                replace_text - type of argument which you want to replace(optional)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]

        if "replace_text" in arg:
            replace_text = arg["replace_text"]
            xpath = Util.GetXpath({"locate": section_type})
            xpath = xpath.replace("replaced_text", replace_text)
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click section: %s, which xpath is %s" % (section_type, xpath), "result": "1"})
        else:
            ##Click on section
            xpath = Util.GetXpath({"locate": section_type})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click section: %s, which xpath is %s" % (section_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMicrositeComponent.ClickOnSection')
