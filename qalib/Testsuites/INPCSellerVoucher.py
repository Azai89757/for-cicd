import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCSellerVoucher Precondition =="
        dumplogger.info("== Setup INPCSellerVoucher Precondition ==")

    def test_INPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerVoucher-01.xml")

    def test_INPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerVoucher-02.xml")

    def test_INPCSellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerVoucher-03.xml")

    def test_INPCSellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCSellerVoucher Post condition =="
        dumplogger.info("== Setup INPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
