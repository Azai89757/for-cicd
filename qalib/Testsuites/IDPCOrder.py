
import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCOrder Precondition =="
        dumplogger.info("== Setup IDPCOrder Precondition ==")

    def test_IDPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-01.xml")

    def test_IDPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-02.xml")

    def test_IDPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-03.xml")

    def test_IDPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-04.xml")

    def test_IDPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-05.xml")

    def test_IDPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-06.xml")

    def test_IDPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-07.xml")

    def test_IDPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-08.xml")

    def test_IDPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-09.xml")

    def test_IDPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-10.xml")

    def test_IDPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-11.xml")

    def test_IDPCOrder12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-12.xml")

    def test_IDPCOrder13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCOrder-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCOrder Post condition =="
        dumplogger.info("== Setup IDPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
