#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidShopeeMartMethod.py: The def of this file called by XML mainly.
'''
##import system library
import time

##Import framework common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AndroidShopeeMartPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HomePageNavigation(arg):
        '''
        HomePageNavigation : Click on a icon at the buttom of the home page
                Input argu :
                    type - which progress tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Check type is mall or not
        if type == 'mall':

            ##Back to homepage
            AndroidShopeeMartPage.HomePageNavigation({"type":"home", "result": "1"})

            ##Slide down and check for mall shop
            AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"mall"}), "direction":"down", "isclick":"1", "result": "1"})

        else:

            ##Go to different tab
            time.sleep(3)

            ##Click sub feature
            locate = Util.GetXpath({"locate":type})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in the me page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopeeMartPage.HomePageNavigation -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipeDownToSection(arg):
        '''
        SwipeDownToSection : Swipe down to specific section in shopee mart homepage
                Input argu :
                    section - voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Swipe down to specific section if it exists
        xpath = Util.GetXpath({"locate": section})
        if AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "passok":"0", "times":"7", "result": "1"}):
            AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": xpath, "ratio": "0.5", "result": "1"})
        else:
            dumplogger.info('Failed to find element in shopee mart homepage')
            ret = 0

        OK(ret, int(arg['result']), "AndroidShopeeMartPage.SwipeDownToSection")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShopeeMartBannerExist(arg):
        '''
        CheckShopeeMartBannerExist : Check banner in shopee mart exist
                Input argu :
                    banner_type - mart_carousel / mart_skinny
                    image - OpenCV project file  NOTE: if wanna use mutiple image, seperate image name by ","  ex. photo_name1,photo_name2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        banner_type = arg["banner_type"]
        image = arg["image"]

        ##Retry to compare img
        for retry_count in range(1, 6):

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Set up redirect url
            url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/supermarket"
            dumplogger.info("url = %s" % (url))
            AndroidCommonMethod.AndroidForbiddenZonePage.NavigateUrl({"url": url, "page_type": "rn_page", "result": "1"})
            time.sleep(15)

            ##Check whether if there is mutiliple image
            image_split_result = image.split(",")

            ##Start to begin image compare by different file types
            if len(image_split_result) > 1:
                ##Means to use GIF compare function
                match_result = XtFunc.MatchGIFImg({"mode":"android", "image1": image_split_result[0], "image2": image_split_result[1], "env": Config._EnvType_, "expected_result":arg['result']})
            else:
                ##Means to use Picture compare function
                match_result = XtFunc.MatchPictureImg({"mode":"android", "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                ret = 1
                dumplogger.info("Android Banner image/GIF matching successful at %d run!!!!" % (retry_count))
                break
            else:
                time.sleep(30)
                dumplogger.info("Android Banner image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, int(arg['result']), "AndroidShopeeMartPage.CheckShopeeMartBannerExist -> " + banner_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHorizontalCollection(arg):
        '''
        CheckHorizontalCollection : Check if horizontal collection in shopee mart exists
                Input argu :
                    page_type - homepage / deals_tab
                    title - collection title
                    is_swipe - swipe collection to the past or not (1 / 0)
                    image - OpenCV project file  NOTE: if wanna use mutiple image, seperate image name by ","  ex. photo_name1,photo_name2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        page_type = arg["page_type"]
        title = arg["title"]
        is_swipe = arg["is_swipe"]
        image = arg["image"]

        ##Retry to compare img
        for retry_count in range(1, 6):

            ##Close APP and open
            AndroidCommonMethod.ReopenShopeeApp({"result": "1"})

            ##Set up redirect url
            url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/supermarket"
            dumplogger.info("url = %s" % (url))
            AndroidCommonMethod.AndroidForbiddenZonePage.NavigateUrl({"url": url, "page_type": "rn_page", "result": "1"})
            time.sleep(15)

            ##If page type is deals tab page, then redirect to deals tab page first
            if page_type == "deals_tab":
                xpath = Util.GetXpath({"locate": page_type})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click redirect to deals tab page", "result": "1"})
                time.sleep(5)

            ##Move down to specific collection
            xpath = Util.GetXpath({"locate": "collection_list"})
            xpath = xpath.replace("title_to_be_replaced", title)
            AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "0", "result": "1"})
            AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": xpath, "ratio": "0.6", "result": "1"})
            time.sleep(5)

            ##If need to swipe, then horizontal swipe collection to the last
            if int(is_swipe):
                AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":"7", "result": "1"})
                time.sleep(5)

            ##Check whether if there is mutiliple image
            image_split_result = image.split(",")

            ##Start to begin image compare by different file types
            if len(image_split_result) > 1:
                ##Means to use GIF compare function
                match_result = XtFunc.MatchGIFImg({"mode":"android", "image1": image_split_result[0], "image2": image_split_result[1], "env": Config._EnvType_, "expected_result":arg['result']})
            else:
                ##Means to use Picture compare function
                match_result = XtFunc.MatchPictureImg({"mode":"android", "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                ret = 1
                dumplogger.info("Android horizontal collection image/GIF matching successful at %d run!!!!" % (retry_count))
                break
            else:
                time.sleep(30)
                dumplogger.info("Android horizontal collection image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, int(arg['result']), "AndroidShopeeMartPage.CheckHorizontalCollection")


class AndroidShopeeMartButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        '''
        ClickCategory : Click category
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click category
        xpath = Util.GetXpath({"locate": "category_block"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose category", "result": "1"})

        OK(ret, int(arg['result']), "AndroidShopeeMartButton.ClickCategory")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseButtomTab(arg):
        '''
        ClickChooseButtomTab : Click choose bottom tab
                Input argu :
                    tab - bottom tab (supermarket / on_sale / categories / all_products)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click choose bottom tab
        xpath = Util.GetXpath({"locate": tab})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose bottom tab -> " + tab, "result": "1"})

        OK(ret, int(arg['result']), "AndroidShopeeMartButton.ClickChooseButtomTab")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchFilterPanel(arg):
        '''
        ClickSearchFilterPanel : Click search filter panel
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search filter panel
        xpath = Util.GetXpath({"locate": "search_filter_panel"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search filter panel", "result": "1"})

        OK(ret, int(arg['result']), "AndroidShopeeMartButton.ClickSearchFilterPanel")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionSeeMore(arg):
        '''
        ClickCollectionSeeMore : Click collection see more button
                Input argu :
                    title - collection title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]

        ##Click collection see more
        xpath = Util.GetXpath({"locate": "see_more"})
        xpath = xpath.replace("title_to_be_replaced", title)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click collection see more", "result": "1"})

        OK(ret, int(arg['result']), "AndroidShopeeMartButton.ClickCollectionSeeMore")

class AndroidSearchFilterPanelSectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseItemType(arg):
        '''
        ClickChooseItemType : Click choose item type
                Input argu :
                    type - new_item / discount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click choose item type
        xpath = Util.GetXpath({"locate": type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose item type -> " + type, "result": "1"})

        OK(ret, int(arg['result']), "AndroidSearchFilterPanelSectionButton.ClickChooseItemType")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click reset", "result": "1"})

        OK(ret, int(arg['result']), "AndroidSearchFilterPanelSectionButton.ClickReset")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApply(arg):
        '''
        ClickApply : Click apply
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click apply btn
        xpath = Util.GetXpath({"locate": "apply_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click apply", "result": "1"})

        OK(ret, int(arg['result']), "AndroidSearchFilterPanelSectionButton.ClickApply")

class AndroidMartCategoryPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseFirstLevelCategory(arg):
        '''
        ClickChooseFirstLevelCategory : Click choose L1 category
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click apply btn
        xpath = Util.GetXpath({"locate": "l1_category_block"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click category block", "result": "1"})

        OK(ret, int(arg['result']), "AndroidMartCategoryPageButton.ClickChooseFirstLevelCategory")

class AndroidAllProductPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseFilterTab(arg):
        '''
        ClickChooseFilterTab : Click choose search filter tab
                Input argu :
                    filter_tab - relevance / discount / top_sales / price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        filter_tab = arg["filter_tab"]

        ##Click filter tab
        xpath = Util.GetXpath({"locate": filter_tab})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose filter tab -> " + filter_tab, "result": "1"})

        OK(ret, int(arg['result']), "AndroidAllProductPageButton.ClickChooseFilterTab")
