#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminNewNotificationMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time
import json

##Import common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchNotificationAdmin(arg):
    '''
    LaunchNotificationAdmin : Go to notification admin page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    NotificationAdmin_cookie = {}

    if 'NotificationAdmin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_notification", "method":"select", "verify_result":"", "assign_data_list":"", "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        NotificationAdmin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        NotificationAdmin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        NotificationAdmin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['NotificationAdmin'] = [NotificationAdmin_cookie]

    ##notification admin url
    url = "https://admin.noti." + Config._EnvType_ + ".shopee.io"

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"NotificationAdmin", "result": "1"})
    time.sleep(5)

    ##Go to Notification Admin
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})
    time.sleep(10)

    ##Choose country
    ##Click drop down list
    xpath = Util.GetXpath({"locate": "drop_down_list"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click drop down list", "result": "1"})

    ##Click country
    xpath = Util.GetXpath({"locate": "country"})
    xpath = xpath.replace('replace_country', Config._TestCaseRegion_)
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click country", "result": "1"})
    time.sleep(10)

    OK(ret, int(arg['result']), 'LaunchNotificationAdmin')


class AdminInAppPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskInfo(arg):
        '''
        InputTaskInfo : Input task info
                Input argu :
                    task_name - create
                    owner - owner
                    description - description
                    app - app
                    region - region
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        task_name = arg["task_name"]
        owner = arg["owner"]
        description = arg["description"]
        app = arg["app"]
        region = arg["region"]

        ret = 1

        ##Input task name
        if task_name:
            xpath = Util.GetXpath({"locate": "task_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "inapp", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select app
        if app:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_app"})
            option = Util.GetXpath({"locate":"app_dropdown_list"})
            option = option.replace('replace_app', app)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "app", "selector": selector, "option": option, "result": "1"})

            ##Click other place to commit option
            xpath = Util.GetXpath({"locate":"white_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click white space", "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "inapp", "page_type": "create", "action": "input", "region": region, "result": "1"})

            ##Click other place to commit option
            xpath = Util.GetXpath({"locate":"white_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminInAppPage.InputTaskInfo')


class AdminPnArPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskInfo(arg):
        '''
        InputTaskInfo : Input ringtone info
                Input argu :
                    task_name - create
                    owner - owner
                    description - description
                    region - region
                    absolute_path - absolute path
                    pn_content - PN content
                    ar_folder - AR content
                    ar_content - AR content
                    ar_icon - AR icon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        task_name = arg["task_name"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]
        absolute_path = arg["absolute_path"]
        pn_content = arg["pn_content"]
        ar_folder = arg["ar_folder"]
        ar_content = arg["ar_content"]
        ar_icon = arg["ar_icon"]

        ret = 1

        ##Input task name
        if task_name:
            xpath = Util.GetXpath({"locate": "task_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "pnar", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "pnar", "page_type": "create", "action": "input", "region": region, "result": "1"})

            ##Click other place to commit option
            xpath = Util.GetXpath({"locate":"white_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click white space", "result": "1"})

        ##Input absolute path
        if absolute_path:
            xpath = Util.GetXpath({"locate": "absolute_path"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": absolute_path, "result": "1"})

        ##Input PN content
        if pn_content:
            xpath = Util.GetXpath({"locate": "pn_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_content, "result": "1"})

        ##Select AR folder
        if ar_folder:
            ##Click notification folder drop down menu option
            AdminNotificationPage.InputNotificationFolder({"page": "pnar", "page_type": "create", "action": "input", "notification_folder": ar_folder, "result": "1"})

        ##Input AR content
        if ar_content:
            xpath = Util.GetXpath({"locate": "ar_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_content, "result": "1"})

        ##Input AR icon
        if ar_icon:
            ##Click upload button
            xpath = Util.GetXpath({"locate":"upload_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

            ##Upload icon
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": ar_icon, "file_type": "png", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPnArPage.InputTaskInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMergePnDetail(arg):
        '''
        InputMergePnDetail : Input merge pn detail
                Input argu :
                    pn_content - pn content
                    pn_redirection - url(ex: https://staging.shopee.co.id/)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pn_content = arg["pn_content"]
        pn_redirection = arg["pn_redirection"]
        ret = 1

        ##Input merge pn content
        if pn_content:
            xpath = Util.GetXpath({"locate": "pn_content"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_content, "result": "1"})

        ##Input merge pn redirection
        if pn_redirection:
            xpath = Util.GetXpath({"locate": "pn_redirection"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_redirection, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPnArPage.InputMergePnDetail')


class AdminPnArButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckBox(arg):
        '''
        ClickOnCheckBox : Click on check box
                Input argu :
                    box_type - merge_pn / highligh_ar
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        box_type = arg["box_type"]
        ret = 1

        ##Click on checkbox
        xpath = Util.GetXpath({"locate":box_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPnArButton.ClickOnCheckBox -> ' + box_type)


class AdminManualQueuePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    notification_name - notification name
                    notification_folder - notification folder
                    description - description
                    priority - priority
                    tags - tags to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        notification_name = arg["notification_name"]
        notification_folder = arg["notification_folder"]
        description = arg["description"]
        priority = arg["priority"]
        tags = arg["tags"]
        ret = 1

        ##Input status
        if notification_name:
            xpath = Util.GetXpath({"locate": "notification_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": notification_name, "result": "1"})

        ##Select notification folder
        if notification_folder:
            ##Click notification folder drop down menu option
            AdminNotificationPage.InputNotificationFolder({"page": "manual_queue", "page_type": "create", "action": "input", "notification_folder": notification_folder, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select priority
        if priority:
            ##Click priority drop down menu option
            AdminNotificationPage.InputPriority({"page": "manual_queue", "page_type": "create", "action": "input", "priority": priority, "result": "1"})

        ##Select tags
        if tags:
            ##Click priority drop down menu option
            AdminNotificationPage.InputTaskTags({"page": "manual_queue", "page_type": "create", "action": "select", "tags": tags, "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualQueuePage.InputGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTemplate(arg):
        '''
        InputTemplate : Input template
                Input argu :
                    pn_title - pn title
                    pn_content - pn content
                    ar_title - ar title
                    ar_content - ar content
                    ar_url - ar url
                    ar_icon - ar icon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pn_title = arg["pn_title"]
        pn_content = arg["pn_content"]
        ar_title = arg["ar_title"]
        ar_content = arg["ar_content"]
        ar_url = arg["ar_url"]
        ar_icon = arg["ar_icon"]
        ret = 1

        ##Input PN title
        if pn_title:
            xpath = Util.GetXpath({"locate": "pn_title"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_title, "result": "1"})

        ##Input PN Content
        if pn_content:
            xpath = Util.GetXpath({"locate": "pn_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_content, "result": "1"})

        ##Input AR title
        if ar_title:
            xpath = Util.GetXpath({"locate": "ar_title"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_title, "result": "1"})

        ##Input AR Content
        if ar_content:
            xpath = Util.GetXpath({"locate": "ar_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_content, "result": "1"})

        ##Input AR Url
        if ar_url:
            xpath = Util.GetXpath({"locate": "ar_url"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_url, "result": "1"})

        ##Upload image
        if ar_icon:
            ##Click upload button
            xpath = Util.GetXpath({"locate":"upload_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

            ##Upload icon
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": ar_icon, "file_type": "png", "result": "1"})
            time.sleep(5)

        ##Select classification
        xpath = Util.GetXpath({"locate":"content_item"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select classification", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualQueuePage.InputTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadUserId(arg):
        '''
        UploadUserId : Upload user id
                Input argu :
                    page - test / schedule
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        file_name = arg["file_name"]
        ret = 1

        if page == "schedule":
            ##Click audience
            xpath = Util.GetXpath({"locate":"upload_id_item"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click Upload Userids", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": page})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

        ##Upload icon
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminManualQueuePage.UploadUserId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputARExpiry(arg):
        '''
        InputARExpiry : Input AR expiry
                Input argu :
                    expiry_type - default / manual
                    expiry_time - only can set minutes and (ex: 1hr 10 -> 70)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        expiry_type = arg["expiry_type"]
        expiry_time = arg["expiry_time"]
        ret = 1

        ##Click AR expiry type
        xpath = Util.GetXpath({"locate": expiry_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click AR expiry type button", "result": "1"})

        if expiry_type == "manual":
            ##AR Expriy time is assemble by current time、GMT and expriy time
            manual_select_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(expiry_time), 0)

            ##Click calander input field
            xpath = Util.GetXpath({"locate": "calendar_selector"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set calander", "result": "1"})

            ##Input expriy time
            xpath = Util.GetXpath({"locate": "input_calendar"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": manual_select_time, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualQueuePage.InputARExpiry')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSendTime(arg):
        '''
        InputSendTime : Input send time
                Input argu :
                    send_type - immediately / schedule
                    schedule_time - only can set minutes and (ex: 1hr 10 -> 70)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        send_type = arg["send_type"]
        schedule_time = arg["schedule_time"]
        ret = 1

        ##Click AR expiry type
        xpath = Util.GetXpath({"locate": send_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click send type button", "result": "1"})

        if send_type == "schedule":
            ##AR Expriy time is assemble by current time、GMT and expriy time
            manual_select_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(schedule_time), 0)

            ##Click calander input field
            xpath = Util.GetXpath({"locate": "calendar_selector"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set calander", "result": "1"})

            ##Input expriy time
            xpath = Util.GetXpath({"locate": "input_calendar"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": manual_select_time, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualQueuePage.InputSendTime')


class AdminCRMQueuePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input General info
                Input argu :
                    task_name - create
                    priorty - priorty
                    time - real_time / preferred_time_solt
                    select_time_slot - Time slot
                    channel - pn / ar
                    notification_folder - notification folder
                    queue_type - queue type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        task_name = arg["task_name"]
        priority = arg["priority"]
        time = arg["time"]
        select_time_slot = arg["select_time_slot"]
        channel = arg["channel"]
        notification_folder = arg["notification_folder"]
        queue_type = arg["queue_type"]
        ret = 1

        ##Input task name
        if task_name:
            xpath = Util.GetXpath({"locate": "task_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Input priority
        if priority:
            xpath = Util.GetXpath({"locate":"priority"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": priority, "result": "1"})

        ##Select time - real time
        if time == "real_time":
            xpath = Util.GetXpath({"locate":"real_time"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click select real time", "result": "1"})

        ##Select time - preferred_time_solt and need to select time slot
        elif time == "preferred_time_solt":
            ##Click radio box
            xpath = Util.GetXpath({"locate":"preferred_time_solt"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click select preferred time solt", "result": "1"})

            ##Click selector
            xpath = Util.GetXpath({"locate":"select_time_slot"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click select time slot", "result": "1"})

            ##Input time slot value
            xpath = Util.GetXpath({"locate":"input_time_slot"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": select_time_slot, "result": "1"})

            ##Click drop down menu option
            xpath = Util.GetXpath({"locate":"time_slot_dropdown_list"})
            xpath = xpath.replace("replace_time_slot", select_time_slot)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click select time slot", "result": "1"})

        ##Select channel
        if channel:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_channel"})
            option = Util.GetXpath({"locate":"channel_dropdown_list"})
            option = option.replace("replace_channel", channel)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "channel", "selector": selector, "option": option, "result": "1"})

        ##Select notification folder
        if notification_folder:
            ##Click notification folder drop down menu option
            AdminNotificationPage.InputNotificationFolder({"page": "crm_queue", "page_type": "create", "action": "input", "notification_folder": notification_folder, "result": "1"})

        ##Select queue type
        if queue_type:
            ##Click and input queue type
            selector = Util.GetXpath({"locate":"select_queue_type"})
            input_field = Util.GetXpath({"locate":"input_queue_type"})
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "queue_type", "selector": selector, "input_field": input_field, "input_value": notification_folder, "result": "1"})

            ##Get selector、input field and option xpath to select option
            option = Util.GetXpath({"locate":"queue_type_dropdown_list"})
            option = option.replace("replace_queue_type", queue_type)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "queue_type", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCRMQueuePage.InputGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTemplate(arg):
        '''
        InputTemplate : Input template
                Input argu :
                    pn_title - pn title
                    pn_content - pn content
                    ar_title - ar title
                    ar_content - ar content
                    ar_url - ar url
                    pc_url - pc url
                    ar_icon - ar icon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pn_title = arg["pn_title"]
        pn_content = arg["pn_content"]
        ar_title = arg["ar_title"]
        ar_content = arg["ar_content"]
        ar_url = arg["ar_url"]
        pc_url = arg["pc_url"]
        ar_icon = arg["ar_icon"]
        ret = 1

        ##Input PN title
        if pn_title:
            xpath = Util.GetXpath({"locate": "pn_title"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_title, "result": "1"})

        ##Input PN Content
        if pn_content:
            xpath = Util.GetXpath({"locate": "pn_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_content, "result": "1"})

        ##Input AR title
        if ar_title:
            xpath = Util.GetXpath({"locate": "ar_title"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_title, "result": "1"})

        ##Input AR Content
        if ar_content:
            xpath = Util.GetXpath({"locate": "ar_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_content, "result": "1"})

        ##Input AR Url
        if ar_url:
            xpath = Util.GetXpath({"locate": "ar_url"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ar_url, "result": "1"})

        ##Input PC Url
        if pc_url:
            xpath = Util.GetXpath({"locate": "pc_url"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pc_url, "result": "1"})

        ##Upload image
        if ar_icon:
            ##Click upload button
            xpath = Util.GetXpath({"locate":"upload_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

            ##Upload icon
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": ar_icon, "file_type": "png", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCRMQueuePage.InputTemplate')


class AdminEmailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskInfo(arg):
        '''
        InputTaskInfo : Input task info
                Input argu :
                    action - input / select / other_space
                    email_task_name - email task name
                    category - category
                    owner - owner
                    sender_name - sender name
                    description - description
                    email_template - email template
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action = arg["action"]
        email_task_name = arg["email_task_name"]
        category = arg["category"]
        owner = arg["owner"]
        sender_name = arg["sender_name"]
        description = arg["description"]
        email_template = arg["email_template"]

        ret = 1

        ##Input email task name
        if email_task_name:
            xpath = Util.GetXpath({"locate": "email_task_name"})

            ##Auto create task and task name cannot be repeated, so use current time to replace it
            if "Auto" in email_task_name and "input" in action:
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})
                BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

            ##Input email task name field by editing
            else:
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": email_task_name, "result": "1"})
                BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##In xml use email_task_name="" to clear email_task_name field value
        else:
            xpath = Util.GetXpath({"locate": "email_task_name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input / Select category
        if category:
            AdminNotificationPage.InputCategory({"page": "email", "page_type": "create", "action": action, "category": category, "result": "1"})

        ##Input / Select owner
        if owner:
            AdminNotificationPage.InputOwner({"page": "email", "page_type": "create", "action": action, "owner": owner, "result": "1"})

        ##Input / Select sender_name
        if sender_name:
            selector = Util.GetXpath({"locate":"select_sender_name"})
            input_field = Util.GetXpath({"locate": "input_sender_name"})
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "sender_name", "selector": selector, "input_field": input_field, "input_value": sender_name, "result": "1"})

            ##action type - input, by pressing "enter" to input value
            if "input" in action:
                time.sleep(1)
                BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})

            ##action type - select, by selecting dropdown list value
            elif "select" in action:
                option = Util.GetXpath({"locate":"sender_name_dropdown_list"})
                option = option.replace('replace_sender_name', sender_name)
                AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sender_name", "selector": selector, "option": option, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        else:
            ##Click description and click other space
            if "other_space" in action:
                xpath = Util.GetXpath({"locate": "select_description"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click description", "result": "1"})
                xpath = Util.GetXpath({"locate": "other_label"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other label", "result": "1"})

            ##In xml use description="" to clear description field value
            else:
                xpath = Util.GetXpath({"locate": "description"})
                BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input / Select email_template
        if email_template:
            selector = Util.GetXpath({"locate":"select_email_template"})
            input_field = Util.GetXpath({"locate": "input_email_template"})
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "email_template", "selector": selector, "input_field": input_field, "input_value": email_template, "result": "1"})

            ##action type - input, by pressing "enter" to input value
            if "input" in action:
                xpath = Util.GetXpath({"locate": "input_email_template"})
                time.sleep(2)
                BaseUILogic.KeyboardAction({"actiontype": "down", "locate": xpath, "result": "1"})
                time.sleep(2)
                BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

            ##action type - select, by selecting dropdown list value
            elif "select" in action:
                option = Util.GetXpath({"locate":"email_template_dropdown_list"})
                option = option.replace('replace_email_template', email_template)
                AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "email_template", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminEmailPage.InputTaskInfo')


class AdminSMSPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    message_name - message name
                    category - category
                    subcategory - subcategory
                    task_priority - task priority
                    owner - owner
                    description - description
                    region - region
                    tech_lead_approver - tech lead approver
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message_name = arg["message_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        task_priority = arg["task_priority"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]
        sender_name = arg["sender_name"]
        tech_lead_approver = arg["tech_lead_approver"]

        ret = 1

        ##Input message name
        xpath = Util.GetXpath({"locate": "message_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select category
        if category:
            ##Click priority drop down menu option
            AdminNotificationPage.InputCategory({"page": "sms", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_subcategory"})
            option = Util.GetXpath({"locate":"subcategory_dropdown_list"})
            option = option.replace('replace_subcategory', subcategory)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "subcategory", "selector": selector, "option": option, "result": "1"})

        ##Select task priority
        if task_priority:
            ##Click priority drop down menu option
            AdminNotificationPage.InputTaskPriority({"page": "sms", "page_type": "create", "task_priority": task_priority, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "sms", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "sms", "page_type": "create", "action": "input", "region": region, "result": "1"})

        ##Click other place
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other place", "result": "1"})

        ##Select sender name
        if sender_name:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_sender_name"})
            option = Util.GetXpath({"locate":"sender_name_dropdown_list"})
            option = option.replace('replace_sender_name', sender_name)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sender_name", "selector": selector, "option": option, "result": "1"})

        ##Select tech lead approver
        if tech_lead_approver:
            ##Click approver drop down menu option
            AdminNotificationPage.InputApprover({"page": "sms", "page_type": "create", "action": "input", "approver": tech_lead_approver, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSMSPage.InputGeneralInfo')


class AdminManualSMSPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    message_name - message name
                    category - category
                    subcategory - subcategory
                    task_priority - task priority
                    owner - owner
                    description - description
                    region - region
                    sender_name - sender name
                    sending_frequency - sending frequency
                    num_message_sent - num of message sent
                    task_owners_email - task owner email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message_name = arg["message_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        task_priority = arg["task_priority"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]
        sender_name = arg["sender_name"]
        sending_frequency = arg["sending_frequency"]
        num_message_sent = arg["num_message_sent"]
        task_owners_email = arg["task_owners_email"]

        ret = 1

        ##Input message name
        xpath = Util.GetXpath({"locate": "message_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select category
        if category:
            ##Click category drop down menu option
            AdminNotificationPage.InputCategory({"page": "manual_sms", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_subcategory"})
            option = Util.GetXpath({"locate":"subcategory_dropdown_list"})
            option = option.replace('replace_subcategory', subcategory)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "subcategory", "selector": selector, "option": option, "result": "1"})

        ##Select task priority
        if task_priority:
            ##Click priority drop down menu option
            AdminNotificationPage.InputTaskPriority({"page": "manual_sms", "page_type": "create", "task_priority": task_priority, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "manual_sms", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "manual_sms", "page_type": "create", "action": "input", "region": region, "result": "1"})

        ##Click other place
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other place", "result": "1"})

        ##Select sender name
        if sender_name:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_sender_name"})
            option = Util.GetXpath({"locate":"sender_name_dropdown_list"})
            option = option.replace('replace_sender_name', sender_name)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sender_name", "selector": selector, "option": option, "result": "1"})

        ##Select sending frequency
        if sending_frequency:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_sending_frequency"})
            option = Util.GetXpath({"locate":"sending_frequency_dropdown_list"})
            option = option.replace('replace_sending_frequency', sending_frequency)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sending_frequency", "selector": selector, "option": option, "result": "1"})

        ##Select number of message sent
        if num_message_sent:
            xpath = Util.GetXpath({"locate":"select_num_message_sent"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": num_message_sent, "result": "1"})

            ##Click other place
            xpath = Util.GetXpath({"locate":"other_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other space", "result": "1"})

        ##Select task owner email
        if task_owners_email:
            ##Click approver drop down menu option
            AdminNotificationPage.InputApprover({"page": "manual_sms", "page_type": "create", "action": "input", "approver": task_owners_email, "result": "1"})

            ##Click other place
            xpath = Util.GetXpath({"locate":"other_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualSMSPage.InputGeneralInfo')


class AdminWhatsAppPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    message_name - message name
                    category - category
                    subcategory - subcategory
                    task_priority - task priority
                    owner - owner
                    description - description
                    region - region
                    sending_frequency - sending frequency
                    num_message_sent - num of message sent
                    task_owners_email - task owner email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message_name = arg["message_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        task_priority = arg["task_priority"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]
        sending_frequency = arg["sending_frequency"]
        num_message_sent = arg["num_message_sent"]
        task_owners_email = arg["task_owners_email"]

        ret = 1

        ##Input message name
        if message_name:
            xpath = Util.GetXpath({"locate": "message_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select category
        if category:
            ##Click category drop down menu option
            AdminNotificationPage.InputCategory({"page": "manual_whatsapp", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_subcategory"})
            option = Util.GetXpath({"locate":"subcategory_dropdown_list"})
            option = option.replace('replace_subcategory', subcategory)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "subcategory", "selector": selector, "option": option, "result": "1"})

        ##Select task priority
        if task_priority:
            ##Click task priority drop down menu option
            AdminNotificationPage.InputTaskPriority({"page": "manual_whatsapp", "page_type": "create", "action": "input", "task_priority": task_priority, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "manual_whatsapp", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "manual_whatsapp", "page_type": "create", "action": "input", "region": region, "result": "1"})

        ##Click other place
        xpath = Util.GetXpath({"locate": "message_name"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other place", "result": "1"})

        ##Select sending frequency
        if sending_frequency:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_sending_frequency"})
            option = Util.GetXpath({"locate":"sending_frequency_dropdown_list"})
            option = option.replace('replace_sending_frequency', sending_frequency)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sending_frequency", "selector": selector, "option": option, "result": "1"})

        ##Select number of message sent
        if num_message_sent:
            xpath = Util.GetXpath({"locate":"select_num_message_sent"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": num_message_sent, "result": "1"})

            ##Click other place
            xpath = Util.GetXpath({"locate":"other_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other space", "result": "1"})

        ##Select task owner email
        if task_owners_email:
            ##Click approver drop down menu option
            AdminNotificationPage.InputApprover({"page": "manual_sms", "page_type": "create", "action": "input", "approver": task_owners_email, "result": "1"})

            ##Click other place
            xpath = Util.GetXpath({"locate":"other_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhatsAppPage.InputGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTemplate(arg):
        '''
        InputTemplate : Input message template
                Input argu :
                    whatsapp_channel - whatsapp channel
                    message_type - message type
                    template_name - template name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        whatsapp_channel = arg["whatsapp_channel"]
        message_type = arg["message_type"]
        template_name = arg["template_name"]

        ret = 1

        ##Select whatsapp channel
        if whatsapp_channel:
            ##Click and input whatsapp channel value
            selector = Util.GetXpath({"locate":"select_whatsapp_channel"})
            input_field = Util.GetXpath({"locate": "input_whatsapp_channel"})
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "whatsapp_channel", "selector": selector, "input_field": input_field, "input_value": whatsapp_channel, "result": "1"})

            ##Get selector、input field and option xpath to select option
            option = Util.GetXpath({"locate":"whatsapp_channel_dropdown_list"})
            option = option.replace('replace_whatsapp_channel', whatsapp_channel)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "whatsapp_channel", "selector": selector, "option": option, "result": "1"})

        ##Select whatsapp message type
        if message_type:
            ##Click selector and get xpath to select option
            selector = Util.GetXpath({"locate":"select_message_type"})
            option = Util.GetXpath({"locate":"message_type_dropdown_list"})
            option = option.replace('replace_message_type', message_type)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "whatsapp_channel", "selector": selector, "option": option, "result": "1"})

        ##Select template name
        if template_name:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_template_name"})
            input_field = Util.GetXpath({"locate": "input_template_name"})
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "template_name", "selector": selector, "input_field": input_field, "input_value": template_name, "result": "1"})

            ##Click drop down menu option
            option = Util.GetXpath({"locate":"template_name_dropdown_list"})
            option = option.replace('replace_template_name', template_name)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "template_name", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhatsAppPage.InputTemplate')


class AdminNotiWhatsAppTemplate:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    template_name - template name
                    template_category - category
                    language - language
                    head - None / Text / Media (Add a text title or choose a type of media for the header)
                    body - body (Add a text of media for the body)
                    footer - footer (Add a text of media for the footer)
                    act_button - None / Quick replay / Call To Action (Create interactive button for customer's response or take action)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        template_name = arg["template_name"]
        template_category = arg["template_category"]
        language = arg["language"]
        head = arg["head"]
        body = arg["body"]
        footer = arg["footer"]
        act_button = arg["act_button"]

        ret = 1

        ##Input template name
        if template_name:
            xpath = Util.GetXpath({"locate": "template_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": template_name + "_" + str(GlobalAdapter.TrackerVar._CurUnixTime_), "result": "1"})

        ##Select template category
        if template_category:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_template_category"})
            option = Util.GetXpath({"locate":"template_category_dropdown_list"})
            option = option.replace('replace_template_category', template_category)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "template_category", "selector": selector, "option": option, "result": "1"})

        ##Select language
        if language == "check":
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_language"})
            BaseUICore.Click({"method": "xpath", "locate": selector, "message": "Click selector -> " + language, "result": "1"})

            ##Click other place
            other_place = Util.GetXpath({"locate":"other_place"})
            BaseUICore.Click({"method": "xpath", "locate": other_place, "message": "Click other place", "result": "1"})

        else:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_language"})
            option = Util.GetXpath({"locate":"language_dropdown_list"})
            option = option.replace('replace_language', language)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "language", "selector": selector, "option": option, "result": "1"})

        ##Select head
        if head:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_head"})
            option = Util.GetXpath({"locate":"head_dropdown_list"})
            option = option.replace('replace_head', head)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "head", "selector": selector, "option": option, "result": "1"})

        ##Select body
        if body:
            xpath = Util.GetXpath({"locate": "body"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": body, "result": "1"})

        ##Input footer
        if footer:
            xpath = Util.GetXpath({"locate": "footer"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": footer, "result": "1"})

        ##Select interactive button
        if act_button:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_act_button"})
            option = Util.GetXpath({"locate":"act_button_dropdown_list"})
            option = option.replace('replace_act_button', act_button)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "act_button", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotiWhatsAppTemplate.InputGeneralInfo')


class AdminManualLinePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    message_name - message name
                    category - category
                    subcategory - subcategory
                    task_priority - task priority
                    owner - owner
                    line_official_account - line official account
                    description - description
                    region - region
                    sending_frequency - sending frequency
                    num_message_sent - num of message sent
                    task_owners_email - task owner email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message_name = arg["message_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        task_priority = arg["task_priority"]
        owner = arg["owner"]
        line_official_account = arg["line_official_account"]
        description = arg["description"]
        region = arg["region"]
        sending_frequency = arg["sending_frequency"]
        num_message_sent = arg["num_message_sent"]
        task_owners_email = arg["task_owners_email"]

        ret = 1

        ##Input message name
        if message_name:
            xpath = Util.GetXpath({"locate": "message_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select category
        if category:
            ##Click and input category value
            AdminNotificationPage.InputCategory({"page": "manual_line", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Click selector
            selector = Util.GetXpath({"locate":"select_subcategory"})
            option = Util.GetXpath({"locate":"subcategory_dropdown_list"})
            option = option.replace('replace_subcategory', subcategory)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "subcategory", "selector": selector, "option": option, "result": "1"})

        ##Select task priority
        if task_priority:
            ##Click selector
            AdminNotificationPage.InputTaskPriority({"page": "manual_line", "page_type": "create", "task_priority": task_priority, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "manual_line", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Select line official account
        if line_official_account:
            ##Click selector
            selector = Util.GetXpath({"locate":"select_line_official_account"})

            ##Click drop down menu option
            option = Util.GetXpath({"locate":"line_official_account_dropdown_list"})
            option = option.replace('replace_line_official_account', line_official_account)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "line_official_account", "selector": selector, "option": option, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "manual_line", "page_type": "create", "action": "input", "region": region, "result": "1"})

        ##Click other place
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other place", "result": "1"})

        ##Select sending frequency
        if sending_frequency:
            ##Click selector
            selector = Util.GetXpath({"locate":"select_sending_frequency"})

            ##Click drop down menu option
            option = Util.GetXpath({"locate":"sending_frequency_dropdown_list"})
            option = option.replace('replace_sending_frequency', sending_frequency)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sending_frequency", "selector": selector, "option": option, "result": "1"})

        ##Select number of message sent
        if num_message_sent:
            xpath = Util.GetXpath({"locate":"select_num_message_sent"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": num_message_sent, "result": "1"})

            ##Click other place
            xpath = Util.GetXpath({"locate":"other_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other space", "result": "1"})

        ##Select task owner email
        if task_owners_email:
            ##Click approver drop down menu option
            AdminNotificationPage.InputApprover({"page": "manual_line", "page_type": "create", "action": "input", "approver": task_owners_email, "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualLinePage.InputGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTemplate(arg):
        '''
        InputTemplate : Input message template
                Input argu :
                    message_send - message send
                    message_type - text / images / confirm button template
                    text_content - text_content(for message type text)
                    image - image(for message type images)
                    text - text(for message type confirm button template)
                    button_label - button label(for message type confirm button template, need input two label)
                    button_url - button url(for message type confirm button template, need input two url)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message_send = arg["message_send"]
        message_type = arg["message_type"]
        text_content = arg["text_content"]
        image = arg["image"]
        text = arg["text"]
        button_label = arg["button_label"]
        button_url = arg["button_url"]

        ret = 1

        ##Select message send
        if message_send:
            ##Click selector
            selector = Util.GetXpath({"locate":"select_message_send"})

            ##Click drop down menu option
            option = Util.GetXpath({"locate":"message_send_dropdown_list"})
            option = option.replace('replace_message_send', message_send)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "message_send", "selector": selector, "option": option, "result": "1"})

        ##Select template name
        if message_type:
            ##Click selector
            selector = Util.GetXpath({"locate":"select_message_type"})

            ##Click drop down menu option
            option = Util.GetXpath({"locate":"message_type_dropdown_list"})
            option = option.replace('replace_message_type', message_type)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "message_type", "selector": selector, "option": option, "result": "1"})

        ##Input text content
        if text_content:
            xpath = Util.GetXpath({"locate": "text_content"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_content, "result": "1"})

        ##Upload image
        if image:
            ##Click upload button
            xpath = Util.GetXpath({"locate":"upload_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

            ##Upload icon
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": image, "file_type": "jpg", "result": "1"})
            time.sleep(5)

        ##Input button label and button url
        if button_label and button_url:
            ##Input button label1
            xpath = Util.GetXpath({"locate": "button_label1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": button_label.split(",")[0], "result": "1"})

            ##Input button url1
            xpath = Util.GetXpath({"locate": "button_url1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": button_url.split(",")[0], "result": "1"})

            ##Input button label2
            xpath = Util.GetXpath({"locate": "button_label2"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": button_label.split(",")[1], "result": "1"})

            ##Input button url2
            xpath = Util.GetXpath({"locate": "button_url2"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": button_url.split(",")[1], "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualLinePage.InputTemplate')


class AdminNotiFlowBuilderPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    flow_name - flow name
                    category - category
                    subcategory - subcategory
                    owner - owner
                    description - description
                    region - region
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        flow_name = arg["flow_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]

        ret = 1

        ##Input flow name
        if flow_name:
            xpath = Util.GetXpath({"locate": "flow_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select category
        if category:
            ##Click and input category value
            AdminNotificationPage.InputCategory({"page": "noti_flow_builder", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Click selector
            selector = Util.GetXpath({"locate":"select_subcategory"})
            option = Util.GetXpath({"locate":"subcategory_dropdown_list"})
            option = option.replace('replace_subcategory', subcategory)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "subcategory", "selector": selector, "option": option, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "noti_flow_builder", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "noti_flow_builder", "page_type": "create", "action": "input", "region": region, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotiFlowBuilderPage.InputGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToolBar(arg):
        '''
        InputToolBar : Input tool bar
                Input argu :
                    tool_bar - tool bar
                    channel - channel
                    subchannel - sub channel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        tool_bar = arg["tool_bar"]
        channel = arg["channel"]
        subchannel = arg["subchannel"]

        ret = 1

        ##Input incoming notification
        ##Click select button
        xpath = Util.GetXpath({"locate": "select"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click select button", "result": "1"})

        ##Click tool bar
        xpath = Util.GetXpath({"locate": "tool_bar"})
        xpath = xpath.replace("replace_tool_bar", tool_bar)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click tool bar button -> %s" % (tool_bar), "result": "1"})

        ##Select a channel
        selector = Util.GetXpath({"locate": "select_channel"})
        option = Util.GetXpath({"locate":"channel_dropdown_list"})
        option = option.replace('replace_channel', channel)
        AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "channel", "selector": selector, "option": option, "result": "1"})

        ##Select a sub channel
        if subchannel:
            selector = Util.GetXpath({"locate": "select_subchannel"})
            option = Util.GetXpath({"locate":"subchannel_dropdown_list"})
            option = option.replace('replace_subchannel', subchannel)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "channel", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotiFlowBuilderPage.InputToolBar')


class AdminNotiManualFlowBuilderPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    flow_name - flow name
                    category - category
                    subcategory - subcategory
                    owner - owner
                    description - description
                    region - region
                    sending_frequency - sending frequency
                    task_owners_email - task owners email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        flow_name = arg["flow_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]
        sending_frequency = arg["sending_frequency"]
        task_owners_email = arg["task_owners_email"]
        ret = 1

        ##Input flow name
        if flow_name:
            xpath = Util.GetXpath({"locate": "flow_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.TrackerVar._CurUnixTime_, "result": "1"})

        ##Select category
        if category:
            ##Click and input category value
            AdminNotificationPage.InputCategory({"page": "manual_noti_flow_builder", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Click and input subcategory value
            AdminNotificationPage.InputSubCategory({"page": "manual_noti_flow_builder", "page_type": "create", "action": "select", "subcategory": subcategory, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "manual_noti_flow_builder", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "manual_noti_flow_builder", "page_type": "create", "action": "input", "region": region, "result": "1"})

        ##Click other place
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other place", "result": "1"})

        ##Select sending frequency
        if sending_frequency:
            ##Get selector、input field and option xpath to select option
            selector = Util.GetXpath({"locate":"select_sending_frequency"})
            option = Util.GetXpath({"locate":"sending_frequency_dropdown_list"})
            option = option.replace('replace_sending_frequency', sending_frequency)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "sending_frequency", "selector": selector, "option": option, "result": "1"})

        ##Select task owner email
        if task_owners_email:
            ##Click approver drop down menu option
            AdminNotificationPage.InputApprover({"page": "manual_noti_flow_builder", "page_type": "create", "action": "input", "approver": task_owners_email, "result": "1"})

            ##Click other place
            xpath = Util.GetXpath({"locate":"other_space"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click other space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotiManualFlowBuilderPage.InputGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToolBar(arg):
        '''
        InputToolBar : Input tool bar
                Input argu :
                    tool_bar - tool bar
                    channel - channel
                    subchannel - sub channel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        tool_bar = arg["tool_bar"]
        channel = arg["channel"]
        subchannel = arg["subchannel"]

        ret = 1

        ##Input incoming notification
        ##Click select button
        xpath = Util.GetXpath({"locate": "select"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click select button", "result": "1"})

        ##Click tool bar
        xpath = Util.GetXpath({"locate": "tool_bar"})
        xpath = xpath.replace("replace_tool_bar", tool_bar)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click tool bar button -> %s" % (tool_bar), "result": "1"})

        ##Select a channel
        selector = Util.GetXpath({"locate": "select_channel"})
        option = Util.GetXpath({"locate":"channel_dropdown_list"})
        option = option.replace('replace_channel', channel)
        AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "channel", "selector": selector, "option": option, "result": "1"})

        ##Select a sub channel
        if subchannel:
            selector = Util.GetXpath({"locate": "select_subchannel"})
            option = Util.GetXpath({"locate":"subchannel_dropdown_list"})
            option = option.replace('replace_subchannel', subchannel)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "channel", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotiManualFlowBuilderPage.InputToolBar')


class AdminNotiVoicePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeneralInfo(arg):
        '''
        InputGeneralInfo : Input general info
                Input argu :
                    message_name - message name
                    category - category
                    subcategory - subcategory
                    task_priority - task_priority
                    owner - owner
                    description - description
                    region - region
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        message_name = arg["message_name"]
        category = arg["category"]
        subcategory = arg["subcategory"]
        task_priority = arg["task_priority"]
        owner = arg["owner"]
        description = arg["description"]
        region = arg["region"]

        ret = 1

        ##Input message name
        if message_name:
            xpath = Util.GetXpath({"locate": "message_name"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": message_name + str(GlobalAdapter.TrackerVar._CurUnixTime_), "result": "1"})

        ##Select category
        if category:
            ##Click and input category value
            AdminNotificationPage.InputCategory({"page": "voice", "page_type": "create", "action": "select", "category": category, "result": "1"})

        ##Select subcategory
        if subcategory:
            ##Click and input subcategory value
            AdminNotificationPage.InputSubCategory({"page": "voice", "page_type": "create", "action": "select", "subcategory": subcategory, "result": "1"})

        ##Select task priority
        if task_priority:
            ##Click selector
            AdminNotificationPage.InputTaskPriority({"page": "voice", "page_type": "create", "task_priority": task_priority, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "voice", "page_type": "create", "action": "input", "owner": owner, "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Select region
        if region:
            ##Click region drop down menu option
            AdminNotificationPage.InputRegion({"page": "voice", "page_type": "create", "action": "input", "region": region, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotiVoicePage.InputGeneralInfo')


class AdminNotificationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSelectorExpandAndInput(arg):
        '''
        CheckSelectorExpandAndInput : Check selector is expand or not, if not will click it and input value
                Input argu :
                    selector - selector xpath
                    selector_expand - selector xpath
                    input_field - input field xpath
                    input_value - input string
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        selector_name = arg["selector_name"]
        selector = arg["selector"]
        input_field = arg["input_field"]
        input_value = arg["input_value"]

        ret = -1

        for retry_time in range(3):
            ##Click selector
            BaseUICore.Click({"method": "xpath", "locate": selector, "message": "Click selector -> " + selector_name, "result": "1"})

            ##Wait for API loading
            if "email_template" in selector_name:
                time.sleep(2)

            ##Check selector is expand or not
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                ##Input value
                BaseUICore.Input({"method": "xpath", "locate": input_field, "string": input_value, "result": "1"})

                ret = 1
                break

            ##If not and need to click selector again
            else:
                dumplogger.info('Selector collaspe and need rerun function step!')

        OK(ret, int(arg['result']), 'AdminNotificationPage.CheckSelectorExpandAndInput')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSelectorExpandAndClick(arg):
        '''
        CheckSelectorExpandAndClick : Check selector is expand or not, if not will click it and select option
                Input argu :
                    selector_name - selector's name
                    selector - selector xpath
                    option - select's option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        selector_name = arg["selector_name"]
        selector = arg["selector"]
        option = arg["option"]
        ret = 1

        ##Click selector
        BaseUICore.Click({"method": "xpath", "locate": selector, "message": "Click selector -> " + selector_name, "result": "1"})
        time.sleep(2)

        ##Click value
        BaseUICore.Click({"method": "javascript", "locate": option, "message": "Click select option -> " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.CheckSelectorExpandAndClick')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCsv(arg):
        '''
        UploadCsv : Upload csv file
                Input argu :
                    file_name - file_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        ret = 1

        ##Click upload button
        xpath = Util.GetXpath({"locate":"upload_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

        ##Upload file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNotificationPage.UploadCsv')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadAttachmentFile(arg):
        '''
        UploadAttachmentFile : Upload attachment file
                Input argu :
                    file_name - file_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        ret = 1

        ##Click upload button
        xpath = Util.GetXpath({"locate":"upload_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})

        ##Upload file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "zip", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNotificationPage.UploadAttachmentFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskId(arg):
        '''
        InputTaskId : Input task id
                Input argu :
                    page - in_app / pn_ar / manual_queue / crm_queue / sms / manual_sms / manual_whatsapp / manual_line / email
                    task_id - task id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        task_id = arg["task_id"]
        ret = 1

        ##Input task id
        xpath = Util.GetXpath({"locate": page})
        if task_id:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": task_id, "result": "1"})
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputTaskId -> ' + task_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskName(arg):
        '''
        InputTaskName : Input task name(include task name, notification name and message name)
                Input argu :
                    page - pnar / inapp / manual_queue / crm_queue / sms / manual_sms / manual_whatsapp / manual_line / email
                    task_name - task name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        task_name = arg["task_name"]
        ret = 1

        ##Input task name
        xpath = Util.GetXpath({"locate": page})
        if task_name:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": task_name, "result": "1"})
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputTaskName -> ' + task_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOwner(arg):
        '''
        InputOwner : Input owner
                Input argu :
                    page - inapp / pnar / sms / manual_sms / manual_whatsapp / email
                    action - select / input
                    owner - owner
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        owner = arg["owner"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_owner_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "owner", "selector": selector, "input_field": input_field, "input_value": owner, "result": "1"})

            ##Get selector、input field and option xpath to select option
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_owner', owner)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "owner", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "owner", "selector": selector, "input_field": input_field, "input_value": owner, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputOwner -> ' + owner)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRegion(arg):
        '''
        InputRegion : Input region
                Input argu :
                    page - inapp / pnar / sms / manual_sms / manual_whatsapp / email
                    action - select / input
                    region - region
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        region = arg["region"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_region_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "region", "selector": selector, "input_field": input_field, "input_value": region, "result": "1"})

            ##Select region
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_region', region)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "region", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "region", "selector": selector, "input_field": input_field, "input_value": region, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputRegion -> ' + region)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNotificationFolder(arg):
        '''
        InputNotificationFolder : Input notification folder(AR folder)
                Input argu :
                    page - pnar / sms / manual_sms / manual_whatsapp / email
                    action - select / input
                    notification_folder - notification_folder
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        notification_folder = arg["notification_folder"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_notification_folder_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "notification folder", "selector": selector, "input_field": input_field, "input_value": notification_folder, "result": "1"})

            ##Select notification_folder
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_notification_folder', notification_folder)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "notification_folder", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "notification folder", "selector": selector, "input_field": input_field, "input_value": notification_folder, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputNotificationFolder -> ' + notification_folder)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStatus(arg):
        '''
        InputStatus : Input status
                Input argu :
                    page - manual_queue / crm_queue / sms / manual_sms / manual_whatsapp / email
                    action - select / input
                    status - status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        status = arg["status"]
        ret = 1

        ##Click and input status value
        selector = Util.GetXpath({"locate": page + "_status_selector"})
        input_field = Util.GetXpath({"locate": page + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "status", "selector": selector, "input_field": input_field, "input_value": status, "result": "1"})

            ##Select status
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_status', status)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "status", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "status", "selector": selector, "input_field": input_field, "input_value": status, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputStatus -> ' + status)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCategory(arg):
        '''
        InputCategory : Input category
                Input argu :
                    page - sms / manual_sms / manual_whatsapp / email / voice
                    action - select / input
                    category - category
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        category = arg["category"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_category_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "category", "selector": selector, "input_field": input_field, "input_value": category, "result": "1"})

            ##Select category
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_category', category)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "category", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "category", "selector": selector, "input_field": input_field, "input_value": category, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputCategory -> ' + category)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSubCategory(arg):
        '''
        InputSubCategory : Input sub category
                Input argu :
                    page - voice
                    action - select / input
                    subcategory - subcategory
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        subcategory = arg["subcategory"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_subcategory_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "subcategory", "selector": selector, "input_field": input_field, "input_value": subcategory, "result": "1"})

            ##Select subcategory
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_subcategory', subcategory)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "subcategory", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "subcategory", "selector": selector, "input_field": input_field, "input_value": subcategory, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputSubCategory -> ' + subcategory)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriority(arg):
        '''
        InputPriority : Input priority(only for market push channel)
                Input argu :
                    page - manual_queue
                    action - select / input
                    priority - priority
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        priority = arg["priority"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_priority_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "priority", "selector": selector, "input_field": input_field, "input_value": priority, "result": "1"})

            ##Select priority
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_priority', priority)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "priority", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "priority", "selector": selector, "input_field": input_field, "input_value": priority, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputPriority -> ' + priority)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskPriority(arg):
        '''
        InputTaskPriority : Input task priority(for MGK channel)
                Input argu :
                    page - sms / manual_sms / manual_whatsapp
                    task_priority - High priority queue / Normal queue
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        task_priority = arg["task_priority"]
        page_type = arg["page_type"]
        ret = 1

        ##Select task priority option
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_task_priority_selector"})
        option = Util.GetXpath({"locate": "select"})
        option = option.replace("replace_task_priority", task_priority)

        ##Click selector
        AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "task_priority", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputTaskPriority -> ' + task_priority)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTaskTags(arg):
        '''
        InputTaskTags : Input task tags
                Input argu :
                    page - manual_queue
                    action - select / input
                    tags - tags
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        tags = arg["tags"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_tags_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "tags", "selector": selector, "input_field": input_field, "input_value": tags, "result": "1"})

            ##Select tags
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_tags', tags)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "tags", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(3):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "tags", "selector": selector, "input_field": input_field, "input_value": tags, "result": "1"})
                time.sleep(2)

                ##Press keyboard down to select option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "down", "locate": input_field, "result": "1"})
                    time.sleep(1)

                    ##Press keyboard enter to select option
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                        break

                    else:
                        dumplogger.info('Selector collaspe and need rerun function step!')

                else:
                    dumplogger.info('Selector collaspe and need rerun function step!')

        ##Click other place
        xpath = Util.GetXpath({"locate": "other_palce"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click other place", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputTaskTags -> ' + tags)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTitleContent(arg):
        '''
        InputTitleContent : Input title content(for Market Push channel)
                Input argu :
                    page - manual_queue
                    channel_type - channel type
                    input_content - content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        channel_type = arg["channel_type"]
        input_content = arg["input_content"]
        ret = 1

        ##Select channel type option
        selector = Util.GetXpath({"locate": page + "_title_content_selector"})
        option = Util.GetXpath({"locate": "select"})
        option = option.replace("replace_channel_type", channel_type)

        ##Click selector
        AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "channel_type", "selector": selector, "option": option, "result": "1"})

        if input_content:
            ##Input value
            xpath = Util.GetXpath({"locate": page + "_input_content"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputTitleContent -> %s' % (channel_type))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputApprover(arg):
        '''
        InputApprover : Input approver(email)
                Input argu :
                    page - sms / manual_sms / manual_whatsapp
                    action - select / input
                    approver - approver
                    page_type - create / menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        action = arg["action"]
        approver = arg["approver"]
        page_type = arg["page_type"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_approver_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "approver", "selector": selector, "input_field": input_field, "input_value": approver, "result": "1"})

            ##Select approver
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_approver', approver)
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": option, "passok": "0", "result": "1"}):
                AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "approver", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(3):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "approver", "selector": selector, "input_field": input_field, "input_value": approver, "result": "1"})
                time.sleep(2)

                ##Press keyboard down to select option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "down", "locate": input_field, "result": "1"})
                    time.sleep(1)

                    ##Press keyboard enter to select option
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                        break

                    else:
                        dumplogger.info('Selector collaspe and need rerun function step!')

                else:
                    dumplogger.info('Selector collaspe and need rerun function step!')

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputApprover -> ' + approver)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOperator(arg):
        '''
        InputOperator : Input operator
                Input argu :
                    page - whatsapp_template / email_template
                    operator - operator
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        operator = arg["operator"]
        ret = 1

        ##Input operator
        xpath = Util.GetXpath({"locate": page})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": operator, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputOperator -> ' + operator)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLanguage(arg):
        '''
        InputLanguage : Input language
                Input argu :
                    page - whatsapp_template
                    page_type - create / menu
                    action - select / input
                    language - language
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page = arg["page"]
        page_type = arg["page_type"]
        action = arg["action"]
        language = arg["language"]
        ret = 1

        ##Take selector and input field xpath to CheckSelectorExpandAndInput func
        selector = Util.GetXpath({"locate": page + "_" + page_type + "_selector"})
        input_field = Util.GetXpath({"locate": page + "_" + page_type + "_input"})

        if action == 'select':
            AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "language", "selector": selector, "input_field": input_field, "input_value": language, "result": "1"})

            ##Select category
            option = Util.GetXpath({"locate": action})
            option = option.replace('replace_language', language)
            AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "language", "selector": selector, "option": option, "result": "1"})

        elif action == 'input':
            ##Retry the complete operation when send keyboard scenario occur accident
            for retry_time in range(2):
                AdminNotificationPage.CheckSelectorExpandAndInput({"selector_name": "language", "selector": selector, "input_field": input_field, "input_value": language, "result": "1"})

                ##Press keyboard enter to commit option
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": input_field, "passok": "0", "result": "1"}):
                    BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": input_field, "result": "1"})
                    break

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputLanguage -> ' + language)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSMSContent(arg):
        '''
        InputSMSContent : Input sms content
                Input argu :
                    sms_content - sms content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        sms_content = arg["sms_content"]

        ret = 1

        ##Input message name
        xpath = Util.GetXpath({"locate": "sms_content"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": sms_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputSMSContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoiceContent(arg):
        '''
        InputVoiceContent : Input voice content
                Input argu :
                    voice_content - voice content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        voice_content = arg["voice_content"]

        ret = 1

        ##Input voice content
        xpath = Util.GetXpath({"locate": "voice_content"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voice_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputVoiceContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGoogleSheetURL(arg):
        '''
        InputGoogleSheetURL : Input google sheet link with recipient details
                Input argu :
                    google_sheet_url - google sheet url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        google_sheet_url = arg["google_sheet_url"]

        ret = 1

        ##Input google sheet url
        xpath = Util.GetXpath({"locate": "google_sheet_url"})

        if google_sheet_url:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": google_sheet_url, "result": "1"})
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.InputGoogleSheetURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateTaskFromJson(arg):
        '''
        CreateTaskFromJson : Create task from json
                Input argu :
                    file_name - file name
                    owner - owner
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        owner = arg["owner"]
        ret = 1

        ##Setting path for all system
        slash = Config.dict_systemslash[Config._platform_]

        ##Get file path
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + "Notification" + slash + "file" + slash + file_name + '.' + "json"

        ##Get file contant json data
        file = open(file_path, 'r')
        data = json.load(file)
        input_text = json.dumps(data)
        input_text = input_text.replace('replace_task_name', str(GlobalAdapter.TrackerVar._CurUnixTime_))

        ##Input json data
        xpath = Util.GetXpath({"locate": "json_text"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_text, "result": "1"})

        ##Select owner
        if owner:
            ##Click owner drop down menu option
            AdminNotificationPage.InputOwner({"page": "inapp", "page_type": "json", "action": "input", "owner": owner, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationPage.CreateTaskFromJson')


class AdminNotificationCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnLeftPanel(arg):
        '''
        ClickTabOnLeftPanel : Click a tab at the left panel
                Input argu :
                    type - ringtone_management / in_app / pn_ar / manual_queue / invisible_push / crm_queue /
                           sms / manual_sms / whatsapp / manual_whatsapp / email / manual_line / noti_flow_builder
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickTabOnLeftPanel -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTaskMenuAction(arg):
        '''
        ClickTaskMenuAction : Click task menu button, need to set task id
                Input argu :
                    page_action - "page" combine with edit / disable / enable / view_stats / view / send
                    task_id - task id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_action = arg["page_action"]
        task_id = arg["task_id"]
        ret = 1

        ##Click action button
        xpath = Util.GetXpath({"locate": page_action})
        xpath = xpath.replace('replace_task_id', task_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickTaskMenuAction -> ' + page_action)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInTaskAction(arg):
        '''
        ClickInTaskAction : Click button which in task page
                Input argu :
                    page_action - "page" combine with next / cancel / previous / back / close_icon / submit / confirm / send
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_action = arg["page_action"]
        ret = 1

        xpath = Util.GetXpath({"locate": page_action})

        ##Click close  icon button
        if "close_icon" in page_action:
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        else:
            ##Click next_btn / cancel_btn / previous_btn / back_btn
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + page_action + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickInTaskAction -> ' + page_action)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreate(arg):
        '''
        ClickCreate : Click create button in menu page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create button
        xpath = Util.GetXpath({"locate": "create_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click create button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickCreate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button in menu page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search button
        xpath = Util.GetXpath({"locate": 'search_button'})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset button in menu page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset button
        xpath = Util.GetXpath({"locate": 'reset_button'})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackArrow(arg):
        '''
        ClickBackArrow : Click back arrow button at upper left side
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back arrow button
        xpath = Util.GetXpath({"locate": "back_arrow"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click back arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickBackArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickImport(arg):
        '''
        ClickImport : Click import button in menu page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click import button
        xpath = Util.GetXpath({"locate": 'import_button'})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click import button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickImport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseIcon(arg):
        '''
        ClickCloseIcon : Click close icon(for input field)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close icon
        xpath = Util.GetXpath({"locate": "close_icon"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickCloseIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTaskCheckBox(arg):
        '''
        ClickTaskCheckBox : Click task checkbox in menu page
                Input argu :
                    task_name - task name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        task_name = arg['task_name']
        ret = 1

        ##Click task checkbox
        xpath = Util.GetXpath({"locate": 'task_checkbox'})
        xpath = xpath.replace('replace_task_name', task_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click task checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickTaskCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowOnlySelect(arg):
        '''
        ClickShowOnlySelect : Click "show only selected tasks" checkbox(after choose a task this checkbox will display)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click "show only selected tasks" checkbox
        xpath = Util.GetXpath({"locate": 'only_selected_tasks'})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show only selected tasks checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickShowOnlySelect')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRunValidation(arg):
        '''
        ClickRunValidation : Click run validation button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click run validation button
        xpath = Util.GetXpath({"locate": "run_validation_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click run validation button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickRunValidation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendTime(arg):
        '''
        ClickSendTime : Click send time checkbox
                Input argu :
                    action - send_now / schedule
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action = arg["action"]
        ret = 1

        ##Click checkbox
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + action + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickSendTime -> ' + action)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendType(arg):
        '''
        ClickSendType : Click send type
                Input argu :
                    send_type - send type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        send_type = arg["send_type"]
        ret = 1

        ##Click send type
        selector = Util.GetXpath({"locate": "select_send_type"})
        option = Util.GetXpath({"locate": "send_type_dropdown_list"})
        option = option.replace("replace_send_type", send_type)
        AdminNotificationPage.CheckSelectorExpandAndClick({"selector_name": "send_type", "selector": selector, "option": option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickSendType -> ' + send_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStepIcon(arg):
        '''
        ClickStepIcon : Click step button (for Market Push page)
                Input argu :
                    step - 1 / 2 / 3 / 4 /5
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        step = arg["step"]
        ret = 1

        ##Click step button
        xpath = Util.GetXpath({"locate": "step_icon"})
        xpath = xpath.replace('replace_step', step)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click step button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNotificationCommonButton.ClickStepIcon -> ' + step)
