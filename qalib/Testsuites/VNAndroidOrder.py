import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *

class VNAndroidOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidOrder Precondition =="
        dumplogger.info("== Setup VNAndroidOrder Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"vn"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_VNAndroidOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-01.xml")

    def test_VNAndroidOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-02.xml")

    def test_VNAndroidOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-03.xml")

    def test_VNAndroidOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-04.xml")

    def test_VNAndroidOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-05.xml")

    def test_VNAndroidOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-06.xml")

    def test_VNAndroidOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-07.xml")

    def test_VNAndroidOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-08.xml")

    def test_VNAndroidOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidOrder-09.xml")

    # def test_VNAndroidOrder10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNAndroidOrder-10.xml")

    # def test_VNAndroidOrder11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNAndroidOrder-11.xml")

    # def test_VNAndroidOrder12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNAndroidOrder-12.xml")

    # def test_VNAndroidOrder13(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNAndroidOrder-13.xml")

    # def test_VNAndroidOrder14(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNAndroidOrder-14.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidOrder Post condition =="
        dumplogger.info("== Setup VNAndroidOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
