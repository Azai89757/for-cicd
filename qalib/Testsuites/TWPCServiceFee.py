import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCServiceFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCServiceFee Precondition =="
        dumplogger.info("== Setup TWPCServiceFee Precondition ==")

    def test_TWPCServiceFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCServiceFee-01.xml")

    def test_TWPCServiceFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCServiceFee-02.xml")

    def test_TWPCServiceFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCServiceFee-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCServiceFee Post condition =="
        dumplogger.info("== Setup TWPCServiceFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
