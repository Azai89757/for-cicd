import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class VNPCShopeeMart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopeeMart Precondition =="
        dumplogger.info("== Setup VNPCShopeeMart Precondition ==")

    def test_VNPCShopeeMart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeMart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopeeMart Post condition =="
        dumplogger.info("== Setup VNPCShopeeMart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
