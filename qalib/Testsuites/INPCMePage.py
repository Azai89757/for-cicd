import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCMePage Precondition =="
        dumplogger.info("== Setup INPCMePage Precondition ==")

    def test_INPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCMePage Post condition =="
        dumplogger.info("== Setup INPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
