import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCCoins Precondition =="
        dumplogger.info("== Setup TWPCCoins Precondition ==")

    def test_TWPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-01.xml")

    def test_TWPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-02.xml")

    def test_TWPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-03.xml")

    def test_TWPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-04.xml")

    def test_TWPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-05.xml")

    def test_TWPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-06.xml")

    def test_TWPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-07.xml")

    def test_TWPCCoins08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-08.xml")

    def test_TWPCCoins09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCoins-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCCoins Post condition =="
        dumplogger.info("== Setup TWPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
