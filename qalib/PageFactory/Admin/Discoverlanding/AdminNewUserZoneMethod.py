#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminNewUserZoneMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import os
import re
import time
import datetime

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import GlobalAdapter

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminNewUserZonePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchPlatform(arg):
        '''
        SwitchPlatform : Switch platform (mobile / lite / rw / pc)
            Input argu :
                platform - mobile / lite / rw / pc
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        platform = arg["platform"]

        xpath = Util.GetXpath({"locate": platform})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Switch platform", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.SwitchPlatform -> ' + platform)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyNewUserZoneComponent(arg):
        '''
        ModifyNewUserZoneComponent : Modify new user zone component content
                Input argu :
                    component_type - component type (top_visual / welcome_items / voucher)
                    title - title
                    visibility_type - visible / invisible
                    start_time - start time
                    end_time - end_time
                    image_name - image name
                    image_type - image type
                    rule_set - rule set
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        title = arg["title"]
        visibility_type = arg["visibility_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]
        rule_set = arg["rule_set"]

        ##Input title
        if title:
            xpath = Util.GetXpath({"locate":"title_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":title, "result": "1"})

        ##Click visibility type dropdown
        if visibility_type:
            xpath = Util.GetXpath({"locate": "visibility_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visibility type dropdown", "result": "1"})
            time.sleep(1)

            ##Click choose visibility type
            xpath = Util.GetXpath({"locate": visibility_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility type -> " + visibility_type, "result": "1"})
            time.sleep(5)

        ##Adjust start time and end time
        ##Input start time
        if start_time:
            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        ##Input end time
        if end_time:
            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0)

            ##Modify end date
            xpath = Util.GetXpath({"locate": "end_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        ##Input rule set
        if rule_set:
            xpath = Util.GetXpath({"locate": "rule_set_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set dropdown", "result": "1"})
            time.sleep(1)

            ##Input rule set name
            xpath = Util.GetXpath({"locate": "rule_set_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rule_set, "result": "1"})
            time.sleep(1)

            ##Click choose visibility type
            xpath = Util.GetXpath({"locate": "rule_set_name"})
            xpath = xpath.replace("name_to_be_replaced", rule_set)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose rule set -> " + rule_set, "result": "1"})
            time.sleep(5)

        ##Input image
        if image_name and image_type:
            xpath = Util.GetXpath({"locate": component_type})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": image_name, "file_type": image_type, "result": "1"})
            time.sleep(10)

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.ModifyNewUserZoneComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUrl(arg):
        '''
        InputUrl : Input url (For top_visual / banner)
                Input argu :
                    url - redirection url (top_visual / banner)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        url = arg["url"]

        ##Input url
        xpath = Util.GetXpath({"locate":"url_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.InputUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductCaption(arg):
        '''
        InputProductCaption : Input product caption (For welcome_items)
                Input argu :
                    product_caption_en - product caption in English
                    product_caption_zh - product caption in Simplified Chinese
                    product_caption_others - product caption in language other than English and Simplified Chinese
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        product_caption_en = arg["product_caption_en"]
        product_caption_zh = arg["product_caption_zh"]
        product_caption_others = arg["product_caption_others"]

        ##Input product caption
        if product_caption_en:
            xpath = Util.GetXpath({"locate":"product_caption_en_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_caption_en, "result": "1"})

        if product_caption_zh:
            xpath = Util.GetXpath({"locate":"product_caption_zh_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_caption_zh, "result": "1"})

        if product_caption_others:
            xpath = Util.GetXpath({"locate":"product_caption_others_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_caption_others, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.InputProductCaption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderText(arg):
        '''
        InputHeaderText : Input header text (banner / welcome_items)
                Input argu :
                    header_text_en - header text in English (if no parameter input, then no bar input)
                    header_text_zh - header text in Simplified Chinese (if no parameter input, then no bar input)
                    header_text_others - header text in language other than English and Simplified Chinese (if no parameter input, then no bar input)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        header_text_en = arg["header_text_en"]
        header_text_zh = arg["header_text_zh"]
        header_text_others = arg["header_text_others"]

        ##Input header text
        if header_text_en:
            xpath = Util.GetXpath({"locate":"text_en_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": header_text_en, "result": "1"})

        if header_text_zh:
            xpath = Util.GetXpath({"locate":"text_zh_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": header_text_zh, "result": "1"})

        if header_text_others:
            xpath = Util.GetXpath({"locate":"text_others_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": header_text_others, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.InputHeaderText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeHeaderTextColor(arg):
        '''
        ChangeHeaderTextColor : Change header text color
                Input argu :
                    color_hex - color hex
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        color_hex = arg["color_hex"]

        ##Input color hex
        xpath = Util.GetXpath({"locate":"header_text_color_hex_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":color_hex, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.ChangeHeaderTextColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeSectionBackgroundColor(arg):
        '''
        ChangeSectionBackgroundColor : Change section background color
                Input argu :
                    color_hex - color hex
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        color_hex = arg["color_hex"]

        ##Input color hex
        xpath = Util.GetXpath({"locate":"background_color_hex_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":color_hex, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.ChangeSectionBackgroundColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeProductCaptionColor(arg):
        '''
        ChangeProductCaptionColor : Change product caption color
                Input argu :
                    color_hex - color hex
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        color_hex = arg["color_hex"]

        ##Input color hex
        xpath = Util.GetXpath({"locate":"pc_color_hex_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":color_hex, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.ChangeProductCaptionColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBackgroundImage(arg):
        '''
        UploadBackgroundImage : Upload background image (voucher)
                Input argu :
                    background_name - background image name (voucher)
                    background_type - background image type (voucher)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        background_name = arg["background_name"]
        background_type = arg["background_type"]

        ##Upload background image
        xpath = Util.GetXpath({"locate": "background_image_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": background_name, "file_type": background_type, "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.UploadBackgroundImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteNewUserZoneComponent(arg):
        '''
        DeleteNewUserZoneComponent : Delete new user zone component
                Input argu :
                    component_type - component type (top_visual / welcome_items / voucher)
                    component_name - component name
                    action - delete / cancel
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        component_name = arg["component_name"]
        action = arg["action"]

        ##Delete nuz component
        xpath = Util.GetXpath({"locate": component_type})
        xpath = xpath.replace("name_to_be_replaced", component_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
            time.sleep(1)

            ##Choose post action
            xpath = Util.GetXpath({"locate": action})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose post action -> " + action, "result": "1"})
            time.sleep(10)

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.DeleteNewUserZoneComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditNewUserZoneComponent(arg):
        '''
        EditNewUserZoneComponent : Edit new user zone component
                Input argu :
                    component_type - component type (top_visual / welcome_items / voucher)
                    component_name - component name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        component_name = arg["component_name"]

        ##Edit nuz component
        xpath = Util.GetXpath({"locate": component_type})
        xpath = xpath.replace("name_to_be_replaced", component_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.EditNewUserZoneComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBENewUserComponentExist(arg):
        '''
        CheckBENewUserComponentExist : Edit new user zone component
                Input argu :
                    component_type - component type (top_visual / welcome_items / voucher)
                    component_name - component name
                    visibility_type - visibility type shown in BE
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        component_name = arg["component_name"]
        visibility_type = arg["visibility_type"]

        xpath = Util.GetXpath({"locate": component_type + "_result"}).replace("name_to_be_replaced", component_name).replace("visibility_to_be_replaced", visibility_type)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePage.CheckBENewUserComponentExist')

class AdminNewUserZonePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewUserZoneComponent(arg):
        '''
        ClickAddNewUserZoneComponent : Click add new user zone component
            Input argu :
                component_type - component type (top_visual / voucher / welcome_items / new_user_banner)
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]

        xpath = Util.GetXpath({"locate": component_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new component", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePageButton.ClickAddNewUserZoneComponent -> ' + component_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditComponent(arg):
        '''
        ClickEditComponent : Click edit component button
                Input argu :
                    component_type - component type (top_visual / welcome_items / voucher)
                    component_name - component name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]
        component_name = arg["component_name"]

        ##Edit nuz component
        xpath = Util.GetXpath({"locate": component_type})
        xpath = xpath.replace("name_to_be_replaced", component_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePageButton.ClickEditComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveComponent(arg):
        '''
        ClickSaveComponent : Click save component button
                Input argu :
                    N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewUserZonePageButton.ClickSaveComponent')
