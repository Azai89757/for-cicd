import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRAPIRestoreFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRAPIRestoreFlashSale Precondition =="
        dumplogger.info("== Setup FRAPIRestoreFlashSale Precondition ==")

    def test_FRAPIRestoreFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/FRAPIRestoreFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRAPIRestoreFlashSale Post condition =="
        dumplogger.info("== Setup FRAPIRestoreFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
