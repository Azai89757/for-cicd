import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpCancellation Precondition =="
        dumplogger.info("== Setup TWHttpCancellation Precondition ==")

    def test_TWHttpCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCancellation-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpCancellation Post condition =="
        dumplogger.info("== Setup TWHttpCancellation Post condition ==")
