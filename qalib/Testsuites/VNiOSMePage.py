import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNiOSMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSMePage Precondition =="
        dumplogger.info("== Setup VNiOSMePage Precondition ==")

        ##Get device id
        Config._DeviceID_ = GetiOSDeviceId("iOS")
        print Config._DeviceID_

        #Get device info by device id
        if Config._DeviceID_:

            ##Start Appium
            iOSStartAppium({"result": "1"})

            ##Get iOS version of this device
            device_version = GetiOSDeviceInfo()

            ##Call function from iOSBaseUICore
            print "Device connection check: Pass"
            print "Get device version: " + device_version

            ##Assign device platform version & device udid to global var
            Config._iOSDesiredCaps_['udid'] = Config._DeviceID_

            ##Call function from iOSBaseUICore
            iOSInitialDriver()

        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_VNiOSMePage001(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSMePage-001.xml")

    def test_VNiOSMePage002(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSMePage-002.xml")

    def test_VNiOSMePage003(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSMePage-003.xml")

    def test_VNiOSMePage004(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSMePage-004.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSMePage Post condition =="
        dumplogger.info("== Setup VNiOSMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
