import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCCart Precondition =="
        dumplogger.info("== Setup INPCCart Precondition ==")

    def test_INPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCart-01.xml")

    def test_INPCCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCart-02.xml")

    def test_INPCCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCart-03.xml")

    def test_INPCCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCCart-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCCart Post condition =="
        dumplogger.info("== Setup INPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
