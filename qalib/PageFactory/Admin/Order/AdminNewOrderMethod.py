#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminNewOrderMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time
import re
import datetime

##Import framework common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import GlobalAdapter

##Import API related library
from api import APICommonMethod
from api.http import HttpAPICore
from api.http import SellerCenterAPIMethod

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##Import OMS library
from PageFactory.Admin.Order import AdminOMSMethod

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchOrderAdmin(arg):
    ''' LaunchOrderAdmin : Launch order admin
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1
    db_file = "order_admin_cookie"
    admin_cookie = {}

    ##Get BE url and go to BE
    url = "https://order-admin." + GlobalAdapter.UrlVar._Domain_

    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(10)

    ##If cookie is not stored for the first time, get cookie from db
    if 'order_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['order_admin'] = [admin_cookie]

    ##Go to DMS (dispute management system) admin
    BaseUICore.SetBrowserCookie({"storage_type":"order_admin", "result": "1"})
    time.sleep(3)
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchOrderAdmin')


class AdminNewOrderCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckValidDate(arg):
        '''
        CheckValidDate : Check valid date in all admin order page (Admin Portal → Orders)
                Input argu :
                    locate - locate element display date time you want to check it
                    string_format - string format of datetime (e.g. %Y-%m-%d)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg['locate']
        string_format = arg['string_format']

        ##Get current date (e.g. YYYY-MM-DD or DD-MM-YYYY)
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(string_format), string_format)

        ##Get locate element display date time
        BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Get date time from element text (e.g. YYYY-MM-DD or DD-MM-YYYY)
        match = re.match(r"(\d{4}-\d{2}-\d{2}|\d{2}-\d{2}-\d{4})", GlobalAdapter.CommonVar._PageAttributes_)

        ##Check regex match result
        if match:
            ##Get valid date (e.g. YYYY-MM-DD or DD-MM-YYYY)
            valid_date = datetime.datetime.strptime(match.group(0), string_format)
            ##Check valid date >= current date
            if valid_date >= current_date:
                dumplogger.info("Locate valid date is greater than current date!!")
            else:
                ret = 0
                dumplogger.error("Locate valid date is less than current date!!")
        else:
            ret = 0
            dumplogger.error("No any date time string (e.g. YYYY-MM-DD or DD-MM-YYYY) in locate element!!")

        OK(ret, int(arg['result']), 'AdminNewOrderCommon.CheckValidDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderID(arg):
        '''
        CheckOrderID : Check order ID in new order page
                Input argu :
                    page_type - ofg_page, rr_detail_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Check order ID in new order page
        xpath = Util.GetXpath({"locate": page_type})
        xpath_order_id = xpath.replace("order_id_to_be_replaced", GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_order_id, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderCommon.CheckOrderID')


class AdminNewOrderComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NavigateToPage(arg):
        '''
        NavigateToPage : Click tab in order admin page left panel
                Input argu :
                    page_type - main_orders, main_return, main_payments, orders, order_accounting, return_refund_request, payments, seller_transaction_fee, service_fee,
                                commission_fee, custom_tax, payment_channel_maintenance, whitelist_seller, blacklist_seller, escrow_release
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderComponent.NavigateToPage -> ' + page_type)


class AdminNewOrderPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchOnOrderListPage(arg):
        '''
        SearchOnOrderListPage : Search seller on order list page
                Input argu :
                    search_type - seller, buyer
                    username - use seller or buyer account to search for orders
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        username = arg["username"]

        ##Input seller name and search
        xpath = Util.GetXpath({"locate":search_type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":username, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderPage.SearchOnOrderListPage -> ' + username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToOrderDetail(arg):
        '''
        GoToOrderDetail : Go to order detail
                Input argu :
                    search_type - seller, buyer
                    username - use seller or buyer account to search for orders
                    row_number - the row number of the order which you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        search_type = arg["search_type"]
        username = arg["username"]
        row_number = arg["row_number"]

        ##Input username and search
        AdminNewOrderPage.SearchOnOrderListPage({"search_type":search_type, "username":username, "result": "1"})

        ##Check order search result first row is correct
        xpath = Util.GetXpath({"locate":"order_search_result_first_row"})
        xpath = xpath.replace("username_to_be_replaced", username)
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Click order id link
        xpath = Util.GetXpath({"locate":"order_search_result_order_id"})
        xpath = xpath.replace("row_number_to_be_replaced", row_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click order id link", "result": "1"})

        for retry_times in range(3):
            ##Wait for page loading
            time.sleep(5)

            ##Get order status in admin order detail page
            xpath = Util.GetXpath({"locate":"order_status"})
            BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

            ##Check if order detail page loaded successfully
            if GlobalAdapter.CommonVar._PageAttributes_:
                dumplogger.info("order detail page loaded!")
                ret = 1
                break
            else:
                dumplogger.info("loading error!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GoToOrderDetail -> ' + username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderID(arg):
        '''
        GetOrderID : Get Order ID in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for retry_times in range(3):
            ##Get order id in admin order detail page
            xpath = Util.GetXpath({"locate":"order_id_display_text"})
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"order_id", "result": "1"})

            time.sleep(3)

            ##Check if get order id successfully
            if GlobalAdapter.OrderE2EVar._OrderID_:
                dumplogger.info("got order id!")
                ret = 1
                break
            else:
                dumplogger.info("text display error!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GetOrderID -> ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetReturnID(arg):
        '''
        GetReturnID : Get Return ID in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for retry_times in range(3):
            ##Get return id in admin order detail page
            xpath = Util.GetXpath({"locate":"return_id_display_text"})
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"return_id", "result": "1"})

            time.sleep(3)

            ##Check if get return id successfully
            if GlobalAdapter.OrderE2EVar._ReturnID_:
                dumplogger.info("got return id!")
                ret = 1
                break
            else:
                dumplogger.info("text display error!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GetReturnID -> ' + GlobalAdapter.OrderE2EVar._ReturnID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetCheckoutID(arg):
        '''
        GetCheckoutID : Get Checkout ID in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for retry_times in range(3):
            ##Get checkout id in admin order detail page
            xpath = Util.GetXpath({"locate":"checkout_id_display_text"})
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"checkout_id", "result": "1"})

            time.sleep(3)

            ##Check if get checkout id successfully
            if GlobalAdapter.OrderE2EVar._CheckOutID_:
                dumplogger.info("got checkout id!")
                ret = 1
                break
            else:
                dumplogger.info("text display error!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GetCheckoutID -> ' + GlobalAdapter.OrderE2EVar._CheckOutID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOFGID(arg):
        '''
        GetOFGID : Get OFG ID in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check if OFG id reveal
        for retry_times in range(3):
            ##Check if forder id reveal
            xpath = Util.GetXpath({"locate":"ofg_id"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                break
            else:
                ##Refresh browser and retry
                BaseUILogic.BrowserRefresh({"message":"Refresh browser to retry", "result": "1"})
                time.sleep(3)

                ##Click the section of 'status_timeline'
                AdminNewOrderButton.ClickMiddleSectionTab({"tab_type":"status_timeline", "result": "1"})

                ##Move to 'Audit Log' Section. Let the 3PL status history field show in screenshot when case failed.
                xpath = Util.GetXpath({"locate":"audit_log"})
                BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        ##Get OFG id
        xpath = Util.GetXpath({"locate":"ofg_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"ofg_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GetOFGID -> ' + GlobalAdapter.OrderE2EVar._OFGID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSellerTxnFeeRuleId(arg):
        '''
        CheckSellerTxnFeeRuleId : Check seller transaction rule id in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check seller transaction rule id in admin order detail page
        xpath = Util.GetXpath({"locate":"seller_txn_rule_id"})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._TransactionFeeCSRuleID_, "isfuzzy": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.CheckSellerTxnFeeRuleId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderSN(arg):
        '''
        GetOrderSN : Get Order SN in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for retry_times in range(3):
            ##Get order sn
            xpath = Util.GetXpath({"locate":"order_sn"})
            BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"ordersn", "result": "1"})

            time.sleep(3)

            ##Check if get order sn successfully
            if "ordersn" in GlobalAdapter.OrderE2EVar._OrderSNDict_:
                dumplogger.info("got order sn!")
                ret = 1
                break
            else:
                dumplogger.info("text display error!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GetOrderSN -> %s' % (GlobalAdapter.OrderE2EVar._OrderSNDict_))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLogisticStatus(arg):
        '''
        CheckLogisticStatus : Check order logistic status in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check logistic status is request created
        xpath = Util.GetXpath({"locate":"logistic_status"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Logistic status is request created.")
        else:
            dumplogger.info("Logistic status is not request created.")
            dumplogger.info("Sending repush API")

            ##Repush arrange shipment by API
            HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "repush_arrange_shipment", "result": "1"})
            SellerCenterAPIMethod.SellerCenterOMSAPI.AssignDataForRepushArrangeShipment({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method":"get", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})

            ##Refresh browser to check logistic status
            BaseUILogic.BrowserRefresh({"message": "Refresh page", "result": "1"})
            time.sleep(10)

            ##Recheck logistic status
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Logistic status is request created.")
            else:
                ##Go to ISC tool to take a screenshot of order status
                dumplogger.info("Logistic status is not request created after repushing order id.")
                AdminOMSMethod.AdminOMSPushStatus.OMSCheckArrangeShipmentStatus({"result": "1"})
                ret = 0

        OK(ret, int(arg['result']), 'AdminNewOrderPage.CheckLogisticStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPromotionId(arg):
        '''
        CheckPromotionId : Check promotion id in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check promotion id exist in admin order detail page
        xpath = Util.GetXpath({"locate":"promotion_id_text"})
        xpath_promotion_id = xpath.replace("promotion_id_to_be_replace", GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[0]["id"])
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_promotion_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderPage.CheckPromotionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetFOrderID(arg):
        '''
        GetFOrderID : Get FOrder ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check if forder id reveal
        for retry_times in range(3):
            ##Click the section of 'status_timeline'
            AdminNewOrderButton.ClickMiddleSectionTab({"tab_type":"status_timeline", "result": "1"})

            ##Move to 'Audit Log' Section. Let the 3PL status history field show in screenshot when case failed.
            xpath = Util.GetXpath({"locate":"audit_log"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

            ##Check if forder id reveal
            xpath = Util.GetXpath({"locate":"forder_id"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                break
            else:
                ##Refresh browser and retry
                BaseUILogic.BrowserRefresh({"message":"Refresh browser to retry", "result": "1"})
                time.sleep(3)

        ##Get forder id
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"forder_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderPage.GetFOrderID-> ' + GlobalAdapter.OrderE2EVar._FOrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PayoutByBankTransfer(arg):
        '''
        PayoutByBankTransfer : Click Pay btn in Checkout detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for retry_times in range(3):
            ##Click pay btn in checkout detail page
            xpath = Util.GetXpath({"locate":"pay_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click pay btn", "result": "1"})

            time.sleep(3)

            ##Check had pay sucessfully in payment log
            xpath = Util.GetXpath({"locate":"success_pay_text"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                dumplogger.info("success pay!")
                ret = 1
                break
            else:
                dumplogger.info("text display error!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.PayoutByBankTransfer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckEscrowCalculation(arg):
        '''
        CheckEscrowCalculation : Check the sum of each field is equal to escrow amount
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get sign of each field
        xpath = Util.GetXpath({"locate":"signs"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "get_attribute","attribute": "data-icon", "mode": "multi", "result": "1"})
        signs = GlobalAdapter.CommonVar._PageAttributesList_

        ##Get values of each field
        xpath = Util.GetXpath({"locate":"values"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "multi", "result": "1"})
        values = GlobalAdapter.CommonVar._PageAttributesList_

        ##Calculate each field and get the value in escrow_amount
        escrow_calculation = 0
        for index in range(len(signs)):
            if signs[index] == 'plus':
                escrow_calculation += float(values[index].replace(',',''))
            elif signs[index] == 'minus':
                escrow_calculation -= float(values[index].replace(',',''))
            elif signs[index] == 'dollar':
                ##Get the number in escrow_amount field
                escrow_amount = ''
                for character in values[index]:
                    if character.isdigit() or character in ('.', '-'):
                        escrow_amount = escrow_amount + character
                escrow_amount = float(escrow_amount)

        ##Check if the result is equal to the value in 'escrow_amount' field
        dumplogger.info("The result of calculation is " + str(escrow_calculation))
        dumplogger.info("The value in \'escrow_amount\' field is " + str(escrow_amount))
        if abs(escrow_calculation - escrow_amount) < 0.001:
            dumplogger.info("The result is equal to the value in \'escrow_amount\' field.")
        else:
            ret = 0
            dumplogger.info("The result is not equal to the value in \'escrow_amount\' field!!")

        OK(ret, int(arg['result']), 'AdminNewOrderPage.CheckEscrowCalculation')


class AdminNewOrderButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMiddleSectionTab(arg):
        '''
        ClickMiddleSectionTab : Click middle section tab in admin order detail page
                Input argu :
                    tab_type - order_infomation, status_timeline, operator_action
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click middle section tab in admin order detail page
        xpath = Util.GetXpath({"locate":tab_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click middle section tab in admin order detail page => %s" % (tab_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickMiddleSectionTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHyperLink(arg):
        '''
        ClickHyperLink : Click hyper link in admin order detail page
                Input argu :
                    link_type - checkout_id, ofg_id, return_id, refund_id, return_id, seller_txn_fee_rule, buyer, seller, auto_push_shipment
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_type = arg["link_type"]

        ##Click link in order detail page
        xpath = Util.GetXpath({"locate": link_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click link in admin order detail page => %s" % (link_type), "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickHyperLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderHistoryTab(arg):
        '''
        ClickOrderHistoryTab : Click 'Order and Logistic History' tab under 'Status & Timeline' tab in admin order detail page
                Input argu :
                    tab_type - all, order, logistic
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click 'Order and Logistic History' tab
        xpath = Util.GetXpath({"locate":tab_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click 'Order and Logistic History' tab => %s" % (tab_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickOrderHistoryTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCodingMonkey(arg):
        '''
        ClickCodingMonkey : Click coding monkey button in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click coding monkey button
        xpath = Util.GetXpath({"locate": "coding_monkey_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click coding monkey button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickCodingMonkey')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductListArrow(arg):
        '''
        ClickProductListArrow : Click downward/right arrow in Product List section in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click downward/right arrow
        xpath = Util.GetXpath({"locate":"arrow"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click downward/right arrow", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickProductListArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductListNextPage(arg):
        '''
        ClickProductListNextPage : Click next page btn of Product List section in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next page btn
        xpath = Util.GetXpath({"locate":"next_page_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click next page in product list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickProductListNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductListExclamationIcon(arg):
        '''
        ClickProductListExclamationIcon : Click exclamation icon in Product List section in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exclamation icon
        xpath = Util.GetXpath({"locate":"exclamation_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click exclamation icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickProductListExclamationIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def Click3PLStatusArrow(arg):
        '''
        Click3PLStatusArrow : Click downward/right arrow in 3PL Status History in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click downward/right arrow
        xpath = Util.GetXpath({"locate":"arrow"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click downward/right arrow", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.Click3PLStatusArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEscrowAmount(arg):
        '''
        ClickEscrowAmount : Click escrow amount button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click escrow amount button
        xpath = Util.GetXpath({"locate":"escrow_detail_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click escrow amount button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickEscrowAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionInfo(arg):
        '''
        ClickPromotionInfo : Click promotion info inside order detail page
            Input argu :
                column - shopee_voucher_discount, shop_voucher_discount, shopee_voucher_earned, shop_voucher_earned
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column = arg['column']

        ##Click promotion info in admin order detail page
        xpath = Util.GetXpath({"locate":column})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message":"Click " + column, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickPromotionInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnID(arg):
        '''
        ClickReturnID : Click return id in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return id in admin order detail page
        xpath = Util.GetXpath({"locate": "return_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click return id in admin order detail page", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNewOrderButton.ClickReturnID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DisplayHiddenValue(arg):
        '''
        DisplayHiddenValue : Display all hidden value in order detail page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click display value icon in order detail page
        xpath = Util.GetXpath({"locate":"display_icon"})
        for hidden_icon in range(11):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message":"Click display icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewOrderButton.DisplayHiddenValue')


class AdminOFGPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderSN(arg):
        '''CheckOrderSN : Check order SN in new order page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Check order SN in new order page
        xpath = Util.GetXpath({"locate":"order_sn_display_text"})
        xpath = xpath.replace('order_sn_to_be_replaced', GlobalAdapter.OrderE2EVar._OrderSNDict_["ordersn"][-1])
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOFGPage.CheckOrderSN')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOFGID(arg):
        '''CheckOFGID : Check ofg ID in new order page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Check ofg ID in new order page
        xpath = Util.GetXpath({"locate":"ofg_id_display_text"})
        xpath = xpath.replace('ofg_id_to_be_replaced', GlobalAdapter.OrderE2EVar._OFGID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOFGPage.CheckOFGID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckFOrderID(arg):
        '''CheckFOrderID : Check fulfilment order ID in new order page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Check fulfilment order ID in new order page
        xpath = Util.GetXpath({"locate":"forder_id_display_text"})
        xpath = xpath.replace('forder_id_to_be_replaced', GlobalAdapter.OrderE2EVar._FOrderID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOFGPage.CheckFOrderID')


class AdminOFGButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNavigationTab(arg):
        '''ClickNavigationTab : Click navigation tab in new order detail page
                Input argu : tab_type - basic_data / status_history
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click navigation tab in new order detail page
        xpath = Util.GetXpath({"locate":tab_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click navigation tab in new order detail page => %s" % (tab_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminOFGButton.ClickNavigationTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCodingMonkey(arg):
        ''' ClickCodingMonkey : Click coding monkey button in OFG detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        #Click coding monkey button
        xpath = Util.GetXpath({"locate": "coding_monkey_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click coding monkey button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOFGButton.ClickCodingMonkey')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdateFulfilmentStatus(arg):
        ''' ClickUpdateFulfilmentStatus : Click Update Fulfilment Status button in OFG detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        #Click Update Fulfilment Status button
        xpath = Util.GetXpath({"locate": "update_fulfilment_status_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Update Fulfilment Status button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOFGButton.ClickUpdateFulfilmentStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseToSwipeWindow(arg):
        ''' ClickCloseToSwipeWindow : Click close to swipe right window
                Input argu : tab_type - coding_monkey / fulfilment_status
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        #Click coding monkey button
        xpath = Util.GetXpath({"locate": tab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close to swipe right window => %s" % (tab_type), "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOFGButton.ClickCloseToSwipeWindow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectFulfilmentStatus(arg):
        ''' SelectFulfilmentStatus : Select the fulfilment to change status
                Input argu :
                        status - request_cancel / outbound_failed / outbound_retry / outbound_successful / successful / lost / delivery_failed
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        status = arg['status']

        #Click Update Fulfilment Status button
        xpath = Util.GetXpath({"locate": "update_fulfilment_status_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Update Fulfilment Status button", "result": "1"})
        time.sleep(3)

        ##Expand change status dropdown list
        xpath = Util.GetXpath({"locate": "change_status_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Expand change status dropdown", "result": "1"})

        ##Select product category
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select status", "result": "1"})

        ##Input word in comment
        xpath = Util.GetXpath({"locate": "comment_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "auto", "result": "1"})

        ##Click save to update status
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Save button to update status", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOFGButton.SelectFulfilmentStatus')


class AdminNewReturnPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToReturnRefundDetail(arg):
        '''
        GoToReturnRefundDetail : Go to Return Refund detail by search order id
                Input argu :
                    row_number - the row number of the order which you want to choose
                    is_click - 1 (go to ReturnRefundDetail ) / 0 (Stay in search result)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row_number = arg["row_number"]
        is_click = arg["is_click"]

        ##Input order id and search
        xpath = Util.GetXpath({"locate": "order_id_input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Check target order id on list page
        xpath = Util.GetXpath({"locate": "first_return_list_search_result"})
        xpath = xpath.replace("order_id_to_be_replaced", GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number": "5", "result": "1"})

        if int(is_click):
            ##Click return id
            xpath = Util.GetXpath({"locate": "return_id"})
            xpath = xpath.replace("replace_row_number", str(int(row_number)+1))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click target return id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnPage.GoToReturnRefundDetail -> ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetReturnSN(arg):
        '''
        GetReturnSN : Get return sn by search return id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input return id
        xpath = Util.GetXpath({"locate": "return_id_input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._ReturnID_, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Get refund sn from search result
        xpath = Util.GetXpath({"locate": "return_sn_search_result"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "return_sn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnPage.GetReturnSN')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckReturnID(arg):
        '''
        CheckReturnID : Check return ID in all admin return refund page
                Input argu :
                    locate - locate of element return ID you want to check it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg["locate"]

        ##Get element display text (return ID)
        BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Check return ID in locate element text
        if GlobalAdapter.OrderE2EVar._ReturnID_ in GlobalAdapter.CommonVar._PageAttributes_:
            dumplogger.info("Element display correct return ID => %s" % (GlobalAdapter.OrderE2EVar._ReturnID_))
        else:
            ret = 0
            dumplogger.info("Return ID not in locate element text. Return ID => %s. Locate element text => %s" % (GlobalAdapter.OrderE2EVar._ReturnID_, GlobalAdapter.CommonVar._PageAttributes_))

        OK(ret, int(arg['result']), 'AdminNewReturnPage.CheckReturnID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCheckOutID(arg):
        '''
        CheckCheckOutID : Check checkout ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"checkout_id_display_text"})
        xpath = xpath.replace('checkout_id_to_be_replaced', GlobalAdapter.OrderE2EVar._CheckOutID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnPage.CheckCheckOutID')


class AdminNewReturnButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit to change return status in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnStatusDropDown(arg):
        '''
        ClickReturnStatusDropDown : Click return status dropdown in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return status dropdown
        xpath = Util.GetXpath({"locate": "rr_status_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click return status dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnButton.ClickReturnStatusDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMiddleSectionTab(arg):
        '''
        ClickMiddleSectionTab : Click middle section tab in R/R detail page
                Input argu :
                    tab_type - return_refund_info, status_timeline
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click middle section tab in R/R detail page
        xpath = Util.GetXpath({"locate":tab_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click middle section tab in R/R detail page => %s" % (tab_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnButton.ClickMiddleSectionTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLink(arg):
        '''
        ClickLink : Click link in R/R detail page
                Input argu :
                    link_type - order_id, checkout_id, refund_id, general_info_seller_name, general_info_buyer_name, status_summary_operator
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_type = arg["link_type"]

        ##Click link in R/R detail page
        xpath = Util.GetXpath({"locate": link_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click link in R/R detail page => %s" % (link_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnButton.ClickLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStatusSummary(arg):
        '''
        ClickStatusSummary : Click status summary in R/R detail page
                Input argu :
                    status - return_status, logistic_status, negotiation_status, compensation_status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click status summary in R/R detail page
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status summary in R/R detail page => %s" % (status), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnButton.ClickStatusSummary')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewAttachment(arg):
        '''
        ClickViewAttachment : Click view attechment in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view attechment in R/R detail page
        xpath = Util.GetXpath({"locate": "view_attachment_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view attechment in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewReturnButton.ClickViewAttachment')


class AdminSellerTxnFeePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleName(arg):
        '''
        InputRuleName : Input rule name in seller transaction fee page
                Input argu :
                    rule_name - seller transaction fee rule name you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg["rule_name"]

        ##Input rule name field
        xpath = Util.GetXpath({"locate":"rule_name_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.InputRuleName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleTimePeriod(arg):
        '''
        InputRuleTimePeriod : Input rule time period in seller transaction fee page
                Input argu :
                    start_time - start time delta (minutes) you want to input
                    end_time - end time delta (minutes) you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time field (Need to click element first to active input field)
        start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S %z", minutes=int(start_time))
        start_time_string = start_time_string[:-2] + ":" + start_time_string[-2:]
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_string, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input end time field (Need to click element first to active input field)
        end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S %z", minutes=int(end_time))
        end_time_string = end_time_string[:-2] + ":" + end_time_string[-2:]
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time_string, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.InputRuleTimePeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGroupID(arg):
        '''
        InputGroupID : Input group id in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input group id
        xpath = Util.GetXpath({"locate":"group_id_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.SellerTxnFeeE2EVar._GroupID_, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.InputGroupID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRatePercentage(arg):
        '''
        InputRatePercentage : Input rate percentage in seller transaction fee page
                Input argu :
                    rate_percentage - 0 ~ 100 (%)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rate_percentage = arg["rate_percentage"]

        ##Input rate percentage
        xpath = Util.GetXpath({"locate":"rate_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rate_percentage, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.InputRatePercentage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFee(arg):
        '''
        InputFee : Input fee in seller transaction fee page
                Input argu :
                    fee_type - min_fee, max_fee, flat_fee
                    fee_amount - how many fee amount you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        fee_type = arg["fee_type"]
        fee_amount = arg["fee_amount"]

        ##Input fee
        xpath = Util.GetXpath({"locate":fee_type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":fee_amount, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.InputFee -> ' + fee_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetSettingTitleID(arg):
        '''
        GetSettingTitleID : Get seller txn fee setting title ID (rule ID / group ID)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get seller txn fee setting title XPath
        xpath = Util.GetXpath({"locate":"seller_txn_fee_setting_title"})

        ##Get seller txn fee rule ID
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"seller_txn_fee_rule_id", "result": "1"})

        ##Get seller txn fee group ID
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"seller_txn_fee_group_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.GetSettingTitleID -> Rule ID:%s, Group ID:%s' % (GlobalAdapter.SellerTxnFeeE2EVar._RuleID_, GlobalAdapter.SellerTxnFeeE2EVar._GroupID_))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetRuleTime(arg):
        '''
        SetRuleTime : Set seller transaction fee rule time (unix time) to 00:00 A.M. in DB
                Input argu :
                    db_file - db file name
                    set_type - start_time, end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = arg["db_file"]
        set_type = arg["set_type"]

        ##Get unix time in today 00:00 A.M.
        today_unix = int(time.mktime(datetime.date.today().timetuple()))
        dumplogger.info("Unix time of 00:00 A.M. today: %d " % (today_unix))

        ##Assign start time, end time and rule id to global first
        GlobalAdapter.CommonVar._DynamicCaseData_[set_type] = today_unix
        GlobalAdapter.CommonVar._DynamicCaseData_["group_id"] = GlobalAdapter.SellerTxnFeeE2EVar._GroupID_

        ##Define SQL assign list
        assign_list = [{"column": set_type, "value_type": "string"}, {"column": "group_id", "value_type": "string"}]

        ##Send SQL command
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_order_accounting", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.SetRuleTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRuleID(arg):
        '''
        CheckRuleID : Check rule ID in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check rule ID in seller transaction fee page
        xpath = Util.GetXpath({"locate":"rule_id_display_text"})
        xpath = xpath.replace("rule_id_to_be_replaced", GlobalAdapter.SellerTxnFeeE2EVar._RuleID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.CheckRuleID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShopIDCsv(arg):
        '''
        UploadShopIDCsv : Upload shop ID csv file in seller transaction fee setting page
                Input argu :
                    file_name : file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Get upload csv input field
        xpath = Util.GetXpath({"locate": "upload_csv_input_field"})

        ##Upload shops id csv file
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate":xpath, "element": "", "element_type": "xpath", "project": Config._TestCaseFeature_, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeePage.UploadShopIDCsv')


class AdminSellerTxnFeeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewRule(arg):
        '''
        ClickNewRule : Click new rule in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click new rule button
        xpath = Util.GetXpath({"locate": "new_rule_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreate(arg):
        '''
        ClickCreate : Click create in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create button
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickCreate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        '''
        ClickUpdate : Click update in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click update button
        xpath = Util.GetXpath({"locate": "update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click tab in seller transaction fee page
                Input argu :
                    shop_category - local, cross_border
                    rule_type - normal, special_user_group
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_category = arg["shop_category"]
        rule_type = arg["rule_type"]

        ##Click shop category button
        xpath = Util.GetXpath({"locate": shop_category})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop category tab => %s" % (shop_category), "result": "1"})

        ##Click rule type button
        xpath = Util.GetXpath({"locate": rule_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule type button => %s" % (rule_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRatePercentage(arg):
        '''
        ClickRatePercentage : Click rate percentage in seller transaction fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click rate percentage button
        xpath = Util.GetXpath({"locate": "rate_percentage_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rate percentage button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickRatePercentage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProceed(arg):
        '''
        ClickProceed : Click proceed in seller transaction fee page
                Input argu :
                    proceed_type - proceed, do_not_proceed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        proceed_type = arg["proceed_type"]

        ##Click proceed button
        xpath = Util.GetXpath({"locate": proceed_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click proceed button => %s" % (proceed_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickProceed')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDown(arg):
        '''
        ClickDropDown : Click drop down in seller transaction fee page
                Input argu :
                    drop_down_type - rule_type, payment_channel, charge_type, shop_category, upload_shop_id, option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drop_down_type = arg["drop_down_type"]

        ##Click drop down
        xpath = Util.GetXpath({"locate": drop_down_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down => %s" % (drop_down_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRuleTypeDropDownOption(arg):
        '''
        ClickRuleTypeDropDownOption : Click rule type drop down option in seller transaction fee page
                Input argu :
                    option - normal_rule, special_user_group_rule
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click rule type drop down option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule type drop down option => %s" % (option), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickRuleTypeDropDownOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentChannelDropDownOption(arg):
        '''
        ClickPaymentChannelDropDownOption : Click payment channel drop down option in seller transaction fee page
                Input argu :
                    payment_channel_option_name - which payment channel option name you want to click it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        payment_channel_option_name = arg["payment_channel_option_name"]

        ##Click payment channel drop down option
        xpath = Util.GetXpath({"locate": "pay_channel_drop_down_option"})
        xpath_pay_channel = xpath.replace("pay_channel_option_name_to_be_replaced", payment_channel_option_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_pay_channel, "message": "Click payment channel drop down option => %s" % (payment_channel_option_name), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickPaymentChannelDropDownOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDownOption(arg):
        '''
        ClickDropDownOption : Click drop down option in seller transaction fee page
                Input argu :
                    option_name - which option name you want to click it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option_name = arg["option_name"]

        ##Click payment channel info option drop down option
        xpath = Util.GetXpath({"locate": "drop_down_option"})
        xpath_pay_channel = xpath.replace("option_name_to_be_replaced", option_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_pay_channel, "message": "Click drop down option => %s" % (option_name), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickDropDownOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChargeTypeDropDownOption(arg):
        '''
        ClickChargeTypeDropDownOption : Click charge type drop down option in seller transaction fee page
                Input argu :
                    option - rate, flat_fee
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click charge type drop down option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click charge type drop down option => %s" % (option), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickChargeTypeDropDownOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeCategoryDropDownOption(arg):
        '''
        ClickShopeCategoryDropDownOption : Click shop category drop down option in seller transaction fee page
                Input argu :
                    option - local, cross_border
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click shop category drop down option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop category drop down option => %s" % (option), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTxnFeeButton.ClickShopeCategoryDropDownOption')


class AdminServiceFeePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchShopID(arg):
        '''
        SearchShopID : Input shopID to search in seller view page
                Input argu :
                    shop_id - shop ID want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopID = arg['shop_id']

        ##Input shopID to search in seller view page
        xpath = Util.GetXpath({"locate": "shopid_search_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopID, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Check desired result display or not
        xpath = Util.GetXpath({"locate": "shopid_result_column"})
        xpath = xpath.replace("shop_id_replaced", shopID)
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Click shopID to information page
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop ID link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.SearchShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleNameField(arg):
        '''
        InputRuleNameField : Input rule name
                Input argu :
                    rule_name - service fee rule name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg['rule_name']

        ##Input rule name
        xpath = Util.GetXpath({"locate":"rule_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputRuleNameField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayNameField(arg):
        '''
        InputDisplayNameField : Input display name
                Input argu :
                    display_name - service fee rule name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        display_name = arg['display_name']

        ##Input rule name
        xpath = Util.GetXpath({"locate":"display_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":display_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputDisplayNameField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopCategoryField(arg):
        '''
        InputShopCategoryField : Select shop category
                Input argu :
                    category_type - C2C, B2C, CB
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['category_type']

        category_type = {
            "C2C": "1",
            "B2C": "2",
            "CB": "3"
        }

        time.sleep(3)

        ##Click shop category field
        xpath = Util.GetXpath({"locate":"dropdown_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click drop dwon icon", "result": "1"})

        time.sleep(3)

        ##Select shop category
        xpath = Util.GetXpath({"locate": "select_icon"}).replace('value_to_be_replace', category_type[type])
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select shop catagory", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputShopCategoryField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStartPeriodField(arg):
        '''
        InputStartPeriodField : Input start period
                Input argu :
                    start_time_day - service fee start time days setting
                    start_time_min - service fee start time minutes setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time_day = arg['start_time_day']
        start_time_min = arg['start_time_min']

        if start_time_day or start_time_min:

            GlobalAdapter.ServiceFeeE2EVar._StartTime_ = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:00 %z", int(start_time_day), int(start_time_min), 0)[:-2] + ":00"

            dumplogger.info("Service Fee Start Time: %s" % (GlobalAdapter.ServiceFeeE2EVar._StartTime_))

            ##Wait for page loading
            time.sleep(2)

            ##Get start period field xpath
            xpath = Util.GetXpath({"locate": "start_period"})

            ##Clear old value
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start period field", "result": "1"})

            ##Get clear button xpath
            clear_button = Util.GetXpath({"locate": "clear_button"})

            ##Check field is empty
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": clear_button, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": clear_button, "message":"Click clear button", "result": "1"})
            time.sleep(2)

            ##Input start time
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.ServiceFeeE2EVar._StartTime_, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type": "enter","string": "","result": "1"})
        else:
            ##Input empty start time
            xpath = Util.GetXpath({"locate":"start_period"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message":"Click start period field", "result":"1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"", "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter","string": "", "result":"1"})
        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputStartPeriodField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEndPeriodField(arg):
        '''
        InputEndPeriodField : Input end period
                Input argu :
                    end_time_day - service fee end time days setting
                    end_time_min - service fee end time minutes setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time_day = arg['end_time_day']
        end_time_min = arg['end_time_min']

        if end_time_day or end_time_min:

            GlobalAdapter.ServiceFeeE2EVar._EndTime_ = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:59 %z", int(end_time_day), int(end_time_min), 0)[:-2] + ":00"

            dumplogger.info("Service Fee End Time: %s" % (GlobalAdapter.ServiceFeeE2EVar._EndTime_))

            ##Wait for page loading
            time.sleep(2)

            ##Get end period field xpath
            xpath = Util.GetXpath({"locate":"end_period"})

            ##Clear old value
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end period field", "result": "1"})

            ##Get clear button xpath
            clear_button = Util.GetXpath({"locate":"clear_button"})

            ##Check field is empty
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": clear_button, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": clear_button, "message": "Click clear button", "result": "1"})

            time.sleep(2)

            ##Input end time
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.ServiceFeeE2EVar._EndTime_, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
        else:
            ##Input empty end time
            xpath = Util.GetXpath({"locate":"end_period"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end period field", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"", "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputEndPeriodField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRateChargedField(arg):
        '''
        InputRateChargedField : Input rate charged
                Input argu :
                    rate_charged - service fee rule rate charged
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rate_charged = arg['rate_charged']

        ##Input rate charged
        xpath = Util.GetXpath({"locate":"rate_charged"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rate_charged, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputRateChargedField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemPriceCapField(arg):
        '''
        InputItemPriceCapField : Input item price cap
                Input argu :
                    item_price_cap - service fee rule item price cap
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_price_cap = arg['item_price_cap']

        ##Input item price cap
        xpath = Util.GetXpath({"locate":"item_price_cap"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":item_price_cap, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputItemPriceCapField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSellerShopsidsCsv(arg):
        '''
        UploadSellerShopsidsCsv : Upload seller shopsids csv file in update shops page
                Input argu :
                    upload_csv - upload csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_csv = arg['upload_csv']

        if upload_csv:
            ##Upload seller shopsids csv file
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "file", "element_type": "type", "project": Config._TestCaseFeature_, "action": "file", "file_name": upload_csv, "file_type": "csv", "result": "1"})
            time.sleep(5)

        else:
            ##Upload seller shopsids non csv file
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "file", "element_type": "type", "project": Config._TestCaseFeature_, "action": "file", "file_name": "non_csv_servicefee_rule", "file_type": "txt", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminServiceFeePage.UploadSellerShopsidsCsv -> ' + upload_csv)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRuleID(arg):
        '''
        GetRuleID : Get service fee rule ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get service fee rule ID
        xpath = Util.GetXpath({"locate":"rule_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"service_fee_rule_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.GetRuleID -> ' + GlobalAdapter.ServiceFeeE2EVar._RuleID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetAndCheckRuleTime(arg):
        '''
        GetAndCheckRuleTime : Check service fee start time or end time
                Input argu :
                    time_type - start time or end time to be checked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time_type = arg['time_type']

        ##Get service fee rule start time and check
        xpath = Util.GetXpath({"locate":time_type})
        if time_type == "start_time":
            BaseUILogic.GetAndCheckElementAttribute({"method": "xpath", "locate": xpath, "string": GlobalAdapter.ServiceFeeE2EVar._StartTime_, "attribute": "value", "result": "1"})

        elif time_type == "end_time":
            BaseUILogic.GetAndCheckElementAttribute({"method": "xpath", "locate": xpath, "string": GlobalAdapter.ServiceFeeE2EVar._EndTime_, "attribute": "value", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.GetAndCheckRuleTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTimeInChangeLog(arg):
        '''
        CheckTimeInChangeLog : Check element in change log in service fee settings rule page
                Input argu :
                    check_element - start_time/end_time
                    log_row_index - the index of which log want to check in change log table
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_element = arg['check_element']
        log_row_index = arg['log_row_index']

        ##Locate check element xpath
        xpath = Util.GetXpath({"locate":'log_remark'})

        ##Replace xpath by change log row
        xpath = xpath.replace("log_row_replaced", log_row_index)

        ##Transfer time format to corresponding format in change log
        if check_element == 'start_time':
            element_value = datetime.datetime.strptime(GlobalAdapter.ServiceFeeE2EVar._StartTime_[:-10],'%Y-%m-%d %H:%M')
        else:
            element_value = datetime.datetime.strptime(GlobalAdapter.ServiceFeeE2EVar._EndTime_[:-10],'%Y-%m-%d %H:%M')

        ##Assemble element_value
        element_value = datetime.datetime.strftime(element_value,'%a %b %d %Y %H:%M')

        ##Replace xpath by change log row
        xpath = xpath.replace("log_value_replaced", element_value)

        ##Get the value of element in change log and check the value
        BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.CheckChangeLog')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchRule(arg):
        '''
        SearchRule : Input information to search rule in service fee page
                Input argu :
                    search_type - search by rule_name/ rule_ID/ product_label
                    input_content - search keyword
                    is_click - 1 (click rule ID), 0 (do not click rule ID)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg['search_type']
        input_content = arg['input_content']
        is_click = arg['is_click']
        search_type_map = {
            "rule_name":"Rule Name",
            "rule_id":"Rule ID",
            "start_time":"Start Time",
            "end_time":"End Time",
            "display_name":"Display Name",
            "product_label":"Product Label ID"
        }

        ##Get ruleID by GetRuleID function
        if not input_content:
            input_content = GlobalAdapter.ServiceFeeE2EVar._RuleID_
        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "search_table"})
        xpath = xpath.replace('search_type_replaced', search_type_map[search_type])

        ##According search type to search rule
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input box", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Click RuleID to information page
        if int(is_click):
            time.sleep(3)
            xpath = Util.GetXpath({"locate": "search_result"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule ID link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.SearchRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UpdateShopsSeller(arg):
        '''
        UpdateShopsSeller : Click update shops and add or remove seller
                Input argu :
                    update_type - add or remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        update_type = arg['update_type']

        ##Click update shops
        xpath = Util.GetXpath({"locate": "combo_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click combo box to select update type", "result": "1"})

        ##Select add or remove seller
        xpath = Util.GetXpath({"locate": update_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add update type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.UpdateShopsSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShopByUserTag(arg):
        '''
        UploadShopByUserTag : Upload shop by user tag and choose seller type
                Input argu :
                    user_tag_name - user tag name
                    seller_type - preferred/mall/-/normal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_tag_name = arg['user_tag_name']
        seller_type = arg['seller_type']

        ##Click "By User Tag" checkbox
        xpath = Util.GetXpath({"locate":"user_tag_checkbox"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click By User Tag checkbox", "result": "1"})

        ##Click "Included" or "Excluded" checkbox
        xpath = Util.GetXpath({"locate":"included_checkbox"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click Included or Excluded checkbox", "result": "1"})

        if seller_type:
            ##Click seller type dropdown menu
            xpath = Util.GetXpath({"locate":"seller_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller type dropdown menu", "result": "1"})

            ##Select seller type
            xpath = Util.GetXpath({"locate":seller_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select seller type", "result": "1"})

        time.sleep(3)

        ##Input user tag name
        xpath = Util.GetXpath({"locate":"user_tag_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":user_tag_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.UploadShopByUserTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOrderCountFullRange(arg):
        '''
        InputOrderCountFullRange : Input lower and upper bound value of No. of Orders
                Input argu :
                    lower_bound_value - value of No. of Orders
                    upper_bound_value - value of No. of Orders
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        lower_bound_value = arg['lower_bound_value']
        upper_bound_value = arg['upper_bound_value']

        ##Input lower bound value
        xpath = Util.GetXpath({"locate":"lower_bound_value"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":lower_bound_value, "result": "1"})

        ##Input upper bound value
        xpath = Util.GetXpath({"locate":"upper_bound_value"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":upper_bound_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputOrderCountFullRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPastNumberOfDays(arg):
        '''
        InputPastNumberOfDays : Input past number of days in Shop Sales Criteria
                Input argu :
                    past_number_of_days - past number of days
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        past_number_of_days = arg['past_number_of_days']

        ##Input past number of days
        xpath = Util.GetXpath({"locate":"past_number_of_days"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":past_number_of_days, "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.InputPastNumberOfDays')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetRuleStatus(arg):
        '''
        SetRuleStatus : Set service fee rule db time to make rule active or upcoming immediately
                Input argu :
                    rule_id - set vaule to 1 and get rule id in Global
                    action_type - active / upcoming_under_1_hour / upcoming_over_1_hour
                    db_file - db file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_id = arg['rule_id']
        db_file = arg["db_file"]
        action_type = arg['action_type']

        ##Get unix time in today 00:00 A.M.
        today_unix = int(time.mktime(datetime.datetime.now().timetuple()))
        dumplogger.info("Unix time of 00:00 A.M. today: %d " % (today_unix))

        ##Assign start time and rule id to global first
        if action_type in ('active', 'upcoming_over_1_hour', 'upcoming_under_1_hour'):
            if action_type == 'active':
                GlobalAdapter.CommonVar._DynamicCaseData_["start_time"] = today_unix
            elif action_type == 'upcoming_over_1_hour':
                GlobalAdapter.CommonVar._DynamicCaseData_["start_time"] = 2104394000
            elif action_type == 'upcoming_under_1_hour':
                GlobalAdapter.CommonVar._DynamicCaseData_["start_time"] = today_unix + 1800

            GlobalAdapter.CommonVar._DynamicCaseData_["rule_id"] = GlobalAdapter.ServiceFeeE2EVar._RuleID_

            ##Define assign list
            assign_list = []

            ##Check whether if need to assign rule id or not
            if rule_id:
                assign_list.append({"column": "rule_id", "value_type": "string"})
            else:
                dumplogger.info("Do not need to assign rule id")

            assign_list.append({"column": "start_time", "value_type": "string"})
            ##Send SQL command
            DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_servicefee", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})
        else:
            dumplogger.info("please check the argument action_type, it should be active / upcoming_under_1_hour / upcoming_over_1_hour.")
            ret = 0

        OK(ret, int(arg['result']), 'AdminServiceFeePage.SetRuleStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetRuleShopTime(arg):
        '''
        SetRuleShopTime : Set service fee rule shop db time to make add action and remove action work immediately
                Input argu :
                            rule_id - set vaule to 1 and get rule id in Global
                            action_type - add / remove
                            db_file - db file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_id = arg['rule_id']
        db_file = arg["db_file"]
        action_type = arg['action_type']

        ##Get unix time in today 00:00 A.M.
        today = datetime.date.today()
        today_unix = int(time.mktime(today.timetuple()))
        dumplogger.info("Unix time of 00:00 A.M. today: %d " % (today_unix))

        ##Assign start time, end time and rule id to global first
        GlobalAdapter.CommonVar._DynamicCaseData_["start_time"] = today_unix
        GlobalAdapter.CommonVar._DynamicCaseData_["end_time"] = today_unix
        GlobalAdapter.CommonVar._DynamicCaseData_["rule_id"] = GlobalAdapter.ServiceFeeE2EVar._RuleID_

        ##Define assign list
        assign_list = []

        ##Check whether if need to assign rule id or not
        if rule_id:
            assign_list.append({"column": "rule_id", "value_type": "string"})
        else:
            dumplogger.info("Do not need to assign rule id")

        ##Check need to assign start time or end time
        if action_type == 'add':
            assign_list.append({"column": "start_time", "value_type": "string"})
        elif action_type == 'remove':
            assign_list.append({"column": "end_time", "value_type": "string"})

        ##Send SQL command
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_servicefee", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeePage.SetRuleShopTime')


class AdminServiceFeeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewRule(arg):
        '''
        ClickNewRule : Click new rule in service fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click new rule button
        xpath = Util.GetXpath({"locate": "new_rule_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreate(arg):
        '''
        ClickCreate : Click create button in service fee setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create button
        xpath = Util.GetXpath({"locate":"create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickCreate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        '''
        ClickOk : Click ok button in service fee create page
                Input argu :
                    btn_type - create_rule / error_message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button
        ok_btn = arg['btn_type']
        xpath = Util.GetXpath({"locate":ok_btn})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickOk')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerView(arg):
        '''
        ClickSellerView : Click seller view in service fee create page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click seller view btn button
        xpath = Util.GetXpath({"locate":"seller_view_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller view button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickSellerView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportSellerShopIds(arg):
        '''
        ClickExportSellerShopIds : Click export seller shop Ids button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Wait for web loading
        time.sleep(3)

        ##Click export seller shop Ids button in service fee settings page
        xpath = Util.GetXpath({"locate":"export_seller_shop_ids_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click export seller shop Ids button", "result": "1"})

        ##Wait for download
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickExportSellerShopIds')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportGlobalProductCat(arg):
        '''
        ClickExportGlobalProductCat : Click export global product cat button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Wait for web loading
        time.sleep(3)

        ##Click export seller shop Ids button in service fee settings page
        xpath = Util.GetXpath({"locate":"export_global_product_cat_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click export global product cat button", "result": "1"})

        ##Wait for download
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickExportGlobalProductCat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportLocalProductCat(arg):
        '''
        ClickExportLocalProductCat : Click export local product cat button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Wait for web loading
        time.sleep(3)

        ##Click export seller shop Ids button in service fee settings page
        xpath = Util.GetXpath({"locate":"export_local_product_cat_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click export local product cat button button", "result": "1"})

        ##Wait for download
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickExportLocalProductCat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadAuditID(arg):
        '''
        ClickDownloadAuditID : Click download audit ID button in change log
                Input argu :
                    row - row number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row = arg['row']

        ##Click download audit ID button in change log
        xpath = Util.GetXpath({"locate": "download_audit_id_btn"})
        xpath = xpath.replace("row_replace",str(int(row)+1))
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download audit id btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickDownloadAuditID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCrossBorderTab(arg):
        '''
        ClickCrossBorderTab : Click cross border tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cross border tab
        xpath = Util.GetXpath({"locate":"cb_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CB tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickCrossBorderTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button in service fee settings page
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete button in service fee settings page
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickYes(arg):
        '''
        ClickYes : Click Yes button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Yes button in service fee settings page
        xpath = Util.GetXpath({"locate": "yes_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Yes button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickYes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        '''
        ClickUpdate : Click Update button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click update button in service fee settings page
        xpath = Util.GetXpath({"locate": "update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefresh(arg):
        '''
        ClickRefresh : Click refresh button service fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refresh rule tab
        xpath = Util.GetXpath({"locate":"refresh_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click refresh button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickRefresh')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button in service fee settings page
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRateCharged(arg):
        '''
        ClickRateCharged : Click rate charged field in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click rate charged field in service fee settings page
        xpath = Util.GetXpath({"locate": "rate_charged"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rate charged", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickRateCharged')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemPriceCap(arg):
        '''
        ClickRateCharged : Click item price cap field in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click item price cap field in service fee settings page
        xpath = Util.GetXpath({"locate": "item_price_cap"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item price cap", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickItemPriceCap')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadShopID(arg):
        '''
        ClickUploadShopID : Click upload shopid button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload shopid button in service fee settings page
        xpath = Util.GetXpath({"locate": "upload_shopid"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload shopid", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickUploadShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button in service fee settings page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button in service fee settings page
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadChangeLog(arg):
        '''
        ClickDownloadChangeLog : Click Download button in service fee settings page change log section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click downlaod button in service fee settings page
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickDownloadChangeLog')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadIcon(arg):
        '''
        ClickDownloadIcon : Click download icon in pop up window after create rule
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download icon in service fee settings page
        xpath = Util.GetXpath({"locate": "download_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickDownloadIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserTagSync(arg):
        '''
        ClickUserTagSync : Click user tag sync in servicefee rule view page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click user tag sync button in service fee settings page
        xpath = Util.GetXpath({"locate": "user_tag_sync"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user tag sync", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.ClickUserTagSync')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectOrderCountCheckbox(arg):
        '''
        SelectOrderCountCheckbox : Select order count checkbox in Shop Sales Criteria
                Input argu :
                    order_count_checkbox - lower_bound/upper_bound/full_range
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order_count_checkbox = arg['order_count_checkbox']

        ##Click order count checkbox
        xpath = Util.GetXpath({"locate":order_count_checkbox})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click order count checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.SelectOrderCountCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectViewType(arg):
        '''
        SelectViewType : Select view type in servicefee list page
                Input argu :
                    view_type - expanded_view/simplified_view/b2c
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        view_type = arg['view_type']

        ##Click order count checkbox
        xpath = Util.GetXpath({"locate":view_type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click %s." % (view_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeButton.SelectViewType')


class AdminNewEscrowReleasePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderIdInTab(arg):
        '''
        CheckOrderIdInTab : Check single/multiple tab of escrow release page if order Id exist
                Input argu :
                    tabs - Use ',' to separate 'created,pending,verified,payout,completed'
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tabs = arg["tabs"]

        ##Get tabs that need to check
        tabs_to_check = tabs.split(',')

        ##Check the tabs if order Id exist
        for tab in tabs_to_check:

            ##Click Escrow Release Status
            xpath = Util.GetXpath({"locate":tab})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click escrow release status", "result": "1"})
            time.sleep(15)

            ##Check order Id exist in specific tab
            xpath = Util.GetXpath({"locate":"order_id_txt"})
            xpath = xpath.replace('order_id_to_be_replaced', GlobalAdapter.OrderE2EVar._OrderID_)
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                break
            else:
                ret = 0

        OK(ret, int(arg['result']), 'AdminNewEscrowReleasePage.CheckOrderIdInTab -> \'' + tab + '\', order ID: ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchOnEscrowReleaseListPage(arg):
        '''
        SearchOnEscrowReleaseListPage : Search and filter on escrow release list page
                Input argu :
                    search_type - order_id, seller_username, update_time, fraud_reason
                    input_string - order_id, (specific seller username), (time interval with '~'), (fraud reason(s) id, separate with ',')
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        input_string = arg["input_string"]

        if search_type == "order_id":
            ##Search by order_id
            xpath = Util.GetXpath({"locate":"order_id"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

        elif search_type == "seller_username":
            ##Search by seller_username
            xpath = Util.GetXpath({"locate":"seller_username"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":input_string, "result": "1"})

        elif search_type == "update_time":
            ##Get start date and end date
            start_date = input_string.split('~')[0]
            end_date = input_string.split('~')[1]

            ##Filter by time interval
            xpath = Util.GetXpath({"locate":"update_time_begin"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_date, "result": "1"})
            xpath = Util.GetXpath({"locate":"update_time_end"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_date, "result": "1"})

        elif search_type == "fraud_reason":
            ##Show Reason List
            xpath = Util.GetXpath({"locate":"fraud_reason_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click fraud reson field", "result": "1"})
            xpath = Util.GetXpath({"locate":"fraud_reason_scrollbar"})
            BaseUICore.WebInnerElementScroll({"locate": xpath, "direction": "down", "distance": "600", "result": "1"})

            ##Click the reason(s)
            for reason_id in input_string.split(','):
                ##Search by fraud_reason
                xpath = Util.GetXpath({"locate":"fraud_reason_option"})
                xpath = xpath.replace('reason_id_to_be_replaced', reason_id)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select the fraud reason -> " + reason_id, "result": "1"})

        time.sleep(2)

        ##Click Search btn
        xpath = Util.GetXpath({"locate":"search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Search btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNewEscrowReleasePage.SearchOnEscrowReleaseListPage -> ' + search_type + ': ' + input_string)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToEscrowDetail(arg):
        '''
        GoToEscrowDetail : Click order id and go to escrow detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Order ID and go to escrow detail page
        xpath = Util.GetXpath({"locate":"order_id"})
        xpath = xpath.replace("order_id_to_be_replaced", GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order id and go to escrow detail page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminNewEscrowReleasePage.GoToEscrowDetail')


class AdminNewEscrowReleaseButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEscrowReleaseStatus(arg):
        '''
        ClickEscrowReleaseStatus : Click Escrow Release Status
                Input argu :
                    status - created/pending/verified/payout/completed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click Escrow Release Status
        xpath = Util.GetXpath({"locate":status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click escrow release status", "result": "1"})
        time.sleep(15)

        OK(ret, int(arg['result']), 'AdminNewEscrowReleaseButton.ClickEscrowReleaseStatus ->' + status)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHyperLink(arg):
        '''
        ClickHyperLink : Click hyper link in escrow release list/detail page
                Input argu :
                    link_type - order_id, seller_usename, bank_account_status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_type = arg["link_type"]

        ##Click link in escrow release list/detail page
        xpath = Util.GetXpath({"locate": link_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click link in escrow release list/detail page => %s" % (link_type), "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNewEscrowReleaseButton.ClickHyperLink')


class AdminCommissionFeePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleName(arg):
        '''
        InputRuleName : Input rule name of commission fee rule
                Input argu :
                    rule_name - rule name to be set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg["rule_name"]

        ##Input rule name
        xpath = Util.GetXpath({"locate":"rule_name_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputRuleName -> ' + rule_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleStartDate(arg):
        '''
        InputRuleStartDate : Input rule time period in commission fee rule setting page
                Input argu :
                    start_date - start time delta (days) you want to input
                    end_date - end time delta (days) you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_date = arg["start_date"]

        ##Input start time field (Need to click element first to active input field)
        start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %z", days=int(start_date))
        start_time_string = start_time_string[:-5] + "00:00:00 " + start_time_string[-5:]
        start_time_string = start_time_string[:-2] + ":" + start_time_string[-2:]
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_string, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputRuleStartDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleEndDate(arg):
        '''
        InputRuleEndDate : Input rule time period in commission fee rule setting page
                Input argu :
                    start_date - start time delta (days) you want to input
                    end_date - end time delta (days) you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_date = arg["end_date"]

        ##Input end time field (Need to click element first to active input field)
        end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %z", days=int(end_date))
        end_time_string = end_time_string[:-5] + "23:59:59 " + end_time_string[-5:]
        end_time_string = end_time_string[:-2] + ":" + end_time_string[-2:]
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time_string, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputRuleEndDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRateCharged(arg):
        '''
        InputRateCharged : Input rate charged of commission fee rule
                Input argu :
                    rate - rate to be set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rate = arg["rate"]

        ##Input rate
        xpath = Util.GetXpath({"locate":"rate_charged_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rate, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputRateCharged -> ' + rate)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemPriceCap(arg):
        '''
        InputItemPriceCap : Input item price cap of Commission fee rule
                Input argu :
                    item_price_cap - item price cap to be set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_price_cap = arg["item_price_cap"]

        ##Input item cap
        xpath = Util.GetXpath({"locate":"item_price_cap_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":item_price_cap, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputItemPriceCap -> ' + item_price_cap)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayName(arg):
        '''
        InputDisplayName : Input display rule name of Commission fee rule
                Input argu :
                    display_name - display rule name to be set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        display_name = arg["display_name"]

        ##Input display name
        xpath = Util.GetXpath({"locate":"display_name_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":display_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputDisplayName -> ' + display_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRedirectLink(arg):
        '''
        InputRedirectLink : Input redirect link of Commission fee rule
                Input argu :
                    redirect_link - redirect link to be set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        redirect_link = arg["redirect_link"]

        ##Input url
        xpath = Util.GetXpath({"locate":"redirect_link_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":redirect_link, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputRedirectLink -> ' + redirect_link)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShopCategory(arg):
        '''
        SelectShopCategory : Select shop category of Commission fee rule
                Input argu :
                    shop_category - local_c2c / local_b2c / cb
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_category = arg["shop_category"]

        ##Click shop category field
        xpath = Util.GetXpath({"locate":"shop_category_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop category dropdown field", "result": "1"})

        ##Select shop category
        xpath = Util.GetXpath({"locate":shop_category})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop category dropdown option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectShopCategory -> ' + shop_category)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUploadShopType(arg):
        '''
        SelectUploadShopType : Select upload shop type of Commission fee rule
                Input argu :
                    upload_shop - shop_id/default/user_tag
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_shop = arg["upload_shop"]

        ##Select shop upload type
        xpath = Util.GetXpath({"locate":upload_shop})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload shop type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectUploadShopType -> ' + upload_shop)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUploadShopAction(arg):
        '''
        SelectUploadShopAction : Select upload shop type of Commission fee rule
                Input argu :
                    action - add_shops/remove_shops
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Click action dropdown list
        xpath = Util.GetXpath({"locate":"action_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action dropdown", "result": "1"})

        ##Select action
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload shop action", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectUploadShopAction -> ' + action)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShopIDFile(arg):
        '''
        UploadShopIDFile : Upload shop id file of Commission fee rule
                Input argu :
                    file_name - file name to be uploaded
                    file_type - upload file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload csv file
        xpath = Util.GetXpath({"locate": "input_file_path"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name":file_name, "file_type":file_type, "result":"1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.UploadShopIDFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectOrderCountMode(arg):
        '''
        SelectOrderCountMode : Select order count mode of Commission fee rule
                Input argu :
                    mode - mode_1 / mode_2 / mode_3
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mode = arg["mode"]

        ##Select order count mode
        xpath = Util.GetXpath({"locate":mode})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select order count mode", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectOrderCountMode -> ' + mode)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOrderCountInfo(arg):
        '''
        InputOrderCountInfo : Input order count bound of Commission fee rule
                Input argu :
                    lower - lower bound
                    upper - upper bound
                    past_days - order count in past days
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        lower = arg["lower"]
        upper = arg["upper"]
        past_days = arg["past_days"]

        ##Input lower bound
        xpath = Util.GetXpath({"locate":"lower_bound_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":lower, "result": "1"})

        ##Input upper bound
        xpath = Util.GetXpath({"locate":"upper_bound_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":upper, "result": "1"})

        ##Input past days
        xpath = Util.GetXpath({"locate":"past_days_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":past_days, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputOrderCountInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCategoryFile(arg):
        '''
        UploadCategoryFile : Upload shop id file of Commission fee rule
                Input argu :
                    file_name - file name to be uploaded
                    cat_type - local / global
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        cat_type = arg["cat_type"]

        ##Upload category file
        xpath = Util.GetXpath({"locate": cat_type})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name":file_name, "file_type":"csv", "result":"1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.UploadCategoryFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectSellerType(arg):
        '''
        SelectSellerType : Select seller type of user tag
                Input argu :
                    seller_type - normal / preferred / mall
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_type = arg["seller_type"]

        ##Click seller type dropdown
        xpath = Util.GetXpath({"locate":"seller_type_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller type dropdown", "result": "1"})

        ##Select seller type
        xpath = Util.GetXpath({"locate":seller_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectSellerType -> ' + seller_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserTag(arg):
        '''
        InputUserTag : Input user tag of Commission fee rule
                Input argu :
                    user_tag - user tag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_tag = arg["user_tag"]

        ##Input user tag
        xpath = Util.GetXpath({"locate":"user_tag_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":user_tag, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.InputUserTag -> ' + user_tag)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUserTagIncludeType(arg):
        '''
        SelectUserTagIncludeType : Select include or exclude for user tag
                Input argu :
                    include_type - include / exclude
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        include_type = arg["include_type"]

        ##Select user tag include type
        xpath = Util.GetXpath({"locate":include_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click include type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectUserTagIncludeType -> ' + include_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRuleID(arg):
        '''
        GetRuleID : Get commission fee rule ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get commission fee rule ID
        xpath = Util.GetXpath({"locate":"rule_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"commission_fee_rule_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.GetRuleID -> ' + GlobalAdapter.CommissionFeeE2EVar._RuleID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetGroupID(arg):
        '''
        GetGroupID : Get commission fee group ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get commission fee group ID
        xpath = Util.GetXpath({"locate":"group_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"commission_fee_group_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.GetGroupID -> ' + GlobalAdapter.CommissionFeeE2EVar._GroupID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetRuleTime(arg):
        '''
        SetRuleTime : Set commission fee rule start time (unix time) in DB
                Input argu :
                    db_file - db file name
                    time_type - start_time / end_time
                    time_delta - time delta by minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = arg["db_file"]
        time_type = arg["time_type"]
        time_delta = int(arg["time_delta"])

        ##If rule is input by user, use the input value
        if "rule_id" in arg:
            rule_id = int(arg["rule_id"])
        else:
            ##Use reserved rule id
            rule_id = int(GlobalAdapter.CommissionFeeE2EVar._RuleID_)

        ##Set unix time by date
        setting_datetime = datetime.date.today() + datetime.timedelta(days=time_delta, minutes=0, seconds=0)
        if time_type == "end_time":
            setting_datetime = setting_datetime + datetime.timedelta(days=time_delta, minutes=0, seconds=-1)
        unix_time = time.mktime(setting_datetime.timetuple())
        dumplogger.info("time to set: %d " % (unix_time))

        ##Assign start time, end time and rule id to global first
        GlobalAdapter.CommonVar._DynamicCaseData_[time_type] = unix_time
        GlobalAdapter.CommonVar._DynamicCaseData_["rule_id"] = rule_id

        ##Define SQL assign list
        assign_list = [{"column": time_type, "value_type": "number"}, {"column": "rule_id", "value_type": "number"}]

        ##Send SQL command
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_servicefee", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SetRuleTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchRule(arg):
        '''
        SearchRule : Set commission fee rule time (unix time) to 00:00 A.M. in DB
                Input argu :
                    input_type - rule_id/group_id/rule_name
                    search_target - the string to input in search field
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]

        ##If search target is not input by user, use the reserved value from previous step
        if input_type == "rule_id" and "search_target" not in arg:
            search_target = GlobalAdapter.CommissionFeeE2EVar._RuleID_
        elif input_type == "group_id" and "search_target" not in arg:
            search_target = GlobalAdapter.CommissionFeeE2EVar._GroupID_
        else:
            search_target = arg["search_target"]

        dumplogger.info("The string to search : %s" % (search_target))

        ##Input search target and click enter
        xpath = Util.GetXpath({"locate": input_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": search_target, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SearchRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectSearchStatus(arg):
        '''
        SelectSearchStatus : Set commission fee rule time (unix time) to 00:00 A.M. in DB
                Input argu :
                    status - the status to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click status input field
        xpath = Util.GetXpath({"locate":"input_field"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click status input field", "result": "1"})

        ##Select status
        xpath = Util.GetXpath({"locate":status})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select status -> %s" % (status), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SelectSearchStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchSeller(arg):
        '''
        SearchSeller : Search for seller in seller view
                Input argu :
                    shop_id - shop id to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Input seller id and click enter
        xpath = Util.GetXpath({"locate": "shop_id_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        ##Click the seller id
        xpath = Util.GetXpath({"locate": "shop_id_result"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first search result", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.SearchSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRuleDetail(arg):
        '''
        ClickRuleDetail : Set commission fee rule time (unix time) to 00:00 A.M. in DB
                Input argu :
                    value - the rule value to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value = arg["value"]
        if not value:
            value = GlobalAdapter.CommissionFeeE2EVar._RuleID_

        ##Click rule id link
        xpath = Util.GetXpath({"locate":"rule_search_result"})
        xpath = xpath.replace("value_to_be_replaced", value)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click rule id link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.ClickRuleDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckUploadShopResultFile(arg):
        '''
        CheckUploadShopResultFile : Check upload shop result file
                Input argu :
                    file_name - filename to check
                    row_number - the row to check
                    shop_id - check shop id
                    is_existed - 1 for positive check, 0 for negative check
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        row_number = arg['row_number']
        shop_id = arg['shop_id']
        is_existed = int(arg["is_exist"])

        ##Check if shop id existed in target row
        if is_existed:
            XtFunc.CheckDownloadCsvContent({"file_name": file_name, "check_content": shop_id, "check_row_position": row_number, "isfuzzy": "1", "result": "1"})
        else:
            ##Check if shop id is not existed in target row
            XtFunc.CheckDownloadCsvContent({"file_name": file_name, "check_content": shop_id, "check_row_position": row_number, "isfuzzy": "1", "result": "0"})

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.CheckUploadShopResultFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRuleReadyStatus(arg):
        '''
        CheckRuleReadyStatus : Check if the rule is ready after created
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        xpath = Util.GetXpath({"locate":"disable_edit_button"})
        for retry_count in range(1, 8):
            ##Check if edit button is still disabled in rule page
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Waiting for edit button become valid retry %d times" % (retry_count))
                time.sleep(7)
                BaseUILogic.BrowserRefresh({"message": "Refresh rule page", "result": "1"})
                time.sleep(5)
            else:
                ret = 1
                break

        OK(ret, int(arg['result']), 'AdminCommissionFeePage.CheckRuleReadyStatus')


class AdminCommissionFeeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewRule(arg):
        '''
        ClickNewRule : Click new rule in commission fee page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click new rule button
        xpath = Util.GetXpath({"locate": "new_rule_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreate(arg):
        '''
        ClickCreate : Click create in commission fee new rule page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create button
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickCreate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK after commission fee new rule is successfully created
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in rule detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerView(arg):
        '''
        ClickSellerView : Click seller view in rule list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click seller view button
        xpath = Util.GetXpath({"locate": "seller_view_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickSellerView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRuleView(arg):
        '''
        ClickRuleView : Click rule view in rule list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click rule view button
        xpath = Util.GetXpath({"locate": "rule_view_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickRuleView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewType(arg):
        '''
        ClickViewType : Click view type in rule list page
                Input argu :
                    view_type - simplified/expanded
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        view_type = arg["view_type"]

        ##Click view type button
        xpath = Util.GetXpath({"locate": view_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickViewType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadShopID(arg):
        '''
        ClickUploadShopID : Click Upload ShopID button in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_shop_id_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Upload ShopID button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickUploadShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click rule tab in rule list page
                Input argu :
                    tab - local/cross_border
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click rule tab
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete button
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickYes(arg):
        '''
        ClickYes : Click yes button after click delete in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click yes button
        xpath = Util.GetXpath({"locate": "yes_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click yes button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickYes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNo(arg):
        '''
        ClickNo : Click no button after click delete in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click no button
        xpath = Util.GetXpath({"locate": "no_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click no button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickNo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownload(arg):
        '''
        ClickDownload : Click download button in upload shop id page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download button
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickDownload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetFailedResult(arg):
        '''
        ClickGetFailedResult : Click download failed result icon pop up window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click get failed result icon
        xpath = Util.GetXpath({"locate": "get_failed_result_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click get failed result icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickGetFailedResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export button in rule page
                Input argu :
                    export_type - shop_id / local_category / global_category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        export_type = arg['export_type']

        ##Click export button
        xpath = Util.GetXpath({"locate": export_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        '''
        ClickUpdate : Click update button in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click update button
        xpath = Util.GetXpath({"locate": "update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click next page of the seller view page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next page button
        xpath = Util.GetXpath({"locate": "next_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLoadMore(arg):
        '''
        ClickLoadMore : Click load more of the seller view page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click load more button
        xpath = Util.GetXpath({"locate": "load_more_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click load more button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickLoadMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserTagSync(arg):
        '''
        ClickUserTagSync : Click User Tag Sync button in rule list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click user tag sync button
        xpath = Util.GetXpath({"locate": "user_tag_sync_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user tag sync btn button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickUserTagSync')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddUserTag(arg):
        '''
        ClickAddUserTag : Click add more user tag input field in rule edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add user tag button
        xpath = Util.GetXpath({"locate":"add_user_tag_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add user tag", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeButton.ClickAddUserTag')


class AdminCommissionFeeComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnLabel(arg):
        '''
        ClickOnLabel : Click any type of label with well defined locator
            Input argu :
                label_type - type of label which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        label_type = arg["label_type"]

        ##Click on label
        xpath = Util.GetXpath({"locate": label_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click label: %s, which xpath is %s" % (label_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeComponent.ClickOnLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column (for example, rule_name / rate_charged, etc.)
                input_content - the content to input
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCommissionFeeComponent.InputToColumn')


class AdminPaymentComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any button relate to payment
            Input argu :
                button_type - type of button relate to payment
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column which defined by caller
                input_content - the content to input
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Click on column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s column" % (column_type), "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "message": "Input %s which xpath is %s, input content is %s" % (column_type, xpath, input_content), "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentComponent.InputToColumn')


class AdminPaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddPaymentMaintenance(arg):
        '''
        AddPaymentMaintenance : Add Payment Maintenance
                Input argu :
                    channel - channel ID
                    comment - comment msg
                    message - message
                    message_localised - message localised
                    start_time - start time delta (minutes) you want to input
                    end_time - end time delta (minutes) you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel = arg["channel"]
        comment = arg["comment"]
        message = arg["message"]
        message_localised = arg["message_localised"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input info that needs it
        xpath = Util.GetXpath({"locate": "add_maintenance"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Add Maintenance", "result": "1"})
        xpath = Util.GetXpath({"locate": "channel_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment channel dropdown", "result": "1"})
        xpath = Util.GetXpath({"locate": "channel_section"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click channel section", "result": "1"})
        xpath = Util.GetXpath({"locate": "message_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": message, "result": "1"})
        xpath = Util.GetXpath({"locate": "message_localised_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": message_localised, "result": "1"})
        xpath = Util.GetXpath({"locate": "comment_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": comment, "result": "1"})

        ##Input start time field (Need to click element first to active input field)
        start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S %z", minutes=int(start_time))
        start_time_string = start_time_string[:-2] + ":" + start_time_string[-2:]
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_string, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input end time field (Need to click element first to active input field)
        end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S %z", minutes=int(end_time))
        end_time_string = end_time_string[:-2] + ":" + end_time_string[-2:]
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time_string, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentPage.AddPaymentMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeactivatePaymentMaintenance(arg):
        '''
        DeactivatePaymentMaintenance : Deactivate Payment Maintenance
                Input argu :
                    channel - channel ID you wanna deactivate
                    comment - comment msg
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel = arg["channel"]
        comment = arg["comment"]

        xpath = Util.GetXpath({"locate": "deactivate_section"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click deactivate section", "result": "1"})
        time.sleep(3)

        xpath = Util.GetXpath({"locate": "comment_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": comment, "result": "1"})

        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentPage.DeactivatePaymentMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddIDToList(arg):
        '''
        AddIDToList : import/remove seller/buyer to payment whitelist/blacklist
                Input argu :
                    action - import/remove
                    channel - payment channel name
                    file_name - csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        channel = arg["channel"]
        file_name = arg['file_name']

        ##Select channel
        xpath = Util.GetXpath({"locate": "channel_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment channel dropdown", "result": "1"})
        xpath = Util.GetXpath({"locate": "channel_section"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click channel section", "result": "1"})

        ##Choose action type
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s button" % (action), "result": "1"})

        ##Upload csv file
        xpath = Util.GetXpath({"locate": "choose_file"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        ##Click confirm
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit btn", "result": "1"})

        ##Reload log table
        xpath = Util.GetXpath({"locate": "reload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reload btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentPage.AddIDToList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterList(arg):
        '''
        FilterList : input specific content in filter
                Input argu :
                    target_id - shopid/userid
                    channel_name - channel name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        target_id = arg["target_id"]
        channel_name = arg["channel_name"]

        if target_id:
            ##Input shop id
            xpath = Util.GetXpath({"locate": "id_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": target_id, "message": "Input shop/user id", "result": "1"})

        if channel_name:
            ##Select channel
            xpath = Util.GetXpath({"locate": "channel_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment channel dropdown", "result": "1"})
            xpath = Util.GetXpath({"locate": "channel_section"})
            xpath = xpath.replace("channel_to_be_replaced", channel_name)
            BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click channel section", "result": "1"})

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentPage.FilterList')


class AdminCustomTaxPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchUploadOrderIDs(arg):
        '''
        BatchUploadOrderIDs : Input rule name of commission fee rule
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]

        ##Get upload csv input field
        xpath = Util.GetXpath({"locate": "upload_csv_input_field"})

        ##Upload shops id csv file
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate":xpath, "element": "", "element_type": "xpath", "project": Config._TestCaseFeature_, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCustomTaxPage.BatchUploadOrderIDs')


class AdminCustomTaxButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click function tab on the top of the page
                Input argu :
                    tab_type - tax_counter, mark_orderid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click function tab
        xpath = Util.GetXpath({"locate": tab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + tab_type + " tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCustomTaxButton.ClickTab')
