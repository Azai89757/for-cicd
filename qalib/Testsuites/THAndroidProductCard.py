import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class THAndroidProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidProductCard Precondition =="
        dumplogger.info("== Setup THAndroidProductCard Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"th"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_THAndroidProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-01.xml")

    def test_THAndroidProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-02.xml")

    def test_THAndroidProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-03.xml")

    def test_THAndroidProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-04.xml")

    def test_THAndroidProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-05.xml")

    def test_THAndroidProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-06.xml")

    def test_THAndroidProductCard07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidProductCard-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidProductCard Post condition =="
        dumplogger.info("== Setup THAndroidProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
