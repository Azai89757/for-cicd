import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpAddOnDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpAddOnDeal Precondition =="
        dumplogger.info("== Setup TWHttpAddOnDeal Precondition ==")

    def test_TWHttpAddOnDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-01.xml")

    def test_TWHttpAddOnDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-02.xml")

    def test_TWHttpAddOnDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-03.xml")

    def test_TWHttpAddOnDeal04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-04.xml")

    def test_TWHttpAddOnDeal05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-05.xml")

    def test_TWHttpAddOnDeal06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-06.xml")

    def test_TWHttpAddOnDeal07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-07.xml")

    def test_TWHttpAddOnDeal08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-08.xml")

    def test_TWHttpAddOnDeal09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-09.xml")

    def test_TWHttpAddOnDeal10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDeal-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpAddOnDeal Post condition =="
        dumplogger.info("== Setup TWHttpAddOnDeal Post condition ==")
