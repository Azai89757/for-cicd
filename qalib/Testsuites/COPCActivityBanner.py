import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class COPCActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCActivityBanner Precondition =="
        dumplogger.info("== Setup COPCActivityBanner Precondition ==")

    def test_COPCActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-01.xml")

    def test_COPCActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-02.xml")

    def test_COPCActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-03.xml")

    def test_COPCActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-04.xml")

    def test_COPCActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-05.xml")

    def test_COPCActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-06.xml")

    def test_COPCActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-07.xml")

    def test_COPCActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-08.xml")

    def test_COPCActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCActivityBanner-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCActivityBanner Post condition =="
        dumplogger.info("== Setup COPCActivityBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
