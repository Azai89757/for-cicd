import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDHttpSTSRefactor(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSTSRefactor Precondition =="
        dumplogger.info("== Setup IDHttpSTSRefactor Precondition ==")
    '''
    def test_IDHttpSTSRefactor01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-01.xml")
    '''

    def test_IDHttpSTSRefactor02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-02.xml")

    def test_IDHttpSTSRefactor03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-03.xml")

    def test_IDHttpSTSRefactor04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-04.xml")

    def test_IDHttpSTSRefactor05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-05.xml")

    def test_IDHttpSTSRefactor06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-06.xml")

    def test_IDHttpSTSRefactor07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-07.xml")

    def test_IDHttpSTSRefactor08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-08.xml")

    def test_IDHttpSTSRefactor09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-09.xml")

    def test_IDHttpSTSRefactor10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-10.xml")

    def test_IDHttpSTSRefactor11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-11.xml")

    def test_IDHttpSTSRefactor12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-12.xml")

    def test_IDHttpSTSRefactor13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTSRefactor-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSTSRefactor Post condition =="
        dumplogger.info("== Setup IDHttpSTSRefactor Post condition ==")
