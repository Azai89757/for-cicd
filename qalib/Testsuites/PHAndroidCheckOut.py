import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class PHAndroidCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAndroidCheckOut Precondition =="
        dumplogger.info("== Setup PHAndroidCheckOut Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"ph"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    # def test_PHAndroidCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHAndroidCheckOut-01.xml")

    # def test_PHAndroidCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHAndroidCheckOut-02.xml")

    # def test_PHAndroidCheckOut03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHAndroidCheckOut-03.xml")

    # def test_PHAndroidCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHAndroidCheckOut-04.xml")

    # def test_PHAndroidCheckOut05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHAndroidCheckOut-05.xml")

    # def test_PHAndroidCheckOut06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHAndroidCheckOut-06.xml")

    def test_PHAndroidCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidCheckOut-07.xml")

    def test_PHAndroidCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidCheckOut-08.xml")

    def test_PHAndroidCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidCheckOut-09.xml")

    def test_PHAndroidCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidCheckOut-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAndroidCheckOut Post condition =="
        dumplogger.info("== Setup PHAndroidCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
