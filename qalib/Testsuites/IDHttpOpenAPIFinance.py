import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpOpenAPIFinance(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpOpenAPIFinance Precondition =="
        dumplogger.info("== Setup IDHttpOpenAPIFinance Precondition ==")

    def test_IDHttpOpenAPIFinance01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpOpenAPIFinance-01.xml")

    def test_IDHttpOpenAPIFinance02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpOpenAPIFinance-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpOpenAPIFinance Post condition =="
        dumplogger.info("== Setup IDHttpOpenAPIFinance Post condition ==")
