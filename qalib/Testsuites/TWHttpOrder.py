import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpOrder Precondition =="
        dumplogger.info("== Setup TWHttpOrder Precondition ==")

    def test_TWHttpOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-01.xml")

    def test_TWHttpOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-02.xml")

    def test_TWHttpOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-03.xml")

    def test_TWHttpOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-04.xml")

    def test_TWHttpOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-05.xml")

    def test_TWHttpOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-06.xml")

    def test_TWHttpOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-07.xml")

    def test_TWHttpOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-08.xml")

    def test_TWHttpOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-09.xml")

    def test_TWHttpOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-10.xml")

    def test_TWHttpOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-11.xml")

    def test_TWHttpOrder12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-12.xml")

    def test_TWHttpOrder13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-13.xml")

    def test_TWHttpOrder14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-14.xml")

    def test_TWHttpOrder15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOrder-15.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpOrder Post condition =="
        dumplogger.info("== Setup TWHttpOrder Post condition ==")
