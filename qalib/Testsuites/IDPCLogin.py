import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCLogin Precondition =="
        dumplogger.info("== Setup IDPCLogin Precondition ==")

    def test_IDPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCLogin-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCLogin Post condition =="
        dumplogger.info("== Setup IDPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
