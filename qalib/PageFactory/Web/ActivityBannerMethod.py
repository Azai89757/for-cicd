#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ActivityBannerMethod.py: The def of this file called by XML mainly.
'''
##import python library
import re
import time
import datetime

##Import framework common library
import Util
import XtFunc
import Config
import ConfigParser
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic
import CommonMethod
import LoginSignupMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class ActivityBanner:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShopeeLaunchWebSite(arg):
        '''
        ShopeeLaunchWebSite : Launch shopee website only (for detect pop up banner)
                Input argu : banner_type - home_popup / category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        banner_type = arg["banner_type"]

        url = "https://" + GlobalAdapter.UrlVar._Domain_
        dumplogger.info("url = %s" % (url))

        ##Go to url
        BaseUICore.GotoURL({"url":url, "result": "1"})

        if banner_type == "home_popup":
            ##Choose language when entering FE
            if Config._TestCaseRegion_ in ("TH", "MY", "IN"):
                xpath = Util.GetXpath({"locate": "choose_language"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose language", "result": "1"})
            elif Config._TestCaseRegion_ in ("PL", "ES", "FR"):
                xpath = Util.GetXpath({"locate": "cookies_btn"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close cookies message", "result": "1"})

        OK(ret, int(arg['result']), 'ActivityBanner.ShopeeLaunchWebSite')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBannerExist(arg):
        '''
        CheckBannerExist : Check banner exist or not
                Input argu :
                    banner_type - category / floating / skinny / home_popup / mall_popup / mall_banner / home_carousel / mall_carousel / digital_product_carousel / digital_product_scrolling / home_square
                    image - OpenCV image to upload, if want to upload mutiliple images to check GIF, use "," to split image name
                    account  - login account, if account is none, then we won't login
                    password - login password, if account is none, then we won't login

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        image = arg["image"]
        account = arg["account"]
        password = arg["password"]

        ##Check banner 13 times to confirm banner display normally
        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close browser and re-open, then launch to Shopee homepage to check banner dispaly
            time.sleep(15)
            BaseUICore.DeInitialWebDriver({})
            BaseUICore.InitialWebDriver({"browser":"chrome","result": "1"})
            time.sleep(15)

            ##Launch shopee FE and go to cooresponding banner page
            ActivityBanner.LaunchShopeeSiteAndGoToBannerPage({"banner_type":banner_type, "account":account, "password":password, "result": "1"})

            ##Check whether if there is mutiliple image
            image_split_result = image.split(",")

            ##Start to begin image compare by different file types
            if len(image_split_result) > 1:
                ##Means to use GIF compare function
                match_result = XtFunc.MatchGIFImg({"mode":"web", "image1": image_split_result[0], "image2": image_split_result[1], "env": Config._EnvType_, "expected_result":arg['result']})
            else:
                ##Means to use Picture compare function
                match_result = XtFunc.MatchPictureImg({"mode":"web", "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("PC Banner image/GIF matching successful at %d run!!!!" % (retry_count))
                break
            else:
                dumplogger.info("PC Banner image/GIF matching failed at %d run..." % (retry_count))
                time.sleep(20)

        OK(ret, match_result, 'ActivityBanner.CheckBannerExist -> ' + banner_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBannerQuota(arg):
        '''
        CheckBannerQuota : Check banner quota
                Input argu :
                    image - OpenCV project file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]

        ##Retry to click img
        for retry_count in range(1, 7):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Means to use Picture compare function
            match_result = XtFunc.MatchPictureImg({"mode": "web", "image": image, "env": Config._EnvType_, "threshold": "0.9", "expected_result": arg['result']})

            ##If banner exist and click it
            if match_result:
                time.sleep(5)
                XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": "web", "image": image, "threshold": "0.9", "is_click": "yes", "result": "1"})
                dumplogger.info("Click Banner successful at %d run!!!!" % (retry_count))
                break
            else:
                time.sleep(3)
                dumplogger.info("Click Banner image/GIF failed at %d run..." % (retry_count))

        OK(ret, match_result, 'ActivityBanner.CheckBannerQuota')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LaunchShopeeSiteAndGoToBannerPage(arg):
        '''
        LaunchShopeeSiteAndGoToBannerPage : Go to each banner page from homepage before verifying banner image
                Input argu :
                    banner_type - category / floating / skinny / home_popup / mall_popup / mall_banner / home_carousel / mall_carousel / digital_product_carousel / digital_product_scrolling / home_square
                    account  - login account, if account is none, then we won't login
                    password - login password, if password is none, then we won't login
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        account = arg["account"]
        password = arg["password"]

        category = {
            "vn": "category_banner-cat.11000255",
            "ph": "category_banner-cat.11000020",
            "th": "category_banner-cat.11000021",
            "mx": "category_banner-cat.21124985",
            "br": "category_banner-cat.36009",
            "my": "category_banner-cat.21125193",
            "sg": "category_banner-cat.21125273",
            "cl": "category_banner-cat.21126004",
            "in": "category_banner-cat.21126473",
            "fr": "category_banner-cat.21126505",
            "co": "category_banner-cat.21126003",
            "ar": "category_banner-cat.21126928",
            "es": "category_banner-cat.21126664",
            "pl": "category_banner-cat.21126063",
            "tw": "category_banner-cat.21127572",
            "id": "category_banner-cat.21127587"
        }

        if banner_type in ("home_popup", "category", "home_carousel"):
            if account:
                CommonMethod.ShopeeLaunchWebSite({"result": "1"})
                LoginSignupMethod.ShopeeLogin({"account": account, "password": password, "logintype": "account", "result": "1"})
            else:
                ##Launch shopee website by different banner
                if banner_type == "home_popup":
                    ActivityBanner.ShopeeLaunchWebSite({"banner_type": "home_popup", "result": "1"})
                elif banner_type in ("category", "home_carousel"):
                    CommonMethod.ShopeeLaunchWebSite({"result": "1"})

            if banner_type == "category":
                url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/" + category[Config._TestCaseRegion_.lower()]
                BaseUICore.GotoURL({"url": url, "result": "1"})
                BaseUILogic.BrowserRefresh({"message": "Refresh category page", "result": "1"})
                time.sleep(5)
                dumplogger.info("Banner type %s => Proceed to verify via openCV" % (banner_type))
        else:
            CommonMethod.ShopeeLaunchWebSite({"result": "1"})
            if account:
                LoginSignupMethod.ShopeeLogin({"account": account, "password": password, "logintype": "account", "result": "1"})

            ##landing page banner / floating banner => re-open shopee web site
            if banner_type in ("floating", "home_square", "welcomepackage_ertrance_banner"):
                dumplogger.info("Banner type %s => Proceed to verify via openCV" % (banner_type))

            ##official shop banner => re-open shopee web site and click official shop icon
            elif banner_type in ("mall_carousel", "mall_popup"):
                dumplogger.info("Banner type %s => Click more button first then verify via openCV" % (banner_type))

                url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/mall"
                BaseUICore.GotoURL({"url":url, "result": "1"})
                time.sleep(5)

                if banner_type == "mall_carousel":
                    BaseUILogic.BrowserRefresh({"message":"Refresh mall page", "result": "1"})
                ##Mall popup need to wait longer
                else:
                    time.sleep(10)

            ##skinny banner => re-open shopee web site
            elif banner_type == "skinny":
                dumplogger.info("Banner type %s => Scroll first then verify via openCV" % (banner_type))
                time.sleep(10)
                CommonMethod.ScrollToSection({"section":"skinny_banner_section", "result": "1"})

            ##mall banner => re-open shopee web site and click official shop icon
            elif banner_type in ("mall_banner", "mall_shop_banner"):
                dumplogger.info("Banner type %s => Scroll first then verify via openCV" % (banner_type))
                BaseUILogic.MouseScrollEvent({"x": "100", "y": "3500", "result": "1"})
                time.sleep(5)
                ##If xpath exists
                xpath = Util.GetXpath({"locate": "shopee_mall"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "product_card", "result": "1"})
                    BaseUILogic.MouseScrollEvent({"x":str(GlobalAdapter.GeneralE2EVar._LocationX_), "y":str(GlobalAdapter.GeneralE2EVar._LocationY_-300), "result": "1"})
                else:
                    dumplogger.info("Unable to get product name xpath.")
                    ret = 0

        OK(ret, int(arg['result']), 'ActivityBanner.LaunchShopeeSiteAndGoToBannerPage -> ' + banner_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryBanner(arg):
        '''
        ClickCategoryBanner : Click category banner
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click category banner by javascript
        BaseUICore.ExecuteScript({"script": "document.elementFromPoint(1300,150).shadowRoot.querySelector('shopee-banner-simple').shadowRoot.querySelector('a').click()", "result": "1"})

        OK(ret, int(arg['result']), 'ActivityBanner.ClickCategoryBanner')
