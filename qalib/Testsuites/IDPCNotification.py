import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCNotification(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCNotification Precondition =="
        dumplogger.info("== Setup IDPCNotification Precondition ==")

    def test_IDPCNotification01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-01.xml")

    def test_IDPCNotification02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-02.xml")

    def test_IDPCNotification03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-03.xml")

    def test_IDPCNotification04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-04.xml")

    def test_IDPCNotification05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-05.xml")

    def test_IDPCNotification06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-06.xml")

    def test_IDPCNotification07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-07.xml")

    def test_IDPCNotification08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-08.xml")

    def test_IDPCNotification09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-09.xml")

    def test_IDPCNotification10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-10.xml")

    def test_IDPCNotification11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCNotification-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCNotification Post condition =="
        dumplogger.info("== Setup IDPCNotification Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
