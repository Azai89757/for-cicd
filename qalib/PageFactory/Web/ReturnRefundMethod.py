﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 OrderMethod.py: The def of this file called by XML mainly.
'''
##import system library
import time
import datetime
import re

##Import framework common library
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import DecoratorHelper
import GlobalAdapter

##Import web library
import BaseUICore
import BaseUILogic
import OrderMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class ReturnRefundButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        ''' ClickSubmit : Click submit return refund button in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click submit return refund button in R/R detail page
        xpath = Util.GetXpath({"locate":"click_submit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit return refund button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm return refund button in R/R detail page
                Input argu :
                    button_type - confirm_return_refund / confirm_shipping / confirm_cancel_return_refund / confirm_question / confirm_pickup / confirm_continue_with_return
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click confirm return refund button in R/R detail page
        xpath = Util.GetXpath({"locate":button_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm return refund button in R/R detail page => %s" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisitShop(arg):
        ''' ClickVisitShop : Click visit shop button in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click visit shop button in R/R detail page
        xpath = Util.GetXpath({"locate":"click_visit_shop"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click visit shop button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickVisitShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel button in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click cancel button in R/R detail page
        xpath = Util.GetXpath({"locate":"click_cancel"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDiscuss(arg):
        ''' ClickDiscuss : Click discuss button in R/R detail page
                Input argu :
                        button_type - chat / history
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click discuss button in R/R detail page
        xpath = Util.GetXpath({"locate":button_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click discuss button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickDiscuss')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseReason(arg):
        ''' ClickChooseReason : Click choose return refund reason in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click choose return refund reason in R/R detail page
        xpath = Util.GetXpath({"locate":"choose_reason_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose return refund reason in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickChooseReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReasonOption(arg):
        '''
        ClickReasonOption : Click return refund reason option
                Input argu :
                    reason_option - non_received_items / received_wrong_product / not_match_description / incomplete_product / physical_damage
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason_option = arg["reason_option"]

        ##Switch to return refund reason frame
        return_reason_frame = Util.GetXpath({"locate":"return_reason_frame"})
        BaseUICore.SwitchToFrame({"frame":return_reason_frame, "result": "1"})

        ##Click return refund reason option
        xpath = Util.GetXpath({"locate":reason_option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return refund reason option => %s" % (reason_option), "result": "1"})

        ##Switch to original frame
        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickReasonOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseSolution(arg):
        ''' ClickChooseSolution : Click return refund solution in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click choose return refund solution in R/R detail page
        xpath = Util.GetXpath({"locate":"solution_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose return refund solution in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickChooseSolution')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSolutionOption(arg):
        ''' ClickSolutionOption : Click return refund solution option
                Input argu : solution_option - refund_only / return_and_refund
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        solution_option = arg["solution_option"]

        ##Switch to return refund solution frame
        return_solution_frame = Util.GetXpath({"locate":"return_solution_frame"})
        BaseUICore.SwitchToFrame({"frame":return_solution_frame, "result": "1"})

        ##Click return refund solution option
        xpath = Util.GetXpath({"locate":solution_option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return refund solution option => %s" % (solution_option), "result": "1"})

        ##Switch to original frame
        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickSolutionOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseShipping(arg):
        ''' ClickChooseShipping : Click return refund shipping in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click choose return refund shipping in R/R detail page
        xpath = Util.GetXpath({"locate":"shipping_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose return refund shipping in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickChooseShipping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingOption(arg):
        '''
        ClickShippingOption : Click return refund shipping option
                Input argu :
                    shipping_option - self_arrange / drop_off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping_option = arg["shipping_option"]

        ##Switch to return refund shipping frame
        return_shipping_frame = Util.GetXpath({"locate":"return_shipping_frame"})
        BaseUICore.SwitchToFrame({"frame":return_shipping_frame, "result": "1"})

        ##Click return refund shipping option
        xpath = Util.GetXpath({"locate":shipping_option})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return refund shipping option => %s" % (shipping_option), "result": "1"})

        ##Switch to original frame
        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickShippingOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickQuestionMark(arg):
        ''' ClickQuestionMark : Click question mark in request R/R page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click question mark in request R/R page
        xpath = Util.GetXpath({"locate":"question_mark_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click question mark in request R/R page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickQuestionMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewExample(arg):
        ''' ClickViewExample : Click view example in request R/R page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click view example in request R/R page
        xpath = Util.GetXpath({"locate":"view_example_link"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click view example in request R/R page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickViewExample')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLearnMore(arg):
        ''' ClickLearnMore : Click learn more in request R/R page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click learn more in request R/R page
        xpath = Util.GetXpath({"locate":"learn_more_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click learn more in request R/R page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickLearnMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChoosePickupTime(arg):
        '''
        ClickChoosePickupTime : Click choose pickup time in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose pickup time in R/R detail page
        xpath = Util.GetXpath({"locate":"pickup_time_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose pickup time in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickChoosePickupTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPickupTimeOption(arg):
        '''
        ClickPickupTimeOption : Click pickup time option
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Switch to pickup time option frame
        pickup_time_frame = Util.GetXpath({"locate":"pickup_time_frame"})
        BaseUICore.SwitchToFrame({"frame":pickup_time_frame, "result": "1"})

        ##Click pickup time option
        xpath = Util.GetXpath({"locate":"pickup_time_option"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click pickup time option", "result": "1"})

        ##Switch to original frame
        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickPickupTimeOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinueWithReturn(arg):
        '''
        ClickContinueWithReturn : Click continue with return in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click continue with return in R/R detail page
        xpath = Util.GetXpath({"locate":"continue_with_return_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click continue with return in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickContinueWithReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHowToReturn(arg):
        '''
        ClickHowToReturn : Click how to return
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click how to return
        xpath = Util.GetXpath({"locate":"how_to_return_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click how to return", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickHowToReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnInstructions(arg):
        '''
        ClickReturnInstructions : Click return instructions in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return instructions in R/R detail page
        xpath = Util.GetXpath({"locate":"return_instructions_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return instructions in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickReturnInstructions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAcceptProposal(arg):
        '''
        ClickAcceptProposal : Click accept proposal in discuss page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click accept proposal in discuss page
        xpath = Util.GetXpath({"locate":"accept_proposal_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click accept proposal", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickAcceptProposal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundTag(arg):
        '''
        ClickReturnRefundTag : Click return refund tag in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return refund tag in R/R detail page
        xpath = Util.GetXpath({"locate":"return_refund_tag"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return refund tag", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickReturnRefundTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpand(arg):
        '''
        ClickExpand : Click expand button in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click expand button in R/R detail page
        xpath = Util.GetXpath({"locate":"expand_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click expand button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundButton.ClickExpand')


class ReturnRefundPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckReturnDate(arg):
        '''CheckReturnDate : Check return valid date
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                              -1 - error
        '''
        ret = 1

        date_format = "%d-%m-%Y"
        ##Get current date
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(date_format), date_format)

        ##Get and check order valid time
        xpath = Util.GetXpath({"locate":"valid_time"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Get date from text
        date_start = re.search(r"\/\d\d\/", GlobalAdapter.CommonVar._PageAttributes_).span()[0] - 2
        date_end = re.search(r"\/\d\d\/", GlobalAdapter.CommonVar._PageAttributes_).span()[1] + 4
        valid_date = datetime.datetime.strptime(GlobalAdapter.CommonVar._PageAttributes_[date_start:date_end], date_format)

        ##Check if DTS > current date
        if valid_date >= current_date:
            dumplogger.info("dts date is bigger than current date!!")
        else:
            ret = 0
            dumplogger.info("dts date is less than current date!!")

        OK(ret, int(arg['result']), 'ReturnRefundPage.CheckReturnDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckReturnSN(arg):
        '''CheckReturnSN : Check return SN
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                              -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"return_sn"})
        xpath = xpath.replace('replace_return_sn', str(GlobalAdapter.OrderE2EVar._ReturnSN_))
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundPage.CheckReturnSN')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RequestReturnRefund(arg):
        '''
        RequestReturnRefund : Request return refund default choose R/R reason "non received items" in R/R request page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose R/R reason
        ReturnRefundButton.ClickChooseReason({"result": "1"})

        ##Sleep 5 second to wait R/R reason frame is opened
        time.sleep(5)

        ##Click R/R reason "non received items"
        ReturnRefundButton.ClickReasonOption({"reason_option": "non_received_items", "result": "1"})

        ##Sleep 5 second to wait R/R reason frame is closed
        time.sleep(5)

        ##Input R/R description
        ReturnRefundPage.InputReturnRefundDescription({"description": "auto test", "result": "1"})

        ##Input R/R email
        ReturnRefundPage.InputReturnRefundEmail({"email": "autotest@shopee.com", "result": "1"})

        ##Click submit R/R
        ReturnRefundButton.ClickSubmit({"result": "1"})

        ##Click confirm R/R
        ReturnRefundButton.ClickConfirm({"button_type": "confirm_return_refund", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundPage.RequestReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImages(arg):
        '''
        UploadImages : In order return refund Page, upload image for return refund
                Input argu :
                    file_name - image file neme
                    file_type - image file type
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload return refund image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "hidden-file-input image-uploader-role__hidden-file-input", "element_type": "class", "project": Config._TestCaseFeature_, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundPage.UploadImages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRefundAmount(arg):
        ''' InputRefundAmount : Input refund amount
                Input argu :
                    amount - amount you want to input
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        amount = arg["amount"]

        ##Input description about return refund reason
        xpath = Util.GetXpath({"locate":"refund_amount_input_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":amount, "result":"1"})

        OK(ret, int(arg['result']), 'ReturnRefundPage.InputRefundAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReturnRefundDescription(arg):
        ''' InputReturnRefundDescription : In Order Return Refund Page , input description for return or refund
                Input argu :
                    input_description - input description
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        description = arg["description"]

        ##Input description content
        xpath = Util.GetXpath({"locate":"description_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":description, "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'ReturnRefundPage.InputReturnRefundDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReturnRefundEmail(arg):
        ''' InputReturnRefundEmail : Input return refund email address
                Input argu :
                    email - email you want to input
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        email = arg["email"]

        ##Input return refund email address
        xpath = Util.GetXpath({"locate":"email_input_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":email, "result":"1"})

        OK(ret, int(arg['result']), 'ReturnRefundPage.InputReturnRefundEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundProductCheckbox(arg):
        ''' ClickReturnRefundProductCheckbox : Click return refund product checkbox
                Input argu :
                    product_name - product name
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click return refund product checkbox
        xpath = Util.GetXpath({"locate":"product_name_checkbox"})
        xpath_product_name_checkbox = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_name_checkbox, "message":"Click return refund product checkbox", "result": "1"})

        ##Record return product name to prepare retry return refund
        GlobalAdapter.OrderE2EVar._ReturnProductNameList_.append(product_name)

        OK(ret, int(arg['result']), 'ReturnRefundPage.ClickReturnRefundProductCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundShopCheckbox(arg):
        ''' ClickReturnRefundShopCheckbox : Click return refund shop checkbox
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click return refund shop checkbox
        xpath = Util.GetXpath({"locate":"shop_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return refund shop checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'ReturnRefundPage.ClickReturnRefundShopCheckbox')
