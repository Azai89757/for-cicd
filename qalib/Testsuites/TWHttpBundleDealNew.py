import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpBundleDealNew(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpBundleDealNew Precondition =="
        dumplogger.info("== Setup TWHttpBundleDealNew Precondition ==")

    def test_TWHttpBundleDealNew01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-01.xml")

    def test_TWHttpBundleDealNew02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-02.xml")

    def test_TWHttpBundleDealNew03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-03.xml")

    def test_TWHttpBundleDealNew04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-04.xml")

    def test_TWHttpBundleDealNew05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-05.xml")

    def test_TWHttpBundleDealNew06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-06.xml")

    def test_TWHttpBundleDealNew07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-07.xml")

    def test_TWHttpBundleDealNew08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDealNew-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpBundleDealNew Post condition =="
        dumplogger.info("== Setup TWHttpBundleDealNew Post condition ==")
