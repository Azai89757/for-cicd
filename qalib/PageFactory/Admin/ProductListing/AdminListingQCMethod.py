#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminListingQCMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time

##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

##Import Admin library
from PageFactory.Admin import AdminCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchListingQCAdmin(arg):
    '''
    LaunchListingQCAdmin : Go to admin page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    dumplogger.info("Enter LaunchListingQCAdmin")
    ret = 1
    listingqc_admin_cookie = {}
    db_file = "admin_cookie_listingqc"

    ##Get current env and country
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

    if 'listingqc_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        listingqc_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        listingqc_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        listingqc_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['listingqc_admin'] = [listingqc_admin_cookie]

    ##listing admin url
    url = "https://listingqc." + Config._EnvType_ + ".shopee.io/home"
    dumplogger.info("url = %s" % (url))

    ##Go to url
    BaseUICore.GotoURL({"url":url, "result": "1"})
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"listingqc_admin", "result": "1"})
    time.sleep(5)

    ##Go to Listing qc Admin
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchListingQCAdmin')


class AdminProductQCPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductQCPage(arg):
        '''
        GoToProductQCPage : go to QC listing page
                Input argu :
                    pid - product ID
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pid = arg["pid"]
        shop_id = arg["shop_id"]

        ret = 1

        ##Get QC url and go to QC
        url = "https://listingqc." + Config._EnvType_ + ".shopee.io/ruleqc/review?source=2&item_id=" + pid + "&shop_id=" + shop_id + "&region=" + Config._TestCaseRegion_

        ##Go to BE
        BaseUICore.GotoURL({"url": url, "result": "1"})
        time.sleep(15)

        OK(ret, int(arg['result']), 'AdminProductQCPage.GoToProductQCPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyProductStatus(arg):
        '''
        ModifyProductStatus : go to QC listing admin and delete/ban item
                Input argu :
                    action_type - delete/ban
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action_type = arg["action_type"]
        ret = 1

        ##ban or delete product
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Delete", "result": "1"})
        time.sleep(5)

        ##click listing bar
        xpath = Util.GetXpath({"locate": "listing_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click listing bar", "result": "1"})

        ##Choose reason option
        reason_xpath = Util.GetXpath({"locate": "select_delete_reason"})
        BaseUICore.Click({"method": "xpath", "locate": reason_xpath, "message": "choose banned reason option", "result": "1"})

        ##Click Submit
        AdminProductQCButton.ClickSubmit({"result": "1"})

        OK(ret, int(arg['result']), 'AdminProductQCPage.ModifyProductStatus -> Change product status:' + action_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetProductStatus(arg):
        '''
        ResetProductStatus : Reset product status for all kinds of situation
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click drop down list and choose country
        AdminProductQCPage.ChooseCountry({"result": "1"})

        ##Click Submit
        AdminProductQCButton.ClickSubmit({"result": "1"})

        OK(ret, int(arg['result']), 'AdminProductQCPage.ResetProductStatus -> reset product status')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCountry(arg):
        '''
        ChooseCountry : Choose country
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        country_name = Config._TestCaseRegion_

        ##Listing QC Platform country mapping
        test_country = {"PL": "TESTV6", "ES": "TESTV7", "FR": "TESTV8", "IN": "TESTV9"}
        if country_name in test_country:
            country_name = test_country[country_name]

        selected_country_xpath = Util.GetXpath({"locate": "selected_country"})
        selected_country_xpath = selected_country_xpath.replace('replace_country', country_name)

        ##retry 5 times
        for retry_times in range(5):
            if not BaseUICore.CheckElementExist({"method": "xpath", "locate": selected_country_xpath, "passok": "0", "result": "1"}):
                ##Click bar drop down list
                xpath = Util.GetXpath({"locate": "bar_drop_down_list"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click bar drop down list", "result": "1"})

                xpath = Util.GetXpath({"locate": "country"})
                xpath = xpath.replace('replace_country', country_name)

                drop_down_list_exsit_xpath = Util.GetXpath({"locate": "drop_down_list_exsit"})

                not_target_country_xpath = Util.GetXpath({"locate": "not_target_country"})
                not_target_country_xpath = not_target_country_xpath.replace('replace_country', country_name)
                time.sleep(1)
                for count in range(20):
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": not_target_country_xpath, "passok": "0", "result": "1"}):
                        ##Select next country
                        BaseUICore.SendSeleniumKeyEvent({"action_type": "down", "result": "1"})
                        dumplogger.info("Element doesn't exist at the current screen")
                        time.sleep(1)
                    else:
                        break

                ##Check bar drop down list status
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": drop_down_list_exsit_xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click country", "result": "1"})
                    break
                else:
                    dumplogger.info("retry ChooseCountry")
            else:
                dumplogger.info("Country had been chosen")
                break

        ##final check selected country
        BaseUICore.CheckElementExist({"method": "xpath", "locate": selected_country_xpath, "passok": "1", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductQCPage.ChooseCountry')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadMassQCItem(arg):
        '''
        UploadMassQCItem : Upload csv file for mass modify item
                Input argu :
                    action - ban/delete/revert_ban/revert_delete
                    file_name - csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        file_name = arg["file_name"]

        ##Click drop down list and choose country
        AdminProductQCPage.GoToProductQCPage({"pid": "0", "shop_id": "0", "result": "1"})

        #AdminProductQCPage.ChooseCountry({"result": "1"})
        BaseUICore.GotoURL({"url": "https://listingqc.staging.shopee.io/mass/bandelete/upload", "result": "1"})

        ##Click action type
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action : " + action, "result": "1"})

        if action in ("ban", "delete"):
            ##Click RQC Radio button
            xpath = Util.GetXpath({"locate": "rqc_radio"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click RQC Radio button", "result": "1"})

            time.sleep(5)

            ##Click reason field
            xpath = Util.GetXpath({"locate": "reason_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reason field", "result": "1"})

            ##Select reason
            xpath = Util.GetXpath({"locate": "auto_test_reason"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select auto team reason", "result": "1"})

        time.sleep(10)

        ##Upload item csv file
        xpath = Util.GetXpath({"locate": "upload_input_field"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Check if file is choosed successfully
        xpath = Util.GetXpath({"locate": "file_name"})
        xpath = xpath.replace('replaced_file_name', file_name)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##Click upload file button
        xpath = Util.GetXpath({"locate": "upload_file_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload file button", "result": "1"})

        time.sleep(30)

        ##Check if file is uploaded successfully
        xpath = Util.GetXpath({"locate": "success_msg"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductQCPage.UploadMassQCItem -> ' + action)


class AdminProductQCButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Submit
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Submit", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductQCButton.ClickSubmit')


class AdminListingQCUploadControlPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTitleLength(arg):
        '''
        InputTitleLength : Input title min length or max length in LUC Title Length page
                Input argu :
                    setting_type - min/max
                    title_length - input number of title length
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title_length = arg["title_length"]
        setting_type = arg["setting_type"]

        ##Input title min length or max length in LUC Title Length page
        xpath = Util.GetXpath({"locate": "input_box_of_title_length"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title_length, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputTitleLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageQuantity(arg):
        '''
        InputImageQuantity : Input image min qty or max qty in LUC image quantity page
                Input argu :
                    setting_type - min/max
                    image_qty - input number of image quantity
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_qty = arg["image_qty"]
        setting_type = arg["setting_type"]

        ##Input image min qty or max qty in LUC image quantity page
        xpath = Util.GetXpath({"locate": "input_box_of_image_quantity"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_qty, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputImageQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageResolution(arg):
        '''
        InputImageResolution : Input image resolution min length or min height in LUC image resolution page
                Input argu :
                    setting_type - "widtih" for length/height
                    pixel - input number of image resolution pixel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        pixel = arg["pixel"]
        setting_type = arg["setting_type"]

        ##Input image resolution min length or min height in LUC image resolution page
        xpath = Util.GetXpath({"locate": "input_box_of_image_resolution"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pixel, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputImageResolution')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceRange(arg):
        '''
        InputPriceRange : Input min price or max price in LUC price range page
                Input argu :
                    setting_type - min/max
                    price - input number of price range
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        price = arg["price"]
        setting_type = arg["setting_type"]

        ##Input min price or max price in LUC price range page
        xpath = Util.GetXpath({"locate": "input_box_of_price_range"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": price, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputPriceRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWholesalePrice(arg):
        '''
        InputWholesalePrice : Input wholesale price in LUC wholesale price page
                Input argu :
                    ratio - input ratio of wholesale price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ratio = arg["ratio"]

        ##Input wholesale price in LUC wholesale price page
        xpath = Util.GetXpath({"locate": "input_box_of_wholesale_price"})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ratio, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputWholesalePrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceGapLimit(arg):
        '''
        InputPriceGapLimit : Input price gap limit in LUC price gap limit page
                Input argu :
                    price_gap - input number of price gap limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        price_gap = arg["price_gap"]

        ##Input price gap limit in LUC price gap limit page
        xpath = Util.GetXpath({"locate": "input_box_of_price_gap_limit"})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": price_gap, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputPriceGapLimit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDescriptionLength(arg):
        '''
        InputDescriptionLength : Input description min length or max length in LUC description length page
                Input argu :
                    setting_type - min/max
                    desc_length - input number of description length
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        desc_length = arg["desc_length"]
        setting_type = arg["setting_type"]

        ##Input description min length or max length in LUC description length page
        xpath = Util.GetXpath({"locate": "input_box_of_desc_length"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": desc_length, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputDescriptionLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationOptionLength(arg):
        '''
        InputVariationOptionLength : Input variation option min length or max length in LUC variation option page
                Input argu :
                    setting_type - min/max
                    option_length - input number of variation option length
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option_length = arg["option_length"]
        setting_type = arg["setting_type"]

        ##Input variation option min length or max length in LUC variation option page
        xpath = Util.GetXpath({"locate": "input_box_of_variation_option_length"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": option_length, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputVariationOptionLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationValueLength(arg):
        '''
        InputVariationValueLength : Input variation value max length or min length in LUC variation value page
                Input argu :
                    setting_type - min/max
                    value_length - input number of variation value length
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value_length = arg["value_length"]
        setting_type = arg["setting_type"]

        ##Input variation value max length or min length in LUC variation value page
        xpath = Util.GetXpath({"locate": "input_box_of_variation_value_length"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value_length, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputVariationValueLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGroupName(arg):
        '''
        InputGroupName : Input Group Name in LUC group settings page
                Input argu :
                    name - input group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Input Group Name in LUC group settings page
        xpath = Util.GetXpath({"locate": "input_box_of_group_name"})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGroupDescription(arg):
        '''
        InputGroupDescription : Input group description in LUC group settings page
                Input argu :
                    description- input description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]

        ##Input group description in LUC group settings page
        xpath = Util.GetXpath({"locate": "input_box_of_group_desc"})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputGroupDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchNonPreorderDTS(arg):
        '''
        SwitchNonPreorderDTS : Switch non preorder DTS radio button in LUC days to ship page
                Input argu :
                    day - 1 / 2 / 3
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        day = arg["day"]

        ##Switch non preorder DTS radio button in LUC days to ship page
        xpath = Util.GetXpath({"locate": "non_preorder_dts_radio_button"})
        xpath = xpath.replace("dts_day_to_be_replaced", day)
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.SwitchNonPreorderDTS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckProhibitedCharacter(arg):
        '''
        CheckProhibitedCharacter : Check "Prohibited Characters" area in LUC Prohibited Characters page
                Input argu :
                    text - prohibited character you want to check
                Return code :
                    1 - exist
                    0 - Nonexistent
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Check "Prohibited Characters" area in LUC Prohibited Characters page
        xpath = Util.GetXpath({"locate": "prohibited_character"})
        xpath = xpath.replace("prohibited_character_to_be_replaced", text)
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ret = 0

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.CheckProhibitedCharacter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDTSRange(arg):
        '''
        InputDTSRange : Input DTS range from or to in LUC days to ship page
                Input argu :
                    setting_type - "min" for from/"max" for to
                    days - input nunmber of DTS
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        days = arg["days"]
        setting_type = arg["setting_type"]

        ##Input DTS range from or to in LUC days to ship page
        xpath = Util.GetXpath({"locate": "input_box_of_dts_range"})
        xpath = xpath.replace("type_to_be_replaced", setting_type)
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": days, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputDTSRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDTSNote(arg):
        '''
        InputDTSNote : Input DTS note in LUC days to ship page
                Input argu :
                    note - input note
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        note = arg["note"]

        ##Input DTS note in LUC days to ship page
        xpath = Util.GetXpath({"locate": "input_box_of_dts_note"})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": note, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.InputDTSNote')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectOverwriteSettings(arg):
        '''
        SelectOverwriteSettings : select overwrite settings in LUC group settings page
                Input argu :
                    setting_type - upload control settings
                    title_length/prohibited_characters/variation_option/variation_value/desc_length/image_resolution/image_quantity/price_gap_limit/price_range/prohibited_categories/dts_pre_order/dts_non_pre_oreder/wholesale_price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        setting_type = arg["setting_type"]

        ##click overwirte settings
        xpath = Util.GetXpath({"locate": "overwrite_filed"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "select overwrite settings", "result": "1"})
        xpath = Util.GetXpath({"locate": "overwrite_fileds"})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": setting_type[0], "result": "1"})
        xpath = Util.GetXpath({"locate": setting_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "select overwrite settings", "result": "1"})
        xpath = Util.GetXpath({"locate": "basic_information"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "select overwrite settings", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.SelectOverwriteSettings')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProhibitedCharacters(arg):
        '''
        UploadProhibitedCharacters : Upload Prohibited Characters in LUC page
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Input file to input bar
        xpath = Util.GetXpath({"locate":"upload_path"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.UploadProhibitedCharacters')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadApplicableShops(arg):
        '''
        UploadApplicableShops : Upload applicable shops in LUC page
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Input file to input bar
        xpath = Util.GetXpath({"locate":"upload_path"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlPage.UploadApplicableShops')


class AdminListingQCUploadControlButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseSellerType(arg):
        '''
        ChooseSellerType : Choose seller type in LUC page
                Input argu :
                    seller_type - normal/preferred/official
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_type = arg["seller_type"]

        ##Choose seller type in LUC page
        xpath = Util.GetXpath({"locate": seller_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose seller type", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ChooseSellerType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in LUC page
                Input argu :
                    page_type - title_length/variation_option/variation_value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click edit button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditGroup(arg):
        '''
        ClickEditGroup : Click edit group button in LUC page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit group button in LUC page
        xpath = Util.GetXpath({"locate": "edit_group"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit group button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickEditGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateGroup(arg):
        '''
        ClickCreateGroup : Click create group button in LUC page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create group button in LUC page
        xpath = Util.GetXpath({"locate": "create_group"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create group button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickCreateGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteGroup(arg):
        '''
        ClickDeleteGroup : Click delete group button in LUC page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete group button in LUC page
        xpath = Util.GetXpath({"locate": "delete_group"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete group button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickDeleteGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTrashCan(arg):
        '''
        ClickTrashCan : Click trash can in LUC page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click trash can in LUC page
        xpath = Util.GetXpath({"locate": "list_item"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash can ", "result": "1"})
        xpath = Util.GetXpath({"locate": "trash_can"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash can ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickTrashCan')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToList(arg):
        '''
        ClickBackToList : Click back to list in LUC page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to list in LUC page
        xpath = Util.GetXpath({"locate": "back_to_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to list button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickBackToList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button in LUC page
                Input argu :
                    page_type - title_length/variation_option/variation_value/dts_global/price_range/wholesale_price/price_gap_limit
                                    image_quantity/image_resolution/description_length/create_group/categories_global/add_dts
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click save button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAdd(arg):
        '''
        ClickAdd : Click add button in LUC page
                Input argu :
                    page_type - dts_global/prohibited_characters/group_dts_global/group_prohibited_characters
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click add button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReplace(arg):
        '''
        ClickReplace : Click replace button in LUC page
                Input argu :
                    page_type - prohibited_characters
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click replace button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click replace button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickReplace')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownload(arg):
        '''
        ClickDownload : Click download button in LUC page
                Input argu :
                    page_type - prohibited_characters
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click download button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickDownload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in LUC page
                Input argu :
                    page_type - dts_global/group_information
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click cancel button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button in LUC page
                Input argu :
                    page_type - group_information
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click delete button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickYes(arg):
        '''
        ClickYes : Click yes button in LUC page
                Input argu :
                    page_type - dts_global
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click yes button in LUC page
        if page_type == "dts_global":
            xpath = Util.GetXpath({"locate": "no_button"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click yes button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickYes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNo(arg):
        '''
        ClickNo : Click no button in LUC page
                Input argu :
                    page_type - dts_global
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click no button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click no button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickNo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close button in LUC page
                Input argu :
                    page_type - add_dts
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click close button in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload control close button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSDelete(arg):
        '''
        ClickDTSDelete : Click upload DTS delete button in LUC page
                Input argu :
                    dts_id - dts id or last dts setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_id = arg["dts_id"]

        ##Click upload DTS delete button in LUC page
        if dts_id == "last":
            xpath = Util.GetXpath({"locate": "preorder_dts_delete_last_button"})
        else:
            xpath = Util.GetXpath({"locate": "preorder_dts_delete_button"})
            xpath = xpath.replace("dts_id_to_be_replaced", dts_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload DTS delete button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickDTSDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSDeleteByNote(arg):
        '''
        ClickDTSDeleteByNote : Click upload DTS delete button in LUC page by note
                Input argu :
                    dts_note - dts note
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_note = arg["dts_note"]

        ##Click upload DTS delete button in LUC page
        xpath = Util.GetXpath({"locate": "preorder_dts_delete_button"})
        xpath = xpath.replace("dts_note_to_be_replaced", dts_note)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload DTS delete button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickDTSDeleteByNote')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSDownload(arg):
        '''
        ClickDTSDownload : Click DTS download button in LUC page
                Input argu :
                    dts_id - dts id or last dts setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_id = arg["dts_id"]

        ##Click DTS download button in LUC page
        if dts_id == "last":
            xpath = Util.GetXpath({"locate": "preorder_dts_download_last_button"})
        else:
            xpath = Util.GetXpath({"locate": "preorder_dts_download_button"})
            xpath = xpath.replace("dts_id_to_be_replaced", dts_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click DTS download button ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickDTSDownload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupName(arg):
        '''
        ClickGroupName : Click group name in LUC group settings page
                Input argu :
                    name - input name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click group name in LUC group settings page
        xpath = Util.GetXpath({"locate": "group_name"})
        xpath = xpath.replace("group_name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group name ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickGroupName ')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
        ClickUpload : Click upload in LUC page
                Input argu :
                    page_type - prohibited_characters
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click upload in LUC page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadControlTab(arg):
        '''
        ClickUploadControlTab : Click upload control tab in LUC page
                Input argu :
                    control_tab - control tab
                            title/variation/description/image/price/categories_local/categories_global/dts_local/dts_global
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        control_tab = arg["control_tab"]

        ##Click upload control tab in LUC page
        xpath = Util.GetXpath({"locate": control_tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload control tab", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickUploadControlTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryCheckbox(arg):
        '''
        ClickCategoryCheckbox : Click category checkbox in LUC page
                Input argu :
                    category_name - category checkbox name you need to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click category checkbox
        category_name = arg["category_name"]
        xpath = Util.GetXpath({"locate": "category_checkbox"})
        xpath_category = xpath.replace("category_name_to_be_replace", category_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_category, "message": "Click category checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickCategoryCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryName(arg):
        '''
        ClickCategoryName : Click Category Name in LUC page
                Input argu :
                    category_name - category name you need to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Category Name
        category_name = arg["category_name"]
        xpath = Util.GetXpath({"locate": "category_tab"})
        xpath_category = xpath.replace("category_name_to_be_replace", category_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_category, "message": "Click category name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingQCUploadControlButton.ClickCategoryName')


class AdminListingQCPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToUpLoadControlPage(arg):
        '''
        GoToUpLoadControlPage : Go to upload control page and choose setting type
                Input argu :
                    setting_type - group/cb/local/cnsc
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        setting_type = arg["setting_type"]

        ret = 1

        ##Click upload control
        xpath = Util.GetXpath({"locate":"upload_control_tab"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload control tab", "result":"1"})

        ##Choose setting type
        xpath = Util.GetXpath({"locate":setting_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click setting type tab", "result":"1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminListingQCPage.GoToUpLoadControlPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetAndStoreGroupIdByGroupName(arg):
        '''
        GetAndStoreGroupIdByGroupName : Get and store group ID by group name
                Input argu :
                    group_name - group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        group_name = arg["group_name"]

        ret = 1

        ##Get group ID by group name
        xpath = Util.GetXpath({"locate":"group_id"})
        xpath = xpath.replace("group_name_to_be_replace", group_name)

        ##Check if group exist
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})

            ##Store the group ID
            GlobalAdapter.ListingE2EVar._GroupID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info("Group ID: %s" % (GlobalAdapter.ListingE2EVar._GroupID_))

        else:
            dumplogger.info("No group name match.")

        OK(ret, int(arg['result']), 'AdminListingQCPage.GetAndStoreGroupIdByGroupName')
