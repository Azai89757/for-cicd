import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCProductDetailPage Precondition =="
        dumplogger.info("== Setup VNPCProductDetailPage Precondition ==")

    def test_VNPCProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-01.xml")

    def test_VNPCProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-02.xml")

    def test_VNPCProductDetailPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-03.xml")

    def test_VNPCProductDetailPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-04.xml")

    def test_VNPCProductDetailPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-05.xml")

    def test_VNPCProductDetailPage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-06.xml")

    def test_VNPCProductDetailPage07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-07.xml")

    def test_VNPCProductDetailPage08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-08.xml")

    def test_VNPCProductDetailPage09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-09.xml")

    def test_VNPCProductDetailPage10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductDetailPage-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCProductDetailPage Post condition =="
        dumplogger.info("== Setup VNPCProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
