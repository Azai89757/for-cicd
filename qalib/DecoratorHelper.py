#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 DecoratorHelper.py:
'''
import time
import os
from Config import dumplogger, chrome_logger
import XtFunc
import Parser
import Util
from functools import wraps
import inspect
import re
import GlobalAdapter
import json

class FuncRecorder(object):

    def __init__(self, func):
        self._func = func

    def __call__(self, args):

        ##Log function name before running
        dumplogger.info("Prepare enter %s" % (self._func.__name__))
        dumplogger.info(args)
        chrome_logger.info("Prepare enter %s" % (self._func.__name__))
        chrome_logger.info(args)

        ##Save inline function name to global, for checking if is api func
        GlobalAdapter.CommonVar._FailedCaseInfo_["inline_func"] = self._func.__name__
        #dumplogger.info(inspect.stack()[1])
        #dumplogger.info(inspect.stack()[1][4])

        dumplogger.info("Parser._CASE_['failskip'] in this scenario: %s" % (Parser._CASE_['failskip']))
        if "Android" in self._func.__name__ and Parser._CASE_['id'] and Parser._CASE_['failskip'] == 0:
            from PageFactory.Android import AndroidBaseUICore
            AndroidBaseUICore.AppiumConnectionChecker()

        '''
        ##Get function call from memory stack
        if inspect.stack()[1][4]:
            stt = inspect.stack()[1][4][0].strip()
            inspect_stack = inspect.stack()

            ##Filter previous class/function call for static method
            match_static = re.search('(\w+\.\w+)(\()', stt, re.IGNORECASE)
            ##Filter previous function call for basic method
            match_func = re.search('(\w+)(\()', stt, re.IGNORECASE)

            if match_static:
                dumplogger.info(match_static.group(1))

                ##Stored function name from inline code
                Parser._CASE_["funcname_inline"] = match_static.group(1)
                dumplogger.info(inspect.stack()[1][4])
                dumplogger.info(inspect.stack()[1][4][0])
                dumplogger.info('Parser._CASE_["funcname_inline"] = %s' % (Parser._CASE_["funcname_inline"]))
                GlobalAdapter.CommonVar._MemoryStack_.append(Parser._CASE_["funcname_inline"])

            elif match_func:
                dumplogger.info(match_func.group(1))

                ##Stored function name from inline code
                Parser._CASE_["funcname_inline"] = match_func.group(1)
                dumplogger.info(inspect.stack()[1][4])
                dumplogger.info(inspect.stack()[1][4][0])
                dumplogger.info('Parser._CASE_["funcname_inline"] = %s' % (Parser._CASE_["funcname_inline"]))
                GlobalAdapter.CommonVar._MemoryStack_.append(Parser._CASE_["funcname_inline"])

            else:
                dumplogger.info("Don't find staticmethod class/function !!")

            ##From line:64-87, for retry function stack
            ##Filter previous filename/class/function call for static method
            match_func_inline = re.search('(\w+)(\.\w+\.\w+)(\()', stt, re.IGNORECASE)

            ##Get the first string
            if match_func_inline:
                func_file_name = match_func_inline.group(1)
            elif match_static:
                func_file_name = re.search('(\w+)(\.\w+)(\()', stt, re.IGNORECASE).group(1)
            elif match_func:
                func_file_name = re.search('(\w+)(\()', stt, re.IGNORECASE).group(1)

            ##If call by xml
            if inspect_stack[5][3] == 'XMLParser':
                method_file_name = []
                ##Get module file name
                for method_file in Parser.list_func_obj:
                    method_file_name.append(method_file.__name__.split('.')[-1])
                ##If the first string is file name in parser list
                if func_file_name in method_file_name:
                    ##Get the original command
                    GlobalAdapter.CommonVar._FuncStack_.append(Parser._CASE_["funcname_inline"] + '(' + json.dumps(args) + ')')
                ##If the first string is not file name
                else:
                    ##Get the file name which call this function
                    func_file_name = inspect_stack[1][1].split('\\')[-1].split('.')[0]
                    ##Reorganize file name and command
                    GlobalAdapter.CommonVar._FuncStack_.append(func_file_name + '.' + Parser._CASE_["funcname_inline"] + '(' + json.dumps(args) + ')')
            '''

        ##Start time
        time_first = time.time()

        ##Execute function
        self._func(args)

        ##End time
        time_second = time.time()

        ##Log function name after running
        dumplogger.info("Leave %s at %.2f sec" % (self._func.__name__, time_second-time_first))
        chrome_logger.info("Leave %s" % (self._func.__name__))


def Extract(func):

    @wraps(func)
    def ExtractElements(*args, **kwargs):

        ##Output function name
        dumplogger.info(func.__name__)

        ##Output footnote
        dumplogger.info(func.__doc__)

        return Util.MappingXpathFromJson(*args)
        #return func(*args)
        #return func(*args, **kwargs)

    return ExtractElements
