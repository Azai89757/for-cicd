import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCOrder Precondition =="
        dumplogger.info("== Setup ARPCOrder Precondition ==")

    def test_ARPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-01.xml")

    def test_ARPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-02.xml")

    def test_ARPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-03.xml")

    def test_ARPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-04.xml")

    def test_ARPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-05.xml")

    def test_ARPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-06.xml")

    def test_ARPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCOrder-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCOrder Post condition =="
        dumplogger.info("== Setup ARPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver({})
