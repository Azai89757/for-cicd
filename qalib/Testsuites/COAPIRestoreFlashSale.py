import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COAPIRestoreFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIRestoreFlashSale Precondition =="
        dumplogger.info("== Setup COAPIRestoreFlashSale Precondition ==")

    def test_COAPIRestoreFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/COAPIRestoreFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIRestoreFlashSale Post condition =="
        dumplogger.info("== Setup COAPIRestoreFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
