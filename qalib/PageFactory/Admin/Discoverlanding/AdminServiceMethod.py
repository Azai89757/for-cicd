#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminServiceMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import os
import re
import time
import datetime
from unittest import result

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import GlobalAdapter

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminServicePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBannerBundle(arg):
        ''' CreateBannerBundle : Create a banner bundle
                Input argu :
                    banner_type - banner type (Category / Landing Page / Group Banner / Homepage Mall Banner / Home Square / Official Shop / Home Popup / Mall Popup / Microsite / Floating Banner / Skinny Banner)
                    bundle_name - name of bundle
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        bundle_name = arg["bundle_name"]

        ##Click create banner bundle
        xpath = Util.GetXpath({"locate":"create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create banner bundle", "result": "1"})
        time.sleep(2)

        ##Click banner type dropdown
        xpath = Util.GetXpath({"locate":"banner_type_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner type dropdown", "result": "1"})
        time.sleep(2)

        ##Choose banner type
        xpath = Util.GetXpath({"locate":"banner_type"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": banner_type, "result": "1"})
        time.sleep(2)

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Input bundle name
        xpath = Util.GetXpath({"locate": "bundle_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": bundle_name, "result": "1"})
        time.sleep(2)

        ##Click other place
        xpath = Util.GetXpath({"locate": "other_place"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click other place", "result": "1"})
        time.sleep(5)

        ##Click save
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminServicePage.CreateBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyBannerBundle(arg):
        ''' ModifyBannerBundle : Modify banner bundle content
                Input argu :
                    banner_type - banner type (Category / Landing Page / Group Banner / Homepage Mall Banner / Home Square / Official Shop / Home Popup / Mall Popup / Microsite / Floating Banner / Skinny Banner)
                    bundle_name - name of bundle
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        bundle_name = arg["bundle_name"]

        if banner_type:
            ##Click banner type dropdown
            xpath = Util.GetXpath({"locate":"banner_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner type dropdown", "result": "1"})
            time.sleep(2)

            ##Choose banner type
            xpath = Util.GetXpath({"locate":"banner_type"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": banner_type, "result": "1"})
            time.sleep(2)

        if bundle_name:
            ##Input bundle name
            xpath = Util.GetXpath({"locate": "bundle_name_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": bundle_name, "result": "1"})
            time.sleep(2)

            ##Click other place
            xpath = Util.GetXpath({"locate": "other_place"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click other place", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminServicePage.ModifyBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchBannerBundleByName(arg):
        ''' SearchBannerBundleByName : Search banner bundle by name
                Input argu :
                    name - bundle name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click "Bundle Name" search bar
        xpath = Util.GetXpath({"locate": "bundle_name_search_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle name search bar", "result": "1"})
        time.sleep(2)

        ##Input search bar
        xpath = Util.GetXpath({"locate": "bundle_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})
        time.sleep(2)

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.SearchBannerBundleByName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBannerBundle(arg):
        ''' DeleteBannerBundle : Delete banner bundle
                Input argu :
                    bundle_name - bundle name
                    banner_type - banner type
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        bundle_name = arg["bundle_name"]

        ##Search bundle initially
        AdminServicePage.SearchBannerBundleByName({"name": bundle_name, "result": "1"})
        time.sleep(5)

        ##Click delete for search result (if there is result)
        xpath = Util.GetXpath({"locate": "delete_icon"})
        xpath = xpath.replace("type_to_be_replaced", banner_type).replace("name_to_be_replaced", bundle_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete bundle", "result": "1"})
            time.sleep(2)

            ##Click confirm btn
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.DeleteBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerBundle(arg):
        ''' EditBannerBundle : Edit banner bundle
                Input argu :
                    bundle_name - bundle name
                    banner_type - banner type
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        bundle_name = arg["bundle_name"]

        ##Search bundle initially
        AdminServicePage.SearchBannerBundleByName({"name": bundle_name, "result": "1"})
        time.sleep(5)

        ##Click edit for search result
        xpath = Util.GetXpath({"locate": "edit_icon"})
        xpath = xpath.replace("type_to_be_replaced", banner_type).replace("name_to_be_replaced", bundle_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit bundle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.EditBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditRuleSetName(arg):
        ''' EditRuleSetName : Edit rule set name (with time stamp)
                Input argu :
                    name - rule set name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        name = arg["name"]
        time_stamp = XtFunc.GenerateCurrentTimeString("%Y%m%d%H%M")

        ##Input rule set name
        xpath = Util.GetXpath({"locate": "rule_set_name_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name + "_" + time_stamp, "result": "1"})
        time.sleep(2)

        ##Click title to let it input success
        xpath = Util.GetXpath({"locate": "rule_set_title"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.EditRuleSetName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputABTestingRange(arg):
        ''' InputABTestingRange : Input ab-testing user id range
                Input argu :
                    start_id - start from last 2 id
                    end_id - end with last 2 id
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        start_id = arg["start_id"]
        end_id = arg["end_id"]

        ##Input start id
        xpath = Util.GetXpath({"locate": "start_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_id, "result": "1"})
        time.sleep(2)

        ##Input end id
        xpath = Util.GetXpath({"locate": "end_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_id, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminServicePage.InputABTestingRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectLocationGroup(arg):
        ''' SelectLocationGroup : Select location group
                Input argu :
                    group_name - location group name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        group_name = arg["group_name"]

        ##Click location dropdown
        xpath = Util.GetXpath({"locate": "location_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click location dropdown", "result": "1"})

        ##Click location content
        xpath = Util.GetXpath({"locate": "location_content"})
        xpath = xpath.replace("name_to_be_replaced", group_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click location dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.SelectLocationGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectLanguageTag(arg):
        ''' SelectLanguageTag : Select language tag
                Input argu :
                    language - which language you want to choose
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        language = arg["language"]

        ##Click language dropdown
        xpath = Util.GetXpath({"locate": "language_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click language dropdown", "result": "1"})

        ##Click language content
        xpath = Util.GetXpath({"locate": "language_content"})
        xpath = xpath.replace("name_to_be_replaced", language)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click language", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.SelectLanguageTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAppVersion(arg):
        ''' SelectAppVersion : Select app version
                Input argu :
                    app_version - android / ios
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        app_version = arg["app_version"]

        ##Click app version checkbox
        xpath = Util.GetXpath({"locate": app_version})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click app version checkbox", "result": "1"})

        ##Input app version
        time.sleep(3)
        xpath = Util.GetXpath({"locate": "app_version_feild"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": '12345', "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.SelectAppVersion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GernerateUnifiedLink(arg):
        ''' GernerateUnifiedLink : Generation unified link
                Input argu :
                    upload_type - csv / manual
                    file_name - if choose upload type with csv, should give file name witch you want to upload
                    origin_url - shopee web url
                    smtt - SMTT media data
                    pid - pid(channel)
                    campaign_name - c(campaign name)
                    utm_source - utm source
                    utm_medium - utm medium
                    utm_other_param - utm other param (format: key=value; key=value) and use ";" to separate the values
                    additional_param - addtional tracking param (format: key=value; key=value) and use ";" to separate the values
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        origin_url = arg["origin_url"]
        smtt = arg["smtt"]
        pid = arg["pid"]
        campaign_name = arg["campaign_name"]
        utm_source = arg["utm_source"]
        utm_medium = arg["utm_medium"]
        utm_other_param = arg["utm_other_param"]
        additional_param = arg["additional_param"]

        if upload_type == "csv":
            xpath = Util.GetXpath({"locate": "upload_csv"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
            BaseUICore.Click({"method": "xpath", "locate": Util.GetXpath({"locate":"generate"}), "message": "Click generate", "result": "1"})
        elif upload_type == "manual":
            xpath = Util.GetXpath({"locate":"origin_url"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": origin_url, "result": "1"})

            if smtt:
                xpath = Util.GetXpath({"locate":"smtt"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": smtt, "result": "1"})

            if pid:
                xpath = Util.GetXpath({"locate":"pid"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pid, "result": "1"})

            if campaign_name:
                xpath = Util.GetXpath({"locate":"campaign_name"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": campaign_name, "result": "1"})

            if utm_source:
                xpath = Util.GetXpath({"locate":"utm_source"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": utm_source, "result": "1"})

            if utm_medium:
                xpath = Util.GetXpath({"locate":"utm_medium"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": utm_medium, "result": "1"})

            if utm_other_param:
                xpath = Util.GetXpath({"locate":"utm_other_param"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": utm_other_param, "result": "1"})

            if additional_param:
                xpath = Util.GetXpath({"locate":"additional_param"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": additional_param, "result": "1"})

        ##Click general button
        BaseUICore.ExecuteScript({"script": "document.querySelector('#additionalParams').scrollIntoView()", "result": "1"})
        xpath = Util.GetXpath({"locate":"generate"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click general button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServicePage.GernerateUnifiedLink')


class AdminServiceButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteBannerBundle(arg):
        ''' ClickDeleteBannerBundle : Click delete a specific banner bundle
                Input argu :
                    bundle_name - name of bundle
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        bundle_name = arg["bundle_name"]

        ##Click delete banner bundle
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace("name_to_be_replaced", bundle_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete banner bundle -> " + bundle_name,"result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminServiceButton.ClickDeleteBannerBundle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' ClickConfirm : Click confirm btn
                Input argu :
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn","result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel
                Input argu :
                    N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn","result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        ''' ClickSave : Click save
                Input argu :
                    N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn","result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewRuleSet(arg):
        ''' ClickAddNewRuleSet : Click add new rule set
                Input argu :
                    N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click add new rule ser btn
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new ruleset btn","result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceButton.ClickAddNewRuleSet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckRuleSet(arg):
        ''' ClickCheckRuleSet : Click checkbox of rule set
                Input argu :
                    action - check / uncheck
                    rule_set_name - name of rule set (user_tag / user_list / ab_testing / location / language / time / app_version / unregister_devices)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        action = arg["action"]
        rule_set_name = arg["rule_set_name"]

        ##Click rule set checkbox
        if action == "check":
            xpath = Util.GetXpath({"locate": rule_set_name})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check rule set -> " + rule_set_name,"result": "1"})
        elif action == "uncheck":
            xpath = Util.GetXpath({"locate": rule_set_name + "_checked"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click uncheck rule set -> " + rule_set_name,"result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceButton.ClickCheckRuleSet')


class AdminRuleSetPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUserGroup(arg):
        '''
        SelectUserGroup : Select user group
                Input argu :
                    user_group - which user group you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_group = arg["user_group"]

        ##Click user group option
        xpath = Util.GetXpath({"locate": "user_group_checkbox"})
        xpath = xpath.replace("name_to_be_replaced", user_group)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user group checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRuleSetPage.SelectUserGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAdditionalRuleSet(arg):
        '''
        SelectAdditionalRuleSet : Select additional rule set
                Input argu :
                    rule_set - which rule set you want to choose
                    action - check / uncheck
                Return code :
                    1 - success
                    0 - fail
                    1 - error
        '''
        ret = 1
        rule_set = arg["rule_set"]
        action = arg["action"]

        ##Click additional rule set option
        xpath = Util.GetXpath({"locate": rule_set + '_' + action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set -> " + rule_set,"result": "1"})

        OK(ret, int(arg['result']), 'AdminRuleSetPage.SelectAdditionalRuleSet')
