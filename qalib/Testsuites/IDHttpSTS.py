import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDHttpSTS(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSTS Precondition =="
        dumplogger.info("== Setup IDHttpSTS Precondition ==")

    def test_IDHttpSTS01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-01.xml")

    def test_IDHttpSTS02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-02.xml")

    def test_IDHttpSTS03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-03.xml")

    def test_IDHttpSTS04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-04.xml")

    def test_IDHttpSTS05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-05.xml")

    def test_IDHttpSTS06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-06.xml")

    def test_IDHttpSTS07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-07.xml")

    def test_IDHttpSTS08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-08.xml")

    def test_IDHttpSTS09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-09.xml")

    def test_IDHttpSTS10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-10.xml")

    def test_IDHttpSTS11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-11.xml")

    def test_IDHttpSTS12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-12.xml")

    def test_IDHttpSTS13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSTS-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSTS Post condition =="
        dumplogger.info("== Setup IDHttpSTS Post condition ==")
