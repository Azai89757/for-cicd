import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCMePage Precondition =="
        dumplogger.info("== Setup THPCMePage Precondition ==")

    def test_THPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMePage-01.xml")

    def test_THPCMePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMePage-02.xml")

    def test_THPCMePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMePage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCMePage Post condition =="
        dumplogger.info("== Setup THPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
