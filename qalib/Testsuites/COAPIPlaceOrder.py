import unittest
from Config import dumplogger
from FrameWorkBase import *


class COAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup COAPIPlaceOrder Precondition ==")

    def test_COAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/COAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup COAPIPlaceOrder Post condition ==")
