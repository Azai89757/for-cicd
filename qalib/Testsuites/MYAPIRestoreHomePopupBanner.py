import unittest
from Config import dumplogger
from FrameWorkBase import *


class MYAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup MYAPIRestoreHomePopupBanner Precondition ==")

    def test_MYAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MYAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup MYAPIRestoreHomePopupBanner Post condition ==")
