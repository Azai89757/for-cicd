import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCNotification(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCNotification Precondition =="
        dumplogger.info("== Setup THPCNotification Precondition ==")

    def test_THPCNotification01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCNotification-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCNotification Post condition =="
        dumplogger.info("== Setup THPCNotification Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
