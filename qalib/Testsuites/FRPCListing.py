import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCListing Precondition =="
        dumplogger.info("== Setup FRPCListing Precondition ==")

    def test_FRPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCListing-01.xml")

    def test_FRPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCListing-02.xml")

    def test_FRPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCListing Post condition =="
        dumplogger.info("== Setup FRPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
