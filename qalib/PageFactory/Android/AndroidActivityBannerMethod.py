﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidActivityBannerMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidHomePageMethod
import AndroidLoginSignUpMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidActivityBanner:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBannerExist(arg):
        '''
        CheckBannerExist : Check banner exist
                Input argu :
                    banner_type - category / floating / skinny / home_popup / mall_popup / mall_banner / home_carousel / mall_carousel / digital_product_carousel / digital_product_scrolling
                    image - OpenCV project file  NOTE: if wanna use mutiple image, seperate image name by ","  ex. photo_name1,photo_name2
                    account  - login account, if account is none, then we won't login
                    password - login password, if password is none, then we won't login
                    scroll_time - scroll the bar times
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        image = arg["image"]
        account = arg["account"]
        password = arg["password"]
        scroll_time = arg["scroll_time"]

        ##Retry to compare img
        for retry_count in range(1, 6):

            ##Close APP and open
            time.sleep(10)
            AndroidBaseUICore.AndroidCloseShopeeAPP({"result": "1"})
            time.sleep(5)
            AndroidBaseUICore.AndroidOpenShopeeAPP({"result": "1"})
            time.sleep(30)
            ##Perform different actions for different banners
            AndroidActivityBanner.LaunchShopeeSiteAndGoToBannerPage({"banner_type":banner_type, "account":account, "password":password, "scroll_time":scroll_time, "result": "1"})
            time.sleep(5)

            ##Check whether if there is mutiliple image
            image_split_result = image.split(",")

            ##Start to begin image compare by different file types
            if len(image_split_result) > 1:
                ##Means to use GIF compare function
                match_result = XtFunc.MatchGIFImg({"mode":"android", "image1": image_split_result[0], "image2": image_split_result[1], "env": Config._EnvType_, "expected_result":arg['result']})
            else:
                ##Means to use Picture compare function
                match_result = XtFunc.MatchPictureImg({"mode":"android", "image":image, "env": Config._EnvType_, "threshold": "0.8", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("Android Banner image/GIF matching successful at %d run!!!!" % (retry_count))
                break
            else:
                time.sleep(30)
                dumplogger.info("Android Banner image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'AndroidActivityBanner.CheckBannerExist -> ' + banner_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBannerQuota(arg):
        '''
        CheckBannerQuota : Check banner quota
                Input argu :
                    image - OpenCV project file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]

        ##Compare banner and check direct page
        time.sleep(2)
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": "android", "image": image, "threshold": "0.8", "is_click": "yes", "result": "1"})
        xpath = Util.GetXpath({"locate": "google_page"})
        AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype": "back", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidActivityBanner.CheckBannerQuota')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LaunchShopeeSiteAndGoToBannerPage(arg):
        '''
        LaunchShopeeSiteAndGoToBannerPage : Go to each banner page from homepage before verifying banner image
                Input argu :
                    banner_type - category / floating / skinny / home_popup / mall_popup / mall_banner / home_carousel / mall_carousel / digital_product_carousel / digital_product_scrolling
                    account  - login account, if account is none, then we won't login
                    password - login password, if password is none, then we won't login
                    scroll_time - scroll the bar times
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        account = arg["account"]
        password = arg["password"]
        scroll_time = arg["scroll_time"]

        ##Clean app data and grant permission
        if banner_type in ("home_popup", "mall_popup", "digital_product_scrolling", "digital_product_carousel"):
            ##Clean Shopee APP Data and rant all Shopee APP permission
            AndroidBaseUICore.AndroidCleanAPPData({"result": "1"})
            time.sleep(15)

            ##When login account is needed
            if account:
                AndroidBaseUICore.AndroidCloseBanner({"banner_type": "home"})
                time.sleep(10)
                AndroidLoginSignUpMethod.AndroidShopeeLogin({"account": account, "password": password, "logintype": "account", "result": "1"})
                time.sleep(5)
                AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type": "home", "result": "1"})
                AndroidBaseUILogic.AndroidRelativeMove({"direction": "up", "times": "1", "result": "1"})
                time.sleep(15)
            else:
                time.sleep(10)
                AndroidBaseUILogic.AndroidRelativeMove({"direction": "up", "times": "1", "result": "1"})
                dumplogger.info("No need to login!!!")

            ##Enter official shop page after login
            if "mall_popup" in banner_type:
                AndroidBaseUICore.AndroidCloseBanner({"banner_type": "home"})
                AndroidBaseUILogic.AndroidRelativeMove({"direction": "up", "times": "2", "result": "1"})
                time.sleep(5)
                AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": Util.GetXpath({"locate": "offical_shop_view_more"}), "direction": "down", "isclick": "1", "result": "1"})
                time.sleep(15)

            elif "digital_product" in banner_type:
                ##Move to daily discover title section
                AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": Util.GetXpath({"locate": "daily_discover_title"}), "direction": "down", "isclick": "0", "result": "1"})
                AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "1", "result": "1"})
                AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type": "metab", "result": "1"})
                time.sleep(5)
                AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type": "home", "result": "1"})
                time.sleep(5)

                ##Since scroll bar will not always show up, so only scroll the bar when it shows up
                if scroll_time and AndroidBaseUICore.AndroidCheckElementExist({"method": "xpath", "locate": Util.GetXpath({"locate": "product_scroll_bar"}), "passok": "0", "result": "1"}):
                    AndroidBaseUILogic.AndroidDirectMove({"locate": Util.GetXpath({"locate": "product_scroll_bar"}), "direction": "right", "times": scroll_time, "result": "1"})
        else:
            #AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type": "metab", "result": "1"})
            #time.sleep(5)
            #AndroidHomePageMethod.AndroidHomePage.HomePageNavigation({"type": "home", "result": "1"})
            #time.sleep(5)
            AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"3", "result": "1"})

            ##Goto category to check category banner
            if "category" in banner_type:
                ##Move to category component and click category
                AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"category_name"}), "direction":"down", "isclick":"0", "result": "1"})
                AndroidBaseUILogic.AndroidMoveElementToPosition({"locate": Util.GetXpath({"locate":"category_name"}), "ratio": "0.5", "result": "1"})
                xpath = Util.GetXpath({"locate": "category_group"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click category group", "result": "1"})
                time.sleep(5)
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"2", "result": "1"})
                time.sleep(5)

            ##Goto official shop to check official shop banner
            elif "mall_carousel" in banner_type:
                AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"offical_shop_view_more"}), "direction":"down", "isclick":"1", "result": "1"})
                time.sleep(15)
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "result": "1"})
                time.sleep(5)

            ##Swipe to mall banner section
            elif "mall_banner" in banner_type:
                ##Move to shopee mall title
                AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":Util.GetXpath({"locate":"shopee_mall_title"}), "direction":"down", "isclick":"0", "result": "1"})
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "result": "1"})
                AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":"6", "locate_y":"800", "movex":"6", "movey":"500", "result": "1"})
                time.sleep(5)

            ##Refresh homepage when checking landing, skinny, and floating
            elif banner_type in ("home_carousel", "skinny", "floating"):
                time.sleep(15)
                AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"2", "result": "1"})
                if Config._TestCaseRegion_ == "MX" and banner_type == "skinny":
                    AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "1", "result": "1"})
                time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidActivityBanner.LaunchShopeeSiteAndGoToBannerPage -> ' + banner_type)
