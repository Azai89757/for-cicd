import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCFreeShippingVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCFreeShippingVoucher Precondition =="
        dumplogger.info("== Setup IDPCFreeShippingVoucher Precondition ==")

    def test_IDPCFreeShippingVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCFreeShippingVoucher-01.xml")

    def test_IDPCFreeShippingVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCFreeShippingVoucher-02.xml")

    def test_IDPCFreeShippingVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCFreeShippingVoucher-03.xml")

    def test_IDPCFreeShippingVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCFreeShippingVoucher-04.xml")

    def test_IDPCFreeShippingVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCFreeShippingVoucher-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCFreeShippingVoucher Post condition =="
        dumplogger.info("== Setup IDPCFreeShippingVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
