#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminWalletMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchWalletAdmin(arg):
    ''' LaunchWalletAdmin : Go to wallet admin page
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    ret = 1
    db_file = arg["db_file"]
    wallet_cookie = {}

    ##If cookie is not stored for the first time, get cookie from db
    if 'wallet_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":"", "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        wallet_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        wallet_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        wallet_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['wallet_admin'] = [wallet_cookie]

    ##Go to url
    if Config._TestCaseRegion_ == 'ID':
        url = "https://admin.staging.wallet.airpay.co.id"
    elif Config._TestCaseRegion_ == 'TH':
        url = "https://adminv2.uat.airpay.in.th/"
    elif Config._TestCaseRegion_ == 'VN':
        url = "https://adminv2.uat.airpay.vn/"
    else:
        ret = 0
        dumplogger.info("Wallet Admin only available in ID/TH/VN.")

    BaseUICore.GotoURL({"url":url, "result": "1"})
    dumplogger.info("url = %s" % (url))
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"wallet_admin", "result": "1"})
    time.sleep(5)

    ##Go to Wallet Admin
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    ##Check if admin login success from cookies stored in db, if not, use testing account to login for alternative
    xpath = Util.GetXpath({"locate": "admin_logout_btn"})
    if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):

        if Config._TestCaseRegion_ != 'ID':
            ##Need to click sign in with google if fail to set cookie in Airpay admin
            xpath = Util.GetXpath({"locate": "signin_with_google"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sign in with google", "result": "1"})

        ##Login with google account if needed
        AdminCommonMethod.GoogleAccountLogin({"account": "shopeetwqa", "password": "shopee@123!", "result": "1"})

    OK(ret, int(arg['result']), 'LaunchWalletAdmin')

class WalletAdminTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnLeftPanel(arg):
        ''' ClickTabOnLeftPanel : Click a tab at the left panel
                Input argu :
                    type - user / buyer_wallet_txns / search_order / transaction
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        OK(ret, int(arg['result']), 'WalletAdminTab.ClickTabOnLeftPanel -> ' + type)

class AdminBuyerWalletTransactionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchWalletTransaction(arg):
        ''' SearchWalletTransaction : Search Wallet Transaction by user ID or airpay_order_id
                Input argu :
                    search_by - user_id / airpay_order_id
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        search_by = arg["search_by"]

        if search_by == "user_id":
            ##Search Wallet Transaction by user ID
            xpath = Util.GetXpath({"locate":"userid_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._OrderSNDict_, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            time.sleep(5)

            ##Wait for table loading complete before next step
            xpath = Util.GetXpath({"locate":"search_result"})
            xpath = xpath.replace('user_id_to_be_replaced', GlobalAdapter.OrderE2EVar._OrderSNDict_)
            BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"3", "result": "1"})

        elif search_by == "airpay_order_id":
            xpath = Util.GetXpath({"locate":"airpay_order_id_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PaymentE2EVar._AirpayOrderID_, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBuyerWalletTransactionPage.SearchWalletTransaction -> search by: ' + search_by)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToAirpayOrderDetail(arg):
        ''' GoToAirpayOrderDetail : Click airpay order search result
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click search result
        xpath = Util.GetXpath({"locate": "search_result"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click airpay order search result", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerWalletTransactionPage.GoToAirpayOrderDetail')
