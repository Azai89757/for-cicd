#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 HttpAPICore.py: The def of this file called by other function.
'''

##Import system library
from requests.exceptions import ReadTimeout
from requests.exceptions import ConnectionError
import json
from datetime import datetime

##Import Framework common library
import FrameWorkBase
import Config
from Config import dumplogger
from Config import apilogger
import Parser
import XtFunc
import GlobalAdapter
import HttpApiHelper
import DecoratorHelper


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def InitialHttpAPI(arg):
    ''' InitialHttpAPI : API initial for HTTP request
                Input argu :
                    json_data - json data to be load
                    session - new / inherit / unused
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''
    ret = 1
    json_data = arg["json_data"]
    session = arg["session"]
    slash = Config.dict_systemslash[Config._platform_]

    if json_data:
        ##Get json data from common folder
        json_file = Config._APICaseDataFilePath_ + slash + "http" + slash + json_data
        dumplogger.info("Get json data from %s" % (json_file))
    else:
        ##Get json data from testcase folder
        json_file = Config._APICaseDataFilePath_ + slash + "http" + slash + "tcjson" + slash + Config._TestCaseRegion_ + slash + Config._TestCaseFeature_ + slash + Parser._CASE_['id']
        dumplogger.info("Get json data from %s" % (json_file))

    ##Get json file to load data
    XtFunc.GetJSONContentFromFile({"jsonfile":json_file, "result": "1"})

    ##Restore session or inherit session already exists
    if session == "new":
        GlobalAdapter.APIVar._HttpSession_ = "new_session"
    elif session == "inherit":
        ##If session is not empty, means that it came from new started session
        if GlobalAdapter.APIVar._APIResponse_["session"]:
            GlobalAdapter.APIVar._HttpSession_ = GlobalAdapter.APIVar._APIResponse_["session"]
    elif session == "unused":
        GlobalAdapter.APIVar._HttpSession_ = ""
        dumplogger.info("If you send unused, usually means you don't need it.")
    else:
        dumplogger.info("Please make sure your session type is correct!!!")
        ret = 0

    OK(ret, int(arg['result']), 'InitialHttpAPI')

@DecoratorHelper.FuncRecorder
def DeInitialHttpAPI(arg):
    ''' DeInitialHttpAPI : API deinitial for HTTP request
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''
    ret = 1

    ##Restore global adapter to avoid error for next case
    GlobalAdapter.APIVar._HttpUrl_ = ""
    GlobalAdapter.APIVar._HttpUploadFiles_ = {}
    GlobalAdapter.APIVar._HttpPayload_ = {}
    GlobalAdapter.APIVar._HttpHeaders_ = {}
    GlobalAdapter.APIVar._HttpUrlParameter_ = {}
    GlobalAdapter.APIVar._JsonCaseData_ = {}
    GlobalAdapter.APIVar._Csrftoken_ = ""
    GlobalAdapter.APIVar._RequestTimestamp_ = ""

    OK(ret, int(arg['result']), 'DeInitialHttpAPI')

@DecoratorHelper.FuncRecorder
def GetAndSetHttpAPIData(arg):
    ''' GetAndSetHttpAPIData : get and set data from global dict
            Input argu :
                collection - collection key to use in the json file, and value need include api url、payload、headers、body，and response status、response text
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    collection = arg["collection"]
    message = "collection name : %s" % (collection)
    ret = 1

    ##Store case related data for a collection
    if collection in GlobalAdapter.CommonVar._JSONRawDataFromFile_:
        GlobalAdapter.APIVar._JsonCaseData_ = GlobalAdapter.CommonVar._JSONRawDataFromFile_[collection]
        dumplogger.info(GlobalAdapter.APIVar._JsonCaseData_)

        ##Verify if _JsonCaseData_ is valid
        if "url" not in GlobalAdapter.APIVar._JsonCaseData_:
            message = "Please make sure url is included in your json data file!!!"
            dumplogger.info(message)
            ret = 0
        else:
            ##Assign url to global variable
            dumplogger.info("Assign url to global variable")
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._JsonCaseData_["url"]

            ##Assign payload in _JsonCaseData_ or None
            if "payload" in GlobalAdapter.APIVar._JsonCaseData_:
                dumplogger.info("Assign payload to global variable")
                GlobalAdapter.APIVar._HttpPayload_ = GlobalAdapter.APIVar._JsonCaseData_["payload"]

                dumplogger.info("GlobalAdapter.APIVar._HttpSession_: %s" % (GlobalAdapter.APIVar._HttpSession_))
                ##If start a new session, usually it means that login is required
                if GlobalAdapter.APIVar._HttpSession_ == "new_session":
                    ##New session usually means login action is required
                    GlobalAdapter.APIVar._HttpPayload_["username"] = arg["username"]
                    GlobalAdapter.APIVar._HttpPayload_["password"] = arg["password"]
                    dumplogger.info("username to login: %s" % (GlobalAdapter.APIVar._JsonCaseData_["payload"]["username"]))
                    dumplogger.info("password to login: %s" % (GlobalAdapter.APIVar._JsonCaseData_["payload"]["password"]))
                else:
                    dumplogger.info("Following API request will use inherited session from previous step!!!")

            else:
                dumplogger.info("payload is not existed in your json...")
                GlobalAdapter.APIVar._HttpPayload_ = {}

            ##Assign headers in _JsonCaseData_ or None
            if "headers" in GlobalAdapter.APIVar._JsonCaseData_:
                dumplogger.info("Assign headers to global variable")
                GlobalAdapter.APIVar._HttpHeaders_ = GlobalAdapter.APIVar._JsonCaseData_["headers"]

            else:
                dumplogger.info("headers is not existed in your json...")
                GlobalAdapter.APIVar._HttpHeaders_ = {}

            ##Assign query_string_parameters in _JsonCaseData_ or None
            if "query_string_parameters" in GlobalAdapter.APIVar._JsonCaseData_:
                dumplogger.info("Assign query_string_parameters to global variable")
                GlobalAdapter.APIVar._HttpUrlParameter_ = GlobalAdapter.APIVar._JsonCaseData_["query_string_parameters"]
            else:
                dumplogger.info("query_string_parameters is not existed in your json...")
                GlobalAdapter.APIVar._HttpUrlParameter_ = {}

            ##Assign files in _JsonCaseData_ or None
            if "files" in GlobalAdapter.APIVar._JsonCaseData_:
                dumplogger.info("Assign files to global variable")
                GlobalAdapter.APIVar._HttpUploadFiles_ = GlobalAdapter.APIVar._JsonCaseData_["files"]
            else:
                dumplogger.info("files is not existed in your json...")
                GlobalAdapter.APIVar._HttpUploadFiles_ = {}
    else:
        message = "Please make sure your collection name is correct!!! json collection -> %s != %s" % (collection, str(GlobalAdapter.CommonVar._JSONRawDataFromFile_.keys()))
        dumplogger.info(message)
        ret = 0

    OK(ret, int(arg['result']), 'GetAndSetHttpAPIData -> ' + message)

@DecoratorHelper.FuncRecorder
def SendHttpRequest(arg):
    ''' SendHttpRequest : Send Http Request
            Input argu :
                http_method - Send http request, you can use get / post / put
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    http_method = arg["http_method"]

    ##Intial API Controller
    AP = HttpApiHelper.APIController()

    ##Compose url if cookie is given
    if GlobalAdapter.APIVar._HttpUrlParameter_:
        url_param = AP.ComposeURL(http_method, "", GlobalAdapter.APIVar._HttpUrlParameter_)
        final_url = GlobalAdapter.APIVar._HttpUrl_ + '?' + url_param
    else:
        final_url = GlobalAdapter.APIVar._HttpUrl_
    dumplogger.info("Url to send API: %s" % (final_url))
    dumplogger.info("Payload to send API: %s" % (GlobalAdapter.APIVar._HttpPayload_))

    try:
        ##Save timestamp of sending request
        GlobalAdapter.APIVar._RequestTimestamp_ = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        ##Send API request and assign response to global
        GlobalAdapter.APIVar._APIResponse_ = AP.SendAPIPacket(
                                                                http_method=http_method,
                                                                url=final_url,
                                                                payload=GlobalAdapter.APIVar._HttpPayload_,
                                                                files=GlobalAdapter.APIVar._HttpUploadFiles_,
                                                                session=GlobalAdapter.APIVar._HttpSession_,
                                                                cookies=GlobalAdapter.APIVar._HttpCookie_,
                                                                headers=GlobalAdapter.APIVar._HttpHeaders_,
                                                                url_param=GlobalAdapter.APIVar._HttpUrlParameter_
                                                                )
        ##Handle wrong http method
        if GlobalAdapter.APIVar._APIResponse_ == 1:
            dumplogger.info("Wrong http method calling APIController.SendAPIPacket, please check error message!!!")
            ret = 0

    ##Handle ReadTimeOut from http request
    except ReadTimeout:
        dumplogger.exception("Encounter error calling APIController.SendAPIPacket, please check error message!!!")
        ret = -1

    except ConnectionError:
        dumplogger.exception("Encounter connection error when calling APIController.SendAPIPacket, please check error message!!!")
        ret = -1

    OK(ret, int(arg['result']), 'SendHttpRequest -> ' + final_url)
