import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCForm(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCForm Precondition =="
        dumplogger.info("== Setup VNPCForm Precondition ==")

    def test_VNPCForm01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCForm-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCForm Post condition =="
        dumplogger.info("== Setup VNPCForm Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
