import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCPayment Precondition =="
        dumplogger.info("== Setup ESPCPayment Precondition ==")

    def test_ESPCPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCPayment-01.xml")

    def test_ESPCPayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCPayment-02.xml")

    def test_ESPCPayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCPayment-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCPayment Post condition =="
        dumplogger.info("== Setup ESPCPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
