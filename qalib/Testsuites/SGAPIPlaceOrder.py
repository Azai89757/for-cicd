import unittest
from Config import dumplogger
from FrameWorkBase import *


class SGAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup SGAPIPlaceOrder Precondition ==")

    def test_SGAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/SGAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup SGAPIPlaceOrder Post condition ==")
