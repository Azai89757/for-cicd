import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCOrder Precondition =="
        dumplogger.info("== Setup COPCOrder Precondition ==")

    def test_COPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-01.xml")

    def test_COPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-02.xml")

    def test_COPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-03.xml")

    def test_COPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-04.xml")

    def test_COPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-05.xml")

    def test_COPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-06.xml")

    def test_COPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-07.xml")

    def test_COPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-08.xml")

    def test_COPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-09.xml")

    def test_COPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-10.xml")

    def test_COPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCOrder-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCOrder Post condition =="
        dumplogger.info("== Setup COPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
