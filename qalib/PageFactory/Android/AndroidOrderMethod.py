﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidOrderMethod.py: The def of this file called by XML mainly.
'''
##import system library
import re
import time
import datetime

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidOrderCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompareEscrowInfo(arg):
        '''
        CompareEscrowInfo : Compare escrow release info
                Input argu :
                    column - column to compare
                    locate - locate of element
                    string_format - string format of datetime
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column = arg['column']
        locate = arg["locate"]
        string_format = arg['string_format']

        ##Get order valid time
        AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        match = re.match(r".+(\d{2}-\d{2}-\d{4})", GlobalAdapter.CommonVar._PageAttributes_)

        try:
            if match:
                if column == "ship_by_date":
                    if datetime.datetime.fromtimestamp(int(GlobalAdapter.OrderE2EVar._ShipByDate_)).strftime(string_format) != match.group(1):
                        ret = 0
                        dumplogger.info("Ship by date not match, backend: %s, frontend: %s" % (datetime.datetime.fromtimestamp(int(GlobalAdapter.OrderE2EVar._ShipByDate_)).strftime(string_format), match.group(1)))
                elif column == "estimated_delivery_time":
                    if datetime.datetime.fromtimestamp(int(GlobalAdapter.OrderE2EVar._EstimatedDeliveryTime_)).strftime(string_format) != match.group(1):
                        ret = 0
                        dumplogger.info("Estimate delivery time not match, backend: %s, frontend: %s" % (GlobalAdapter.OrderE2EVar._EstimatedDeliveryTime_, match.group(1)))
                elif column == "escrow_release_created_time":
                    if datetime.datetime.fromtimestamp(int(GlobalAdapter.OrderE2EVar._EscrowReleaseCreatedTime_)).strftime(string_format) != match.group(1):
                        ret = 0
                        dumplogger.info("Escrow release created time not match, backend: %s, frontend: %s" % (GlobalAdapter.OrderE2EVar._EscrowReleaseCreatedTime_, match.group(1)))
                else:
                    ret = 0
                    dumplogger.error("Unsupported column, column:" + column)
        except ValueError:
            ret = -1
            dumplogger.exception("Something wrong when convert datetime format.")
        except:
            ret = -1
            dumplogger.exception("Encounter unknown error!")

        OK(ret, int(arg['result']), "AndroidOrderCommon.CompareEscrowInfo")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderValidDate(arg):
        '''
        CheckOrderValidDate : Check order valid date bigger than today
                Input argu :
                    locate - locate of element
                    string_format - string format of datetime
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg["locate"]
        string_format = arg['string_format']

        ##Get valid date
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(string_format), string_format)

        ##Get and check order valid time
        AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Get date from text
        match = re.search(r"(\d{2}-\d{2}-\d{4}\s\d{2}:\d{2}|\d{2}-\d{2}-\d{4}|\d{2}/\d{2}/\d{4}|\d{4}-\d{2}-\d{2}|\d{2} \w{3} \d{4})", GlobalAdapter.CommonVar._PageAttributes_)
        if match:
            valid_date = datetime.datetime.strptime(match.group(1), string_format)

            ##Check if DTS > current date
            if valid_date >= current_date:
                dumplogger.info("%s is bigger than %s!!" % (valid_date, current_date))
            else:
                ret = 0
                dumplogger.info("%s is less than %s!!" % (valid_date, current_date))
        else:
            ret = -1
            dumplogger.info("Did not find match date!!!")

        OK(ret, int(arg['result']), 'AndroidOrderCommon.CheckOrderValidDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderTotalPrice(arg):
        '''
        CheckOrderTotalPrice : Check order total price in all order processing page
                Input argu :
                    locate - locate of element you want to check it display total price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg['locate']

        ##Get element display text (order total price)
        AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Filter order total price by regex. Order total price string rule => Mixed number (0-9) and dot (.) and comma (,) Example: 12,345.00
        ##Regex group 1 => Part of order total price (integer)
        ##Regex group 2 => Part of order total price (decimal)
        ##Regex group 3 => Order total price string boundary
        ##Regex group 4 => Order total price only have integer price
        dumplogger.info("Locate element order total price string => \"%s\"" % (GlobalAdapter.CommonVar._PageAttributes_))
        match = re.search(r"([,\.\d]+)[,\.](\d\d)(\D|\b)|([,\.\d]+)", GlobalAdapter.CommonVar._PageAttributes_)

        ##Check regex is match
        if match:
            ##Regex group 2 will match decimal price. If match, this total price have decimal number. Example: 12,345.00
            if match.group(2):
                ##Remove dot and comma from integer price, and add decimal price. Example: 12,345.00 => 12345.00
                order_total_price_str = match.group(1).replace(",", "").replace(".", "") + "." + match.group(2)

            ##This total price only have integer number. Example: 12,345
            else:
                ##Remove dot and comma from integer price. Example: 12,345 => 12345
                order_total_price_str = match.group(4).replace(",", "").replace(".", "")

            dumplogger.info("After remove dot and comma from integer price => \"%s\"" % (order_total_price_str))

            ##Check checkout price data in global create order data
            if "checkout_price_data" in GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]:

                ##Because checkout get API response will receive additional "00000" number, so we will *100000 to handle it. Example: FE display "12,345" but checkout get API response "1234500000"
                order_total_price = int(float(order_total_price_str) * 100000)

                ##Checkout data total price need to round. Example: 56593810 => 56594000 . Because FE display round number
                dumplogger.info("Checkout data price before round: \"%s\"" % (GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]["checkout_price_data"]["total_payable"]))
                checkout_data_total_price = int(round(GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]["checkout_price_data"]["total_payable"], -3))

                ##Check locate element order total price = checkout data total price
                if order_total_price == checkout_data_total_price:
                    ret = 1
                    dumplogger.info("Check order price pass!!! Locate element price is \"%s\". Checkout data price is \"%s\"" % (order_total_price, checkout_data_total_price))
                else:
                    ret = 0
                    dumplogger.info("Check order price fail!!! Locate element price is \"%s\". Checkout data price is \"%s\"" % (order_total_price, checkout_data_total_price))
            else:
                ret = 0
                dumplogger.info("No any checkout price data in Global variable _CreateOrderData_, maybe your create order by API not run or fail")
        else:
            ret = 0
            dumplogger.info("No any order price string in locate element!! Please check your locate element have mixed number (0-9) and dot (.) and comma (,)")

        OK(ret, int(arg['result']), 'AndroidOrderCommon.CheckOrderTotalPrice')


class AndroidOrderButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeGuaranteeDynamicTooltip(arg):
        '''
        ClickShopeeGuaranteeDynamicTooltip : Click shopee guarantee dynamic tooltips
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "dynamic_tooltip"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click shopee guarantee dynamic tooltip", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickShopeeGuaranteeDynamicTooltip')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExtendShopeeGuarantee(arg):
        '''
        ClickExtendShopeeGuarantee : Click extend shopee guarantee btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "extend_shopee_guarantee_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click extend shopee guarantee btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickExtendShopeeGuarantee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExtendShopeeGuaranteeOptions(arg):
        '''
        ClickExtendShopeeGuaranteeOptions : Click extend shopee guarantee options
                Input argu :
                    options - cancel / ok
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        options = arg["options"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": options})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click extend shopee guarantee btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickExtendShopeeGuaranteeOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditRating(arg):
        '''
        ClickEdit : Click edit rating
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit rating button
        xpath = Util.GetXpath({"locate":"edit_rating_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit rating button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickEditRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTransferNow(arg):
        '''
        ClickTransferNow : Click transfer now button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click transfer now button
        xpath = Util.GetXpath({"locate":"transfer_now"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click transfer now button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickTransferNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyAgain(arg):
        '''
        ClickBuyAgain : Click buy again button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy again
        xpath = Util.GetXpath({"locate":"buy_again"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "0", "result": "1"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click buy again button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickBuyAgain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePayment(arg):
        '''
        ClickChangePayment : Click change payment button in order detail page
                Input argu :
                    type - bottom_section / payment_section
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click change payment button in order detail page
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click change payment button in order detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickChangePayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandPaymentInfo(arg):
        '''
        ClickExpandPaymentInfo : Click down arrow to expand payment info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click down arrow
        xpath = Util.GetXpath({"locate":"expand_btn"})

        ##If button exist, then click down arrow
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click expand button", "result": "1"})
        else:
            dumplogger.info("No expand arrow.")

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickExpandPaymentInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandOrderInfo(arg):
        '''
        ClickExpandOrderInfo : Click down arrow to expand order info
                Input argu :
                    shop_name - Click down arrow to expand order info
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg['shop_name']

        ##Click down arrow
        xpath = Util.GetXpath({"locate":"expand_btn"})
        xpath = xpath.replace('shop_name_to_replace', shop_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click down arrow to expand order info", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickExpandOrderInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyOrderID(arg):
        '''
        ClickCopyOrderID : Click copy btn to copy order id
                Input argu :
                    shop_name - shop name order id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Click copy btn
        xpath = Util.GetXpath({"locate":"copy_btn"})
        xpath = xpath.replace("shop_name_to_replace", shop_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click copy btn to copy order id in order detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCopyOrderID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyAddress(arg):
        '''
        ClickCopyAddress : Click copy btn to copy address info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click copy btn
        xpath = Util.GetXpath({"locate":"copy_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click copy btn to copy address info", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCopyAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOrder(arg):
        '''
        ClickCancelOrder : Click cancel order btn in order detail page
                Input argu :
                    cancel_type - all / single
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cancel_type = arg['cancel_type']

        ##Click cancel order btn
        xpath = Util.GetXpath({"locate":cancel_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel order button in order detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelReason(arg):
        '''
        ClickCancelReason : Click cancel reason radio button in order detail page
                Input argu :
                    reason_option - need_change_delivery_address / need_voucher_code / need_modify_order / payment_trouble / found_cheaper / dont_want_to_buy / seller_no_response / other
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason_option = arg['reason_option']

        ##Click cancel reason radio button in order detail page
        xpath = Util.GetXpath({"locate":reason_option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel reason radio button in order detail page => %s" % (reason_option), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCancelReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click x btn to close popup
                Input argu :
                    page_title - page title of x btn near by
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_title = arg['page_title']

        ##Click cancel order btn
        xpath = Util.GetXpath({"locate":"x_btn"})
        xpath = xpath.replace("title_to_replace", page_title)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click x btn to close popup", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupWindow(arg):
        '''
        ClickPopupWindow : Click popup window buttons to dismiss or confirm
                Input argu :
                    action_type - dismiss / agree
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg['action_type']

        ##Click popup window btn
        xpath = Util.GetXpath({"locate":action_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click popup window btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickPopupWindow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandBillInfo(arg):
        '''
        ClickExpandBillInfo : Click ＾ btn to expand bill info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ＾ btn on expand bill info
        xpath = Util.GetXpath({"locate":"expand_arrow"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to expand bill info", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickExpandBillInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopy(arg):
        '''
        ClickCopy : Click copy button in order detail page
                Input argu :
                    button_type - delivery_copy / order_sn_copy
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click copy button in order detail page
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click copy button in order detail page => %s" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCopy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditAddress(arg):
        '''
        ClickEditAddress : Click edit address in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit address button
        xpath = Util.GetXpath({"locate":"edit_address"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit address button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickEditAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProcessTab(arg):
        '''
        ClickProcessTab : Click return/refund process tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok
        xpath = Util.GetXpath({"locate":"ok"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click process tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickProcessTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelfDeliver(arg):
        '''
        ClickSelfDeliver : Click self deliver button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click self deliver
        xpath = Util.GetXpath({"locate":"ship"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click self deliver button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickSelfDeliver')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewRating(arg):
        '''
        ClickViewRating : Click view rating
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view rating
        xpath = Util.GetXpath({"locate":"view_rating_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click view rating btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickViewRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewShippingInfo(arg):
        '''
        ClickViewShippingInfo : Click view shipping info button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view shipping info button
        xpath = Util.GetXpath({"locate":"shipping_info_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click view shipping info button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickViewShippingInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewReturnRefundDetail(arg):
        '''
        ClickViewReturnRefundDetail : Click view return refund detail button
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click view return refund detail button
        xpath = Util.GetXpath({"locate":"rr_detail_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click view return refund detail button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickViewReturnRefundDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderDetailProduct(arg):
        '''
        ClickOrderDetailProduct : Click order detail product
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order detail product
        xpath = Util.GetXpath({"locate":"order_detail_product_section"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click order detail product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickOrderDetailProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderListTab(arg):
        '''
        ClickOrderListTab : Click order list tab in order list page
                Input argu :
                    order_status - topay / toship / toreceive / completed / cancelled / toreturn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order_status = arg["order_status"]

        ##Check device cpu usage is stable
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

        ##Click order list tab in order list page
        xpath = Util.GetXpath({"locate":order_status})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click order list tab in order list page => %s" % (order_status), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickOrderListTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderRating(arg):
        '''
        ClickOrderRating : Click order rating
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Order rating
        xpath = Util.GetXpath({"locate":"btn_rate"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Order rating", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickOrderRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderReceived(arg):
        '''
        ClickOrderReceived : Click order received button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order received button
        xpath = Util.GetXpath({"locate":"received"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click order received", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickOrderReceived')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectCoinNow(arg):
        '''
        ClickCollectCoinNow : Click coin collect now
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click coin collect now button
        xpath = Util.GetXpath({"locate":"collect_now_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click coin collect now", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCollectCoinNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu :
                    type - order_received / buyer_rating / seller_rating / cancel_order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Check confirm button exist
        xpath = Util.GetXpath({"locate":type})

        ##If button exist, then click confirm
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})
        else:
            dumplogger.info("No confirm popup.")

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturn(arg):
        '''
        ClickReturn : Click return button on order detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order return button
        xpath = Util.GetXpath({"locate":"return_refound_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click return button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRequestCancelOrder(arg):
        '''
        ClickRequestCancelOrder : Click request cancel order button on order detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click request cancel button
        xpath = Util.GetXpath({"locate":"request_cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click request cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickRequestCancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRaiseDispute(arg):
        '''
        ClickRaiseDispute : Click raise dispute of this order button on order list
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click raise dispute button
        xpath = Util.GetXpath({"locate":"raise_dispute_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click RaiseDispute button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickRaiseDispute')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPayNow(arg):
        '''
        ClickPayNow : Click pay now in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pay now btn
        xpath = Util.GetXpath({"locate": "pay_now_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click pay now btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickPayNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefundReason(arg):
        '''
        ClickRefundReason : Click refund reason in return/refund page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refund reason
        xpath = Util.GetXpath({"locate": "choose_reason"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click refund reason", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickRefundReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContact(arg):
        '''
        ClickContact : Click Contact button in order detail page
                Input argu :
                    type - contact_buyer, contact_seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click contact btn
        xpath = Util.GetXpath({"locate": type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click contact btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickContact')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShop(arg):
        '''
        ClickGoShop : Click go to shop in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click goto shop btn
        xpath = Util.GetXpath({"locate": "go_shop"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click shop", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickGoShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNote(arg):
        '''
        ClickAddNote : Click add note in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add note btn
        xpath = Util.GetXpath({"locate": "add_note"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add note btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickAddNote')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddAddress(arg):
        '''
        ClickAddAddress : Click add an address in my address page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Move down and click add address btn
        locate = Util.GetXpath({"locate": "add_address_icon"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": locate, "direction": "down", "isclick": "1", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickAddAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBankTransferNow(arg):
        '''
        ClickBankTransferNow : Click bank transfer now in order list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Banktransfer now btn
        xpath = Util.GetXpath({"locate": "bank_transfer_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click bank transfer btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickBankTransferNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeGuarantee(arg):
        '''
        ClickShopeeGuarantee : Click Shopee Guarantee in order list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Shopee guarantee icon
        xpath = Util.GetXpath({"locate": "shopee_guarantee"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click Shopee guarantee icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickShopeeGuarantee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReceive(arg):
        '''
        ClickReceive : Click Receive in order list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click receive btn
        xpath = Util.GetXpath({"locate": "receive_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click Receive btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickReceive')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok btn
                Input argu :
                    type - ship_ok_btn, add_note_ok, ok, return_refund_ok, buy_again_ok, model_confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click ok btn
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLearnMore(arg):
        '''
        ClickLearnMore : Click learn more button in order page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click learn more button in order page
        xpath = Util.GetXpath({"locate": "learn_more_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click learn more button in order page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickLearnMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancellationDetail(arg):
        '''
        ClickCancellationDetail : Click cancellation detail btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancellation detail btn
        xpath = Util.GetXpath({"locate": "cancel_detail_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cancel detail btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickCancellationDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHandlingFeeQuestionMark(arg):
        '''
        ClickHandlingFeeQuestionMark : Click handling fee question mark in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click handling fee question mark
        xpath = Util.GetXpath({"locate": "question_mark_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click handling fee question mark", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderButton.ClickHandlingFeeQuestionMark')


class AndroidOrderListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderListProduct(arg):
        '''
        ClickOrderListProduct : Click order product
                Input argu :
                    name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']

        ##Get order list product XPath
        xpath = Util.GetXpath({"locate":"product_name"})
        xpath = xpath.replace('replace_name', name)

        ##If emulator is Espresso, use specific xpath
        if "Espresso" in Config._DesiredCaps_.values():
            xpath = xpath.replace(")[1]", "[@visible='true'])[1]")

        ##Check device cpu usage is stable
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

        ##Click order list product
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click order product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderListPage.ClickOrderListProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchOrderTab(arg):
        '''
        SwitchOrderTab : Switch tab in OrderListPage
                Input argu :
                    type - which order status tab you want to switch => topay / toship / toreceive / completed / cancelled / toreturn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##if tab in page 1
        if type in ("topay", "toship", "toreceive"):
            ##Switch order tab
            AndroidOrderButton.ClickOrderListTab({"order_status": type, "result": "1"})
        ##if tab in page 2
        else:
            ##Click view all
            locate = Util.GetXpath({"locate":"view_all"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click to view all tab page", "result": "1"})
            time.sleep(2)
            ##avoid cancelled tab not in page
            AndroidOrderButton.ClickOrderListTab({"order_status": "toreceive", "result": "1"})
            time.sleep(2)
            AndroidOrderButton.ClickOrderListTab({"order_status": "completed", "result": "1"})
            time.sleep(3)
            AndroidOrderButton.ClickOrderListTab({"order_status": type, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderListPage.SwitchOrderTab')


class AndroidOrderDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelOrder(arg):
        '''
        CancelOrder : Cancel Order
                Input argu :
                    role - buyer / seller
                    order_status - topay / toship
                    reason - soldout / other
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        role = arg["role"]
        order_status = arg["order_status"]
        reason = arg['reason']

        ##Click order cancel button
        xpath = Util.GetXpath({"locate":"cancel_order"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down","times":"3", "result": "1"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"ClickOrderCancel button", "result": "1"})

        if role == "buyer" and order_status == "topay":
            ##Choose cancel reason
            xpath = Util.GetXpath({"locate":"buyer_cancel_reason_topay"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose cancel reason", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"buyer_cancel_order_topay"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"ClickOrderCancel confirm", "result": "1"})

        elif role == "buyer" and order_status == "toship":
            ##Choose cancel reason
            xpath = Util.GetXpath({"locate":"buyer_cancel_reason_toship"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose cancel reason", "result": "1"})

            ##Click cancel confirm
            xpath = Util.GetXpath({"locate":"buyer_cancel_order_toship"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"ClickOrderCancel confirm", "result": "1"})

        elif role == "seller" and order_status == "toship":
            ##Click cancel list
            xpath = Util.GetXpath({"locate":"seller_cancel_list_toship"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel list", "result": "1"})

            if reason == "soldout":
                ##Click cancel reason
                xpath = Util.GetXpath({"locate": "seller_cancel_soldout_reason_toship"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cancel reason", "result": "1"})

                ##Click product checkbox
                time.sleep(3)
                xpath = Util.GetXpath({"locate": "product_checkbox"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click product checkbox", "result": "1"})

                ##Click submit
                time.sleep(3)
                xpath = Util.GetXpath({"locate": "submit"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click submit", "result": "1"})
            else:
                ##Click cancel reason
                xpath = Util.GetXpath({"locate": "seller_cancel_reason_toship"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cancel reason", "result": "1"})

            ##Click submit
            time.sleep(10)
            xpath = Util.GetXpath({"locate":"submit"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.CancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderValidDate(arg):
        '''
        CheckOrderValidDate : Check order valid date bigger than today
                Input argu :
                    locate - locate of element
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        locate = arg["locate"]

        if Config._TestCaseRegion_ == "TW":
            date_format = "%Y-%m-%d"
        else:
            date_format = "%d-%m-%Y"
        ##Get valid date
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(date_format), date_format)

        ##Get and check order valid time
        AndroidBaseUICore.AndroidGetElements({"locate":locate, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Get date from text
        match = re.search(r"(\d{2}-\d{2}-\d{4}|\d{4}-\d{2}-\d{2})", GlobalAdapter.CommonVar._PageAttributes_)
        if match:
            valid_date = datetime.datetime.strptime(match.group(1), date_format)

            ##Check if DTS > current date
            if valid_date >= current_date:
                dumplogger.info("%s is bigger than %s!!" % (valid_date, current_date))
            else:
                ret = 0
                dumplogger.info("%s is less than %s!!" % (valid_date, current_date))
        else:
            ret = 0
            dumplogger.info("Did not found date info.")

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.CheckOrderValidDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCancelReason(arg):
        '''
        ChooseCancelReason : Buyer cancel the order in to ship
                Input argu :
                    cancel_reason - which reason for cancel (reason1/reason2.../reason7)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cancel_reason = arg["cancel_reason"]

        dumplogger.info("cancel_reason = " + cancel_reason)
        locate = Util.GetXpath({"locate":cancel_reason})

        if "reason" in cancel_reason:
            ##Click different reasons
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Choose cancel reason", "result": "1"})
        else:
            ##Click first reason as cancel reason as default
            locate = Util.GetXpath({"locate":"reason1"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Choose cancel reason 1 as default", "result": "1"})

        ##Check the cancel order button is exist
        xpath = Util.GetXpath({"locate":"cancel_order_btn"})

        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "message":"Check popup message", "passok": "0", "result": "1"}):
            ##Click Cancel btn
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":Util.GetXpath({"locate":"cancel_order_btn"}), "message":"Click cancel order btn", "result": "1"})

        ##Click confirm
        time.sleep(5)
        AndroidOrderButton.ClickConfirm({"type":"confirm_btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ChooseCancelReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ArrangePickupProduct(arg):
        '''
        ArrangePickupProduct : Click arrange pickup product
                Input argu :
                    shipping_method - 711/blackcat/hilife/family/ok/chinapost/selfdelivery/viettelpost
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shipping_method = arg['shipping_method']

        ##Click arrange pickup
        xpath = Util.GetXpath({"locate":"btn_arrange_pickup"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click arrange pickup", "result": "1"})

        if Config._TestCaseRegion_ == "TW":
            if shipping_method == 'blackcat':
                ##Click choose date
                xpath = Util.GetXpath({"locate":"choose_date_list"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click list", "result": "1"})

                ##Click confirm to pickup
                xpath = Util.GetXpath({"locate":"confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
            elif shipping_method != 'chinapost' and shipping_method != 'selfdelivery' and shipping_method != 'cb_711':
                xpath = Util.GetXpath({"locate":"real_name"})
                AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":"TWQA", "result": "1"})

                ##Click confirm
                xpath = Util.GetXpath({"locate":"confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
                time.sleep(3)
            else:
                ##Click confirm to pickup
                xpath = Util.GetXpath({"locate":"confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        elif Config._TestCaseRegion_ == "VN":
            if shipping_method == 'viettelpost' or shipping_method == 'GHN' or shipping_method == 'GHTK':
                ##Chose Viettel Post
                xpath = Util.GetXpath({"locate":"viettel_post"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click arrange pickup option", "result": "1"})
                time.sleep(3)

                ##Chose pikup date
                xpath = Util.GetXpath({"locate":"pikup_date"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click arrange pickup sub option", "result": "1"})
                time.sleep(2)

                ##Chose frist date
                xpath = Util.GetXpath({"locate":"day_1"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click pickup day", "result": "1"})
                time.sleep(5)
            else:
                ##Enter real name
                xpath = Util.GetXpath({"locate":"real_name"})
                AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":"TWQA", "result": "1"})

            ##Click confirm to pickup
            xpath = Util.GetXpath({"locate":"confirm"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
            time.sleep(5)

            ##Click ok to pickup
            AndroidOrderButton.ClickConfirm({"type": "ok", "result": "1"})
            time.sleep(5)

        elif Config._TestCaseRegion_ == "TH":
            if shipping_method == 'thailandpostems':
                ##Input Trackingode
                xpath = Util.GetXpath({"locate":"tracking_code"})
                AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":'EA056301134TH', "result": "1"})

                ##Click confirm
                xpath = Util.GetXpath({"locate":"btn_trackingcode_confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
                time.sleep(5)
            elif shipping_method == 'th_registeredmall':
                ##Input Trackingode
                xpath = Util.GetXpath({"locate":"tracking_code"})
                AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":'EA056301134TH', "result": "1"})

                ##Click confirm
                xpath = Util.GetXpath({"locate":"btn_trackingcode_confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
                time.sleep(5)
            elif shipping_method == 'kerrypost':
                ##Click create tracking qrcode
                xpath = Util.GetXpath({"locate":"qrcode_btn"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click create qrcode_btn", "result": "1"})
                time.sleep(5)

                ##Click kerry tracking page confirm btn
                xpath = Util.GetXpath({"locate":"confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
                time.sleep(5)
            elif shipping_method == 'aCommerce':
                ##Click aCommerce select ship date btn
                xpath = Util.GetXpath({"locate":"aCommerce_ship_btn"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm btn", "result": "1"})
                time.sleep(5)
            else:
                ##Click confirm
                xpath = Util.GetXpath({"locate":"btn_trackingcode_confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ArrangePickupProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDeliverInfo(arg):
        '''
        InputDeliverInfo : Fill deliver info
                Input argu :
                    length - random string length
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        length = arg["length"]

        GlobalAdapter.GeneralE2EVar._RandomString_ = XtFunc.GenerateRandomString(int(length), "characters")

        ##TW region need to select self deliver method first, it's different with onther countries
        if Config._TestCaseRegion_ == "TH":
            ##Click shipping
            xpath = Util.GetXpath({"locate":"ship"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click deliver", "result": "1"})
        else:
            ##Input trace number
            xpath = Util.GetXpath({"locate":"shipping_traceno"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.GeneralE2EVar._RandomString_, "result": "1"})
            time.sleep(5)

            ##Click shipping
            xpath = Util.GetXpath({"locate":"ship"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click deliver", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.InputDeliverInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundProductCheckbox(arg):
        '''
        ClickReturnRefundProductCheckbox : Click return refund product checkbox
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click return refund product checkbox
        xpath = Util.GetXpath({"locate":"product_name_checkbox"})
        xpath_product_name_checkbox = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_product_name_checkbox, "message":"Click return refund product checkbox", "result": "1"})

        ##Record return product name to prepare retry return refund
        GlobalAdapter.OrderE2EVar._ReturnProductNameList_.append(product_name)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickReturnRefundProductCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundPromotionProductCheckbox(arg):
        '''
        ClickReturnRefundPromotionProductCheckbox : Click product with promotion tag checkbox on r/r page
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click product with promotion tag checkbox on r/r page
        xpath = Util.GetXpath({"locate":"promotion_product_name_checkbox"})
        xpath_product_name_checkbox = xpath.replace("promotion_product_name_checkbox", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_product_name_checkbox, "message":"Click return refund product checkbox", "result": "1"})

        ##Record return product name to prepare retry return refund
        GlobalAdapter.OrderE2EVar._ReturnProductNameList_.append(product_name)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickReturnRefundPromotionProductCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReturnRefundInfo(arg):
        '''
        InputReturnRefundInfo : Input return/refund info
                Input argu :
                    reason - reason
                    upload_image - Upload return/refund image, input "1" to process, or input "0" to ignore
                    choose_shipment - Choose return/refund shipment, input "1" to process, or input "0" to ignore
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason = arg['reason']
        upload_image = arg['upload_image']
        choose_shipment = arg['choose_shipment']

        ##Click to chose reason
        xpath = Util.GetXpath({"locate":"reason"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to chose reason", "result": "1"})
        time.sleep(3)

        ##Click a reason
        xpath = Util.GetXpath({"locate":reason})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click a reason", "result": "1"})
        time.sleep(3)

        ##Upload return/refund image
        if int(upload_image):
            ##Click to upload image
            xpath = Util.GetXpath({"locate":"add_photo"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})

            ##Click to upload from album
            xpath = Util.GetXpath({"locate":"from_album"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})

            ##Click first image
            xpath = Util.GetXpath({"locate":"image"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})

            ##Click check
            xpath = Util.GetXpath({"locate":"check"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click check", "result": "1"})

        ##Choose return/refund shipment
        if int(choose_shipment):
            ##Select shipping option
            xpath = Util.GetXpath({"locate":"shipping_option"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click shipping option", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        time.sleep(3)

        ##Swipe down to the button of the page
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"2", "result": "1"})

        ##Input email
        xpath = Util.GetXpath({"locate":"email"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":"test@test.com", "result": "1"})

        ##Click submit
        AndroidOrderButton.ClickConfirm({"type": "submit", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.InputReturnRefundInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseReturnRefund(arg):
        '''
        ChooseReturnRefund : Choose return/refund
                Input argu :
                    reason - reason
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason = arg['reason']

        ##Click to chose reason
        xpath = Util.GetXpath({"locate":"reason"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to chose reason", "result": "1"})

        ##Click a reason
        xpath = Util.GetXpath({"locate":reason})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click a reason", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ChooseReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AcceptRefundBySeller(arg):
        '''
        AcceptRefundBySeller : Seller accept refund
                Input argu :
                    refund_type - btn_refund_without_return / btn_refund
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        refund_type = arg['refund_type']

        ##Click accept refund
        xpath = Util.GetXpath({"locate":refund_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click accept refund", "result": "1"})

        ##Check if refund now btn exit
        xpath = Util.GetXpath({"locate":"refund_now"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click refund now (TW refund flow don't need to click this step)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click refund now", "result": "1"})
        else:
            dumplogger.info('No need to click refound now, keep executing.')

        ##Click confirm
        AndroidOrderButton.ClickConfirm({"type":"confirm_btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.AcceptRefundBySeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AppealReturnBySeller(arg):
        '''
        AppealReturnBySeller : Seller appeal return
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click appeal
        xpath = Util.GetXpath({"locate":"appeal"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click appeal", "result": "1"})

        ##Select reason
        xpath = Util.GetXpath({"locate":"reason_1"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click reason", "result": "1"})

        ##Click to select image
        xpath = Util.GetXpath({"locate":"add_photo"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to select image", "result": "1"})

        ##Click select from album
        xpath = Util.GetXpath({"locate":"from_album"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click select from album", "result": "1"})

        ##Click first image
        xpath = Util.GetXpath({"locate":"image_1"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})

        ##Click confirm
        AndroidOrderButton.ClickConfirm({"type": "confirm", "result": "1"})

        ##Input email
        xpath = Util.GetXpath({"locate":"email"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":"test@test.com", "result": "1"})

        time.sleep(3)

        ##Click submit
        xpath = Util.GetXpath({"locate":"submit"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        ##Click ok
        AndroidOrderButton.ClickConfirm({"type": "confirm_btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.AppealReturnBySeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundSeller(arg):
        '''
        ClickReturnRefundSeller : Click return/refund seller when order more than 1 products from same seller
                Input argu :
                    seller - seller name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller = arg["seller"]

        ##Click choose return refund seller
        xpath = Util.GetXpath({"locate":"choose_seller"})
        xpath = xpath.replace("seller_name_to_be_replace", seller)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to choose return refund seller", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickReturnRefundSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundItem(arg):
        '''
        ClickReturnRefundItem : Click return/refund item when order more than 1 products
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click choose return refund seller
        xpath = Util.GetXpath({"locate":"choose_product"}).replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to choose return refund product", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickReturnRefundItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRatingStar(arg):
        '''
        ClickRatingStar : Click 5 star
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"5_star"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click 5 star", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickRatingStar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseNewAddress(arg):
        '''
        ChooseNewAddress : Choose another address in edit page
                Input argu :
                    name - recipient of address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']

        ##Choose another address
        xpath = Util.GetXpath({"locate":"new_address"})
        xpath = xpath.replace('replace_name', name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"choose new address", "result": "1"})
        time.sleep(2)

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ChooseNewAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseAddress(arg):
        '''
        ChooseAddress : Choose specific address
                Input argu :
                    address_name - name of the address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        address_name = arg["address_name"]

        ##Choose Address
        locate = Util.GetXpath({"locate":"address_name"})
        locate = locate.replace("address_name_to_replaced", address_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Choose specific address", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ChooseAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundDetail(arg):
        '''
        ClickReturnRefundDetail : Click return refund detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate": "return_refund_detail"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click return refund detail", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickReturnRefundDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeIncomeDetails(arg):
        '''
        ClickSeeIncomeDetails : Click see income details in order detail page to my income page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see income details in order detail page to my income page
        xpath = Util.GetXpath({"locate":"see_income_details"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see income details text", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickSeeIncomeDetails')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadRatingPhoto(arg):
        '''
        UploadRatingPhoto : Upload photo in rating page
                Input argu :
                    role - seller / buyer
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        role = arg['role']

        ##Click to upload image
        xpath = Util.GetXpath({"locate":"add_photo"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})
        time.sleep(2)

        if role == 'buyer':
            ##Click to upload from album
            xpath = Util.GetXpath({"locate":"from_album"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})
            time.sleep(2)

            ##Click first image
            xpath = Util.GetXpath({"locate":"image"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})
            time.sleep(2)

            ##Click next
            xpath = Util.GetXpath({"locate":"next"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next", "result": "1"})
            time.sleep(2)

            ##Click upload
            xpath = Util.GetXpath({"locate":"upload"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload", "result": "1"})
            time.sleep(2)

        elif role == 'seller':
            ##Click to upload from album
            xpath = Util.GetXpath({"locate":"seller_from_album"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})
            time.sleep(2)

            ##Click first image
            xpath = Util.GetXpath({"locate":"seller_image"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})
            time.sleep(2)

            ##Click next
            xpath = Util.GetXpath({"locate":"seller_next"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.UploadRatingPhoto')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UnfoldAnotherOrder(arg):
        '''
        UnfoldAnotherOrder : Click another order tab
                Input argu :
                    seller_name - seller name of order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_name = arg['seller_name']

        ##Click another order tab
        xpath = Util.GetXpath({"locate":"order_tab"})
        xpath = xpath.replace('seller_name_to_be_replaced', seller_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click another order tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.UnfoldAnotherOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExtendShopeeGuarantee(arg):
        '''
        ClickExtendShopeeGuarantee : Click extend shopee guarantee in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click extend shopee guarantee in order detail page
        xpath = Util.GetXpath({"locate":"extend_guarantee"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click extend shopee guarantee in order detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.ClickExtendShopeeGuarantee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelReturnRequest(arg):
        '''
        CancelReturnRequest : Click cancel return button on order detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order cancel return button
        xpath = Util.GetXpath({"locate":"cancel_return_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Cancel return button", "result": "1"})
        time.sleep(2)

        ##Check ok button exist
        xpath = Util.GetXpath({"locate":"ok_btn"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click ok button
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})
        else:
            dumplogger.info("Don't need click")

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.CancelReturnRequest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RetryReturnRefundRequest(arg):
        '''
        RetryReturnRefundRequest : Retry return/refund request
                Input argu :
                    reason - reason
                    upload_image - Upload return/refund image, input "1" to process, or input "0" to ignore
                    choose_shipment - Choose return/refund shipment, input "1" to process, or input "0" to ignore
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason = arg['reason']
        upload_image = arg['upload_image']
        choose_shipment = arg['choose_shipment']

        ##Check if error message show up
        xpath = Util.GetXpath({"locate":"request_failed_msg"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

            AndroidOrderDetailPage.CancelReturnRequest({"result": "1"})
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype": "back", "result": "1"})
            AndroidOrderButton.ClickReturn({"result": "1"})

            ##Choose all target return product, record in OrderE2EVar._ReturnProductNameList_
            for return_product_name in GlobalAdapter.OrderE2EVar._ReturnProductNameList_:
                ##Click return refund product checkbox
                xpath = Util.GetXpath({"locate":"product_name_checkbox"})
                xpath_product_name_checkbox = xpath.replace("product_name_to_be_replace", return_product_name)
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_product_name_checkbox, "message":"Click return refund product checkbox", "result": "1"})

            ##Click to chose reason
            xpath = Util.GetXpath({"locate":"reason"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to chose reason", "result": "1"})
            time.sleep(3)

            ##Click a reason
            xpath = Util.GetXpath({"locate":reason})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click a reason", "result": "1"})
            time.sleep(3)

            ##Upload return/refund image
            if int(upload_image):
                ##Click to upload image
                xpath = Util.GetXpath({"locate":"add_photo"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})

                ##Click to upload from album
                xpath = Util.GetXpath({"locate":"from_album"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})

                ##Click first image
                xpath = Util.GetXpath({"locate":"image"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})

                ##Click check
                xpath = Util.GetXpath({"locate":"check"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click check", "result": "1"})

            ##Choose return/refund shipment
            if int(choose_shipment):
                ##Select shipping option
                xpath = Util.GetXpath({"locate":"shipping_option"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click shipping option", "result": "1"})

                ##Click confirm
                xpath = Util.GetXpath({"locate":"confirm"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

            time.sleep(3)

            ##Swipe down to the button of the page
            AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"2", "result": "1"})

            ##Input email
            xpath = Util.GetXpath({"locate":"email"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":"test@test.com", "result": "1"})

            ##Click submit
            AndroidOrderButton.ClickConfirm({"type": "submit", "result": "1"})
            AndroidOrderButton.ClickOK({"type": "return_refund_ok", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidOrderDetailPage.RetryReturnRefundRequest')
