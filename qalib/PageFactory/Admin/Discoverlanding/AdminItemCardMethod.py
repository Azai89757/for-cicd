#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminItemCardMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminItemCardDisplayPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputName(arg):
        '''
        InputName : Input the customized name on the top section
                Input argu :
                    name_string - name string
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name_string = arg['name_string']

        ##Input the customized name on the top section
        name_input = Util.GetXpath({"locate":"name_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":name_input, "result": "1"})
        BaseUICore.Input({"locate":name_input, "string":name_string, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.InputName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadDisplayImage(arg):
        '''
        UploadDisplayImage : Upload display image
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        file_type = arg['file_type']

        ##Upload display image
        xpath = Util.GetXpath({"locate":"upload_path"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.UploadDisplayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductLabels(arg):
        '''
        SelectProductLabels : Select product labels
                Input argu :
                    product_label - product label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_label = arg['product_label']

        ##Click product label selector
        xpath = Util.GetXpath({"locate":"label_input"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product label selector", "result": "1"})

        ##Input label name
        xpath = Util.GetXpath({"locate": "input_text"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": product_label, "method":"xpath", "result": "1"})

        ##Select label
        xpath = Util.GetXpath({"locate":"label_name"})
        xpath = xpath.replace('replaced_text', product_label)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select product label", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.SelectProductLabels')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCampaignImage(arg):
        '''
        UploadCampaignImage : Upload campaign image
                Input argu :
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        file_type = arg['file_type']

        ##Upload campaign image
        xpath = Util.GetXpath({"locate":"campaign_upload_path"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.UploadCampaignImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteImageFlag(arg):
        '''
        DeleteImageFlag : Delete image flag
                Input argu :
                    image_flag_name - image flag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_flag_name = arg['image_flag_name']

        ##Click delete image flag
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace('replaced_text', image_flag_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete image flag", "result": "1"})

        ##Click confirm delete button
        xpath = Util.GetXpath({"locate": "confirm_delete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.DeleteImageFlag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayEnglishText(arg):
        '''
        InputDisplayEnglishText : Input display text in english
                Input argu :
                    english_string - display text string in english
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        english_string = arg['english_string']

        ##Input display text in english
        xpath = Util.GetXpath({"locate": "display_text_english"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": english_string, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.InputDisplayEnglishText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayLocalText(arg):
        '''
        InputDisplayLocalText : Input display text in local language
                Input argu :
                    local_language_string - display text string in local language
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        local_language_string = arg['local_language_string']

        ##Input display text in local language
        xpath = Util.GetXpath({"locate": "display_text_local"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": local_language_string, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.InputDisplayLocalText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayChineseText(arg):
        '''
        InputDisplayChineseText : Input display text in chinese
                Input argu :
                    chinese_language_string - display text string in chinese
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        chinese_language_string = arg["chinese_language_string"]

        ##Input display text in chinese
        xpath = Util.GetXpath({"locate": "display_text_chinese"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": chinese_language_string, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.InputDisplayChineseText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputColourCode(arg):
        '''
        InputColourCode : Input colour code
                Input argu :
                    colour_code - colour code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        colour_code = arg['colour_code']

        ##Input colour code
        xpath = Util.GetXpath({"locate": "colour_code"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": colour_code, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.InputColourCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCustomPriorityOrder(arg):
        '''
        ChangeCustomPriorityOrder : Change custom component priority order
                Input argu :
                    original_component_name - original component name
                    target_component_name - target component name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        original_component_name = arg['original_component_name']
        target_component_name = arg['target_component_name']

        ##Get target and original component name
        origin_xpath = Util.GetXpath({"locate": "origin_element"})
        origin_xpath = origin_xpath.replace('replaced_text', original_component_name)
        target_xpath = Util.GetXpath({"locate": "target_element"})
        target_xpath = target_xpath.replace('replaced_text', target_component_name)

        ##Reorder these two component priority
        BaseUICore.DragAndDrop({"drag_elem": origin_xpath, "target_elem": target_xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.ChangeCustomPriorityOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectStartTime(arg):
        '''
        SelectStartTime : Select start time
                Input argu :
                    start_time - start time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg['start_time']

        ##Select start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": start_time, "method":"xpath", "result": "1"})

        ##Click OK button
        xpath = Util.GetXpath({"locate": "start_time_ok"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.SelectStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectEndTime(arg):
        '''
        SelectEndTime : Select end time
                Input argu :
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = arg['end_time']

        ##Select end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"locate": xpath, "string": end_time, "method":"xpath", "result": "1"})

        ##Click OK button
        xpath = Util.GetXpath({"locate": "end_time_ok"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.SelectEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCurrentStartTime(arg):
        '''
        SelectCurrentStartTime : Select current start time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select current start time
        xpath = Util.GetXpath({"locate":"start_time"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time", "result": "1"})
        xpath = Util.GetXpath({"locate":"now_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.SelectCurrentStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCurrentEndTime(arg):
        '''
        SelectCurrentEndTime : Select current end time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select current end time
        xpath = Util.GetXpath({"locate":"end_time"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time", "result": "1"})
        xpath = Util.GetXpath({"locate":"now_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayPage.SelectCurrentEndTime')


class AdminItemCardDisplayButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItemCard(arg):
        '''
        ClickEditItemCard : Click edit item card button
                Input argu :
                    customized_name - customized name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        customized_name = arg['customized_name']

        ##Click edit item card button
        xpath = Util.GetXpath({"locate":"edit_button"})
        xpath = xpath.replace('replaced_text', customized_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click item card edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickEditItemCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewImage(arg):
        '''
        ClickAddNewImage : Click add new image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new image
        xpath = Util.GetXpath({"locate":"add_new_image"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new image", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickAddNewImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductLabelsSelector(arg):
        '''
        ClickProductLabelsSelector : Click product labels selector
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product labels selector
        xpath = Util.GetXpath({"locate":"product_labels_selector"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product labels selector", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickProductLabelsSelector')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisibilityToggle(arg):
        '''
        ClickVisibilityToggle : Click visibility toggle
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click visibility toggle
        xpath = Util.GetXpath({"locate":"visibility_toggle"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click visibility toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickVisibilityToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeletDisplayImage(arg):
        '''
        ClickDeletDisplayImage : Click delete display image icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete display image icon
        xpath = Util.GetXpath({"locate":"delete_display_img"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete display image icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickDeletDisplayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseFromImageStock(arg):
        '''
        ClickChooseFromImageStock : Click choose from image stock button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose from image stock button
        xpath = Util.GetXpath({"locate":"img_stock"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose from image stock button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickChooseFromImageStock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseProductLabel(arg):
        '''
        ClickCloseProductLabel : Click close product label
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close product label
        xpath = Util.GetXpath({"locate":"close_label"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click close product label", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickCloseProductLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditImageFlag(arg):
        '''
        ClickEditImageFlag : Click edit image flag
                Input argu :
                    image_flag_name - image flag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_flag_name = arg['image_flag_name']

        ##Click edit image flag button
        xpath = Util.GetXpath({"locate":"edit_img_btn"})
        xpath = xpath.replace('replaced_text', image_flag_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit image button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickEditImageFlag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTopBarSection(arg):
        '''
        ClickTopBarSection : Click top bar section
                Input argu :
                    category - product element on the top section bar
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category = arg['category']

        ##Click top bar section
        xpath = Util.GetXpath({"locate":"category_tab"})
        xpath = xpath.replace('replaced_text', category)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click top bar section" + category, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickTopBarSection' + category)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReorder(arg):
        '''
        ClickReorder : Click reorder button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reorder button
        xpath = Util.GetXpath({"locate":"reorder_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click reorder button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickReorder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEndTimeToggle(arg):
        '''
        ClickEndTimeToggle : Click end time toggle
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click end time toggle
        xpath = Util.GetXpath({"locate":"end_time_toggle"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemCardDisplayButton.ClickEndTimeToggle')
