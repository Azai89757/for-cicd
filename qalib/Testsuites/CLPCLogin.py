import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCLogin Precondition =="
        dumplogger.info("== Setup CLPCLogin Precondition ==")

    def test_CLPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCLogin-01.xml")

    def test_CLPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCLogin Post condition =="
        dumplogger.info("== Setup CLPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
