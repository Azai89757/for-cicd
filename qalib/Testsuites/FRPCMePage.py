import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCMePage Precondition =="
        dumplogger.info("== Setup FRPCMePage Precondition ==")

    def test_FRPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCMePage Post condition =="
        dumplogger.info("== Setup FRPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
