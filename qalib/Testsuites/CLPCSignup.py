import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCSignup Precondition =="
        dumplogger.info("== Setup CLPCSignup Precondition ==")

    def test_CLPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCSignup Post condition =="
        dumplogger.info("== Setup CLPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
