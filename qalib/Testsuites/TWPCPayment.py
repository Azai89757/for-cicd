import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCPayment Precondition =="
        dumplogger.info("== Setup TWPCPayment Precondition ==")

    def test_TWPCPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-01.xml")

    def test_TWPCPayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-02.xml")

    def test_TWPCPayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-03.xml")

    def test_TWPCPayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-04.xml")

    def test_TWPCPayment05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-05.xml")

    def test_TWPCPayment06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-06.xml")

    def test_TWPCPayment07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCPayment-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCPayment Post condition =="
        dumplogger.info("== Setup TWPCPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
