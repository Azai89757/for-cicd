#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidPurchaseWithPurchaseMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidPurchaseWithPurchaseSectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainItemSection(arg):
        '''
        ClickMainItemSection : Click main item section on PDP's PWP section
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click main item section on PDP's PWP section
        xpath = Util.GetXpath({"locate": "main_item"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click main item section on PDP PWP section", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseSectionPage.ClickMainItemSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubItemSection(arg):
        '''
        ClickSubItemSection : Click sub item section on PDP's PWP section
                Input argu :
                    main_item_qty - main item quantity
                    sub_item_qty - sub item quantity
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        main_item_qty = int(arg["main_item_qty"])
        sub_item_qty = int(arg["sub_item_qty"])

        ##calculate the total quantity of all the items
        total_item_qty = main_item_qty + sub_item_qty

        ##if total item quantity > 4, there will be a scroll bar on PWP section(for users to swipe)
        if total_item_qty > 4:
            xpath = Util.GetXpath({"locate": "sub_item_with_scrollviiw"})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click sub item section on PDP PWP section", "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": "sub_item_with_no_scrollview"})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click sub item section on PDP PWP section", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseSectionPage.ClickSubItemSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwipeAndCheckPurchaseWithPurchaseItem(arg):
        '''
        SwipeAndCheckPurchaseWithPurchaseItem : Swipe to the right to check if sub-item displayed correctly
                Input argu :
                    swipe_direction - direction of scrolling the category list
                    swipe_time - time scrolling the category list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        swipe_direction = arg["swipe_direction"]
        swipe_time = arg["swipe_time"]

        ##Swipe horizontally to check PWP sub-itm displayed
        xpath = Util.GetXpath({"locate":"pwp_horiz_scroll"})
        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":swipe_direction, "times":swipe_time, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseSectionPage.SwipeAndCheckPurchaseWithPurchaseItem')


class AndroidPurchaseWithPurchaseButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewMore(arg):
        '''
        ClickViewMore : Click PurchaseWithPurchase view more btn on PDP
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check view more btn
        xpath = Util.GetXpath({"locate": "pwp_viewmore"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click view more btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickViewMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOnSubItemSelection(arg):
        '''
        ClickCancelOnSubItemSelection : Click cancel(x) on sub item selection
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel(x) on sub item selection
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cancel on sub item selection", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickCancelOnSubItemSelection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeSubItemQuantity(arg):
        '''
        ClickChangeSubItemQuantity : Click sub item quantity
                Input argu :
                    action - add / minus
                    product_name - sub item name
                    quantity - click item add/minus quantity button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        product_name = arg["product_name"]
        quantity = int(arg["quantity"])

        ##Click sub item quantity on sub item selection drawer
        xpath = Util.GetXpath({"locate":action})
        xpath = xpath.replace('product_name_to_be_replaced', product_name)

        for count in range(quantity):
            ##Click to add/minus quantity
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click sub item quantity", "result": "1"})
            dumplogger.info("Click quantity %d times" % (count))
            time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickChangeSubItemQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubItemCheckbox(arg):
        '''
        ClickSubItemCheckbox : Click sub item checkbox
                Input argu :
                    product_name - sub item name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click sub item checkbox
        xpath = Util.GetXpath({"locate": "sub_item_checkbox"})
        xpath = xpath.replace('product_name_to_be_replaced', product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click sub item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickSubItemCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMiniCart(arg):
        '''
        ClickMiniCart : Click mini cart to add main item to cart on PWP landing page
                Input argu :
                    product_name - main item name
                    quantity - item quantity to put in cart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]
        quantity = int(arg["quantity"])

        xpath = Util.GetXpath({"locate": "mini_cart"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)

        for count in range(quantity):
            ##Click mini cart to put item in cart
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click mini cart to add main item to cart", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickMiniCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainItem(arg):
        '''
        ClickMainItem : Click main item on PWP landing page
                Input argu :
                    product_name - main item name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click main item on PWP landing page
        xpath = Util.GetXpath({"locate": "main_item"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click main item on PWP landing page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickMainItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnItemDrawer(arg):
        '''
        ClickAddOnItemDrawer : Click to open sub item selection drawer
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to open sub item selection drawer
        xpath = Util.GetXpath({"locate": "subitem_page_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to open sub item selection drawer", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPurchaseWithPurchaseButton.ClickAddOnItemDrawer')
