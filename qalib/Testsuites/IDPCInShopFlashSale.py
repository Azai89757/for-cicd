import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCInShopFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCInShopFlashSale Precondition =="
        dumplogger.info("== Setup IDPCInShopFlashSale Precondition ==")

    def test_IDPCInShopFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCInShopFlashSale-01.xml")

    def test_IDPCInShopFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCInShopFlashSale-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCInShopFlashSale Post condition =="
        dumplogger.info("== Setup IDPCInShopFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
