#!/usr/bin/python
from __future__ import print_function

'''
Welcome to Python 3.x world!
Please attention, in this module, some syntax usaage will be changed:

print


This module will be discarded if FrameWorkBase upgrade to 3.x

'''

'''
# It's sample code , just for reference now and replaced by ospreyController.OutputAllEvent
def outputAllEvent (filename=None):
    fp = None
    action = print  # It cannot used in Python 2.x
    token = ''
    if filename != None:
        fp = open(filename, 'w')
        action = fp.write
        token = '\n'
    if Osprey.HTTPLIST:
        notifyCount = 0
        contentScanCount = 0
        for i in xrange(len(Osprey.HTTPLIST)) :
            if 'HTTP' in Osprey.HTTPLIST[i].protocol:
                notifyCount += 1
                eval('action("===========  HTTP Event No.%d  =============%s" % (notifyCount, token))')
                eval('action("URL: %s%s" % (str(Osprey.HTTPLIST[i].wszUrl), token))')
                eval('action("Scan: %s%s" % (str(Osprey.HTTPLIST[i].scan), token))')
                eval('action("SubType: %s%s" % (str(Osprey.HTTPLIST[i].subType), token))')
                eval('action("Mode = %d%s" % (Osprey.HTTPLIST[i].mode, token))')
                eval('action("CategoryGroupCode = %s%s" % (str(Osprey.HTTPLIST[i].wszCategoryGroupCode), token))')
                eval('action("CategoryGroupName = %s%s" % (str(Osprey.HTTPLIST[i].wszCategoryGroupName), token))')
                eval('action("Level = %d%s" % (Osprey.HTTPLIST[i].credlevel, token))')
                eval('action("Score = %d%s" % (Osprey.HTTPLIST[i].credscore, token))')
                eval('action("Real IP = %s%s" % (Osprey.HTTPLIST[i].szRealIp, token))')
                eval('action("Result Type = %d%s" % (Osprey.HTTPLIST[i].restype, token))')
                eval('action("resAction = %d%s" % (Osprey.HTTPLIST[i].resaction, token))')
                eval('action("scanType = %d%s" % (Osprey.HTTPLIST[i].scanType, token))')
                eval('action("Time = %s%s\\n" % (Osprey.HTTPLIST[i].currentTime, token))')
            elif 'HTTP_CONTENT_SCAN_NOTIFY' in Osprey.HTTPLIST[i].id:
                contentScanCount += 1
                eval('action("===========  HTTP CONTENT SCAN Event No.%d  =============%s" % (contentScanCount, token))')
                eval('action("URL: %s%s" % (str(Osprey.HTTPLIST[i].wszUrl), token))')
                eval('action("Action Mode = %d%s" % (Osprey.HTTPLIST[i].actionMode, token))')
                eval('action("Scores = %d%s" % (Osprey.HTTPLIST[i].contentScores), token)')
                eval('action("Threat Type = %d%s" % (Osprey.HTTPLIST[i].threatType, token))')
                eval('action("Result Action = %d%s" % (Osprey.HTTPLIST[i].resAction, token))')
                eval('action("Time = %s%s\\n" % (Osprey.HTTPLIST[i].currentTime, token))')
        eval('action("\\n--------------------   HTTP notify end here  --------------------\\n")')
    else:
        eval('action("No HTTP event in HTTPLIST\\n")')
'''
