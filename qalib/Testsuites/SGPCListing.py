import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCListing Precondition =="
        dumplogger.info("== Setup SGPCListing Precondition ==")

    def test_SGPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCListing-01.xml")

    def test_SGPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCListing-02.xml")

    def test_SGPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCListing Post condition =="
        dumplogger.info("== Setup SGPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
