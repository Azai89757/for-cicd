﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminFraudMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util

##Inport Web library
from PageFactory.Web import BaseUICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminLogisticFraudPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetupShippingLimit(arg):
        ''' SetupShippingLimit : Setting shipping limit
                Input argu :
                        checkout_fs_limit_daily - Daily checkout free shipping limit you want to setting
                        checkout_fs_limit_weekly - Weekly checkout free shipping limit you want to setting
                        web_checkout_fs_limit_daily - Daily web checkout free shipping limit you want to setting
                        web_checkout_fs_limit_weekly - Weekly web checkout free shipping limit you want to setting
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        checkout_fs_limit_daily = arg["checkout_fs_limit_daily"]
        checkout_fs_limit_weekly = arg["checkout_fs_limit_weekly"]
        web_checkout_fs_limit_daily = arg["web_checkout_fs_limit_daily"]
        web_checkout_fs_limit_weekly = arg["web_checkout_fs_limit_weekly"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        ##Input daily checkout free shipping limit
        xpath = Util.GetXpath({"locate": "fs_limit_daily_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": checkout_fs_limit_daily, "result": "1"})

        ##Input weekly checkout free shipping limit
        xpath = Util.GetXpath({"locate": "fs_limit_weekly_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": checkout_fs_limit_weekly, "result": "1"})

        ##Input daily web checkout free shipping limit
        xpath = Util.GetXpath({"locate": "web_fs_limit_daily_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": web_checkout_fs_limit_daily, "result": "1"})

        ##Input weekly web checkout free shipping limit
        xpath = Util.GetXpath({"locate": "web_fs_limit_weekly_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": web_checkout_fs_limit_weekly, "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminLogisticFraudPage.SetupShippingLimit')

class AdminPaymentFraudPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCODLimit(arg):
        ''' SetCODLimit : Setting maximum number of COD orders limits
                Input argu :
                        orders - Maximum number of COD orders
                        time_frame - Maximum number of COD orders time frame (in days)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        orders = arg["orders"]
        time_frame = arg["time_frame"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        ##Input maximum number of COD orders
        xpath = Util.GetXpath({"locate": "orders"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": orders, "result": "1"})

        ##Input maximum number of COD orders time frame (in days)
        xpath = Util.GetXpath({"locate": "time_frame"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_frame, "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentFraudPage.SetCODLimit')
