#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCategoryMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import os
import re
import time
import datetime

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import GlobalAdapter

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminCategoryManagementPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCustomImage(arg):
        '''
        UploadCustomImage : Upload custom image
                Input argu :
                    category_name - category name
                    image_name - image name
                    image_type - image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]

        ##Upload custom image
        xpath = Util.GetXpath({"locate": "image_input"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": image_name, "file_type": image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCategoryManagementPage.UploadCustomImage ->' + category_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSelectionImage(arg):
        '''
        UploadSelectionImage : Upload selection image
                Input argu :
                    category_name - category name
                    selection_type - before / after
                    image_name - image name
                    image_type - image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]
        selection_type = arg["selection_type"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]

        ##Upload before / after selection image
        xpath = Util.GetXpath({"locate": selection_type})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": image_name, "file_type": image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCategoryManagementPage.UploadSelectionImage ->' + category_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckDisplayPage(arg):
        '''
        ClickCheckDisplayPage : Click check display page
                Input argu :
                    action - check / uncheck
                    category_name - category name
                    display_page - homepage_categories / all_categories
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        category_name = arg["category_name"]
        display_page = arg["display_page"]

        ##Click check display page
        xpath = Util.GetXpath({"locate": display_page + '_' + action})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + action + " -> " + display_page, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCategoryManagementPage.ClickCheckDisplayPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToSpecificCategory(arg):
        '''
        MoveToSpecificCategory : Move to specific category scope
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Move to specific category section
        xpath = Util.GetXpath({"locate": "category_list"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCategoryManagementPage.MoveToSpecificCategory')


class AdminCategoryManagementPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseCategoryAction(arg):
        '''
        ClickChooseCategoryAction : Click choose action for specific category
                Input argu :
                    action - enable / disable / edit / save / cancel / expand / collapse
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        category_name = arg["category_name"]

        ##Click enable category
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + action + " category -> " + category_name, "result":"1"})

        OK(ret, int(arg['result']), 'AdminCategoryManagementPageButton.ClickChooseCategoryAction')
