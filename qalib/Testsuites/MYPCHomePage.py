import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCHomePage Precondition =="
        dumplogger.info("== Setup MYPCHomePage Precondition ==")

    def test_MYPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCHomePage-01.xml")

    def test_MYPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCHomePage-02.xml")

    '''def test_MYPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCHomePage Post condition =="
        dumplogger.info("== Setup MYPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
