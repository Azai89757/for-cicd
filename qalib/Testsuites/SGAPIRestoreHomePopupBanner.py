import unittest
from Config import dumplogger
from FrameWorkBase import *


class SGAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup SGAPIRestoreHomePopupBanner Precondition ==")

    def test_SGAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/SGAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup SGAPIRestoreHomePopupBanner Post condition ==")
