import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCSignup Precondition =="
        dumplogger.info("== Setup PLPCSignup Precondition ==")

    def test_PLPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCSignup Post condition =="
        dumplogger.info("== Setup PLPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
