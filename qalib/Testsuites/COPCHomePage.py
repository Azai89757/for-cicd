import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCHomePage Precondition =="
        dumplogger.info("== Setup COPCHomePage Precondition ==")

    def test_COPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCHomePage-01.xml")

    def test_COPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCHomePage-02.xml")

    '''def test_COPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCHomePage Post condition =="
        dumplogger.info("== Setup COPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
