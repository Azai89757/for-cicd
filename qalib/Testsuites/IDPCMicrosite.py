import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMicrosite Precondition =="
        dumplogger.info("== Setup IDPCMicrosite Precondition ==")

    def test_IDPCMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-01.xml")

    def test_IDPCMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-02.xml")

    def test_IDPCMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-03.xml")

    def test_IDPCMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-04.xml")

    def test_IDPCMicrosite05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-05.xml")

    def test_IDPCMicrosite06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-06.xml")

    def test_IDPCMicrosite07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-07.xml")

    def test_IDPCMicrosite08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMicrosite-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMicrosite Post condition =="
        dumplogger.info("== Setup IDPCMicrosite Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
