import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCShopeeVoucher Precondition =="
        dumplogger.info("== Setup SGPCShopeeVoucher Precondition ==")

    def test_SGPCShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCShopeeVoucher-01.xml")

    def test_SGPCShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCShopeeVoucher-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCShopeeVoucher Post condition =="
        dumplogger.info("== Setup SGPCShopeeVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
