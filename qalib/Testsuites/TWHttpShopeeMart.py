import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpShopeeMart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpShopeeMart Precondition =="
        dumplogger.info("== Setup TWHttpShopeeMart Precondition ==")

    def test_TWHttpShopeeMart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeMart-01.xml")

    def test_TWHttpShopeeMart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeMart-02.xml")

    def test_TWHttpShopeeMart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeMart-03.xml")

    def test_TWHttpShopeeMart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeMart-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpShopeeMart Post condition =="
        dumplogger.info("== Setup TWHttpShopeeMart Post condition ==")
