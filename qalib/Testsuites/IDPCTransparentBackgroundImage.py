import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCTransparentBackgroundImage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCTransparentBackgroundImage Precondition =="
        dumplogger.info("== Setup IDPCTransparentBackgroundImage Precondition ==")

    def test_IDPCTransparentBackgroundImage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCTransparentBackgroundImage-01.xml")

    def test_IDPCTransparentBackgroundImage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCTransparentBackgroundImage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCTransparentBackgroundImage Post condition =="
        dumplogger.info("== Setup IDPCTransparentBackgroundImage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
