import unittest
from Config import dumplogger
from FrameWorkBase import *


class VNAPIRestoreFraud(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIRestoreFraud Precondition =="
        dumplogger.info("== Setup VNAPIRestoreFraud Post condition")

    def test_VNAPIRestoreFraud001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIRestoreFraud-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIRestoreFraud Post condition =="
        dumplogger.info("== Setup VNAPIRestoreFraud Post condition")
