import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSellerDiscount Precondition =="
        dumplogger.info("== Setup VNPCSellerDiscount Precondition ==")

    def test_VNPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-01.xml")

    def test_VNPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-02.xml")

    def test_VNPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-03.xml")

    def test_VNPCSellerDiscount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-04.xml")

    def test_VNPCSellerDiscount05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-05.xml")

    def test_VNPCSellerDiscount06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-06.xml")

    def test_VNPCSellerDiscount07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-07.xml")

    def test_VNPCSellerDiscount08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-08.xml")

    def test_VNPCSellerDiscount09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-09.xml")

    def test_VNPCSellerDiscount10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerDiscount-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSellerDiscount Post condition =="
        dumplogger.info("== Setup VNPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
