import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCShopPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCShopPage Precondition =="
        dumplogger.info("== Setup THPCShopPage Precondition ==")

    def test_THPCShopPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopPage-01.xml")

    def test_THPCShopPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopPage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCShopPage Post condition =="
        dumplogger.info("== Setup THPCShopPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
