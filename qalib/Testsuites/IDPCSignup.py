import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSignup Precondition =="
        dumplogger.info("== Setup IDPCSignup Precondition ==")

    def test_IDPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSignup Post condition =="
        dumplogger.info("== Setup IDSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
