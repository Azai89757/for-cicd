import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *

class VNiOSLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSLogin Precondition =="
        dumplogger.info("== Setup VNiOSLogin Precondition ==")

        ##Get device id and device t type
        Config._DeviceID_, device_type = GetiOSDeviceId()
        print "Device id : %s" % (Config._DeviceID_)
        print "Device type :", device_type

        ##Get device info by device id
        if Config._DeviceID_:
            #Config._MobileDeviceInfo_[Config._DeviceID_] = {}
            #Config._MobileDeviceInfo_[Config._DeviceID_]["device_name"] = device_type
            ##Build WDA to iOS device
            iOSBuildWDAToDevice({"device_type":device_type, "uuid":Config._DeviceID_})

            ##Call function from iOSBaseUICore
            iOSInitialDriver()
        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"
            ##Directly Leave Setup
            self.skipTest("")

    def test_VNiOSLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSLogin-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSLogin Post condition =="
        dumplogger.info("== Setup VNiOSLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
