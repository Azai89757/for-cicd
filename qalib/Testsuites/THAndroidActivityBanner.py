import sys
import os
import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class THAndroidActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidActivityBanner Precondition =="

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Disable wifi module by command only if proxy server is not used
            if not Config._EnableProxy_:
                ##Disable wifi module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc wifi disable"
                dumplogger.info("Disable emulator wifi module: %s" % (cmd))
                os.system(cmd)

                ##Enable mobile data module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc data enable"
                dumplogger.info("Enable emulator mobile data module: %s" % (cmd))
                os.system(cmd)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"th", "result": "1"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_THAndroidActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-01.xml")

    def test_THAndroidActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-02.xml")

    def test_THAndroidActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-03.xml")

    ##Catgory not at the bottom of management page
    def test_THAndroidActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-04.xml")

    def test_THAndroidActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-05.xml")

    def test_THAndroidActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-06.xml")

    def test_THAndroidActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-07.xml")

    def test_THAndroidActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-08.xml")

    def test_THAndroidActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-09.xml")

    ##Homepage campaign spec change
    def test_THAndroidActivityBanner10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-10.xml")

    def test_THAndroidActivityBanner11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidActivityBanner-11.xml")

    # def test_THAndroidActivityBanner12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/TH/THAndroidActivityBanner-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidActivityBanner Post condition =="
        dumplogger.info("== Setup THAndroidActivityBanner Post condition ==")
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
