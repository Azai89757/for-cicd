import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCCart Precondition =="
        dumplogger.info("== Setup SGPCCart Precondition ==")

    def test_SGPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCCart Post condition =="
        dumplogger.info("== Setup SGPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
