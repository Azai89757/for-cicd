﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSNotificationMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import iOSBaseUICore
import iOSBaseUILogic
import DecoratorHelper

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class iOSNotificationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRedirect(arg):
        ''' CheckRedirect : Check Redirect
                Input argu :
                    redirect - Action Redirect Type
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        redirect = arg["redirect"]
        result = arg["result"]

        ##Redirect to page type
        if redirect == 'none':
            dumplogger.info("Skip Check, do not setting redirect.")
        elif redirect == 'myProducts':
            iOSBaseUICore.iOSCheckElementExist({"method": "xpath", "locate": Util.GetXpath({"locate":"myProducts"}), "result": result})
        elif redirect == 'shoppingCart':
            iOSBaseUICore.iOSCheckElementExist({"method": "xpath", "locate": Util.GetXpath({"locate":"shoppingCart"}), "result": result})
        elif redirect == 'editShopProfile':
            iOSBaseUICore.iOSCheckElementExist({"method": "xpath", "locate": Util.GetXpath({"locate":"editShopProfile"}), "result": result})
        elif redirect == 'appPath':
            dumplogger.info("Skip Check, setting app path redirect.")
        elif redirect == 'myAccount':
            iOSBaseUICore.iOSCheckElementExist({"method": "xpath", "locate": Util.GetXpath({"locate":"myAccount"}), "result": result})
        elif redirect == 'reactNative':
            dumplogger.info("Skip Check, setting react native redirect.")

        OK(ret, int(arg['result']), 'iOSNotificationPage.CheckRedirect')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPromotionsFolder(arg):
        ''' GoToPromotionsFolder : Goto Promotions Folder
                Input argu :
                    folder - notification folder type
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        folder = arg["folder"]
        message = arg["message"]
        result = arg["result"]

        ##Folder type
        iOSBaseUICore.iOSClick({"method": "id", "locate": Util.GetXpath({"locate":folder}), "message": message, "result": result})

        OK(ret, int(arg['result']), 'iOSNotificationPage.GoToPromotionsFolder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoShoppingNow(arg):
        ''' GotoShoppingNow : Goto shopping now
                Input argu :N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":"shopping_now"}), "message": "click goto shopping now", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.GotoShoppingNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToSellerFolderSubTab(arg):
        ''' GoToSellerFolderSubTab : Goto seller folder sub tab
                Input argu :
                        type - activity_notif/sellerupdate
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        type = arg["type"]

        ##In seller folder will have 2 sub tabs can chose
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":type}), "message": "Click" + type + "tab", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.GoToSellerFolderSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstOrderUpdate(arg):
        ''' ClickFirstOrderUpdate : Click first order update
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click first order update
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":"first_order_update_content"}), "message": "Click first order updates", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.ClickFirstOrderUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstPromotions(arg):
        ''' ClickFirstPromotions : Click first promotions
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click first promotion
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":"first_promotion"}), "message": "Click first order updates", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.ClickFirstPromotions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoNotificationSettingSubTab(arg):
        ''' GotoNotificationSettingSubTab : Goto notification setting sub tab
                Input argu :
                    type - push_notif/email_notif
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click notification setting sub tab
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":"push_notification"}), "message": "Click push notification tab", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.GotoNotificationSettingSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNotificationToggle(arg):
        ''' ClickNotificationToggle : Click notification toggle
                Input argu :
                        type - push_notif/phase_notif/order_notif/chat_notif/activity_notif/follow_notif/soldout_notif/rating_notif/wallet_notif
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click to on/off toggle
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":"push_notif"}), "message": "Click "+type+" toggle", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.ClickNotificationToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelNotificationSetting(arg):
        ''' CancelNotificationSetting : Cancel notification setting
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click cancel btn
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": Util.GetXpath({"locate":"cancel_btn"}), "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSNotificationPage.CancelNotificationSetting')
