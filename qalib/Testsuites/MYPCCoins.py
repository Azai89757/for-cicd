import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class MYPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCCoins Precondition =="
        dumplogger.info("== Setup MYPCCoins Precondition ==")

    def test_MYPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-01.xml")

    def test_MYPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-02.xml")

    def test_MYPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-03.xml")

    def test_MYPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-04.xml")

    def test_MYPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-05.xml")

    def test_MYPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-06.xml")

    def test_MYPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCoins-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCCoins Post condition =="
        dumplogger.info("== Setup MYPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
