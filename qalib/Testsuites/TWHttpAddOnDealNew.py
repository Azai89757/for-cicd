import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpAddOnDealNew(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpAddOnDealNew Precondition =="
        dumplogger.info("== Setup TWHttpAddOnDealNew Precondition ==")

    def test_TWHttpAddOnDealNew01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-01.xml")

    def test_TWHttpAddOnDealNew02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-02.xml")

    def test_TWHttpAddOnDealNew03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-03.xml")

    def test_TWHttpAddOnDealNew04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-04.xml")

    def test_TWHttpAddOnDealNew05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-05.xml")

    def test_TWHttpAddOnDealNew06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-06.xml")

    def test_TWHttpAddOnDealNew07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-07.xml")

    def test_TWHttpAddOnDealNew08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-08.xml")

    def test_TWHttpAddOnDealNew09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-09.xml")

    def test_TWHttpAddOnDealNew10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-10.xml")

    def test_TWHttpAddOnDealNew11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-11.xml")

    def test_TWHttpAddOnDealNew12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-12.xml")

    def test_TWHttpAddOnDealNew13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-13.xml")

    def test_TWHttpAddOnDealNew14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAddOnDealNew-14.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpAddOnDealNew Post condition =="
        dumplogger.info("== Setup TWHttpAddOnDealNew Post condition ==")
