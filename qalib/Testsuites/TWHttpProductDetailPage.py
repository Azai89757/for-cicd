import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductDetailPage Precondition =="
        dumplogger.info("== Setup TWHttpProductDetailPage Precondition ==")

    def test_TWHttpProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductDetailPage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductDetailPage Post condition =="
        dumplogger.info("== Setup TWHttpProductDetailPage Post condition ==")
