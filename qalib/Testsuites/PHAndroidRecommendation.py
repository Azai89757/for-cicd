#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Config
import unittest

from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class PHAndroidRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAndroidRecommendation Precondition =="
        dumplogger.info("== Setup PHAndroidRecommendation Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Disable wifi module by command only if proxy server is not used
            """
            if not Config._EnableProxy_:
                ##Disable wifi module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc wifi disable"
                dumplogger.info("Disable emulator wifi module: %s" % (cmd))
                os.system(cmd)
                ##Enable mobile data module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc data enable"
                dumplogger.info("Enable emulator mobile data module: %s" % (cmd))
                os.system(cmd)
            """

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"ph"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_PHAndroidRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-01.xml")

    def test_PHAndroidRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-02.xml")

    def test_PHAndroidRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-03.xml")

    def test_PHAndroidRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-04.xml")

    def test_PHAndroidRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-05.xml")

    def test_PHAndroidRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-06.xml")

    def test_PHAndroidRecommendation07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-07.xml")

    def test_PHAndroidRecommendation08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-08.xml")

    def test_PHAndroidRecommendation10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-10.xml")

    def test_PHAndroidRecommendation11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-11.xml")

    def test_PHAndroidRecommendation12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-12.xml")

    def test_PHAndroidRecommendation13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-13.xml")

    def test_PHAndroidRecommendation15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidRecommendation-14.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAndroidRecommendation Post condition =="
        dumplogger.info("== Setup PHAndroidRecommendation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver({})

        ##Kill Android driver
        AndroidDeInitialDriver({})

        ##Kill Appium
        KillAppium({"isFail":"0"})
