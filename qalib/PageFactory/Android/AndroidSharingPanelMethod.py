﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidSharingPanelMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidNotificationMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidSharingPanelPage:

    def __IsForTest__(self):
        pass

    @DecoratorHelper.FuncRecorder
    def AddContactToDevice(arg):
        '''
        AddContactToDevice : Add Contact To Device
                Input argu :
                    name - contct name
                    number - contct phone number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        number = arg["number"]

        ##Add contact by adb
        AndroidBaseUICore.SendADBCommand({"command_type":"AddContact", "adb_arg":"name '" + name + "' -e phone " + number, "is_return": "0", "result": "1"})
        time.sleep(2)

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click referral button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidSharingPanelPage.AddContactToDevice')

    @DecoratorHelper.FuncRecorder
    def ClearContactList(arg):
        '''
        ClearContactList : Delete all Contact in Device
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Add contact by adb
        AndroidBaseUICore.SendADBCommand({"command_type":"DeleteContact", "adb_arg":"", "is_return": "0", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidSharingPanelPage.ClearContactList')

    @DecoratorHelper.FuncRecorder
    def GetIntoSharingPageByScreenShot(arg):
        '''
        GetIntoSharingPageByScreenShot : Get into sharing page by clicking screen shot sharing button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Expand the device notification page
        AndroidNotificationMethod.AndroidDeviceNotificationPage.ExpandNotification({"result": "1"})

        ##Click screen shot button
        screen_shot_button = Util.GetXpath({"locate": "screen_shot_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": screen_shot_button, "message": "Click screen hot button", "result": "1"})

        ##Click other place to collapse the image on upper right corner
        other_place = Util.GetXpath({"locate":"other_place"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":other_place, "message":"Click other place", "result": "1"})

        ##Click screen shot sharing button
        screen_shot_share_button = Util.GetXpath({"locate":"screen_shot_share_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":screen_shot_share_button, "message":"Click sharing button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSharingPanelPage.GetIntoSharingPageByScreenShot')

class AndroidSharingPanelButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMore(arg):
        '''
        ClickMore : Click more button
                Input argu :
                    page_type - microsite / shop
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click more button
        xpath = Util.GetXpath({"locate": page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click more button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShare(arg):
        '''
        ClickShare : Click share button
                Input argu :
                    page_type - shop / product / product_other / screen_shot
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click share button
        xpath = Util.GetXpath({"locate":page_type})
        if page_type == "screen_shot":
            AndroidBaseUICore.AndroidClickCoordinates({"x_cor":"1000", "y_cor":"2020"})
        else:
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click share button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickShare')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSocialMedia(arg):
        '''
        ClickSocialMedia : Click social media button
                Input argu :
                    social_media_type - facebook / messenger / twitter / email / copy_info / copy_link / whatsapp / telegram
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        social_media_type = arg["social_media_type"]

        ##Click social media button
        xpath = Util.GetXpath({"locate": social_media_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click social media button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickSocialMedia->' + social_media_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllowPermission(arg):
        '''
        ClickAllowPermission : Click allow permission button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click allow permission button
        xpath = Util.GetXpath({"locate": "allow"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click allow button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickAllowPermission')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel permission button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel permission button
        xpath = Util.GetXpath({"locate": "cancel"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDoNotAskAgain(arg):
        '''
        ClickDoNotAskAgain : Click do_not_ask_again button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click do_not_ask_again button
        xpath = Util.GetXpath({"locate": "do_not_ask_again"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click do_not_ask_again button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickDoNotAskAgain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetting(arg):
        '''
        ClickSetting : Click setting button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click setting button
        xpath = Util.GetXpath({"locate": "setting"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click setting button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickScreenShot(arg):
        '''
        ClickScreenShot : Click screenhot button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click screen hot button
        xpath = Util.GetXpath({"locate": "screen_shot_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click screen hot button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSharingPanelButton.ClickScreenShot')


class AndroidSendMailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReceiverEmail(arg):
        '''
        InputReceiverEmail : Input receiver email
                Input argu :
                    receiver_email - email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        receiver_email = arg["receiver_email"]

        ##Input receiver email
        xpath = Util.GetXpath({"locate":"receiver_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":receiver_email, "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSendMailPage.InputReceiverEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithEmail(arg):
        '''
        ShareWithEmail : Share with email
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send button to share
        xpath = Util.GetXpath({"locate": "send_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSendMailPage.ShareWithEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithCopy(arg):
        '''
        ShareWithCopy : Share with copy link or copy info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Focus the content filed
        xpath = Util.GetXpath({"locate": "content_field"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click content field", "result": "1"})

        ##Select all and clean field
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"select_all", "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"delete", "result": "1"})

        ##Paste the copy info
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"paste", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSendMailPage.ShareWithCopy')


class AndroidInstagramSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputContentField(arg):
        '''
        InputContentField : Input content field
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next button
        xpath = Util.GetXpath({"locate": "next_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click next button", "result": "1"})

        ##Click share button in modify image page
        xpath = Util.GetXpath({"locate": "share_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click share button in modify image page", "result": "1"})

        ##Focus content field
        xpath = Util.GetXpath({"locate":"content_field"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to focus content field", "result": "1"})

        ##Paste the information
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"paste", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidInstagramSharingPage.InputContentField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditInstagramStory(arg):
        '''
        EditInstagramStory : Edit with instagram story
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Focus content field
        xpath = Util.GetXpath({"locate":"content_field"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click to focus content field", "result": "1"})

        ##Paste the information
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"paste", "result": "1"})

        ##Click finish button
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"finish_button", "threshold":"0.8", "is_click":"yes", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidInstagramSharingPage.EditInstagramStory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithInstagramStory(arg):
        '''
        ShareWithInstagramStory : Share with instagram story
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button
        xpath = Util.GetXpath({"locate": "share_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click share button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidInstagramSharingPage.ShareWithInstagramStory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithInstagramFeed(arg):
        '''
        ShareWithInstagramFeed : Share with instagram feed
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button in modify content page
        xpath = Util.GetXpath({"locate": "share_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click share button in modify content page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidInstagramSharingPage.ShareWithInstagramFeed')


class AndroidFacebookSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FacebookLogin(arg):
        '''
        FacebookLogin : Login with facebook
                Input argu :
                    account - facebook account
                    password - facebook password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg["account"]
        password = arg["password"]

        ##Check if need login facebook account
        xpath = Util.GetXpath({"locate":"facebook_account"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

            ##Input account
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})
            xpath = Util.GetXpath({"locate":"facebook_password"})

            ##Input password
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})
            xpath = Util.GetXpath({"locate": "login_button"})

            ##Click login button
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click login button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFacebookSharingPage.FacebookLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithMessenger(arg):
        '''
        ShareWithMessenger : Share with messenger
                Input argu :
                    page_type - shop / product/ microsite/ referral/ screenshot_product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click send button to share
        xpath = Util.GetXpath({"locate": page_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFacebookSharingPage.ShareWithMessenger')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithFacebook(arg):
        '''
        ShareWithFacebook : Share with facebook
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button
        xpath = Util.GetXpath({"locate": "share_button"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFacebookSharingPage.ShareWithFacebook')


class AndroidTelegramSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithTelegram(arg):
        '''
        ShareWithTelegram : Share with telegram
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Choose a friend to share message
        xpath = Util.GetXpath({"locate": "friend"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click friend to send msg", "result": "1"})
            time.sleep(5)

        ##Click send button to share
        xpath = Util.GetXpath({"locate": "send_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidTelegramSharingPage.ShareWithTelegram')

class AndroidTwitterSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareWithTwitter(arg):
        '''
        ShareWithTwitter : Share with twitter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send button to share
        xpath = Util.GetXpath({"locate": "send_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTwitterSharingPage.ShareWithTwitter')


class AndroidReferralPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShare(arg):
        '''
        ClickShare : Click share button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button
        xpath = Util.GetXpath({"locate": "share_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click share button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReferralPageButton.ClickShare')

class AndroidWhatsAppSharingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseFriendToShare(arg):
        '''
        ChooseFriendToShare : Choose a friend to share message
                Input argu :
                    friend_name - friend name who want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        friend_name = arg["friend_name"]
        ret = 1

        ##Choose a friend to share message
        xpath = Util.GetXpath({"locate": "friend"})
        xpath = xpath.replace("string_to_be_replaced", friend_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})
        time.sleep(2)

        ##Click send button to share
        xpath = Util.GetXpath({"locate": "send_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidWhatsAppSharingPage.ChooseFriendToShare')

class AndroidWhatsAppSharingPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSend(arg):
        '''
        ClickSend : Click send button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send button
        xpath = Util.GetXpath({"locate":"send_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidWhatsAppSharingPageButton.ClickSend')
