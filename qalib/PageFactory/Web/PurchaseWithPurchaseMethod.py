#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 PurchaseWithPurchaseMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class PurchaseWithPurchaseButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductNameOnPWPSection(arg):
        '''
        ClickProductNameOnPWPSection : Click product name on PDP's PWP section
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product name
        xpath = Util.GetXpath({"locate": "product_name"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product name", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickProductNameOnPWPSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewMore(arg):
        '''
        ClickViewMore : Click view more button on PDP's PWP section
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view more button
        xpath = Util.GetXpath({"locate": "view_more_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view more button", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickViewMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainItem(arg):
        '''
        ClickMainItem : Click main item on PWP landing page
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click main item name
        xpath = Util.GetXpath({"locate": "main_item"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main item", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickMainItem')

    @DecoratorHelper.FuncRecorder
    def ClickMiniCart(arg):
        '''
        ClickMiniCart : Click mini cart to add main item to cart on PWP landing page
                Input argu :
                    product_name - main item name
                    quantity - item quantity to put in cart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]
        quantity = int(arg["quantity"])

        xpath = Util.GetXpath({"locate": "mini_cart"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)

        for count in range(quantity):
            ##Click mini cart to put item in cart
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mini cart to add main item to cart", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickMiniCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnItemDrawer(arg):
        '''
        ClickAddOnItemDrawer : Click to popup sub item selection window
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to popup sub item selection window
        xpath = Util.GetXpath({"locate": "addon_drawer"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to popup sub item selection window", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickAddOnItemDrawer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOnSubItemSelection(arg):
        '''
        ClickCancelOnSubItemSelection : Click cancel on sub item selection
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel on sub item selection
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel on sub item selection", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickCancelOnSubItemSelection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDoneOnSubItemSelection(arg):
        '''
        ClickDoneOnSubItemSelection : Click done on sub item selection
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done on sub item selection
        xpath = Util.GetXpath({"locate": "done_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click done on sub item selection", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickDoneOnSubItemSelection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubItemCheckbox(arg):
        '''
        ClickSubItemCheckbox : Click sub item checkbox
                Input argu :
                    product_name - sub item name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click sub item checkbox
        xpath = Util.GetXpath({"locate": "sub_item_checkbox"})
        xpath = xpath.replace('product_name_to_be_replaced', product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickSubItemCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeSubItemQuantity(arg):
        '''
        ClickChangeSubItemQuantity : Click to change sub item quantity
                Input argu :
                    action - add / minus
                    product_name - sub item name
                    quantity - click item add/minus quantity button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        product_name = arg["product_name"]
        quantity = int(arg["quantity"])

        ##Click sub item quantity on sub item selection drawer
        xpath = Util.GetXpath({"locate":action})
        xpath = xpath.replace('product_name_to_be_replaced', product_name)

        for count in range(quantity):
            ##Click to add/minus quantity
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub item quantity", "result": "1"})
            dumplogger.info("Click quantity %d times" % (count))
            time.sleep(3)

        OK(ret, int(arg['result']), 'PurchaseWithPurchaseButton.ClickChangeSubItemQuantity')
