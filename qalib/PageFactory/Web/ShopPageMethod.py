#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ShopPageMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import FrameWorkBase
import Util
import BaseUICore
import BaseUILogic
import DecoratorHelper


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class ShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToShopeeHomePage(arg):
        ''' GoToShopeeHomePage : go to shopee home page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Go to shopee home page
        xpath = Util.GetXpath({"locate":"shopee_homepage_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Go to shopee home page", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPage.GoToShopeeHomePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDropDownList(arg):
        ''' SelectDropDownList : Select "In this shop" / "In shopee" / "In mall" from the drop-down list
                Input argu :
                        search_scope - in_this_shop / in_shopee / in_mall
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        search_scope = arg["search_scope"]

        xpath_drop_down = Util.GetXpath({"locate":"shop_drop_down_list"})

        if search_scope == "in_this_shop":
            xpath_search_scope = Util.GetXpath({"locate":"in_this_shop"})

        elif search_scope == "in_shopee":
            xpath_search_scope = Util.GetXpath({"locate":"in_shopee"})

        elif search_scope == "in_mall":
            xpath_search_scope = Util.GetXpath({"locate":"in_mall"})

        ##Select "In this shop" or "In shopee" or "In Mall"
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath_drop_down, "locatehidden":xpath_search_scope, "message":"Select " + search_scope + " from the drop down list", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPage.SelectDropDownList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickQuestionMark(arg):
        ''' ClickQuestionMark : Click question mark
                Input argu :
                        click_button - ok / read_more
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        click_button = arg["click_button"]

        ##Click question mark
        xpath = Util.GetXpath({"locate":"question_mark"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click question mark", "result": "1"})

        if click_button == "ok":
            ##Click question mark and click ok
            xpath = Util.GetXpath({"locate":"question_mark_ok"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        elif click_button == "read_more":
            ##Click question mark and click read more
            xpath = Util.GetXpath({"locate":"question_mark_read_more"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click read more", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPage.ClickQuestionMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductSortTab(arg):
        ''' SelectProductSortTab : Click product sort tab
                Input argu :
                        sort_tab - popular / latest / top_sales / price_from_high_to_low / price_from_low_to_high
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        sort_tab = arg["sort_tab"]

        if sort_tab == "popular":
            ##Click popular tab
            xpath = Util.GetXpath({"locate":"popular_tab"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click popular tab", "result": "1"})

        elif sort_tab == "latest":
            ##Click latest tab
            xpath = Util.GetXpath({"locate":"latest_tab"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click latest tab", "result": "1"})

        elif sort_tab == "top_sales":
            ##Click top sales tab
            xpath = Util.GetXpath({"locate":"top_sales_tab"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click top sales tab", "result": "1"})

        elif sort_tab == "price_from_high_to_low":
            xpath_drop_down = Util.GetXpath({"locate":"price_drop_down_list"})
            xpath_high_to_low = Util.GetXpath({"locate":"high_to_low"})

            ##Select "High To Low"
            BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath_drop_down, "locatehidden":xpath_high_to_low, "message":"Choose price from high to low", "result": "1"})

        elif sort_tab == "price_from_low_to_high":
            xpath_drop_down = Util.GetXpath({"locate":"price_drop_down_list"})
            xpath_low_to_high = Util.GetXpath({"locate":"low_to_high"})

            ##Select "Low To High"
            BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath_drop_down, "locatehidden":xpath_low_to_high, "message":"Choose price from low to high", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPage.SelectProductSortTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherSectionArrow(arg):
        ''' ClickVoucherSectionArrow : Click voucher section arrow
                Input argu :
                        voucher_arrow - next / previous
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        voucher_arrow = arg["voucher_arrow"]

        if voucher_arrow == "next":
            ##Click next voucher button
            xpath = Util.GetXpath({"locate":"next_voucher_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click next voucher button", "result": "1"})

        elif voucher_arrow == "previous":
            ##Click previous voucher button
            xpath = Util.GetXpath({"locate":"previous_voucher_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click previous voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPage.ClickVoucherSectionArrow')

class AllProductsTabPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShopCollection(arg):
        ''' SelectShopCollection : select shop collection on left side category
                Input argu :
                        shop_collection - select shop collection
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        shop_collection = arg["shop_collection"]

        ##Select shop collection on left side category
        xpath = Util.GetXpath({"locate":"shop_category"})
        xpath_shop_collection = xpath.replace("shop_collection_to_be_replace", shop_collection)
        BaseUICore.Click({"method":"xpath", "locate":xpath_shop_collection, "message":"Select shop collection", "result": "1"})

        OK(ret, int(arg['result']), 'AllProductsTabPage.SelectShopCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCategoryOptionCheckbox(arg):
        ''' SelectCategoryOptionCheckbox : select shop category option checkbox on left side
                Input argu :
                        shop_category - select shop category option
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        shop_category_option = arg["shop_category_option"]

        ##Select shop category option checkbox on left side
        xpath = Util.GetXpath({"locate":"shop_category_option"})
        xpath_shop_category_option = xpath.replace("shop_category_option_to_be_replace", shop_category_option)
        BaseUICore.Click({"method":"xpath", "locate":xpath_shop_category_option, "message":"Select shop category option", "result": "1"})

        OK(ret, int(arg['result']), 'AllProductsTabPage.SelectCategoryOptionCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceRangeFilter(arg):
        ''' InputPriceRangeFilter : input the lowest price and the highest price on the filter panel
                Input argu :
                        lowest_price - the lowest price
                        highest_price - the highest price
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        lowest_price = arg["lowest_price"]
        highest_price = arg["highest_price"]

        lowest_price_box = Util.GetXpath({"locate":"lowest_price_box"})
        highst_price_box = Util.GetXpath({"locate":"highest_price_box"})
        apply_button = Util.GetXpath({"locate":"apply_button"})

        ##Enter the price range on the filter panel and click apply button
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":lowest_price_box, "result": "1"})
        BaseUICore.Input({"locate":lowest_price_box, "string":lowest_price, "method":"xpath", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":highst_price_box, "result": "1"})
        BaseUICore.Input({"locate":highst_price_box, "string":highest_price, "method":"xpath", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":apply_button, "message":"click apply button", "result": "1"})

        OK(ret, int(arg['result']), 'AllProductsTabPage.InputPriceRangeFilter => Between ' + lowest_price + ' and ' + highest_price)

class ShopPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollow(arg):
        ''' ClickFollow : Click follow button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click follow button
        xpath = Util.GetXpath({"locate":"label_follow"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click follow button", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickFollow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChat(arg):
        ''' ClickChat : Click chat button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click chat button
        xpath = Util.GetXpath({"locate":"label_chat"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click chat button", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickChat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeAll(arg):
        ''' ClickSeeAll : Click see all button
                Input argu :
                        section - category / top_product / hot_deals
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Click see all button
        xpath = Util.GetXpath({"locate":section})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see all button", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickSeeAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllProductsTab(arg):
        ''' ClickAllProductsTab : Click all products tab
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click all products tab
        xpath = Util.GetXpath({"locate":"all_products_tab"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click all products tab", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickAllProductsTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBannerPagination(arg):
        ''' ClickBannerPagination : Click banner pagination
                Input argu :
                            pagination_numbering - the numbering of pagination
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        pagination_numbering = arg["pagination_numbering"]

        ##Click banner pagination
        xpath = Util.GetXpath({"locate":"pagination_dot"})
        xpath = xpath.replace('replaced_text', pagination_numbering)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click banner pagination dot", "result": "1"})

        ##Click banner
        xpath = Util.GetXpath({"locate":"banner"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click banner", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickBannerPagination')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        ''' ClickCategory : Choose category
                Input argu :
                        category - select category
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        category = arg["category"]

        ##Select category
        xpath = Util.GetXpath({"locate":"category"})
        xpath = xpath.replace("category_to_be_replace", category)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select category", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerVoucherClaim(arg):
        ''' ClickSellerVoucherClaim : Click seller voucher claim button
                Input argu :N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click seller voucher claim button
        xpath = Util.GetXpath({"locate":"claim_voucher"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click seller voucher claim button", "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickSellerVoucherClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCard(arg):
        ''' ClickProductCard : Click product card
                Input argu :
                        product_name - product name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click product card
        xpath = Util.GetXpath({"locate":"product_card"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product card: "+product_name, "result": "1"})

        OK(ret, int(arg['result']), 'ShopPageButton.ClickProductCard')

class AllProductsTabPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickResetFilter(arg):
        ''' ClickResetFilter : Click reset filter button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click reset filter button
        xpath = Util.GetXpath({"locate":"reset_filter_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click reset filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AllProductsTabPageButton.ClickResetFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToShopHome(arg):
        ''' ClickBackToShopHome : Click back to shop home button
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click back to shop home button
        xpath = Util.GetXpath({"locate":"back_to_shop_home_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back to shop home button", "result": "1"})

        OK(ret, int(arg['result']), 'AllProductsTabPageButton.ClickBackToShopHome')
