#!/usr/bin/env python
# -*- coding: utf-8 -*-

##import python library
from __future__ import unicode_literals
import re
import sys
import time
import json
import inspect
from operator import itemgetter

##import framework library
import Config
import GlobalAdapter
import Parser
import XtFunc
from Config import dumplogger

##Import Web
from PageFactory.Web import *

##Import Android
from PageFactory.Android import *

##Import iOS
from PageFactory.iOS import *

##Import Admin
from PageFactory.Admin import *
from PageFactory.Admin.Payment import *
from PageFactory.Admin.Order import *
from PageFactory.Admin.Discoverlanding import *
from PageFactory.Admin.ProductListing import *
from PageFactory.Admin.Growth import *

##Import API Related Scripts
from api import *
from api.http import *
from api.socket import *
from api.spex import *

##Import db Related Scripts
from db import DBCommonMethod

def LoadAction(case_platform):
    ''' LoadAction : Load failed action json file
            Input argu : N/A
            Return code :
                output[case_platform] - retry case define by platform
    '''
    slash = Config.dict_systemslash[Config._platform_]
    json_path = Config._DataFilePath_ + slash + "common" + slash + "RetryAction.json"

    with open(json_path, 'rb') as f:
        output = json.load(f)

    ##Sorted scenario from general to specific by key "retry_begin_position"
    sorted_output = sorted(output[case_platform], key=itemgetter('retry_begin_position'), reverse=False)

    return sorted_output

def RetryAction(frame_cur):
    ''' RetryAction : For retry case define in RetryAction.json
            Input argu : frame_cur - current failed frame
            Return code : 1 - Retry current step pass
                          0 - Retry XML function pass
                          raise - raise exception when execute fail
    '''
    ##Get current steps in fail function
    retry_func_inline_list = GlobalAdapter.CommonVar._FuncStack_[:]

    ##Define platform by current failed step
    ##frame_cur[1] == file path of failed step
    if 'Web' in frame_cur[1] or 'Admin' in frame_cur[1]:
        platform = 'pc'
    elif 'Android' in frame_cur[1]:
        platform = 'android'
    else:
        platform = Config._TestCasePlatform_.lower()

    ##Load retry function list
    retry_action_list = LoadAction(platform)
    for func_dict in retry_action_list:
        if func_dict["main_func_position"] >= -1*len(GlobalAdapter.CommonVar._XMLStack_) and func_dict["retry_begin_position"] >= -1*len(GlobalAdapter.CommonVar._XMLStack_):

            ##If retry function match the failed scenario
            if func_dict["main_func"] in GlobalAdapter.CommonVar._XMLStack_[func_dict["main_func_position"]] and func_dict["begin_func"] in GlobalAdapter.CommonVar._XMLStack_[func_dict["retry_begin_position"]]:
                if platform == 'pc':
                    dumplogger.info("Begin to retry for %s scenario %s..." % (platform, func_dict["main_func"]))
                    try:
                        ##If Platform == PC, refresh brwoser
                        BaseUILogic.BrowserRefresh({"message":"Refresh browser to retry", "result": "1"})
                    except:
                        dumplogger.exception("Probably an API case...")

                    for retry_steps in range(func_dict["retry_begin_position"], 0):
                        ##Handle no redirection scenario
                        ##If no redireciton, can exectute the actions before the last action
                        try:
                            dumplogger.info("Start retry action: %s" % (GlobalAdapter.CommonVar._XMLStack_[retry_steps]))
                            eval(GlobalAdapter.CommonVar._XMLStack_[retry_steps])
                            time.sleep(1)
                        except:
                            dumplogger.exception("Retry scenario failed!!!")
                            if retry_steps == -1:
                                raise
                elif platform == 'android':
                    dumplogger.info("Begin to retry for %s scenario %s..." % (platform, func_dict["main_func"]))
                    try:
                        ##If can exceute main function, it means that the page did not redirect
                        dumplogger.info("Start retry action: %s" % (GlobalAdapter.CommonVar._XMLStack_[func_dict["main_func_position"]]))
                        ##Execute main function to avoid no reaction scenario
                        AndroidBaseUICore.AndroidGetAndStorgeCurrentImage({"img": GlobalAdapter.CommonVar._Retry_Start_Img_, "function_name": GlobalAdapter.CommonVar._XMLStack_[func_dict["main_func_position"]], "function_status": GlobalAdapter.CommonVar._Retry_Function_Name_})
                        eval(GlobalAdapter.CommonVar._XMLStack_[
                             func_dict["main_func_position"]])
                        time.sleep(1)
                        ##If can execute main function, keep running the rest of steps from next step
                        retry_index = func_dict["main_func_position"] + 1
                    except:
                        ##If cannot execute the main function, it means that the element is no longer on this page
                        ##Maybe it had pop up error or unpredictable behavior
                        dumplogger.exception("Cant click main function again. Back to last page.")
                        ##If not the redirection scenario, back to last page
                        AndroidBaseUICore.AndroidKeyboardAction({"actiontype": "back", "result": "1"})
                        ##Execute from begin function
                        retry_index = func_dict["retry_begin_position"]

                    for retry_steps in range(retry_index, 0):
                        try:
                            ##Execute the rest of steps depends on scenario
                            dumplogger.info("Start retry action: %s" % (GlobalAdapter.CommonVar._XMLStack_[retry_steps]))
                            ##Execute step by step from index defined above
                            AndroidBaseUICore.AndroidGetAndStorgeCurrentImage({"img": GlobalAdapter.CommonVar._Retry_Start_Img_, "function_name": GlobalAdapter.CommonVar._XMLStack_[retry_steps] + str(retry_steps),"function_status": GlobalAdapter.CommonVar._Retry_Function_Name_})
                            eval(GlobalAdapter.CommonVar._XMLStack_[retry_steps])
                            time.sleep(1)
                        except:
                            dumplogger.exception("Encounter retry steps in fail function exception!!!!")
                            if retry_steps == -1 or "API" in GlobalAdapter.CommonVar._XMLStack_[retry_steps]:
                                raise
                dumplogger.info("retry_func_inline_list:{}".format(retry_func_inline_list))

                '''
                ##Retry step by step in fail function
                for func_inline in retry_func_inline_list:
                    try:
                        dumplogger.info("Retry steps in fail function: %s" % (func_inline))
                        exec(func_inline)
                        time.sleep(1)
                    except NameError:
                        dumplogger.exception("This function cannot be execute..")
                    except:
                        dumplogger.exception("Encounter retry steps in fail function exception!!!!")
                        raise
                '''

                ##If not raise exception, case passed
                dumplogger.info("Retry for %s scenario %s pass!!!" % (platform, func_dict["main_func"]))

                ##Reset Retry GlobalAdapter when retry success
                if platform == 'android':
                    GlobalAdapter.CommonVar._Start_Img_[-1] = GlobalAdapter.CommonVar._Retry_Start_Img_[-1]
                    GlobalAdapter.CommonVar._Retry_Start_Img_ = []
                    GlobalAdapter.CommonVar._Retry_Function_Name_ = []

                ##Assign steps before retry back to common variable
                # GlobalAdapter.CommonVar._FuncStack_ = retry_func_inline_list

                return 0
    else:
        ##Reorganize command with function name and function arguments
        retry_command = frame_cur[3] + '(' + str(inspect.getargvalues(frame_cur[0]).locals['arg']) + ')'

        ##Get import module name
        module_name = (inspect.getmodule(frame_cur[0]).__file__).replace(Config.dict_systemslash[Config._platform_], '.')
        module_name = module_name.replace('qalib.', '')
        module_name = module_name.replace('.pyc', '')
        module_name = module_name.replace('.py', '')

        ##Execute import and function
        dumplogger.info("Import module in framework not ok: %s" % (module_name))
        exec('from ' + module_name + ' import *')
        dumplogger.info("Start retry current action in framework not ok: %s" % (retry_command))
        exec(retry_command)
        dumplogger.info("Retry for non-difference retry pass!!!")
        ##Assign steps before retry back to common variable
        GlobalAdapter.CommonVar._FuncStack_ = retry_func_inline_list
        return 1
