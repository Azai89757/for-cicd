import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCProductDetailPage Precondition =="
        dumplogger.info("== Setup CLPCProductDetailPage Precondition ==")

    def test_CLPCProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductDetailPage-01.xml")

    def test_CLPCProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductDetailPage-02.xml")

    def test_CLPCProductDetailPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductDetailPage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCProductDetailPage Post condition =="
        dumplogger.info("== Setup CLPCProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
