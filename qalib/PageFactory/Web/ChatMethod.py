﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ChatMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import common library
import Util
import FrameWorkBase
import DecoratorHelper

##Import Web library
import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class SellerPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        '''
        ClickChatIcon : Click chat icon in seller page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click chat icon in seller page
        xpath = Util.GetXpath({"locate":"chat_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click chat icon in seller page", "result": "1"})

        OK(ret, int(arg['result']), 'SellerPage.ClickChatIcon')


class ChatMainWindow:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteDialogue(arg):
        '''
        DeleteDialogue : Delete target dialogue
                Input argu :
                    user - user name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        user = arg["user"]
        ret = 1

        ##Delete target dialogue if it exists
        xpath = Util.GetXpath({"locate":"user_chat"})
        xpath = xpath.replace('replace_username', user)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

            ##Click action btn for chat
            xpath = Util.GetXpath({"locate":"chat_action"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose action for user chat", "result": "1"})

            ##Choose delete btn
            xpath = Util.GetXpath({"locate":"delete_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Delete chat", "result": "1"})
            time.sleep(15)

            ##Check if user chat still exists
            xpath = Util.GetXpath({"locate":"user_chat"})
            xpath = xpath.replace('string_to_be_replaced', user)
            BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "0"})

        OK(ret, int(arg['result']), 'ChatMainWindow.DeleteDialogue -> ' + user)

class ChatMainWindowButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectChatWindowBottomList(arg):
        '''
        ClickSelectChatWindowBottomList : Select element of chat window bottom list
                Input argu :
                    type - order / product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click specific element
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select bottom list", "result": "1"})

        OK(ret, int(arg['result']), 'ChatMainWindowButton.ClickSelectChatWindowBottomList -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDialogueOrderDetail(arg):
        '''
        ClickDialogueOrderDetail : Click order detial in dialogue
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order detail in dialogue
        xpath = Util.GetXpath({"locate":"order_detail"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click order detail in dialogue", "result": "1"})

        OK(ret, int(arg['result']), 'ChatMainWindowButton.ClickDialogueOrderDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAccepetBargain(arg):
        '''
        ClickAccepetBargain : Click accept bargain
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click accept bargain
        xpath = Util.GetXpath({"locate":"accept_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click accept bargain", "result": "1"})

        OK(ret, int(arg['result']), 'ChatMainWindowButton.ClickAccepetBargain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRejectBargain(arg):
        '''
        ClickRejectBargain : Click reject bargain
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reject bargain
        xpath = Util.GetXpath({"locate":"reject_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click reject bargain", "result": "1"})

        OK(ret, int(arg['result']), 'ChatMainWindowButton.ClickRejectBargain')


class ChatboxOrderSectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderDetail(arg):
        '''
        ClickOrderDetail : Click order detail button in chat box order section page
                Input argu :
                    product_name - product name
                    product_number - product number (avoid duplicate product names, the first one set 1, second set 2)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        product_name = arg["product_name"]
        product_number = arg["product_number"]
        ret = 1

        ##Click order detail button in chat box order section page
        xpath = Util.GetXpath({"locate":"order_detail"})
        xpath = xpath.replace('replace_product_name', product_name)
        xpath = xpath.replace('replace_product_number', product_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click order detail button", "result": "1"})

        OK(ret, int(arg['result']), 'ChatboxOrderSectionButton.ClickOrderDetail ->' + product_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductImage(arg):
        '''
        ClickProductImage : Click product image in chat box order section page
                Input argu :
                    product_name - product name
                    product_number - product number (avoid duplicate product names, the first one set 1, second set 2)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        product_name = arg["product_name"]
        product_number = arg["product_number"]
        ret = 1

        ##Click product image in chat box order section page
        xpath = Util.GetXpath({"locate":"product_image"})
        xpath = xpath.replace('replace_product_name', product_name)
        xpath = xpath.replace('replace_product_number', product_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product image", "result": "1"})

        OK(ret, int(arg['result']), 'ChatboxOrderSectionButton.ClickProductImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTransferOrderDetail(arg):
        '''
        ClickTransferOrderDetail : Click transfer product detail button in chat box order section page
                Input argu :
                    product_name - product name
                    product_number - product number (avoid duplicate product names, the first one set 1, second set 2)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        product_name = arg["product_name"]
        product_number = arg["product_number"]
        ret = 1

        ##Click order detail button in chat box order section page
        xpath = Util.GetXpath({"locate":"transfer_data"})
        xpath = xpath.replace('replace_product_name', product_name)
        xpath = xpath.replace('replace_product_number', product_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click transfer data button", "result": "1"})

        OK(ret, int(arg['result']), 'ChatboxOrderSectionButton.ClickTransferOrderDetail')
