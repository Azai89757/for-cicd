import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpOpenAPIFinance(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpOpenAPIFinance Precondition =="
        dumplogger.info("== Setup TWHttpOpenAPIFinance Precondition ==")

    def test_TWHttpOpenAPIFinance01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-01.xml")

    def test_TWHttpOpenAPIFinance02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-02.xml")

    def test_TWHttpOpenAPIFinance03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-03.xml")

    def test_TWHttpOpenAPIFinance04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-04.xml")

    def test_TWHttpOpenAPIFinance05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-05.xml")

    def test_TWHttpOpenAPIFinance06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-06.xml")

    def test_TWHttpOpenAPIFinance07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-07.xml")

    def test_TWHttpOpenAPIFinance08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-08.xml")

    def test_TWHttpOpenAPIFinance09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-09.xml")

    def test_TWHttpOpenAPIFinance10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-10.xml")

    def test_TWHttpOpenAPIFinance11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-11.xml")

    def test_TWHttpOpenAPIFinance12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpOpenAPIFinance-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpOpenAPIFinance Post condition =="
        dumplogger.info("== Setup TWHttpOpenAPIFinance Post condition ==")
