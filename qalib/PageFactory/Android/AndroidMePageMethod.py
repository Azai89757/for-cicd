﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidMePageMethod.py: The def of this file called by XML mainly.
'''

##import python common library
import re
import time
import random
import datetime

##Import common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic
import AndroidLoginSignUpMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidMainPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        '''
        ClickChatIcon : Click Chat Icon on mepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Chat Icon on mepage
        xpath = Util.GetXpath({"locate":"chat_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Chat Icon on mepage", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMainPage.ClickChatIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollowerName(arg):
        '''
        ClickFollowerName : Click name on follower/following page
                Input argu :
                    name - name of the follower/following on follower/following page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        ret = 1

        ##Click follower/following on follower/following page
        xpath = Util.GetXpath({"locate":"name"})
        xpath = xpath.replace("name_to_be_replaced", name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click name on follower/following page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMainPage.ClickFollowerName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetEmailNow(arg):
        '''
        ClickSetEmailNow : Click set email now on mepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set email now on mepage
        xpath = Util.GetXpath({"locate":"set_email_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click set email now on mepage", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMainPage.ClickSetEmailNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetNameNow(arg):
        '''
        ClickSetNameNow : Click set name, gender now on mepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set name, gender now on mepage
        xpath = Util.GetXpath({"locate":"set_name_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click set name, gender now on mepage", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMainPage.ClickSetNameNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserPage(arg):
        '''
        ClickUserPage : Click function on mepage
                Input argu :
                    type - user photo / background photo / following / follower / user name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click function on mepage
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click function on mepage", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMainPage.ClickUserPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLike(arg):
        '''
        ClickLike : Click Like in mylike page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Like in mylike page
        xpath = Util.GetXpath({"locate":"like_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Like in mylike page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMainPage.ClickLike')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchRole(arg):
        '''
        SwitchRole : Switch role with buyer/seller
                Input argu :
                    type - buyer/seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Switch role
        locate = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click selling tab on Me tab page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMainPage.SwitchRole -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BuyerMePageNavigation(arg):
        '''
        BuyerMePageNavigation : Click on a sub-feature in the buyer me page
                Input argu :
                    type - which progress tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Go to different sub-feature
        locate = Util.GetXpath({"locate":type})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":locate, "direction":"down", "isclick":"1", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMainPage.BuyerMePageNavigation -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SellerMePageNavigation(arg):
        '''
        SellerMePageNavigation : Click on a sub-feature in the buyer me page
                Input argu :
                    type - which progress tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Go to different sub-feature
        locate = Util.GetXpath({"locate":type})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":locate, "direction":"down", "isclick":"1", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMainPage.SellerMePageNavigation -> ' + type)


class AndroidMyAirPayWalletButton:

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubTab(arg):
        '''
        ClickSubTab : Click sub button on my airpay wallet page
                Input argu : type - top_up / withdrawal / more / transactions
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##withdrawal button need to click more button first
        if type == "withdrawal":
            xpath = Util.GetXpath({"locate":"more"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click more button", "result": "1"})

        ##Click sub button on my airpay wallet page
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click sub button on my airpay wallet page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMyAirPayWalletButton.ClickSubTab-> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstTransactionHistory(arg):
        '''
        ClickFirstTransactionHistory : Click first transaction history
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click first transaction history
        xpath = Util.GetXpath({"locate":"transaction_history"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first transaction history", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMyAirPayWalletButton.ClickFirstTransactionHistory')

class AndroidMyAccountPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MyAccountNavigation(arg):
        '''
        MyAccountNavigation : Click on a sub-feature under the "My account" tab
                Input argu :
                    type - which progress tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Go to different sub-page
        locate = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in the me page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMyAccountPage.MyAccountNavigation -> ' + type)


class AndroidEditPersonalInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToSubPage(arg):
        '''
        GoToSubPage : Click to sub pages in edit user info page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        dumplogger.info("type = " + type)

        locate = Util.GetXpath({"locate":type})

        ##Go to change password
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in edit user info page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.GoToSubPage -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePasswordFET(arg):
        '''
        ChangePasswordFET : Change user password
                Input argu :
                    oldpassword - old password
                    newpassword - new password
                    confirm_newpassword - confirm new password
                    scenario - print the scenario
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        oldpassword = arg["oldpassword"]
        newpassword = arg["newpassword"]
        confirm_newpassword = arg["confirm_newpassword"]
        scenario = arg["scenario"]

        ret = 1

        ##Click old password field
        locate = Util.GetXpath({"locate":"password_column"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message": "Click old password field", "result": "1"})

        ##Input old password
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":oldpassword, "result": "1"})

        ##Click Continue
        AndroidMePageButton.ClickContinue({"type":"change_password_page", "result": "1"})

        if newpassword:

            ##Click new password field
            locate = Util.GetXpath({"locate":"new_password_column1"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message": "Click new password field", "result": "1"})

            ##Input new password
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":newpassword, "result": "1"})

            ##Click confirm new password field
            locate = Util.GetXpath({"locate":"new_password_column2"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message": "Click confirm password field", "result": "1"})

            ##Input confirm new password
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":confirm_newpassword, "result": "1"})

            ##Click Reset
            locate = Util.GetXpath({"locate":"reset_button"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click reset button", "result": "1"})

            if 'popup' in scenario:

                ##Click OK after password is reset
                locate = Util.GetXpath({"locate":"ok_button"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click OK", "result": "1"})

                ##Wait for reopening app
                time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangePasswordFET -> ' + oldpassword + ' to ' + newpassword)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeGender(arg):
        '''
        ChangeGender : Change Gender
                Input argu :
                    gender - male/female/other
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        gender = arg["gender"]
        ret = 1

        ##Choose gender
        locate = Util.GetXpath({"locate":gender})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Choose Gender", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeGender -> ' + gender)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeEmail(arg):
        '''
        ChangeEmail : Change Email
                Input argu :
                    password - user password
                    email - email address / if need auto generate, input "auto_generate"
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        email = arg["email"]
        password = arg["password"]

        ##Decide if auto generate email
        if email == "auto_generate":
            email = "auto" + XtFunc.GenerateRandomString(6, "numbers") + "@test.com"
        ##Input password first
        locate = Util.GetXpath({"locate":"password_column"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":password, "result": "1"})

        ##Click continue
        AndroidMePageButton.ClickContinue({"type":"change_email_page", "result": "1"})

        ##Change email address
        time.sleep(3)
        locate = Util.GetXpath({"locate":"email_column"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click email field", "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"delete", "result": "1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":email, "result": "1"})

        ##Click finish
        AndroidMePageButton.ClickFinish({"result": "1"})

        ##Click OK
        AndroidMePageButton.ClickOK({"result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeEmail -> ' + email)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeUserName(arg):
        '''
        ChangeUserName : Change Username
                Input argu :
                    usename - username
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        username = arg["username"]
        ret = 1

        ##Input username
        locate = Util.GetXpath({"locate":"username_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":username, "result": "1"})

        ##Click Continue
        AndroidMePageButton.ClickContinue({"type":"change_username_page", "result": "1"})

        ##Click confirm
        locate = Util.GetXpath({"locate":"confirm_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeUserName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeName(arg):
        '''
        ChangeName : Change name
                Input argu :
                    name - name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        ret = 1

        ##Input name
        locate = Util.GetXpath({"locate":"name_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":name, "result": "1"})

        ##Click Continue
        AndroidMePageButton.ClickContinue({"type":"change_name_page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAvatar(arg):
        '''
        ChangeAvatar : Change avatar in account setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Edit Avatar
        locate = Util.GetXpath({"locate":"edit_avatar"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click edit avatar button", "result": "1"})

        ##Click previous button in image upload and choose page
        locate = Util.GetXpath({"locate":"back_previous"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeAvatar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBackground(arg):
        '''
        ChangeBackground : Change background in account setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Edit background
        locate = Util.GetXpath({"locate":"edit_background"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click edit background button", "result": "1"})

        ##Click previous button in image upload and choose page
        locate = Util.GetXpath({"locate":"back_previous"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBirthDate(arg):
        '''
        ChangeBirthDate : Change User Birth date
                Input argu :
                    date_type - future/now/past
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        date_type = arg["date_type"]
        ret = 1

        ##Get today month time in different region
        month_mapping = {
            "VN": {
                "1":"thg 1",
                "2":"thg 2",
                "3":"thg 3",
                "4":"thg 4",
                "5":"thg 5",
                "6":"thg 6",
                "7":"thg 7",
                "8":"thg 8",
                "9":"thg 9",
                "10":"thg 10",
                "11":"thg 11",
                "12":"thg 12",
            }
        }

        ##Get current time
        if date_type == 'past':
            cur_time = datetime.datetime.now() - datetime.timedelta(days=1)

        elif date_type == 'future':
            cur_time = datetime.datetime.now() + datetime.timedelta(days=1)

        elif date_type == 'now':
            cur_time = datetime.datetime.now()

        else:
            ret = -1
            dumplogger.info("Please check your birth date type %s" % (date_type))

        ##Get today day time
        day = str(cur_time.day)

        ##Get today month time
        month = month_mapping[Config._TestCaseRegion_][str(cur_time.month)]

        ##Get today year time
        year = str(cur_time.year)

        ##Sleep for swipe stable
        time.sleep(3)

        ##Swipe to make submit clickalbe
        AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":"300", "locate_y":"1100", "movex":"300", "movey":"1250", "result": "1"})

        ##Change day
        xpath = Util.GetXpath({"locate":"day"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":day, "result": "1"})

        ##Change month
        xpath = Util.GetXpath({"locate":"month"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":month, "result": "1"})

        ##Change year
        xpath = Util.GetXpath({"locate":"year"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":year, "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangeBirthDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePhone(arg):
        '''
        ChangePhone : Change phone number
                Input argu :
                    phone - account phone / if u want auto genrate, input "auto_generate"
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        phone = arg["phone"]
        ret = 1

        ##Due to close anti-fraud strategy, change phone can be simpify step like following script and without bank number
        ##Auto generate set to 1 and did not enter phone number
        if phone == "auto_generate":
            ##generate phone number with 0082 + random phone number
            phone = "00" + XtFunc.GenerateRandomString(8, "numbers")

        ##Clean phone field
        xpath = Util.GetXpath({"locate": "phone"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click phone number field", "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input phone field
        AndroidEditPersonalInfoPage.InputPhone({"phone": phone, "result": "1"})

        ##Click Continue
        AndroidMePageButton.ClickContinue({"type": "change_phone_page", "result": "1"})
        time.sleep(3)

        ##Input OTP
        xpath = Util.GetXpath({"locate": "otp_block"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": "123456", "result": "1"})

        ##Click Confirm
        AndroidMePageButton.ClickConfirm({"type": "change_phone_otp_page", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ChangePhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditPhone(arg):
        '''
        ClickEditPhone : Click edit phone in change phone page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit phone
        locate = Util.GetXpath({"locate":"edit_text"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click edit button", "result": "1"})
        time.sleep(5)

        if Config._TestCaseRegion_ in ("ID"):
            xpath = Util.GetXpath({"locate":"password_column"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message": "Click password field", "result": "1"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string": "Aa123456", "result": "1"})
            AndroidMePageButton.ClickContinue({"type":"change_phone_page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.ClickEditPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputName(arg):
        '''
        InputName : Input name in name page of my profile
                Input argu :
                    name - name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Input name
        xpath = Util.GetXpath({"locate":"name_field"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype": "delete", "result": "1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate": xpath, "string": name, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.InputName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhone(arg):
        '''
        InputPhone : Input Phone in change phone page
                Input argu :
                    phone - phone number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        phone = arg['phone']
        ret = 1

        ##Input phone
        xpath = Util.GetXpath({"locate": "phone"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": phone, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.InputPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUsername(arg):
        '''
        InputUsername : Input name in name page of my profile
                Input argu :
                    username - username / if need auto generate, input "auto_generate"
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        username = arg["username"]

        ##Auto generate set to 1
        if username == "auto_generate":
            username = "id_user_" + XtFunc.GenerateRandomString(6, "numbers")

        ##Input username
        xpath = Util.GetXpath({"locate":"username_field"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype": "delete", "result": "1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate": xpath, "string": username, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditPersonalInfoPage.InputUsername')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinue(arg):
        '''
        ClickContinue : Click Continue button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click button
        xpath = Util.GetXpath({"locate":"continue_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditPersonalInfoPage.ClickContinue')


class AndroidEditAddressPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToEditAddress(arg):
        '''
        ClickToEditAddress : Click a address field to edit an address in my address page
                Input argu :
                    keyword - for clicking any keyword on the addree field
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        keyword = arg["keyword"]
        ret = 1

        ##Click a address field to edit an address in my address page
        locate = Util.GetXpath({"locate":"address_to_be_edited"})
        locate_replace = locate.replace('address_to_be_edited', keyword)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_replace, "message":"Click a address field to edit an address in my address page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.ClickToEditAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNameAndPhone(arg):
        '''
        InputNameAndPhone : Input name and phone field
                Input argu :
                    name - address name
                    phone - address phone number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        phone = arg["phone"]

        ##Input name/company name
        locate = Util.GetXpath({"locate":"name_icon"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":name, "result": "1"})

        ##Input phone number
        locate = Util.GetXpath({"locate":"phone_column"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":phone, "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputNameAndPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputState(arg):
        '''
        InputState : Input state field
                Input argu :
                    state - address state
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        state = arg["state"]

        ##Click state
        locate = Util.GetXpath({"locate":"state_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click state list", "result": "1"})
        time.sleep(3)

        if Config._TestCaseRegion_ in ("MY"):
            ##Input state
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":state, "result": "1"})

        ##Sleep for waiting list show up
        time.sleep(5)

        ##Select state
        locate = Util.GetXpath({"locate":"state_choose"})
        locate_replace = locate.replace('state_to_be_choosed', state)
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":locate_replace, "direction":"down", "isclick":"1", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputState')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCity(arg):
        '''
        InputCity : Input city field
                Input argu :
                    city - address city
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        city = arg["city"]

        if Config._TestCaseRegion_ in ("TW", "MX", "VN", "PL"):
            ##Choose a city
            locate = Util.GetXpath({"locate":"city_icon"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click city list", "result": "1"})

        ##Sleep for waiting list show up
        time.sleep(5)

        locate = Util.GetXpath({"locate":"city_choose"})
        locate_replace = locate.replace('city_to_be_choosed', city)
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":locate_replace, "direction":"down", "isclick":"1", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputCity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPostcode(arg):
        '''
        InputPostcode : Input postcode field
                Input argu :
                    postcode - postcode
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        postcode = arg["postcode"]

        if Config._TestCaseRegion_ in ("VN", "TH", "ID"):
            ##select postcode
            locate = Util.GetXpath({"locate":"postcode_choose"})
            locate_replace = locate.replace('postcode_to_be_choosed', postcode)
            AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":locate_replace, "direction":"down", "isclick":"1", "result": "1"})

        else:
            ##Input postcode
            locate = Util.GetXpath({"locate":"postcode_column"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":postcode, "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputPostcode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDistrict(arg):
        '''
        InputDistrict : Input district field
                Input argu :
                    district - address district
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        district = arg["district"]

        ##Click and select district
        if Config._TestCaseRegion_ not in ("VN", "PH", "ID"):
            locate = Util.GetXpath({"locate":"district_icon"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Choose a district", "result": "1"})

        locate = Util.GetXpath({"locate":"district_choose"})
        locate_replace = locate.replace('district_to_be_choosed', district)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_replace, "message":"Choose a district", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputDistrict')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTown(arg):
        '''
        InputTown : Input town field
                Input argu :
                    town - address town
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        town = arg["town"]

        ##Click town column
        if Config._TestCaseRegion_ not in ("PH", "VN"):
            locate = Util.GetXpath({"locate":"town_icon"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click town field", "result": "1"})

            ##Use Espresso have to press town list first
            if Config._DesiredCaps_['automationName'] == "Espresso":
                ##Click town list to input or click effectively
                locate = Util.GetXpath({"locate":"town_list"})
                AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": locate, "message":"Click town list", "result": "1"})

        ##Click town
        locate = Util.GetXpath({"locate":"town_choose"})
        locate_replace = locate.replace('town_to_be_choosed', town)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate_replace, "message":"Choose a town", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputTown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDetailAddress(arg):
        '''
        InputDetailAddress : Input detail address field
                Input argu :
                    address - detail address to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        address = arg["address"]

        if Config._TestCaseRegion_ in ("VN", "ID", "MY", "TH", "PH"):

            ##Click detail address column
            locate = Util.GetXpath({"locate": "detail_address_column"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click address detail column", "result": "1"})
            time.sleep(7)

            ##Input address detail
            locate = Util.GetXpath({"locate": "detail_address_input"})
            AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": locate, "string": address, "result": "1"})

            if Config._TestCaseRegion_ in ("VN"):
                time.sleep(3)
                AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

            ##Click submit button
            AndroidEditAddressPage.ClickSubmit({"result": "1"})

        else:

            ##Input address detail
            locate = Util.GetXpath({"locate": "detail_address_input"})
            AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": locate, "string": address, "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputDetailAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStreetName(arg):
        '''
        InputStreetName : Input street name
                Input argu :
                    street - street name to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        street = arg["street"]

        ##Input street name
        locate = Util.GetXpath({"locate": "street_name"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": locate, "string":street, "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputStreetName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSuburb(arg):
        '''
        InputSuburb : Input Suburb
                Input argu :
                    suburb - suburb to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        suburb = arg["suburb"]

        ##Input suburb
        locate = Util.GetXpath({"locate": "suburb_column"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": locate, "string": suburb, "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.InputSuburb')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDefaultAddressSwitch(arg):
        '''
        ClickDefaultAddressSwitch : Click default address switch
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set default address switch
        locate = Util.GetXpath({"locate": "set_default"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": locate, "message": "Click set default address switch", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.ClickDefaultAddressSwitch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseAddressLabel(arg):
        '''
        ChooseAddressLabel : Click label as options
                Input argu :
                    button_type: home / work
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click label as options
        locate = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": locate, "message": "Click label as options", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.ChooseAddressLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit btn
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set default address switch
        locate = Util.GetXpath({"locate": "submit_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": locate, "message": "Click set default address switch", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        '''
        ClickOk : Click ok btn
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn
        locate = Util.GetXpath({"locate": "ok_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": locate, "message": "Click ok btn", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidEditAddressPage.ClickOk')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddConvenientStoreAddress(arg):
        '''
        AddConvenientStoreAddress : Add convenient store address in my address
                Input argu :
                    name - user name or company name
                    phone - phone number
                    shipping - ship by which convenient store
                    store - store
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        phone = arg["phone"]
        shipping = arg["shipping"]
        store = arg["store"]
        ret = 1

        ##Choose a store
        locate = Util.GetXpath({"locate":"select_store_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click select store button", "result": "1"})

        if shipping == "711":
            locate = Util.GetXpath({"locate":shipping})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click search by store name", "result": "1"})
            time.sleep(5)

            ##Click Input by store name
            locate = Util.GetXpath({"locate":"store_name"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":store, "result": "1"})
            time.sleep(1)

            ##Input storename
            locate = Util.GetXpath({"locate":"search_key"})
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":store, "result": "1"})
            time.sleep(1)

            locate = Util.GetXpath({"locate":"send_key"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click send", "result": "1"})
            time.sleep(1)

            ##Click store detail
            locate = Util.GetXpath({"locate":"store_info"})
            locate = locate.replace('to_be_replaced', store)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click store", "result": "1"})
            time.sleep(1)

            ##Confirm store on map
            locate = Util.GetXpath({"locate":"711_choose_store"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click choose store", "result": "1"})
            time.sleep(1)

            ##Agree with rule
            locate = Util.GetXpath({"locate":"agree_confirm"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click agree", "result": "1"})
            time.sleep(1)

            ##Click confirm
            locate = Util.GetXpath({"locate":"agree_confirm"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click confirm", "result": "1"})
            time.sleep(3)

        ##Input name/company name
        locate = Util.GetXpath({"locate":"name_column"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":name, "result": "1"})

        ##Input phone number
        locate = Util.GetXpath({"locate":"phone_column"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":phone, "result": "1"})

        ##Click save
        locate = Util.GetXpath({"locate":"save"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click save", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.AddConvenientStoreAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetDefaultAddress(arg):
        '''
        SetDefaultAddress : Set an address as default address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set default addree
        locate = Util.GetXpath({"locate":"set_to_default_address"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Set default address", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.SetDefaultAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteAddress(arg):
        '''
        ClickDeleteAddress : Click to delete an address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Swipe down first to avoid error
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "result": "1"})

        ##Click delete address
        locate = Util.GetXpath({"locate":"delete_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click delete address", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.ClickDeleteAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckGPSSettingPopup(arg):
        '''
        CheckGPSSettingPopup : Check if GPS setting message pop up
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check if pop up message exists
        xpath = Util.GetXpath({"locate":"popup_message"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click cancel
            AndroidMePageButton.ClickNegative({"result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.CheckGPSSettingPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelLocationPermission(arg):
        '''
        CancelLocationPermission : Close location permission popup
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        if Config._TestCaseRegion_ not in ("PH", "CL", "IN"):
            ##Click confirm button
            locate = Util.GetXpath({"locate":"confirm_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click confirm button", "result": "1"})

        ##Click deny button
        locate = Util.GetXpath({"locate":"deny_location"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click deny location text", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.CancelLocationPermission')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGeoMap(arg):
        '''
        ClickGeoMap : Click Geo map
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Geo block
        locate = Util.GetXpath({"locate":"geo_block"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.ClickGeoMap')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGeoKeyword(arg):
        '''
        InputGeoKeyword : Input search keyword in geo page
                Input argu :
                    keyword - input for search in geo search tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        keyword = arg["keyword"]

        ##Input keyword in Geo search block
        locate = Util.GetXpath({"locate":"search_block"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":locate, "string":keyword, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidEditAddressPage.InputGeoKeyword')


class AndroidMePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainPage(arg):
        '''
        ClickMainPage : Go to main page from mepage
                Input argu :
                    type - account_settings / my_purchases / my_vouchers / my_shopee_coins
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Scroll down to target main page
        xpath = Util.GetXpath({"locate":type})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"method":"xpath", "locate": xpath, "direction":"down", "isclick":"0", "result": "1"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down","times":"1", "result": "1"})

        ##Click main page button
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Go to main page from mepage", "result": "1"})
        time.sleep(5)

        ##Back to mepage and sleep 5 second to avoid keep loading problem
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"back", "result": "1"})
        time.sleep(5)

        ##Click main page button again to avoid keep loading problem
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Go to main page from mepage again", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickMainPage -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : type - page type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click confirm button
        locate = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Go to chat setting page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFinish(arg):
        '''
        ClickFinish : Click finish button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click finish button
        locate = Util.GetXpath({"locate":"finish_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"click finish button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickFinish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button
        locate = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinue(arg):
        '''
        ClickContinue : Click continue button
                Input argu :
                    type - page type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click continue button
        locate = Util.GetXpath({"locate":type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickContinue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPositive(arg):
        '''
        ClickPositive : Click positive button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click positive button
        locate = Util.GetXpath({"locate":"positive_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click positive button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickPositive')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNegative(arg):
        '''
        ClickNegative : Click negative button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click negative button
        locate = Util.GetXpath({"locate":"negative_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click negative button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickNegative')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddAddress(arg):
        ''' ClickAddAddress : Click to add an address in my address page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        locate = Util.GetXpath({"locate":"add_address_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click to add an address in my address page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickAddAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyPurchases(arg):
        '''
        ClickMyPurchases : Click my purchases tab in Me Page
                Input argu :
                    tab_name - to_pay / to_ship / to_receive / to_rate / to_return
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg["tab_name"]

        ##Click my purchases tab in Me Page
        xpath = Util.GetXpath({"locate":tab_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click my purchases tab in Me Page", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickMyPurchases -> ' + tab_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCircle(arg):
        '''
        ClickCircle : Click circle testing icon in Me Page
                Input argu :
                    circle_name - circle1 / circle2 / circle3 / circle4
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_name = arg["circle_name"]

        ##Click circle testing icon in Me Page
        xpath = Util.GetXpath({"locate":circle_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":" Click circle testing icon in Me Page", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickCircle -> ' + circle_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubAccount(arg):
        '''
        ClickSubAccount : Click Sub Account icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Sub Account icon
        xpath = Util.GetXpath({"locate":"sub_account_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Sub Account icon", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidMePageButton.ClickSubAccount')


class AndroidLivePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCartIcon(arg):
        '''
        ClickCartIcon : Click Cart icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart icon on live stream page
        xpath = Util.GetXpath({"locate":"cart_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Cart Icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLivePage.ClickCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        ''' ClickChatIcon : Click Chat icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click chat icon on live stream page
        xpath = Util.GetXpath({"locate":"chat_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Chat Icon", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLivePage.ClickChatIcon')


class AndroidMySellerPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductDetailPage(arg):
        '''
        GoToProductDetailPage : Go to product detail page
                Input argu :
                    product_name - product name which you will go to product detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        product_name = arg['product_name']

        ##Click product name
        xpath = Util.GetXpath({"locate": "product_name"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "click product name", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMySellerPage.GoToProductDetailPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseFirstListedSuccessfully(arg):
        '''
        CloseFirstListedSuccessfully : Close first listed successfully, need to locate with product name
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        product_name = arg['product_name']

        ##Click product listed successfully close btn
        xpath = Util.GetXpath({"locate": "close_btn"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "close product listed successfully", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMySellerPage.CloseFirstListedSuccessfully')


class AndroidShopeePayPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstTransaction(arg):
        '''
        ClickFirstTransaction : Click first transaction history
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click first transaction history
        xpath = Util.GetXpath({"locate":"transaction_history"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first transaction history", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopeePayPage.ClickFirstTransaction')


class AndroidMyLikePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShopping(arg):
        '''
        ClickGoShopping : Click go shopping button in My Like
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go shopping button in my like
        xpath = Util.GetXpath({"locate":"go_shopping_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click go shopping button in my like", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidMyLikePageButton.ClickGoShopping')


class AndroidLanguageSettingButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseLanguage(arg):
        '''
        ChooseLanguage : Choose language in language setting page
                Input argu :
                    language_type - which language want to change
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        language_type = arg["language_type"]

        ##Choose language in language setting page
        time.sleep(3)

        xpath = Util.GetXpath({"locate":"language_button"})
        xpath = xpath.replace("language_type_to_replace", language_type)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose language in language setting page", "result": "1"})

        ##Check device cpu usage is stable
        AndroidBaseUICore.AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

        OK(ret, int(arg['result']), 'AndroidLanguageSettingButton.ChooseLanguage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save language setting
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click save language setting
        time.sleep(3)

        xpath = Util.GetXpath({"locate":"save_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click save language setting", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidLanguageSettingButton.ClickSave')


class AndroidAccountSetUpPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseNotifications(arg):
        '''
        ClickCloseNotifications : Click cancel account setup notifications
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click cancel account setup notifications
        time.sleep(3)
        xpath = Util.GetXpath({"locate":"cross_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel account setup notifications", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAccountSetUpPageButton.ClickCloseNotifications')


class AndroidShopeeWalletPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToTransactionPage(arg):
        '''
        GoToTransactionPage : Click transaction record button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click transaction record button
        xpath = Util.GetXpath({"locate":"transactions"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click transaction record button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopeeWalletPage.GoToTransactionPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstTransaction(arg):
        '''
        ClickFirstTransaction : Click first transaction history
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click first transaction history
        xpath = Util.GetXpath({"locate":"transaction_history"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first transaction history", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidShopeeWalletPage.ClickFirstTransaction')


class AndroidRequestAccuntDeletionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDeletionReason(arg):
        '''
        SelectDeletionReason : Select a reason to delete account
                Input argu :
                    reason_option - change_username / no_longer_use / others
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason_option = arg["reason_option"]

        ##Choose reason in reason page
        xpath = Util.GetXpath({"locate": reason_option})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Choose reason option -> " + reason_option, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRequestAccuntDeletionPage.SelectDeletionReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEmail(arg):
        '''
        InputEmail : Input a email address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input email address to email field in request account deletion page
        xpath = Util.GetXpath({"locate": "email_field"})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": "abc@abc.com", "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRequestAccuntDeletionPage.InputEmail')


class AndroidRequestAccuntDeletionPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeletionReasonPage(arg):
        '''
        ClickDeletionReasonPage : Click button to go to 'choose delete reason' page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reason page button
        xpath = Util.GetXpath({"locate": "reason_page_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click choose deletion reason page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRequestAccuntDeletionPageButton.ClickDeletionReasonPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckbox(arg):
        '''
        ClickCheckbox : Click checkbox to agree to delete account
                Input argu: N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click checkbox button
        xpath = Util.GetXpath({"locate": "checkbox"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRequestAccuntDeletionPageButton.ClickCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSend(arg):
        '''
        ClickSend : Click button to send delete request
                Input argu: N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send button
        xpath = Util.GetXpath({"locate": "send_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRequestAccuntDeletionPageButton.ClickSend')


class AndroidMePageComponent:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMePageComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnIcon(arg):
        '''
        ClickOnIcon : Click any type of icon with well defined locator
            Input argu :
                icon_type - type of icon which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate":icon_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click icon: %s, which xpath is %s" % (icon_type, xpath), "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidMePageComponent.ClickOnIcon')
