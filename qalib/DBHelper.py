#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
MySQLController.py: The def of this file called by other function.
'''
import pymysql
import datetime
import time

##MySQL Interface
class MySQLController():

    def __init__(self, env_config, database=""):

        #print "Enter __init__ of MySQLController"
        ##Initiial DB setting
        self.conn = pymysql.connect(
            host = env_config["host"],
            port = int(env_config["port"]),
            user = env_config["user"],
            passwd = env_config["pwd"],
            charset = env_config["charset"],
            db = database)

        ##Get cursor
        self.cursor = self.conn.cursor()

    def Execute(self, sql):
        ##Execute SQL command
        self.cursor.execute(sql)

        ##Get all of result from DB
        ##"Fetchall" command will return "list(tuple)"
        result2 = self.cursor.fetchall()
        #print "Result = ",
        #print result2
        #print "<br>"

        return result2
        '''##Sequence to load all of records from DB
        row = self.cursor.fetchone()
        print type(row)
        print len(row)
        while row:
            print str(row[0]) + " " + str(row[1]) + " " + str(row[2]) + " " + str(row[3])
            row = self.cursor.fetchone()
        '''

    def Update(self, sql):
        getRC = lambda cur:cur.rowcount if hasattr(cur, 'rowcount') else -1
        self.cursor.execute(sql)
        self.conn.commit()
        #print "Change = %d" %(self.cursor.rowcount)
        return getRC(self.cursor)

    def Insert(self, sql, val):
        self.cursor.execute(sql, val)
        self.conn.commit()

    def CallProc(self, sp_name, para_list=[]):

        if para_list:
            self.cursor.callproc(sp_name, para_list)
        else:
            self.cursor.callproc(sp_name)

        self.cursor.close()
        self.conn.commit()
        #print "Success call!!"

    def GetTitle(self):
        return self.cursor.description

    def __del__(self):
        #print "__del__"
        self.conn.close()
        #print "__del__  ->  deinitial"
