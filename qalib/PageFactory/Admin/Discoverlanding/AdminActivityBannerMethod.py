#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminActivityBannerMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminActivityBannerPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBannerRuleSet(arg):
        '''
        ChangeBannerRuleSet : Change banner to Rule Set List Mode
                Input argu :
                    name - name of the rule set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        name = arg["name"]

        ##Click scope to rule set
        xpath = Util.GetXpath({"locate": "rule_set_scope"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to rule set scope", "result": "1"})
        time.sleep(8)

        ##Click rule set list
        for retry_time in range(3):
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "rule_set_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set list", "result": "1"})
            time.sleep(5)

            ##Click choose rule set
            xpath = Util.GetXpath({"locate": "select_rule_set_name"})
            xpath = xpath.replace("string_to_be_replaced", name)
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose rule set -> " + name, "result": "1"})
                ret = 1
                break

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChangeBannerRuleSet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBannerUserList(arg):
        '''
        ChangeBannerUserList : Change banner User List Mode
                Input argu :
                    project - from which project the file from
                    action - file / image
                    user_list_mode - user list mode (whitelist / blacklist)
                    user_list_action - add / remove
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        project = arg["project"]
        action = arg["action"]
        user_list_mode = arg["user_list_mode"]
        user_list_action = arg["user_list_action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click scope to user list
        xpath = Util.GetXpath({"locate": "user_list_scope"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to user list scope", "result": "1"})
        time.sleep(3)

        for retry_time in range(3):
            ##Click choose user list mode dropdown
            xpath = Util.GetXpath({"locate": "user_list_mode_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user list mode dropdown", "result": "1"})

            ##Click choose user list mode
            xpath = Util.GetXpath({"locate": user_list_mode})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user list mode -> " + user_list_mode, "result": "1"})
                break
            time.sleep(3)

        ##Input csv
        xpath = Util.GetXpath({"locate": user_list_action})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChangeBannerUserList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBannerType(arg):
        '''
        ChooseBannerType : Click add new banner button
                Input argu :
                    type - category/landing_page/group_banner/official_shop/home_popup/skinny/floating_banner/mall_banner
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        banner_type = {
            "category": "1",
            "landing_page": "0",
            "group_banner": "20",
            "official_shop": "3",
            "home_popup": "2",
            "skinny_banner": "4",
            "floating_banner": "5",
            "mall_banner": "21"
        }

        ##Click banner select for visible
        xpath = Util.GetXpath({"locate": "dropdown_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner select", "result": "1"})

        ##Choose option
        xpath = Util.GetXpath({"locate": "select_icon"}).replace('value_to_be_replace', banner_type[type])
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner select", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChooseBannerType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCategoryType(arg):
        '''
        ChooseCategoryType : New category or old category you want to choose
                Input argu :
                    type - new_category / old_category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click banner select for visible
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "dropdown_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner select", "result": "1"})
        time.sleep(5)

        ##Choose option
        xpath = Util.GetXpath({"locate": "category_type"}).replace('value_to_be_replace', type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category type", "result": "1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChooseCategoryType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBannerImg(arg):
        '''
        UploadBannerImg : Upload image for banner
                Input argu :
                    element - input element id or class
                    element_type - element type: id/name/class
                    project - your project name
                    action - image/file
                    image - image name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        xpath = arg["xpath"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        for try_time in range(5):
            ##Upload banner image with input and handle error image upload exception
            locate = Util.GetXpath({"locate": xpath})
            BaseUICore.UploadFileWithCSS({"locate":locate, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})
            time.sleep(15)
            dumplogger.info("Try upload image for %d time." % (try_time))

            image_xpath = Util.GetXpath({"locate": "banner_img"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": image_xpath, "passok": "0", "result": "1"}):
                ret = 1
                break
            else:
                time.sleep(5)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.UploadBannerImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBannerGrouping(arg):
        '''
        CreateBannerGrouping : Create banner grouping
                Input argu :
                    group_name - banner group name
                    dimension - 600*180 / 600*320 / 600*400 / 600*600 / 600*800 / 1200*360 / 1200*640 / 1200*1200
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_name = arg["group_name"]
        dimension = arg["dimension"]

        ##Input group name if needed
        if group_name:
            xpath = Util.GetXpath({"locate": "group_name_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_name, "result": "1"})
            time.sleep(2)

        ##Click dimension if needed
        if dimension:
            xpath = Util.GetXpath({"locate": "dimension_checkbox"})
            xpath = xpath.replace("dimension_to_be_replaced", dimension)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dimension -> " + dimension, "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.CreateBannerGrouping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBannerTypeSearchBar(arg):
        '''
        InputBannerTypeSearchBar : Input banner type search bar
                Input argu :
                    input_text - text to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_text = arg["input_text"]

        ##Input banner type search bar
        xpath = Util.GetXpath({"locate": "search_bar_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.InputBannerTypeSearchBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchUploadBannerImage(arg):
        '''
        BatchUploadBannerImage : Batch upload banner image
                Input argu :
                    banner_type - banner type
                    project - project where the file from
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click banner type dropdown
        if banner_type:
            xpath = Util.GetXpath({"locate": "banner_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner type dropdown", "result": "1"})
            time.sleep(3)

            ##Click choose banner type
            xpath = Util.GetXpath({"locate": "search_bar_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": banner_type, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(3)

        ##Input file
        xpath = Util.GetXpath({"locate": "file_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action":action, "file_name":file_name, "file_type":file_type, "result":"1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.BatchUploadBannerImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterBatchUploadListWithStatus(arg):
        '''
        FilterBatchUploadListWithStatus : Filter batch upload list with status
                Input argu :
                    status - batch upload status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click status filter dropdown
        xpath = Util.GetXpath({"locate": "filter_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status filter dropdown", "result": "1"})
        time.sleep(1)

        ##Click choose status
        xpath = Util.GetXpath({"locate": "filter_content"})
        xpath = xpath.replace("status_to_be_replaced", status)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose status -> " + status, "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.FilterBatchUploadListWithStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBanner(arg):
        '''
        CreateBanner : Create Banner
                Input argu :
                    banner_type - type of banner (home_popup / home_carousel /category / mall_carousel / skinny / floating / mall_banner / digital_product_scrolling / digital_product_carousel/ mobile_browser)
                    adjust_start_time - start time of the banner (format : 1 for 1 minutes after current time stamp)
                    adjust_end_time - end time of the banner (format : -1 for 1 minutes before current time stamp)
                    visibility_type : visible / invisible
                    url - redirect url for the banner
                    title - title of the banner
                    is_upload : 1 - upload
                                0 - not upload
                    description - description
                    text_en - english text
                    text_local - local text
                    text_color - text color hex
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        banner_type = arg["banner_type"]
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        visibility_type = arg["visibility_type"]
        url = arg["url"]
        title = arg["title"]
        is_upload = arg["is_upload"]
        description = arg["description"]
        text_en = arg["text_en"]
        text_local = arg["text_local"]
        text_color = arg["text_color"]
        ret = 1

        time.sleep(5)
        BaseUILogic.BrowserRefresh({"message": "Browser refresh", "result": "1"})
        time.sleep(10)

        ##Choose if Visible
        if visibility_type:
            xpath = Util.GetXpath({"locate": visibility_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility", "result": "1"})

        ##Input URL
        if banner_type != "skinny":
            xpath = Util.GetXpath({"locate": "url"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": "url1"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        ##Input title or text
        if banner_type == "digital_product_scrolling":

            ##Input text information
            if text_en:
                xpath = Util.GetXpath({"locate": "text_en_input"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_en, "result": "1"})
                time.sleep(2)
            if text_local:
                xpath = Util.GetXpath({"locate": "text_local_input"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_local, "result": "1"})
                time.sleep(2)
            if text_color:
                xpath = Util.GetXpath({"locate": "text_color_input"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text_color, "result": "1"})
                time.sleep(2)

        else:
            if title:
                xpath = Util.GetXpath({"locate": "title"})
                BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        if banner_type == "category":
            ##Click second dropdown list
            xpath = Util.GetXpath({"locate": "dropdown_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click dropdown list", "result": "1"})
            time.sleep(3)

            ##Choose category type
            xpath = Util.GetXpath({"locate": "category_banner_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "category_banner", "result": "1"})
            time.sleep(3)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(3)

        ##Upload specific banner picture
        if int(is_upload):
            ##Chose differen banner pictures
            if banner_type in ("home_popup", "mall_popup", "category", "mall_carousel", "digital_product_carousel", "digital_product_scrolling"):
                ##Upload banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": banner_type, "project": "ActivityBanner", "action": "image", "file_name": banner_type + "_1", "file_type": "jpg", "result": "1"})

            elif banner_type in ("home_carousel", "skinny", "mall_banner"):
                ##Upload mobile banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": "mobile_" + banner_type, "project": "ActivityBanner", "action": "image", "file_name": "mobile_" + banner_type + "_1", "file_type": "jpg", "result": "1"})

                ##Upload pc banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": "pc_" + banner_type, "project": "ActivityBanner", "action": "image", "file_name": "pc_" + banner_type + "_1", "file_type": "jpg", "result": "1"})

            elif banner_type == "floating":
                ##Upload mobile banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": "mobile_" + banner_type, "project": "ActivityBanner", "action": "image", "file_name": "mobile_" + banner_type + "_1", "file_type": "png", "result": "1"})

                ##Upload pc banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": "pc_" + banner_type, "project": "ActivityBanner", "action": "image", "file_name": "pc_" + banner_type + "_1", "file_type": "png", "result": "1"})

        ##Input Start Time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_start_time), 0)
            time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify start time
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
        else:
            dumplogger.info("Do not setting start time.")

        ##Input End Time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_end_time), 0)
            time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_end_time), 0)

            ##Modify end date
            xpath = Util.GetXpath({"locate": "end_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify end time
            xpath = Util.GetXpath({"locate": "end_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
        else:
            dumplogger.info("Do not setting end time.")

        ##Input Description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        time.sleep(15)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.CreateBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterBannerDisplayByPlatform(arg):
        '''
        FilterBannerDisplayByPlatform : Edit banner platform hide
                Input argu :
                    ios_web - ios web platform hide or not (checked)
                    ios_app - ios app platform hide or not (checked)
                    android_web - android web platform hide or not (checked)
                    android_app - android app platform hide or not (checked)
                    pc - pc platform hide or not (checked)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ios_web = arg["ios_web"]
        ios_app = arg["ios_app"]
        android_web = arg["android_web"]
        android_app = arg["android_app"]
        pc_mall = arg["pc_mall"]

        ##Choose hide platform
        if ios_web:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "ios_web"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in ios web", "result": "1"})
        if ios_app:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "ios_app"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in ios app", "result": "1"})
        if android_web:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "android_web"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in android web", "result": "1"})
        if android_app:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "android_app"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in android app", "result": "1"})
        if pc_mall:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "pc_mall"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in pc", "result": "1"})

        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.FilterBannerDisplayByPlatform')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerGIF(arg):
        '''
        EditBannerGIF : Edit banner GIF image
                Input argu :
                    project - project where the image from
                    action - file / image
                    banner_type - banner type (floating / skinny / home_popup / mall_popup)
                    file_name - upload file name (just need to enter the previous name since file type is always .gif)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        project = arg["project"]
        action = arg["action"]
        banner_type = arg["banner_type"]
        file_name = arg["file_name"]

        time.sleep(15)

        if banner_type == "home_popup" or banner_type == "mall_popup":
            ##Upload single GIF
            locate = Util.GetXpath({"locate": banner_type})
            BaseUICore.UploadFileWithCSS({"locate": locate, "action": action, "file_name": file_name, "file_type": "gif", "result": "1"})
            time.sleep(15)

        elif banner_type == "skinny" or banner_type == "floating":
            ##Upload mobile GIF
            locate = Util.GetXpath({"locate": "mobile_" + banner_type})
            BaseUICore.UploadFileWithCSS({"locate": locate, "action": action, "file_name": "mobile_" + file_name, "file_type": "gif", "result": "1"})
            time.sleep(15)

            ##Upload PC GIF
            locate = Util.GetXpath({"locate": "pc_" + banner_type})
            BaseUICore.UploadFileWithCSS({"locate": locate, "action": action, "file_name": "pc_" + file_name, "file_type": "gif", "result": "1"})
            time.sleep(15)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.EditBannerGIF')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerByDesciption(arg):
        '''
        EditBannerByDesciption : Use description to find specific banner and edit it
                Input argu :
                    banner_type - banner type
                    description - banner description
                    status - banner status (Live / Disabled)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        description = arg["description"]
        status = arg["status"]
        order_column = "1"

        ##Determine order column number
        if banner_type == "category" or banner_type == "mall_carousel":
            order_column = "2"

        ##Get and reserve banner order
        order_xpath = Util.GetXpath({"locate": "edit_icon_order"})
        order_xpath = order_xpath.replace("description_to_be_replaced", description).replace("status_to_be_replaced", status).replace("order_index_to_be_replaced", order_column)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to edit banner
        xpath = Util.GetXpath({"locate": "edit_banner_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit banner", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.EditBannerByDesciption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddCampaignLabel(arg):
        '''
        AddCampaignLabel : Add campaign label
                Input argu :
                    first_label - first label
                    second_label - second label
                    third_label - third label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        first_label = arg["first_label"]
        second_label = arg["second_label"]
        third_label = arg["third_label"]

        if first_label:
            ##Retry to click first dropdown list
            for retry_time in range(3):
                xpath = Util.GetXpath({"locate": "dropdown_list"})
                xpath = xpath.replace("order_to_be_replaced", "1")
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    ##Click dropdown list
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list 1", "result": "1"})
                    time.sleep(5)
                else:
                    dumplogger.info("Click the dropdown list successfully.")
                    break

            ##Choose dropdown content
            xpath = Util.GetXpath({"locate": "dropdown_content"})
            xpath = xpath.replace("string_to_be_replaced", first_label)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
            time.sleep(5)

        if second_label:
            ##Click add icon
            xpath = Util.GetXpath({"locate": "last_add_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add icon", "result": "1"})
            time.sleep(10)

            ##Retry to click second dropdown list
            for retry_time in range(3):
                xpath = Util.GetXpath({"locate": "dropdown_list"})
                xpath = xpath.replace("order_to_be_replaced", "2")
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    ##Click dropdown list
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list 2", "result": "1"})
                    time.sleep(5)
                else:
                    dumplogger.info("Click the dropdown list successfully.")
                    break

            ##Choose dropdown content
            xpath = Util.GetXpath({"locate": "dropdown_content"})
            xpath = xpath.replace("string_to_be_replaced", second_label)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
            time.sleep(5)

        if third_label:
            ##Click add icon
            xpath = Util.GetXpath({"locate": "last_add_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add icon", "result": "1"})
            time.sleep(5)

            ##Click dropdown list
            xpath = Util.GetXpath({"locate": "dropdown_list"})
            xpath = xpath.replace("order_to_be_replaced", "3")
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list 3", "result": "1"})
            time.sleep(5)

            ##Choose dropdown content
            xpath = Util.GetXpath({"locate": "dropdown_content"})
            xpath = xpath.replace("string_to_be_replaced", third_label)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.AddCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBannerOrder(arg):
        '''
        ChangeBannerOrder : Change banner order to by input order number
                Input argu:
                    description - banner description
                    order - order after adjustment
                    action - action after input order (save / cancel)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]
        order = arg["order"]
        action = arg["action"]

        ##Click order edit icon
        xpath = Util.GetXpath({"locate": "order_edit_icon"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        time.sleep(2)

        ##Change order number
        xpath = Util.GetXpath({"locate": "order_input"})
        xpath = xpath.replace("string_to_be_replaced", description)
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": order, "result": "1"})
        time.sleep(2)

        ##Save or cancel
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose post action -> " + action, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChangeBannerOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBanner(arg):
        '''
        DeleteBanner : Delete Banner
                Input argu :
                    banner_type - banner type (format: popup, landing_page, category, official_shop, skinny, floating, mall_banner, mall_shop_banner)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]

        if banner_type == "mall_banner" or banner_type == "mall_shop_banner":
            ##Click delete btn
            xpath = Util.GetXpath({"locate": "delete_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete mall banner", "result": "1"})
            time.sleep(2)

            ##Click confirm deletion btn
            xpath = Util.GetXpath({"locate": "confirm"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Confirm to delete mall banner", "result": "1"})
        else:
            ##For rest of the banner type
            xpath = Util.GetXpath({"locate": "trash_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete banner", "result": "1"})
            time.sleep(8)
            if BaseUILogic.DetectPopupWindow():
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.DeleteBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditImpressionThreshold(arg):
        '''
        EditImpressionThreshold : Edit impression threshold
                Input argu :
                    threshold - value of threshold
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        threshold = arg["threshold"]

        ##input threshold (0 < threshold < 100)
        xpath = Util.GetXpath({"locate": "threshold_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": threshold, "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.EditImpressionThreshold')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerImg(arg):
        '''
        EditBannerImg : Edit Banner Img
                Input argu :
                    project - from which project the image
                    action - file / action
                    banner_type - (format: home_popup, landing_page, category, official_shop, skinny, floating, mall_banner, mall_shop_banner)
                    file_name - image file name
                    file_type - image file type
                    second_file_name - second image file name (skinny / mall_banner / floating)
                    second_file_type - second image file type (skinny / mall_banner / floating)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        banner_type = arg["banner_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        second_file_name = arg["second_file_name"]
        second_file_type = arg["second_file_type"]

        if banner_type in ("home_popup", "mall_carousel", "category", "mall_popup", "digital_product", "floating"):
            ##Click to upload banner
            AdminActivityBannerPage.UploadBannerImg({"xpath": banner_type, "project": Config._TestCaseFeature_, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        elif banner_type in ("home_carousel", "skinny", "mall_banner"):
            ##Upload mobile banner
            AdminActivityBannerPage.UploadBannerImg({"xpath": "mobile_" + banner_type, "project": Config._TestCaseFeature_, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

            ##Upload pc banner
            AdminActivityBannerPage.UploadBannerImg({"xpath": "pc_" + banner_type, "project": Config._TestCaseFeature_, "action": action, "file_name": second_file_name, "file_type": second_file_type, "result": "1"})

        time.sleep(20)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.EditBannerImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanAllBanners(arg):
        '''
        CleanAllBanners : Clean all kinds of banners before we start running homepage banner automation test.
                Input argu :
                    banner_type - banner type
                    platform - pc / android
                Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''

        ret = 1
        banner_type = arg["banner_type"]
        platform = arg["platform"]
        order_column = "1"

        ##Determine order column number
        if banner_type == "category" or banner_type == "mall_carousel":
            order_column = "2"

        ##Count current exist banner numbers
        time.sleep(5)
        delete_icon_xpath = Util.GetXpath({"locate": platform})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": delete_icon_xpath, "reservetype": "count", "result": "1"})
        delete_icon_count = GlobalAdapter.GeneralE2EVar._Count_

        ##If current exist banner number more than one, start delete each banner, if not, skip process
        if delete_icon_count:
            for icon_count in range(delete_icon_count):
                ##Get and reserve banner order
                order_xpath = Util.GetXpath({"locate": platform + "_order"})
                order_xpath = order_xpath.replace("order_index_to_be_replaced", order_column)
                BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

                ##Click to delete banner
                xpath = Util.GetXpath({"locate": "delete_banner_icon"})
                xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete banner", "result": "1"})
                time.sleep(1)
                confirm_xpath = Util.GetXpath({"locate": "confirm_btn"})
                BaseUICore.Click({"method": "xpath", "locate": confirm_xpath, "message": "Confirm delete banner", "result": "1"})
                dumplogger.info("Deleting %d banner" % (icon_count))
                time.sleep(2)

                ##Refresh browser
                BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})
                time.sleep(5)

        dumplogger.info("Clean all banners succeed.")

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.CleanAllBanners')


class AdminActivityBannerButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewBannerButton(arg):
        '''
        ClickAddNewBannerButton : Click add new banner button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new banner button
        BaseUILogic.BrowserRefresh({"message": "Browser refresh", "result": "1"})
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new banner button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickAddNewBannerButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefreshButton(arg):
        '''
        ClickRefreshButton : Click refresh btn in batch upload page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refresh btn
        xpath = Util.GetXpath({"locate": "refresh_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refresh btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickRefreshButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplateCSV(arg):
        '''
        ClickDownloadTemplateCSV : Click download template csv in batch upload page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download template csv
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download template csv", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickDownloadTemplateCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseBannerTypeDropdown(arg):
        '''
        ClickChooseBannerTypeDropdown : Click dropdown list of choose banner type
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click dropdown list of choose banner type
        xpath = Util.GetXpath({"locate": "banner_type_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list of choose banner type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickChooseBannerTypeDropdown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUpload(arg):
        '''
        ClickBatchUpload : Click batch upload button in banner page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch upload button
        xpath = Util.GetXpath({"locate": "batch_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickBatchUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToBannerList(arg):
        '''
        ClickBackToBannerList : Click back to banner list btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to banner list btn
        xpath = Util.GetXpath({"locate": "back_to_banner_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to banner list button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickBackToBannerList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewBannerGroupingButton(arg):
        '''
        ClickAddNewBannerGroupingButton : Click add new banner grouping button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new banner grouping button
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new banner grouping button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickAddNewBannerGroupingButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        ##Handle timeout error or parameter error
        for try_time in range(3):
            error_xpath = Util.GetXpath({"locate": "error_msg"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": error_xpath, "passok": "0", "result": "1"}):
                time.sleep(15)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
            else:
                dumplogger.info("Click save btn successfully!!!")
                break

        time.sleep(15)

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelButton(arg):
        '''
        ClickCancelButton : Click cancel button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickCancelButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditBannerButton(arg):
        '''
        ClickEditBannerButton : Click to Edit Banner
            Input argu :
                platform - pc/android
                banner_type - banner type (format: popup, landing_page, category, official_shop, skinny, floating, mall_banner, mall_shop_banner)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        platform = arg["platform"]
        banner_type = arg["banner_type"]
        order_column = "1"

        ##Determine order column number
        if banner_type == "category" or banner_type == "mall_carousel":
            order_column = "2"

        ##Get and reserve banner order
        order_xpath = Util.GetXpath({"locate": platform + "_order"})
        order_xpath = order_xpath.replace("order_index_to_be_replaced", order_column)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to edit banner
        xpath = Util.GetXpath({"locate": "edit_banner_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit banner -> " + banner_type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickEditBannerButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicateBannerButton(arg):
        '''
        ClickDuplicateBannerButton : Click to duplicate Banner
            Input argu :
                platform - pc/android
                banner_type - banner type (format: popup, landing_page, category, official_shop, skinny, floating, mall_banner, mall_shop_banner)
                is_continue - 1: continue ; 0: no continue
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        platform = arg["platform"]
        banner_type = arg["banner_type"]
        is_continue = arg["is_continue"]
        order_column = "1"

        ##Determine order column number
        if banner_type == "category" or banner_type == "mall_carousel":
            order_column = "2"

        ##Get and reserve banner order
        order_xpath = Util.GetXpath({"locate": platform + "_order"})
        order_xpath = order_xpath.replace("order_index_to_be_replaced", order_column)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":order_xpath, "reservetype":"banner_order_id", "result": "1"})

        ##Click to duplicate banner
        xpath = Util.GetXpath({"locate": "duplicate_banner_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to duplicate banner -> " + banner_type, "result": "1"})
        time.sleep(3)

        ##Determine whether continue or not
        if int(is_continue):
            xpath = Util.GetXpath({"locate": "continue_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to continue", "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickDuplicateBannerButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyBannerButton(arg):
        '''
        ClickCopyBannerButton : Click to copy Banner
            Input argu :
                banner_type - banner type (format: popup, landing_page, category, official_shop, skinny, floating, mall_banner, mall_shop_banner)
                platform - pc/android
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]
        platform = arg["platform"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": platform})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to copy banner -> " + banner_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickCopyBannerButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeBannerOrderButton(arg):
        '''
        ClickChangeBannerOrderButton : Change Banner Order
                Input argu :
                    banner_type - banner type (format: popup, landing_page, category, official_shop, skinny, floating, mall_shop_banner)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]

        ##Click to change banner order
        xpath = Util.GetXpath({"locate": banner_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change order: " + banner_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickChangeBannerOrderButton -> ' + banner_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveBannerOrderButton(arg):
        '''
        ClickSaveBannerOrderButton : Save Banner Order
                Input argu :
                    banner_type - banner type (format: popup, landing_page, category, official_shop, skinny, floating)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        banner_type = arg["banner_type"]

        ##Click to change banner order
        xpath = Util.GetXpath({"locate": banner_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to change order: " + banner_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminActivityBannerButton.ClickSaveBannerOrderButton')


class AdminBannerTemplatesPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBannerTemplateAction(arg):
        '''
        ChooseBannerTemplateAction : Click edit btn or preview btn of templates registration
                Input argu :
                    template_id - templates order id
                    template_name - template name
                    action_type - edit / preview
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        template_id = arg['template_id']
        template_name = arg['template_name']
        action_type = arg['action_type']

        ##Click edit btn or preview btn of templates registration
        xpath = Util.GetXpath({"locate": action_type})
        xpath = xpath.replace("id_to_be_replaced", template_id).replace("name_to_be_replaced", template_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action type -> " + action_type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.ChooseBannerTemplateAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTimeIsCurrent(arg):
        '''
        CheckTimeIsCurrent : Check update_time / create_time default is current time
                Input argu :
                    column - update_time / create_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        column = arg["column"]

        ##Get current time
        current_time = XtFunc.GetCurrentDateTime("%d/%m/%Y %H:%M", 0, 0, 0)[:-5]

        ##Get default time
        xpath = Util.GetXpath({"locate": column})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute":"value", "mode":"single", "result": "1"})
        default_time = GlobalAdapter.CommonVar._PageAttributes_[:-15]

        ##Compare current time and default time
        if default_time == current_time:
            ret = 1
            dumplogger.info("Current time %s equals to defalut time %s" % (current_time, default_time))
        else:
            dumplogger.info("Current time %s not equals to defalut time %s" % (current_time, default_time))

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.CheckTimeIsCurrent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseVisibleSection(arg):
        '''
        ChooseVisibleSection : Choose visible or invisible in section
                Input argu :
                    type - visible / invisible you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg["type"]

        ##Choose visible or invisible in section
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visible type -> " + type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChooseVisibleSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseRegionsSection(arg):
        '''
        ChooseRegionsSection : Choose region in region section
                Input argu :
                    region - ALL / ID / TH / VN / SG / PH / MY / TW you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        region = arg["region"]

        ##Choose region in region section
        xpath = Util.GetXpath({"locate": "region"})
        xpath = xpath.replace("region_to_be_replaced", region)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click region type -> " + region, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminActivityBannerPage.ChooseRegionsSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseLanguageSection(arg):
        '''
        ChooseLanguageSection : Choose language in region section
                Input argu :
                    language - language you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        language = arg["language"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("string_to_be_replaced", language)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.ChooseLanguageSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLanguageKeyword(arg):
        '''
        InputLanguageKeyword : Input keyword to search language
                Input argu :
                    language - language you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        language = arg["language"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "language_input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": language, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.InputLanguageKeyword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBannerModeSection(arg):
        '''
        ChooseBannerModeSection : Choose banner mode in region section
                Input argu :
                    banner_mode - banner mode you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        banner_mode = arg["banner_mode"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("string_to_be_replaced", banner_mode)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.ChooseBannerModeSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBannerModeKeyword(arg):
        '''
        InputBannerModeKeyword : Input keyword to search banner mode
                Input argu :
                    banner_mode - banner mode you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_mode = arg["banner_mode"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": banner_mode, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.InputBannerModeKeyword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseLayoutModeSection(arg):
        '''
        ChooseLayoutModeSection : Choose layout mode in layout mode section
                Input argu :
                    layout_mode - layout mode you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        layout_mode = arg["layout_mode"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("string_to_be_replaced", layout_mode)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.ChooseLayoutModeSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLayoutModeKeyword(arg):
        '''
        InputLayoutModeKeyword : Input keyword to search layout mode
                Input argu :
                    layout_mode - layout mode you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        layout_mode = arg["layout_mode"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": layout_mode, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.InputLayoutModeKeyword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseAppTypeSection(arg):
        '''
        ChooseAppTypeSection : Choose app type in app type section
                Input argu :
                    app_type - app type you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        app_type = arg["app_type"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(5)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("string_to_be_replaced", app_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.ChooseAppTypeSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAppTypeKeyword(arg):
        '''
        InputAppTypeKeyword : Input keyword to search app type
                Input argu :
                    app_type - app type you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        app_type = arg["app_type"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(8)

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": app_type, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.InputAppTypeKeyword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSampleImg(arg):
        '''
        UploadSampleImg : Upload image for template
                Input argu :
                    image - image name you want to uplaod
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        file_name = arg["file_name"]

        ##Upload sample img
        xpath = Util.GetXpath({"locate": "banner_img"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": "png", "result": "1"})
        time.sleep(15)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.UploadSampleImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFEJson(arg):
        '''
        InputFEJson : Input FE json to json field
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Input FE json to json field
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "{}", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesPage.InputFEJson')


class AdminBannerTemplatesButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewTemplateButton(arg):
        '''
        ClickAddNewTemplateButton : Click add new template btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new template btn
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new template btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesButton.ClickAddNewTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelTemplateButton(arg):
        '''
        ClickCancelTemplateButton : Click edit cancel btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesButton.ClickCancelTemplateButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClosePreviewTemplate(arg):
        '''
        ClickClosePreviewTemplate : Click close preview btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close preview btn
        xpath = Util.GetXpath({"locate": "close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close preview btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesButton.ClickClosePreviewTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewImgButton(arg):
        '''
        ClickAddNewImgButton : Click add new sample img btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new sample img
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new sample img btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesButton.ClickAddNewImgButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click save btn
                Input argu :
                    page_type - sample_img_window / auto_banner_template
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page_type = arg['page_type']

        ##Click save btn
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesButton.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditButton(arg):
        '''
        ClickEditButton : Click edit sample img btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit sample img btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBannerTemplatesButton.ClickEditButton')


class AdminCampaignUnitPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBanner(arg):
        '''
        EditBanner : click edit banner in all campaign unit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to edit banner
        xpath = Util.GetXpath({"locate": "edit_banner_icon"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit banner", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.EditBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCampaignUnit(arg):
        '''
        EditCampaignUnit : Use name to find specific campaign unit and click edit info btn in all campaign unit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click more btn
        xpath = Util.GetXpath({"locate": "more_icon"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        time.sleep(3)

        ##Click to edit campaign info
        xpath = Util.GetXpath({"locate": "edit_info_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit campaign info", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.EditCampaignUnit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchCampaignUnitByName(arg):
        '''
        SearchCampaignUnitByName : Use campaign unit name to search
                Input argu :
                    campaign_unit_name - campaign unit name you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign_unit_name = arg["campaign_unit_name"]

        ##Click search icon
        xpath = Util.GetXpath({"locate": "search_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search icon", "result": "1"})
        time.sleep(3)

        ##Input campaign unit name to search
        xpath = Util.GetXpath({"locate": "search_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": campaign_unit_name, "result": "1"})
        time.sleep(3)

        ##Click search
        xpath = Util.GetXpath({"locate": "search_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.SearchCampaignUnitByName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanAllBanners(arg):
        '''
        CleanAllBanners : Clean all banners in campaign unit group
                Input argu :
                    campaign_unit_name - campaign_unit_name type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        campaign_unit_name = arg["campaign_unit_name"]

        ##Search campaign
        AdminCampaignUnitPage.SearchCampaignUnitByName({"campaign_unit_name": campaign_unit_name, "result": "1"})
        time.sleep(5)

        ##Go to all banner page in campaign unit page
        AdminCampaignUnitPage.EditBanner({"result": "1"})
        time.sleep(8)

        ##Count current exist banner numbers
        xpath = Util.GetXpath({"locate": "more_icon"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
        more_icon_count = GlobalAdapter.GeneralE2EVar._Count_

        ##If current exist banner number more than one, start delete each banner, if not, skip process
        if more_icon_count:
            for icon_count in range(more_icon_count):

                ##Move to more icon and click delete btn
                xpath_drop_down = Util.GetXpath({"locate": "more_icon"})
                xpath_delete = Util.GetXpath({"locate": "delete_banner_icon"})
                BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath_drop_down, "locatehidden": xpath_delete, "result": "1"})

                ##Click confirm delete btn
                xpath_confirm = Util.GetXpath({"locate": "confirm_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath_confirm, "message": "Confirm delete banner", "result": "1"})
                dumplogger.info("Deleting %d banner" % (icon_count))
                time.sleep(10)

        dumplogger.info("Clean all banners succeed.")

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.CleanAllBanners')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateBanner(arg):
        '''
        CreateBanner : Create Banner
                Input argu :
                    banner_type - type of banner (home_popup / home_carousel /category / mall_carousel / skinny / floating / mall_banner / digital_product_scrolling / digital_product_carousel/ mobile_browser)
                    visibility_type : visible / invisible
                    url - redirect url for the banner
                    title - title of the banner
                    is_upload : 1 - upload
                                0 - not upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        banner_type = arg["banner_type"]
        visibility_type = arg["visibility_type"]
        url = arg["url"]
        title = arg["title"]
        is_upload = arg["is_upload"]

        ##Refresh page
        BaseUILogic.BrowserRefresh({"message": "Browser refresh", "result": "1"})
        time.sleep(5)

        ##Choose if Visible
        if visibility_type:
            xpath = Util.GetXpath({"locate": visibility_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility", "result": "1"})

        ##Input banner name
        xpath = Util.GetXpath({"locate": "title"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Input URL
        xpath = Util.GetXpath({"locate": "url"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        ##Upload specific banner picture
        if int(is_upload):
            ##Chose differen banner pictures
            if banner_type in ("home_popup", "mall_popup", "category", "floating", "mall_carousel", "digital_product_carousel", "digital_product_scrolling"):
                ##Upload banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": banner_type, "project": "ActivityBanner", "action": "image", "file_name": banner_type + "_1", "file_type": "jpg", "result": "1"})

            elif banner_type in ("home_carousel", "skinny", "mall_banner"):
                ##Upload mobile banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": "mobile_" + banner_type, "project": "ActivityBanner", "action": "image", "file_name": "mobile_" + banner_type + "_1", "file_type": "jpg", "result": "1"})

                ##Upload pc banner
                AdminActivityBannerPage.UploadBannerImg({"xpath": "pc_" + banner_type, "project": "ActivityBanner", "action": "image", "file_name": "pc_" + banner_type + "_1", "file_type": "jpg", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.CreateBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseOverElement(arg):
        '''
        ChooseCountdownTimer : Choose countdown timer
                Input argu :
                    element_type - coloreddot / countdowntimer
                    visibility_type - visible / invisible
                    vertical_offset_value - vertical offset value
                    horizontal_offset_value - horizontal offset value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element_type = arg["element_type"]
        visibility_type = arg["visibility_type"]
        vertical_offset_value = arg["vertical_offset_value"]
        horizontal_offset_value = arg["horizontal_offset_value"]

        ##Choose if Visible
        xpath = Util.GetXpath({"locate": visibility_type + "_" + element_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click visibility", "result": "1"})

        ##Input vertical offset value
        xpath = Util.GetXpath({"locate": "vertical_offset_value_" + element_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": vertical_offset_value, "result": "1"})

        ##Input horizontal offset value
        xpath = Util.GetXpath({"locate": "horizontal_offset_value_" + element_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": horizontal_offset_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.ChooseOverElement')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCountdownTimerEndTime(arg):
        '''
        SetCountdownTimerEndTime : set countdown timer end time
                Input argu :
                    end_time - time slot end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        end_time = arg["end_time"]

        ##Get current date time and add minutes on it
        time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(end_time), 0)
        time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(end_time), 0)

        ##Modify end date
        xpath = Util.GetXpath({"locate": "end_date_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "end_date_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
        time.sleep(2)

        ##Modify end time
        xpath = Util.GetXpath({"locate": "end_time_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "end_time_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.SetCountdownTimerEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterBannerDisplayByPlatform(arg):
        '''
        FilterBannerDisplayByPlatform : Edit banner platform hide
                Input argu :
                    ios_web - ios web platform hide or not (checked)
                    ios_app - ios app platform hide or not (checked)
                    android_web - android web platform hide or not (checked)
                    android_app - android app platform hide or not (checked)
                    pc - pc platform hide or not (checked)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ios_web = arg["ios_web"]
        ios_app = arg["ios_app"]
        android_web = arg["android_web"]
        android_app = arg["android_app"]
        pc_mall = arg["pc_mall"]

        ##Choose hide platform
        if ios_web:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "ios_web"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in ios web", "result": "1"})
        if ios_app:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "ios_app"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in ios app", "result": "1"})
        if android_web:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "android_web"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in android web", "result": "1"})
        if android_app:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "android_app"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in android app", "result": "1"})
        if pc_mall:
            ##Check if checkbox is checked already, if not check the checkbox
            xpath = Util.GetXpath({"locate": "pc_mall"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Hide in pc", "result": "1"})

        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.FilterBannerDisplayByPlatform')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBannerQuota(arg):
        '''
        ChooseBannerQuota : Choose banner quota type for quota setting
                Input argu :
                    quota_type - banner_impression / banner_click
                    quota - how much quota do you want to set for banner
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        quota_type = arg["quota_type"]
        quota = arg["quota"]

        ##Click quota dropdown list
        xpath = Util.GetXpath({"locate": "quota_dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click quota dropdown list", "result": "1"})

        ##Click quota type
        time.sleep(5)
        xpath = Util.GetXpath({"locate": quota_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click quota type", "result": "1"})

        ##Input quota quantity
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "quota_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": quota, "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.ChooseBannerQuota')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBannerUserList(arg):
        '''
        ChangeBannerUserList : Change banner User List Mode
                Input argu :
                    action - file / image
                    user_list_mode - user list mode (whitelist / blacklist)
                    user_list_action - add / remove
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        user_list_mode = arg["user_list_mode"]
        user_list_action = arg["user_list_action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click scope to user list
        xpath = Util.GetXpath({"locate": "user_list_scope"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to user list scope", "result": "1"})
        time.sleep(3)

        for retry_time in range(3):
            ##Click choose user list mode dropdown
            xpath = Util.GetXpath({"locate": "user_list_mode_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user list mode dropdown", "result": "1"})

            ##Click choose user list mode
            xpath = Util.GetXpath({"locate": user_list_mode})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user list mode -> " + user_list_mode, "result": "1"})
                break
        else:
            ret = 0
            dumplogger.info("Failed to click dropdown list")

        ##Input csv
        time.sleep(3)
        xpath = Util.GetXpath({"locate": user_list_action})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.ChangeBannerUserList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditBannerImg(arg):
        '''
        EditBannerImg : Edit Banner Img
                Input argu :
                    action - file / action
                    banner_type - (format: home_popup, landing_page, category, official_shop, skinny, floating, mall_banner, mall_shop_banner)
                    file_name - image file name
                    file_type - image file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        banner_type = arg["banner_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        second_file_name = arg["second_file_name"]
        second_file_type = arg["second_file_type"]

        if banner_type in ("home_popup", "mall_carousel", "floating", "category", "mall_popup", "digital_product"):
            ##Click to upload banner
            AdminActivityBannerPage.UploadBannerImg({"xpath": banner_type, "project": Config._TestCaseFeature_, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        elif banner_type in ("home_carousel", "skinny", "mall_banner"):
            ##Upload mobile banner
            AdminActivityBannerPage.UploadBannerImg({"xpath": "mobile_" + banner_type, "project": Config._TestCaseFeature_, "action": action, "file_name": "mobile_" + file_name, "file_type": file_type, "result": "1"})

            ##Upload pc banner
            AdminActivityBannerPage.UploadBannerImg({"xpath": "pc_" + banner_type, "project": Config._TestCaseFeature_, "action": action, "file_name": "pc_" + second_file_name, "file_type": second_file_type, "result": "1"})

        time.sleep(20)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.EditBannerImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateCampaignUnit(arg):
        '''
        CreateCampaignUnit : Create campaign unit
                Input argu :
                    visibility_type : visible / invisible
                    name - name of the campaign unit
                    adjust_start_time - start time of the banner (format : 1 for 1 minutes after current time stamp)
                    adjust_end_time - end time of the banner (format : -1 for 1 minutes before current time stamp)
                    description - description of campaign unit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        visibility_type = arg["visibility_type"]
        name = arg["name"]
        description = arg["description"]

        ##Choose if Visible
        if visibility_type:
            xpath = Util.GetXpath({"locate": visibility_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose visibility", "result": "1"})

        ##Input Start Time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_start_time), 0)
            time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify start time
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
        else:
            dumplogger.info("Do not setting start time.")

        ##Input End Time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_end_time), 0)
            time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_end_time), 0)

            ##Modify end date
            xpath = Util.GetXpath({"locate": "end_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_date_input"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

            ##Modify end time
            xpath = Util.GetXpath({"locate": "end_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)
        else:
            dumplogger.info("Do not setting end time.")

        ##Input campagin unit name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        ##Input description
        xpath = Util.GetXpath({"locate": "description"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.CreateCampaignUnit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterBannerSpaceGroup(arg):
        '''
        FilterBannerSpaceGroup : Edit banner platform show
                Input argu :
                    category_banner - category banner hide or not (checked)
                    mall_carousel - mall carousel banner hide or not (checked)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mall_carousel = arg["mall_carousel"]
        category_banner = arg["category_banner"]

        ##Choose show platform
        if category_banner:
            ##Click category banner space group
            xpath = Util.GetXpath({"locate": "category_banner"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "show in category", "result": "1"})

            ##Click category dropdown
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "category_dropdown_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category dropdown list", "result": "1"})

            ##Input category
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "category_select"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "category_banner", "result": "1"})
            time.sleep(3)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        if mall_carousel:
            ##Click mall carousel banner space group
            xpath = Util.GetXpath({"locate": "mall_carousel"})
            BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "show in mall carousel", "result": "1"})

            ##Click mall carousel dropdown
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "mall_dropdown_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category dropdown list", "result": "1"})

            ##Select mall
            time.sleep(5)
            xpath = Util.GetXpath({"locate": "mall_popular"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Popular", "result": "1"})
            time.sleep(3)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.FilterBannerSpaceGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBannerOrder(arg):
        '''
        ChangeBannerOrder : Change banner order to by input order number
                Input argu:
                    description - banner description
                    order - order after adjustment
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]
        order = arg["order"]

        ##Change order number
        xpath = Util.GetXpath({"locate": "order_input"})
        xpath = xpath.replace("string_to_be_replaced", description)
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(3)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": order, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.ChangeBannerOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCampaignPriority(arg):
        '''
        ChangeCampaignPriority : Change campaign order to by input priority number
                Input argu:
                    description - banner description
                    order - order after adjustment
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]
        order = arg["order"]

        ##Change order number
        xpath = Util.GetXpath({"locate": "order_input"})
        xpath = xpath.replace("string_to_be_replaced", description)
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(2)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": order, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.ChangeCampaignPriority')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCampaignUnitTeam(arg):
        '''
        ChangeCampaignUnitTeam : Change campaign unit team
                Input argu:
                    team - which team you want to show
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        team = arg["team"]

        ##Click drop down list
        BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})
        time.sleep(8)
        xpath = Util.GetXpath({"locate": "team_dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click team dropdown list", "result": "1"})
        time.sleep(3)

        ##Choose team
        xpath = Util.GetXpath({"locate": "team_content"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": team, "result": "1"})
        time.sleep(3)

        ##Click content
        xpath = Util.GetXpath({"locate": "team"})
        xpath = xpath.replace("string_to_be_replaced", team)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click team dropdown content", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUnitPage.ChangeCampaignUnitTeam')


class AdminBannerSpacePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchUploadBannerSpacesInfo(arg):
        '''
        BatchUploadBannerSpacesInfo : Batch upload banner spaces info
                Input argu :
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Input file
        xpath = Util.GetXpath({"locate": "file_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action":action, "file_name":file_name, "file_type":file_type, "result":"1"})

        OK(ret, int(arg['result']), 'AdminBannerSpacePage.BatchUploadBannerSpacesInfo')


class AdminBannerSpaceButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUploadBannerSpacesInfo(arg):
        '''
        ClickBatchUploadBannerSpacesInfo : Click batch upload banner spaces info button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch upload banner spaces info button
        xpath = Util.GetXpath({"locate": "batch_upload_banner_space_info_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch upload banner spaces info button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerSpaceButton.ClickBatchUploadBannerSpacesInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderEdit(arg):
        '''
        ClickOrderEdit : Click order edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click order edit button
        xpath = Util.GetXpath({"locate": "order_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order edit button","result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerSpaceButton.ClickOrderEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAutoAdjust(arg):
        '''
        ClickAutoAdjust : Click auto adjust button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click auto adjust button
        xpath = Util.GetXpath({"locate": "auto_adjust_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click auto adjust button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerSpaceButton.ClickAutoAdjust')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPriorityEdit(arg):
        '''
        ClickPriorityEdit : Click priority edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click priority edit button
        xpath = Util.GetXpath({"locate": "priority_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click priority edit button","result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerSpaceButton.ClickPriorityEdit')

class AdminCampaignUnitButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditBanner(arg):
        '''
        ClickEditBanner : Use banner name to edit specific Banner in all banner page
            Input argu :
                banner_name - banner_name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        banner_name = arg["banner_name"]

        ##Get and reserve banner order
        order_xpath = Util.GetXpath({"locate": "edit_icon_order"})
        order_xpath = order_xpath.replace("name_to_be_replaced", banner_name)
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": order_xpath, "reservetype": "banner_order_id", "result": "1"})

        ##Click to edit banner
        xpath = Util.GetXpath({"locate": "edit_banner_icon"})
        xpath = xpath.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit banner", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickEditBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewBanner(arg):
        '''
        ClickAddNewBanner : Click add new banner button in all banner page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new banner button
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new banner button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickAddNewBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Click save button
        for retry_times in range(5):
            xpath = Util.GetXpath({"locate": "save_btn"})
            time.sleep(10)
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
                dumplogger.info("Click campaign unit banner page save btn, at %d times" % (retry_times))
            else:
                dumplogger.info("Success to click campaign unit banner page save btn!!!")
                ret = 1
                break

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCampaignUnit(arg):
        '''
        ClickAddCampaignUnit : Click add new campaign unit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new campaign unit btn
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new campaign unit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickAddCampaignUnit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveAndAddNewBanner(arg):
        '''
        ClickSaveAndAddNewBanner : Click save campaign and add new banner button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save campaign and add new banner button
        xpath = Util.GetXpath({"locate": "add_new_banner"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save campaign and add new banner button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickSaveAndAddNewBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok btn after save campaign unit
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn after save campaign unit
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicateBanner(arg):
        '''
        ClickDuplicateBanner : Click to duplicate Banner
                Input argu :
                    banner_name - banner_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_name = arg["banner_name"]

        ##Get and reserve banner order
        order_xpath = Util.GetXpath({"locate": "edit_icon_order"})
        order_xpath = order_xpath.replace("name_to_be_replaced", banner_name)
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": order_xpath, "reservetype": "banner_order_id", "result": "1"})

        ##Move to more icon and click duplicate btn
        xpath_drop_down = Util.GetXpath({"locate": "more_banner_icon"})
        xpath_drop_down = xpath_drop_down.replace("order_to_be_replaced", GlobalAdapter.BannerE2EVar._SequenceID_)
        xpath_duplicate = Util.GetXpath({"locate": "duplicate_banner_icon"})
        BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath_drop_down, "locatehidden": xpath_duplicate, "result": "1"})

        ##Click continue btn
        xpath = Util.GetXpath({"locate": "continue_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click continue banner", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickDuplicateBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveOrder(arg):
        '''
        ClickSaveOrder : Click save banner order button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save order button
        time.sleep(5)
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save order button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignUnitButton.ClickSaveOrder')


class AdminQuotaValidationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseQuotaType(arg):
        '''
        ChooseQuotaType : Choose quota type
            Input argu :
                quota_type - quota type which you want to select
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        quota_type = arg["quota_type"]

        ##Choose quota type
        xpath = Util.GetXpath({"locate": quota_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click quota type checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminQuotaValidationPage.ChooseQuotaType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBannerQuotaField(arg):
        '''
        InputBannerQuotaField : Input banner quota data in banner quota field
            Input argu :
                input_field - which banner quota field you want to input ex: banner_id_field / user_id_field / devices_id_field
                input_data - what data you want to input
                banner_quota_name - banner quota name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        input_field = arg["input_field"]
        input_data = arg["input_data"]
        banner_quota_name = arg["banner_quota_name"]

        ##Input data in banner quota field
        xpath = Util.GetXpath({"locate": input_field})
        xpath = xpath.replace("name_to_be_replaced", banner_quota_name)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_data, "result": "1"})

        OK(ret, int(arg['result']), 'AdminQuotaValidationPage.InputBannerQuotaField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignUnitQuotaField(arg):
        '''
        InputCampaignUnitQuotaField : Input campaign unit quota data in campaign unit quota field
            Input argu :
                input_field - which campaign unit quota field you want to input ex: campaign_unit_id_field / user_id_field / devices_id_field
                input_data - what data you want to input
                campaign_unit_quota_name - campaign unit quota name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        input_field = arg["input_field"]
        input_data = arg["input_data"]
        campaign_unit_quota_name = arg["campaign_unit_quota_name"]

        ##Input data in campaign unit quota field
        xpath = Util.GetXpath({"locate": input_field})
        xpath = xpath.replace("name_to_be_replaced", campaign_unit_quota_name)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_data, "result": "1"})

        OK(ret, int(arg['result']), 'AdminQuotaValidationPage.InputCampaignUnitQuotaField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseSpaceGroup(arg):
        '''
        ChooseSpaceGroup : Choose space group
            Input argu :
                space_group - which space group you want to choose
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        space_group = arg["space_group"]

        ##Click space group drop down list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click space group dropdown list", "result": "1"})
        time.sleep(5)

        ##Click dropdown content
        xpath = Util.GetXpath({"locate": space_group})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click space group dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminQuotaValidationPage.ChooseSpaceGroup')


class AdminQuotaValidationButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheck(arg):
        '''
        ClickCheck : Click check button
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click check button
        xpath = Util.GetXpath({"locate": "check_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminQuotaValidationButton.ClickCheck')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset button
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click reset button
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminQuotaValidationButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditRequest(arg):
        '''
        ClickEditRequest : Click edit request button
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click edit request button
        xpath = Util.GetXpath({"locate": "edit_request_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit request button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminQuotaValidationButton.ClickEditRequest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button in quota type section
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminQuotaValidationButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicate(arg):
        '''
        ClickDuplicate : Click duplicate button
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click duplicate button
        xpath = Util.GetXpath({"locate": "duplicate_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click duplicate button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminQuotaValidationButton.ClickDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove button
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminQuotaValidationButton.ClickRemove')
