import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCOrder Precondition =="
        dumplogger.info("== Setup SGPCOrder Precondition ==")

    # def test_SGPCOrder01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-01.xml")

    # def test_SGPCOrder02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-02.xml")

    # def test_SGPCOrder03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-03.xml")

    # def test_SGPCOrder04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-04.xml")

    # def test_SGPCOrder05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-05.xml")

    # def test_SGPCOrder06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-06.xml")

    # def test_SGPCOrder07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-07.xml")

    # def test_SGPCOrder08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-08.xml")

    # def test_SGPCOrder09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-09.xml")

    # def test_SGPCOrder10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-10.xml")

    # def test_SGPCOrder11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-11.xml")

    # def test_SGPCOrder12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/SG/SGPCOrder-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCOrder Post condition =="
        dumplogger.info("== Setup SGPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
