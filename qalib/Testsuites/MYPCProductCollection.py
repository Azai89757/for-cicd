import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCProductCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCProductCollection Precondition =="
        dumplogger.info("== Setup MYPCProductCollection Precondition ==")

    def test_MYPCProductCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-01.xml")

    def test_MYPCProductCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-02.xml")

    def test_MYPCProductCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-03.xml")

    def test_MYPCProductCollection04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-04.xml")

    def test_MYPCProductCollection05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-05.xml")

    def test_MYPCProductCollection06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-06.xml")

    def test_MYPCProductCollection07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCProductCollection-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCProductCollection Post condition =="
        dumplogger.info("== Setup MYPCProductCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
