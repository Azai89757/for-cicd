﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 BaseUIControl.py: The def of this file called by XML mainly.
'''

##Import python library
import os
import re
import time
import base64
import traceback

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger
from Config import _GeneralWaitingPeriod_, _AverageWaitingPeriod_, _ShortWaitingPeriod_, _InstantWaitingPeriod_

##Import Web library
import BaseUICore

##Import selenium related module
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoAlertPresentException, TimeoutException, NoSuchElementException, UnexpectedAlertPresentException


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def SwitchBrowserWindow(arg):
    '''
    SwitchBrowserWindow : For popup message or poup browser
            Input argu :
                isclose : 0 (won't close the old window (default)) / 1 (close the old window)
                switchback : 0 (switch to new window (default)) / 1 (switch back to old window)
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    isclose = arg["isclose"]
    switchback = arg["switchback"]
    window_before = ''
    window_after = ''

    ##Get first handle
    if int(switchback):
        try:
            window_before = BaseUICore._WebDriver_.window_handles[1]
        except:
            dumplogger.debug("Can't get the second tier window...")
    else:
        window_before = BaseUICore._WebDriver_.window_handles[0]
    dumplogger.info("Ori window handle = " + window_before)

    ##Get new handle
    if int(switchback):
        window_after = BaseUICore._WebDriver_.window_handles[0]
    else:
        window_after = BaseUICore._WebDriver_.window_handles[1]
    dumplogger.info("New windows handle = " + window_after)

    ##Switch to first handle
    BaseUICore._WebDriver_.switch_to_window(window_before)
    time.sleep(5)
    ##Close first handle
    if int(isclose):
        BaseUICore._WebDriver_.close()
        time.sleep(5)
    ##Switch to new handle
    BaseUICore._WebDriver_.switch_to_window(window_after)
    time.sleep(5)

    OK(ret, int(arg['result']), 'SwitchBrowserWindow')

@DecoratorHelper.FuncRecorder
def BrowserRefresh(arg):
    '''
    BrowserRefresh : Refresh browser
            Input argu : N/A
            Return code :
                1 - success
                -1 - error
    '''
    ret = 1

    BaseUICore._WebDriver_.refresh()

    OK(ret, int(arg['result']), 'BrowserRefresh')

@DecoratorHelper.FuncRecorder
def BrowserPageNavigate(arg):
    '''
    BrowserPageNavigate : Go to previous page/ next page on browser
            Input argu :
                control - back or forward
            Return code :
                1 - success
                -1 - error
    '''
    ret = 1
    control = arg['control']

    if control == 'back':
        BaseUICore._WebDriver_.back()
        message = "Go to previous page"

    elif control == 'forward':
        BaseUICore._WebDriver_.forward()
        message = "Go to next page"

    else:
        message = "Wrong input arg..."
        dumplogger.info(message)
        ret = -1

    OK(ret, int(arg['result']), 'BrowserPageNavigate->' + message)

@DecoratorHelper.FuncRecorder
def GetAndCheckBrowserCurrentUrl(arg):
    '''
    GetAndCheckBrowserCurrentUrl : Get current browser url and check if it matches input value
            Input argu :
                url - expected url
            Return code :
                1 - match
                0 - unmatch
    '''
    ret = 0
    url = arg['url']

    ##Get current url first
    BaseUICore.GetBrowserCurrentUrl({"result": "1"})

    ##Compare and check url
    if url == GlobalAdapter.CommonVar._PageUrl_:
        ret = 1
        message = "Matched."
        dumplogger.info(message)

    else:
        message = "Not Matched."
        dumplogger.info(message)

    OK(ret, int(arg['result']), 'GetAndCheckBrowserCurrentUrl->' + message)

@DecoratorHelper.FuncRecorder
def PopupHandle(arg):
    '''
    PopupHandle : Handle web popup window
            Input argu :
                handle_action - popup window handle type, should be accept or dismiss
                clear_message : 1 (no clear) / 0 (clear)
            Return code :
                1 - no popup
                0 - popup
    '''
    ret = 1
    handle_action = arg['handle_action']
    message = ""

    try:
        clear_message = arg["clear_message"]
    except KeyError:
        clear_message = "1"
        dumplogger.exception('KeyError - clear_message default is 1')
    except:
        clear_message = "1"
        dumplogger.exception('Other error - clear_message default is 1')
    finally:
        dumplogger.info("clear_message = %s" % (clear_message))

    if clear_message == '1':
        ##Clean alert text
        GlobalAdapter.GeneralE2EVar._AlertTextList_ = []

    try:
        ##Switch to alert window
        WebDriverWait(BaseUICore._WebDriver_, _GeneralWaitingPeriod_).until(EC.alert_is_present(),
            'Timed out waiting for PA creation ' + 'confirmation popup to appear.')

        alert = BaseUICore._WebDriver_.switch_to_alert()
        GlobalAdapter.GeneralE2EVar._AlertTextList_.append(alert.text.replace("\n", ""))

        ##Get alert text
        dumplogger.info("Popup message = " + alert.text)
        ret = 0

        ##Handle alert window
        if handle_action == 'dismiss':
            ##Dismiss alert window
            message = "Dismiss alert window"
            dumplogger.info(message)
            alert.dismiss()
        else:
            ##Accept alert window
            message = "Accept alert window"
            dumplogger.info(message)
            alert.accept()

    except NoAlertPresentException:
        message = "No popup exist."
        dumplogger.info(message)
        ret = 1

    OK(ret, int(arg['result']), 'PopupHandle' + message)

def DetectPopupWindow():
    '''
    DetectPopupWindow : Detect whether popup window shows up
            Input argu : N/A
            Return code :
                is_exist - True (popup exists) / False (no popup)
    '''

    is_exist = True

    try:
        ##Check if there's popup alert window
        WebDriverWait(BaseUICore._WebDriver_, _ShortWaitingPeriod_).until(EC.alert_is_present(),
            'Timed out waiting for PA creation ' + 'confirmation popup to appear.')

    except NoAlertPresentException:
        ##No popup window
        dumplogger.info("No popup")
        is_exist = False

    except:
        dumplogger.info("No popup")
        is_exist = False

    return is_exist

@DecoratorHelper.FuncRecorder
def HandleUnknownPopUpWindow(arg):
    '''
    HandleUnknownPopUpWindow : Handle popup window in expected times
            Input argu :
                expected_time : How many times want to handle popup window
            Return code :
                1 - Handle out off all popup window
                -1 - error
    '''
    ret = 1
    expected_time = int(arg['expected_time'])

    ##Handle popup window
    for retry_times in range(expected_time):
        if DetectPopupWindow():
            PopupHandle({"handle_action":"accept", "clear_message":"0", "result": "0"})
            time.sleep(5)
            dumplogger.info("Detect popup window and handle it %d times" % (retry_times+1))

        else:
            dumplogger.info("No popup window anymore")
            break

    OK(ret, int(arg['result']), 'HandleUnknownPopUpWindow')

@DecoratorHelper.FuncRecorder
def CheckPopupWindowMessage(arg):
    '''
    CheckPopupWindowMessage : Check the message of popup window that whether if it is the same as expected message
            Input argu :
                expected_message : Expected message of popup window
            Return code :
                1 - Message of popup window is the same as expected message
                0 - Message of popup window is the different from expected message
    '''
    ret = 1
    expected_message = arg['expected_message']

    ##Log popup window message list
    dumplogger.info("GlobalAdapter.GeneralE2EVar._AlertTextList_ : %s" % (GlobalAdapter.GeneralE2EVar._AlertTextList_))

    ##Check popup window message
    if expected_message in GlobalAdapter.GeneralE2EVar._AlertTextList_:
        dumplogger.info("Last popup window message is equal to expected message")
        message = "Equal."

    else:
        message = "Not equal."
        dumplogger.info("Last popup window message is not equal to expected message")
        ret = 0

    OK(ret, int(arg['result']), 'CheckPopupWindowMessage->' + message)

@DecoratorHelper.FuncRecorder
def GetAndCheckElements(arg):
    '''
    GetAndCheckElements : Get the elements and check if it's identical
            Input argu :
                method - xpath, id, name, classname, css
                locate - path
                reserve_type - which one you would like to reserve
                isfuzzy - Fuzzy comparison or perfect comparison
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    method = arg["method"]
    string = arg["string"]
    locate = arg["locate"]
    isfuzzy = ""

    ##Error handle for variable
    try:
        isfuzzy = arg["isfuzzy"]
    except KeyError:
        isfuzzy = "0"
        dumplogger.exception('KeyError - fuzzy default is 0')
    except:
        isfuzzy = "0"
        dumplogger.exception('Other error - fuzzy default is 0')
    finally:
        dumplogger.info("isfuzzy = %s" % (isfuzzy))

    ##Get elements
    BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode": "single", "result": "1"})
    BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

    ##Check elements
    BaseUICore.ParseElements({"keyword":string, "isfuzzy":isfuzzy, "result": "1"})

    OK(ret, int(arg['result']), 'GetAndCheckElements')

@DecoratorHelper.FuncRecorder
def GetAndCheckElementsSize(arg):
    '''
    GetAndCheckElementsSize : Get the elements size and check if it's identical
            Input argu :
                height - which height you would like to reserve
                width - which width you would like to reserve
                locate - path
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    height = arg["height"]
    width = arg["width"]
    locate = arg["locate"]

    ##Get element size
    GetAndReserveElements({"method": "xpath", "locate": locate, "reservetype": "element_size", "result": "1"})

    ##Get element height and compare
    GlobalAdapter.CommonVar._PageAttributes_ = str(GlobalAdapter.GeneralE2EVar._ElementSizeDict_["height"])
    BaseUICore.ParseElements({"keyword": height, "isfuzzy": "0", "result": "1"})

    ##Get element width and compare
    GlobalAdapter.CommonVar._PageAttributes_ = str(GlobalAdapter.GeneralE2EVar._ElementSizeDict_["width"])
    BaseUICore.ParseElements({"keyword": width, "isfuzzy": "0", "result": "1"})

    OK(ret, int(arg['result']), 'GetAndCheckElementsSize')

@DecoratorHelper.FuncRecorder
def GetPageSourceAndCheck(arg):
    '''
    GetPageSourceAndCheck : Get page source and verify if string is in this page
            Input argu :
                string - string to be verified
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    ret = 0
    string = arg["string"]

    ##Get page source
    try:

        ##Get web page source
        html_source = BaseUICore._WebDriver_.page_source

        ##Check keyword string in web page source
        if string in html_source:
            ret = 1
        message = "Get string."

    except TimeoutException:
        message = "Cannot load page."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'GetPageSourceAndCheck->' + message)

@DecoratorHelper.FuncRecorder
def GetAndReserveElements(arg):
    '''
    GetAndReserveElements : Get the elements and reserve to global list
            Input argu :
                locate - xpath of element
                reserve_type - which one you would like to reserve
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    locate = arg["locate"]
    reserve_type = arg["reservetype"]
    message = "reserve type => %s" % (reserve_type)
    ret = 1

    try:
        ##Reserve elements
        if "ordersn" in reserve_type:
            ##Get elements and save to global
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            ##Check key is in order sn dict
            if reserve_type in GlobalAdapter.OrderE2EVar._OrderSNDict_:
                dumplogger.info("%s key already in ordersn dict" % (reserve_type))
            else:
                ##Assign key
                GlobalAdapter.OrderE2EVar._OrderSNDict_[reserve_type] = []
            ##Append order sn into list with key name xxx_ordersn
            GlobalAdapter.OrderE2EVar._OrderSNDict_[reserve_type].append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.OrderE2EVar._OrderSNDict_)

        ##Reserve elements
        elif reserve_type == "count":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"multi", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"count", "mode":"multi", "result": "1"})
            GlobalAdapter.GeneralE2EVar._Count_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.GeneralE2EVar._Count_)

        ##Reserve elements
        elif reserve_type == "flashsale_status":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"multi", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"multi", "result": "1"})
            GlobalAdapter.PromotionE2EVar._FlashSaleIDList_ = GlobalAdapter.CommonVar._PageAttributesList_
            dumplogger.info(GlobalAdapter.PromotionE2EVar._FlashSaleIDList_)

        ##Reserve elements
        elif reserve_type == "personalinfo":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"multi", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"get_attribute", "mode":"multi","attribute":"innerHTML", "result": "1"})
            GlobalAdapter.AccountE2EVar._PersonalInfoList_ = GlobalAdapter.CommonVar._PageAttributesList_
            dumplogger.info(GlobalAdapter.AccountE2EVar._PersonalInfoList_)

        ##Reserve elements
        elif reserve_type == "checkout_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._CheckOutID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._CheckOutID_)

        ##Reserve elements
        elif reserve_type == "refund_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._RefundID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._RefundID_)

        ##Reserve elements
        elif reserve_type == "return_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._ReturnID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._ReturnID_)

        ##Reserve elements
        elif reserve_type == "return_sn":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._ReturnSN_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._ReturnSN_)

        ##Reserve elements
        elif reserve_type == "order_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._OrderID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._OrderID_)

        ##Reserve elements
        elif reserve_type == "forder_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._FOrderID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._FOrderID_)

        ##Reserve elements
        elif reserve_type == "ofg_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._OFGID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._OFGID_)

        ##Reserve elements
        elif reserve_type == "promotion_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.PromotionE2EVar._PromotionIDList_)

        ##Reserve elements
        elif reserve_type == "shopee_voucher_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"code": "", "id": GlobalAdapter.CommonVar._PageAttributes_})
            dumplogger.info(GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_)

        elif reserve_type == "dragon_pay_refno":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._DragonPayRefNo_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._DragonPayRefNo_)

        elif reserve_type == "dragon_pay_total_due":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._PayableAmount_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._PayableAmount_)

        ##Reserve elements
        elif reserve_type == "free_shipping_voucher_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_.append({"code": "", "id": GlobalAdapter.CommonVar._PageAttributes_})
            dumplogger.info(GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_)

        ##Reserve elements
        elif reserve_type == "category_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.ProductE2EVar._CategoryID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.ProductE2EVar._CategoryID_)

        ##Reserve elements
        elif reserve_type == "product_card":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
            GlobalAdapter.GeneralE2EVar._LocationX_ = GlobalAdapter.CommonVar._PageAttributes_["x"]
            GlobalAdapter.GeneralE2EVar._LocationY_ = GlobalAdapter.CommonVar._PageAttributes_["y"]
            dumplogger.info(GlobalAdapter.GeneralE2EVar._LocationX_)
            dumplogger.info(GlobalAdapter.GeneralE2EVar._LocationY_)

        ##Reserve elements
        elif reserve_type == "statement_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._StatementID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._StatementID_)

        ##Reserve elements
        elif reserve_type == "coin_rule":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.CoinsE2EVar._RuleID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.CoinsE2EVar._RuleID_)

        ##Reserve elements
        elif reserve_type == "shopee_coin":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.CoinsE2EVar._CoinsAmount_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.CoinsE2EVar._CoinsAmount_)

        ##Reserve elements
        elif reserve_type == "logistic_price":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.OrderE2EVar._LogisticPrice_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.OrderE2EVar._LogisticPrice_)

        ##Reserve elements
        elif reserve_type == "transaction_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._TransactionID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._TransactionID_)

        ##Reserve elements
        elif reserve_type == "refund_transaction_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._TransactionID_ = GlobalAdapter.CommonVar._PageAttributes_.split("=")[1].split(":")[0].replace(" ", "")
            dumplogger.info(GlobalAdapter.PaymentE2EVar._TransactionID_)

        ##Reserve elements
        elif reserve_type == "statement_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._StatementID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._StatementID_)

        ##Reserve elements
        elif reserve_type == "service_fee_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.ServiceFeeE2EVar._RuleID_ = re.search(r"\d+",GlobalAdapter.CommonVar._PageAttributes_).group()
            dumplogger.info(GlobalAdapter.ServiceFeeE2EVar._RuleID_)

        ##Reserve elements
        elif reserve_type == "seller_txn_fee_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.SellerTxnFeeE2EVar._RuleID_ = re.search(r"Rule ID (\d+)", GlobalAdapter.CommonVar._PageAttributes_).group(1)
            dumplogger.info(GlobalAdapter.SellerTxnFeeE2EVar._RuleID_)

        ##Reserve elements
        elif reserve_type == "seller_txn_fee_group_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.SellerTxnFeeE2EVar._GroupID_ = re.search(r"Group ID (\d+)",GlobalAdapter.CommonVar._PageAttributes_).group(1)
            dumplogger.info(GlobalAdapter.SellerTxnFeeE2EVar._GroupID_)

        ##Reserve elements
        elif reserve_type == "commission_fee_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.CommissionFeeE2EVar._RuleID_ = re.search(r"Rule ID (\d+)",GlobalAdapter.CommonVar._PageAttributes_).group(1)
            dumplogger.info(GlobalAdapter.CommissionFeeE2EVar._RuleID_)

        ##Reserve elements
        elif reserve_type == "commission_fee_group_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.CommissionFeeE2EVar._GroupID_ = re.search(r"Group ID (\d+)",GlobalAdapter.CommonVar._PageAttributes_).group(1)

        ##Reserve elements
        elif reserve_type == "captcha":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "get_attribute", "attribute": "src", "mode": "single", "result": "1"})
            GlobalAdapter.AccountE2EVar._CaptchaList_.append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.AccountE2EVar._CaptchaList_)

        ##Reserve elements
        elif reserve_type == "shopee_voucher_code":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"multi", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"multi", "result": "1"})
            for shopee_voucher_code in GlobalAdapter.CommonVar._PageAttributesList_:
                GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"id": "", "code": shopee_voucher_code})
            dumplogger.info(GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_)

        elif reserve_type == "filter_panel":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
            GlobalAdapter.GeneralE2EVar._LocationX_ = GlobalAdapter.CommonVar._PageAttributes_["x"]
            GlobalAdapter.GeneralE2EVar._LocationY_ = GlobalAdapter.CommonVar._PageAttributes_["y"]
            dumplogger.info(GlobalAdapter.GeneralE2EVar._LocationX_)
            dumplogger.info(GlobalAdapter.GeneralE2EVar._LocationY_)

        ##Reserve elements
        elif reserve_type == "maintenance_starttime":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute": "value", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_)

        ##Reserve elements
        elif reserve_type == "maintenance_endtime":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute": "value", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_)

        ##Reserve elements
        elif reserve_type == "subscribe":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.FormE2EVar._FormID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.FormE2EVar._FormID_)

        ##Reserve elements
        elif reserve_type == "unsubscribe":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.FormE2EVar._UnsubFormID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.FormE2EVar._UnsubFormID_)

        ##Reserve elements
        elif reserve_type == "form_start_time":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.FormE2EVar._StartTime_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.FormE2EVar._StartTime_)

        ##Reserve elements
        elif reserve_type == "form_end_time":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.FormE2EVar._EndTime_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.FormE2EVar._EndTime_)

        ##Reserve elements
        elif reserve_type == "spm_payment_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._SPMPaymentID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._SPMPaymentID_)

        ##Reserve elements
        elif reserve_type == "address_inner_window":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
            GlobalAdapter.GeneralE2EVar._LocationX_ = GlobalAdapter.CommonVar._PageAttributes_["x"]
            GlobalAdapter.GeneralE2EVar._LocationY_ = GlobalAdapter.CommonVar._PageAttributes_["y"]
            dumplogger.info(GlobalAdapter.GeneralE2EVar._LocationX_)
            dumplogger.info(GlobalAdapter.GeneralE2EVar._LocationY_)

        ##Reserve elements
        elif reserve_type == "payment_code":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._IndomaretID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._IndomaretID_)

        ##Reserve elements
        elif reserve_type == "spm_channel_ref":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._SPMChannelRef_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._SPMChannelRef_)

        ##Reserve elements
        elif reserve_type == "bank_va_number":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._BankVANumber_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._BankVANumber_)

        ##Reserve elements
        elif reserve_type == "banner_order_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.BannerE2EVar._SequenceID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.BannerE2EVar._SequenceID_)

        ##Reserve elements
        elif reserve_type == "productcollection_old_name":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute":"value", "mode":"single", "result":"1"})
            GlobalAdapter.ProductCollectionE2EVar._OldCollectionName_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.ProductCollectionE2EVar._OldCollectionName_)

        ##Reserve elements
        elif reserve_type == "google_account_username":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_)

        ##Reserve elements
        elif reserve_type == "atm_ref1":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._ATMRef1_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._ATMRef1_)

        ##Reserve elements
        elif reserve_type == "element_size":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "size", "mode": "single", "result": "1"})
            GlobalAdapter.GeneralE2EVar._ElementSizeDict_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.GeneralE2EVar._ElementSizeDict_)

        ##Reserve elements
        elif reserve_type == "airpay_order_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._AirpayOrderID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._AirpayOrderID_)

        ##Reserve elements
        elif reserve_type == "transaction_fee_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._TransactionFeeRuleID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._TransactionFeeRuleID_)

        ##Reserve elements
        elif reserve_type == "transaction_fee_cs_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._TransactionFeeCSRuleID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._TransactionFeeCSRuleID_)

        elif reserve_type == "buyer_txn_fee_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PaymentE2EVar._BuyerTxnFeeRuleID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PaymentE2EVar._BuyerTxnFeeRuleID_)

        elif reserve_type == "audit_data_info":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result":"1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PromotionE2EVar._AuditDataInfo_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.PromotionE2EVar._AuditDataInfo_)

        ##Reserve elements
        elif reserve_type == "product_collection_old_update_time":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.ProductCollectionE2EVar._UpdateTime_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.ProductCollectionE2EVar._UpdateTime_)
        elif reserve_type == "bundledeal_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result":"1"})
            GlobalAdapter.PromotionE2EVar._BundleDealIDList_.append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.PromotionE2EVar._BundleDealIDList_)

        ##Reserve elements
        elif reserve_type == "records_count":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._RecordsCount_.append(re.findall(r"\d+", GlobalAdapter.CommonVar._PageAttributes_)[0])
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._RecordsCount_)

        ##Reserve elements
        elif reserve_type == "new_shop_collection_id":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._CollectionID_.append(re.findall(r"\d+", GlobalAdapter.CommonVar._PageAttributes_)[0])
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._CollectionID_)

        ##Reserve elements
        elif reserve_type == "new_shop_collection_name":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_.append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_)

        ##Reserve elements
        elif reserve_type == "edit_shop_collection_name":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "get_attribute", "attribute": "value", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_.append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_)

        ##Reserve elements
        elif reserve_type == "items_per_page":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._ItemsPerPage_ = re.findall(r"\d+", GlobalAdapter.CommonVar._PageAttributes_)[0]
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._ItemsPerPage_)

        ##Reserve elements
        elif reserve_type == "number_of_page":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._NumberOfPage_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._NumberOfPage_)

        ##Reserve elements
        elif reserve_type == "feature_id":
            ##Get elements
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.MePageE2EVar._FeatureID_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.MePageE2EVar._FeatureID_)

        ##Reserve elements
        elif reserve_type == "auto_generate_collection_rule_id":
            ##Get elements
            BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
            GlobalAdapter.ShopCollectionE2EVar._RuleID_ = re.findall(r"\d+", GlobalAdapter.CommonVar._PageAttributes_)[1]
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._RuleID_)

    except AttributeError:
        message = "Encounter attribute assign error. Please check reserve type."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown error."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'GetAndReserveElements->' + message)

@DecoratorHelper.FuncRecorder
def GetDiffInReserveElements(arg):
    '''
    GetDiffInReserveElements : Get/Compare all of elements in global list
            Input argu :
                compareType - type of object to be compared
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    length_list = 0
    compare_type = arg["compareType"]
    dumplogger.info("compare_type = %s" % (compare_type))

    def CompareEachElementsInList(length_list, list_elements):
        '''
        CompareEachElementsInList : Compare all of elements in global list
                Input argu :
                    length_list - lenght for list(int)
                    list_element - list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        dumplogger.info("Enter CompareEachElementsInList")
        dumplogger.info("length_list = %d" % (length_list))
        dumplogger.info(list_elements)
        ret = 1

        ##Detect whether elements is more than 1
        if length_list >= 2:

            ##Sequence to loag all elements
            for count in range(1, length_list):

                ##Compare the first elements with eachone
                if list_elements[0] == list_elements[count]:
                    #print "Equal -> %s = %s" %(list_elements[0], list_elements[count])
                    dumplogger.info("Equal -> %s == %s" % (list_elements[0], list_elements[count]))

                ##If not equal, will break loop directly
                else:
                    #print "Not equal : %s != %s" %(list_elements[0], list_elements[count])
                    dumplogger.info("Not equal -> %s != %s" % (list_elements[0], list_elements[count]))
                    ret = 0
                    OK(ret, int(arg['result']), 'GetDiffInReserveElements-> %s != %s' % (list_elements[0], list_elements[count]))
                    break

            if ret == 1:
                OK(ret, int(arg['result']), 'GetDiffInReserveElements->Equal')

        elif length_list == 1:
            #print "Only one value in list -> %s" %(list_elements[0])
            ret = 0
            OK(ret, int(arg['result']), 'GetDiffInReserveElements->Only one value in list')

        else:
            #print "No value in list"
            ret = 0
            OK(ret, int(arg['result']), 'GetDiffInReserveElements->No value in list')

    try:
        ##Compare all of ordersn in list
        if compare_type == "orderSN":
            for shop in GlobalAdapter.OrderE2EVar._OrderSNDict_:
                CompareEachElementsInList(len(GlobalAdapter.OrderE2EVar._OrderSNDict_[shop]), GlobalAdapter.OrderE2EVar._OrderSNDict_[shop])

        ##Compare all of captcha in list
        elif compare_type == "captcha":
            CompareEachElementsInList(len(GlobalAdapter.AccountE2EVar._CaptchaList_), GlobalAdapter.AccountE2EVar._CaptchaList_)

        ##Compare all of captcha in list
        elif compare_type == "shopee_voucher_code":
            CompareEachElementsInList(len(GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_), GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_)

        ##Compare all of records in list
        elif compare_type == "records_count":
            CompareEachElementsInList(len(GlobalAdapter.ShopCollectionE2EVar._RecordsCount_), GlobalAdapter.ShopCollectionE2EVar._RecordsCount_)

        ##Compare all of collection id in list
        elif compare_type == "new_shop_collection_id":
            CompareEachElementsInList(len(GlobalAdapter.ShopCollectionE2EVar._CollectionID_), GlobalAdapter.ShopCollectionE2EVar._CollectionID_)

        ##Compare all of collection name in list
        elif compare_type == "new_shop_collection_name":
            CompareEachElementsInList(len(GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_), GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_)

    except:
        dumplogger.exception('Other error - Please check your input data')
        ret = -1

        OK(ret, int(arg['result']), 'GetDiffInReserveElements->Error')

@DecoratorHelper.FuncRecorder
def FilterStringToGlobal(arg):
    '''
    FilterStringToGlobal : Filter specific string
            Input argu :
                type - ordersn
                obj - _OrderSN_ -> For GetAndReserveElements filter specific string
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    type = arg["type"]
    obj = arg["object"]
    message = "Type - %s" % (type)
    dumplogger.info(arg)

    try:
        if type == "ordersn":

            ####For GetAndReserveElements to et var from _OrderID_
            if obj == "_OrderSN_" and GlobalAdapter.OrderE2EVar._OrderSNDict_:
                dumplogger.info(GlobalAdapter.OrderE2EVar._OrderSNDict_)

                for shop in GlobalAdapter.OrderE2EVar._OrderSNDict_:
                    ##Pop the last var from list
                    temp_pop = GlobalAdapter.OrderE2EVar._OrderSNDict_[shop].pop()
                    dumplogger.info("temp_pop = %s" % (temp_pop))

                    ##Filter Order SN from string by regex. Order SN rule: 1. Start 6 date time numbers (YYMMDD)  2. End mixed english charactors (A-Z) and numbers (0-9). Example: 210603C5U85BMU
                    match = re.search(r"\d{6}\d*[A-Z][A-Z\d]*", temp_pop)
                    if match:
                        temp_new = match.group(0)
                        dumplogger.info("temp_new = %s" % (temp_new))
                        ##Append the new string to list
                        GlobalAdapter.OrderE2EVar._OrderSNDict_[shop].append(temp_new)
                        dumplogger.info(GlobalAdapter.OrderE2EVar._OrderSNDict_[shop])
                    else:
                        message = "Not match ordersn. Odersn => %s" % (match.group(0))
                        dumplogger.info(message)
                        ret = 0

        elif type == "table":
            dumplogger.info("table_content: {} {}".format(len(GlobalAdapter.CommonVar._PageElements_), GlobalAdapter.CommonVar._PageElements_))
            for table_content in GlobalAdapter.CommonVar._PageElements_:
                ##init every row empty
                row = []
                tds = table_content.find_elements_by_tag_name('td')
                if tds:
                    for td in tds:
                        #dumplogger.info("table_content: {}".format(td.text))
                        row.append(str(td.text))
                    dumplogger.info("current row: {}".format(row))
                else:
                    message = "No td column found."
                    dumplogger.info(message)
                    ret = 0

                ##Append current row to global table
                GlobalAdapter.ProductCollectionE2EVar._TemplateList_.append(row)

        elif type == "promotion_url":
            dumplogger.info("browser url: %s" % (GlobalAdapter.CommonVar._PageUrl_))

            ##Split page url to get promotion id
            promotion_id = GlobalAdapter.CommonVar._PageUrl_.split("/")[-3]
            dumplogger.info("filtered promotion id: %s" % (promotion_id))

            ##Append to promotion id
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(promotion_id)
            dumplogger.info("saved promotion id: %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))

        elif type == "dragon_pay":
            if obj == "_PayableAmount_" and GlobalAdapter.PaymentE2EVar._PayableAmount_:
                match_pattern = r"PHP\s(\d+)"
                match = re.match(match_pattern, GlobalAdapter.PaymentE2EVar._PayableAmount_)
                if match:
                    GlobalAdapter.PaymentE2EVar._PayableAmount_ = match.group(1)
                else:
                    message = "No payable amount found."
                    dumplogger.info("No payable amount found, string: %s, pattern: %s" % (GlobalAdapter.PaymentE2EVar._PayableAmount_,match_pattern))
                    ret = 0
            dumplogger.info(GlobalAdapter.PaymentE2EVar._PayableAmount_)

    except:
        message = "Encounter error."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'FilterStringToGlobal->' + message)


@DecoratorHelper.FuncRecorder
def DetectPopupBanner(arg):
    '''
    DetectPopupBanner : Detect popup banner and refresh browser at home page.
          Input argu :
                locate - path
            Return code :
                1 - success
                0 - fail
    '''
    ret = 1
    locate = arg["locate"]

    time.sleep(10)

    try:
        ##To check popup img on the current windows and refresh browser until not exists
        element = WebDriverWait(BaseUICore._WebDriver_, _ShortWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate))))
        while(element.is_displayed()):
            dumplogger.info("There exists popup img on the window.")
            BrowserRefresh({"message":"Refresh browser.", "result": "1"})
            element = WebDriverWait(BaseUICore._WebDriver_, _ShortWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate))))
        BaseUICore.PageHasLoaded({"result": "1"})

    except:
        dumplogger.info("There is no popup img on the window, great!")
        BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'DetectPopupBanner')


@DecoratorHelper.FuncRecorder
def KeyboardAction(arg):
    '''
    KeyboardAction : Determine kinds of keyboard action using on browser.
        Input argu :
            locate - path
            actiontype - copy / cut / paste / delete / enter
        Return code :
            1 - success
            0 - fail
            -1 - error
    '''
    locate = arg["locate"]
    actiontype = arg["actiontype"]
    ret = 1

    if actiontype == 'copy':
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "select all", "result": "1"})
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "copy", "result": "1"})

    elif actiontype == 'cut':
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "select all", "result": "1"})
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "cut", "result": "1"})

    elif actiontype == 'paste':
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "select all", "result": "1"})
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "paste", "result": "1"})

    elif actiontype == 'delete':
        ##Use different action by different OS
        if Config._platform_ == "windows":
            ##Use Select all + Delete on windows
            BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "select all", "result": "1"})
            BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "delete", "result": "1"})
        elif Config._platform_ == "darwin":
            ##Get input area value length and back all text
            BaseUICore.GetElements({"method":"xpath", "locate":locate, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute":"value", "mode":"single", "result": "1"})
            for character_length in range(len(GlobalAdapter.CommonVar._PageAttributes_)):
                BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "back", "result": "1"})

    elif actiontype == 'enter':
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "enter", "result": "1"})

    ##Press keyboard arrow down
    elif actiontype == 'down':
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "down", "result": "1"})

    elif actiontype == 'tab':
        BaseUICore.SendKeyboardEvent({"locate":locate, "sendtype": "tab", "result": "1"})

    else:
        dumplogger.debug("Please choose actiontype correctly!")

    OK(ret, int(arg['result']), 'KeyboardAction->' + actiontype)

@DecoratorHelper.FuncRecorder
def GetAndCheckElementAttribute(arg):
    '''
    GetAndCheckElementAttribute : Get and check element attribite get from browser html
            Input argu :
                method - xpath, id, name, classname, css
                locate - path
                string - string to compare
                isfuzzy - Fuzzy comparison or perfect comparison
            Return code :
                1 - success
                0 - fail
                -1 - errror
    '''
    ret = 1
    method = arg["method"]
    string = arg["string"]
    locate = arg["locate"]
    attribute = arg["attribute"]
    isfuzzy = ""

    ##Error handle for variable
    try:
        isfuzzy = arg["isfuzzy"]
    except KeyError:
        isfuzzy = "0"
        dumplogger.exception('KeyError - fuzzy default is 0')
    except:
        isfuzzy = "0"
        dumplogger.exception('Other error - fuzzy default is 0')
    finally:
        dumplogger.info("isfuzzy = %s" % (isfuzzy))

    ##Get elements
    BaseUICore.GetElements({"method":method, "locate":locate, "mode": "single", "result": "1"})
    BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute": attribute, "mode":"single", "result": "1"})

    ##Check elements
    BaseUICore.ParseElements({"keyword":string, "isfuzzy":isfuzzy, "result": "1"})

    OK(ret, int(arg['result']), 'GetAndCheckElementAttribute')

@DecoratorHelper.FuncRecorder
def UploadFileWithPath(arg):
    '''
    UploadFileWithPath : Upload file with specific path
            Input argu:
                method - xpath / input
                project - your project name
                action - image / file
                file_type - file type
                element - xpath elemenu
                element_type - xpath element type
                locate - input btn xpath for method is "xpath"
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    method = arg["method"]
    project = arg["project"]
    action = arg["action"]
    file_name = arg["file_name"]
    file_type = arg["file_type"]
    element = arg["element"]
    element_type = arg["element_type"]

    ##Different method using different kind of xpath
    xpath = ""
    if method == "input":
        xpath = "//input[@" + element_type + "='" + element + "']"
    elif method == "xpath":
        locate = arg["locate"]
        xpath = locate

    ##Define slash and input file path
    slash = Config.dict_systemslash[Config._platform_]
    file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + project + slash + action + slash + file_name + "." + file_type

    ##Different element type using different execute script
    if element_type == "id":
        BaseUICore.ExecuteScript({"script":"document.getElementById('" + element + "').removeAttribute('hidden')", "result": "1"})
    elif element_type == "name":
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('" + element + "')[0].removeAttribute('hidden')", "result": "1"})
        BaseUICore.ExecuteScript({"script":"document.getElementsByName('" + element + "')[0].removeAttribute('disabled')", "result": "1"})
    elif element_type == "class":
        BaseUICore.ExecuteScript({"script":"document.getElementsByClassName('" + element + "')[0].setAttribute('style', 'display:block')", "result": "1"})
    elif element_type == "xpath":
        BaseUICore.ExecuteScript({"script":"document.evaluate(\"" + xpath + "\", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0).setAttribute('style', 'display:block')", "result": "1"})
    elif element_type == "type":
        BaseUICore.ExecuteScript({"script":"document.querySelectorAll('input[type=" + element + "]')[0].setAttribute('style', 'display:block')", "result": "1"})

    ##Input file to input bar
    try:
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
        message = "Upload file name %s" % (file_name)
    except UnexpectedAlertPresentException:
        message = "Encounter popup alert exception. -> %s" % (file_name)
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'UploadFileWithPath->' + message)

@DecoratorHelper.FuncRecorder
def CheckElementWithRetry(arg):
    '''
    CheckElementWithRetry :
            Input argu :
                locate - path
                total_retry_number - total number of retry times
                expected_result - 0 (not exist) / 1 (is exist)
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    locate = arg["locate"]
    total_retry_number = arg["total_retry_number"]
    expected_result = int(arg['result'])
    message = ""

    ##Retry to check if element exists
    for retry_count in range(1, int(total_retry_number)):
        ##If is exist will return 1, is not exist return 0
        ##If element status match the expected result
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"}) == expected_result:
            dumplogger.info("Element %s !! Match the expected result!!" % (locate))
            ret = expected_result
            break
        ##Rest of the situation should not break this loop
        else:
            message = "After retry %d times still cannot locate element => " % (retry_count) + locate
            dumplogger.info(message)
            time.sleep(3)
            ret = 0

    OK(ret, int(arg['result']), 'CheckElementWithRetry->' + message)

def GetDownloadedDataFromRemoteDriver(case_id):
    '''
    GetDownloadedDataFromRemoteDriver : Get downloaded data and write a file to local for remote chrome driver (Selenium Grid)
            Input argu :
                case_id - case id for file name
            Return code :
                file name - file name to be returned
                0 - fail
    '''
    time.sleep(10)
    latest_download_file_list = []

    ##Go to chrome://downloads/
    if not BaseUICore._WebDriver_.current_url.startswith("chrome://downloads"):
        BaseUICore._WebDriver_.get("chrome://downloads/")

    ##Execute script to get latest downloaded file from html source
    file_list = BaseUICore._WebDriver_.execute_script(
        "return  document.querySelector('downloads-manager')  "
        " .shadowRoot.querySelector('#downloadsList')         "
        " .items.filter(e => e.state === 'COMPLETE')          "
        " .map(e => e.filePath || e.file_path || e.fileUrl || e.file_url); ")

    ##If file list have content
    if file_list:
        ##Inject input column on html source
        input_element = BaseUICore._WebDriver_.execute_script(
            "var input = window.document.createElement('INPUT'); "
            "input.setAttribute('type', 'file'); "
            "input.hidden = true; "
            "input.onchange = function (e) { e.stopPropagation() }; "
            "return window.document.documentElement.appendChild(input); ")

        ##Send value and text to the path
        latest_download_file_list.append(file_list[0])
        dumplogger.info(latest_download_file_list)
        input_element._execute('sendKeysToElement', {'value':latest_download_file_list, 'text':latest_download_file_list})

        ##Add file reader function
        get_file_content = BaseUICore._WebDriver_.execute_async_script(
            "var input = arguments[0], callback = arguments[1]; "
            "var reader = new FileReader(); "
            "reader.onload = function (ev) { callback(reader.result) }; "
            "reader.onerror = function (ex) { callback(ex.message) }; "
            "reader.readAsDataURL(input.files[0]); "
            "input.remove(); ", input_element)

        ##Open a new file in local dir, then write the content into new file
        if get_file_content.startswith('data:'):
            ##Transform file content with base64
            file_content = base64.b64decode(get_file_content[get_file_content.find('base64,') + 7:])
            file_name = case_id + ".csv"
            with open(file_name, 'wb') as local_file:
                local_file.write(file_content)
        ##Error handling
        else:
            dumplogger.info("Failed to get file content: %s" % (get_file_content))
            return 0
    else:
        dumplogger.info("No file be downloaded.")
        return 0

    ##Back to previous page
    BrowserPageNavigate({"control":"back", "result": "1"})

    return file_name

@DecoratorHelper.FuncRecorder
def CloseBlockThirdPartyCookies(arg):
    '''
    CloseBlockThirdPartyCookies : Close block third party cookies for chrome driver in incognito mode
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - errror
    '''
    ret = 1

    ##Go to new tab
    BaseUICore.GotoURL({"url": "chrome://newtab", "result": "1"})

    ##Close block third party cookies
    xpath = Util.GetXpath({"locate": "cookie_toggle"})
    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Close block cookies tab", "result": "1"})

    OK(ret, int(arg['result']), 'CloseBlockThirdPartyCookies')

@DecoratorHelper.FuncRecorder
def MouseScrollEvent(arg):
    '''
    MouseScrollEvent : via javascript to execute script
            Input argu :
                xCoordinate - scroll to x coordinate
                yCoordinate - scroll to y coordinate
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    xCoordinate = arg["x"]
    yCoordinate = arg["y"]
    ret = 1

    ##Combine x, y with js cmd
    js = "window.scrollTo(" + xCoordinate + "," + yCoordinate + ");"

    ##Execute js cmd
    BaseUICore.ExecuteScript({"script":js, "result":"1"})

    OK(ret, int(arg['result']), 'MouseScrollEvent, scroll to x: ' + xCoordinate + ',y: ' + yCoordinate)
