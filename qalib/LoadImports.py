#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 LoadImports.py: LoadImports function
'''

##Import common library
import os
import importlib

##import framework library
import Config
from Config import dumplogger

##Append import function of __init__.py
def AppendImportFunc():
    ''' AppendImportFunc : Append import function of __init__.py of subfoders in framework
                Input argu :
                    N/A
                Return code :
                    N/A
    '''
    ##Get all the folders under qalib
    for root, dirs, files in os.walk(os.path.join(Config._CurDIR_, 'qalib')):
        for sub_dir in dirs:
            dir_path = os.path.join(root, sub_dir)
            files = os.listdir(dir_path)

            ## Append all the __init__.py inside each folder one by one
            imps = []
            for file_count in xrange(len(files)):
                name = files[file_count].split('.')
                if len(name) > 1:
                    if name[1] == 'py' and name[0] != '__init__':
                        name = name[0]
                        imps.append(name)

            ##Open __init__.py file and append all the func
            file = open(os.path.join(dir_path, '__init__.py'), 'w')
            toWrite = '__all__ = ' + str(imps)
            file.write(toWrite)
            file.close()

def DynamicImportModule(module):
    ''' DynamicImportModule : Dynamic import module for testing purpose
                Input argu :
                    module - package location of the module with module name
                    (Ex. qalib.api.spex.proto.TestA is the module structure of qalib/api/spex/proto/TestA)
                Return code :
                    import_module - module successfully imported and return
                    -1 - error
    '''
    ##Unmark this when you need to debug import problem
    #dumplogger.info("Enter DynamicImportModule")
    #dumplogger.info("module = %s" % (module))

    ##Import module by module name
    try:
        import_module = importlib.import_module(module)
        return import_module
    except ImportError:
        dumplogger.exception("Encounter Import Error!!!")
        return -1
    except:
        dumplogger.exception("Encounter Other Error!!!")
        return -1
