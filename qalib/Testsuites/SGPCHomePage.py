import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCHomePage Precondition =="
        dumplogger.info("== Setup SGPCHomePage Precondition ==")

    def test_SGPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCHomePage-01.xml")

    def test_SGPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCHomePage-02.xml")

    '''def test_SGPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCHomePage Post condition =="
        dumplogger.info("== Setup SGPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
