import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCheckOut Precondition =="
        dumplogger.info("== Setup VNPCCheckOut Precondition ==")

    def test_VNPCCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-01.xml")

    def test_VNPCCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-02.xml")

    def test_VNPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-03.xml")

    def test_VNPCCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-04.xml")

    def test_VNPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-05.xml")

    # def test_VNPCCheckOut06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCCheckOut-06.xml")

    def test_VNPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-07.xml")

    def test_VNPCCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-08.xml")

    def test_VNPCCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-09.xml")

    def test_VNPCCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCheckOut-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCheckOut Post condition =="
        dumplogger.info("== Setup VNPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
