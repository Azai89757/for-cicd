﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminOrdersMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import random

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminCodingMonkeyToolsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRunButton(arg):
        '''
        ClickRunButton : Click run button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate": "run_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click run cron jobs button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCodingMonkeyToolsPage.ClickRunButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCronJobByName(arg):
        '''
        SelectCronJobByName : COD order screening
                Input argu :
                    cron_job_name - name of specific cron job
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cron_job_name = arg['cron_job_name']

        xpath = Util.GetXpath({"locate": "cron_jobs"})
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": cron_job_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCodingMonkeyToolsPage.SelectCronJobByName -> ' + cron_job_name)

class AdminRegisterShopeeUserPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RegisterShopeeAccount(arg):
        '''
        RegisterShopeeAccount : Register Shopee account
                Input argu :
                    username - username
                    password - password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error

        '''
        ret = 1
        username = arg['username']
        password = arg['password']

        ##Input user name
        xpath = Util.GetXpath({"locate": "username"})
        BaseUICore.Input({"method": "id", "locate": xpath, "string": username, "result": "1"})

        ##Input password
        xpath = Util.GetXpath({"locate": "password"})
        BaseUICore.Input({"method": "id", "locate": xpath, "string": password, "result": "1"})

        ##Input phone number
        xpath = Util.GetXpath({"locate": "phone"})
        BaseUICore.Input({"method": "id", "locate": xpath, "string": str(random.randint(0, 999999)), "result": "1"})

        ##Click create user
        xpath = Util.GetXpath({"locate": "create"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create user", "result": "1"})

        ##Popup handle
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        BaseUILogic.CheckPopupWindowMessage({"expected_message": "Success", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRegisterShopeeUserPage.RegisterShopeeAccount')

class AdminAutoCancelSettingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelEditButton(arg):
        '''
        ClickCancelEditButton : Click the edit button to set the cancel rule
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click the edit button to set the cancel rule
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click the edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoCancelSettingPage.ClickCancelEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAutoCancelSetting(arg):
        '''
        InputAutoCancelSetting : Input the arrange shipping time or the shipping out time for seller
                Input argu :
                    setting_type - arrange_shipping_time_noncb_seller / arrange_shipping_time_cb_seller / shipping_out_time_noncb_seller / shipping_out_time_cb_seller
                    input_time - input the time you want to set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        setting_type = arg['setting_type']
        input_time = arg['input_time']

        ##Input the arranging time for which setting_type you choose
        xpath = Util.GetXpath({"locate": setting_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoCancelSettingPage.InputAutoCancelSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelSaveButton(arg):
        '''
        ClickCancelSaveButton : Click the save button to set the cancel rule
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click the save button to set the cancel rule
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click the save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoCancelSettingPage.ClickCancelSaveButton')
