import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCMarketPlacePayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMarketPlacePayment Precondition =="
        dumplogger.info("== Setup MYPCMarketPlacePayment Precondition ==")

    def test_MYPCMarketPlacePayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMarketPlacePayment-01.xml")

    def test_MYPCMarketPlacePayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMarketPlacePayment-02.xml")

    def test_MYPCMarketPlacePayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMarketPlacePayment-03.xml")

    def test_MYPCMarketPlacePayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMarketPlacePayment-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMarketPlacePayment Post condition =="
        dumplogger.info("== Setup MYPCMarketPlacePayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
