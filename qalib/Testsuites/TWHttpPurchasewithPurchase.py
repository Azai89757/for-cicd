import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpPurchasewithPurchase(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpPurchasewithPurchase Precondition =="
        dumplogger.info("== Setup TWHttpPurchasewithPurchase Precondition ==")

    def test_TWHttpPurchasewithPurchase01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-01.xml")

    def test_TWHttpPurchasewithPurchase02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-02.xml")

    def test_TWHttpPurchasewithPurchase03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-03.xml")

    def test_TWHttpPurchasewithPurchase04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-04.xml")

    def test_TWHttpPurchasewithPurchase05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-05.xml")

    def test_TWHttpPurchasewithPurchase06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-06.xml")

    def test_TWHttpPurchasewithPurchase07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-07.xml")

    def test_TWHttpPurchasewithPurchase08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-08.xml")

    def test_TWHttpPurchasewithPurchase09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-09.xml")

    def test_TWHttpPurchasewithPurchase10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpPurchasewithPurchase-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpPurchasewithPurchase Post condition =="
        dumplogger.info("== Setup TWHttpPurchasewithPurchase Post condition ==")
