import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class VNPCMallPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMallPage Precondition =="
        dumplogger.info("== Setup VNPCMallPage Precondition ==")

    def test_VNPCMallPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMallPage-01.xml")

    def test_VNPCMallPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMallPage-02.xml")

    def test_VNPCMallPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMallPage-03.xml")

    def test_VNPCMallPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMallPage-04.xml")

    def test_VNPCMallPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMallPage-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMallPage Post condition =="
        dumplogger.info("== Setup VNPCMallPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
