import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCAccounts(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCAccounts Precondition =="
        dumplogger.info("== Setup VNPCAccounts Precondition ==")

    def test_VNPCAccounts01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCAccounts-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCAccounts Post condition =="
        dumplogger.info("== Setup VNPCAccounts Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
