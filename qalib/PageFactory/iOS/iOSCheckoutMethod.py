﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSCheckoutMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import DecoratorHelper
import Config

##Import ios library
import iOSBaseUICore
import iOSBaseUILogic
import iOSCommonMethod
import iOSPaymentMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class iOSCheckOutPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseShippingMethod(arg):
        ''' ChooseShippingMethod : Choose a shipping method
                Input argu : shipping - shipping method to choose
                             shipping_mode_position - which order's shipping method to click
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        shipping = arg['shipping']
        shipping_mode_position = arg['shipping_mode_position']

        ##Try 5 times when choose shipping mode
        time.sleep(20)

        ##Click shipping method option
        iOSCheckOutButton.ClickShippingMethod({"shipping_mode_position":shipping_mode_position, "result":"1"})

        ##Prevent ObjectStale error
        time.sleep(3)

        ##Choose shipping type
        iOSCheckOutButton.ChooseShippingType({"shipping":shipping, "result":"1"})

        ##Waiting loading
        time.sleep(10)

        ##Click confirm btn
        iOSCheckOutButton.ClickConfirm({"result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutPage.ChooseShippingMethod -> ' + shipping)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePaymentMethod(arg):
        ''' ChoosePaymentMethod : Choose payment method
                Input argu :
                    payment : payment method to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        payment = arg['payment']

        ##Prevent ObjectStale error
        time.sleep(5)

        ##Click payment method option
        iOSCheckOutButton.ClickPaymentMethod({"result":"1"})

        time.sleep(5)

        ##Choose a payment method
        iOSPaymentMethod.iOSPaymentPage.SelectPaymentMethod({"payment":payment, "result":"1"})

        ##Prevent ObjectStale error
        time.sleep(5)

        ##Click confirm btn
        iOSCheckOutButton.ClickConfirm({"result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutPage.ChoosePaymentMethod -> ' + payment)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def WaitCheckoutLoadingIsComplete(arg):
        ''' WaitCheckoutLoadingIsComplete : Wait checkout loading is complete
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"loading_popup"})

        ##Retry to check if loading image is exist
        for retry_times in range(10):
            if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
                dumplogger.info("Loading is not complete!, retry %d times" % (retry_times))
                time.sleep(5)
            else:
                dumplogger.info("Loading is complete!")
                break
        else:
            dumplogger.info("Loading is not complete!!!")
            ret = 0

        OK(ret, int(arg['result']), 'iOSCheckOutPage.WaitCheckoutLoadingIsComplete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PayOutByCreditCard(arg):
        '''PayOutByCreditCard : payout by credit card
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pay btn
        locate = Util.GetXpath({"locate":"pay_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click pay btn", "result":"1"})

        ##Prevent ObjectStale error
        time.sleep(5)

        ##Click submit btn
        locate = Util.GetXpath({"locate":"submit_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click confirm btn", "result":"1"})

        time.sleep(10)

        ##Click confirm btn
        iOSCheckOutButton.ClickConfirm({"result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutPage.PayOutByCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PayOutByWallet(arg):
        '''PayOutByWallet : Payout by wallet
                Input argu : wallet_type - shopee_wallet / shopee_pay / airpay_wallet / airpay_bt
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        wallet_type = arg['wallet_type']

        ##Check if OTP page pop up
        xpath = Util.GetXpath({"locate":"otp_input_area"})

        if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            ##Enter vcode 123456
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":"123456", "result":"1"})

            ##Click verify button
            xpath = Util.GetXpath({"locate":"verify_btn"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click verify btn", "result":"1"})
            time.sleep(2)

        ##Click ensure wallet pay button
        xpath = Util.GetXpath({"locate": wallet_type + "_pay_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click payout btn", "result":"1"})

        time.sleep(5)

        if Config._TestCaseRegion_ == "ID":
            ##Input Shopee pay password default passsword : 012345 (only ID)
            ##ID Shopeepay pin like 111111 or 123456 is not allowed because is too simple and predictable
            for pin in range(6):
                iOSBaseUICore.iOSKeyboardAction({"actiontype":"key", "key":str(pin), "result":"1"})

            ##Click ensure wallet pay button
            time.sleep(5)
            xpath = Util.GetXpath({"locate":"confirm"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result":"1"})

            ##Click go back to homepage button
            time.sleep(5)
            xpath = Util.GetXpath({"locate":"back_to_homepage_btn"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click OK", "result":"1"})

        else:
            ##Input Wallet pay password default passsword : 111111
            for index in range(1, 7):
                xpath = Util.GetXpath({"locate":"password_field"})
                iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string": "1", "result":"1"})
                time.sleep(1)

        ##Click ensure wallet pay and ok button
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"confirm"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result":"1"})

        if wallet_type == 'airpay_bt':
            time.sleep(5)
            ##Check if otp message pop up
            xpath = Util.GetXpath({"locate": "otp_message_ok"})
            if iOSBaseUICore.iOSCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click otp message ok btn", "result": "1"})

                ##Input otp 123456 in otp page
                xpath = Util.GetXpath({"locate": "otp_input"})
                iOSBaseUICore.iOSInput({"method": "xpath", "locate": xpath, "string": "123456", "result": "1"})

                ##Click next btn
                xpath = Util.GetXpath({"locate": "next_btn"})
                iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click next btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSCheckOutPage.PayOutByWallet')


class iOSCheckOutButton:
    ''' Button : All of Page could inherit the Button class to handle event'''

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' iOSClickConfirm : Click confirm button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click confirm button
        locate = Util.GetXpath({"locate":"confirm_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click confirm button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentMethod(arg):
        '''ClickPaymentMethod : Click payment method
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click payment method
        locate = Util.GetXpath({"locate":"payment_method"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click payment method", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutButton.ClickPaymentMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckOut(arg):
        '''ClickCheckOut : Click place order btn
                Input argu :
                    N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click checkout btn
        locate = Util.GetXpath({"locate":"checkout_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click checkout btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutButton.ClickCheckOut')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingMethod(arg):
        '''ClickShippingMethod : Click shipping method in checkout page
                Input argu :
                    shipping_mode_position - which order's shipping method to click
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        shipping_mode_position = arg['shipping_mode_position']

        ##Click shipping method
        xpath = Util.GetXpath({"locate":"shipping_mode"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath + '[' + shipping_mode_position + ']', "message": "Click change shipping method on checkout page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSCheckOutButton.ClickShippingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseShippingType(arg):
        ''' ChooseShippingType : ChooseShippingType
                Input argu :
                    shipping: shipping method name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        shipping = arg['shipping']

        ##Check shipping mode exist in check out page
        xpath = Util.GetXpath({"locate":shipping})
        is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "message":"Check shipping mode is exist", "passok":"0", "result":"1"})

        ##if using selfdeliver and selfdeliver is not exist
        if shipping == "selfdeliver" and not is_exist:
            ##Click other shipping mode
            xpath = Util.GetXpath({"locate":"other_shipping_mode"})
            iOSBaseUILogic.iOSRelativeMove({"direction":"down", "result":"1"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click other shipping method on checkout page", "result":"1"})
        elif not is_exist:
            ##Swipe Down and check shipping method exist
            iOSBaseUILogic.iOSRelativeMove({"direction":"down", "result":"1"})

        ##Wait loading is complete
        iOSCheckOutPage.WaitCheckoutLoadingIsComplete({"result":"1"})

        ##Click shipping method name
        # xpath = Util.GetXpath({"locate":shipping})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click shipping method name => " + shipping, "result":"1"})

        if Config._TestCaseRegion_ == 'VN':
            ##Check if delivery time panel exist
            delivery_time_panel_xpath = Util.GetXpath({"locate":"delivery_time_panel"})
            is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":delivery_time_panel_xpath, "message":"Check delivery time option is exist", "passok":"0", "result":"1"})

            ##Click delivery time option
            if is_exist:
                time.sleep(3)
                delivery_time_option_xpath = Util.GetXpath({"locate":"delivery_time_option"})
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":delivery_time_option_xpath, "message":"Click delivery time option", "result":"1"})

                iOSCheckOutButton.ClickConfirm({"result":"1"})

        OK(ret, int(arg['result']), 'iOSCheckOutButton.ChooseShippingType')
