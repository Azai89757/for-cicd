import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpExclusivePrice(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpExclusivePrice Precondition =="
        dumplogger.info("== Setup TWHttpExclusivePrice Precondition ==")

    def test_TWHttpExclusivePrice01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-01.xml")

    def test_TWHttpExclusivePrice02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-02.xml")

    def test_TWHttpExclusivePrice03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-03.xml")

    def test_TWHttpExclusivePrice04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-04.xml")

    def test_TWHttpExclusivePrice05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-05.xml")

    def test_TWHttpExclusivePrice06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-06.xml")

    def test_TWHttpExclusivePrice07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-07.xml")

    def test_TWHttpExclusivePrice08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-08.xml")

    def test_TWHttpExclusivePrice09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-09.xml")

    def test_TWHttpExclusivePrice10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-10.xml")

    def test_TWHttpExclusivePrice11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-11.xml")

    def test_TWHttpExclusivePrice12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-12.xml")

    def test_TWHttpExclusivePrice13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-13.xml")

    def test_TWHttpExclusivePrice14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpExclusivePrice-14.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpExclusivePrice Post condition =="
        dumplogger.info("== Setup TWHttpExclusivePrice Post condition ==")
