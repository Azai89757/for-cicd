﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
BaseUICore.py: The def of this file called by XML mainly.
'''

##Import python library
import os
import time
import json
import random
import StringIO
from urllib3.exceptions import MaxRetryError

##Import framework common library
import Parser
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger, chrome_logger
from Config import _GeneralWaitingPeriod_, _AverageWaitingPeriod_, _ShortWaitingPeriod_, _InstantWaitingPeriod_, _OutputFolderPath_, dict_systemslash, _platform_

##Import selenium library
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException, UnexpectedAlertPresentException, InvalidCookieDomainException
from selenium.common.exceptions import TimeoutException, InvalidArgumentException, WebDriverException, NoSuchWindowException, InvalidSessionIdException

##Import Web library
import BaseUILogic

_WebDriver_ = ""


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def InitialWebDriver(arg):
    '''
    InitialWebDriver : Initial selenium web driver
            Input argu :
                browser - ie, firefox, chrome
                local_storage - 1 (default value, will use incognito mode) / 0 (will use local storage to open chrome)
            Return code :
                1 - success
                0 - fail (Especially for WebDriverException)
                -1 - error
    '''
    browser = arg["browser"]
    ret = 1
    message = ""

    ##For special usage, will need to use local storage to launch browser
    try:
        local_storage = arg["local_storage"]
    except KeyError:
        local_storage = 0
        dumplogger.info('KeyError - local_storage default is 0')
    except:
        local_storage = 0
        dumplogger.info('Other error - local_storage default is 0')
    finally:
        dumplogger.info("local_storage = %s" % (local_storage))

    ##Declare and initial _WebDriver_
    global _WebDriver_

    ##If _WebDriver_ already exist, then can just clean it's cookie to avoid performance overload
    if not _WebDriver_:
        ##Used to calculate total new opened web driver time.
        dumplogger.info("InitialWebDriver with no driver already opened.")

        ##Select browser, default is IE
        if browser == "ie":
            iedriver = Config._CurDataDIR_ + r"\bin\IEDriverServer_x86_" + Config._IEDriverVersion_ + ".exe"
            _WebDriver_ = webdriver.Ie(iedriver)

        elif browser == "firefox":
            _WebDriver_ = webdriver.Firefox()

        elif browser == "safari":
            _WebDriver_ = webdriver.Safari()

        elif browser == "chrome":
            ##Configure chrome driver options
            options = webdriver.ChromeOptions()

            ##If need to use local storage with Chrome
            if local_storage == "1":
                chrome_local_storage = r"C:\\Users\\" + Config._CurUser_ + "\\AppData\\Local\\Google\\Chrome\\User Test Data"
                options.add_argument(r"user-data-dir=" + chrome_local_storage)
            else:
                options.add_argument("--incognito")

            ##Using headless mode
            if Config._Headless_:
                options.add_argument('--headless')
                options.add_argument('--disable-gpu')

            ##Rest of the options
            options.add_argument('--no-sandbox')
            options.add_argument('--disable-dev-shm-usage')
            options.add_argument('--disable-notifications')
            options.add_argument('--disable-infobars')
            options.add_argument('--allow-running-insecure-content')
            options.add_argument('--window-size=1920,1080')
            options.add_argument('--disk-cache-size=1')
            options.add_argument('--log-level=2')
            options.add_argument('--enable-extensions')
            options.add_argument("goog:loggingPrefs={'performance': 'ALL'}")
            options.add_experimental_option('w3c', False)
            options.add_experimental_option("prefs",
            {
                "profile.default_content_setting_values.automatic_downloads": 1,
                "profile.default_content_settings.popups": 0,
                "download.default_directory": Config._CurDIR_,
                "download.prompt_for_download": False,
                "download.directory_upgrade": True,
                "safebrowsing.enabled": True
            })

            ##Define browser capabilities
            caps = DesiredCapabilities.CHROME
            caps['goog:loggingPrefs'] = {'performance': 'ALL'}
            caps['goog:chromeOptions'] = options.to_capabilities()['goog:chromeOptions']

            for retry_time in range(7):
                dumplogger.info("Start InitialWebDriver retry loop in %d time." % (retry_time + 1))
                try:
                    ##To decide selenium trigger mode
                    if Config._RemoteSeleniumDriver_:
                        ##Sleep 5~10 secs randomly to avoid error
                        time.sleep(random.randint(5,10))
                        grid_url = "http://" + Config._SeleniumGridUrl_[str(Config._RemoteSeleniumDriver_)] + "/wd/hub"
                        dumplogger.info("This case will use Selenium Grid to execute test.")
                        dumplogger.info("Grid URL: %s" % (grid_url))
                        _WebDriver_ = webdriver.Remote(command_executor=grid_url, desired_capabilities=caps, options=options)
                    else:
                        dumplogger.info("This case will use Local Browser to execute test.")
                        _WebDriver_ = webdriver.Chrome(Config._ChromeDriver_, chrome_options=options, desired_capabilities=caps)

                    ##Clear all brower cookies and maximize window
                    DeleteWebDriverCookie()
                    _WebDriver_.maximize_window()

                    ##Set implicitly wait period
                    _WebDriver_.implicitly_wait(_AverageWaitingPeriod_)

                except WebDriverException:
                    #dumplogger.exception("Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error. Please check log for more informaion.")
                    DeInitialWebDriver("kill_browser")
                    ret = 0
                    time.sleep(15)

                except:
                    #dumplogger.exception("Encounter unknown exception. Please check log for more informaion.")
                    DeInitialWebDriver("kill_browser")
                    ret = -1
                    time.sleep(15)
                else:
                    dumplogger.info("InitialWebDriver succeeded in %d time with no exception." % (retry_time + 1))
                    ret = 1
                    break

    ##Determine output message
    if ret == 0:
        if Config._CaseRetried_:
            message = "InitialWebDriver still encounter webdriver exception after retry. Maybe wedriver is crashed / closed / connection error."
            dumplogger.exception(message)
            if Config._RemoteSeleniumDriver_:
                message = "[%s] grid encounter InitialWebDriver webdriver exception." % (grid_url)
                dumplogger.exception(message)
        else:
            message = "InitialWebDriver encounter webdriver exception without retry. Maybe wedriver is crashed / closed / connection error."
            dumplogger.exception(message)
    elif ret == -1:
        message = "InitialWebDriver encounter unknown exception."
        dumplogger.exception(message)

    OK(ret, int(arg['result']), 'InitialWebDriver->' + message)

@DecoratorHelper.FuncRecorder
def DeInitialWebDriver(mode):
    '''
    DeInitialWebDriver : deinitial selenium web driver
            Input argu :
                mode - null (This argument defined with default, all entry from xml should follow this way) / kill_browser (kill browser entirly, should only called from function with specific needs)
            Return code : N/A
    '''
    global _WebDriver_

    try:
        if _WebDriver_:
            ##clean currecnt selenium sessions
            if mode == "kill_browser":
                _WebDriver_.quit()
                ##prevent Error 10053 for android and ios cases
                dumplogger.info("Quit web driver and close brower success")
                dumplogger.info("Set variable _WebDriver_ => None")
                _WebDriver_ = None

            ##Clean browser cookie only
            else:
                ## Handle unknow popup window
                BaseUILogic.HandleUnknownPopUpWindow({"expected_time": "1", "result": "1"})
                DeleteWebDriverCookie()
        else:
            dumplogger.info("Web driver already set to None")

    except AttributeError:
        dumplogger.info("Please initial web driver first, maybe web driver is closed in script fail or tear down")
        dumplogger.info("Set variable _WebDriver_ => None")
        _WebDriver_ = None

    except WebDriverException:
        dumplogger.exception("Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error. Please check log for more informaion.")
        dumplogger.info("Set variable _WebDriver_ => None")
        _WebDriver_ = None

    except:
        dumplogger.exception('Got exception error')
        dumplogger.info("Set variable _WebDriver_ => None")
        _WebDriver_ = None

def DeleteWebDriverCookie():
    '''
    DeleteWebDriverCookie : Delete web driver cookie
            Input argu : N/A
            Return code : N/A
    '''
    dumplogger.info("Enter DeleteWebDriverCookie")

    try:
        for retry_time in range(5):
            if _WebDriver_.get_cookies():
                _WebDriver_.delete_all_cookies()
                dumplogger.info("Deleting all browser cookies. Retry time: %s" % (retry_time + 1))
                time.sleep(5)
            else:
                dumplogger.info("Web driver no cookie exist")
                break
        else:
            dumplogger.error("Web driver cookie still exist!!!")

    except WebDriverException:
        dumplogger.exception("Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error. Please check log for more informaion.")

    except:
        dumplogger.exception("Encounter unknown exception. Please check log for more informaion.")

@DecoratorHelper.FuncRecorder
def GotoURL(arg):
    '''
    GotoURL : Go to URL by selenium, need call InitialWebDriver first
            Input argu :
                url - url link
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    dumplogger.info("Enter GotoURL")
    url = arg['url']
    ret = 1

    dumplogger.info("url = " + url)

    ##Go to URL
    try:
        _WebDriver_.get(url)
        message = "Go to url => %s" % (url)

    except TimeoutException:
        message = "Encounter timeout exception."
        dumplogger.exception(message)
        ret = -1

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'GotoURL->' + message)

@DecoratorHelper.FuncRecorder
def Input(arg):
    '''
    Input : Input any strings in Web
            Input argu :
                locate - Position in web
                string - Input strings
                method - xpath, css, id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    locate = arg['locate']
    string = arg['string']
    method = arg['method']
    message = "Input %s" % str((string))
    ret = 1

    try:
        dumplogger.info("string = %s" % (string))
    except:
        dumplogger.info("string may be a key combination.")

    dumplogger.info("method = %s" % (method))

    ##Detect path to find element by method
    try:
        if method == "xpath":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.XPATH, (locate)))).clear()
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.XPATH, (locate)))).send_keys(string)

        elif method == "css":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CSS_SELECTOR, (locate)))).clear()
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CSS_SELECTOR, (locate)))).send_keys(string)

        elif method == "id":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.ID, (locate)))).clear()
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.ID, (locate)))).send_keys(string)

        elif method == "name":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CLASS_NAME, (locate)))).clear()
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CLASS_NAME, (locate)))).send_keys(string)

        elif method == "xpath_no_clear":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.XPATH, (locate)))).send_keys(string)

        elif method == "javascript":
            ExecuteScript({"script": "document.evaluate(\"" + locate + "\", document,  null, XPathResult.ANY_TYPE, null).iterateNext().click()", "result": "1"})
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CLASS_NAME, (locate)))).clear()
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CLASS_NAME, (locate)))).send_keys(string)

        else:
            print "Please check method"

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except NoSuchWindowException:
        message = "Windows has been closed."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    time.sleep(0.5)

    OK(ret, int(arg['result']), 'Input->' + message)

@DecoratorHelper.FuncRecorder
def Click(arg):
    '''
    Click : Click any content in Web
            Input argu :
                locate - Position in web
                method - locate element method
                message - output message
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg['locate']
    method = arg['method']
    message = arg['message']
    ret = 1

    try:

        if method == "xpath":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.XPATH, (locate)))).click()

        elif method == "css":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CSS_SELECTOR, (locate)))).click()

        elif method == "id":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.ID, (locate)))).click()

        elif method == "classname":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.CLASS_NAME, (locate)))).click()

        elif method == "javascript":
            ExecuteScript({"script": "document.evaluate(\"" + locate + "\", document,  null, XPathResult.ANY_TYPE, null).iterateNext().click()", "result": "1"})

        else:
            dumplogger.error("Please check method")
            ret = -1

        dumplogger.info(message)

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    except NoSuchWindowException:
        message = "Windows has been closed."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    time.sleep(2)

    OK(ret, int(arg['result']), 'Click->' + message)

@DecoratorHelper.FuncRecorder
def DoubleClick(arg):
    '''
    DoubleClick : double click element
            Input argu :
                locate - xpath of the element
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    locate = arg["locate"]
    message = ""

    try:
        ##Get element xpath
        source_element = _WebDriver_.find_element_by_xpath(locate)

        ##Initial action by webdriver
        action = ActionChains(_WebDriver_)

        ##Double click element
        action.double_click(source_element).perform()

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'DoubleClick->' + message)

@DecoratorHelper.FuncRecorder
def GetElements(arg):
    '''
    GetElements : Get any elements in web
            Input argu :
                locate - Position in web
                method - xpath, css, id
                mode - single, multi
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    mode = arg["mode"]
    locate = arg["locate"]
    method = arg["method"]
    message = ""
    ret = 1

    ##Get web elements by method
    try:
        if mode == "single":
            if method == "xpath":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_element_by_xpath(locate)
            elif method == "css":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_element_by_css_selector(locate)
            elif method == "id":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_element_by_id(locate)
            elif method == "classname":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_element_by_class_name(locate)

        elif mode == "multi":
            if method == "xpath":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_elements_by_xpath(locate)
            elif method == "css":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_elements_by_css_selector(locate)
            elif method == "id":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_elements_by_id(locate)
            elif method == "classname":
                GlobalAdapter.CommonVar._PageElements_ = _WebDriver_.find_elements_by_class_name(locate)

    except NoSuchElementException:
        message = "Get elements failed. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except IndexError:
        message = "Index error"
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception"
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'GetElements->' + message)

@DecoratorHelper.FuncRecorder
def SendSeleniumKeyEvent(arg):
    '''
    SendSeleniumKeyEvent : Send key event in web driver by selenium action chains
            Input argu :
                action_type - string / enter / down / esc / back_space
                string - string you want to send
            Return code :
                1 - success
                -1 - error
    '''

    ret = 1
    action_type = arg["action_type"]
    message = ""

    try:
        ##Initial action by webdriver
        action = ActionChains(_WebDriver_)

        ##Send string
        if action_type == "string":
            string = arg["string"]
            action.send_keys(string)

        ##Send enter
        elif action_type == "enter":
            action.send_keys(Keys.ENTER)

        ##Send down
        elif action_type == "down":
            action.send_keys(Keys.DOWN)

        ##Send esc
        elif action_type == "esc":
            action.send_keys(Keys.ESCAPE)

        ##Send back space
        elif action_type == "back_space":
            action.send_keys(Keys.BACK_SPACE)

        ##Send page down
        elif action_type == "page_down":
            action.send_keys(Keys.PAGE_DOWN)

        ##Perform action
        action.perform()

        ##Clear action
        action.reset_actions()

    except KeyError:
        message = "Encounter KeyError"
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Got exception error."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'SendSeleniumKeyEvent->' + message)

@DecoratorHelper.FuncRecorder
def ParseElements(arg):
    '''
    ParseElements : Parse any elements in web
            Input argu :
                isfuzzy : 1 (fuzzy Comparison) / 0 (fully Comparison)
                Keyword - the keyword want to check
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    message = ""
    isfuzzy = arg["isfuzzy"]
    keyword = arg["keyword"]
    ret = 1

    ##Error handle of page attributes is none, mostly not possible
    if GlobalAdapter.CommonVar._PageAttributes_ is None:
        ##Page attribute is empty
        dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = " + type(GlobalAdapter.CommonVar._PageAttributes_))
        message = "Page attribute is empty"
        ret = 0

    ## Page attributes can be null string or anything
    else:
        ##Check string is match
        dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = " + GlobalAdapter.CommonVar._PageAttributes_)

        ##use fuzzy compare or there is \n in the string got from selenium
        if isfuzzy == "1" or "\n" in GlobalAdapter.CommonVar._PageAttributes_:
            if keyword in GlobalAdapter.CommonVar._PageAttributes_:
                message = "Keyword found"

            else:
                dumplogger.info("Did not find any matched keyword in page attributes")
                message = "Keyword not found. Attribute => %s" % (GlobalAdapter.CommonVar._PageAttributes_)
                ret = 0

        ##use fully compare
        elif isfuzzy == "0":
            ##Don't use fuzzy compare so keyword must match page attributes equally
            if keyword == GlobalAdapter.CommonVar._PageAttributes_.strip():
                message = "Keyword fully matched"

            else:
                message = "Keyword not found. Attribute => %s" % (GlobalAdapter.CommonVar._PageAttributes_)
                dumplogger.info("message")
                ret = 0

        ##unexpected isfuzzy option
        else:
            message = "isfuzzy should be only 1 or 0, but got '%s'" % (isfuzzy)
            dumplogger.exception(message)
            ret = 0

    OK(ret, int(arg['result']), 'ParseElements->' + message)

@DecoratorHelper.FuncRecorder
def CheckButtonClickable(arg):
    '''
    CheckButtonClickable : Check the button state clickable or not
            Input argu :
                locate - Position in web
                message - output message
                method - xpath, css, id
                state - enable, disable
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 0
    message = arg['message']
    method = arg["method"]
    locate = arg["locate"]
    state = arg["state"]
    message = ""

    ##Check element is clickable or not
    try:
        if method == "xpath":
            ele_clickable = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).is_enabled()
        elif method == "css":
            ele_clickable = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.CSS_SELECTOR, (locate)))).is_enabled()
        elif method == "id":
            ele_clickable = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.ID, (locate)))).is_enabled()
        elif method == "name":
            ele_clickable = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.NAME, (locate)))).is_enabled()

        ##Check element clickable result is expected or not
        if (state == 'enable' and ele_clickable) or (state == 'disable' and not ele_clickable):
            dumplogger.info('Button clickable is %s and result as expected %s' % (ele_clickable, state))
            ret = 1
        else:
            message = "Button clickable is %s but expected result is %s" % (ele_clickable, state)
            dumplogger.info(message)
            ret = 0

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown error."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'CheckButtonClickable->' + message)

def CheckElementExist(arg):
    '''
    CheckElementExist : Check the element exist or not
            Input argu :
                locate - Position in web
                method - xpath, css, id
                passok - for specific purpose, if don't want to return OK stauts when calling this function
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    method = arg["method"]
    locate = arg["locate"]
    message = ""
    dumplogger.info("Enter CheckElementExist")
    dumplogger.info("locate = %s" % (locate))

    ##Delclare passok argument, if not pass in argument, set it to default value "1"
    try:
        passok = arg["passok"]
    except KeyError:
        passok = "1"
        dumplogger.info('KeyError - passok default is 1')
    except:
        passok = "1"
        dumplogger.info('Other error - passok default is 1')
    finally:
        dumplogger.info("passok = %s" % (passok))

    ##Locate element by specific method, use presence_of_element_located wait util element display in _AverageWaitingPeriod_ seconds
    try:
        if method == "xpath":
            WebDriverWait(_WebDriver_, _AverageWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate))))
        elif method == "css":
            WebDriverWait(_WebDriver_, _AverageWaitingPeriod_).until(EC.presence_of_element_located((By.CSS_SELECTOR, (locate))))
        elif method == "id":
            WebDriverWait(_WebDriver_, _AverageWaitingPeriod_).until(EC.presence_of_element_located((By.ID, (locate))))
        elif method == "name":
            WebDriverWait(_WebDriver_, _AverageWaitingPeriod_).until(EC.presence_of_element_located((By.NAME, (locate))))
        else:
            ##If using error method will return error
            message = "Cannot find method => %s" % (method)
            dumplogger.error(message)
            ret = 0

    ##Can not locate element in timeout exception
    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_AverageWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    ##Can not locate element in unknown exception
    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = 0

    ##If passok is 1, enter ok function to end function, or return ret to checkout result in other function
    if int(passok):
        OK(ret, int(arg['result']), 'CheckElementExist->' + message)
    else:
        return ret

@DecoratorHelper.FuncRecorder
def Move2ElementAndClick(arg):
    '''
    Move2ElementAndClick : Move to a location and click it's subitem which is hidden/live data
            Input argu :
                locate - Position of the main item
                locatehidden - Position of the hidden/live data subitem
                method - xpath / css
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg['locate']
    locatehidden = arg['locatehidden']
    method = arg['method']
    ret = 1
    message = ""

    ##Initial action by webdriver
    action = ActionChains(_WebDriver_)

    try:
        ##Get element
        element = _WebDriver_.find_element_by_xpath(locate)

        ##Move to first element
        action.move_to_element(element).perform()
        time.sleep(1)

        ##Wait element appear by method
        if method == "xpath":
            dumplogger.info("Enter xpath method")
            wait = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, locatehidden)))

        elif method == "css":
            dumplogger.info("Enter css method")
            wait = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.CSS_SELECTOR, locatehidden)))

        ##Move to second element
        action.move_to_element(wait).click().perform()

    except NoSuchElementException:
        message = "No such element. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'Move2ElementAndClick->' + message)

@DecoratorHelper.FuncRecorder
def Move2ElementAndCheckPopUp(arg):
    '''
    Move2ElementAndCheckPopUp : Move to a location and check it's subitem which is hidden/live data
            Input argu :
                locate - Position of the main item
                locatehidden - Position of the hidden/live data subitem
                method - xpath, css
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg['locate']
    locatehidden = arg['locatehidden']
    method = arg['method']
    ret = 1

    try:
        ##Initial action by webdriver
        action = ActionChains(_WebDriver_)

        ##Get element
        element = _WebDriver_.find_element_by_xpath(locate)

        ##Move to first element
        action.move_to_element(element).perform()
        time.sleep(1)

        message = "Hidden element showed up."
        if method == "xpath":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, locatehidden)))
        elif method == "css":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.CSS_SELECTOR, locatehidden)))
        dumplogger.info(message)

    except TimeoutException:
        message = "Hidden element not showing up. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error"
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'Move2ElementAndCheckPopUp->' + message)

@DecoratorHelper.FuncRecorder
def CheckMouseHover(arg):
    '''
    CheckMouseHover : Move to a location and check style changed
            Input argu :
                locate - Position of the main item
                style - style of attribute
                attribute - element attribute
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg['locate']
    style = arg['style']
    attribute = arg['attribute']
    message = ""
    ret = 1

    ##Initial action by webdriver
    action = ActionChains(_WebDriver_)

    try:
        ##Get element
        element = _WebDriver_.find_element_by_xpath(locate)

        ##Move to element
        action.move_to_element(element).perform()

        ##Wait element appear by method
        dumplogger.info("Enter xpath method")
        time.sleep(1)
        mouse_hover_element = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, locate)))

        ##Get mouse hover element stlye by giving specific style
        display = mouse_hover_element.value_of_css_property(style)
        dumplogger.info("Get style attribute: %s %s" % (style, display))
        if attribute not in display:
            ret = 0

    except NoSuchElementException:
        message = "No such element. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'CheckMouseHover->' + message)

@DecoratorHelper.FuncRecorder
def GetAndCheckValueOfCSSProperty(arg):
    '''
    GetAndCheckValueOfCSSProperty : Move to a location and check style changed
            Input argu :
                locate - Position of the main item
                style - element style attribute
                attribute - expected style attribute value
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1
    locate = arg['locate']
    style = arg['style']
    attribute = arg['attribute']
    message = ""

    try:
        ##Wait and Get element
        element = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, locate)))

        ##Get element css property by giving specific style
        display = element.value_of_css_property(style)
        dumplogger.info("Get style attribute: {} {}".format(style, display))

        ##Compare giving attribute with attribute get
        if attribute not in display:
            ret = 0

    except NoSuchElementException:
        message = "No Such element. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    OK(ret, int(arg['result']), 'GetAndCheckValueOfCSSProperty->' + message)

@DecoratorHelper.FuncRecorder
def ClickByElementXYCoordinates(arg):
    '''
    ClickByElementXYCoordinates : Click element by its X,Y coordinates
            Input argu :
                locateX - X axis of the item
                locateY - Y axis of the item
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    locateX = arg['locate_x']
    locateY = arg['locate_y']
    message = ""
    ret = 1

    ##Initial action by webdriver
    action = ActionChains(_WebDriver_)

    ##Click the coordinates
    try:
        action.move_by_offset(locateX, locateY).click().perform()
        dumplogger.info("button found and clicked.")

    except NoSuchElementException:
        message = "Button is not clickable at the time."
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    except:
        message = "Got exception error."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'ClickByElementXYCoordinates->' + message)

@DecoratorHelper.FuncRecorder
def DragAndDropByOffsetForImg(arg):
    '''
    DragAndDropByOffsetForImg : Drag and Drop by offset for img
            Input argu :
                x_offset - before x offset
                y_offset - before y offset
                after_x_offset - which x offset you want to set
                after_y_offset - which y offset you want to set
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    x_offset = arg['x_offset']
    y_offset = arg['y_offset']
    after_x_offset = arg['after_x_offset']
    after_y_offset = arg['after_y_offset']
    message = ""

    try:
        ##Initial action by webdriver
        action = ActionChains(_WebDriver_)

        ##Move to offset
        action.move_by_offset(x_offset, y_offset).click_and_hold().perform()
        action.move_by_offset(after_x_offset, after_y_offset).release().perform()

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'DragAndDropByOffsetForImg->' + message)

@DecoratorHelper.FuncRecorder
def SwitchToFrame(arg):
    '''
    SwitchToFrame : To switch Webdriver focus frame.
                    <frame name='test1'>
                    <frame name='test2'>
                    In usage, the default frame is focus on test1. We can use this function to switch frame
            Input argu :
                frame - xpath of specific frame
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    frame = arg['frame']
    message = ""
    ret = 1

    try:
        ##Switch to specific frame
        _WebDriver_.switch_to.frame(_WebDriver_.find_element_by_xpath(frame))

    except NoSuchElementException:
        message = "No such element. Element xpath = > %s" % (frame)
        dumplogger.exception(message)
        ret = 0

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'SwitchToFrame->' + message)

@DecoratorHelper.FuncRecorder
def SwitchToOriginalFrame(arg):
    '''
    SwitchToOriginalFrame : To switch Webdriver back to oringinal frame
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    message = ""

    try:
        ##Switch to oringinal frame
        _WebDriver_.switch_to_default_content()

    except NoSuchElementException:
        message = "No such element."
        dumplogger.exception(message)
        ret = 0

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'SwitchToOriginalFrame->' + message)

@DecoratorHelper.FuncRecorder
def GetAttributes(arg):
    '''
    GetAttributes : Get element attribute from _PageElements_
            Input argu :
                attrtype - text, location, count, get_attribute
                attribute - attribute for get_attribute
                mode - single, multi
                message - output message
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    GlobalAdapter.CommonVar._PageAttributes_ = ""
    GlobalAdapter.CommonVar._PageAttributesList_ = []
    attrtype = arg["attrtype"]
    mode = arg["mode"]
    message = ""
    ret = 1

    try:
        attribute = arg["attribute"]
    except KeyError:
        attribute = ""
        dumplogger.info('KeyError - attribute default is empty string')
    except:
        attribute = ""
        dumplogger.info('Other error - attribute default is empty string')
    finally:
        dumplogger.info("attribute = %s" % (attribute))

    try:
        if mode == "single":
            if attrtype == 'text':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.text
            elif attrtype == 'location':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.location
            elif attrtype == 'size':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.size
            elif attrtype == 'get_attribute':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.get_attribute(attribute)

        elif mode == "multi":
            for tmp in GlobalAdapter.CommonVar._PageElements_:
                if attrtype == 'text':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.text)
                elif attrtype == 'location':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.location)
                elif attrtype == 'size':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.size)
                elif attrtype == 'get_attribute':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.get_attribute(attribute))
            if attrtype == 'count':
                GlobalAdapter.CommonVar._PageAttributes_ = len(GlobalAdapter.CommonVar._PageElements_)

    except NoSuchElementException:
        message = "Get attribute failed."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception"
        dumplogger.exception(message)
        ret = -1

    finally:
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributesList_)

    OK(ret, int(arg['result']), 'GetAttributes->' + message)

@DecoratorHelper.FuncRecorder
def ParseAttributes(arg):
    '''
    ParseAttributes : Parse any elements in web
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    message = ""
    ret = 0

    if "\n" in GlobalAdapter.CommonVar._PageAttributes_:
        if arg['keyword'] in GlobalAdapter.CommonVar._PageAttributes_:
            message = "In -> Success"
            ret = 1
    else:
        ##Check string is match
        if arg['keyword'] == GlobalAdapter.CommonVar._PageAttributes_:
            message = "Equal -> Success"
            ret = 1

    OK(ret, int(arg['result']), 'ParseAttributes->' + message)

@DecoratorHelper.FuncRecorder
def PageHasLoaded(arg):
    '''
    PageHasLoaded : Check DOM status code and wait for the page is fully loaded.
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 0
    message = ""

    ##Get current time
    start_time = time.time()

    try:
        ##Check webpage status in 60 seconds
        while time.time() < start_time + 60:
            page_state = _WebDriver_.execute_script('return document.readyState;')

            ##Page status is interactive
            if page_state in ('interactive', 'complete'):
                dumplogger.info("DOM is now interactive, total waiting time is %s" % (start_time))
                ret = 1
                break

            ##Page status is not interactive, wait and keep checking
            else:
                time.sleep(0.1)
                dumplogger.info("DOM is not fully loaded yet, current waiting time is %s" % (start_time))

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'PageHasLoaded->' + message)

@DecoratorHelper.FuncRecorder
def GetBrowserCurrentUrl(arg):
    '''
    GetBrowserCurrentUrl : Get browser current url
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    try:
        GlobalAdapter.CommonVar._PageUrl_ = _WebDriver_.current_url
        message = "Browser url: %s" % (GlobalAdapter.CommonVar._PageUrl_)
        dumplogger.info(message)

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Can't get current url!!!"
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'GetBrowserCurrentUrl->' + message)

@DecoratorHelper.FuncRecorder
def SendKeyboardEvent(arg):
    '''
    SendKeyboardEvent : Send keyboard event and combinations to path (xpath only).
            Input argu :
                locate - path
                sendtype - choose what kind of keys you want to send, ctrl+"a"/ctrl+"c"/ctrl+"x"/ctrl+"v"/Del/Enter/Back/Tab/Down
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg["locate"]
    sendtype = arg["sendtype"]
    message = "Send type %s" % (sendtype)
    ret = 1

    try:

        if sendtype == "select all":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.CONTROL,'a')
        elif sendtype == "copy":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.CONTROL,'c')
        elif sendtype == "cut":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.CONTROL,'c')
        elif sendtype == "paste":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.CONTROL,'v')
        elif sendtype == "delete":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.DELETE)
        elif sendtype == "enter":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.ENTER)
        elif sendtype == "back":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.BACK_SPACE)
        elif sendtype == "tab":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.TAB)
        elif sendtype == "down":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.visibility_of_element_located((By.XPATH, (locate)))).send_keys(Keys.DOWN)
        elif sendtype == "esc":
            ##Press escape globally
            webdriver.ActionChains(_WebDriver_).send_keys(Keys.ESCAPE).perform()
        else:
            dumplogger.debug("Please choose the right method again.")
            ret = -1

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'SendKeyboardEvent->' + message)

@DecoratorHelper.FuncRecorder
def SelectDropDownList(arg):
    '''
    SelectDropDownList : Click a drop down list by using "select"
        Input argu :
            locate - path
            selecttype - select by index, value, or text
            selectkey - select key of index, value, or text
        Return code :
            1 - success
            0 - fail
            -1 - error
    '''
    locate = arg["locate"]
    selecttype = arg["selecttype"]
    selectkey = arg["selectkey"]
    message = "Select key => %s" % (selectkey)
    ret = 1

    try:
        ##Get element locate
        s1 = Select(_WebDriver_.find_element_by_xpath(locate))

        ##Using select method to locate dropdown list by different select type
        if selecttype == 'index':
            s1.select_by_index(selectkey)
        elif selecttype == 'value':
            s1.select_by_value(selectkey)
        elif selecttype == 'text':
            s1.select_by_visible_text(selectkey)
        else:
            dumplogger.error("Please choose the right type again")
            ret = -1

    except TimeoutException:
        message = "Cannot find element. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'SelectDropDownList->' + message)

@DecoratorHelper.FuncRecorder
def DragAndDrop(arg):
    '''
    DragAndDrop : Drag and Drop
            Input argu :
                drag_elem - element need to drag
                target_elem - drag element to target
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    drag_elem = arg["drag_elem"]
    target_elem = arg["target_elem"]
    message = ""

    try:
        ##Locate the drag element xpath and target element xpath
        source_element = _WebDriver_.find_element_by_xpath(drag_elem)
        dest_element = _WebDriver_.find_element_by_xpath(target_elem)

        ##Initial action by webdriver
        action = ActionChains(_WebDriver_)

        ##Move source_element to dest_element
        action.drag_and_drop(source_element, dest_element).perform()

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'DragAndDrop->' + message)

@DecoratorHelper.FuncRecorder
def DragAndDropByOffset(arg):
    '''
    DragAndDropByOffset : Drag and Drop by offset
            Input argu :
                drag_elem - element need to drag
                x_offset - x offset
                y_offset - y offset
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    drag_elem = arg["drag_elem"]
    x_offset = arg['x_offset']
    y_offset = arg['y_offset']
    message = ""

    try:
        ##Locate the drag element xpath
        source_element = _WebDriver_.find_element_by_xpath(drag_elem)

        ##Initial action by webdriver
        action = ActionChains(_WebDriver_)

        ##Move to offset
        action.click_and_hold(source_element).pause(2).move_by_offset(x_offset, y_offset).pause(2).release(source_element).perform()

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'DragAndDropByOffset->' + message)

@DecoratorHelper.FuncRecorder
def ExecuteScript(arg):
    '''
    ExecuteScript : Execute javascript
            Input argu :
                xpath - interactive element xpath if need
                script - script need to excute
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    script = arg["script"]
    message = ""

    try:
        xpath = arg["xpath"]
        locate = _WebDriver_.find_element_by_xpath(xpath)
    except KeyError:
        xpath = "0"
        dumplogger.exception('KeyError - xpath default is 0')
    except:
        xpath = "0"
        dumplogger.exception('Other error - xpath default is 0')
    finally:
        dumplogger.info("xpath = %s" % (xpath))

    try:
        if int(xpath):
            _WebDriver_.execute_script(script, locate)
        else:
            _WebDriver_.execute_script(script)

    except UnexpectedAlertPresentException:
        message = "Encounter Popup Alert Exception!!!"
        dumplogger.exception(message)
        ret = 0

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'ExecuteScript->' + message)

@DecoratorHelper.FuncRecorder
def ParseNetWorkCookies(arg):
    '''
    ParseNetWorkCookies : Parse network result in selenium log and get request info
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    time.sleep(5)
    message = "Parse sessionid and csrftoken from admin"

    try:
        ##Get cookies from web driver
        for each_cookies in _WebDriver_.get_cookies():
            dumplogger.info(each_cookies)
            if each_cookies["name"] == "sessionid":
                dumplogger.info("session id found!")
                GlobalAdapter.APIVar._HttpCookie_["sessionid"] = each_cookies["value"]

            elif each_cookies["name"] == "csrftoken":
                dumplogger.info("csrftoken found!")
                GlobalAdapter.APIVar._HttpCookie_["csrftoken"] = each_cookies["value"]

        dumplogger.info(GlobalAdapter.APIVar._HttpCookie_)

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'ParseNetWorkCookies->' + message)

@DecoratorHelper.FuncRecorder
def MoveToElementCoordinate(arg):
    '''
    MoveToElementCoordinate : Move to element with action chains
            Input argu :
                locate - xpath of the element
                is_click - 1 (move to element and perform click) / 2 (move to element only)
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    locate = arg["locate"]
    is_click = arg["is_click"]

    ##Initial action chains
    action = ActionChains(_WebDriver_)

    try:
        xpath = _WebDriver_.find_element_by_xpath(locate)
        if int(is_click):
            action.move_to_element(xpath).click().perform()
            message = "Click element"
        else:
            action.move_to_element(xpath).perform()
            message = "Move to element"

    except TimeoutException:
        message = "Cannot find element in 15 seconds. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = -1

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'MoveToElementCoordinate->' + message)

@DecoratorHelper.FuncRecorder
def CreateNewTab(arg):
    '''
    CreateNewTab : Create new tab
            Input argu : N/A
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Execute javascript to create new tab
    ExecuteScript({"script":"window.open('');", "result": "1"})

    OK(ret, int(arg['result']), 'CreateNewTab')

@DecoratorHelper.FuncRecorder
def CheckElementIsSelected(arg):
    '''
    CheckElementIsSelected : Check element is selected
            Input argu :
                locate - Position in web
                method - xpath, css, id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    method = arg["method"]
    locate = arg["locate"]
    ret = 1

    try:
        if method == "xpath":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).is_selected()
        elif method == "css":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).is_selected()
        elif method == "id":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).is_selected()
        elif method == "name":
            WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).is_selected()
        message = "Selected"
    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #GetElementTreeAndCompare({"locate":locate})
        ret = 0

    OK(ret, int(arg['result']), 'CheckElementIsSelected->' + message)

@DecoratorHelper.FuncRecorder
def GetCurrentBrowserCookies(arg):
    '''
    GetCurrentBrowserCookies : Get current browser session and store it into global variable
            Input argu :
                storage_type - Cookie storage type to decide which cookie need to be set
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    storage_type = arg["storage_type"]
    message = ""
    ret = 1

    ##Assemble expected domain
    expected_domain = "admin." + GlobalAdapter.UrlVar._Domain_

    try:
        ##Get and check cookies in 10 times
        for retry_time in range(10):
            ##Get current browser cookies
            GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_][storage_type] = _WebDriver_.get_cookies()

            ##Check cookies content
            ##To solve cookie wrong key issue by Selenium
            for cookie in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_][storage_type]:
                ##Get wrong cookies and wait a moment to get cookies again
                if cookie.get('domain') != expected_domain:
                    time.sleep(2)
                    dumplogger.info("Get wrong cookies, get cookies again")
                    break

                elif cookie.get('expiry', None):
                    cookie['expires'] = cookie.pop('expiry')

            else:
                dumplogger.info("Get correct cookies")
                break

        dumplogger.info(GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_][storage_type])

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'GetCurrentBrowserCookies->' + message)

@DecoratorHelper.FuncRecorder
def SetBrowserCookie(arg):
    '''
    SetBrowserCookie : Set browser cookie by the value stored in global variable
            Input argu :
                storage_type - Cookie storage type to decide which cookie need to be set
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    storage_type = arg["storage_type"]
    ret = 1
    message = ""
    dumplogger.info("GlobalAdapter.CommonVar._BrowserCookie_: ")
    dumplogger.info(GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_])

    try:
        ##Set cookie for required type
        if storage_type in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
            for cookie in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_][storage_type]:
                _WebDriver_.add_cookie(cookie)
                dumplogger.info("cookie to be set: %s" % (cookie))
        else:
            dumplogger.info("Storage type not found in _BrowserCookie_, skip!!!")

    ##Handle Invalid Argument Exception
    except InvalidArgumentException:
        message = "Encounter InvalidArgumentException Error!!!"
        dumplogger.exception(message)
        ret = -1

    ##Handle Invalid Cookie Domain Exception
    except InvalidCookieDomainException:
        message = "Encounter invalidCookieDomainException Error!!!"
        dumplogger.exception(message)
        ret = -1

    ##Handle webdriver exception
    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    ##Handle Other Exception
    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'SetBrowserCookie->' + message)

@DecoratorHelper.FuncRecorder
def GetBrowserScreenShot(filename):
    '''
    GetBrowserScreenShot : Get browser screenshot
            Input argu :
                filename - screenshot filename
            Return code : N/A
    '''
    try:
        ##Save browser screenshot
        _WebDriver_.save_screenshot(filename)
        dumplogger.info("End get screenshot, file save at: %s" % (filename))

    ##Handle webdriver alert exception
    except UnexpectedAlertPresentException:
        dumplogger.exception("Encounter Popup Alert Exception!!!")

    ##Handle webdriver exception
    except WebDriverException:
        dumplogger.exception("Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error. Please check log for more informaion.")
        ret = -1

    ##Handle MaxRetryError exception
    except MaxRetryError:
        dumplogger.exception("Encounter MaxRetryError exception in post action.")

    ##Handle webdriver exception
    except:
        dumplogger.exception("Encounter Unexpected Error!!!")

def GetBrowserNetworkRequest():
    '''
    GetBrowserNetworkRequest : Get browser network requests
            Input argu : N/A
            Return code : N/A
    '''
    response_content = ""
    requestId = ""
    request_dict = {}
    log_block = ""

    try:
        browser_log = _WebDriver_.get_log('performance')
    except WebDriverException:
        dumplogger.exception("Encounter WebDriverException !!!")
        browser_log = []
    except:
        dumplogger.exception("Encounter Other exception !!!")
        browser_log = []

    for entry in browser_log:
        message = json.loads(entry['message'])['message']
        message_params = message['params']

        ##Get request id
        if message['method'] == "Network.responseReceived" and 'requestId' in message_params and 'response' in message_params:
            requestId = message_params['requestId']
            request_dict[requestId] = []
            request_dict[requestId].append("request_url : %s" % json.dumps(message_params['response']['url']))
            request_dict[requestId].append(json.dumps(message_params['response']['headers']))

            ##Get response body with requestID
            try:
                response_content = _WebDriver_.execute_cdp_cmd('Network.getResponseBody', {'requestId': requestId})

            ##Handle webdriver exception
            except WebDriverException:
                response_content = "Encounter WebDriverException, maybe this requestID has no response, requestID:%s" % (requestId)

            ##Handle Max retry exception
            except MaxRetryError:
                dumplogger.exception("Encounter MaxRetryError in post action. Stop getting request ID.")
                break

            ##Handle other exception
            except:
                dumplogger.exception("Encounter Unkown Error!!!")

            ##Append response body to requestId
            request_dict[requestId].append(json.dumps(response_content))

    ##Form the logs into a block
    for request in request_dict:
        log_block = log_block + '\n' + "requestId: " + request + '\n'
        log_block = log_block + '\n'.join(request_dict[request])

    chrome_logger.info(log_block)

    dumplogger.info("End get network request")

@DecoratorHelper.FuncRecorder
def WebInnerElementScroll(arg):
    '''
    WebInnerElementScroll : Do scroll action in inner window
            Input argu :
                locate - locate scroll bar
                direction - left / right / up / down
                distance - scroll distance
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    locate = arg["locate"]
    direction = arg["direction"]
    distance = arg["distance"]
    message = ""

    ##Find scroll bar and choose scroll left or right for some distance
    try:
        scroll = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate))))
        if direction == "right":
            _WebDriver_.execute_script("arguments[0].scrollLeft -= " + distance, scroll)
        elif direction == "left":
            _WebDriver_.execute_script("arguments[0].scrollLeft += " + distance, scroll)
        elif direction == "up":
            _WebDriver_.execute_script("arguments[0].scrollTop = -" + distance, scroll)
        elif direction == "down":
            _WebDriver_.execute_script("arguments[0].scrollTop = " + distance, scroll)

    except WebDriverException:
        message = "ncounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown Exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'WebInnerElementScroll->' + message)

@DecoratorHelper.FuncRecorder
def UploadFileWithCSS(arg):
    '''
    UploadFileWithCSS : Upload file with input structure with only "style" element
            Input argu:
                locate - input btn xpath
                action - image/file
                file_name - file name
                file_type - file type
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    locate = arg["locate"]
    action = arg["action"]
    file_name = arg["file_name"]
    file_type = arg["file_type"]

    ##Define slash and input file path
    slash = Config.dict_systemslash[Config._platform_]
    file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + action + slash + file_name + "." + file_type

    try:
        ##Different element type using different execute script
        xpath = WebDriverWait(_WebDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate))))
        _WebDriver_.execute_script("arguments[0].setAttribute('style', 'display:block')", xpath)

        ##Input file to input bar
        Input({"method": "xpath", "locate": locate, "string": file_path, "result": "1"})
        message = "Success upload %s" % (file_name)

    except UnexpectedAlertPresentException:
        message = "Encounter Popup Alert Exception."
        dumplogger.exception(message)
        ret = -1

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Fail to upload file name %s" % (file_name)
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'UploadFileWithCSS->' + message)

@DecoratorHelper.FuncRecorder
def StartRecording(arg):
    '''
    StartRecording : Start recording video for pc
            Input argu:
                N/A
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    message = ""

    ##Output folder
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Create log folder if not exist
    if os.path.exists(output_casename_folder):
        dumplogger.info(output_casename_folder + "path exist")
    else:
        command = "mkdir " + output_casename_folder
        os.system(command)
        dumplogger.info("command = " + command)

    ##Get current date time as file name (30 min timeout)
    GlobalAdapter.CommonVar._ScreenshotFilename_["pc"] = Parser._CASE_['id'] + "_" + XtFunc.GetCurrentDateTime("%Y_%m_%d_%H%M", 0, 0, 0, 1) + ".mkv"

    ##Setup pc recording command
    record_video_cmd = 'start /MIN cmd.exe @cmd /k ffmpeg -f dshow -i video="screen-capture-recorder" -r 30 -vcodec mpeg4 -vtag xvid -qscale:v 0 -s 848x480 -t 1800 '
    record_video_cmd += output_casename_folder + Config.dict_systemslash[Config._platform_] + GlobalAdapter.CommonVar._ScreenshotFilename_["pc"]

    ##Launch new command prompt
    dumplogger.info(record_video_cmd)
    os.system(record_video_cmd)
    time.sleep(3)

    ##Get process ID
    GlobalAdapter.CommonVar._ProcessID_["pc_screenshot"] = FrameWorkBase.PNameQueryProcess("cmd", str(record_video_cmd))
    dumplogger.info("pc screenshot pid: %s" % (GlobalAdapter.CommonVar._ProcessID_["pc_screenshot"]))
    if GlobalAdapter.CommonVar._ProcessID_["pc_screenshot"] == -1:
        ret = 0
        message = "Get process ID Failed!!!"
        dumplogger.info(message)

    ##Wait for driver to initial
    time.sleep(5)

    OK(ret, int(arg['result']), 'StartRecording->' + message)

@DecoratorHelper.FuncRecorder
def EndRecording(arg):
    '''
    EndRecording : End recording video for pc
            Input argu:
                N/A
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Close cmd prompt of the screenshot process
    FrameWorkBase.KillProcessByPID(GlobalAdapter.CommonVar._ProcessID_["pc_screenshot"])
    time.sleep(5)

    ##Reset global variable anyway
    GlobalAdapter.CommonVar._ProcessID_["pc_screenshot"] = ""
    GlobalAdapter.CommonVar._ScreenshotFilename_["pc"] = ""

    OK(ret, int(arg['result']), 'EndRecording')

@DecoratorHelper.FuncRecorder
def MinimizeWindow(arg):
    '''
    MinimizeWindow : Minimize browser window
            Input argu : N/A
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    message = ""

    try:
        _WebDriver_.minimize_window()

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'MinimizeWindow->' + message)

@DecoratorHelper.FuncRecorder
def MaximizeWindow(arg):
    '''
    MaximizeWindow : Maximize browser window
            Input argu : N/A
            Return code:
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    message = ""

    try:
        _WebDriver_.maximize_window()

    except WebDriverException:
        message = "Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error."
        dumplogger.exception(message)
        ret = -1

    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'MaximizeWindow->' + message)

@DecoratorHelper.FuncRecorder
def GetElementTreeAndCompare(arg):
    '''
    GetElementTreeAndCompare : Get PC Element Tree And Compare
            Input argu :
                locate - locate of xpath
            Return code : N/A
    '''
    locate = arg["locate"]

    try:
        ##Dump DOM Tree first
        html = _WebDriver_.execute_script("return document.documentElement.outerHTML")
        #dumplogger.info(html)

    except WebDriverException:
        dumplogger.exception("Encounter webdriver exception. Maybe wedriver is crashed / closed / connection error. Please check log for more informaion.")
        ret = -1

    except:
        dumplogger.exception("Encounter unknown exception. Please check log for more informaion.")
        ret = -1

    ##Find target locate in DOM tree
    if not XtFunc.ParseElementTreeAndFind({"dom_source":StringIO.StringIO(html), "target":locate}):
        dumplogger.info("Element xpath not found in web page dump of current page!!!")
