import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopeeVoucher Precondition =="
        dumplogger.info("== Setup VNPCShopeeVoucher Precondition ==")

    def test_VNPCShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-01.xml")

    def test_VNPCShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-02.xml")

    def test_VNPCShopeeVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-03.xml")

    def test_VNPCShopeeVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-04.xml")

    def test_VNPCShopeeVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-05.xml")

    def test_VNPCShopeeVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-06.xml")

    def test_VNPCShopeeVoucher07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-07.xml")

    def test_VNPCShopeeVoucher08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-08.xml")

    def test_VNPCShopeeVoucher09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-09.xml")

    def test_VNPCShopeeVoucher10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-10.xml")

    def test_VNPCShopeeVoucher11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-11.xml")

    def test_VNPCShopeeVoucher12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeeVoucher-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopeeVoucher Post condition =="
        dumplogger.info("== Setup VNPCShopeeVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
