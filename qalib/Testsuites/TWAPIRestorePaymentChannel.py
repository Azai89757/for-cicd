import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWAPIRestorePaymentChannel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIRestorePaymentChannel Precondition =="
        dumplogger.info("== Setup TWAPIRestorePaymentChannel Precondition ==")

    def test_TWAPIRestorePaymentChannel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWAPIRestorePaymentChannel-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIRestorePaymentChannel Post condition =="
        dumplogger.info("== Setup TWAPIRestorePaymentChannel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
