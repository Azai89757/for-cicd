#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminMissionMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util
import GlobalAdapter
import XtFunc

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminMissionDisplayPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignName(arg):
        '''
        InputCampaignName : Input campaign name
                Input argu :
                   campaign_name - campaign_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign_name = arg['campaign_name']

        ##Input campaign name
        name_input = Util.GetXpath({"locate":"name_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":name_input, "result": "1"})
        BaseUICore.Input({"locate":name_input, "string":campaign_name, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.InputCampaignName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectStartTime(arg):
        '''
        SelectStartTime : Select start time
                Input argu :
                    start_time - start_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg['start_time']

        ##assiagn start date value
        start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%m:%S", 0, int(start_time), 0, is_tw_time=1)

        ##Select start time
        start_time_input = Util.GetXpath({"locate":"start_time_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":start_time_input, "result": "1"})
        BaseUICore.Input({"locate":start_time_input, "string":start_datetime, "method":"xpath", "result": "1"})

        ##Click ok
        xpath = Util.GetXpath({"locate":"ok_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.SelectStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectEndTime(arg):
        '''
        SelectEndTime : Select end time
                Input argu :
                    end_time - end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = arg['end_time']

        ##assiagn end date value
        end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%m:%S", 0, int(end_time), 0, is_tw_time=1)

        ##Select end time
        end_time_input = Util.GetXpath({"locate":"end_time_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":end_time_input, "result": "1"})
        BaseUICore.Input({"locate":end_time_input, "string":end_datetime, "method":"xpath", "result": "1"})

        ##Click ok
        xpath = Util.GetXpath({"locate":"ok_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.SelectEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelecTandC(arg):
        '''
        SelecTandC : Select T and C
                Input argu :
                    tc_type - no_tc / tc_redirection_link / tc_text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tc_type = arg['tc_type']

        ##Select T and C
        xpath = Util.GetXpath({"locate":tc_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click T&C input button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.SelecTandC')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromotionId(arg):
        '''
        InputPromotionId : Input promotion id
                Input argu :
                    promotion_type - first_promotion / second_promotion / third_promotion
                    promotion_id - promotion_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg['promotion_type']
        promotion_id = arg['promotion_id']

        ##Input promotion id
        promotion_id_input = Util.GetXpath({"locate":promotion_type})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":promotion_id_input, "result": "1"})
        BaseUICore.Input({"locate":promotion_id_input, "string":promotion_id, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.InputPromotionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignPromotionIdFromShopeeVoucher(arg):
        '''
        AssignPromotionIdFromShopeeVoucher : Assign promotion id from shopee voucher
                Input argu :
                    promotion_type - first_promotion / second_promotion / third_promotion
                    promotion_id_index - promotion id index of Shopee voucher list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg['promotion_type']
        promotion_id_index = int(arg['promotion_id_index'])

        ##Assign promotion id from shopee voucher
        promotion_id = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[promotion_id_index]["id"]

        ##Input promotion id
        promotion_id_input = Util.GetXpath({"locate":promotion_type})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":promotion_id_input, "result": "1"})
        BaseUICore.Input({"locate":promotion_id_input, "string":promotion_id, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.AssignPromotionIdFromShopeeVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadVoucherImage(arg):
        '''
        UploadVoucherImage : Upload voucher image
                Input argu :
                    position - 1 / 2 / 3
                    file_name - file_name
                    file_type - file_type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        position = arg['position']
        file_name = arg['file_name']
        file_type = arg['file_type']

        ##Upload voucher image
        voucher_input = Util.GetXpath({"locate":"voucher_input"})
        voucher_input = voucher_input.replace('position_replaced', position)

        BaseUICore.UploadFileWithCSS({"locate":voucher_input, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.UploadVoucherImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadTopVisualImage(arg):
        '''
        UploadTopVisualImage : Upload top visual image
                Input argu :
                    file_name - file_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Upload top visual image
        xpath = Util.GetXpath({"locate":"top_visual_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": "png", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.UploadTopVisualImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBackground(arg):
        '''
        ChooseBackground : Choose background
                Input argu :
                    background_type - background_color / upload_background_image
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        background_type = arg['background_type']

        ##Choose background
        xpath = Util.GetXpath({"locate":background_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click background type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.ChooseBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBackgroundImage(arg):
        '''
        UploadBackgroundImage : Upload background image
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Upload top visual image
        xpath = Util.GetXpath({"locate":"background_image"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": "png", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.UploadBackgroundImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCheckoutText(arg):
        '''
        InputCheckoutText : Input checkout text
                Input argu :
                    page_type - mission_landing_page / order_successful_page
                    position - 1 / 2 / 3
                    input_text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg['page_type']
        position = arg['position']
        input_text = arg['input_text']

        ##Get input xpath
        text_input = Util.GetXpath({"locate":page_type})
        text_input = text_input.replace('position_replaced', position)

        ##Input checkout text
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":text_input, "result": "1"})
        BaseUICore.Input({"locate":text_input, "string":input_text, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.InputCheckoutText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCurrentStartTime(arg):
        '''
        SelectCurrentStartTime : Select current start time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click start time input
        xpath = Util.GetXpath({"locate":"start_time_input"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time input", "result": "1"})

        ##Click now button
        xpath = Util.GetXpath({"locate":"now_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.SelectCurrentStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPNARTitle(arg):
        '''
        InputPNARTitle : Input PN/AR title
                Input argu :
                    position - 1 / 2 / 3 / 4
                    title_type - main_title / sub_title
                    title_text - title text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        position = arg['position']
        title_type = arg['title_type']
        title_text = arg['title_text']

        ##Get PN/AR input xpath
        input_xpath = Util.GetXpath({"locate":title_type})
        input_xpath = input_xpath.replace('position_replaced', position)

        ##Input PN/AR title
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":input_xpath, "result": "1"})
        BaseUICore.Input({"locate":input_xpath, "string":title_text, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.InputPNARTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditMissionCampaign(arg):
        '''
        EditMissionCampaign : Edit mission campaign
                Input argu :
                    action - edit / enable / disable / duplicate / delete
                    campaign_name - campaign name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']
        campaign_name = arg['campaign_name']

        ##Edit mission campaign
        xpath = Util.GetXpath({"locate":action})
        xpath = xpath.replace('replaced_text', campaign_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Edit mission campaign", "result": "1"})

        ##If action is delete,need to click confirm delete
        if action == "Delete":
            xpath = Util.GetXpath({"locate":"delete_confirm"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm delete", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.EditMissionCampaign')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductLine(arg):
        '''
        ChooseProductLine : Choose product line
                Input argu :
                    product_line - product line
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_line = arg['product_line']

        ##Click product line input
        xpath = Util.GetXpath({"locate":"product_line_input"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product line input", "result": "1"})

        ##Choose product line
        xpath = Util.GetXpath({"locate":"product_line"})
        xpath = xpath.replace("replaced_text", product_line)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product line", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayPage.ChooseProductLine')


class AdminMissionComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button according to arguments
                Input argu :
                    button_type - type of button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click any type of button according to arguments
        xpath = Util.GetXpath({"locate":button_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click on button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of column according to arguments
                Input argu :
                    column_type - type of column
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_type = arg['column_type']
        input_content = arg['input_content']

        ##Input any type of column according to arguments
        input_xpath = Util.GetXpath({"locate":column_type})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":input_xpath, "result": "1"})
        BaseUICore.Input({"locate":input_xpath, "string":input_content, "method":"xpath", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionComponent.InputToColumn')


class AdminMissionDisplayButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCampaign(arg):
        '''
        ClickAddCampaign : Click add campaign
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add campaign
        xpath = Util.GetXpath({"locate":"add_campaign_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add campaign button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayButton.ClickAddCampaign')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTopBar(arg):
        '''
        ClickTopBar : Click top bar
                Input argu :
                    bar_name - bar name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bar_name = arg['bar_name']

        ##Click bar name
        xpath = Util.GetXpath({"locate":"bar_name"})
        xpath = xpath.replace('replaced_text', bar_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click top bar", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayButton.ClickTopBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToHomepage(arg):
        '''
        ClickBackToHomepage : Click back to homepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to homepage
        xpath = Util.GetXpath({"locate":"back_to_homepage"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back to homepage", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayButton.ClickBackToHomepage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save
        xpath = Util.GetXpath({"locate":"save_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMissionDisplayButton.ClickSave')
