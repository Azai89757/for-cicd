import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class FRAndroidCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRAndroidCheckOut Precondition =="
        dumplogger.info("== Setup FRAndroidCheckOut Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"fr"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_FRAndroidCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-01.xml")

    # def test_FRAndroidCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/FR/FRAndroidCheckOut-02.xml")

    def test_FRAndroidCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-03.xml")

    def test_FRAndroidCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-04.xml")

    def test_FRAndroidCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-05.xml")

    def test_FRAndroidCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-06.xml")

    def test_FRAndroidCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-07.xml")

    def test_FRAndroidCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-08.xml")

    # def test_FRAndroidCheckOut09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/FR/FRAndroidCheckOut-09.xml")

    def test_FRAndroidCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRAndroidCheckOut-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRAndroidCheckOut Post condition =="
        dumplogger.info("== Setup FRAndroidCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
