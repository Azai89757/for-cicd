import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCEscrow(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCEscrow Precondition =="
        dumplogger.info("== Setup VNPCEscrow Precondition ==")

    def test_VNPCEscrow01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-01.xml")

    def test_VNPCEscrow02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-02.xml")

    def test_VNPCEscrow03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-03.xml")

    def test_VNPCEscrow04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-04.xml")

    def test_VNPCEscrow05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-05.xml")

    def test_VNPCEscrow06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-06.xml")

    def test_VNPCEscrow07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCEscrow-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCEscrow Post condition =="
        dumplogger.info("== Setup VNPCEscrow Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
