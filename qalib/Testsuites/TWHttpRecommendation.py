import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpRecommendation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpRecommendation Precondition =="
        dumplogger.info("== Setup TWHttpRecommendation Precondition ==")

    def test_TWHttpRecommendation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-01.xml")

    def test_TWHttpRecommendation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-02.xml")

    def test_TWHttpRecommendation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-03.xml")

    def test_TWHttpRecommendation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-04.xml")

    def test_TWHttpRecommendation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-05.xml")

    def test_TWHttpRecommendation06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-06.xml")

    def test_TWHttpRecommendation07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-07.xml")

    def test_TWHttpRecommendation08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-08.xml")

    def test_TWHttpRecommendation09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-09.xml")

    def test_TWHttpRecommendation10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-10.xml")

    def test_TWHttpRecommendation11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-11.xml")

    def test_TWHttpRecommendation12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-12.xml")

    def test_TWHttpRecommendation13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-13.xml")

    def test_TWHttpRecommendation14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-14.xml")

    def test_TWHttpRecommendation15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-15.xml")

    def test_TWHttpRecommendation16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpRecommendation-16.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return

        print "== Setup TWHttpRecommendation Get condition =="
        dumplogger.info("== Setup TWHttpRecommendation Get condition ==")
