import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *

class CLAndroidOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAndroidOrder Precondition =="
        dumplogger.info("== Setup CLAndroidOrder Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"cl"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_CLAndroidOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-01.xml")

    def test_CLAndroidOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-02.xml")

    def test_CLAndroidOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-03.xml")

    def test_CLAndroidOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-04.xml")

    def test_CLAndroidOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-05.xml")

    def test_CLAndroidOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-06.xml")

    def test_CLAndroidOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-07.xml")

    def test_CLAndroidOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-08.xml")

    def test_CLAndroidOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-09.xml")

    def test_CLAndroidOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-10.xml")

    def test_CLAndroidOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLAndroidOrder-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAndroidOrder Post condition =="
        dumplogger.info("== Setup CLAndroidOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
