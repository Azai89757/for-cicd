#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 FrameWorkBase.py: FrameWorkBase function
'''

##Import common library
import getopt
import sys
import inspect
import time
import os
import re
import psutil
import subprocess
import traceback
import datetime
import cv2

##import framework library
import Config
from Config import dumplogger
import DBHelper
import GlobalAdapter

##To import Parser.py, need to load imports for all the dirs first
import LoadImports
LoadImports.AppendImportFunc()
##Import Parser after load imports
import Parser
import XtFunc
from RetryActionByCase import RetryAction

##import selenium/appium related library
from PageFactory.Web import BaseUICore
from PageFactory.Android import AndroidBaseUICore
from PageFactory.iOS import iOSBaseUICore

##Import DB library
from db import DBCommonMethod

##Set CMD color style
STD_OUTPUT_HANDLE = -11
FOREGROUND_BLACK = 0x0
#text color contains blue.
FOREGROUND_BLUE = 0x01
##text color contains green.
FOREGROUND_GREEN = 0x02
##text color contains red.
FOREGROUND_RED = 0x04
##text color is intensified.
FOREGROUND_INTENSITY = 0x08

dict_foreground_color = {   "windows":
                            {   "black":0x0,
                                "blue":0x01,
                                "green":0x02,
                                "red":0x04,
                                "white":0x07
                            },
                            "darwin":
                            {
                                "black":"\033[0m",
                                "red":"\033[091m",
                                "green":"\033[92m",
                                "yellow":"\033[93m",
                                "blue":"\033[94m"
                            }
                        }


#try:
_OPT_ = {}
opts, args = getopt.getopt(sys.argv[1:], 'E:N:P:T:S:R:HX', ["step"])

##Error handle for input parameter
if sys.argv[1:]:
    for name, value in opts:
        if '-T' in name:
            Config._CaseID_ = value
        _OPT_[name] = value
        if '--step' in name:
            Config._Step_ = True
        if '-N' in name:
            Config._BuildNo_ = value
            dumplogger.info("Build number = %s" % (Config._BuildNo_))
        if '-E' in name:
            ##lanuch emulator
            Config._UsingEmulator_ = True

            ##Always use 'TWQA_XX' as avd name
            Config._AvdName_ = value

            if Config._platform_ == 'windows':
                command = "emulator @" + value + ' -port ' + str(int(value.split('_')[-1]) * 2 + 5552)
                Config._DeviceNumber_ = int(value.split('_')[-1])
                emulator_process = subprocess.Popen(command)
            elif Config._platform_ == 'darwin' or Config._platform_ == 'linux':
                command = "cd $ANDROID_HOME/emulator; emulator @" + value
                emulator_process = subprocess.Popen(command, shell=True)
            time.sleep(90)
        if '-S' in name:
            Config._EnvType_ = value
        if '-H' in name:
            ##Using headless mode
            Config._Headless_ = True
        if '-P' in name:
            Config._TriggerCaseMode_ = value.lower()
        if '-R' in name:
            Config._RemoteSeleniumDriver_ = value
        if '-X' in name:
            Config._EnableProxy_ = True


'''
except:
    print "Usage:"
    print "QATest.py -T   [Loctool,Encode,Wow,D3]"
    print "               # Loctool -> Automation export table from Loctool"
    print "               # Encode  -> Automation check big5 string in txt"
    print "               # Wow     -> Automation spawn test for wow"
    print "               # D3      -> Automation spawn test for D3"
    exit(0)
'''

''''
try:
    #Check DBserver exist or not
    Config._LogDB_ = 1
    loger = httplib.HTTPConnection(Config._LogServerIP_, Config._LogServerPort_, timeout=Config._LogServerTimeout_)
    loger.request("GET", Config._LogServerPath_ + "?CreateTable=1&Platform=" + Config._HostName_ )
    res = loger.getresponse()
#   print res.status, res.reason
    data = res.read()
#   print data
    if "created" in data:
        Config._LogDB_ = 1
    else:
        Config._LogDB_ = 0
except:
    Config._LogDB_ = 0

if Config._LogDB_:
    print "DataBase[%s] connected.." %Config._LogServerIP_
else:
    print "DataBase[%s] cannot connect.." %Config._LogServerIP_
'''

def Caller():
    frame = inspect.currentframe()
    frame = frame.f_back.f_back.f_back
    code = frame.f_code
    return code.co_name

def set_cmd_text_color(color):
    ''' set_cmd_text_color : Set cmd text color to default color
            Input argu :
                color - color to be set
            Note : None
    '''

    if Config._platform_ == "windows":
        import ctypes
        handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
        if color == "red":
            ##FOREGROUND_INTENSITY
            bool = ctypes.windll.kernel32.SetConsoleTextAttribute(handle, dict_foreground_color[Config._platform_][color] | FOREGROUND_INTENSITY)
        else:
            #print "----"
            #print FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE
            #print color
            #print Config._platform_
            #print dict_foreground_color[Config._platform_][color]
            #print "===="
            bool = ctypes.windll.kernel32.SetConsoleTextAttribute(handle, dict_foreground_color[Config._platform_][color])
        return bool

    ##For mac change font color
    elif Config._platform_ == "darwin":
        ##Default color in mac
        #print "\033[0m"

        ##Red font
        #print "033[91m"

        if color == "red":
            print "\033[91m"
        else:
            print "\033[0m"

def OK(src, dst, desc=None):
    ''' OK : Framework handle case result core function
        Input argu :
            N/A
        Note : None
    '''
    ##Variable that determine function has retried or not
    Config._CaseRetried_ = 0

    #set_cmd_text_color(FOREGROUND_RED | FOREGROUND_INTENSITY)
    #print "AAAAAAAAAAAAAA"
    #set_cmd_text_color(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE)

    ##Save exception msg for SQTP log data when case fail
    exception_type = sys.exc_info()[0]
    exception_value = sys.exc_info()[1]
    if str(exception_value) != "None":
        ##If there has exception and the value is not "None", then save to exception_msg
        ##exception_stored = 1 means current function have exception msg
        GlobalAdapter.CommonVar._FailedCaseInfo_["exception_msg"] = str(exception_type.__name__) + " : " + str(exception_value).split("\n")[0]
        exception_stored = 1
    else:
        exception_stored = 0

    oktime = time.time() - Parser._CASE_['oktime']

    ##For rerun process, output case folder will be different
    if Config._RerunList_ and Parser._CASE_['id']:
        output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Parser._CASE_['id'].split("-")[0] + "_rerun" + Config.dict_systemslash[Config._platform_]
    else:
        output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    txt = ''
    retry_status = 1
    if src == dst:
        ##If case had failed, status will be 1
        ##Then, if retry action passed, assign failskip = 0 to prevent steps being skipped
        if Parser._CASE_['status'] == 1 and Parser._CASE_['failskip'] == 1:
            txt = "[Pass. Retry %s at %.2f sec]" % (str(desc), oktime)

            ##Do not skip steps if case passed
            Parser._CASE_['failskip'] = 0
        ##Case had not failed and execution time is 0
        elif Parser._CASE_['oktime'] == 0:
            txt = "[Pass. %s]" % (str(desc))
        else:
            #print "[ok. %s at %.2f sec]" % (str(desc), oktime)
            txt = "[Pass. %s at %.2f sec]" % (str(desc), oktime)
    else:
        ##For test case skip(reduce cycle time), if test action fail
        Parser._CASE_['failskip'] = 1

        ##If has not failed, retry once
        if Parser._CASE_['status'] == 0:
            dumplogger.info("First execution time: %.2f" % (oktime))
            Config._CaseRetried_ = 1

            ##Set _CASE_['status'] to 1 for failed case
            Parser._CASE_['status'] = 1

            ##Retry failed steps
            try:
                ##inspect.stack()[2] is last called function
                frame = inspect.stack()[2]
                dumplogger.info("Last frame: %s" % str(frame))
                ##If retry pass, quit the rest of steps
                retry_status = RetryAction(frame)

                ##Not ok fixed, status = 0
                Parser._CASE_['status'] = 0
                oktime = time.time() - Parser._CASE_['oktime']

                dumplogger.info("Retry action %s passed!!!" % (str(desc)))
                dumplogger.info("Retry pass execution time: %.2f" % (oktime))

                ##Initial exception msg for SQTP log data when case retry pass
                GlobalAdapter.CommonVar._FailedCaseInfo_["exception_msg"] = ""
            except:
                oktime = time.time() - Parser._CASE_['oktime']
                dumplogger.exception("Fail to retry step again!!!")
                dumplogger.info("Retry failed execution time: %.2f" % (oktime))

                ##Set red color in cmd when case fail
                #set_cmd_text_color(FOREGROUND_RED | FOREGROUND_INTENSITY)
                set_cmd_text_color("red")

                ##Get line number
                global line_number

                ##Handle if line number is null, set value is 0
                if not line_number:
                    line_number = 0
                dumplogger.error("line_number = %d" % (line_number))

                #txt="\t[NOT ok : " + str(src) + " != " + str(dst) + " in " +Caller() + "["+ str(desc) + "]...%.2f sec]" % oktime
                txt = "\t[Fail line " + str(line_number) + " : " + str(src) + " != " + str(dst) + " in " + str(desc) + " ...%.2f sec ]" % (oktime)
                dumplogger.error(txt)
                ##if traceback has nothing to trace it will show None\n
                if traceback.format_exc() != "None\n":
                    dumplogger.error(traceback.format_exc())

                    ##Save exception msg for SQTP log data when case retry fail
                    exception_type = sys.exc_info()[0]
                    exception_value = sys.exc_info()[1]
                    if str(exception_value) != "None" and str(exception_type.__name__) != "NameError":
                        ##If there has exception and the value is not "None", then save to exception_msg
                        ##And to avoid NameError in retry func, will skip NameError too
                        GlobalAdapter.CommonVar._FailedCaseInfo_["exception_msg"] = str(exception_type.__name__) + " : " + str(exception_value).split("\n")[0]
                    elif not exception_stored:
                        ##If function failed but do not have exception msg, will clear previous data
                        GlobalAdapter.CommonVar._FailedCaseInfo_["exception_msg"] = ""

                ##Save ok msg/line number/fail function for SQTP log data when case fail
                GlobalAdapter.CommonVar._FailedCaseInfo_["ok_msg"] = txt.strip()
                GlobalAdapter.CommonVar._FailedCaseInfo_["line_number"] = line_number
                GlobalAdapter.CommonVar._FailedCaseInfo_["fail_func"] = Parser._CASE_['funcname']
                GlobalAdapter.CommonVar._FailedCaseInfo_["fail_time"] = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')

                ##Save 3 fail pre-steps(including current fail function) for SQTP log data when case fail
                try:
                    ##At most 2 SleepTime in 3 functions, so the range is (-1, -6)
                    for pre_steps in range(-1, -6, -1):
                        step = GlobalAdapter.CommonVar._XMLStack_[pre_steps].split(".", 1)[1].split("(")[0]
                        ##Pass SleepTime for steps
                        if step != "SleepTime":
                            GlobalAdapter.CommonVar._FailedCaseInfo_["step_dependency"].insert(0, step)
                        ##Only get 3 pre-steps
                        if len(GlobalAdapter.CommonVar._FailedCaseInfo_["step_dependency"]) == 3:
                            break
                ##Handling index error because there probably may not have enough 3 steps
                except IndexError:
                    dumplogger.info("There's no more pre-steps to save to DB!")

                ##Save api response for SQTP log data when case fail(only when fail at api function)
                if GlobalAdapter.CommonVar._FailedCaseInfo_["inline_func"] in ("CheckAPIResponseCode", "CheckAPIResponseText"):
                    GlobalAdapter.CommonVar._FailedCaseInfo_["api_response"] = str(GlobalAdapter.APIVar._APIResponse_)

                GlobalAdapter.CommonVar._FailedCaseInfo_["api_url"] = GlobalAdapter.APIVar._HttpUrl_

                ##Take screenshot and save Android debug log when case fail
                dumplogger.info("Config._TestCaseType_ = %s" % (Config._TestCaseType_))
                dumplogger.info("Parser._CASE_['id'] = %s" % (Parser._CASE_['id']))

                ##Save log when fail case has OrderID
                if GlobalAdapter.OrderE2EVar._OrderID_:
                    dumplogger.info("Fail case OrderID = %s" % (GlobalAdapter.OrderE2EVar._OrderID_))

                ##Save log with Testlink ID when fail
                if Parser._CASE_['id'] in Config._TestLinkID_:
                    dumplogger.info("TestLink ID is : %s" % (Config._TestLinkID_[Parser._CASE_['id']]))
                else:
                    dumplogger.info("NO TestLink ID")

                ##If output folder not exist, created !!
                if os.path.exists(output_casename_folder):
                    dumplogger.info(Config._OutputFolderPath_ + " -> path exist")
                else:
                    if Config._platform_ is "darwin":
                        command = "cd " + Config._OutputFolderPath_ + " && mkdir " + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]
                    else:
                        command = "mkdir " + output_casename_folder

                    dumplogger.info("command = " + command)
                    os.system(command)

                ##If get Test case id, will take screenshot(Prevent FrameWorkBase fail in TestCase setting file) and save Android debug log
                if Parser._CASE_['id']:
                    img_path = ""

                    try:
                        ##Save by Testcase type and id for windows based on trigger mode, remote selenium driver and headless mode will skip
                        if not Config._RemoteSeleniumDriver_ and not Config._Headless_ and Config._TestCasePlatform_.lower() not in ('spex','http'):
                            ##Import lib to save screen shot
                            ##If OS type is Windows-like, import ImageGrab from pyscreenshot library to execute screen shot
                            if Config._platform_ == 'windows' or Config._platform_ == 'darwin':
                                from PIL import ImageGrab
                            ##If OS type is Unix-like, import ImageGrab from PIL library to execute screen shot
                            elif Config._platform_ == 'linux' or Config._platform_ == 'linux2' or Config._platform_ == 'darwin':
                                import pyscreenshot as ImageGrab

                            img = ImageGrab.grab()
                            img.save(output_casename_folder + Parser._CASE_['id'] + "_win.png")
                            dumplogger.info("Case fail - Windows take screenshot success")
                    except IOError:
                        dumplogger.exception("Encounter IOError!!! screen grab failed")

                    ##Save chrome screenshot and browser network log
                    if BaseUICore._WebDriver_:
                        BaseUICore.GetBrowserScreenShot(output_casename_folder + Parser._CASE_['id'] + "_chrome.png")
                        # BaseUICore.GetBrowserNetworkRequest(output_casename_folder + Parser._CASE_['id'] + "_chrome.log")
                        dumplogger.info("Case fail - Chrome take screenshot success")

                        ##Save chrome image path for SQTP log data
                        if "Android" not in Parser._CASE_['funcname'] and "UnexpectedAlertPresentException" not in GlobalAdapter.CommonVar._FailedCaseInfo_["exception_msg"]:
                            img_path = "\\\\DS07\\1_Department\\Marketplace - QA\\AutoLogs" + output_casename_folder.split("QATest_Output")[1] + Parser._CASE_['id'] + "_chrome.png"

                        ##Stop pc video recording
                        if GlobalAdapter.CommonVar._ProcessID_["pc_screenshot"]:
                            BaseUICore.EndRecording({"result": "1"})
                    else:
                        dumplogger.info("Case fail - Web driver not initial, couldn't take screenshot")

                    ##Save by Testcase type and id for android
                    if AndroidBaseUICore._AndroidDriver_:
                        AndroidBaseUICore._AndroidDriver_.get_screenshot_as_file(output_casename_folder + Parser._CASE_['id'] + "_android.png")
                        ##Output Android row image
                        XtFunc.OutputAndroidRowImage({"path":output_casename_folder})
                        dumplogger.info("Case fail - Android driver take screenshot success")

                        ##Save android image path for SQTP log data
                        if "Android" in Parser._CASE_['funcname']:
                            img_path = "\\\\DS07\\1_Department\\Marketplace - QA\\AutoLogs" + output_casename_folder.split("QATest_Output")[1] + Parser._CASE_['id'] + "_android.png"

                        ##Get android network interface
                        #AndroidBaseUICore.AndroidGetNetworkInfo()

                        ##Save Android debug log
                        command = "adb -s " + Config._DeviceID_ + " logcat system_process:I com.shopee:D -t \"" + \
                            Config._CaseStartTime_ + "\" > " + output_casename_folder + Parser._CASE_['id'] + "_android_debuglog.log"
                        dumplogger.info("command = " + command)
                        os.system(command)

                        ##Stop android video recording
                        if GlobalAdapter.CommonVar._ProcessID_["android_screenshot"]:
                            AndroidBaseUICore.AndroidEndRecording({"result": "1"})
                    else:
                        dumplogger.info("Case fail - Android driver not initial, couldn't take screenshot")

                    if iOSBaseUICore._iOSDriver_:
                        iOSBaseUICore._iOSDriver_.get_screenshot_as_file(output_casename_folder + Parser._CASE_['id'] + "_ios.png")
                        iOSBaseUICore._iOSDriver_.dump_source(output_casename_folder + Parser._CASE_['id'] + "_view_tree.xml", "xml")
                        dumplogger.info("Case fail - iOS driver take screenshot success")
                    else:
                        dumplogger.info("Case fail - iOS driver not initial, couldn't take screenshot")

                    ##Handle MatchImgByOpenCV function screenshot by platform
                    if Parser._CASE_['funcname'] == "MatchImgByOpenCV" and Config._TestCasePlatform_ == "Android":
                        img_path = "\\\\DS07\\1_Department\\Marketplace - QA\\AutoLogs" + output_casename_folder.split("QATest_Output")[1] + Parser._CASE_['id'] + "_android.png"
                    elif Parser._CASE_['funcname'] == "MatchImgByOpenCV" and Config._TestCasePlatform_ == "PC":
                        img_path = "\\\\DS07\\1_Department\\Marketplace - QA\\AutoLogs" + output_casename_folder.split("QATest_Output")[1] + Parser._CASE_['id'] + "_chrome.png"
                    GlobalAdapter.CommonVar._FailedCaseInfo_["screenshot_path"] = img_path

                else:
                    ##Test case id not found
                    dumplogger.info("Case fail - Image save fail !!!! -> FrameWorkBase error !!!!")
                    print "Image save fail !!!! -> FrameWorkBase error !!!!"

    if BaseUICore._WebDriver_:
        BaseUICore.GetBrowserNetworkRequest()

    ##If description contains function which calls in xml, print ok and not ok message with text alignment left
    if Parser._CASE_['funcname'] in str(desc) and txt:
        print txt + '\n'
    elif txt:
        ##otherwise print ok and not ok message with tab
        print "\t" + txt

    ##Set color to default color in cmd
    #set_cmd_text_color(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE)
    set_cmd_text_color("white")

    if not Parser._CASE_['txt']:
        Parser._CASE_['txt'] = txt
    else:
        Parser._CASE_['txt'] = str(Parser._CASE_['txt']) + "\n" + txt

    ##Case failed, quit to stop remaining step from running
    ##Or retry xml function passed, no need to execute remaining steps
    if Parser._CASE_['failskip'] == 1 or retry_status == 0:
        quit()

def FilterCaseRegionAndPlatform(testcasetype):
    ''' FilterCaseRegionAndPlatform : Filter region / platform / feature name from trggered command
            Input argu :
                testcasetype - test case type (Ex. TWPCTest)
            Note : None
    '''
    platform = ""
    platform_group = ["PC", "Android", "iOS", "Http", "Spex", "API"]
    region_group = ["TW", "SG", "MY", "ID", "PH", "TH", "VN", "UT", "MX", "BR", "CO", "CL", "PL", "ES", "IN", "FR", "AR"]

    ##Filter case platform
    for each_platform in platform_group:
        if each_platform in testcasetype[2:]:
            dumplogger.info("Config._TestCaseType_ = %s" % (testcasetype))
            dumplogger.info("platform = %s" % (each_platform))
            platform = each_platform
            break
        else:
            #dumplogger.error("Config._TestCaseType_ = %s" % (testcasetype))
            platform = "Please Check your test case platform is correct !!!!"

    ##Filter case region
    region = testcasetype[0:2]
    if region in region_group:
        dumplogger.info("Config._TestCaseType_ = %s" % (testcasetype))
        dumplogger.info("region = %s" % (region))
    else:
        dumplogger.error("Config._TestCaseType_ = %s" % (testcasetype))
        dumplogger.info("region = %s" % (region))
        dumplogger.error("Please Check your test case region is correct !!!!")
        print "Please Check your test case region is correct !!!!"
        region = "Please Check your test case region is correct !!!!"

    ##Filter test case feature name
    if platform in platform_group and region in region_group:
        feature = testcasetype.split(region + platform)[-1]
        dumplogger.info("feature = %s" % (feature))
    else:
        feature = ""
        dumplogger.info("feature = %s" % (feature))
        dumplogger.error("Please Check your test case region is correct !!!!")
        print "Please Check your test case region/platform is correct !!!!"

    return region, platform, feature

def FilterCaseID(cid):
    ''' FilterCaseID : Filter case ID from xml tag
            Input argu :
                cid - test case id get from xml tag
            Note : None
    '''
    global _OPT_

    if '-T' not in _OPT_:
        return cid
    else:
        if ',' in _OPT_['-T']:
            cases = _OPT_['-T'].split(",")
            for case in cases:
                if case in cid:
                    if case.find("-"):
                        Config._TestCaseType_ = case.split("-")[0]
                    else:
                        Config._TestCaseType_ = case

                    dumplogger.info(Config._TestCaseType_)

                    return cid

        elif '[' in _OPT_['-T']:
            prefix = _OPT_['-T'].split("[")
            match = re.search('\[(\d*)-(\d*)\]', _OPT_['-T'], re.IGNORECASE)
            lowerBoundary = match.group(1)
            upperBoundary = match.group(2)

            if lowerBoundary != '' and upperBoundary == '':
                for i in xrange(len(lowerBoundary)):
                    upperBoundary += str(9)
                    i += 1

            elif lowerBoundary == '' and upperBoundary != '':
                for i in xrange(len(upperBoundary)):
                    lowerBoundary += str(0)
                    i += 1

            for i in xrange(int(lowerBoundary), int(upperBoundary) + 1):
                case = prefix[0] + str(i)
                if case in cid:
                    return cid

        else:
            ##Default Action
            cases = _OPT_['-T'].split(",")
            for case in cases:
                if case in cid:
                    Config._TestCaseType_ = case.split("-")[0]
                    dumplogger.info(Config._TestCaseType_)
                    ##Filter case region
                    #Config._TestCaseRegion_ = FilterCaseRegion(Config._TestCaseType_)
                    return cid
                elif case.find("-"):
                    Config._TestCaseType_ = case.split("-")[0]
                    dumplogger.info(Config._TestCaseType_)
                    ##Filter case region
                    #Config._TestCaseRegion_ = FilterCaseRegion(Config._TestCaseType_)
                    #return cid
                else:
                    Config._TestCaseType_ = case
                    dumplogger.info(Config._TestCaseType_)
                    ##Filter case region
                    # Config._TestCaseRegion_ = FilterCaseRegion(Config._TestCaseType_)
                    #return cid
    return

##Currently not in use
# def LogDB(Platform, BuildNo, CaseNo, Status, CaseRes):
#     ''' LogDB : Send log data to DB
#             Input argu :
#                 N/A
#             Note : None
#     '''
#     if Config._LogDB_:
#         #print "\t-DB[%s,%s,%s,%s,%s]"% (Platform ,BuildNo ,CaseNo ,Status ,CaseRes)
#         params = urllib.urlencode({'Platform': Platform, 'BuildNo':BuildNo, 'CaseNo':CaseNo, 'Status':Status, 'CaseRes':CaseRes, 'module':'Osprey'})
#         headers = {"Content-type":"application/x-www-form-urlencoded", "Accept":"text/plain"}
#         loger = httplib.HTTPConnection(Config._LogServerIP_, Config._LogServerPort_, timeout=Config._LogServerTimeout_)
#         loger.request("POST", Config._LogServerPath_, params, headers)
#         res = loger.getresponse()
#         #print res.status, res.reason
#         #data = res.read()
#         #print data
#     else:
#         print "\t-NoDB[%s, %s, %s, %s, %s]" % (Platform, BuildNo, CaseNo, Status, CaseRes)


def SaveDailyResultToDB(stroed_procedure, acid, create_time, exc_time, build_version, result, valid):
    ''' SaveDailyResultToDB : Send automation daily run result to DB
            Input argu :
                N/A
            Note : None
    '''
    DBCommonMethod.InitialSQL({"db_name":"qa", "result": "1"})

    ##Sleep for avoid from DB exception - 1213 - Deadlock found when trying to get lock; try restarting transaction
    ##This exception is happened when Transaction A and Transaction B insert data into db simultaneously
    time.sleep(1)

    ##Initial MySQL controller
    dbc = DBHelper.MySQLController(Config._DBConfig_, "twqa_auto_db")

    ##Call stored procedure to log automation result
    dbc.CallProc(stroed_procedure, [acid, create_time, exc_time, build_version, result, valid])

    ##Deinitial Sql
    DBCommonMethod.DeInitialSQL({"result": "1"})

    ##New one, will replace the old one soon after SQTP function goes live.
    ##Initial Sql
    DBCommonMethod.InitialSQL({"db_name":"qa", "result": "1"})

    ##Sleep for avoid from DB exception - 1213 - Deadlock found when trying to get lock; try restarting transaction
    ##This exception is happened when Transaction A and Transaction B insert data into db simultaneously
    time.sleep(1)

    ##Initial MySQL controller
    dbc = DBHelper.MySQLController(Config._DBConfig_, "twqa_sqtp_db")

    ##Call stored procedure to log automation result
    ##This logic will be modified later, since _CASE_['status'] = 0 stands for pass and _CASE_['status'] = 1 stands for fail, which is oppsite to True and False
    try:
        if int(result) == 0:
            dbc.CallProc(stroed_procedure, [acid, Config._TestCaseRegion_, Config._TestCasePlatform_, Config._TestCaseFeature_, create_time, Config._TestCaseDesc_[acid], Config._TestLinkID_[acid], exc_time, build_version, 1, valid])
        else:
            dbc.CallProc(stroed_procedure, [acid, Config._TestCaseRegion_, Config._TestCasePlatform_, Config._TestCaseFeature_, create_time, Config._TestCaseDesc_[acid], Config._TestLinkID_[acid], exc_time, build_version, 0, valid])

    ##Error handle to avoid exception
    except KeyError:
        dumplogger.exception("Encounter key error, this will only affect data record on SQTP daily analysis page.")
    except:
        dumplogger.exception("Encounter unknown error, this will only affect data record on SQTP daily analysis page.")

    DBCommonMethod.DeInitialSQL({"result": "1"})


def SaveDailyFailLogToDB(stroed_procedure, acid, create_time, build_version, result, valid, region, platform, feature, line_number, fail_function, step_dependency, screenshot_path, ok_msg, exception_msg, issue_report_note, log_file_path, api_response, api_url):
    ''' SaveDailyFailLogToDB : Send automation daily run fail log collection to DB
            Input argu :
                N/A
            Note : None
    '''
    DBCommonMethod.InitialSQL({"db_name":"qa", "result": "1"})

    ##Sleep for avoid from DB exception - 1213 - Deadlock found when trying to get lock; try restarting transaction
    ##This exception is happened when Transaction A and Transaction B insert data into db simultaneously
    time.sleep(1)

    ##Initial MySQL controller
    dbc = DBHelper.MySQLController(Config._DBConfig_, "twqa_auto_db")

    ##Call stored procedure to log automation result
    dbc.CallProc(stroed_procedure, [acid, create_time, build_version, result, valid, region, platform, feature, line_number, fail_function, step_dependency, screenshot_path, ok_msg, exception_msg, issue_report_note, log_file_path, api_response, api_url])
    DBCommonMethod.DeInitialSQL({"result": "1"})


def XMLParser(xml_path):
    '''
    XMLParser : parser xml from QATestData
            Input argu :
                N/A
            Note : None
    '''

    ##Check xml file exist
    cur_xml_path = Config._CurDataDIR_ + xml_path
    if not os.path.isfile(cur_xml_path):
        print "Please check your test case file(xml) exist !!!!"
        dumplogger.error(cur_xml_path)

    else:
        dumplogger.info(cur_xml_path)

        ##Declare glablal var to stored line_number in xml
        global line_number
        ##Didn't forget to replace new ElementTree.py to system python27 folder
        import xml.etree.ElementTree as ET

        ##Get xml file and load root tag, prevent load xml fail caused by xml tag
        try:
            tree = ET.parse(cur_xml_path)
        except ET.ParseError:
            print "ParseError - Please check your XML file !!!!"
            print traceback.print_exc()
            dumplogger.exception("ParseError - Please check your XML file !!!!")
        except:
            print "ParseError - Please check your XML file !!!!"
            print traceback.print_exc()
            dumplogger.exception("Error - Please check your XML file !!!!")

        root = tree.getroot()
        ##Call XMLDescriptionParser to store Description text
        XMLDescriptionParser(tree)

        ##Sequence to load root tag from xml
        for child_of_root in root:
            for temp in child_of_root:

                ##Get line number
                line_number = temp.lineNum

                ##Detect id exist in atrribute
                if "id" in temp.attrib:

                    ##Call start_element
                    Parser.start_element(temp.tag, temp.attrib)

                    ##Get all attributes in xml tag
                    for temp_sub in temp:

                        ##print temp_sub.lineNum, temp_sub.tag, temp_sub.attrib
                        ##Get line number
                        line_number = temp_sub.lineNum

                        ##Call end_element to finish action by each line
                        Parser.end_element(temp_sub.tag, temp_sub.attrib)

                    else:
                        ##Call end_element to finish action by each line
                        Parser.end_element("TestCase")


def XMLDescriptionParser(root):
    '''
    XMLDescriptionParser : parser description from xml
            Input argu :
                N/A
            Note : None
    '''

    for test_case in root.iterfind(".//*TestCase"):
        auto_id = test_case.attrib["id"]
        ##Save xml each testcase testlink id
        for case in test_case.iterfind("Description"):
            ##Get Description text from xml
            match = re.search(r"(\[Testlink\]\s(ART|API)\-)(\d+)(\s:\s)(.+)", case.text, re.MULTILINE | re.IGNORECASE)
            if match:
                Config._TestLinkID_.update({auto_id: str(match.group(2)) + "-" + str(match.group(3))})
                Config._TestCaseDesc_.update({auto_id: str(auto_id) + " - " + str(match.group(5))})
            else:
                dumplogger.info("No TestLink ID or not TestCase for %s" % (auto_id))
                Config._TestLinkID_.update({auto_id: str(auto_id) + " => N/A"})
                Config._TestCaseDesc_.update({auto_id: str(auto_id) + " => No description found, please check <Description> section in xml first."})

    dumplogger.info("xml testlink id is : %s" % (Config._TestLinkID_))
    dumplogger.info("xml testlink desc is : %s" % (Config._TestCaseDesc_))


def isMyCase(casePrefix):
    '''isMyCase: Identfy if this case is mine
    Return code:  True - My case!
                  False - Not my case
    Note: This is work around for Python unittest defect. (Go throught every def in test class)'''

    global _OPT_
    #print ("casePrefix is %s" % (casePrefix))
    tmp = str(casePrefix).split('.')
    tmp2 = tmp[3].split('_')
    #print tmp2[1]
    match = re.match(r"([a-z]+)([0-9]+)", tmp2[1], re.IGNORECASE)
    items = match.groups()

    myCaseID = items[0] + "-" + items[1]
    cases = _OPT_['-T'].split(",")

    for case in cases:
        #print "case=%s, myCaseID=%s" %(case,myCaseID)
        if myCaseID in case or case in myCaseID:
            return True

    return False

def PNameQueryProcess(name=None, command_line=None):
    ''' PNameQueryProcess : Use process name/command line to query process info
            Input argu :
                name - process name
                command_line - command line
            Return code :
                pid_list - process id list
                -1 - exception
            Note : Scenario 1 : Compare command line
                   Scenario 2 : Compare name
                   Scenario 3 : Compare name both command line
    '''
    pid_list = []

    dumplogger.info("Start query process with given name: %s , command: %s" % (name, command_line))

    ##Filter dump_cmd first
    if command_line:
        if Config._platform_ == "windows":
            ##windows cmd comes from start /MIN cmd.exe
            regex = r"(start|START)\s(\/[A-Z]*)\s(...*)"
            match = re.search(regex, command_line)
            dump_cmd = match.group(3)
        elif Config._platform_ == "darwin":
            ##mac cmd comes from xcodebuild
            regex = r"(xcodebuild)\s(\>.+)\s(2>\S+\s)([^&]+\w)"
            match = re.search(regex, command_line)
            if match:
                dump_cmd = " ".join([match.group(1), match.group(4)])
            else:
                dumplogger.info("Cannot get any match result in command line with re")
                return -1
        ##Remove '"' in cmdline
        if '"' in dump_cmd:
            dump_cmd = dump_cmd.replace('"','')

        dumplogger.info("New command line: -> %s" % (dump_cmd))
    else:
        dumplogger.info("No command line")

    ##iter all process
    for proc in psutil.process_iter():
        try:
            ##load process info as dict with key pid, name, cmdline
            proc_info = proc.as_dict(attrs=['pid', 'name', 'cmdline'])

            ##Assemble process dump_cmd instead of list
            if proc_info['cmdline']:
                ##Join cmd list to string
                proc_info['cmdline'] = " ".join(proc_info['cmdline'])
            else:
                ##Assign null to prevent cmdline be None for later usage
                proc_info['cmdline'] = ""

            ##Detect matched process name
            ##Scenario 1 : Only compare command line
            if command_line and not name:
                if dump_cmd in proc_info['cmdline']:
                    dumplogger.info("Get process: %s" % (proc_info))
                    pid_list.append(proc_info['pid'])

            ##Scenario 2 : Only compare name
            elif name and not command_line:
                if name.lower() in proc_info['name'].lower():
                    pid_list.append(proc_info['pid'])

            ##Scenario 3 : Compare name and then compare command line
            elif name and command_line:
                ##Detect process name matched
                if name.lower() in proc_info['name'].lower():
                    ##Check if process cmdline not none
                    if proc_info['cmdline']:
                        ##Detect process cmd with command
                        if dump_cmd in proc_info['cmdline']:
                            dumplogger.info("Process Name->Process ID : %s->%d, info: %s" % (name, proc_info['pid'], proc_info))
                            pid_list.append(proc_info['pid'])
                        else:
                            dumplogger.info("No matched process with name: %s and command: %s" % (name, dump_cmd))
                else:
                    dumplogger.info("No matched process with name: %s or command: %s" % (name, dump_cmd))
            else:
                dumplogger.info("Please at least use name or command line to query pid!!!!!!")

        ##Exception Handle
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            dumplogger.exception("Can't find \"%s\" process" % (name))
            return -1

    return pid_list

def KillProcessByPID(pid_list):
    '''KillProcessByPID : kill process by process id
            Input argu :
                pid_list - process id list
            Return code :

            Note : None
    '''

    ##Kill process by process ID list
    for pid in pid_list:
        if Config._platform_ == 'windows':
            cmd = "taskkill /pid " + str(pid) + " /t /f"
        elif Config._platform_ == 'darwin':
            cmd = "kill -9 " + str(pid)

        dumplogger.info("killing task -> command: %s" % (cmd))
        os.system(cmd)
