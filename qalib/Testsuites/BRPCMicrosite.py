import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCMicrosite Precondition =="
        dumplogger.info("== Setup BRPCMicrosite Precondition ==")

    def test_BRPCMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-01.xml")

    def test_BRPCMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-02.xml")

    def test_BRPCMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-03.xml")

    def test_BRPCMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-04.xml")

    def test_BRPCMicrosite05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-05.xml")

    def test_BRPCMicrosite06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-06.xml")

    def test_BRPCMicrosite07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMicrosite-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCMicrosite Post condition =="
        dumplogger.info("== Setup BRPCMicrosite Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
