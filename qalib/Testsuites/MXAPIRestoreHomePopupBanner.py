import unittest
from Config import dumplogger
from FrameWorkBase import *


class MXAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup MXAPIRestoreHomePopupBanner Precondition ==")

    def test_MXAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MXAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup MXAPIRestoreHomePopupBanner Post condition ==")
