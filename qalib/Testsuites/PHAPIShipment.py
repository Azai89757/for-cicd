import unittest
from Config import dumplogger
from FrameWorkBase import *


class PHAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAPIShipment Precondition =="
        dumplogger.info("== Setup PHAPIShipment Precondition ==")

    def test_PHAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/PHAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAPIShipment Post condition =="
        dumplogger.info("== Setup PHAPIShipment Post condition ==")
