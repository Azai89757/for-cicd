import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCNotification(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print("== Setup FRPCNotification Precondition ==")
        dumplogger.info("== Setup FRPCNotification Precondition ==")

    def test_FRPCNotification01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-01.xml")

    def test_FRPCNotification02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-02.xml")

    def test_FRPCNotification03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-03.xml")

    def test_FRPCNotification04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-04.xml")

    def test_FRPCNotification05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-05.xml")

    def test_FRPCNotification06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-06.xml")

    def test_FRPCNotification07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-07.xml")

    def test_FRPCNotification08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-08.xml")

    def test_FRPCNotification09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-09.xml")

    def test_FRPCNotification10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCNotification-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print("== Setup FRPCNotification Post condition ==")
        dumplogger.info("== Setup FRPCNotification Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
