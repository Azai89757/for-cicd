import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDHttpCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpCart Precondition =="
        dumplogger.info("== Setup IDHttpCart Precondition ==")

    def test_IDHttpCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-01.xml")

    def test_IDHttpCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-02.xml")

    def test_IDHttpCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-03.xml")

    def test_IDHttpCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-04.xml")

    def test_IDHttpCart05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-05.xml")

    def test_IDHttpCart06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-06.xml")

    def test_IDHttpCart07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-07.xml")

    def test_IDHttpCart08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-08.xml")

    def test_IDHttpCart09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-09.xml")

    def test_IDHttpCart10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-10.xml")

    def test_IDHttpCart11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-11.xml")

    def test_IDHttpCart12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-12.xml")

    def test_IDHttpCart13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpCart-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpCart Post condition =="
        dumplogger.info("== Setup IDHttpCart Post condition ==")
