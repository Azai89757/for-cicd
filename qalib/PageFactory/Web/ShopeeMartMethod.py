#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ShopeeMartMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
import GlobalAdapter
import Config
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic
import CommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class ShopeeMartHomePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHorizontalCollection(arg):
        '''
        CheckHorizontalCollection : Check horizontal collection
                Input argu :
                    page_type - homepage / deals_tab
                    title - collection title
                    is_scroll - scroll collection to last or not (1 / 0)
                    image - compare image name
                    click_see_more - click see more or not (1 / 0)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        page_type = arg["page_type"]
        title = arg["title"]
        is_scroll = arg["is_scroll"]
        image = arg["image"]
        click_see_more = arg["click_see_more"]

        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking collection exist" % (retry_count))

            ##Close browser and re-open, then launch to Shopee homepage to check collection dispaly
            BaseUICore.DeInitialWebDriver({})
            BaseUICore.InitialWebDriver({"browser":"chrome","result": "1"})
            CommonMethod.ShopeeLaunchWebSite({"result": "1"})
            time.sleep(15)

            ##Initial country url
            url = "https://" + GlobalAdapter.UrlVar._Domain_

            ##Define url to launch website
            if page_type == "homepage":
                url = url + "/supermarket"
            elif page_type == "deals_tab":
                url = url + "/supermarket/deals"
            dumplogger.info("url = %s" % (url))

            ##Go to url
            BaseUICore.GotoURL({"url":url, "result": "1"})
            time.sleep(15)

            ##Move to specific collection
            xpath = Util.GetXpath({"locate":"collection_title"})
            xpath = xpath.replace("title_to_be_replaced", title)
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "product_card", "result": "1"})
                BaseUILogic.MouseScrollEvent({"x":str(GlobalAdapter.GeneralE2EVar._LocationX_), "y":str(GlobalAdapter.GeneralE2EVar._LocationY_-200), "result": "1"})

                ##Scroll to the last if needed
                if int(is_scroll):
                    ##Check scroll icon exist or not, if exist, Click switch element.
                    xpath = Util.GetXpath({"locate":"right_button"})
                    xpath = xpath.replace("title_to_be_replaced", title)
                    for scroll_time in range(15):
                        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to switch right", "result": "1"})
                            time.sleep(2)
                        else:
                            dumplogger.info("Scroll to the last horizontal collection page.")
                            break

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode": "web", "image": image, "env": Config._EnvType_, "threshold": "0.9", "expected_result": arg['result']})

            ##If image exist, then determine whether to click or rerun
            if match_result:
                time.sleep(5)
                dumplogger.info("Check collection successful at %d run!!!!" % (retry_count))

                if int(click_see_more):
                    xpath = Util.GetXpath({"locate":"see_more"})
                    xpath = xpath.replace("title_to_be_replaced", title)
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click see more.", "result": "1"})
                ret = 1
                break
            else:
                time.sleep(30)
                dumplogger.info("Failed to check horizontal collection at %d run..." % (retry_count))

        OK(ret, match_result, 'ShopeeMartHomePage.CheckHorizontalCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShopeeMartBannerExist(arg):
        '''
        CheckShopeeMartBannerExist : Check horizontal collection
                Input argu :
                    banner_type - mart_carousel / mart_skinny
                    image - compare image name
                    is_click - click image or not (1 / 0)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        banner_type = arg["banner_type"]
        image = arg["image"]
        is_click = arg["is_click"]

        ##Retry checking banner
        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close browser and re-open, then launch to Shopee mart homepage to check banner dispaly
            BaseUICore.DeInitialWebDriver("kill_browser")
            BaseUICore.InitialWebDriver({"browser":"chrome","result": "1"})
            CommonMethod.ShopeeLaunchWebSite({"result": "1"})
            time.sleep(15)

            ##Initial country url
            url = "https://" + GlobalAdapter.UrlVar._Domain_

            ##Define url to launch website
            if banner_type in ("mart_carousel", "mart_skinny"):
                url = url + "/supermarket"
            dumplogger.info("url = %s" % (url))

            ##Go to url
            BaseUICore.GotoURL({"url":url, "result": "1"})
            time.sleep(15)

            ##Move to specific collection
            if banner_type == "mart_skinny":
                CommonMethod.ScrollToSection({"section":"mart_category_section", "result": "1"})

            ##Match image
            time.sleep(3)
            match_result = XtFunc.MatchPictureImg({"mode": "web", "image": image, "env": Config._EnvType_, "threshold": "0.95", "expected_result": arg['result']})

            ##If image exist, then determine whether to click or rerun
            if match_result:
                time.sleep(5)
                dumplogger.info("Check mart banner successful at %d run!!!!" % (retry_count))

                ##Determine click or not
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env":"staging", "mode":"web", "image":image, "threshold":"0.95", "is_click":"yes", "result": "1"})

                ret = 1
                break
            else:
                time.sleep(30)
                dumplogger.info("Failed to check mart banner at %d run..." % (retry_count))

        OK(ret, match_result, 'ShopeeMartHomePage.CheckShopeeMartBannerExist')


class ShopeeMartHomePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBottomBar(arg):
        '''
        ClickBottomBar : Click bottom bar button
                Input argu :
                    button_type - button type of bottom bar (supermarket / on_sale / all_products)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        button_type = arg["button_type"]
        ret = 1

        ##Click bottom bar button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + button_type + " button.", "result": "1"})

        OK(ret, int(arg['result']), 'ShopeeMartHomePageButton.ClickBottomBar')
