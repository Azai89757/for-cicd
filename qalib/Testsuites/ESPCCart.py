import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCCart Precondition =="
        dumplogger.info("== Setup ESPCCart Precondition ==")

    def test_ESPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCCart Post condition =="
        dumplogger.info("== Setup ESPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
