#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminMicrositeMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import os
import re
import time
import datetime

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import GlobalAdapter

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminCampaignManagementPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPageTitle(arg):
        '''
        InputPageTitle : Input page title
                Input argu :
                    page_title - page title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_title = arg["page_title"]

        ##Input page title
        xpath = Util.GetXpath({"locate": "page_title"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        if page_title == "time_stamp":
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "microsite-" + str(GlobalAdapter.TrackerVar._CurUnixTime_), "result": "1"})
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputPageTitle ->' + page_title)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOptionalPageTitle(arg):
        '''
        InputOptionalPageTitle : Input optional page title
                Input argu :
                    page_title - page title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_title = arg["page_title"]

        country = {
            "mx": "en",
            "my": "ms",
            "br": "en",
            "sg": "ms",
            "co": "en",
            "cl": "en",
            "pl": "en",
            "es": "en",
            "in": "hi",
            "fr": "en",
            "vn": "en",
            "ph": "zh-Hans",
            "th": "en",
            "ar": "en",
            "id": "en"
        }

        ##Input page title
        xpath = Util.GetXpath({"locate": "optional_page_title"})
        xpath = xpath.replace("name_to_be_replaced", country[Config._TestCaseRegion_.lower()])
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        if page_title == "time_stamp":
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "microsite-" + str(GlobalAdapter.TrackerVar._CurUnixTime_), "result": "1"})
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputOptionalPageTitle ->' + page_title)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPageURL(arg):
        '''
        InputPageURL : Input page url
                Input argu :
                    page_url - page url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_url = arg["page_url"]

        ##Input page url
        xpath = Util.GetXpath({"locate": "page_url"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        if page_url == "time_stamp":
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "mx-microsite-" + str(GlobalAdapter.TrackerVar._CurUnixTime_), "result": "1"})
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputPageURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayName(arg):
        '''
        InputDisplayName : Input display name
                Input argu :
                    display_name - Display Name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        display_name = arg["display_name"]

        ##Input display name
        xpath = Util.GetXpath({"locate": "display_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputDisplayName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPosition(arg):
        '''
        InputPosition : Input voucher position
                Input argu :
                    position - voucher order position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        position = arg["position"]

        ##Input voucher order position
        xpath = Util.GetXpath({"locate": "input_position"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": position, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputPosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRibbonText(arg):
        '''
        InputRibbonText : Input Ribbon Text
                Input argu :
                    ribbon_text - Ribbon Text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ribbon_text = arg["ribbon_text"]

        ##Input ribbon rext
        xpath = Util.GetXpath({"locate": "ribbon_text"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ribbon_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputRibbonText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBoldText(arg):
        '''
        InputBoldText : Input Bold Text
                Input argu :
                    bold_text - Bold Text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bold_text = arg["bold_text"]

        ##Input bold text
        xpath = Util.GetXpath({"locate": "bold_text"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": bold_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputBoldText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLightText(arg):
        '''
        InputLightText : Input Light Text
                Input argu :
                    light_text - Light Text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        light_text = arg["light_text"]

        ##Input light text
        xpath = Util.GetXpath({"locate": "light_text"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": light_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputLightText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromotionID(arg):
        '''
        InputPromotionID : Input promotion id
                Input argu :
                    promotion_id - promotion id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_id = arg["promotion_id"]

        ##Input promotion id
        xpath = Util.GetXpath({"locate": "promotion_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputPromotionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEndTime(arg):
        '''
        InputEndTime : Input end time
                Input argu :
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = arg["end_time"]

        ##Click select time
        xpath = Util.GetXpath({"locate": "select_time"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select time", "result": "1"})

        ##Input end time
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0)
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        ##Click ok button
        xpath = Util.GetXpath({"locate": "ok_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRedirectURL(arg):
        '''
        InputRedirectURL : Input redirect URL
                Input argu :
                    redirect_url - redirect url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        redirect_url = arg["redirect_url"]

        ##Input redirect URL
        xpath = Util.GetXpath({"locate": "redirect_url"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirect_url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputRedirectURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDescription(arg):
        '''
        InputDescription : Input description
                Input argu :
                    description - description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]

        ##Input description
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignLabel(arg):
        '''
        InputCampaignLabel : Input campaign label
                Input argu :
                    campaign_label - campaign_label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign_label = arg["campaign_label"]

        ##Input campaign label
        xpath = Util.GetXpath({"locate": "campaign_label"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": campaign_label, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShareTitle(arg):
        '''
        InputShareTitle : Input share title
                Input argu :
                    share_title - share_title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        share_title = arg["share_title"]

        ##Input share title
        xpath = Util.GetXpath({"locate": "share_title"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": share_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputShareTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShareDescription(arg):
        '''
        InputShareDescription : Input share description
                Input argu :
                    share_descripton - share_descripton
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        share_descripton = arg["share_descripton"]

        ##Input share description
        xpath = Util.GetXpath({"locate": "share_descripton"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": share_descripton, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputShareDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSEOTitle(arg):
        '''
        InputSEOTitle : Input SEO title
                Input argu :
                    seo_title - SEO_title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seo_title = arg["seo_title"]

        ##Input SEO title
        xpath = Util.GetXpath({"locate": "seo_title"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": seo_title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.InputSEOTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchCampaign(arg):
        '''
        SearchCampaign : Search campaign
                Input argu :
                    type - title / id
                    vaule - page title name / page id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        vaule = arg["vaule"]

        ##Click search type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search type", "result": "1"})

        ##Input search title
        xpath = Util.GetXpath({"locate": "search_field"})
        xpath = xpath.replace('placeholder_to_be_replace',type)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": vaule, "result": "1"})

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_button"})
        xpath = xpath.replace('placeholder_to_be_replace',type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.SearchCampaign ->' + vaule)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShareImage(arg):
        '''
        UploadShareImage : Upload share image
                Input argu :
                    image_name - image file neme
                    image_type - image file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_name = arg["image_name"]
        image_type = arg["image_type"]

        xpath = Util.GetXpath({"locate": "share_image_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": image_name, "file_type": image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPage.UploadShareImage')


class AdminCampaignManagementPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectInteractionType(arg):
        '''
        SelectInteractionType : Select interaction type
            Input argu :
                type - anchorable / non_anchorable
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Select interaction type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select interaction type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.SelectInteractionType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBack(arg):
        '''
        ClickBack : Click back button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click back button
        xpath = Util.GetXpath({"locate": "back_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckboxes(arg):
        '''
        ClickCheckboxes : Click chekcboxes button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click checkboxes button
        xpath = Util.GetXpath({"locate": "checkboxes"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkboxes button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickCheckboxes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click back button
            Input argu :
                type - cancel / leave
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click back button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAction(arg):
        '''
        ClickAction : Click action button
            Input argu :
                type - edit_view / edit_info
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click action button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickAction ->' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyLink(arg):
        '''
        ClickCopyLink : Click copy link button
            Input argu :
                url_name - page_url / redirect_url / canonical_url
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        url_name = arg["url_name"]

        ##Click copy link button
        xpath = Util.GetXpath({"locate": url_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click copy link button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickCopyLink ->' + url_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoTo(arg):
        '''
        ClickGoTo : Click go to button
            Input argu :
                url_name - page_url / redirect_url / canonical_url
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        url_name = arg["url_name"]

        ##Click go to button
        xpath = Util.GetXpath({"locate": url_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickGoTo -> ' + url_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlaceholderArrow(arg):
        '''
        ClickPlaceholderArrow : Click placeholder arrow button
                Input argu :
                    arrow - up / down
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        arrow = arg["arrow"]

        ##Mouse move to placeholder field
        xpath = Util.GetXpath({"locate": "placeholder_field"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

        ##Click placeholder arrow button
        xpath = Util.GetXpath({"locate": arrow})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click placeholder arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickPlaceholderArrow -> ' + arrow)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareImageDelete(arg):
        '''
        ClickShareImageDelete : Click share image delete button
                Input argu :
                    confirm - no / yes
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        confirm = arg["confirm"]

        ##Click Click share image delete button
        xpath = Util.GetXpath({"locate": "delete"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": confirm})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignManagementPageButton.ClickShareImageDelete')


class AdminCampaignViewEditPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddComponent(arg):
        '''
        AddComponent : Add component
            Input argu :
                component_tab - widget / image / products / shop / promotions / game
                component_type - anchor tab bar / gap / back to top .....
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_tab = arg["component_tab"]
        component_type = arg["component_type"]

        ##Click component tab
        if component_tab:
            xpath = Util.GetXpath({"locate": "component_tab"})
            xpath = xpath.replace('component_tab_to_be_replace', component_tab)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click component tab", "result": "1"})

        ##Click add component type
        if component_type:
            xpath = Util.GetXpath({"locate": "add_component"})
            xpath = xpath.replace('component_type_to_be_replace', component_type)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add component type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.AddComponent ->' + component_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputComponentName(arg):
        '''
        InputComponentName : Input component name
            Input argu :
                component_name - component name
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]

        ##Input component name
        xpath = Util.GetXpath({"locate": "component_name_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": component_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputComponentName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectComponent(arg):
        '''
        SelectComponent : Select component
            Input argu :
                component_name - component name
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]

        ##Select component
        xpath = Util.GetXpath({"locate": "select_component"})
        xpath = xpath.replace('component_name_to_be_replace', component_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select component", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassEditToDuplicate(arg):
        '''
        MassEditToDuplicate : Mass edit to duplication
            Input argu :
                component_name - component name
                duplication_position - duplication position
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]
        duplication_position = arg["duplication_position"]

        ##Click check box to mass edit
        AdminCampaignViewEditPageButton.ClickMassEdit({"result": "1"})

        ##Clcik to select component
        xpath = Util.GetXpath({"locate": "component_checkbox"})
        xpath = xpath.replace('component_name_to_be_replace', component_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Clcik to select component", "result": "1"})

        ##Click duplicate button
        xpath = Util.GetXpath({"locate": "duplicate_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click duplicate button", "result": "1"})

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("position_to_be_replace", duplication_position)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.MassEditToDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectFirstScreenTrackingToggle(arg):
        '''
        SelectFirstScreenTrackingToggle : Select first screen tracking toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select first screen tracking toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select first screen tracking toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectFirstScreenTrackingToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisplayItemBackgroundImageToggle(arg):
        '''
        SelectDisplayItemBackgroundImageToggle : Select display item background image toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select first screen tracking toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display item background image toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectDisplayItemBackgroundImageToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCompontentDuration(arg):
        '''
        ChangeCompontentDuration : Change compontent duration
                Input argu :
                    duration_type - always / duration
                    start_time - start time of duration / now
                    end_time - end time of duration / now
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        duration_type = arg["duration_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Click duration type
        xpath = Util.GetXpath({"locate": duration_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click duration type", "result": "1"})

        if duration_type == "duration":

            ##Clear start time
            xpath = Util.GetXpath({"locate": "start_close_button"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

            if start_time:
                ##Click start time field
                xpath = Util.GetXpath({"locate": "start_time_button"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time", "result": "1"})

                ##Click Now button
                if start_time == "now":
                    xpath = Util.GetXpath({"locate": "start_time_now"})
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now", "result": "1"})

                ##Input start time
                else:
                    BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": start_time, "result": "1"})
                    time.sleep(2)
                    ##If OK button clickable, click ok button
                    ok_button_xpath = Util.GetXpath({"locate": "ok_btn"})
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": ok_button_xpath, "passok": "0", "result": "1"}):
                        xpath = Util.GetXpath({"locate": "ok_btn"})
                        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})
                    else:
                        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})
            ##Clear end time
            xpath = Util.GetXpath({"locate": "end_close_button"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

            if end_time:
                ##Click end time field
                xpath = Util.GetXpath({"locate": "end_time_button"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time", "result": "1"})

                ##Click Now button
                if start_time == "now":
                    xpath = Util.GetXpath({"locate": "end_time_now"})
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now", "result": "1"})

                ##Input end time
                else:
                    BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": end_time, "result": "1"})
                    time.sleep(2)
                    ##If OK button clickable, click ok button
                    ok_button_xpath = Util.GetXpath({"locate": "ok_btn"})
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": ok_button_xpath, "passok": "0", "result": "1"}):
                        xpath = Util.GetXpath({"locate": "ok_btn"})
                        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})
                    else:
                        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.ChangeCompontentDuration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAnchorTabBarColor(arg):
        '''
        InputAnchorTabBarColor : Input color
            Input argu :
                style - background / highlight / more_background / more_icon
                value - color value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        style = arg["style"]
        value = arg["value"]

        ##Input color
        xpath = Util.GetXpath({"locate": style})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputAnchorTabBarColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisplayStyle(arg):
        '''
        SelectDisplayStyle : Select display style
            Input argu :
                option - image_tab / text_tab / customized_image
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select display style
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display style", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectDisplayStyle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectHighlightStyle(arg):
        '''
        SelectHighlightStyle : Select highlight style
            Input argu :
                option - underline / outside_borderline / no_additional
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select highlight style
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select highlight style", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectHighlightStyle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectToggle(arg):
        '''
        SelectToggle : Select toggle
            Input argu :
                toggle - more_and_dropdown_on / more_and_dropdown_off / fixed_position_on / fixed_position_off / display_under_text_on / display_under_text_off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectToggle ->' + toggle)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectFixedTabDisplayStyle(arg):
        '''
        SelectFixedTabDisplayStyle : Select fixed tab display style
            Input argu :
                option - vertical_grid / horizontal_scroll_row
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select fixed tab display style
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select fixed tab display style", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectFixedTabDisplayStyle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTabData(arg):
        '''
        InputTabData : Input tab data
            Input argu :
                text - data text
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input text
        xpath = Util.GetXpath({"locate": "text_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTabData')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectBundleName(arg):
        '''
        SelectBundleName : Select bundle name
            Input argu :
                bundle_name - bundle name to choose in dropdown list
                search_name - name to search
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        bundle_name = arg["bundle_name"]
        search_name = arg["search_name"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Search by bundle name
        xpath = Util.GetXpath({"locate": "dropdown_list_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": search_name, "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("bundle_name_to_be_replace", bundle_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectBundleName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUserScope(arg):
        '''
        SelectUserScope : Select user scope
            Input argu :
                user_scope - user scope
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        user_scope = arg["user_scope"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(3)

        ##Input user scope
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": user_scope, "result": "1"})
        time.sleep(3)
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
        time.sleep(5)

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectComponentLabel(arg):
        '''
        SelectComponentLabel : Select user scope
            Input argu :
                component_label - component label
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_label = arg["component_label"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})
        time.sleep(3)

        ##Get dropdown content
        content_xpath = Util.GetXpath({"locate": "dropdown_content"})
        content_xpath = content_xpath.replace("component_label_to_be_replace", component_label)

        ##No data in dropdown list or fail to open dropdown list
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": content_xpath, "passok": "0", "result": "1"}):
            dumplogger.info("No data in dropdown list or fail to open dropdown list")
            ##Click white space
            xpath = Util.GetXpath({"locate": "white_space"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

            ##Click dropdown list
            xpath = Util.GetXpath({"locate": "dropdown_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        BaseUICore.Click({"method": "xpath", "locate": content_xpath, "message": "Choose dropdown content", "result": "1"})

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectComponentLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassEditToDeleteAllComponent(arg):
        '''
        MassEditToDeleteAllComponent : Mass edit to delete all component
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click check box to mass edit
        AdminCampaignViewEditPageButton.ClickMassEdit({"result": "1"})

        ##Get component
        xpath = Util.GetXpath({"locate":"component"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"multi", "result": "1"})
        if len(GlobalAdapter.CommonVar._PageElements_) != 0:

            ##Click check all
            xpath = Util.GetXpath({"locate": "check_all_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check all button", "result": "1"})

            ##Click delete button
            xpath = Util.GetXpath({"locate": "delete_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

            ##Click delete confirm
            xpath = Util.GetXpath({"locate": "delete_confirm"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete confirm", "result": "1"})
        else:
            dumplogger.info("There is no component to delete.")

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.MassEditToDeleteAllComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassEditToRecorder(arg):
        '''MassEditToRecorder : Mass edit to recorder
            Input argu :
                component_name - component name
                recorder_position - recorder position
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]
        recorder_position = arg["recorder_position"]

        AdminCampaignViewEditPageButton.ClickMassEdit({"result": "1"})

        ##Clcik to select component
        xpath = Util.GetXpath({"locate": "component_checkbox"})
        xpath = xpath.replace('component_name_to_be_replace', component_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Clcik to select component", "result": "1"})

        ##Click recorder button
        xpath = Util.GetXpath({"locate": "recorder_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click recorder button", "result": "1"})

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": "dropdown_content"})
        xpath = xpath.replace("position_to_be_replace", recorder_position)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.MassEditToRecorder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectStatusFilter(arg):
        '''
        SelectStatusFilter : select status from dropdown list
            Input argu:
                type - all / ended / ongoing / upcoming
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click status filiter
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click status -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectStatusFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddBundleDealsToComponent(arg):
        '''
        AddBundleDealsToComponent : Add bundledeal promotion to bundledeal component
            Input argu :
                promotion_ids - Bundledeal promotion ids add in compoment
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        promotion_ids = arg["promotion_ids"]
        promotion_ids = promotion_ids.split(",")

        ##Log all promotion ids
        dumplogger.info("All promotion ids => %s" % (promotion_ids))

        ##Click to add bundledeal id to component
        for click_times in range(len(promotion_ids)):
            ##Click promotion id search icon
            xpath = Util.GetXpath({"locate": "id_search_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion id search icon", "result": "1"})

            ##Click search bar and input promotion id
            xpath = Util.GetXpath({"locate": "id_search_bar"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_ids[click_times], "result": "1"})

            ##Click search button
            xpath = Util.GetXpath({"locate": "id_search_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion id search button", "result": "1"})

            ##Click select to add bundledeal to selected sessions
            xpath = Util.GetXpath({"locate": "select_bundledeal"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select to add bundledeal to selected sessions", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.AddBundleDealsToComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBundleDealsToComponent(arg):
        '''
        DeleteBundleDealsToComponent : Delete bundleDeals to component
            Input argu :
                promotion_ids - Bundledeal promotion ids add in compoment
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        promotion_ids = arg["promotion_ids"]
        promotion_ids = promotion_ids.split(",")

        ##Log all promotion ids
        dumplogger.info("All promotion ids => %s" % (promotion_ids))

        ##Click delete to remove bundledeal id from component
        for click_times in range(len(promotion_ids)):
            ##Click select to add bundledeal to selected sessions
            xpath = Util.GetXpath({"locate": "delete_bundledeal"})
            xpath = xpath.replace("promo_id_to_be_replaced", promotion_ids[click_times])
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete bundledeal from selected sessions", "result": "1"})
            time.sleep(2)

            ##Confirm to delete bundledeal
            xpath = Util.GetXpath({"locate": "confirm"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Confirm to delete bundledeal from selected sessions", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.DeleteBundleDealsToComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemIdOrder(arg):
        '''
        InputItemIdOrder : Input item ids with customized order
                Input argu :
                    column - a number (from 1 to 3)
                    item_id - item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column = arg["column"]
        item_id = arg["item_id"]

        ##Input item id to column
        xpath = Util.GetXpath({"locate": "column_field"})
        xpath = xpath.replace("index_to_be_replaced", column)
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputItemIdOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddAddOnDealComponent(arg):
        '''
        AddAddOnDealComponent : Add add on deal component
            Input argu :
                promotion_id - add on deal id
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        promotion_id = arg["promotion_id"]

        ##Click promotion id search icon
        xpath = Util.GetXpath({"locate": "search_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "search by promotion id", "result": "1"})

        ##input promotion id
        xpath = Util.GetXpath({"locate": "id_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_id, "result": "1"})

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion id search button", "result": "1"})

        ##Click to select add on deal
        xpath = Util.GetXpath({"locate": "select_addondeal"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to select add on deal", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.AddAddOnDealComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImage(arg):
        '''
        UploadImage : Upload image for sharing content
                Input argu :
                    type - unselected / selected
                    image_name - image file neme
                    image_type - image file type
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        type = arg["type"]
        image_name = arg["image_name"]
        image_type = arg["image_type"]
        ret = 1

        ##Click upload csv button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload image button", "result": "1"})

        ##Upload image
        XtFunc.UploadFileWithWindowsAutoIt({"project": "Microsite", "action": "image", "file_name": image_name, "file_type": image_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.UploadImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageHash(arg):
        '''
        InputImageHash : Input image hash
            Input argu :
                type - unselected / selected
                value - image hash value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]
        value = arg["value"]

        ##Input text
        xpath = Util.GetXpath({"locate": type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputImageHash')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPositionNumber(arg):
        '''
        InputPositionNumber : Input position number
            Input argu :
                position_number - position number
                confirm - ok / cancel
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        position_number = arg["position_number"]
        confirm = arg["confirm"]

        ##Input text
        xpath = Util.GetXpath({"locate": "position_number_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": position_number, "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": confirm})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputPositionNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputColor(arg):
        '''
        InputColor : Input color
            Input argu :
                style - component_background / text_background / header_text / promotion_discount / product_price / view_more / price_background
                value - color value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        style = arg["style"]
        value = arg["value"]

        ##Input color
        xpath = Util.GetXpath({"locate": style})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
        ##Click white space
        space_xpath = Util.GetXpath({"locate": style})
        BaseUICore.Click({"method": "xpath", "locate": space_xpath, "message": "Click white space", "result": "1"})

        ##Get elements
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute": "value", "mode":"single", "result": "1"})

        ##If input text failed, input color again
        if GlobalAdapter.CommonVar._PageAttributes_ != value:
            dumplogger.info("If input text failed, input color again")
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
            ##Click white space
            BaseUICore.Click({"method": "xpath", "locate": space_xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderTextContent(arg):
        '''
        InputHeaderTextContent : Input header text contnent
            Input argu :
                text - header text
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input header color
        xpath = Util.GetXpath({"locate": "header_content"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputHeaderTextContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectBackground(arg):
        '''
        SelectBackground : Select background option
            Input argu :
                option - color / transparent / image
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select background option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select background option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRedirectUrl(arg):
        '''
        InputRedirectUrl : Input redirect url
            Input argu :
                url - redirect url
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        url = arg["url"]

        ##Input redirect url
        xpath = Util.GetXpath({"locate": "url_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputRedirectUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherGridInfo(arg):
        '''
        InputVoucherGridInfo : Input voucher grid info
            Input argu :
                component_name - Microsite voucher grid component name
                vouchers_per_row - Number of vouchers per row
                vouchers_display_number - Number of vouchers to display
                number_of_vouchers - Number of vouchers add in compoment
                voucher_type - shopee_voucher / seller_voucher / free_shipping_voucher
                promotion_ids - Voucher promotion ids add in compoment
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        component_name = arg['component_name']
        vouchers_per_row = arg['vouchers_per_row']
        vouchers_display_number = arg['vouchers_display_number']
        number_of_vouchers = arg['number_of_vouchers']
        voucher_type = arg['voucher_type']
        promotion_ids = arg['promotion_ids']

        ##Input name
        AdminCampaignViewEditPage.InputComponentName({"component_name":component_name, "result": "1"})

        ##Click UI Style
        AdminCampaignViewEditPageButton.ClickConfigurationTab({"tab":"ui_style", "result": "1"})

        ##Input voucher display number
        xpath = Util.GetXpath({"locate": "voucher_display_number"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": vouchers_display_number, "result": "1"})

        ##Select display voucher per row
        xpath = Util.GetXpath({"locate": "display_voucher_per_row"})
        xpath = xpath.replace('vouchers_per_row_to_be_replace',vouchers_per_row)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display voucher per row", "result": "1"})

        ##Click guide user to shop
        xpath = Util.GetXpath({"locate": "guide_user_to_shop"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "guide user to shop for seller voucher", "result": "1"})

        ##Click Deboost Claimed Voucher
        xpath = Util.GetXpath({"locate": "deboost_on_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "FE Reordering Deboost Claimed Voucher", "result": "1"})

        ##Click Display Voucher Tag and Label
        xpath = Util.GetXpath({"locate": "display_voucher_tag_label"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Display Voucher Tag and Label", "result": "1"})

        ##Click Display Voucher T&C
        xpath = Util.GetXpath({"locate": "display_voucher_T&C"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Display Voucher T&C", "result": "1"})

        ##Click Display Voucher Valid Date Info
        xpath = Util.GetXpath({"locate": "display_voucher_valid_date_info"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Display Voucher Valid Date Info", "result": "1"})

        ##Click Data
        AdminCampaignViewEditPageButton.ClickConfigurationTab({"tab":"data", "result": "1"})

        ##Click edit voucher
        xpath = Util.GetXpath({"locate": "edit_voucher"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit voucher", "result": "1"})

        ##Prepare promotion ids
        if promotion_ids:
            ##Split promotion ids string into promotion_ids
            promotion_ids = promotion_ids.split(',')
        elif voucher_type == "shopee_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_
        elif voucher_type == "seller_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._SellerVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._SellerVoucherList_
        elif voucher_type == "free_shipping_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
        elif voucher_type == "dp_voucher":
            ##Get promotion ids from GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
            promotion_ids = GlobalAdapter.PromotionE2EVar._DPVoucherList_
        else:
            dumplogger.error("Voucher type string is not correct, please use shopee_voucher / seller_voucher / free_shipping_voucher / dp_voucher string!!!")
            ret = 0

        ##Log all promotion ids
        dumplogger.info("All promotion ids => %s" % (promotion_ids))

        if ret == 1:
            ##Click add new voucher btn
            for click_times in range(int(number_of_vouchers)):
                xpath = Util.GetXpath({"locate":"add_new_voucher"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new voucher btn", "result": "1"})
                dumplogger.info("Click add new voucher times: %d" % (click_times))
                time.sleep(2)

                ##Input promotion id
                xpath = Util.GetXpath({"locate":"promotion_id"})
                xpath_promotion_id_field = xpath.replace("times_to_be_replace", str(click_times+1))
                BaseUICore.Input({"method":"xpath", "locate":xpath_promotion_id_field, "string":promotion_ids[click_times]["id"], "result": "1"})

                ##Input redirect url
                xpath = Util.GetXpath({"locate":"redirect_url"})
                xpath_redirect_url_field = xpath.replace("times_to_be_replace", str(click_times+1))
                BaseUICore.Input({"method":"xpath", "locate":xpath_redirect_url_field, "string":"https://google.com", "result": "1"})

                ##Input display name
                xpath = Util.GetXpath({"locate":"display_name"})
                xpath_display_name_field = xpath.replace("times_to_be_replace", str(click_times+1))
                BaseUICore.Input({"method":"xpath", "locate":xpath_display_name_field, "string":"AUTO", "result": "1"})

            ##Click save
            xpath = Util.GetXpath({"locate": "save_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputVoucherGridInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisplayComponentHeaderToggle(arg):
        '''
        SelectDisplayComponentHeaderToggle : Select display component header toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select display component deader toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display component header toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectDisplayComponentHeaderToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisplayValidDateToggle(arg):
        '''
        SelectDisplayValidDateToggle : Select display valid date toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select display valid date toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display valid date toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectDisplayValidDateToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTabStyle(arg):
        '''
        SelectTabStyle : Select tab style
            Input argu :
                option - multi_tabs / no_tabs
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select tab style
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select tab style", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectTabStyle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCardsLayout(arg):
        '''
        SelectCardsLayout : Select cards layout
            Input argu :
                option - vertical_grid / horizontal_row
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select cards layout
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select cards layout", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectCardsLayout')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemCardDisplay(arg):
        '''
        SelectItemCardDisplay : Select item card display
            Input argu :
                option - customised / system_generated
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select item card display
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select item card display", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectItemCardDisplay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectHideCurrentPrice(arg):
        '''
        SelectHideCurrentPrice : Select hide current price toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select hide current price toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select hide current price toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectHideCurrentPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShowOriginalPrice(arg):
        '''
        SelectShowOriginalPrice : Select show original price toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select show original price toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select show original price toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectShowOriginalPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShowProgressBar(arg):
        '''
        SelectShowProgressBar : Select show progress bar toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select show progress bar toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select show progress bar toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectShowProgressBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShowAddToCartAction(arg):
        '''
        SelectShowAddToCartAction : Select show add to cart action toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select show add to cart action toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select show add to cart action toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectShowAddToCartAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCollectionId(arg):
        '''
        InputCollectionId : Input collection id
            Input argu :
                collection_id - collection id
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        collection_id = arg["collection_id"]

        ##Clear collection id
        xpath = Util.GetXpath({"locate": "collection_id_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input collection id
        if collection_id:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collection_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputCollectionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderText(arg):
        '''
        InputHeaderText : Input header text
            Input argu :
                value - header text value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Input header text
        xpath = Util.GetXpath({"locate": "header_text"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputHeaderText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTotalItemsDisplayNumber(arg):
        '''
        InputTotalItemsDisplayNumber : Input total items display number
            Input argu :
                number - display number
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Clear total items display number
        xpath = Util.GetXpath({"locate": "number_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input total items display number
        if number:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": number, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "tab", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTotalItemsDisplayNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEditItemTable(arg):
        '''
        InputEditItemTable : Input item table field
            Input argu :
                field - item_id / shop_id / item_name / item_campaign_stock
                value - input value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        field = arg["field"]
        value = arg["value"]

        ##Clear total items display number
        xpath = Util.GetXpath({"locate": field})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input total items display number
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputEditItemTable -> ' + field)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSEOText(arg):
        '''
        InputSEOText : Input SEO text
            Input argu :
                value - input value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Input SEO text
        xpath = Util.GetXpath({"locate": "SEO_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputSEOText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectPostion(arg):
        '''
        SelectPostion : Select postion
            Input argu :
                option - scrolling / sticky_on_the_top / fixed_sticky
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select postion
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select postion", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectPostion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFixedSticky(arg):
        '''
        InputFixedSticky : Input fixed sticky
            Input argu :
                field - width / top / bottom / left / right
                value - input value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        field = arg["field"]
        value = arg["value"]

        ##Clear fixed sticky
        xpath = Util.GetXpath({"locate": field})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input fixed sticky
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
            time.sleep(5)
        xpath = Util.GetXpath({"locate": "fixed_sticky"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputFixedSticky -> ' + field)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOneImageHash(arg):
        '''
        InputOneImageHash : Input 1 image hash
            Input argu :
                value - input value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear image hash
        xpath = Util.GetXpath({"locate": "image_hash"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input 1 image hash
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputOneImageHash')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLinkUrl(arg):
        '''
        InputLinkUrl : Input link url
            Input argu :
                value - input value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Input link url
        xpath = Util.GetXpath({"locate": "link_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
        xpath = Util.GetXpath({"locate": "link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputLinkUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectURLType(arg):
        '''
        SelectURLType : Select url type
            Input argu :
                type - free url / APPRL / chat page
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectURLType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTableTime(arg):
        '''
        InputTableTime : Input table start time and end time
                Input argu :
                    start_time - start time / now
                    end_time - end time / now
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Clear start time
        xpath = Util.GetXpath({"locate": "start_close_button"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
            time.sleep(3)

        if start_time:
            ##Click start time calendar list
            xpath = Util.GetXpath({"locate": "start_time_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time", "result": "1"})

            ##Click Now button
            if start_time == "now":
                xpath = Util.GetXpath({"locate": "start_time_now"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now", "result": "1"})

            ##Input start time
            else:
                BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": start_time, "result": "1"})
                time.sleep(3)
                BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})

        ##Clear end time
        xpath = Util.GetXpath({"locate": "end_close_button"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
            time.sleep(3)

        if end_time:
            ##Click end time calendar list
            xpath = Util.GetXpath({"locate": "end_time_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time", "result": "1"})

            ##Click Now button
            if end_time == "now":
                xpath = Util.GetXpath({"locate": "end_time_now"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now", "result": "1"})

            ##Input end time
            else:
                BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": end_time, "result": "1"})
                time.sleep(3)
                BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTableTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTableTitle(arg):
        '''
        InputTableTitle : Input title
            Input argu :
                value - title value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear title field
        xpath = Util.GetXpath({"locate": "title_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input title
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTableTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTablePopupMessage(arg):
        '''
        InputTablePopupMessage : Input popup message
            Input argu :
                value - message value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear popup message field
        xpath = Util.GetXpath({"locate": "message_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input popup message
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTablePopupMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReminderUrl(arg):
        '''
        InputReminderUrl : Input reminder url
            Input argu :
                value - message value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear reminder url field
        xpath = Util.GetXpath({"locate": "reminder_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input reminder url
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputReminderUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopId(arg):
        '''
        InputShopId : Input shop id
            Input argu :
                shop_id - shop id
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Clear shop id field
        xpath = Util.GetXpath({"locate": "shop_id_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input shop id
        if shop_id:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputShopId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemId(arg):
        '''
        InputItemId : Input item id
            Input argu :
                item_id - item id
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Input item id
        xpath = Util.GetXpath({"locate": "item_id_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "result": "1"})
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputItemId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTableKey(arg):
        '''
        InputTableKey : Input table key
            Input argu :
                value - key value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear key field
        xpath = Util.GetXpath({"locate": "key_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input table key
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTableKey')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTableDefaultText(arg):
        '''
        InputTableDefaultText : Input table default text
            Input argu :
                value - default text
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear default text field
        xpath = Util.GetXpath({"locate": "default_text_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input table default text
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTableDefaultText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTableFontSizeRatio(arg):
        '''
        InputTableFontSizeRatio : Input table font size ratio
            Input argu :
                value - font size ratio text
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear front size field
        xpath = Util.GetXpath({"locate": "front_size_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input table font size ratio
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTableFontSizeRatio')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectVerticalAlignment(arg):
        '''
        SelectVerticalAlignment : Select Vertical Alignment
            Input argu :
                type - top / center / bottom
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectVerticalAlignment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectHorizontalAlignment(arg):
        '''
        SelectHorizontalAlignment : Select Horizontal Alignment
            Input argu :
                type - left / center / right
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectHorizontalAlignment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTextAlignment(arg):
        '''
        SelectTextAlignment : Select Text Alignment
            Input argu :
                type - left / center / right
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Choose dropdown content
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose dropdown content", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectTextAlignment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPlaceholderImageHash(arg):
        '''
        InputPlaceholderImageHash : Input placeholder image hash
            Input argu :
                value - input value
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Input placeholder image hash
        xpath = Util.GetXpath({"locate": "image_hash"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputPlaceholderImageHash')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTableImageSizeRatio(arg):
        '''
        InputTableImageSizeRatio : Input table image size ratio
            Input argu :
                value - font size ratio text
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear image size field
        xpath = Util.GetXpath({"locate": "image_size_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input table image size ratio
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        BaseUILogic.KeyboardAction({"actiontype": "tab", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTableImageSizeRatio')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductLabel(arg):
        '''
        SelectProductLabel : Select product label
            Input argu :
                product_label - product label
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        product_label = arg["product_label"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Input keyword
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": product_label, "result": "1"})
        time.sleep(3)
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
        time.sleep(5)

        ##Click wite space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectProductLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectInSiteSearch(arg):
        '''
        SelectInSiteSearch : Select in-site search toggle
            Input argu :
                toggle - on / off
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select in-site search toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select in-site search toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectInSiteSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBackgroundImage(arg):
        '''
        UploadBackgroundImage : Upload background image
            Input argu :
                upload_type - file / image
                file_name - file name
                file_type - file type
                project - image file project path
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        project = arg["project"]

        ##Upload background image
        xpath = Util.GetXpath({"locate": "background_image_input"})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": upload_type, "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.UploadBackgroundImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAnchoredComponent(arg):
        '''
        SelectAnchoredComponent : Select anchored component
                Input argu :
                    anchored_component - anchored component
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        anchored_component = arg["anchored_component"]

        ##Click dropdown list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown list", "result": "1"})

        ##Input anchored component name
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": anchored_component, "result": "1"})
        time.sleep(3)
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})
        time.sleep(3)

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectAnchoredComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectComponentModule(arg):
        '''
        SelectComponentModule : Select component module in configuration panel
            Input argu :
                module_name - module name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        module_name = arg["module_name"]

        ##Select module
        xpath = Util.GetXpath({"locate": "select_module"})
        xpath = xpath.replace('module_name_to_be_replace', module_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select module", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectComponentModule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCampaignsSessions(arg):
        '''
        SelectCampaignsSessions : Select available sessions in popup promotion window
            Input argu :
                session_name - available sessions name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        session_name = arg["session_name"]

        ##Select available sessions
        xpath = Util.GetXpath({"locate": "select_sessions"})
        xpath = xpath.replace('sessions_name_to_be_replace', session_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select sessions", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectCampaignsSessions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTextContent(arg):
        '''
        InputTextContent : Input text content
            Input argu :
                text - input text
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        text = arg["text"]

        if text:
            ##Input text content
            BaseUICore.ExecuteScript({"script": "document.querySelector('.CodeMirror').CodeMirror.setValue('" + text + "')", "result": "1"})
        else:
            ##Clear input text
            BaseUICore.ExecuteScript({"script": "document.querySelector('.CodeMirror').CodeMirror.setValue('')", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTextContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditTextContent(arg):
        '''
        EditTextContent : Edit text content
                Input argu :
                    bold - bold (True / False)
                    italic - italic (True / False)
                    heading_type - numbers to click heading type
                    generic_list - generic list (True / False)
                    numbered_list - numbered list (True / False)
                    create_link - create link (True / False)
                    toggle_side_by_side - toggle side by side (True / False)
                    toggle_full_screen - toggle full screen (True / False)
                    markdown_guide - markdown guide (True / False)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bold = arg["bold"]
        italic = arg["italic"]
        heading_type = arg["heading_type"]
        generic_list = arg["generic_list"]
        numbered_list = arg["numbered_list"]
        create_link = arg["create_link"]
        toggle_side_by_side = arg["toggle_side_by_side"]
        toggle_full_screen = arg["toggle_full_screen"]
        markdown_guide = arg["markdown_guide"]

        ##Select all content
        xpath = Util.GetXpath({"locate": "input_content"})
        BaseUICore.ExecuteScript({"script": "document.querySelector('.CodeMirror').CodeMirror.execCommand('selectAll')", "result": "1"})

        if bold:
            ##Click bold button
            xpath = Util.GetXpath({"locate": "bold"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bold button", "result": "1"})

        if italic:
            ##Click italic button
            xpath = Util.GetXpath({"locate": "italic"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click italic button", "result": "1"})

        if heading_type:
            xpath = Util.GetXpath({"locate": "heading"})
            for times in range(int(heading_type)):
                ##Click heading type button
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click heading type button", "result": "1"})

        if generic_list:
            ##Click generic list button
            xpath = Util.GetXpath({"locate": "generic_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click generic list button", "result": "1"})

        if numbered_list:
            ##Click numbered list button
            xpath = Util.GetXpath({"locate": "numbered_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click numbered list button", "result": "1"})

        if create_link:
            ##Click create link button
            xpath = Util.GetXpath({"locate": "create_link"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create link button", "result": "1"})

        if toggle_side_by_side:
            ##Click toggle side by side button
            xpath = Util.GetXpath({"locate": "toggle_side_by_side"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle side by side button", "result": "1"})

        if toggle_full_screen:
            ##Click toggle full screen button
            xpath = Util.GetXpath({"locate": "toggle_full_screen"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle full screen button", "result": "1"})

        if markdown_guide:
            ##Click markdown guide button
            xpath = Util.GetXpath({"locate": "markdown_guide"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click markdown guide button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.EditTextContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFontSizeRatio(arg):
        '''
        InputFontSizeRatio : Input font size ratio
            Input argu :
                value - font size ratio
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear font size ratio value
        xpath = Util.GetXpath({"locate": "font_size_ratio"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        if value:
            ##Input font size ratio
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "tab", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputFontSizeRatio')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayedNumber(arg):
        '''
        InputComponentName : Input number of session displayed
            Input argu :
                input_field - session_displayed_field / item_per_session_field
                input_number - number of session displayed or item per session displayed
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        input_field = arg["input_field"]
        input_number = arg["input_number"]

        ##Input number of session displayed or item per session displayed
        xpath = Util.GetXpath({"locate": input_field})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputDisplayedNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisplayItemName(arg):
        '''
        SelectDisplayItemName : Select display item name in mobile toggle
            Input argu :
                toggle - on / off
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Select display item name in mobile toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display item name in mobile toggle", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectDisplayItemName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCustomAnchorID(arg):
        '''
        InputCustomAnchorID : Input custom anchor id
            Input argu :
                anchor_id - anchor id
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        anchor_id = arg["anchor_id"]

        ##Input custom anchor id
        xpath = Util.GetXpath({"locate": "custom_anchor_id_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": anchor_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputCustomAnchorID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectISFSSessionsTimeSlot(arg):
        '''
        SelectISFSSessionsTimeSlot : Select available ISFS sessions in popup time slot window
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 0

        ##Select available sessions
        xpath = Util.GetXpath({"locate": "available_sessions"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select available sessions", "result": "1"})

        ##Store available sessions starttime display content in left side panel
        xpath = Util.GetXpath({"locate":"available_sessions_starttime"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"form_start_time", "result": "1"})
        available_sessions_starttime = GlobalAdapter.FormE2EVar._StartTime_
        dumplogger.info("available_sessions_starttime : %s" % available_sessions_starttime)

        ##Store selected sessions starttime display content in left side panel
        xpath = Util.GetXpath({"locate":"selected_sessions_starttime"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"form_start_time", "result": "1"})
        selected_sessions_starttime = GlobalAdapter.FormE2EVar._StartTime_
        dumplogger.info("selected_sessions_starttime : %s" % selected_sessions_starttime)

        ##Compare store starttime and displayed value in right side panel
        if selected_sessions_starttime == available_sessions_starttime:
            dumplogger.info("selected sessions display correct!")
            ret = 1
        else:
            dumplogger.info("selected sessions display error!")

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.SelectISFSSessionsTimeSlot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemDisplayName(arg):
        '''
        InputItemDisplayName : Input item display name
                Input argu :
                    display_name - Display Name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        display_name = arg["display_name"]

        ##Input item display name
        xpath = Util.GetXpath({"locate": "item_display_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputItemDisplayName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTablePlaceholderCollectionID(arg):
        '''
        InputTablePlaceholderCollectionID : Input table placeholder collection id
                Input argu :
                    value - placeholder collection id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value = arg["value"]

        ##Clear placeholder collection id field
        xpath = Util.GetXpath({"locate": "placeholder_collection_id"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input placeholder collection id
        if value:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.InputTablePlaceholderCollectionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePageHeaderIconOrder(arg):
        '''
        ChangePageHeaderIconOrder : move page header icon to change order
                Input argu :
                    drag_icon - page header icon to drag
                    target_icon - page header icon to drop
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drag_icon = arg["drag_icon"]
        x_offset = arg['x_offset']
        y_offset = arg['y_offset']

        ##Get element to change order
        xpath = Util.GetXpath({"locate": "page_header_icon"}).replace('icon_to_be_replaced', drag_icon)

        ##Click element by offset
        BaseUICore.DragAndDropByOffset({"drag_elem": xpath, "x_offset": int(x_offset), "y_offset": int(y_offset), "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPage.ChangePageHeaderIconOrder')


class AdminCampaignViewEditPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        '''
        ClickNext : Click next button
            Input argu :
                page - RW / LITE / PC / APP
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        page = arg["page"]

        ##Click next button
        xpath = Util.GetXpath({"locate": page})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickNext ->' + page)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button
        xpath = Util.GetXpath({"locate": "ok_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassUpload(arg):
        '''
        ClickMassUpload : Click mass upload btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mass upload btn
        xpath = Util.GetXpath({"locate": "mass_upload_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass upload btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickMassUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadFile(arg):
        '''
        ClickUploadFile : Click upload file
                Input argu :
                    file_name - csv file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]

        ##Click upload file
        xpath = Util.GetXpath({"locate": "upload_file_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload file", "result": "1"})
        time.sleep(5)

        ##Upload promotion rule csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickUploadFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOverwriteVouchers(arg):
        '''
        ClickOverwriteVouchers : Click overwrite vouchers
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click overwrite vouchers
        xpath = Util.GetXpath({"locate": "overwrite_vouchers_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click overwrite vouchers", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickOverwriteVouchers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteVoucher(arg):
        '''
        ClickDeleteVoucher : Click delete voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete voucher
        xpath = Util.GetXpath({"locate": "delete_voucher_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete vouchers", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickDeleteVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteConfirm(arg):
        '''
        ClickDeleteConfirm : Click delete confirm
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete voucher
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickDeleteConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePosition(arg):
        '''
        ClickChangePosition : Click change position
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change position
        xpath = Util.GetXpath({"locate": "change_position_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change position", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickChangePosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportCSV(arg):
        '''
        ClickExportCSV : Click export CSV btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export CSV btn
        xpath = Util.GetXpath({"locate": "export_csv_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export CSV btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickExportCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewVoucher(arg):
        '''
        ClickAddNewVoucher : Click add new voucher btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new voucher btn
        xpath = Util.GetXpath({"locate":"add_new_voucher"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new voucher btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickAddNewVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageTitle(arg):
        '''
        ClickPageTitle : Click page title then click copy link or goto
            Input argu :
                page_title - page title
                type - copy_link / go_to
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        page_title = arg["page_title"]
        type = arg["type"]

        ##Click page title
        xpath = Util.GetXpath({"locate": "page_title"})
        xpath = xpath.replace('campaign_name_to_be_replace',page_title)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page title", "result": "1"})

        ##Click copy link or goto
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type, "result": "1"})

        ##Click page title to hide copy link or goto
        xpath = Util.GetXpath({"locate": "page_title"})
        xpath = xpath.replace('campaign_name_to_be_replace',page_title)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page title to hide copy link or goto", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickPageTitle ->' + page_title)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicate(arg):
        '''
        ClickDuplicate : Click duplicate button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click duplicate button
        xpath = Util.GetXpath({"locate": "duplicate_button"})
        BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath, "locatehidden": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirmAction(arg):
        '''
        ClickConfirmAction : Click action type button
                Input argu :
                    action_type - cancel / publish_now / duplicate / save_as_draft
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg["action_type"]

        ##Click action type button
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action type button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickConfirmAction ->' + action_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublish(arg):
        '''
        ClickPublish : Click publish button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click publish button
        xpath = Util.GetXpath({"locate": "publish_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click publish button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickPublish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExit(arg):
        '''
        ClickExit : Click exit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exit button
        xpath = Util.GetXpath({"locate": "exit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click exit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickExit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassEdit(arg):
        '''
        ClickMassEdit : Click mass edit
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click check box to mass edit
        xpath = Util.GetXpath({"locate": "mass_edit"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check box to mass edit", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickMassEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicateComponent(arg):
        '''
        ClickDuplicateComponent : Click duplicate component
                Input argu :
                    component_name - component name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]

        ##Select component
        AdminCampaignViewEditPage.SelectComponent({"component_name": component_name, "result": "1"})

        ##Click duplicate component
        xpath = Util.GetXpath({"locate": "duplicate_button"})
        xpath = xpath.replace('component_name_to_be_replace', component_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click duplicate component", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickDuplicateComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close button
        xpath = Util.GetXpath({"locate": "close_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStatusFilter(arg):
        '''ClickStatusFilter : Click status filter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ## Click status filter
        xpath = Util.GetXpath({"locate": "status_filter"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status filter", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickStatusFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click filter button to execute
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter button
        xpath = Util.GetXpath({"locate": "filter_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfigurationTab(arg):
        '''
        ClickConfigurationTab : Click configuration tab
                Input argu :
                    tab - general / ui_style / data
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click configuration tab
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click configuration tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickConfigurationTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditVouchers(arg):
        '''
        ClickEditVouchers : Click Edit Vouchers btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit voucher
        xpath = Util.GetXpath({"locate": "edit_voucher"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditVouchers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToAdd(arg):
        '''
        ClickGoToAdd : Click go to add
                Input argu :
                    type - bundle_name / user_scope / component_label / product_collection
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click go to add
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to add", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickGoToAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlatformHide(arg):
        '''
        ClickPlatformHide : Click platform hide
                Input argu :
                    option - ios / android
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click platform hide
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click platform hide", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickPlatformHide')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditTab(arg):
        '''
        ClickEditTab : Click edit tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit tab
        xpath = Util.GetXpath({"locate": "tab_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditAddOnDeals(arg):
        '''
        ClickEditAddOnDeals : Click edit local / seller add on deals
                Input argu :
                    type - local / seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ## Click edit local / seller add on deals
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit " + type + " add on deals", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditAddOnDeals')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditBundleDeals(arg):
        '''
        ClickEditBundleDeals : Click edit local / seller bundledeals
                Input argu :
                    type - local / seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ## Click edit local / seller bundle deals
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit " + type + " bundle deals", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditBundleDeals')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditToPromotionComponent(arg):
        '''
        ClickEditToPromotionComponent : Click edit button
                Input argu :
                    promotion_id - promotion id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_id = arg["promotion_id"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace("promotion_id_to_be_replaced", promotion_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditToPromotionComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemOrderToBundleDealComponent(arg):
        '''
        ClickItemOrderToBundleDealComponent : Click item order
                Input argu :
                    type - system / customized
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click one type to display item displayed order
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click type -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickItemOrderToBundleDealComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableDelete(arg):
        '''
        ClickTableDelete : Click table edit action
                Input argu :
                    confirm - cancel / ok
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        confirm = arg["confirm"]

        ##Click edit tab action
        xpath = Util.GetXpath({"locate": "delete"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete action", "result": "1"})

        ##Mouse move to popup window icon
        xpath = Util.GetXpath({"locate": "popup_window_icon"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": confirm})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableSave(arg):
        '''
        ClickTableSave : Click table save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableAddNew(arg):
        '''
        ClickTableAddNew : Click table add new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableCancel(arg):
        '''
        ClickTableCancel : Click table cancel button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "cancel_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteUploadImage(arg):
        '''
        ClickDeleteUploadImage : Click delete image
            Input argu :
                confirm - yes / no
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        confirm = arg["confirm"]

        ##Click delete button
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate": confirm})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickDeleteUploadImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableChangePosition(arg):
        '''
        ClickTableChangePosition : Click table change position
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click change position button
        xpath = Util.GetXpath({"locate": "change_position_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change position button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableChangePosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableCopyLink(arg):
        '''
        ClickTableCopyLink : Click table copy link
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click copy link button
        xpath = Util.GetXpath({"locate": "copy_link_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click copy link button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableCopyLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableDuplicate(arg):
        '''
        ClickTableDuplicate : Click table duplicate
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click duplicate button
        xpath = Util.GetXpath({"locate": "duplicate_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click duplicate button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAnchoredComponentCopyLink(arg):
        '''
        ClickAnchoredComponentCopyLink : Click anchored component copy link
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click anchored component copy link button
        xpath = Util.GetXpath({"locate": "copy_link_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click copy link button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickAnchoredComponentCopyLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItem(arg):
        '''
        ClickEditItem : Click edit item
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click edit item
        xpath = Util.GetXpath({"locate": "edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit item", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspots(arg):
        '''
        ClickHotspots : Click hotspots button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click hotspots button
        xpath = Util.GetXpath({"locate": "hotspots_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspots button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickHotspots')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlaceholder(arg):
        '''
        ClickPlaceholder : Click placeholder button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click placeholder button
        xpath = Util.GetXpath({"locate": "placeholder_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click placeholder button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickPlaceholder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditTexts(arg):
        '''
        ClickEditTexts : Click edit texts button
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click edit texts button
        xpath = Util.GetXpath({"locate": "edit_texts_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit texts button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditTexts')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotsTab(arg):
        '''
        ClickHotspotsTab : Click hotspots tab
            Input argu :
                tab - link / reminder / add_to_cart / reminder_and_cart / popup
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click hotspots tab
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspots tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickHotspotsTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableDone(arg):
        '''
        ClickTableDone : Click done button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click done button
        xpath = Util.GetXpath({"locate": "done_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click done button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTableClose(arg):
        '''
        ClickTableClose : Click close button
            Input argu : N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##Click close button
        xpath = Util.GetXpath({"locate": "close_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTableClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotsAddNew(arg):
        '''
        ClickHotspotsAddNew : Click hotspots add new button
            Input argu :
                tab - link / reminder / add_to_cart / reminder_and_cart / popup
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click hotspots add new button
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspots add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickHotspotsAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlaceholderTab(arg):
        '''
        ClickPlaceholderTab : Click placeholder tab
            Input argu :
                tab - text / image
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click placeholder tab
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click placeholder tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickPlaceholderTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlaceholderAddNew(arg):
        '''
        ClickPlaceholderAddNew : Click placeholder add new button
            Input argu :
                tab - text / image
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click placeholder add new button
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click placeholder add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickPlaceholderAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteAnchoredComponent(arg):
        '''
        ClickDeleteAnchoredComponent : Click delete anchored component
                Input argu :
                    anchored_component - anchored component
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        anchored_component = arg["anchored_component"]

        if anchored_component:
            ##Delete specific anchored component
            xpath = Util.GetXpath({"locate": "specific_anchored_component_delete"})
            xpath = xpath.replace('anchored_component_to_be_replace', anchored_component)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete specific anchored component", "result": "1"})

        else:
            ##Delete all anchored component
            xpath = Util.GetXpath({"locate": "anchored_component_field"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
            xpath = Util.GetXpath({"locate": "delete_all_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete all anchored component", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickDeleteAnchoredComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDuplicatePlatform(arg):
        '''
        SelectDuplicatePlatform : Select platform to duplicate
            Input argu :
                platform - app / rw / lite / pc
            Return code : 1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        platform = arg["platform"]

        ##Select platform
        xpath = Util.GetXpath({"locate": platform})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select platform: " + platform, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.SelectDuplicatePlatform')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTotalItemsDisplayNumberArrow(arg):
        '''
        ClickTotalItemsDisplayNumberArrow : Click total items display number arrow button
                Input argu :
                    type - up / down
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click arrow button
        xpath = Util.GetXpath({"locate": "arrow_button"})
        xpath = xpath.replace("type_to_be_replaced", type)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click total items display number " + type + " arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTotalItemsDisplayNumberArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDisplayNumberPerRow(arg):
        '''
        SelectDisplayNumberPerRow : Select display number per row
                Input argu :
                    number - number per row
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Select display number per row
        xpath = Util.GetXpath({"locate": "number_per_row"})
        xpath = xpath.replace("number_to_be_replaced", number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select display number per row", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.SelectDisplayNumberPerRow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTextContent(arg):
        '''
        ClickTextContent : Click text content field
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click text content field
        xpath = Util.GetXpath({"locate": "text_content"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click text content field", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickTextContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseEditor(arg):
        '''
        ClickCloseEditor : Click close editor button
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click close editor button
        xpath = Util.GetXpath({"locate": "close_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close editor button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickCloseEditor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditPlaceHolderTableArrow(arg):
        '''
        ClickEditPlaceHolderTableArrow : Click edit placeholder table arrow button
                Input argu :
                    type - up / down
                    field_type - font_size_ratio / image_size_ratio
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        field_type = arg["field_type"]

        ##Click arrow button
        xpath = Util.GetXpath({"locate": field_type})
        xpath = xpath.replace("type_to_be_replaced", type)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click font size ratio " + type + " arrow button", "result": "1"})

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditPlaceHolderTableArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectVerticalAlignment(arg):
        '''
        SelectVerticalAlignment : Select vertical alignment
                Input argu :
                    option - vertical alignment option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select vertical alignment option
        xpath = Util.GetXpath({"locate": "vertical_alignment_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click vertical alignment dropdown", "result": "1"})

        xpath = Util.GetXpath({"locate": "vertical_alignment_option"})
        xpath = xpath.replace("option_to_be_replaced", option)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select vertical alignment option " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.SelectVerticalAlignment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectHorizontalAlignment(arg):
        '''
        SelectHorizontalAlignment : Select horizontal alignment
                Input argu :
                    option - horizontal alignment option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select horizontal alignment option
        xpath = Util.GetXpath({"locate": "horizontal_alignment_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click horizontal alignment dropdown", "result": "1"})

        xpath = Util.GetXpath({"locate": "horizontal_alignment_option"})
        xpath = xpath.replace("option_to_be_replaced", option)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select horizontal alignment option " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.SelectHorizontalAlignment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTextAlignment(arg):
        '''
        SelectTextAlignment : Select text alignment
                Input argu :
                    option - text alignment option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select text alignment option
        xpath = Util.GetXpath({"locate": "text_alignment_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click text alignment dropdown", "result": "1"})

        xpath = Util.GetXpath({"locate": "text_alignment_option"})
        xpath = xpath.replace("option_to_be_replaced", option)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select text alignment option " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.SelectTextAlignment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditHotSpotsTableArrow(arg):
        '''
        ClickEditHotSpotsTableArrow : Click edit hotspots table arrow button
                Input argu :
                    type - up / down
                    field_type - shop_id / item_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        field_type = arg["field_type"]

        ##Click arrow button
        xpath = Util.GetXpath({"locate": field_type})
        xpath = xpath.replace("type_to_be_replaced", type)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type + " arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditHotSpotsTableArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItemsTableArrow(arg):
        '''
        ClickEditItemsTableArrow : Click edit items table arrow button
                Input argu :
                    type - up / down
                    field_type - shop_id / item_id / item_campaign_stock / change_item_position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        field_type = arg["field_type"]

        ##Click arrow button
        xpath = Util.GetXpath({"locate": field_type})
        xpath = xpath.replace("type_to_be_replaced", type)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type + " arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditItemsTableArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditTextsTableArrow(arg):
        '''
        ClickEditTextsTableArrow : Click edit texts table arrow button
                Input argu :
                    type - up / down
                    field_type - font_size_ratio
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        field_type = arg["field_type"]

        ##Click arrow button
        xpath = Util.GetXpath({"locate": field_type})
        xpath = xpath.replace("type_to_be_replaced", type)
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type + " arrow button", "result": "1"})

        ##Click white space
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageButton.ClickEditTextsTableArrow')


class AdminCampaignViewEditPageComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column
                input_content - input content
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignViewEditPageComponent.InputToColumn')
