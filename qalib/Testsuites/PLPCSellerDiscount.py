import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCSellerDiscount Precondition =="
        dumplogger.info("== Setup PLPCSellerDiscount Precondition ==")

    def test_PLPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCSellerDiscount-01.xml")

    def test_PLPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCSellerDiscount-02.xml")

    def test_PLPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCSellerDiscount-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCSellerDiscount Post condition =="
        dumplogger.info("== Setup PLPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
