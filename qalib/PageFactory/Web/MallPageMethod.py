#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 MallPageMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time

##Import Framework common library
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import DecoratorHelper

##Import Web library
import BaseUICore
import BaseUILogic
import CommonMethod
import HomePageMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class MallPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOfficialShopCategoryExist(arg):
        ''' CheckOfficialShopCategoryExist : Check official shop category exist
                Input argu :
                    page_type - page type (category / mall_page)
                    category_name - category name
                    project - project
                    image - image name
                    env - staging / test
                    swipe_time - swipe time
                    is_click - click image or not
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        match_result = 1
        page_type = arg["page_type"]
        category_name = arg["category_name"]
        project = arg["project"]
        image = arg["image"]
        env = arg["env"]
        swipe_time = arg["swipe_time"]
        is_click = arg["is_click"]

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking official shop category exist" % (retry_count))

            ##Close staging and open
            time.sleep(5)
            BaseUICore.DeInitialWebDriver({})
            BaseUICore.InitialWebDriver({"browser":"chrome","result": "1"})
            CommonMethod.ShopeeLaunchWebSite({"env":env, "result": "1"})
            time.sleep(10)

            if page_type == "mall_page":

                ##Vertical move to mall section
                time.sleep(5)
                BaseUILogic.MouseScrollEvent({"x": "0", "y": "1000", "result": "1"})
                BaseUILogic.MouseScrollEvent({"x": "0", "y": "0", "result": "1"})
                CommonMethod.ScrollToSection({"section":"mall_section", "result": "1"})
                time.sleep(10)

                ##Enter to official shop section
                xpath = Util.GetXpath({"locate":"mall_shop_see_more"})
                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mall shop see more btn", "result": "1"})
                    time.sleep(5)
                    BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
                    time.sleep(10)

                    ##Click specific official shop category
                    xpath = Util.GetXpath({"locate":"official_shop_category"})
                    xpath = xpath.replace("string_to_be_replaced", category_name)
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click official shop category -> " + category_name, "result": "1"})
                    time.sleep(5)

                    if swipe_time:
                        ##Swipe trending shop scroll list
                        for retry_count in range(int(swipe_time)):
                            xpath = Util.GetXpath({"locate":"right_button"})
                            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to switch right", "result": "1"})
                            time.sleep(2)

            elif page_type == "category":

                ##Vertical move to category section
                CommonMethod.ScrollToSection({"section":"category_section", "result": "1"})

                ##Swipe the category list
                HomePageMethod.HomePage.SwitchCategoryListToLastPage({"result": "1"})

                ##Click specific category
                HomePageMethod.HomePage.ClickCategory({"category_name":category_name, "result": "1"})
                time.sleep(3)

                if swipe_time:
                    ##Swipe category shop scroll list
                    for retry_count in range(int(swipe_time)):
                        xpath = Util.GetXpath({"locate":"right_button"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to switch right", "result": "1"})
                        time.sleep(2)

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode":"web", "project":project, "image":image, "env":env, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether official shop category is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("PC official shop category image/GIF matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env":"staging", "mode":"web", "image":image, "threshold":"0.8", "is_click":"yes", "result": "1"})
                break
            else:
                dumplogger.info("PC official shop category image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'MallPage.CheckOfficialShopCategoryExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTrendingShopImageExist(arg):
        ''' CheckTrendingShopImageExist : Check trending shop image exists (Trending shop section must remain exists in shop category page)
                Input argu :
                    category_name - category name
                    project - project
                    image - image name
                    env - staging / test
                    index - index
                    is_click - click image or not
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        match_result = 1
        category_name = arg["category_name"]
        project = arg["project"]
        image = arg["image"]
        env = arg["env"]
        index = arg["index"]
        is_click = arg["is_click"]

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking trending shop image exist" % (retry_count))

            ##Close staging and open
            CommonMethod.ReopenShopeeWebsite({"result": "1"})
            BaseUILogic.MouseScrollEvent({"x": "0", "y": "1000", "result": "1"})
            BaseUILogic.MouseScrollEvent({"x": "0", "y": "0", "result": "1"})

            ##Vertical move to mall section
            CommonMethod.ScrollToSection({"section":"mall_section", "result": "1"})
            time.sleep(10)

            ##Enter to official shop section
            xpath = Util.GetXpath({"locate":"mall_shop_see_more"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mall shop see more btn", "result": "1"})
                time.sleep(5)
                BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
                time.sleep(10)

                ##Click specific official shop category
                xpath = Util.GetXpath({"locate":"official_shop_category"})
                xpath = xpath.replace("string_to_be_replaced", category_name)
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click official shop category -> " + category_name, "result": "1"})
                time.sleep(5)

                ##Click trending shop see more
                xpath = Util.GetXpath({"locate":"trending_shop_see_more"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click trending shop see more btn", "result": "1"})
                time.sleep(5)

                ##Choose index
                if index:
                    xpath = Util.GetXpath({"locate":"index_btn"})
                    xpath = xpath.replace("string_to_be_replaced", index)
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click index -> " + index, "result": "1"})
                    time.sleep(3)

            ##Match image
            match_result = XtFunc.MatchPictureImg({"mode":"web", "project":project, "image":image, "env":env, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether trending shop is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("PC trending shop image/GIF matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env":"staging", "mode":"web", "image":image, "threshold":"0.9", "is_click":"yes", "result": "1"})
                break
            else:
                dumplogger.info("PC trending shop image/GIF matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'MallPage.CheckTrendingShopImageExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBrandOfWeekExist(arg):
        ''' CheckBrandOfWeekExist : Check brand of week exists
                Input argu :
                    brand_title - brand of week title
                    brand_section - brand section
                    project - project
                    image - image name
                    env - staging / test
                    is_click - click image or not
                    swipe_time - scroll swipe time
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        match_result = 1
        brand_title = arg["brand_title"]
        brand_section = arg["brand_section"]
        project = arg["project"]
        image = arg["image"]
        env = arg["env"]
        is_click = arg["is_click"]
        swipe_time = arg["swipe_time"]

        for retry_count in range(1, 10):
            dumplogger.info("Start round: %s for checking banner exist" % (retry_count))

            ##Close staging and open
            CommonMethod.ReopenShopeeWebsite({"result": "1"})
            BaseUILogic.MouseScrollEvent({"x": "0", "y": "1000", "result": "1"})
            BaseUILogic.MouseScrollEvent({"x": "0", "y": "1000", "result": "1"})

            ##Move to official shop section and enter
            ##Vertical move to mall section
            BaseUILogic.MouseScrollEvent({"x": "0", "y": "1000", "result": "1"})
            BaseUILogic.MouseScrollEvent({"x": "0", "y": "0", "result": "1"})
            CommonMethod.ScrollToSection({"section":"mall_section", "result": "1"})
            time.sleep(10)

            ##Enter to official shop section
            xpath = Util.GetXpath({"locate":"mall_shop_see_more"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mall shop see more btn", "result": "1"})
                time.sleep(5)
                BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
                time.sleep(10)

                ##If there's brand title setting
                if brand_title:
                    xpath = Util.GetXpath({"locate":"brand_scroll_list"})
                    xpath = xpath.replace("title_to_be_replaced", brand_title)

                    ##Check brand title exists and scroll to specific section
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                        CommonMethod.ScrollToSection({"section":brand_section, "result": "1"})

                        if swipe_time:
                            ##Swipe brand scroll list
                            for retry_count in range(int(swipe_time)):
                                xpath = Util.GetXpath({"locate":brand_section + "_right_button"})
                                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to switch right", "result": "1"})
                                time.sleep(2)

                ##If no brand title, scroll to brand of week component section
                else:
                    CommonMethod.ScrollToSection({"section":"brand_of_week_component_section", "result": "1"})

            ##Match image
            if image:
                match_result = XtFunc.MatchPictureImg({"mode":"web", "project":project, "image":image, "env":env, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether brand of week is exist or not, break the loop as long as the result is expected
            if match_result:
                dumplogger.info("brand of week matching successful at %d run!!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env":"staging", "mode":"web", "image":image, "threshold":"0.9", "is_click":"yes", "result": "1"})
                break
            else:
                dumplogger.info("brand of week matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'MallPage.CheckBrandOfWeekExist')

class MallPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAlphabet(arg):
        ''' ClickAlphabet : Click the alphabet
                Input argu :
                    alphabet - alphabet
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        alphabet = arg["alphabet"]

        ##Click the alphabet
        xpath = Util.GetXpath({"locate":"alphabet_text"})
        xpath = xpath.replace("string_to_be_replaced", alphabet)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click alphabet: " + alphabet, "result": "1"})

        OK(ret, int(arg['result']), 'MallPageButton.ClickAlphabet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBrandName(arg):
        ''' ClickBrandName : Click brand name
                Input argu :
                    brand_name - brand name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        brand_name = arg["brand_name"]

        ##Click brand name
        xpath = Util.GetXpath({"locate":"brand_name"})
        xpath = xpath.replace("string_to_be_replaced", brand_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click brand name: " + brand_name, "result": "1"})

        OK(ret, int(arg['result']), 'MallPageButton.ClickBrandName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToCategoryBarDropDown(arg):
        ''' MoveToCategoryBarDropDown : Move to category bar drop down and see all category
                Input argu :
                    N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Move to category bar drop down
        xpath = Util.GetXpath({"locate":"category_bar_drop_down"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})

        OK(ret, int(arg['result']), 'MallPageButton.MoveToCategoryBarDropDown')
