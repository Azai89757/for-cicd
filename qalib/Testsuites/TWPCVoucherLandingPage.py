import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCVoucherLandingPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCVoucherLandingPage Precondition =="
        dumplogger.info("== Setup TWPCVoucherLandingPage Precondition ==")

    def test_TWPCVoucherLandingPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCVoucherLandingPage-01.xml")

    def test_TWPCVoucherLandingPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCVoucherLandingPage-02.xml")

    def test_TWPCVoucherLandingPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCVoucherLandingPage-03.xml")

    def test_TWPCVoucherLandingPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCVoucherLandingPage-04.xml")

    def test_TWPCVoucherLandingPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCVoucherLandingPage-05.xml")

    def test_TWPCVoucherLandingPage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCVoucherLandingPage-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCVoucherLandingPage Post condition =="
        dumplogger.info("== Setup TWPCVoucherLandingPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
