import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMePage Precondition =="
        dumplogger.info("== Setup MYPCMePage Precondition ==")

    def test_MYPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMePage Post condition =="
        dumplogger.info("== Setup MYPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
