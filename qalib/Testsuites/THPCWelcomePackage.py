import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCWelcomePackage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCWelcomePackage Precondition =="
        dumplogger.info("== Setup THPCWelcomePackage Precondition ==")

    def test_THPCWelcomePackage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCWelcomePackage-01.xml")

    def test_THPCWelcomePackage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCWelcomePackage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCWelcomePackage Post condition =="
        dumplogger.info("== Setup THPCWelcomePackage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
