import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCListing Precondition =="
        dumplogger.info("== Setup MXPCListing Precondition ==")

    def test_MXPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCListing-01.xml")

    def test_MXPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCListing-02.xml")

    def test_MXPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCListing Post condition =="
        dumplogger.info("== Setup MXPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
