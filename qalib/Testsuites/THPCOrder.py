import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCOrder Precondition =="
        dumplogger.info("== Setup THPCOrder Precondition ==")

    def test_THPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-01.xml")

    def test_THPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-02.xml")

    def test_THPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-03.xml")

    def test_THPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-04.xml")

    def test_THPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-05.xml")

    def test_THPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-06.xml")

    def test_THPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-07.xml")

    def test_THPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-08.xml")

    def test_THPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-09.xml")

    def test_THPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-10.xml")

    def test_THPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-11.xml")

    def test_THPCOrder12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-12.xml")

    def test_THPCOrder13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-13.xml")

    def test_THPCOrder14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-14.xml")

    def test_THPCOrder15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-15.xml")

    def test_THPCOrder16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-16.xml")

    def test_THPCOrder17(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-17.xml")

    def test_THPCOrder18(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-18.xml")

    def test_THPCOrder19(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-19.xml")

    def test_THPCOrder20(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-20.xml")

    def test_THPCOrder21(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-21.xml")

    def test_THPCOrder22(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-22.xml")

    def test_THPCOrder23(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCOrder-23.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCOrder Post condition =="
        dumplogger.info("== Setup THPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
