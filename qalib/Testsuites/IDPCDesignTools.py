import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCDesignTools(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCDesignTools Precondition =="
        dumplogger.info("== Setup IDPCDesignTools Precondition ==")

    def test_IDPCDesignTools01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCDesignTools-01.xml")

    def test_IDPCDesignTools02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCDesignTools-02.xml")

    def test_IDPCDesignTools03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCDesignTools-03.xml")

    def test_IDPCDesignTools04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCDesignTools-04.xml")

    def test_IDPCDesignTools05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCDesignTools-05.xml")

    def test_IDPCDesignTools06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCDesignTools-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCDesignTools Post condition =="
        dumplogger.info("== Setup IDPCDesignTools Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
