import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSellerDiscount Precondition =="
        dumplogger.info("== Setup THPCSellerDiscount Precondition ==")

    def test_THPCSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-01.xml")

    def test_THPCSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-02.xml")

    def test_THPCSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-03.xml")

    def test_THPCSellerDiscount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-04.xml")

    def test_THPCSellerDiscount05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-05.xml")

    def test_THPCSellerDiscount06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-06.xml")

    def test_THPCSellerDiscount07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSellerDiscount-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSellerDiscount Post condition =="
        dumplogger.info("== Setup THPCSellerDiscount Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
