#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ChatadminAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import datetime
import time

##Import common library
import Config
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter

##Import DB library
from api import APICommonMethod
from db import DBCommonMethod
import HttpAPILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class ChatAdminAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreChatAdminCookie(arg):
        '''
        StoreChatAdminCookie : Assign chat Admin cookie to API header
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get current country
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]

        ##Send SQL to get DL admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_chat", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Bulid and Assign cookie to API header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'ChatAdminAPI.StoreChatAdminCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddNewBlackListKeyword(arg):
        '''
        AddNewBlackListKeyword : Setting new keyword name
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        keyword = arg["keyword"]
        ret = 1

        ##Setting time and new blacklistkeyword
        settime = str(datetime.datetime.today())
        variable = "autoapi_rain" + settime

        ##Assign keyword from argument to global
        GlobalAdapter.CommonVar._DynamicCaseData_["keyword"] = variable

        ##Set payload value from global
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "keyword", "target": "keyword", "time_deviation": "", "result": "1"})

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        OK(ret, int(arg['result']), 'ChatAdminAPI.AddNewBlackListKeyword')
