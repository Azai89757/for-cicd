import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCIcon(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCIcon Precondition =="
        dumplogger.info("== Setup IDPCIcon Precondition ==")

    def test_IDPCIcon01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCIcon-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCIcon Post condition =="
        dumplogger.info("== Setup IDPCIcon Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
