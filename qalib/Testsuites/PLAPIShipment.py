import unittest
from Config import dumplogger
from FrameWorkBase import *


class PLAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAPIShipment Precondition =="
        dumplogger.info("== Setup PLAPIShipment Precondition ==")

    def test_PLAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/PLAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAPIShipment Post condition =="
        dumplogger.info("== Setup PLAPIShipment Post condition ==")
