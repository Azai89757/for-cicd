﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidProductCardMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidProductCardButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstLike(arg):
        '''
        ClickFirstLike : Click First Like Btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click first like btn
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":Util.GetXpath({"locate":"like_btn"}), "message":"Click first like btn","result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductCardButton.ClickFirstLike')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def LongPressProductCard(arg):
        '''
        LongPressProductCard : Long press product card
                Input argu :
                    location - which product card you want to long press (e.g 1 for first product card, 2 for second product card)
                    type - standard/simplified
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Long press product card
        type = arg['type']
        location = arg['location']

        xpath = Util.GetXpath({"locate":type})
        xpath = xpath.replace("location_to_be_replaced", location)

        AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductCardButton.LongPressProductCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSimiliarIcon(arg):
        '''
        ClickSimiliarIcon : Click similiar icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click similiar icon
        xpath = Util.GetXpath({"locate":"similiar_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click similiar icon","result": "1"})

        OK(ret, int(arg['result']), 'AndroidProductCardButton.ClickSimiliarIcon')
