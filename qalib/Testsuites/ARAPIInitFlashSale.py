import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARAPIInitFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIInitFlashSale Precondition =="
        dumplogger.info("== Setup ARAPIInitFlashSale Precondition ==")

    def test_ARAPIInitFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/ARAPIInitFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIInitFlashSale Post condition =="
        dumplogger.info("== Setup ARAPIInitFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
