import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpListingAdmin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpListingAdmin Precondition =="
        dumplogger.info("== Setup TWHttpListingAdmin Precondition ==")

    def test_TWHttpListingAdmin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpListingAdmin-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpListingAdmin Post condition =="
        dumplogger.info("== Setup TWHttpListingAdmin Post condition ==")
