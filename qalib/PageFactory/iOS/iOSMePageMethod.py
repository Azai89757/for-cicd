﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSMePageMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time

##Import framework library
import Config
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import iOSBaseUICore
import iOSBaseUILogic
import DecoratorHelper
import GlobalAdapter

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class iOSHomePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HomePageNavigation(arg):
        ''' HomePageNavigation : Click on a icon at the buttom of the home page
                Input argu : type - which progress tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]
        dumplogger.info("type = " + type)

        ##Tabs mapping
        locate = Util.GetXpath({"locate":type})

        ##Go to different tab
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in the me page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'iOSHomePage.HomePageNavigation -> ')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRandomUsername(arg):
        ''' GetRandomUsername : Get random username in homepage
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Get random username in homepage
        locate = Util.GetXpath({"locate":"account_name"})
        iOSBaseUILogic.iOSGetAndReserveElements({"method":"xpath", "locate":locate, "reservetype":"random_username", "result": "1"})

        OK(ret, int(arg['result']), 'iOSHomePage.GetRandomUsername')


class iOSMainPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchRole(arg):
        ''' SwitchRole : Switch role with buyer/seller
                Input argu :
                    type - buyer/seller
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Switch role
        locate = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click selling tab on Me tab page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'iOSMainPage.SwitchRole -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BuyerMePageNavigation(arg):
        ''' BuyerMePageNavigation : Click on a sub-feature in the buyer me page
                Input argu :
                    type - which progress tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        navi_type = arg["type"]

        time.sleep(2)
        top_bar_item = ["cart", "chat", "gear", "avatar", "followers", "following"]

        ##Check if is top bar item
        if navi_type not in top_bar_item:
            iOSBaseUILogic.iOSRelativeMove({"direction":"down", "times":"3", "result": "1"})

        ##Click tab
        locate = Util.GetXpath({"locate":navi_type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click " + navi_type + " tab on Me tab page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMainPage.BuyerMePageNavigation -> ' + navi_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUsernameText(arg):
        ''' ClickUsernameText : Click on username in the buyer me page
                Input argu :
                    username - username
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        username = arg['username']

        time.sleep(2)

        ##Click username
        locate = Util.GetXpath({"locate":"username"})
        locate = locate.replace('replaced_text', username)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click username on Me tab page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMainPage.ClickUsernameText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHeaderBackground(arg):
        '''ClickHeaderBackground : Click header background
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click background
        locate = Util.GetXpath({"locate":"background"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click header", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMainPage.ClickHeaderBackground')


class iOSMyAccountPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MyAccountNavigation(arg):
        ''' MyAccountNavigation : Click on a sub-feature under the "My account" tab
                Input argu : type - which progress tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]
        dumplogger.info("type = " + type)

        locate = Util.GetXpath({"locate":type})

        ##Go to different sub-page
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click sub-feature in the me page", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'MyAccountNavigation -> ' + type)


class iOSUserProfilePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeUserName(arg):
        ''' ChangeUserName : Change Username
                Input argu :
                    usename - username
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        username = arg["username"]
        ret = 1

        ##Click clear input btn
        locate = Util.GetXpath({"locate":"clear_btn"})
        if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"}):
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click input field x btn", "result": "1"})

        ##Input username
        locate = Util.GetXpath({"locate":"username_field"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":username, "result": "1"})

        ##Click save
        locate = Util.GetXpath({"locate":"save_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.ChangeUserName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectPhotoFromLibrary(arg):
        '''SelectPhotoFromLibrary : Select photo from library
                Input argu : profile_image - profile image
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        profile_image = arg['profile_image']

        ##Click library
        locate = Util.GetXpath({"locate":"library"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click choose from library", "result": "1"})

        ##Click image
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":profile_image, "threshold":"0.9", "is_click":"yes", "result": "1"})

        ##Click confirm
        locate = Util.GetXpath({"locate":"confirm"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click confirm btn", "result": "1"})

        ##Click save
        locate = Util.GetXpath({"locate":"save"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click save btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.SelectPhotoFromLibrary')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeGender(arg):
        '''ChangeGender : change gender
                Input argu : gender - male, female, other
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        gender = arg['gender']

        ##Click select gender
        locate = Util.GetXpath({"locate":"gender"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click select gender", "result": "1"})
        time.sleep(10)

        ##Input gender
        locate = Util.GetXpath({"locate":"pickerwheel"})
        gender_text = Util.GetXpath({"locate":gender})
        iOSBaseUICore.iOSInput({"method":"xpath_noclear", "locate":locate, "string":gender_text, "result": "1"})

        time.sleep(2)

        ##Click done
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"pickerwheel_done", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.ChangeGender')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBirth(arg):
        '''ChangeBirth : change birth
                Input argu : type - custom, time_delta
                             year - year
                             month - month
                             day - day
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        type = arg['type']
        unit = [u'年', u'月', u'日']
        date = []

        ##Check if type is time delta or custom time
        if type == 'custom':
            ##Get custom date
            year = arg['year']
            month = arg['month']
            day = arg['day']

            ##save to list
            date = [year, month, day]
        elif type == 'time_delta':
            ##Get date using time delta
            days = int(arg['days'])
            birth = XtFunc.GetCurrentDateTime("%d-%m-%Y", days=days)

            ##reverse the list
            date = birth.split('-')
            date.reverse()
        else:
            ret = 0

        ##Click select birth
        locate = Util.GetXpath({"locate":"birth"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click select birth", "result": "1"})

        ##Input birth to pickerwheel by order -> year, month, date with unit
        locate = Util.GetXpath({"locate":"pickerwheel"})
        iOSBaseUICore.iOSGetElements({"method":"xpath", "locate":locate, "mode":"multi", "result": "1"})
        for (index, element) in enumerate(GlobalAdapter.CommonVar._PageElements_):
            iOSBaseUICore.iOSInput({"method":"element", "locate":element, "string":date[index] + unit[index], "result": "1"})

        ##Click done
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"pickerwheel_done", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.ChangeBirth')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBirth(arg):
        '''CheckBirth : Check birth
                Input argu : days - timedelta days
                             method - locate element method
                             locate - element locate
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Get current date
        birth = XtFunc.GetCurrentDateTime("%d-%m-%Y")

        ##Get and compare setup date
        locate = Util.GetXpath({"locate":"current_birth"})
        iOSBaseUILogic.iOSGetAndCheckElements({"method":"xpath", "locate":locate, "string":birth, "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.CheckBirth')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeEmail(arg):
        '''ChangeEmail : Change email
                Input argu : email - email
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        email = arg['email']

        ##Click email
        locate = Util.GetXpath({"locate":"email"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click email", "result": "1"})

        locate = Util.GetXpath({"locate":"verification_field"})
        is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"})

        if is_exist:
            ##Input verification code
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":"Auto123456", "result": "1"})

            ##Click submit
            locate = Util.GetXpath({"locate":"submit"})
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click submit", "result": "1"})

        ##Input email
        locate = Util.GetXpath({"locate":"email_field"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string": email, "result": "1"})

        ##Click complete
        locate = Util.GetXpath({"locate":"complete_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click complete", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.ChangeEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPhoneNumber(arg):
        '''ClickPhoneNumber : Click phone number cell
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click phone number cell
        locate = Util.GetXpath({"locate":"phone_number_cell"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click phone number cell", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.ClickPhoneNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputName(arg):
        '''InputName : Input name
                Input argu : name - name
                             condition - condition
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        condition = arg['condition']
        name = arg['name']

        if condition == "more_than_100":
            input_string = name
            for index in range(0, (100 / len(name) + 1)):
                input_string += name
        else:
            input_string = name

        ##Click field twice
        input_field = Util.GetXpath({"locate":"name_field"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":input_field, "message":"Click input field", "result": "1"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":input_field, "message":"Click input field", "result": "1"})
        time.sleep(2)

        ##click select all and click delete on keyboard
        select_all = Util.GetXpath({"locate":"select_all"})
        is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":select_all, "passok": "0", "result": "1"})
        if is_exist:
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":select_all, "message":"Click select all", "result": "1"})
            iOSBaseUICore.iOSKeyboardAction({"actiontype":"delete", "result": "1"})

        ##Input text
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":input_field, "string":input_string, "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.InputName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToSubPage(arg):
        '''GoToSubPage : go to sub page in user profile page
                Input argu : subpage - sub page
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        subpage = arg['subpage']

        ##Click subpage
        locate = Util.GetXpath({"locate":subpage})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"go to " + subpage, "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.GoToSubPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePassword(arg):
        '''ChangePassword : Change password
                Input argu :
                    old_password - old password
                    new_password - new password
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        old_password = arg['old_password']
        new_password = arg['new_password']

        ##Click password cell
        locate = Util.GetXpath({"locate":"password_cell"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click password cell", "result": "1"})

        ##Input password
        locate = Util.GetXpath({"locate":"password"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":old_password, "result": "1"})

        ##Click confirm
        locate = Util.GetXpath({"locate":"confirm_password"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click confirm password", "result": "1"})

        ##Input verification code
        locate = Util.GetXpath({"locate":"verification_code"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":"123456", "result": "1"})

        ##Click confirm
        locate = Util.GetXpath({"locate":"confirm_verfication"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click confirm verification", "result": "1"})

        ##Input new password
        locate = Util.GetXpath({"locate":"new_password"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":new_password, "result": "1"})

        ##Input new confirm password
        locate = Util.GetXpath({"locate":"confirm_new_password"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":new_password, "result": "1"})

        ##Click confirm btn
        locate = Util.GetXpath({"locate":"confirm_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSUserProfilePage.ChangePassword from' + old_password + ' to ' + new_password)


class iOSMyLikePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductDetailPage(arg):
        ''' GoToProductDetailPage : Go to product detail page from click product in my like page
                Input argu : name - product name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        product_name = arg["name"]

        ##Go to product detail page
        locate = Util.GetXpath({"locate":"product"}).replace("product_name_to_be_replace", product_name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click product from my like page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikePage.GoToProductDetailPage -> ' + product_name)


class iOSFollowPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUsername(arg):
        '''ClickUsername : Click username
                inpur Argu : username - username
                             type - following, follower
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        username = arg['username']
        type = arg['type']

        ##Click username
        locate = Util.GetXpath({"locate":type})
        locate = locate.replace("replaced_text", username)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click " + username + " in following list", "result": "1"})

        OK(ret, int(arg['result']), "iOSFollowPage.ClickUsername")


class iOSMyLikesPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchBarText(arg):
        '''InputSearchBarText : Input search bar text
                Input argu : text - text
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        text = arg['text']

        ##Input search bar
        locate = Util.GetXpath({"locate":"search_bar"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":text, "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikesPage.InputSearchBarText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProduct(arg):
        '''SearchProduct : Search product
                Input argu : product_name - product name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Input product name
        locate = Util.GetXpath({"locate":"search_bar"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":product_name, "result": "1"})

        ##Press Enter key
        time.sleep(1)
        iOSBaseUICore.iOSKeyboardAction({"actiontype":"enter", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikesPage.SearchProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCard(arg):
        '''ClickProductCard : Click product card
                Input argu : product_name - product name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product
        locate = Util.GetXpath({"locate":"product_name"})
        locate = locate.replace('replaced_text', product_name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click product card", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikesPage.ClickProductCard')


class iOSMyAddressPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCity(arg):
        '''SelectCity: select city
                Input argu : city_name - city name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        city_name = arg['city_name']

        ##Click select city btn
        locate = Util.GetXpath({"locate":"select_city_btn"})
        iOSBaseUILogic.iOSGetAndClickElementByCoordinate({"method":"id", "locate":locate, "result": "1"})

        ##Click city
        locate = Util.GetXpath({"locate":"city"})
        locate = locate.replace('replace_text', city_name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click city", "result": "1"})

        OK(ret, int(arg['result']), "iOSMyAddressPage.SelectCity")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDistrict(arg):
        '''SelectDistrict: select district
                Input argu : district - district
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        district = arg['district']

        ##Click select city btn
        locate = Util.GetXpath({"locate":"select_district_btn"})
        iOSBaseUICore.iOSGetElements({"method":"id", "locate":locate, "mode":"single", "result": "1"})
        element_location = GlobalAdapter.CommonVar._PageElements_.location
        iOSBaseUICore.iOSClickCoordinates({"x_cor":element_location['x'], "y_cor":element_location['y'], "result": "1"})

        ##Click city
        locate = Util.GetXpath({"locate":"district"})
        locate = locate.replace('replace_text', district)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click district", "result": "1"})

        OK(ret, int(arg['result']), "iOSMyAddressPage.SelectCity")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectWard(arg):
        '''SelectWard: select ward
                Input argu : ward - ward
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        ward = arg['ward']

        ##Click select city btn
        locate = Util.GetXpath({"locate":"select_ward_btn"})
        iOSBaseUICore.iOSGetElements({"method":"id", "locate":locate, "mode":"single", "result": "1"})
        element_location = GlobalAdapter.CommonVar._PageElements_.location
        iOSBaseUICore.iOSClickCoordinates({"x_cor":element_location['x'], "y_cor":element_location['y'], "result": "1"})

        ##Click city
        locate = Util.GetXpath({"locate":"ward"})
        locate = locate.replace('replace_text', ward)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click ward", "result": "1"})

        OK(ret, int(arg['result']), "iOSMyAddressPage.SelectWard")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTextField(arg):
        '''InputTextField : Input text field
                Input argu :  type - type of textfield
                              text - text to input
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        type = arg['type']
        text = arg['text']

        ##input text
        locate = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click" + type + "field", "result": "1"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":text, "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyAddressPage.InputTextField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddress(arg):
        '''ClickAddress : Click address
                Input argu : name - name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        name = arg['name']

        ##Click address
        locate = Util.GetXpath({"locate":"name"})
        locate = locate.replace('replace_text', name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click address", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyAddressPage.ClickAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteAddress(arg):
        '''DeleteAddress : delete address
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click delete btn
        locate = Util.GetXpath({"locate":"delete_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click delete btn", "result": "1"})

        ##Click delete btn
        locate = Util.GetXpath({"locate":"confirm_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyAddressPage.DeleteAddress')


class iOSMyLikesButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChat(arg):
        '''ClickChat : Click chat icon
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click chat icon using opencv
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"my_likes_chat_icon", "threshold":"0.9", "is_click":"no", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikeButton.ClickChat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShopping(arg):
        '''ClickGoShopping : Click go shopping btn
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click go shopping btn
        locate = Util.GetXpath({"locate":"go_shopping_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click go shopping btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikesButton.ClickGoShopping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelSearch(arg):
        '''ClickCancelSearch : Click cancel on search result
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click cancel btn
        locate = Util.GetXpath({"locate":"cancel_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMyLikesButton.ClickCancelSearch')


class iOSMePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPhoneNumberChange(arg):
        '''ClickPhoneNumberChange : Click change btn in change phone number page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click change btn
        locate = Util.GetXpath({"locate":"change"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click change btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickPhoneNumberChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEmailAgree(arg):
        '''ClickEmailConfirm : Click agree after change email
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click change btn
        locate = Util.GetXpath({"locate":"agree"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click agree btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickEmailAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmitAddress(arg):
        '''ClickSubmitAddress : Click submit btn in my address
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click submit btn
        locate = Util.GetXpath({"locate":"submit_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickSubmitAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewAddress(arg):
        '''ClickAddNewAddress : Click add new address btn
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click add new address btn
        locate = Util.GetXpath({"locate":"add_new_address_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click add new address", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickAddNewAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''ClickSave : Click save btn in user profile
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click save btn
        locate = Util.GetXpath({"locate":"save_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollow(arg):
        '''ClickFollow : Click Following username's following button in following list
                Input argu : username - username
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        username = arg['username']

        ##Click following btn
        locate = Util.GetXpath({"locate":"following_btn"})
        locate = locate.replace("replaced_text", username)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click " + username + " in following list", "result": "1"})

        OK(ret, int(arg['result']), "iOSMePageButton.ClickFollow")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''ClickOK : Click ok in pop up dialog
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click ok btn
        locate = Util.GetXpath({"locate":"ok_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFinish(arg):
        ''' ClickFinish : Click finish button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click finish button
        locate = Util.GetXpath({"locate":"finish_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"click finish button", "result":"1"})

        OK(ret, int(arg['result']), 'iOSMePageButton.ClickFinish')


class iOSEditAddressPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToEditAddress(arg):
        ''' ClickToEditAddress : Click a address field to edit an address in my address page
                Input argu :
                    keyword - for clicking any keyword on the addree field
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        keyword = arg["keyword"]
        ret = 0

        ##Click a address field to edit an address in my address page
        locate = Util.GetXpath({"locate":"address_to_be_edited"})
        locate_replace = locate.replace('address_to_be_edited', keyword)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate_replace, "message":"Click a address field to edit an address in my address page", "result":"1"})

        OK(ret, int(arg['result']), 'iOSEditAddressPage.ClickToEditAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddAddressDetailPage(arg):
        ''' AddAddressDetailPage : Add address detail page
                Input argu :
                    name - user name or company name
                    phone - phone number
                    city - city
                    area - area
                    postcode - postcode
                    address - address
                    set_default - open / close
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        name = arg["name"]
        phone = arg["phone"]
        city = arg["city"]
        area = arg["area"]
        postcode = arg["postcode"]
        address = arg["address"]
        set_default = arg["set_default"]

        ret = 1

        ##Input name/company name
        locate = Util.GetXpath({"locate":"name_icon"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":name, "result":"1"})

        ##Input phone number
        locate = Util.GetXpath({"locate":"phone_column"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":phone, "result":"1"})

        ##Choose a city
        locate = Util.GetXpath({"locate":"city_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click city list", "result":"1"})
        locate = Util.GetXpath({"locate":"city_choose"})
        locate_replace = locate.replace('city_to_be_choosed', city)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate_replace, "message":"Choose a city", "result":"1"})

        ##Choose a area
        locate = Util.GetXpath({"locate":"area_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click area list", "result":"1"})
        locate = Util.GetXpath({"locate":"area_choose"})
        locate_replace = locate.replace('area_to_be_choosed', area)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate_replace, "message":"Choose a area", "result":"1"})

        ##Input post code
        if postcode:
            if Config._TestCaseRegion_ == "TW":
                locate = Util.GetXpath({"locate":"postcode_column"})
                iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":postcode, "result":"1"})
            elif Config._TestCaseRegion_ == "VN" or Config._TestCaseRegion_ == "TH":
                locate = Util.GetXpath({"locate":"community_icon"})
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Choose a community", "result":"1"})
                locate = Util.GetXpath({"locate":"community_choose"})
                locate_replace = locate.replace('community_to_be_choosed', postcode)
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate_replace, "message":"Choose a area", "result":"1"})

        ##Input detail address
        locate = Util.GetXpath({"locate":"address_column"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":address, "result":"1"})

        if set_default == "open":
            locate = Util.GetXpath({"locate":"set_default"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click open default", "result":"1"})
            time.sleep(3)

        ##Click 完成
        iOSBaseUICore.iOSKeyboardAction({"actiontype":"button", "key":"完成", "result":"1"})

        ##Click finish
        iOSMePageButton.ClickFinish({"result":"1"})

        OK(ret, int(arg['result']), 'iOSEditAddressPage.AddAddressDetailPage')
