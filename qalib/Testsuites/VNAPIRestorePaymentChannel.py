import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNAPIRestorePaymentChannel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIRestorePaymentChannel Precondition =="
        dumplogger.info("== Setup VNAPIRestorePaymentChannel Precondition ==")

    def test_VNAPIRestorePaymentChannel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIRestorePaymentChannel-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIRestorePaymentChannel Post condition =="
        dumplogger.info("== Setup VNAPIRestorePaymentChannel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
