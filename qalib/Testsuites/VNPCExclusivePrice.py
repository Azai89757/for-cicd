import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCExclusivePrice(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCExclusivePrice Precondition =="
        dumplogger.info("== Setup VNPCExclusivePrice Precondition ==")

    def test_VNPCExclusivePrice01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCExclusivePrice-01.xml")

    def test_VNPCExclusivePrice02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCExclusivePrice-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCExclusivePrice Post condition =="
        dumplogger.info("== Setup VNPCExclusivePrice Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
