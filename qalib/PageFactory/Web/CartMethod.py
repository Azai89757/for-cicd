#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 CartMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class CartPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckAndCleanProductInCart(arg):
        '''
        CheckAndCleanProductInCart : Check if there is any product in cart, if so, delete it.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Clean max 20 product
        for count in range(20):

            ##Check if cart is empty
            xpath = Util.GetXpath({"locate":"msg_cart_empty"})

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("No product in cart, leave function.")
                break

            else:
                ##Click delete button
                xpath = Util.GetXpath({"locate":"delete_product_btn"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})
                dumplogger.info("Products exist in cart.. start to purge.")

        OK(ret, int(arg['result']), 'CartPage.CheckAndCleanProductInCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckAndCleanShopProductInCart(arg):
        '''
        CheckAndCleanShopProductInCart : Check if there is any shop product in cart, if so, delete it.
                Input argu :
                    shop_name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg['shop_name']

        ##Clean same shop max 20 product
        for count in range(20):

            ##Check if shop is empty
            xpath = Util.GetXpath({"locate": "shop_name"})
            xpath = xpath.replace("replace_shop_name", shop_name)

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

                ##Click delete button
                xpath = Util.GetXpath({"locate":"delete_product_btn"})
                xpath = xpath.replace("replace_shop_name", shop_name)
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})
                dumplogger.info("Products exist in cart.. start to purge.")

            else:
                dumplogger.info("No product in cart, leave function.")
                break

        OK(ret, int(arg['result']), 'CartPage.CheckAndCleanShopProductInCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def WaitCartLoadingIsComplete(arg):
        '''
        WaitCartLoadingIsComplete : Wait cart loading is complete
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Retry 5 times to wait cart page loading is complete
        for retry_count in range(5):

            ##Check cart buy now button or empty message exist
            xpath = Util.GetXpath({"locate":"cart_buy_btn_or_empty_msg_exist"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ret = 1
                break
            else:
                ##Refresh browser to load cart page again
                BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})

        OK(ret, int(arg['result']), 'CartPage.WaitCartLoadingIsComplete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ScrollToShop(arg):
        '''
        ScrollToShop : Scroll to target shop section
                Input argu :
                    shop_name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Check section is exist
        xpath = Util.GetXpath({"locate":"shop_name_section"})
        xpath = xpath.replace("replace_shop_name", shop_name)

        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

            ##Get section location
            BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"location", "mode":"single", "result": "1"})

            ##Get location y of section
            location_y_of_section = GlobalAdapter.CommonVar._PageAttributes_["y"]
            dumplogger.info("Section location y => " + str(location_y_of_section))

            ##Get top nav size
            xpath = Util.GetXpath({"locate":"home_top"})
            BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype":"size", "mode":"single", "result": "1"})

            ##Get height of top nav
            height_of_top_nav = GlobalAdapter.CommonVar._PageAttributes_["height"]
            dumplogger.info("Top nav height => " + str(height_of_top_nav))

            ##section_location_y subtract by top_nav_height
            location_y_of_section = location_y_of_section - height_of_top_nav

            ##Scroll down by section location y
            BaseUICore._WebDriver_.execute_script("window.scrollBy(0, " + str(location_y_of_section) + ")")
            dumplogger.info("Scroll by vertical number => " + str(location_y_of_section))

        else:
            ret = 0
            dumplogger.info("Cannot find shop name -> " + shop_name)

        OK(ret, int(arg['result']), 'CartPage.ScrollToShop -> ' + shop_name)

class CartPageProductSection:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputQuantity(arg):
        '''
        InputQuantity : Goto cart page and input a number of quantity for a product you want to change quantity
                Input argu :
                    product_name - product name
                    quantity - input quantity
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']
        quantity = arg["quantity"]

        ##Input a number of quantity for a product you want to change quantity
        xpath = Util.GetXpath({"locate":"input_quantity"})
        xpath = xpath.replace('replaced_text', product_name)

        ##Clear the textarea
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(3)

        ##Input quantity
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":quantity, "result": "1"})

        ##Click other place
        xpath = Util.GetXpath({"locate": "other_place"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click other place", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'CartPageProductSection.InputQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMinusQuantity(arg):
        '''
        ClickMinusQuantity : In cart page click minus quantity btn
                Input argu :
                    quantity - how many times to click
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        quantity = arg['quantity']
        product_name = arg['product_name']

        for click_times in range(int(quantity)):
            ##Click minus button
            xpath = Util.GetXpath({"locate":"minus_btn"})
            xpath = xpath.replace('replaced_text', product_name)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click minus button", "result": "1"})
            dumplogger.info("Click minus quantity %d times" % (click_times))
            time.sleep(2)

        OK(ret, int(arg['result']), 'CartPageProductSection.ClickMinusQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopCheckBox(arg):
        '''
        ClickShopCheckBox : Click shop checkbox in cart page
                Input argu :
                    shop_name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg['shop_name']

        ##Click item checkbox
        xpath = Util.GetXpath({"locate":"shop_checkbox"})
        xpath_shop_checkbox = xpath.replace('shop_name_to_be_replace', shop_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_shop_checkbox, "message":"Click shop checkbox => " + shop_name, "result": "1"})

        OK(ret, int(arg['result']), 'CartPageProductSection.ClickShopCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCheckBox(arg):
        '''
        ClickItemCheckBox : Click item checkbox in cart page
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click item checkbox
        xpath = Util.GetXpath({"locate":"checkbox"})
        xpath = xpath.replace('replaced_text', product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'CartPageProductSection.ClickItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddQuantity(arg):
        '''
        ClickAddQuantity : In cart page click add quantity btn
                Input argu :
                    product_name - product name
                    quantity - how many times to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']
        quantity = arg['quantity']

        for click_times in range(int(quantity)):
            ##Click add button
            xpath = Util.GetXpath({"locate":"add_btn"})
            xpath = xpath.replace('replaced_text', product_name)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click add button", "result": "1"})
            dumplogger.info("Click add quantity %d times" % (click_times))
            time.sleep(3)

        OK(ret, int(arg['result']), 'CartPageProductSection.ClickAddQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopName(arg):
        '''
        ClickShopName : Click shop name
                Input argu :
                    shop_name - shop name
                Retutn code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg['shop_name']

        ##Click shop name in cart item row
        xpath = Util.GetXpath({"locate":"shop_name"})
        xpath = xpath.replace("replace_shop_name", shop_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click shop name", "result": "1"})

        OK(ret, int(arg['result']), 'CartPageProductSection.ClickShopName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteProduct(arg):
        '''
        DeleteProduct : Delete product
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product delete btn
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath_delete_btn = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_delete_btn, "message":"Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'CartPageProductSection.DeleteProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductModel(arg):
        '''
        SelectProductModel : Select product model
                Input argu :
                    product_name - product name
                    model_name - model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']
        model_name = arg['model_name']

        ##Click product model dropdown
        xpath = Util.GetXpath({"locate":"label_variations"})
        xpath_product_model_dropdown = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_model_dropdown, "message":"Click product model dropdown", "result": "1"})

        ##Click product model name button
        xpath = Util.GetXpath({"locate":"product_model_name_btn"})
        xpath_product_model_name_btn = xpath.replace("model_name_to_be_replace", model_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_model_name_btn, "message":"Click product model name button", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"order_cart_label_confirm"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'CartPageProductSection.SelectProductModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ApplyVoucherCode(arg):
        '''
        ApplyVoucherCode : Input voucher code after clicking more voucher option
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input voucher code
        voucher = GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["code"]
        xpath = Util.GetXpath({"locate":"input_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":voucher, "result": "1"})

        ##Click apply
        xpath = Util.GetXpath({"locate":"apply_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click apply button", "result": "1"})
        time.sleep(5)

        ##Refresh browser
        BaseUILogic.BrowserRefresh({"message":"Refresh page", "result": "1"})

        OK(ret, int(arg['result']), 'CartPageProductSection.ApplyVoucherCode')


class CartButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShopping(arg):
        '''
        ClickGoShopping : Click go shopping btn in cart empty page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go shopping btn
        xpath = Util.GetXpath({"locate":"go_shopping_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click go shopping button", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickGoShopping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOkForProductUpdate(arg):
        '''
        ClickOkForProductUpdate : Click ok btn in product update popup
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn
        xpath = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok btn in product update popup", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickOkForProductUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductVariationDropDown(arg):
        '''
        ClickProductVariationDropDown : Click product variation drop down button
                Input argu :
                    position - product position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        position = arg['position']

        ##Click product variation dropdown btn
        xpath = Util.GetXpath({"locate":"dropdown_btn"})
        xpath = xpath + '[' + position + ']'
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product variation dropdown btn", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickProductVariationDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingFeeDropDown(arg):
        '''
        ClickShippingFeeDropDown : Click shipping fee drop down in cart page
                    Input argu : N/A
                    Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1

        ##click shipping fee drop down
        xpath = Util.GetXpath({"locate":"ship_fee_drop_down"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click shipping fee drop down", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickShippingFeeDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductVariationWindowConfirm(arg):
        '''
        ClickProductVariationWindowConfirm : Click confirm button in product variation window
                Input argu :
                    action - confirm / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']

        ##Click confirm or cancel btn in product variation window
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product variation dropdown btn", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickProductVariationWindowConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupOption(arg):
        '''
        ClickPopupOption : Click buttons in product delete pop up page
                Input argu :
                    action - confirm / cancel / close / yes / ok / back
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']

        ##Click confirm, cancel in product delete popup
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click button in product delete pop up page", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickPopupOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFooterDelete(arg):
        '''
        ClickFooterDelete : Click delete product button on the footer of cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product delete btn
        xpath = Util.GetXpath({"locate":"delete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click footer delete button", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickFooterDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFooterDeletePopupOption(arg):
        '''
        ClickFooterDeletePopupOption : Choose option of footer delete popup
                Input argu :
                    action - cancel / confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Click product delete btn
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose option:" + action, "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickFooterDeletePopupOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete in cart badge
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click delete
        xpath = Util.GetXpath({"locate":"order_cart_drawer_label_delete"})
        xpath_cart_badge_del_btn = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_cart_badge_del_btn, "message":"Click delete in cart badge", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteProduct(arg):
        '''
        ClickDeleteProduct : Click delete product button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product delete btn
        xpath = Util.GetXpath({"locate":"delete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickDeleteProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAll(arg):
        '''
        ClickSelectAll : In cart page click select all
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select all checkbox
        xpath = Util.GetXpath({"locate": "select_all_checkbox"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click select all checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickSelectAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseVoucher(arg):
        '''
        ClickChooseVoucher : In cart page click choose voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose btn
        xpath = Util.GetXpath({"locate": "title_select_voucher"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose voucher button in cart page", "result": "1"})

        ##Due to ticket : SPPT-18597, add sleep time to prevent fail
        ##After ticket fixed will remove this section
        time.sleep(10)

        OK(ret, int(arg['result']), 'CartButton.ClickChooseVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddShopVoucher(arg):
        '''
        ClickAddShopVoucher : Click add shop voucher (non discoverable voucher)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click 'add voucher for shop' button
        xpath = Util.GetXpath({"locate": "add_voucher_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add shop voucher (non discoverable voucher)", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickAddShopVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUseShopeeCoin(arg):
        '''
        ClickUseShopeeCoin : Click shopee coin checkbox to use shopee coin
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click use shopee coin checkbox
        xpath = Util.GetXpath({"locate": "shopee_coin_checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shopee coin checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickUseShopeeCoin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckOut(arg):
        '''
        ClickCheckOut : Goto cart page and check out
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click check out button
        xpath = Util.GetXpath({"locate":"checkout_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click check out btn", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickCheckOut')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductName(arg):
        '''
        ClickProductName : Click product name
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product name
        xpath = Util.GetXpath({"locate":"product_name_label"})
        xpath_product_name_label = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_name_label, "message":"Click product name", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickProductName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionChange(arg):
        '''
        ClickPromotionChange : Click promotion change
                Input argu :
                    prom_type - the transfikey of the promotion in cart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        prom_type = arg['prom_type']

        ##Click promotion change
        xpath = Util.GetXpath({"locate": prom_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion change", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickPromotionChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetSellerVoucher(arg):
        '''
        ClickGetSellerVoucher : Click get seller voucher
                Input argu :
                    shop_name - shop name you want to get it seller voucher
                    button_type - more_voucher / claim
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]
        button_type = arg["button_type"]

        ##Click get seller voucher
        xpath = Util.GetXpath({"locate":button_type})
        xpath_voucher_btn = xpath.replace("shop_name_to_be_replace", shop_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_voucher_btn, "message":"Click get seller voucher", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickGetSellerVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApplySellerVoucher(arg):
        '''
        ClickApplySellerVoucher : Click apply button of seller voucher in shop voucher panel
                Input argu :
                    voucher_title - title text of target voucher you want to apply
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_title = arg["voucher_title"]

        ##Find voucher and click apply button
        xpath = Util.GetXpath({"locate":"apply_button"})
        xpath = xpath.replace("voucher_title_to_be_replace", voucher_title)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Apply target seller voucher", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickApplySellerVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGWPConfirm(arg):
        '''
        ClickGWPConfirm : Click gwp confirm btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click gwp confirm button
        xpath = Util.GetXpath({"locate": "gwp_confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click gwpn confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickGWPConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlatformVoucher(arg):
        '''
        ClickPlatformVoucher : In cart page click platform voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose btn
        xpath = Util.GetXpath({"locate": "title_select_voucher"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose voucher button in cart page", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickPlatformVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFindSimilar(arg):
        '''
        ClickFindSimilar : In cart page click find similar
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click find similar btn
        xpath = Util.GetXpath({"locate": "find_similar_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click find similar button in cart page", "result": "1"})

        OK(ret, int(arg['result']), 'CartButton.ClickFindSimilar')

class CartBanner:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupBannerCloseButton(arg):
        '''
        ClickPopupBannerCloseButton : Click popup banner close button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click popup banner close button
        xpath = Util.GetXpath({"locate":"popup_close_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click popup banner close btn", "result": "1"})

        OK(ret, int(arg['result']), 'CartBanner.ClickPopupBannerCloseButton')

class CartAddOnDealLandingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAddOnDealProduct(arg):
        '''
        SelectAddOnDealProduct : Select add on deal product in landing page
                Input argu :
                    product_name - your add on deal sub product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click popup banner close button
        xpath = Util.GetXpath({"locate": "checkbox"})
        xpath = xpath.replace("target_sub_product", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select sub product:" + product_name, "result": "1"})

        OK(ret, int(arg['result']), 'CartAddOnDealLandingPage.SelectAddOnDealProduct')

class CartGWPLandingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectGiftProduct(arg):
        '''
        SelectGiftProduct : Select gift product in cart gwp landing page
                Input argu :
                    click_choose_button - 1 / 0 (if 1, will click "select gift" button in the landing page)
                    product_name - your gwp gift product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_choose_button = int(arg["click_choose_button"])
        product_name = arg["product_name"]

        ## Click choose gift button in cart gwp landing page
        if click_choose_button:
            xpath = Util.GetXpath({"locate":"gwp_choose_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose gift button", "result": "1"})

        ## Select gwp gift product in gwp popup
        xpath = Util.GetXpath({"locate":"gwp_product"})
        xpath = xpath.replace("target_gift_product", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click checkbox of gwp product:" + product_name, "result": "1"})

        OK(ret, int(arg['result']), 'CartGWPLandingPage.SelectGiftProduct')

class CartComponent:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        xpath = Util.GetXpath({"locate": button_type})

        ##Click on button
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'CartComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
            Input argu :
                checkbox_type - details_page_all_product / details_page_option_product
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        xpath = Util.GetXpath({"locate": checkbox_type})

        ##Click on checkbox
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'CartComponent.ClickOnCheckbox')
