#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminIconManagement.py: The def of this file called by XML mainly.
'''

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminIconManagementComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
            Input argu :
                checkbox_type - type of checkbox which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        ##Click on checkbox
        xpath = Util.GetXpath({"locate": checkbox_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - title_name
                input_content - input content
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementComponent.InputToColumn')


class AdminIconManagementPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadIconImage(arg):
        '''
        UploadIconImage : Upload icon image
            Input argu :
                file_name - file name
                file_type - file type
                upload_position - position of taget upload field
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        upload_position = arg["upload_position"]
        file_type = arg["file_type"]

        ##Upload icon image
        xpath = Util.GetXpath({"locate":"image_upload_path"})
        xpath = xpath.replace('position_replaced', upload_position)
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementPage.UploadIconImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadIconGIF(arg):
        '''
        UploadIconGIF : Upload icon GIF
            Input argu :
                file_name - file name
                upload_position - position of taget upload field
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        upload_position = arg["upload_position"]

        ##Upload icon image
        xpath = Util.GetXpath({"locate":"gif_upload_path"})
        xpath = xpath.replace('position_replaced', upload_position)
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": "gif", "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementPage.UploadIconGIF')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectPage(arg):
        '''
        SelectPage : Select page
            Input argu :
                page_number - page number
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]

        ##Click page number field
        xpath = Util.GetXpath({"locate": "page_number_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number field", "result": "1"})

        ##Select page
        xpath = Util.GetXpath({"locate": page_number})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number", "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementPage.SelectPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteIcon(arg):
        '''
        DeleteIcon : Delete icon
            Input argu :
                icon_name - icon name
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        icon_name = arg["icon_name"]

        ##Delete icon
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace('name_replaced', icon_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementPage.DeleteIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectStartTime(arg):
        '''
        SelectStartTime : Select start time
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Select start date
        xpath = Util.GetXpath({"locate": "start_date_input"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date input", "result": "1"})
        xpath = Util.GetXpath({"locate": "today_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click today button", "result": "1"})

        ##Select start time
        xpath = Util.GetXpath({"locate": "start_time_input"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time input", "result": "1"})
        xpath = Util.GetXpath({"locate": "now_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementPage.SelectStartTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectEndTime(arg):
        '''
        SelectEndTime : Select end time
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Select end date
        xpath = Util.GetXpath({"locate": "end_date_input"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date input", "result": "1"})
        xpath = Util.GetXpath({"locate": "today_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click today button", "result": "1"})

        ##Select end time
        xpath = Util.GetXpath({"locate": "end_time_input"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time input", "result": "1"})
        xpath = Util.GetXpath({"locate": "now_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminIconManagementPage.SelectEndTime')
