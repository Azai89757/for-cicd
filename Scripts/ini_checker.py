import sys
import os
import ConfigParser

rlt = 0
QATest_path = sys.argv[1]
print(QATest_path)
data_folder = os.path.join(QATest_path, 'common')
print(data_folder)

def CheckIniFile(filename):
    '''
    CheckIniFile : Use to check .ini file format
    '''
    conf = ConfigParser.ConfigParser()
    try:
        ##Use ConfigParser to open ini file
        conf.read(filename)
    ## Handle section duplicate
    except ConfigParser.DuplicateSectionError as exceptions:
        print("Section duplicate in this ini file: %s" % (filename))
        return 1

    ## Handle key duplicate
    except ConfigParser.DuplicateOptionError as exceptions:
        print("Key duplicate in this ini file: %s" % (filename))
        return 1

    ## Handle other exception
    except Exception as exceptions:
        print(exceptions)
        return 1
    return 0

for root, dirs, files in os.walk(data_folder):
    ##Search file under data\common
    for file in files:
        print(file)
        if ".ini" in file:
            ini_file_path = os.path.join(root, file)
            if CheckIniFile(ini_file_path) == 1:
                rlt = 1

sys.exit(rlt)
