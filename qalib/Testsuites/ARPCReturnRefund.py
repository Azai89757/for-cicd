import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCReturnRefund Precondition =="
        dumplogger.info("== Setup ARPCReturnRefund Precondition ==")

    def test_ARPCReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCReturnRefund-01.xml")

    def test_ARPCReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCReturnRefund-02.xml")

    def test_ARPCReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCReturnRefund-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCReturnRefund Post condition =="
        dumplogger.info("== Setup ARPCReturnRefund Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
