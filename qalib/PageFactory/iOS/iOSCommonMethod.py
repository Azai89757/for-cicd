﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
iOSCommonMethod.py: The def of this file called by XML mainly.
'''


##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import traceback
import iOSBaseUICore
import iOSBaseUILogic
import DecoratorHelper
import XtFunc
import GlobalAdapter

##Import selenium related module
from selenium.common.exceptions import NoSuchElementException

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def iOSRestoreNormalNetworkSettings(arg):
    '''iOSRestoreNormalNetworkSettings : Restore network settings to normal
            Input argu : N/A
            Return code : 1 - success
                            0 - fail
                            -1 - error
    '''
    ret = 1

    ##Swipe to show control center
    iOSBaseUICore.iOSRelativeSwipe({"locate_x":"345", "locate_y":"25", "movex":"345", "movey":"100", "result": "1"})

    ##Check airplane mode
    locate = Util.GetXpath({"locate":"airplane_mode"})
    if iOSBaseUICore.iOSCheckElementExist({"method":"id", "locate":locate, "passok": "0", "result": "1"}):
        ##Click airplane btn
        iOSCommonButton.ClickSettingsInControlCenter({"settings":"airplane_mode", "result": "1"})
    else:
        dumplogger.info("airplane mode already off!!")

    ##Swipe to show control center
    iOSBaseUICore.iOSRelativeSwipe({"locate_x":"345", "locate_y":"25", "movex":"345", "movey":"100", "result": "1"})

    ##Check wifi mode
    locate = Util.GetXpath({"locate":"wifi_mode"})
    if iOSBaseUICore.iOSCheckElementExist({"method":"id", "locate":locate, "passok": "0", "result": "1"}):
        ##Click wifi btn
        iOSCommonButton.ClickSettingsInControlCenter({"settings":"wifi", "result": "1"})
    else:
        dumplogger.info("wifi already turned on!!")

    OK(ret, int(arg["result"]), 'iOSRestoreNormalNetworkSettings')


class iOSCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToPreviousPage(arg):
        '''ClickBackToPreviousPage : Click back to previous page
                Input argu : type - back icon type
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        type = arg['type']

        locate = Util.GetXpath({"locate": type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSCommonButton.ClickBackToPreviousPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackButtonByCoordinate(arg):
        ''' ClickBackButtonByCoordinate : Click Back(<-) button on action bar by coordinate
                Input argu : x_cor - x coordinate of back btn
                             y_cor - y coordinate of back btn
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        x_cor = arg['x_cor']
        y_cor = arg['y_cor']

        ##Click close button
        iOSBaseUICore.iOSClickCoordinates({"x_cor":x_cor, "y_cor":y_cor, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSCommonButton.ClickBackButtonByCoordinate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSettingsInControlCenter(arg):
        '''ClickSettingsInControlCenter : Click settings btn in control center
                Input argu : settings - settings to click
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        settings = arg['settings']

        ##Swipe to show control center
        iOSBaseUICore.iOSRelativeSwipe({"locate_x":"345", "locate_y":"25", "movex":"345", "movey":"100", "result": "1"})

        ##Click wifi btn
        time.sleep(5)
        locate = Util.GetXpath({"locate":settings + "_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click wifi btn", "result": "1"})

        time.sleep(5)
        ##Click somewhere else to dismiss control center
        iOSBaseUICore.iOSClickCoordinates({"x_cor":"190", "y_cor":"25", "result": "1"})

        OK(ret, int(arg["result"]), 'iOSCommonButton.ClickSettingsInControlCenter')


class iOSSettingsApp:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSettings(arg):
        '''ClickSettings: Click settings in settings app
                Input argu : type - settings type
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Check navigation bar previous btn exist
        xpath = Util.GetXpath({"locate":"previous_btn"})
        for index in range(0, 5):
            if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click back", "result": "1"})
                time.sleep(1)
            else:
                dumplogger.info("No back arrow to click")
                break

        ##Click settings type
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click {0}".format(type), "result": "1"})

        OK(ret, int(arg['result']), 'iOSSettingsApp.ClickSettings')


class iOSWifiSettingsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchWifiToggle(arg):
        '''SwitchWifiToggle : Switch wifi toggle
                Input argu : status - 1(on) / 0(off)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        status = arg['status']

        time.sleep(5)
        ##Get wifi switch value
        locate = Util.GetXpath({"locate":"wifi_status"})
        iOSBaseUICore.iOSGetElements({"method":"xpath", "locate":locate, "result": "1"})
        iOSBaseUICore.iOSGetAttributes({"mode":"single", "attrtype":"get_attribute", "attribute":"value", "result": "1"})

        ##If wifi value is not same as value you want to set, click wifi toggle to turn on/off
        if status != GlobalAdapter.CommonVar._PageAttributes_:
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Switch wifi toggle", "result": "1"})
        else:
            dumplogger.error("Wifi current status is %s, and is same status you want to set" % (GlobalAdapter.CommonVar._PageAttributes_))
            ret = 0

        OK(ret, int(arg['result']), 'iOSWifiSettingsPage.SwitchWifiToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickWifiHost(arg):
        '''ClickWifiHost : Click wifi network host
                Input argu : host_name - host name of wifi
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        host_name = arg['host_name']

        ##Click host name
        locate = Util.GetXpath({"locate":"host_name"})
        locate = locate.replace("wifi_host_name", host_name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click host name -> " + host_name, "result": "1"})

        OK(ret, int(arg['result']), 'iOSWifiSettingsPage.ClickWifiHost')


class iOSSafariSettings:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSafariSettings(arg):
        '''ClickSafariSettings : Click safari settings
                Input argu : type - settings type
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click settings type in safari
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click {0}".format(type), "result": "1"})

        ##Click popup clear cache
        xpath = Util.GetXpath({"locate":"clear_cache_popup"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click {0}".format(type), "result": "1"})

        OK(ret, int(arg['result']), 'iOSSafariSettings.ClickSafariSettings')
