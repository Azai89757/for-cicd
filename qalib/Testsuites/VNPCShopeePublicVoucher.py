import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCShopeePublicVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopeePublicVoucher Precondition =="
        dumplogger.info("== Setup VNPCShopeePublicVoucher Precondition ==")

    def test_VNPCShopeePublicVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-01.xml")

    def test_VNPCShopeePublicVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-02.xml")

    def test_VNPCShopeePublicVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-03.xml")

    def test_VNPCShopeePublicVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-04.xml")

    def test_VNPCShopeePublicVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-05.xml")

    def test_VNPCShopeePublicVoucher06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-06.xml")

    def test_VNPCShopeePublicVoucher07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-07.xml")

    def test_VNPCShopeePublicVoucher08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-08.xml")

    def test_VNPCShopeePublicVoucher09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-09.xml")

    def test_VNPCShopeePublicVoucher10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-10.xml")

    def test_VNPCShopeePublicVoucher11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-11.xml")

    def test_VNPCShopeePublicVoucher12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-12.xml")

    def test_VNPCShopeePublicVoucher13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopeePublicVoucher-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopeePublicVoucher Post condition =="
        dumplogger.info("== Setup VNPCShopeePublicVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
