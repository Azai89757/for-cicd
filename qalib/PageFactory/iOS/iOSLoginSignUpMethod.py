﻿#!/usr/bin/env python
#-*- coding: utf-8 -*-
'''
 iOSLoginSignupMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import traceback
import XtFunc
import DecoratorHelper
import iOSBaseUICore
import iOSBaseUILogic
import iOSMePageMethod
import iOSCommonMethod
import GlobalAdapter

##Import selenium related module
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def iOSShopeeLogin(arg):
    ''' iOSShopeeLogin : Use for shopee app log in
                    Input argu :
                        account - user account
                        password - user password
                        logintype - account
                        facebook_notinstalled_nologin
                    Return code : 1 - success
                                0 - fail
                                -1 - error
    '''

    account = arg["account"]
    password = arg["password"]
    logintype = arg["logintype"]
    is_login = 1
    ret = 1

    time.sleep(10)

    ##Click Me tab btn
    iOSMePageMethod.iOSHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    ##Move to top
    iOSBaseUILogic.iOSRelativeMove({"direction":"up", "result": "1"})

    ##Try to locate login icon first, if login icon exists, that means it's not logged-in
    locate = Util.GetXpath({"locate":"login_button_mepage"})
    is_login_icon_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"})

    if is_login_icon_exist:
        dumplogger.info("Don't have account logged in, just login directly.")
    else:
        dumplogger.info("Have account logged in, compare if is correct account.")

        ##Get account name
        locate = Util.GetXpath({"locate":"account_display_in_me_page"})
        locate_replace = locate.replace('account_display_name', account)
        is_current_account_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate_replace, "passok": "0", "result": "1"})

        ##Compare if shop_name is same as account, if not, perform logout first.
        if is_current_account_exist:
            dumplogger.info("This account: %s has logged in, not need to logout and re-login again", account)
            iOSMePageMethod.iOSHomePage.HomePageNavigation({"type":"home", "result": "1"})
            is_login = 0

        else:
            dumplogger.info("Prepare to logout!")
            iOSShopeeLogout({"result": "1"})
            time.sleep(5)
            ##Temp comment to prevent crash
            ##Check if Shopee Program is fully loaded by checking element
            #locate = Util.GetXpath({"locate":"home_page_category"})
            #iOSBaseUILogic.iOSMoveAndCheckElement({"locate":locate, "direction":"down", "method":"id", "isclick":"0", "result": "1"})
            ##Click Me tab btn
            iOSMePageMethod.iOSHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    if is_login:
        ##Click login btn
        iOSLoginSignupButton.ClickLogin({"type":"login_button", "result": "1"})

        if logintype == 'account':
            time.sleep(5)
            dumplogger.info("Enter iOSShopeeLogin via account or email")

            ##Enter account and pwd
            locate = Util.GetXpath({"locate":"account_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":account, "result": "1"})
            locate = Util.GetXpath({"locate":"password_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":password, "result": "1"})
            ##Click login button
            locate = Util.GetXpath({"locate":"login_button_login_page"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"click login button", "result": "1"})

            iOSLoginPage.InputVcode({"vcode":"123456", "result": "1"})

        elif logintype == 'facebook_notinstalled_nologin':

            dumplogger.info("Enter iOSShopeeLogin via facebook_notinstalled_nologin")

            ##Click FB button
            time.sleep(5)
            iOSLoginSignupButton.ClickLogin({"type":"facebook_login_button", "result": "1"})

            locate = Util.GetXpath({"locate":"proceed_button"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click proceed", "result": "1"})

            time.sleep(10)
            iOSBaseUILogic.iOSHandleAlert({"type":"accept", "result": "1"})

            ##If first time using fb login, will need to input username and password
            locate = Util.GetXpath({"locate":"fb_login_input_column"})
            is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":locate, "passok": "0", "result": "1"})
            if is_exist:
                ##Type FB account and password
                iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":account, "result": "1"})
                locate = Util.GetXpath({"locate":"fb_password_input_column"})
                iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":password, "result": "1"})

                ##Click login button
                locate = Util.GetXpath({"locate":"fb_login_icon"})
                iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"click FB login button", "result": "1"})

            ##Click continue button
            locate = Util.GetXpath({"locate":"fb_login_continue_button"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"click FB continue button", "result": "1"})

        elif logintype == "google":
            dumplogger.info("Enter iOSShopeeLogin via google login")

            ##Click Google button
            time.sleep(5)
            iOSLoginSignupButton.ClickLogin({"type":"google_login_button", "result": "1"})

            locate = Util.GetXpath({"locate":"proceed_button"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click proceed", "result": "1"})

            iOSBaseUILogic.iOSHandleAlert({"type":"accept", "result": "1"})

            ##If have "Login with other account" btn, click it to login
            try:
                locate = Util.GetXpath({"locate":"google_login_with_other_account_button"})
                iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"click Google continue button", "result": "1"})
            except:
                dumplogger.info("First time login with Google account")

            ##Fill email or phone number
            locate = Util.GetXpath({"locate":"google_email_input_column"})
            iOSBaseUICore.iOSInput({"method":"id", "locate":locate, "string":account, "result": "1"})

            ##Click continue button
            locate = Util.GetXpath({"locate":"google_continue_button"})
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"click Google continue button", "result": "1"})

            ##Inupt password
            locate = Util.GetXpath({"locate":"google_password_input_column"})
            iOSBaseUICore.iOSInput({"method":"id", "locate":locate, "string":password, "result": "1"})

            ##Click continue button
            locate = Util.GetXpath({"locate":"google_continue_button"})
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"click Google continue button", "result": "1"})

        elif logintype == "sms":

            dumplogger.info("Enter iOSShopeeLogin via sms")

            ##Click sms login text
            iOSLoginSignupButton.ClickLogin({"type":"sms_login_button", "result": "1"})

            ##Input phone number
            iOSLoginPage.InputPhoneNumber({"phone":account, "result": "1"})

            ##Click next button
            iOSLoginSignupButton.ClickNext({"page_type":"sms_login_page", "result": "1"})

            ##Input captcha
            iOSLoginPage.InputCaptcha({"captcha":"123456", "result": "1"})

            ##Click confirm button
            iOSLoginSignupButton.ClickConfirm({"page_type": "captcha_page", "result": "1"})

            time.sleep(5)

            ##Input vcode
            iOSLoginPage.InputVcode({"vcode":"123456", "result": "1"})

        else:
            dumplogger.info("Enter iOSShopeeLogin but method not defined yet")

    OK(ret, int(arg['result']), 'iOSShopeeLogin')

@DecoratorHelper.FuncRecorder
def iOSShopeeLogout(arg):
    ''' iOSShopeeLogout : Use for shopee app log out
                    Input argu :
                        N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
    '''
    ret = 1

    ###Click Me tab btn
    iOSMePageMethod.iOSHomePage.HomePageNavigation({"type":"metab", "result": "1"})

    ##Swipe to the top to be able to click setting btn
    iOSBaseUILogic.iOSRelativeMove({"direction":"up", "times":"2", "result": "1"})

    ##Check if current status is logged-in, if so, perform log out to prevent error
    locate = Util.GetXpath({"locate":"check_login_status"})

    if iOSBaseUICore.iOSCheckElementExist({"method":"id", "locate":locate, "message":"Check if seller tag exists", "passok": "0", "result": "1"}):
        dumplogger.info("The app has been logged in, proceed Shopee logout.")

        iOSBaseUILogic.iOSRelativeMove({"direction":"down", "times":"3", "result": "1"})

        ##Click my account settings cell
        xpath = Util.GetXpath({"locate":"my_account_cell"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"click my account cell", "result": "1"})

        ##Scroll account settings to logout btn
        iOSBaseUILogic.iOSRelativeMove({"direction":"down", "times":"3", "result": "1"})

        ##Click log-out btn
        xpath = Util.GetXpath({"locate":"logout_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"click logout btn", "result": "1"})

        time.sleep(3)
        ##Click confirm btn on popup
        xpath = Util.GetXpath({"locate":"confirm_logout_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"click confirm btn", "result": "1"})

        ##To prevent doing things to quick so thay logout not actually logout
        time.sleep(10)

        iOSMePageMethod.iOSHomePage.HomePageNavigation({"type":"metab", "result": "1"})

        iOSBaseUICore.iOSSwitchBranch({"branch":"master-automation"})

    else:

        ##Has been logout status, back to home page
        dumplogger.info("No account logged-in, go to Main page.")
        iOSMePageMethod.iOSHomePage.HomePageNavigation({"type":"home","result": "1"})

    OK(ret, int(arg['result']), 'iOSShopeeLogout')


class iOSLoginSignupButton:
    ''' iOSLoginSignupButton : All of Page could inherit the Button class to handle event'''

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrivacyPolicy(arg):
        '''ClickPrivacyPolicy : Click privacy policy
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click Privacy policy
        locate = Util.GetXpath({"locate":"privacy_policy_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click privacy policy", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickPrivacyPolicy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTermsOfService(arg):
        '''ClickTermsOfService : Click terms of service
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click Privacy policy
        locate = Util.GetXpath({"locate":"terms_of_service_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click privacy policy", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickTermsOfService')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackIcon(arg):
        '''ClickBackIcon : Click back icon to perform back to previous page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back arrow coordinate
        iOSBaseUICore.iOSClickCoordinates({"x_cor":"12", "y_cor":"60", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickBackIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchLoginSignup(arg):
        ''' SwitchLoginSignup : To switch login/signup in metab
                Input argu :
                    switch_type : login_tab / signup_tab
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        switch_type = arg["switch_type"]

        ##Switch login signup tab
        xpath = Util.GetXpath({"locate":switch_type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Switch login/signup tab", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.SwitchLoginSignup -> ' + switch_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickForgetPassword(arg):
        ''' ClickForgetPassword : Click Forget Password button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click forget password button
        xpath = Util.GetXpath({"locate":"forget_password"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click forget password", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickForgetPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        ''' ClickNext : Click next button in any page
                Input argu :
                            page_type : next button in which page
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click SMS login text
        xpath = Util.GetXpath({"locate":page_type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"click next button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickNext')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogin(arg):
        ''' ClickLogin : Click Login button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click login button
        locate = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click Login button", "result": "1"})

        ##handle 3rd party security popup
        if type == "facebook_login_button":
            iOSBaseUILogic.iOSHandleAlert({"type":"accept", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSMSLogin(arg):
        ''' ClickSMSLogin : Click SMS Login button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click to use SMS login
        locate = Util.GetXpath({"locate":"sms_login_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click FB Login button", "result": "1"})

        ##If permission window popup, handle it
        locate = Util.GetXpath({"locate":"permission_continue"})
        is_popup_exist = iOSBaseUICore.iOSCheckElementExist({"method":"id", "locate":locate, "passok": "0", "result": "1"})
        if is_popup_exist:
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickSMSLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFBLogin(arg):
        ''' ClickFBLogin : Click FB Login button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click to use fb login
        locate = Util.GetXpath({"locate":"fb_login_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click FB Login button", "result": "1"})

        ##If permission window popup, handle it
        locate = Util.GetXpath({"locate":"permission_continue"})
        is_popup_exist = iOSBaseUICore.iOSCheckElementExist({"method":"id", "locate":locate, "passok": "0", "result": "1"})
        if is_popup_exist:
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickFBLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFBLoginCancel(arg):
        ''' ClickFBLoginCancel : Click cancel button in FB login page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click button
        locate = Util.GetXpath({"locate":"cancel_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickFBLoginCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoogleLogin(arg):
        ''' ClickGoogleLogin : Click Google Login button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click to use Google login
        locate = Util.GetXpath({"locate":"google_login_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click Google Login button", "result": "1"})

        ##If permission window popup, handle it
        locate = Util.GetXpath({"locate":"permission_continue"})
        is_popup_exist = iOSBaseUICore.iOSCheckElementExist({"method":"id", "locate":locate, "passok": "0", "result": "1"})
        if is_popup_exist:
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickGoogleLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoogleLoginCancel(arg):
        ''' ClickGoogleLoginCancel : Click cancel button in Google login page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click button
        locate = Util.GetXpath({"locate":"cancel_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickGoogleLoginCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFBLoginLater(arg):
        ''' ClickFBLoginLater : Click later button in FB login page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click button
        locate = Util.GetXpath({"locate":"fb_login_later_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickFBLoginLater')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSignup(arg):
        ''' ClickSignup : Click signup button
                Input argu : type - apple / email / fb / google / signup
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click button
        locate = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click Signup button", "result": "1"})

        ##handle 3rd party security popup
        if type == "facebook_signup_button":
            iOSBaseUILogic.iOSHandleAlert({"type":"accept", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickSignup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEmailSignup(arg):
        ''' ClickEmailSignup : Click Email signup button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click button
        locate = Util.GetXpath({"locate":"email_signup_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click Email Signup button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickEmailSignup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' ClickConfirm : Click Confirm button
                Input argu :
                    page_type : confirm button in which page
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click confirm button
        xpath = Util.GetXpath({"locate":page_type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click Confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefreshCaptureText(arg):
        ''' ClickRefreshCaptureText : Click refresh captcha text in catpcha page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click refresh captcha text in catpcha page
        xpath = Util.GetXpath({"locate":"refresh_captcha"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"click refresh captcha text", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickRefreshCaptureText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel button in catpcha page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click cancel button in catpcha page
        xpath = Util.GetXpath({"locate":"cancel_button"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupCancel(arg):
        ''' ClickPopupCancel : Click cancel button at popup page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click login button
        locate = Util.GetXpath({"locate":"cancel_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickPopupCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupConfirm(arg):
        ''' ClickPopupConfirm : Click confitm button at popup page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click login button
        locate = Util.GetXpath({"locate":"ok_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click confitm button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickPopupConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinue(arg):
        ''' ClickContinue : Click Login button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click button
        locate = Util.GetXpath({"locate":"continue_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click continue button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickContinue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVerification(arg):
        ''' ClickVerification : Click Verification button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click button
        locate = Util.GetXpath({"locate":"verification_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click Confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickVerification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHelp(arg):
        ''' ClickHelp : Click help button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click= help button
        locate = Util.GetXpath({"locate":"help_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click need help btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickHelp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewPasswordIcon(arg):
        '''ClickViewPasswordIcon : Click view password icon
                Input argu :
                    type - open / close
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click eye button
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"id", "locate":xpath, "message":"Click eye %s btn" % (type), "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickViewPasswordIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRegister(arg):
        '''ClickRegister : Click register btn
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click register btn
        xpath = Util.GetXpath({"locate":"register_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click register btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginSignupButton.ClickRegister')


class iOSLoginPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhoneNumber(arg):
        '''InputPhoneNumber : Input phone number in sms login page
                Input argu :
                            phone - phone number of account attached
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        phone = arg["phone"]

        ##Input phone number in sms login page
        xpath = Util.GetXpath({"locate":"sms_phone_number_column"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.InputPhoneNumber -> ' + phone)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCaptcha(arg):
        ''' InputCaptcha : Input captcha in captcha verification page
                Input argu :
                            captcha - input captcha for verification
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        captcha = arg["captcha"]

        ##Input captcha in captcha verification page
        xpath = Util.GetXpath({"locate":"sms_captcha_column"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":captcha, "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.InputCaptcha -> ' + captcha)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAccount(arg):
        ''' InputAccount : Input account in login page
                Input argu :
                            account - shopee account and it should be username or email or phone number
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        account = arg["account"]

        ##Input account in login page
        xpath = Util.GetXpath({"locate":"input_account"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.InputAccount -> ' + account)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        ''' InputPassword : Input password in login page
                Input argu :
                            password - shopee account password
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        password = arg["password"]

        ##Input password in login page
        xpath = Util.GetXpath({"locate":"input_password"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.InputPassword -> ' + password)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AccountLoginFET(arg):
        ''' AccountLoginFET : FET test scenario of "Account" login method
                    Input argu :
                        account - user account
                        password - password
                        scenario - to print/log what scenario is running
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''

        account = arg["account"]
        password = arg["password"]
        scenario = arg["scenario"]
        ret = 1

        ##Try to clean the residue account from last time
        locate = Util.GetXpath({"locate":"account_input_column"})
        try:
            iOSBaseUICore._iOSDriver_.implicitly_wait(5)
            iOSBaseUICore._iOSDriver_.find_element_by_xpath(locate).clear()
        except:
            dumplogger.info("There is no reidual account to clean, proceed...")

        if "Scenario" in scenario:

            ##Enter account and pwd
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":account, "result": "1"})
            locate = Util.GetXpath({"locate":"password_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":password, "result": "1"})

            if "back key" in scenario:
                ##Click back key
                iOSCommonMethod.ClickBackIcon({"result": "1"})

            else:
                ##Click login button
                locate = Util.GetXpath({"locate":"login_button"})
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"click login button", "result": "1"})

                ##Input vcode
                iOSLoginPage.InputVcode({"vcode":"123456", "result": "1"})

        elif "Vcode" in scenario:
            ##Enter account and pwd
            locate = Util.GetXpath({"locate":"account_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":account, "result": "1"})
            locate = Util.GetXpath({"locate":"password_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":password, "result": "1"})

            ##Click login button
            locate = Util.GetXpath({"locate":"login_button"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"click login button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.AccountLoginFET -> ' + scenario)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShopeeLoginFET(arg):
        ''' ShopeeLoginFET : Login with random username
                Input argu :
                    password : password
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        password = arg["password"]
        ret = 1

        ##Use previous random username to login shopee
        iOSLoginPage.AccountLoginFET({"account":GlobalAdapter.GeneralE2EVar._RandomString_, "password":password, "scenario":"Scenario: Input previous random username", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.ShopeeLoginFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVcode(arg):
        ''' InputVcode : Input Vcode for login/signup
                    Input argu :
                        vcode - Vcode
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        vcode = arg["vcode"]
        ret = 1

        xpath = Util.GetXpath({"locate":"vcode_title"})
        is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"})
        dumplogger.info("Vcode exist: %d" % (is_exist))

        if is_exist:
            ##Enter vcode
            for index in range(1, 7):
                xpath = Util.GetXpath({"locate":"password_field"})
                xpath = xpath.replace("index", str(index))
                iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string": list(vcode)[index - 1], "result": "1"})

            time.sleep(5)

            ##Click next button
            iOSLoginSignupButton.ClickNext({"page_type":"sms_login_page", "result": "1"})

        else:
            dumplogger.info("Don't need to vcode verify")

        OK(ret, int(arg['result']), 'iOSLoginPage.InputVcode -> ' + vcode)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetPassword(arg):
        ''' ResetPassword : Fill phone number to reset password
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        phone = arg["phone"]
        new_password = arg["new_password"]

        ##Input phone number
        locate = Util.GetXpath({"locate":"phone_input"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":phone, "result": "1"})

        ##Click continue button
        locate = Util.GetXpath({"locate":"continue"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click continue button", "result": "1"})

        ##Input vcode
        Login.InputVcode({"vcode":"123456","result": "1"})
        iOSLoginSignupButton.ClickVerification({"result": "1"})

        ##Input new password
        locate = Util.GetXpath({"locate":"password_input"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":new_password, "result": "1"})

        ##Input password again
        locate = Util.GetXpath({"locate":"password_confirm_input"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":new_password, "result": "1"})

        ##Click reset
        locate = Util.GetXpath({"locate":"reset_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSLoginPage.ResetPassword')


class iOSSignUpPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserInfo(arg):
        ''' InputUserInfo : Input data for sign up detail (Set email and account name)
                Input argu :
                    account - account
                    username - username
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        account = arg["account"]
        username = arg["username"]

        ret = 1

        ##Rename the user name to avoid user name error
        xpath = Util.GetXpath({"locate":"username"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":username, "result": "1"})

        ##Remove email addresss to avoid the duplicate
        xpath = Util.GetXpath({"locate":"email"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        OK(ret, int(arg['result']), 'iOSSignUpPage.InputUserInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhone(arg):
        ''' InputPhone : Input phone number to signup
                    Input argu :
                        phone - phone number / if u want auto genrate, input "auto_generate" / null string
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        phone = arg["phone"]
        ret = 1

        ##Auto generate set to 1 and did not enter phone number
        if phone == "auto_generate":
            ##generate phone number with 0082 + random phone number
            phone = "0082" + XtFunc.GenerateRandomString(6, "numbers")

        ##Input phone number
        xpath = Util.GetXpath({"locate":"phone_number"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        OK(ret, int(arg['result']), 'iOSSignUpPage.InputPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEmail(arg):
        ''' InputEmail : Input email number to signup
                    Input argu :
                        email - email
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        email = arg["email"]
        ret = 1

        ##Input email
        xpath = Util.GetXpath({"locate":"email"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":email, "result": "1"})

        ##Input captcha
        xpath = Util.GetXpath({"locate":"captcha"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":"gdsg123", "result": "1"})

        OK(ret, int(arg['result']), 'iOSSignUpPage.InputEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCaptcha(arg):
        ''' InputCaptcha : Input captcha in captcha verification page
                    Input argu :
                        captcha - captcha number
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        captcha = arg["captcha"]
        ret = 1

        ##Input captcha
        xpath = Util.GetXpath({"locate":"sms_captcha_column"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":captcha, "result": "1"})

        OK(ret, int(arg['result']), 'iOSSignUpPage.InputCaptcha')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVcode(arg):
        ''' InputVcode : Input vcode after phone number to signup
                    Input argu :
                        vcode - vcode number
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        vcode = arg["vcode"]
        ret = 1

        ##Enter vcode
        if vcode:
            for index in range(1, 7):
                xpath = Util.GetXpath({"locate":"password_field"})
                xpath = xpath.replace("index", str(index))
                iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string": vcode[index - 1], "result": "1"})

        time.sleep(5)

        ##Click next button
        iOSLoginSignupButton.ClickNext({"page_type":"sms_login_page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSSignUpPage.InputVcode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        ''' InputPassword : Input password to signup
                    Input argu :
                        password - password
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        password = arg["password"]
        ret = 1

        ##Input password
        xpath = Util.GetXpath({"locate":"password"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        OK(ret, int(arg['result']), 'iOSSignUpPage.InputPassword')


class Facebook:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def iOSFacebookSignup(arg):
        ''' iOSFacebookSignup : Sign up by facebook
                Input argu :
                    account - facebook account
                    password - facebook password
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''

        account = arg["account"]
        password = arg["password"]
        ret = 1

        try:
            locate = Util.GetXpath({"locate":"fb_account_input_column"})
            iOSBaseUICore._iOSDriver_.implicitly_wait(5)
            iOSBaseUICore._iOSDriver_.find_element_by_xpath(locate)

            ##Input account
            locate = Util.GetXpath({"locate":"fb_account_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":account, "result": "1"})

            ##Input account
            locate = Util.GetXpath({"locate":"fb_password_input_column"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":password, "result": "1"})

            ##Click login
            locate = Util.GetXpath({"locate":"fb_login_button"})
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click login btn", "result": "1"})

            ##Click continue
            time.sleep(3)
            locate = Util.GetXpath({"locate":"fb_continue_button"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click login btn", "result": "1"})

        except:
            dumplogger.info("Maybe facebook account has cookie in shopee... proceed.")
            xpath = Util.GetXpath({"locate":"fb_continue_button"})
            is_exist = iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"})
            if is_exist:
                iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click continue in facebook", "result": "1"})

        OK(ret, int(arg['result']), 'iOSFacebookSignup->' + 'Success')
