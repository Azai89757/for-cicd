import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCProductCard Precondition =="
        dumplogger.info("== Setup FRPCProductCard Precondition ==")

    def test_FRPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCard-01.xml")

    def test_FRPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCard-02.xml")

    def test_FRPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCard-03.xml")

    def test_FRPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCard-04.xml")

    def test_FRPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCProductCard-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCProductCard Post condition =="
        dumplogger.info("== Setup FRPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
