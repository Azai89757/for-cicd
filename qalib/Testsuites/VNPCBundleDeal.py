import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNPCBundleDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCBundleDeal Precondition =="
        dumplogger.info("== Setup VNPCBundleDeal Precondition ==")

    def test_VNPCBundleDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-01.xml")

    def test_VNPCBundleDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-02.xml")

    def test_VNPCBundleDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-03.xml")

    def test_VNPCBundleDeal04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-04.xml")

    def test_VNPCBundleDeal05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-05.xml")

    def test_VNPCBundleDeal06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-06.xml")

    def test_VNPCBundleDeal07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-07.xml")

    def test_VNPCBundleDeal08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-08.xml")

    def test_VNPCBundleDeal09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCBundleDeal-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCBundleDeal Post condition =="
        dumplogger.info("== Setup VNPCBundleDeal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
