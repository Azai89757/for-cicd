import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpFlashSale Precondition =="
        dumplogger.info("== Setup TWHttpFlashSale Precondition ==")

    def test_TWHttpFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-01.xml")

    def test_TWHttpFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-02.xml")

    def test_TWHttpFlashSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-03.xml")

    def test_TWHttpFlashSale04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-04.xml")

    def test_TWHttpFlashSale05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-05.xml")

    def test_TWHttpFlashSale06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-06.xml")

    def test_TWHttpFlashSale07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-07.xml")

    def test_TWHttpFlashSale08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpFlashSale-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpFlashSale Post condition =="
        dumplogger.info("== Setup TWHttpFlashSale Post condition ==")
