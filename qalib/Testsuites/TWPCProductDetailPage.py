import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCProductDetailPage Precondition =="
        dumplogger.info("== Setup TWPCProductDetailPage Precondition ==")

    def test_TWPCProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-01.xml")

    def test_TWPCProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-02.xml")

    def test_TWPCProductDetailPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-03.xml")

    def test_TWPCProductDetailPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-04.xml")

    def test_TWPCProductDetailPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-05.xml")

    def test_TWPCProductDetailPage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-06.xml")

    def test_TWPCProductDetailPage07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCProductDetailPage-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCProductDetailPage Post condition =="
        dumplogger.info("== Setup TWPCProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
