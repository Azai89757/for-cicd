import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpShopeeVoucher Precondition =="
        dumplogger.info("== Setup IDHttpShopeeVoucher Precondition ==")

    def test_IDHttpShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpShopeeVoucher-01.xml")

    def test_IDHttpShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpShopeeVoucher-02.xml")

    def test_IDHttpShopeeVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpShopeeVoucher-03.xml")

    def test_IDHttpShopeeVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpShopeeVoucher-04.xml")

    def test_IDHttpShopeeVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpShopeeVoucher-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpShopeeVoucher Post condition =="
        dumplogger.info("== Setup IDHttpShopeeVoucher Post condition ==")
