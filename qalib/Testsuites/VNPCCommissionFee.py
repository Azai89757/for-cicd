import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCCommissionFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCommissionFee Precondition =="
        dumplogger.info("== Setup VNPCCommissionFee Precondition ==")

    def test_VNPCCommissionFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-01.xml")

    def test_VNPCCommissionFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-02.xml")

    def test_VNPCCommissionFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-03.xml")

    def test_VNPCCommissionFee04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-04.xml")

    def test_VNPCCommissionFee05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-05.xml")

    def test_VNPCCommissionFee06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-06.xml")

    def test_VNPCCommissionFee07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-07.xml")

    def test_VNPCCommissionFee08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-08.xml")

    def test_VNPCCommissionFee09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCommissionFee-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCommissionFee Post condition =="
        dumplogger.info("== Setup VNPCCommissionFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
