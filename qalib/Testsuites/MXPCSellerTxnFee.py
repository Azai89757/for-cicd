import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCSellerTxnFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCSellerTxnFee Precondition =="
        dumplogger.info("== Setup MXPCSellerTxnFee Precondition ==")

    def test_MXPCSellerTxnFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCSellerTxnFee-01.xml")

    def test_MXPCSellerTxnFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCSellerTxnFee-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCSellerTxnFee Post condition =="
        dumplogger.info("== Setup MXPCSellerTxnFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
