﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 IntermediatePageMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
import Util
import BaseUICore
from Config import dumplogger
import DecoratorHelper


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class IntermediatePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        ''' ClickBuyNow : Click buy now button
                    Input argu : N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click buy now button
        xpath = Util.GetXpath({"locate":"buy_now"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click but now button", "result": "1"})

        OK(ret, int(arg['result']), 'IntermediatePageButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClaimVoucher(arg):
        ''' ClickProductName : Click claim voucher
                    Input argu : N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click claim voucher
        xpath = Util.GetXpath({"locate":"claim_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click claim voucher", "result": "1"})

        OK(ret, int(arg['result']), 'IntermediatePageButton.ClickClaimVoucher')
