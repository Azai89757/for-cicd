﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 ProductCardMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def MoveToProductCardByName(arg):
    '''
    MoveToProductCardByName : Move web scope to specific product card
            Input argu :
                name - Name of product card
                card_type - product card type
                distance - distance with top page
                is_click - click product card name or not (1: click ; 0: no click)
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    name = arg["name"]
    card_type = arg["card_type"]
    distance = arg["distance"]
    is_click = arg["is_click"]
    ret = 1

    ##Locate product card name and move to the location
    locate = Util.GetXpath({"locate":card_type})
    locate = locate.replace('string_to_be_replaced', name)

    ##If xpath exists
    if BaseUICore.CheckElementExist({"method": "xpath", "locate": locate, "passok": "0", "result": "1"}):
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": locate, "reservetype": "product_card", "result": "1"})
        BaseUILogic.MouseScrollEvent({"x":str(GlobalAdapter.GeneralE2EVar._LocationX_), "y":str(GlobalAdapter.GeneralE2EVar._LocationY_-int(distance)), "result": "1"})

        ##Click product card if needed
        if int(is_click):
            BaseUICore.Click({"method":"xpath", "locate":locate, "message":"Click product card", "result": "1"})
    else:
        dumplogger.info("Unable to get product name xpath.")
        ret = 0

    OK(ret, int(arg['result']), 'MoveToProductCardByName')


class SellerStorePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProduct(arg):
        '''
        ClickProduct : Click product
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product
        xpath = Util.GetXpath({"locate":"product_name"})
        xpath = xpath.replace("replace_product_name", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + product_name, "result": "1"})

        OK(ret, int(arg['result']), 'SellerStorePage.ClickProduct: ' + product_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClaimVoucher(arg):
        '''
        ClickClaimVoucher : Click claim voucher
                Input argu :
                    voucher_message - message of the voucher to be claimed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_message = arg['voucher_message']

        ##Click voucher claim button
        xpath = Util.GetXpath({"locate":"claim_btn"})
        xpath = xpath.replace('replaced_voucher_message', voucher_message)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to claim voucher", "result": "1"})

        OK(ret, int(arg['result']), "SellerStorePage.ClickClaimVoucher")


class ProductCardButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCardLikeIcon(arg):
        '''
        ClickProductCardLikeIcon : Click product card like icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product card like btn
        xpath = Util.GetXpath({"locate": "like_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product card like btn", "result": "1"})

        OK(ret, int(arg['result']), 'ProductCardButton.ClickProductCardLikeIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCartIcon(arg):
        '''
        ClickProductCartIcon : Click product cart icon
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product cart icon
        xpath = Util.GetXpath({"locate":"cart_icon"})
        xpath = xpath.replace('replaced_product_name', product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Add " + product_name + " to cart", "result": "1"})

        OK(ret, int(arg['result']), 'ProductCardButton.ClickProductCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFindSimiliarIcon(arg):
        '''
        ClickFindSimiliarIcon : Click find similiar icon
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click find similiar icon
        xpath = Util.GetXpath({"locate":"similiar_icon"})
        xpath = xpath.replace('name_to_be_replaced', product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + product_name + " similiar icon", "result": "1"})

        OK(ret, int(arg['result']), 'ProductCardButton.ClickFindSimiliarIcon')
