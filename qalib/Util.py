'''
 Util.py: The def in this file is common utility of FrameWorkBase
          Most of them are used internally.
          If your def is used by multiple other def, please place it here.
          If your def is used by XML, place place to XtFunc.py.
'''

##import system library
import sys
import os
import time
import ctypes
import re
from xml.dom.minidom import *
import json
import configparser
import requests
import inspect
import threading

##Import common library
import Config
from Config import dumplogger, android_performance_logger
import Parser
import GlobalAdapter
import DecoratorHelper

##Import Android library
from PageFactory.Android import AndroidBaseUICore

##Import api related module
from api.http import i18NJsonAPIMethod


def FileName(obj_file):

    dumplogger.info(obj_file)
    file_name = ""
    temp = os.path.basename(obj_file)
    list_tmp = temp.split(".")
    file_name = list_tmp[0]
    dumplogger.info(file_name)
    return file_name


def get_OS_64_32():
    '''get_OS_64_32: Decide OS platform x86 or x64
    Return code:  64Bit - x64
                  32Bit - x86
    Note: platform.architecture()[0] is better way to identify OS platform.'''

    i = ctypes.c_int()
    kernel32 = ctypes.windll.kernel32
    process = kernel32.GetCurrentProcess()
    kernel32.IsWow64Process(process, ctypes.byref(i))
    is64bit = (i.value != 0)
    if is64bit:
        return "64Bit"
    else:
        return "32Bit"

def IniParser(ini_file):
    '''
    IniParser: Parse ini file under /data/common
            Input argu: ini file path
            Return code :
                    A 2-dimention dictionary which contains data in ini.
                    True - success
                    False - fail / error
    '''

    dumplogger.info("Enter IniParser")
    config = configparser.ConfigParser(strict=False)
    dict_config_setting = {}

    try:
        if os.path.isfile(ini_file):

            ##Read ini file
            config.read(ini_file)

            ##Sequence to load all of keys in
            for sub_key in config.keys():

                ##Skip DEFAULT key in ini
                if sub_key != "DEFAULT":

                    ##Declare two-dimensional dictionary
                    dict_config_setting[sub_key] = dict()
                    ##Sequence to load all of config keys and values
                    for sub_item in config.items(sub_key):
                        ##Set config key and value into sub_key(Because config key and value is tuple)
                        dict_config_setting[sub_key][sub_item[0]] = sub_item[1]

            return dict_config_setting
        else:
            dumplogger.info("ini file not exist!!!")

    except configparser.NoSectionError:
        dumplogger.info("Please check your ini file path!!!")
        return False

    except:
        dumplogger.info("Other ini exception error.")
        return False

def GetPFBInfo():
    ''' GetPFBInfo : Get environment type (staging/uat/test) and PFB
            Input argu :
                N/A
            Return code : 1 - success
                          0 - fail
            Note : None  '''

    ##Parse data from EnvironmentConfig.ini
    env_config = IniParser(Config._INIFilePath_)

    ##Store data to config variables
    Config._PFB_["PC"] = env_config["PC-PFB"]
    Config._PFB_["Android"] = env_config["Android-PFB"]
    Config._PFB_["iOS"] = env_config["iOS-PFB"]
    dumplogger.info("PC-PFB = %s" % (Config._PFB_["PC"]))
    dumplogger.info("Android-PFB = %s" % (Config._PFB_["Android"]))
    dumplogger.info("iOS-PFB = %s" % (Config._PFB_["iOS"]))

def AssembleShopeeDomain():
    ''' AssembleShopeeDomain : Set shopee domain url for trigger case
            Input argu : N/A
            Return code : N/A
            Note : None  '''

    if Config._TestCaseRegion_ in ("MX", "MY", "BR", "CO", "AR"):
        GlobalAdapter.UrlVar._Domain_ = Config._EnvType_.lower() + ".shopee.com." + Config._TestCaseRegion_.lower()
    elif Config._TestCaseRegion_ in ("TH", "ID"):
        GlobalAdapter.UrlVar._Domain_ = Config._EnvType_.lower() + ".shopee.co." + Config._TestCaseRegion_.lower()
    else:
        GlobalAdapter.UrlVar._Domain_ = Config._EnvType_.lower() + ".shopee." + Config._TestCaseRegion_.lower()

def LoadRegionalJson():
    '''LoadRegionalJson: Load regional json config into global adapter
    Input argu : params
    Return code:
    Note: None'''

    ##Set folder path
    mypath = Config._DataFilePath_ + Config.dict_systemslash[Config._platform_] + "i18n"

    ##Get all region folder list from folder path
    regions = os.listdir(mypath)

    ##Sequence to load regions from list
    for region_name in regions:

        ##Set json folder path
        region_path = os.path.join(mypath, region_name)

        ##Get all json file list from region folder
        files = os.listdir(region_path)

        for filename in files:

            ##Combine file and path
            fullpath = os.path.join(region_path, filename)

            ##Check object is file
            if os.path.isfile(fullpath):

                try:
                    ##Open json file by region
                    with open(fullpath, 'r') as file_content:
                        if filename == 'ADMIN.json':
                            GlobalAdapter.RegionalJson._i18N_[filename.split(".")[0].upper()].update(json.load(file_content))
                        else:
                            GlobalAdapter.RegionalJson._i18N_[filename.split("_")[0].upper()].update(json.load(file_content))
                except IOError:
                    dumplogger.exception("IOError - Please check your regional json file !!!!")
                except KeyError:
                    dumplogger.exception("KeyError - Please check your regional json file !!!!")
                except:
                    dumplogger.exception("Unknown Error - Please check your regional json file !!!!")

def MappingXpathFromJson(params):
    '''MappingXpathFromJson: Code and xml have to mapping exact xpath from json file
    Input argu : params - xml -> arg
                        - code -> locate
    Return code:  exact xpath
    Note: None'''

    dumplogger.info("Enter GetElementPath GetElementPath_ForXML")
    key_string = params["locate"]

    dumplogger.info("Config._TestCaseRegion_ = %s" % (Config._TestCaseRegion_))

    ##Get current frame stack
    frame_stack = inspect.stack()
    dumplogger.info("Frame stack = %s" % (str(frame_stack)))

    try:
        for frame in frame_stack:
            ##If this is called from function
            ##GetXpath only called by xml or functions, so here is no else
            if "PageFactory" in frame[1] or "Parser" in frame[1]:
                ##If this is called for Admin
                if re.search(r"Admin", frame[1]):
                    data = GlobalAdapter.RegionalJson._i18N_["ADMIN"]
                    ##Get current frame
                    current_frame = frame
                    dumplogger.info("Get calling frame = %s" % (str(current_frame)))
                    break
                ##If this is called for FE
                else:
                    data = GlobalAdapter.RegionalJson._i18N_[Config._TestCaseRegion_]
                    current_frame = frame
                    dumplogger.info("Get calling frame = %s" % (str(current_frame)))
                    break

        #dumplogger.info("Whole json data : \n %s \n", data)
        dumplogger.info("Input parameter from func or xml : %s" % (params))
        dumplogger.info("Xpath key name: %s" % (key_string))

        ##Get filename from frame. ex: \PageFactory\Admin\AdminCMSMethod.py
        ##If failed to find frame, raise exception when re.search
        filename = re.search(r"PageFactory.+\W(\w+).py", str(current_frame[1]), re.IGNORECASE)

        ##If get frame and also find filename
        if filename:
            filename = filename.group(1)
            funccode = current_frame[0].f_code
            funcname = funccode.co_name
            dumplogger.info("Called from: %s.%s" % (filename, funcname))

            ##Get gloabal modules to find all classes
            for gloabal_module in current_frame[0].f_globals.values():
                ##Class module has function and is a class
                if hasattr(gloabal_module, funcname) and inspect.isclass(gloabal_module):
                    ##Get function object
                    target_function = getattr(gloabal_module, funcname)

                    ##Get code object depends on function object
                    if hasattr(target_function, '__code__'):
                        target_function_code = target_function.__code__
                    elif hasattr(target_function.__dict__['_func'], '__code__'):
                        target_function_code = target_function.__dict__['_func'].__code__
                    dumplogger.info("code object: %s" % (target_function_code))

                    ##Check if this function code object is same as code object of frame
                    if target_function_code == funccode:
                        ##reassigned function name as class name + function name
                        funcname = "%s.%s" % (gloabal_module.__name__, funcname)
                        dumplogger.info("Function with class: %s" % (funcname))
                        break
                    else:
                        dumplogger.info("%s != %s" % (target_function_code, funccode))

            ##Check key exist in [filename][function]
            if key_string in data[filename][funcname]:
                dumplogger.info('data[%s][%s][%s] = %s' % (filename, funcname, key_string, data[filename][funcname][key_string]))
                ##Mapping transfer key from shopee json and return
                return MappingTransferKeyFromJson(key_string, data[filename][funcname][key_string])
            else:
                dumplogger.warning("Don't find %s in %s !!" % (key_string, data[filename][funcname]))

        ##XML need replace real xpath locate from json
        else:
            dumplogger.info("data[%s][%s] = %s" % (Config._TestCaseType_, key_string, data[Parser._CASE_['id']][key_string]))
            ##Mapping transfer key from shopee json and return
            return MappingTransferKeyFromJson(key_string, data[Parser._CASE_['id']][key_string])

    except KeyError:
        dumplogger.exception("Please check your xpath setting exist in region json file !!!!")
        print "Please check your xpath exist in region json file, missing key:" + key_string

    except IOError:
        dumplogger.exception("Please check your region json file for xpath !!!!")
        print "Please check your regional json file for xpath !!!!"

    except:
        dumplogger.exception("Encounter framework exception !!!!")
        print "Encounter framework exception !!!!"

def MappingTransferKeyFromJson(key_string, xpath):
    '''MappingTransferKeyFromJson: Code and xml have to mapping exact transferkey from json file
    Input argu : params - key_string -> key name in the transfer key file
                        - xpath -> xpath got from regional file
    Return code:  exact xpath
    Note: None'''

    dumplogger.info("Enter MappingTransferKeyFromJson")
    dumplogger.info("key_string : %s" % (key_string))
    try:
        ##Check if specific region i18NJson has value, if not, it means that current case is not supported for transify key method
        if Config._i18NJson_[Config._TestCaseRegion_]:
            ##Check what platform is record in specific region i18NJson
            for each_platform in Config._i18NJson_[Config._TestCaseRegion_]:
                ##Replace actual string into xpath if there exists '@text' or 'text()' in the xpath got from regional json file
                if xpath:
                    ##Replace transify key for PC
                    if "pc" in each_platform:
                        ##Replace 'string_to_be_replaced' into value in i18N Json
                        if 'text()' in xpath:
                            dumplogger.info(GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string])
                            dumplogger.info("final xpath = %s" % (xpath.replace('string_to_be_replaced', GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string])))
                            return xpath.replace('string_to_be_replaced', GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string])
                        elif '@text' in xpath:
                            dumplogger.info('Transify key not found in pc')
                        ##Return xpath directly since there is no @text or text() in given one
                        else:
                            return xpath
                    ##Replace transify key for Android RN
                    elif "android-rn" in each_platform:
                        ##Replace 'string_to_be_replaced' into value in i18N Json
                        if '@text' in xpath:
                            dumplogger.info(GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string])
                            dumplogger.info("final xpath = %s" % (xpath.replace('string_to_be_replaced', GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string])))
                            return xpath.replace('string_to_be_replaced', GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string])
                        elif 'text()' in xpath:
                            dumplogger.info('Transify key not found in android-rn')
                        ##Return xpath directly since there is no @text or text() in given one
                        else:
                            return xpath
                    ##Return xpath directly for special case
                    else:
                        return xpath
                else:
                    ##For return value in i18N Json directly
                    return GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][each_platform][key_string]

            ##Return xpath directly for special case
            else:
                return xpath

        else:
            dumplogger.info("This platform is not supported for transify key yet!")
            return xpath

    except KeyError:
        dumplogger.info("Key : %s doesn't exist in GlobalAdapter.RegionalJson._TransferKey_ ... maybe this element doesn't need to be replaced.", key_string)
        return xpath

@DecoratorHelper.Extract
def GetXpath(x):
    print "Enter GetXpath"
    """does some math"""
    return x

def GetAndroidRNTransifyKey(env, transify_key_name):
    '''GetAndroidRNTransifyKey: Get exact transify key name and get transify key
        Input argu :
            env - staging / uat / test
            transify_key_name - transify_key_name stored in GlobalApapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_]
        Return code :
                1 - success
                0 - get Android RN transify key failed
        Note: None
    '''
    country_env_key = transify_key_name.split("-android-rn")[0]

    url_transify = "https://mall." + GlobalAdapter.UrlVar._Domain_ + "/rn_static/android/strings/i18n/" + country_env_key + ".json"

    ##Store i18N Json file name by test country for the first time
    if transify_key_name not in Config._i18NJson_[Config._TestCaseRegion_]:
        Config._i18NJson_[Config._TestCaseRegion_].append(transify_key_name)

    ##Initial API Helper
    AP = i18NJsonAPIMethod.i18NJsonController()
    response_status, i18N_json = AP.GetLocalizeJson(url_transify)

    if response_status == 200:
        ##Store data in GlobalAdapter
        GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][transify_key_name] = json.loads(i18N_json)
        return 1
    else:
        dumplogger.error("Get transfy key failed, please check what value you get in function: GetLocalizeJson")
        return 0

def GetPCTransifyKey(env, transify_key_name):
    '''GetPCTransifyKey: Get collection id of transify key and use loop to get all transify key
        Input argu :
            env - staging / uat / test
            transify_key_name - transify_key_name stored in GlobalApapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_]
        Return code :
                1 - success
                0 - get space io token failed
                -1 - get collection id failed
        Note: None
    '''
    url_transify = "https://deo.shopeemobile.com/shopee/stm-sg-live/shopee-pcmall-live-sg/"

    ##Initial API Helper
    AP = i18NJsonAPIMethod.i18NJsonController()

    ##Get space io token back from API
    space_response = AP.GetSpaceIOToken()

    ##Check status code of call API to get space io token
    if not space_response:
        dumplogger.error("Get space io token failed, please check what value you get in function: GetSpaceIOToken")
        return 0

    elif space_response['status'] == 200:
        ##Store shopee space io token in GlobalAdapter
        GlobalAdapter.RegionalJson._SpaceIOToken_ = (json.loads(space_response['text']))['token']
        dumplogger.info("Space io token : %s" % (GlobalAdapter.RegionalJson._SpaceIOToken_))

    else:
        dumplogger.error("Get space io token failed, please check status code which you got from GetSpaceIOToken")
        return 0

    ##Get collection id list length from API
    collection_response_for_length = AP.GetCollectionID(GlobalAdapter.RegionalJson._SpaceIOToken_)

    ##Get all collection id back from API
    collection_response = AP.GetCollectionID(GlobalAdapter.RegionalJson._SpaceIOToken_, json.loads(collection_response_for_length['text'])['data']['total_count'])

    ##Check status code of call API to get collection ID
    if collection_response['status'] == 200:
        try:
            ##Store data in GlobalAdapter
            collection_id_json = json.loads(collection_response['text'])
            for index in range(len(collection_id_json['data']['collections'])):
                if collection_id_json['data']['collections'][index]['keys_count'] != 0:
                    ##Store id of all collection id
                    GlobalAdapter.RegionalJson._CollectionID_.append(collection_id_json['data']['collections'][index]['id'])
                    ##Store key count of all collection id
                    GlobalAdapter.RegionalJson._KeyCount_.append(collection_id_json['data']['collections'][index]['keys_count'])
                else:
                    ##If key count of collection id is 0, store to another GlobalAdapter list
                    GlobalAdapter.RegionalJson._EmptyCollection_.append(collection_id_json['data']['collections'][index]['id'])

            dumplogger.info("ID of collection-id : %s" % (GlobalAdapter.RegionalJson._CollectionID_))
            dumplogger.info("Key count of collection-id : %s" % (GlobalAdapter.RegionalJson._KeyCount_))
            dumplogger.info("Empty collection-id : %s" % (GlobalAdapter.RegionalJson._EmptyCollection_))

        ##Exception handle
        except KeyError:
            dumplogger.exception("Response json format error, please check the API url or response format")
            return -1
        except:
            dumplogger.exception("Encounter unknown error, please check the exception log to debug")
            return -1
    else:
        dumplogger.error("Get collection id failed, please check what value you get in function: GetCollectionID")
        return -1

    ##From all the collection ID's get transify key json
    for collection_ID in GlobalAdapter.RegionalJson._CollectionID_:
        ##Compose file name and send request to get i18n data
        transify_key_name_final = transify_key_name.split("-pc")[0]
        url_transify_final = url_transify + Config._EnvType_ + "/" + transify_key_name_final + ".col" + str(collection_ID) + ".json"

        try:
            ##Get i18N json from transify key collection ID
            status_code, transifykey_i18N_json = AP.GetLocalizeJson(url_transify_final)

            ##Check status code of call API to get transify key
            if status_code == 200:
                ##Store transify key in GlobalAdapter
                GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][transify_key_name].update(json.loads(transifykey_i18N_json))
            else:
                dumplogger.error("Get transify key failed, please check what value you get in function: GetLocalizeJson")
                dumplogger.error("Error collection-id url: %s" % (url_transify_final))

        except requests.exceptions.RequestException:
            ##Encounter connection error, wait for more time and skip getting transify key in this collection id
            dumplogger.exception("Encounter connection error in collection url : %s, wait for more time and request again" % (url_transify_final))
            time.sleep(3)

        except:
            ##Ecounter unknown error
            dumplogger.exception("Ecounter unknown error in collection url : %s" % (url_transify_final))
            time.sleep(3)

    ##return 0 if success
    return 1

def StartProxyServer():
    '''StartProxyServer : Start proxy server before case start
            Input argu :
                N/A
            Return code :
                cmd - command line of proxy server

            Note : None
    '''
    ##Use device number to generate proxy port/proxy web port automatically
    proxy_port = Config._ProxyInfo_["port"] + int(Config._DeviceNumber_)
    web_port = Config._ProxyInfo_["webport"] + int(Config._DeviceNumber_)

    ##Start proxy by command
    cmd = 'start /MIN cmd.exe @cmd /k node ' + Config._AnyProxyPath_ + ' -i --ignore-unauthorized-ssl -p ' + str(proxy_port) + ' -w ' + str(web_port) + ' -d ' + Config._CurDateTime_.split("-")[1] + Config._FolderSuffixString_
    os.system(cmd)
    dumplogger.info("Start proxy server: %s" % (cmd))

    ##Get proxy server process ID
    return cmd

def IniWriter(file_name, data):
    '''IniWriter : Ini file writer function
            Input argu :
                file_name - create a ini file with file name
                data - the data to be wrote in the ini file
            Return code :
                N/A
            Note : None
    '''
    config = configparser.ConfigParser()

    ##key should be the section of ini, value should be the key/value of your config
    try:
        for section, section_data in data.items():
            ##Create a section
            config.add_section(section)
            for subkey, subvalue in section_data.items():
                ##Set key, value in the section
                config.set(section, subkey, subvalue)

        ##Open a file and write all of the config
        with open(file_name, 'w') as config_file:
            config.write(config_file)

    ##Error handlng
    except KeyError:
        dumplogger.exception("Encounter KeyError !!!!")
    except IOError:
        dumplogger.exception("Encounter IOError !!!!")
    except:
        dumplogger.exception("Encounter Other Error !!!!")

def RecordAndroidPerformance():
    '''
    RecordAndroidPerformance : Record android performance, it will get android device perofrmance and record it to log
            Input argu : N/A
            Return code : N/A
    '''

    try:
        ##Get current thread
        current_thread = threading.currentThread()

        ##Get current thread running status. If true will continue get device performance. If false will stop get device performance
        while current_thread.thread_running_status:
            ##Get android device performance info per 1 second
            time.sleep(1)

            ##Get android device performance info
            performance_info = AndroidBaseUICore.AndroidGetDevicePerformanceInfo()

            if performance_info:
                ##Record shopee app CPU to global variable
                Config._DeviceCPUUsage_ = float(performance_info["device_sys_cpu"])

                ##Write all device performance message to log file (To avoid pycodestyle check line too long > 280 characters, use multiple line syntax)
                android_performance_logger.info("Device name: TWQA_%s, Device user CPU: %s%%, Device sys CPU: %s%%, Shopee app CPU: %s%%, Shopee app MEM: %s%%"
                    % (Config._DeviceNumber_, performance_info["device_user_cpu"], performance_info["device_sys_cpu"], performance_info["app_cpu"], performance_info["app_mem"]))

            else:
                ##Record device CPU to global variable
                Config._DeviceCPUUsage_ = 0.0
                android_performance_logger.info("No any device CPU MEM message, maybe device offline or lost conection")

    except NameError:
        ##Record device CPU to global variable
        Config._DeviceCPUUsage_ = 0.0
        android_performance_logger.exception("Encounter NameError Exception!!! Maybe your thread not start, please start your thread and assign running status")

    except:
        ##Record device CPU to global variable
        Config._DeviceCPUUsage_ = 0.0
        android_performance_logger.exception("Encounter Other Exception !!!!")

def StartRecordAndroidDevicePerformance():
    '''
    StartRecordAndroidDevicePerformance : Start record android device performance information
            Input argu : N/A
            Return code : N/A
    '''
    ##Create thread to record android device performance
    android_performance_record_thread = threading.Thread(target=RecordAndroidPerformance, name="android_performance_record_thread", args=())
    android_performance_record_thread.thread_running_status = True
    android_performance_record_thread.start()

def EndRecordAndroidDevicePerformance():
    '''
    EndRecordAndroidDevicePerformance : End record android device performance information
            Input argu : N/A
            Return code : N/A
    '''
    ##For loop all active thread
    for thread in threading.enumerate():
        ##If get android performance record thread, change thread running status "False" to stop it
        if thread.name == "android_performance_record_thread":
            thread.thread_running_status = False
            thread.join()
            break
