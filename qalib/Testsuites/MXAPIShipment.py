import unittest
from Config import dumplogger
from FrameWorkBase import *


class MXAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIShipment Precondition =="
        dumplogger.info("== Setup MXAPIShipment Precondition ==")

    def test_MXAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/MXAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAPIShipment Post condition =="
        dumplogger.info("== Setup MXAPIShipment Post condition ==")
