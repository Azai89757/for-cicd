import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCOrder Precondition =="
        dumplogger.info("== Setup FRPCOrder Precondition ==")

    def test_FRPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-01.xml")

    def test_FRPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-02.xml")

    def test_FRPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-03.xml")

    def test_FRPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-04.xml")

    def test_FRPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-05.xml")

    def test_FRPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-06.xml")

    def test_FRPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCOrder-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCOrder Post condition =="
        dumplogger.info("== Setup FRPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
