import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIShipment Precondition =="
        dumplogger.info("== Setup TWAPIShipment Precondition ==")

    def test_TWAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPIShipment Post condition =="
        dumplogger.info("== Setup TWAPIShipment Post condition ==")
