#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SearchAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import json
from operator import itemgetter

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
import HttpAPICore

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class SearchAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCookie(arg):
        ''' AssignCookie : Assign search admin cookie to API header
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get cookie name and value form DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]

        ##Send SQL to get search admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_search", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Bulid cookie for API header
        search_admin_cookie = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign cookie to API header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = search_admin_cookie

        OK(ret, int(arg['result']), 'SearchAPI.AssignCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReorderItemList(arg):
        ''' ReorderItemList : Reorder item list by item id
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get API response
        res_t = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Get item list
        items = res_t['items']

        ##Reorder item list by item id
        items.sort(key=itemgetter('itemid'), reverse=False)
        dumplogger.info("Sort the item list %s" % (items))

        GlobalAdapter.APIVar._APIResponse_["text"] = json.dumps(res_t)
        dumplogger.info("New GlobalAdapter.APIVar._APIResponse_ = %s" % (GlobalAdapter.APIVar._APIResponse_))

        OK(ret, int(arg['result']), 'SearchAPI.ReorderItemList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StorePrefillID(arg):
        '''
        StorePrefillID : Store Search Prefill ID
                    Input argu :
                        N/A
                    Return code :
                        1 - success
                        0 - failed
        '''
        ret = 1
        prefill_id = 0

        ##Get API response
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Get homepage search prefill id
        prefill_id = response["data"]["prefills"][0]["id"]

        GlobalAdapter.CommonVar._DynamicCaseData_["pid"] = prefill_id
        dumplogger.info("GlobalAdapter.CommonVar._DynamicCaseData_ = %s" % (GlobalAdapter.CommonVar._DynamicCaseData_["pid"]))

        OK(ret, int(arg['result']), 'StorePrefillID ->' + str(prefill_id) + ' stored inside: ' + str(GlobalAdapter.CommonVar._DynamicCaseData_["pid"]))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReplaceSearchPrifillURL(arg):
        '''
        ReplaceSearchPrifillURL : Replace the value of field search_prefill in url with dynamic search prefill id
                    Input argu :
                        N/A
                    Return code :
                        1 - success
                        0 - failed
        '''
        ret = 1
        url = str(GlobalAdapter.APIVar._JsonCaseData_["url"])
        prefill_id = GlobalAdapter.CommonVar._DynamicCaseData_["pid"]

        if "prefill_to_be_replaced" in url:
            dumplogger.info("URL before replaced:" + url)
            GlobalAdapter.APIVar._HttpUrl_ = url.replace("prefill_to_be_replaced", str(prefill_id))
            dumplogger.info("URL replaced result:" + GlobalAdapter.APIVar._HttpUrl_)
        else:
            dumplogger.info("Please check your _JsonCaseData_ stored is correct or not !!")
            ret = 0

        OK(ret, int(arg['result']), 'ReplaceSearchPrifillURL')
