﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminPaymentMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import csv
import time
import json

##Import framework common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def UploadWhiteBlackListCSV(arg):
    '''
    UploadWhiteBlackListCSV : Upload user whitelist or blacklist
            Input argu :
                channel - Cybersource (new)/Cybersource Installment/VN Airpay Ibanking/Airpay Wallet V2/Bank Transfer
                list_type - whitelist/blacklist
                action - import/remove
                file_name - file name
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    channel = arg['channel']
    list_type = arg['list_type']
    action = arg['action']
    file_name = arg['file_name']

    slash = Config.dict_systemslash[Config._platform_]
    file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

    ##Select channel
    xpath = Util.GetXpath({"locate": "channel"})
    xpath = xpath.replace("action_to_be_replaced", action)
    BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": channel, "result": "1"})

    ##Select list type
    xpath = Util.GetXpath({"locate": "list_type"})
    xpath = xpath.replace("action_to_be_replaced", action)
    if list_type == "whitelist":
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "0", "result": "1"})
    elif list_type == "blacklist":
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "1", "result": "1"})

    ##Input path directly to upload csv file
    xpath = Util.GetXpath({"locate": "csv_input"})
    xpath = xpath.replace("action_to_be_replaced", action)
    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
    time.sleep(5)

    OK(ret, int(arg['result']), 'UploadWhiteBlackListCSV')


class AdminPaymentButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVerify(arg):
        '''
        ClickVerify : Change status to verified
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click verified button
        xpath = Util.GetXpath({"locate":"verify_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click verified button", "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentButton.ClickVerify')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLinkedRefundId(arg):
        '''
        ClickLinkedRefundId : Click linked refund ID in admin payments page
                Input argu :
                    refund_status - refund_created / refund_paid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        refund_status = arg["refund_status"]

        ##Click linked refund ID button in admin payments page
        xpath = Util.GetXpath({"locate":refund_status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click linked refund ID button in admin payments page => %s" % (refund_status), "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentButton.ClickLinkedRefundId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLinkedOrderId(arg):
        '''
        ClickLinkedOrderId : Click linked order ID button in admin payments page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click linked order ID button in admin payments page
        xpath = Util.GetXpath({"locate":"linked_order_id_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click linked order ID button in admin payments page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentButton.ClickLinkedOrderId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefundIdLink(arg):
        '''
        ClickRefundIdLink : Click refund Id Link in admin payments detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refund id hyperlink in  payments detail page
        xpath = Util.GetXpath({"locate":"refund_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refund id hyperlink in  payments detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentButton.ClickRefundIdLink')


class AdminPaymentDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRefundID(arg):
        '''
        GetRefundID : Get Refund ID in admin payments page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Retry get refund id 3 times to avoid refund created timing issue
        for retry_times in range(1, 4):

            ##Check refund id locate element exist
            xpath = Util.GetXpath({"locate":"refund_id"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ##Get refund id text from locate element
                BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"refund_id", "result": "1"})
                dumplogger.info("Refund id locate element find!")
                break
            else:
                ##Sleep 5 minute to retry. Because order share service refund create fail, will rerun every 5 minute
                time.sleep(300)

                ##Refresh browser to retry get refund id
                BaseUILogic.BrowserRefresh({"message":"Refresh browser to retry get refund id", "result": "1"})
                dumplogger.info("Retry get refund id. Retry times %s." % (retry_times))
        else:
            ret = 0
            dumplogger.info("Retry 3 times get refund id fail!!!")

        OK(ret, int(arg['result']), 'AdminPaymentDetailPage.GetRefundID -> ' + GlobalAdapter.OrderE2EVar._RefundID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTransactionID(arg):
        '''
        GetTransactionID : Get transaction id in admin payments page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get transaction id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"transaction_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"transaction_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentDetailPage.GetTransactionID -> ' + GlobalAdapter.PaymentE2EVar._TransactionID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnmatch(arg):
        '''
        ClickUnmatch : Click unmatch in admin payments page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click unmatch in admin payments page
        xpath = Util.GetXpath({"locate":"unmatch_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click unmatch button in admin payments page", "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action":"dismiss", "result": "0"})
        OK(ret, int(arg['result']), 'AdminPaymentDetailPage.ClickUnmatch')


class AdminEscrowReleasePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderId(arg):
        '''
        CheckOrderId : Check order Id exist in escrow release page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check order Id exist in escrow release page
        xpath = Util.GetXpath({"locate":"order_id_txt"})
        xpath = xpath.replace('order_id_to_be_replaced', GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.CheckOrderId -> order ID: ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderIdInTab(arg):
        '''
        CheckOrderIdInTab : Check single/multiple tab of escrow release page if order Id exist
                Input argu :
                    tabs - Use ',' to separate 'created,pending,verified,payout,completed'
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        tabs = arg["tabs"]

        ##Get tabs that need to check
        tabs_to_check = tabs.split(',')

        ##Check the tabs if order Id exist
        for tab in tabs_to_check:

            ##Click Escrow Release Status
            xpath = Util.GetXpath({"locate":tab})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click escrow release status", "result": "1"})
            time.sleep(30)

            ##Check order Id exist in specific tab
            xpath = Util.GetXpath({"locate":"order_id_txt"})
            xpath = xpath.replace('order_id_to_be_replaced', GlobalAdapter.OrderE2EVar._OrderID_)
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ret = 1
                break

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.CheckOrderIdInTab -> \'' + tab + '\', order ID: ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchEscrowRelease(arg):
        '''
        SearchEscrowRelease : Search escrow release by order ID
                Input argu :
                    search_type - order_id,bank_account_number,seller_username
                    input_string - order_id,(specific bank account number),(specific seller username)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        input_string = arg["input_string"]

        if input_string == "order_id":
            input_string = GlobalAdapter.OrderE2EVar._OrderID_

        ##Wait for table loading complete before next step
        xpath = Util.GetXpath({"locate":"escrow_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Search order from OrderID, Bank account number, or Seller username
        xpath = Util.GetXpath({"locate":search_type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":input_string, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.SearchEscrowRelease -> ' + search_type + ': ' + input_string)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEscrowReleasePaymentType(arg):
        '''
        ClickEscrowReleasePaymentType : Click Escrow Release Payment Type
                Input argu :
                    payment_type - daily/deferred
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        payment_type = arg["payment_type"]

        ##Click Escrow Release Payment Type
        xpath = Util.GetXpath({"locate":payment_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Escrow Release Payment Type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.ClickEscrowReleasePaymentType' + payment_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEscrowReleaseStatus(arg):
        '''
        ClickEscrowReleaseStatus : Click Escrow Release Status
                Input argu :
                    status - created/pending/verified/payout/completed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click Escrow Release Status
        xpath = Util.GetXpath({"locate":status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click escrow release status", "result": "1"})

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.ClickEscrowReleaseStatus ->' + status)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToEscrowDetail(arg):
        '''
        GoToEscrowDetail : Click order id and go to escrow detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Escrow Release Status
        xpath = Util.GetXpath({"locate":"order_id"})
        xpath = xpath.replace("order_id_to_be_replaced", GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order id and go to escrow detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.GoToEscrowDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAction(arg):
        '''
        ClickAction : Click the btn in Escrow list page and confirm the action
                Input argu :
                    action_type - verify / cancel_order / pending / payout / complete
                    option_code - Choose one verification code from the list. (Optional) (Necessary if action_type is verify or pending)
                    comment - Comment for logging purpose. (Optional)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        action_type = arg['action_type']
        option_code = arg['option_code']
        comment = arg['comment']

        ##Click the button
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click verified button", "result": "1"})

        ##Check if Confirm Action Page poped up
        xpath = Util.GetXpath({"locate": "confirm_action_page"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "1", "result": "1"})

        ##Choose one verification code from the list
        if option_code:
            xpath = Util.GetXpath({"locate":"select_option_code"})
            xpath = xpath.replace("option_to_be_replaced", option_code)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose the verification code", "result": "1"})

        ##Input comment for logging purpose
        if comment:
            xpath = Util.GetXpath({"locate":"comment"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string": comment, "result": "1"})

        ##Click confirm
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminEscrowReleasePage.ClickAction')


class AdminRefundRequestPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTransactionID(arg):
        '''
        GetTransactionID : Get transaction id in refund detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get transaction id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"transaction_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"refund_transaction_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.GetTransactionID -> ' + GlobalAdapter.PaymentE2EVar._TransactionID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchRefundRequest(arg):
        '''
        SearchRefundRequest : Search refund request by refund ID
                Input argu :
                    search_type - order_id/refund_id/buyer_username
                    username - input buyer username or selle username which you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        username = arg["username"]

        ##Search refund request by refund id
        if search_type == "refund_id":
            xpath = Util.GetXpath({"locate":search_type})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._RefundID_, "result": "1"})

        elif search_type == "order_id":
            xpath = Util.GetXpath({"locate":search_type})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

        elif search_type == "buyer_username":
            xpath = Util.GetXpath({"locate":search_type})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":username, "result": "1"})

        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.SearchRefundRequest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefundStatus(arg):
        '''
        ClickRefundStatus : Click Refund Status
                Input argu : status - created/pending/verified/payout/completed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click Escrow Release Status
        xpath = Util.GetXpath({"locate":status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refund status", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.ClickRefundStatus' + status)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyerUsernameLink(arg):
        '''
        ClickBuyerUsernameLink : Click buyer username link in refund request page
                Input argu :
                    username - buyer username
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        username = arg['username']
        ##Check refund id xpath
        xpath = Util.GetXpath({"locate":"buyer_username"})
        buyer_username = xpath.replace("username_to_be_replace", username)
        BaseUICore.Click({"method": "xpath", "locate": buyer_username, "message": "Click buyer_username ", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.ClickBuyerUsernameLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLink(arg):
        '''
        ClickLink : Click link in refund request page
                Input argu :
                    link_position - order_id/return_id/payment_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_position = arg['link_position']

        ##Check refund id xpath
        xpath = Util.GetXpath({"locate":link_position})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message":"Click link in refund request page detail page.", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.ClickLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderId(arg):
        '''
        CheckOrderId : Check order id in refund_list_overview page or refund_info_details page
                Input argu :
                    search_page - refund_list_overview/refund_info_details
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        search_page = arg['search_page']
        ##Get search page
        xpath = Util.GetXpath({"locate":search_page})

        ##Check refund id xpath
        order_id = xpath.replace("order_id_to_be_replace", GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": order_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.CheckOrderId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRefundId(arg):
        '''
        CheckRefundId : Check refund id in refund request page
                Input argu :
                    search_page - refund_list_overview/refund_info_details
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_page = arg['search_page']
        ##Get search page
        xpath = Util.GetXpath({"locate":search_page})

        ##Check refund id xpath
        refund_id = xpath.replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": refund_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.CheckRefundId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToRefundDetail(arg):
        '''
        GoToRefundDetail : Click refund id and go to refund detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Escrow Release Status
        xpath = Util.GetXpath({"locate":"refund_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refund id and go to refund detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRefundRequestPage.GoToRefundDetail')


class AdminMatchPaymentRequestPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MatchBankTransfer(arg):
        '''
        MatchBankTransfer : Match Bank Transfer
                Input argu :
                    account - buyer account (Only ID need this arg, other country please input blank value)
                    approve - no value (don't press approve) / yes (press approve)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        account = arg['account']
        approve = arg["approve"]

        ##Use for loop to query checkout id to avoid cron job delay error
        for retry_times in range(1, 11):
            ##Input statement ID to search target statement
            if Config._TestCaseRegion_ == "TH":
                xpath = Util.GetXpath({"locate":"th_input_stmt_id"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PaymentE2EVar._StatementID_, "result": "1"})
                BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            else:
                xpath = Util.GetXpath({"locate":"input_stmt_id"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PaymentE2EVar._StatementID_, "result": "1"})
                BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
                xpath = Util.GetXpath({"locate": "search_type"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click statement id as search type", "result": "1"})
            time.sleep(5)

            ##Click first statement
            xpath = Util.GetXpath({"locate": "first_statement"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first bank transfer statment record", "result": "1"})

            ##Get checkout ID search field XPath
            if Config._TestCaseRegion_ == "PH":
                xpath = Util.GetXpath({"locate":"ph_search_bar"})
            else:
                xpath = Util.GetXpath({"locate":"search_bar"})

            ##Input checkout ID to search target checkout
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._CheckOutID_, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
            time.sleep(3)

            ##Buyer account (Only ID need this arg, other country please input blank value)
            if account:
                ##Get buyer name display XPath in "Choose a Checkout" table
                xpath = Util.GetXpath({"locate":"buyer_display"})
                xpath = xpath.replace("account_to_be_replaced", account)
            else:
                ##Get checkout id display XPath in "Choose a Checkout" table
                xpath = Util.GetXpath({"locate":"checkout_id_display"})
                xpath = xpath.replace("checkout_id_to_be_replaced", GlobalAdapter.OrderE2EVar._CheckOutID_)

            ##Check "Choose a Checkout" search result is correct
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ret = 1
                dumplogger.info("Checkout ID found!")
                break
            else:
                ret = 0
                dumplogger.info("Checkout ID not found after querying %d times...." % (retry_times))
                BaseUILogic.BrowserRefresh({"message":"Refresh for querying checkout id in admin", "result": "1"})
                time.sleep(5)

        ##Click first checkout
        xpath = Util.GetXpath({"locate":"first_checkout"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click first checkout record", "result": "1"})
        time.sleep(5)

        ##Click Match btn
        xpath = Util.GetXpath({"locate":"match"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click match btn", "result": "1"})
        time.sleep(3)

        ##If appear need to select at least one statement error popup
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action":"dismiss", "result": "0"})
            ret = 0
        elif approve:
            ##Click approve
            xpath = Util.GetXpath({"locate":"approve"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Approve", "result": "1"})
            time.sleep(3)

            ##handle popup
            BaseUILogic.PopupHandle({"handle_action":"dismiss", "result": "0"})

        OK(ret, int(arg['result']), 'AdminMatchPaymentRequestPage.MatchBankTransfer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApprove(arg):
        '''
        ClickApprove : Click approve
                Input argu :
                    account - buyer account (Only ID need this arg, other country please input blank value)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click approve
        xpath = Util.GetXpath({"locate":"approve"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Approve and Create Refund", "result": "1"})
        time.sleep(3)

        ##handle popup
        BaseUILogic.PopupHandle({"handle_action":"dismiss", "result": "0"})

        OK(ret, int(arg['result']), 'AdminMatchPaymentRequestPage.ClickApprove')


class AdminBankStatementOverviewPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetStatementID(arg):
        '''
        GetStatementID : Get statement ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get statement ID
        xpath = Util.GetXpath({"locate":"statement_id"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "statement_id", "result": "1"})\

        OK(ret, int(arg['result']), 'AdminBankStatementOverviewPage.GetStatementID->' + GlobalAdapter.PaymentE2EVar._StatementID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBankTransferCsv(arg):
        '''
        UploadBankTransferCsv : Upload bank transfer csv
                Input argu :
                    file_name - file name contents bank transfer info
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        time.sleep(15)

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

        ##Input path directly to upload csv file
        xpath = Util.GetXpath({"locate": "csv_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
        time.sleep(5)

        ##Click import transaction
        xpath = Util.GetXpath({"locate": "import_transaction"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click import transaction", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBankStatementOverviewPage.UploadBankTransferCsv')


class AdminWhiteBlackListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadWhiteBlackList(arg):
        '''
        UploadWhiteBlackList : Upload buyer whitelist or blacklist
                Input argu :
                    role - buyer/seller
                    channel - Cybersource (new)/Cybersource Installment/VN Airpay Ibanking/Airpay Wallet V2/Bank Transfer
                    list_type - whitelist/blacklist
                    action - import/remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        role = arg['role']
        channel = arg['channel']
        list_type = arg['list_type']
        action = arg['action']

        ##Upload buyer whitelist or blacklist
        UploadWhiteBlackListCSV({"channel": channel, "list_type": list_type, "action": action, "file_name": action + "_" + role + "_list", "result": "1"})

        ##Click import/remove transaction
        xpath = Util.GetXpath({"locate": action + "_" + role + "_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s user id" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhiteBlackListPage.UploadWhiteBlackList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOperateTime(arg):
        '''
        CheckOperateTime : Check operate time is current time
                Input argu :
                    time - created_time / modified_time
                    list_type - whitelist / blacklist
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time = arg['time']
        list_type = arg['list_type']

        ##Get current time and avoid minute will be different
        if Config._TestCaseRegion_ == "TW":
            current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)
        elif Config._TestCaseRegion_ == "PH":
            current_time = XtFunc.GetCurrentDateTime("%m/%d/%Y %H:%M", 0, 0, 0)
        else:
            current_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", 0, 0, 0)
        current_time = current_time[:-1]

        ##Compare date time
        xpath = Util.GetXpath({"locate": time})
        xpath = xpath.replace("list_type_to_be_replaced", list_type)
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": current_time, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhiteBlackListPage.CheckOperateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageNumber(arg):
        '''
        ClickPageNumber : Change blacklist/ whitelist page
                Input argu :
                    list_type - whitelist / blacklist
                    number - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        list_type = arg['list_type']
        number = arg['number']

        ##Change audit log display per page
        xpath = Util.GetXpath({"locate":"current_page"})
        xpath = xpath.replace("list_type_to_be_replaced", list_type)
        xpath = xpath.replace("page_to_be_replaced", number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Change audit log display per page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhiteBlackListPage.ClickPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemDisplayPerPage(arg):
        '''
        SelectItemDisplayPerPage : Select number of item need to display on list
                Input argu :
                    list_type - whitelist / blacklist
                    compare - number of item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        list_type = arg['list_type']
        number = arg['number']

        ##Change audit log display per page
        xpath = Util.GetXpath({"locate":"per_page"})
        xpath = xpath.replace("list_type_to_be_replaced", list_type)
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhiteBlackListPage.SelectItemDisplayPerPage')


class AdminWhiteBlackListButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeListPage(arg):
        '''
        ClickChangeListPage : Click to change page
                Input argu :
                    list_type - whitelist / blacklist
                    action - previous / next / first / last
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        list_type = arg['list_type']
        action = arg['action']

        ##Click to change page
        xpath = Util.GetXpath({"locate":action + "_page"})
        xpath = xpath.replace("list_type_to_be_replaced", list_type)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s page button" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminWhiteBlackListButton.ClickChangeListPage')


class AdminPaymentChannelMaintenanceButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMaintenanceAction(arg):
        '''
        ClickMaintenanceAction : Change payment channel maintenance status
                Input argu :
                    channel - Bank Transfer / COD
                    action - activate / deactivate
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel = arg['channel']
        action = arg["action"]

        ##Click activate/deactivate button
        xpath = Util.GetXpath({"locate":action + "_btn"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s button" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenanceButton.ClickMaintenanceAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Change payment channel maintenance status
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click activate/deactivate button
        xpath = Util.GetXpath({"locate":"close_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenanceButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickForcePropagation(arg):
        '''
        ClickForcePropagation : Click force propagation button to clear cache
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click force propagation button to clear cache
        xpath = Util.GetXpath({"locate":"force_propagation"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click force propagation button", "result": "1"})

        ##Click yes i am sure
        xpath = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click yes i am sure", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenanceButton.ClickForcePropagation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeAuditLogPage(arg):
        '''
        ClickChangeAuditLogPage : Click to change page
                Input argu :
                    action - previous / next / first / last
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']

        ##Click to change page
        xpath = Util.GetXpath({"locate":action + "_page"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s page button" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenanceButton.ClickChangeAuditLogPage')


class AdminPaymentChannelMaintenancePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPaymentChannelMaintenance(arg):
        '''
        EditPaymentChannelMaintenance : Edit payment channel maintenance info
                Input argu :
                    channel - Bank Transfer / COD
                    start_time - maintenance start time
                    end_time - maintenance end time
                    message - message
                    localized message - localized message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel = arg['channel']
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        message = arg["message"]
        localized_message = arg["localized_message"]

        ##Input message
        xpath = Util.GetXpath({"locate":"message_input"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":message, "result": "1"})

        ##Input localized message
        xpath = Util.GetXpath({"locate":"localized_message_input"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":localized_message, "result": "1"})

        if start_time:
            ##Input start time
            start_time = XtFunc.GetCurrentDateTime("%Y/%m/%d %H:%M", minutes=int(start_time))
            xpath = Util.GetXpath({"locate":"start_time"})
            xpath = xpath.replace("channel_to_be_replaced", channel)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_time, "result": "1"})

        if end_time:
            ##Input end time
            end_time = XtFunc.GetCurrentDateTime("%Y/%m/%d %H:%M", minutes=int(end_time))
            xpath = Util.GetXpath({"locate":"end_time"})
            xpath = xpath.replace("channel_to_be_replaced", channel)
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.EditPaymentChannelMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMaintenanceMessage(arg):
        '''
        InputMaintenanceMessage : Input payment channel maintenance message after click activaye/deactivate button
                Input argu :
                    comment - maintenance message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        comment = arg['comment']

        ##input comment
        xpath = Util.GetXpath({"locate":"comment_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":comment, "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate":"submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.InputMaintenanceMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeMaintenanceStatus(arg):
        '''
        ChangeMaintenanceStatus : Change payment channel maintenance status
                Input argu :
                    channel - Bank Transfer / COD
                    action - activate / deactivate
                    comment - maintenance message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel = arg['channel']
        action = arg["action"]
        comment = arg["comment"]

        ##Click activate/deactivate button
        AdminPaymentChannelMaintenanceButton.ClickMaintenanceAction({"channel":channel, "action":action, "result": "1"})

        ##Input message
        AdminPaymentChannelMaintenancePage.InputMaintenanceMessage({"comment":comment, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.ChangeMaintenanceStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreMaintenance(arg):
        '''
        RestoreMaintenance : Restore maintenance to normal
                Input argu :
                        channel - Bank Transfer / COD
                        comment - maintenance message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel = arg['channel']
        comment = arg["comment"]

        ##Check if is still under maintenance, if is then deactivate first
        xpath = Util.GetXpath({"locate":"deactivate_btn"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AdminPaymentChannelMaintenancePage.ChangeMaintenanceStatus({"channel": channel, "action": "deactivate", "comment": comment, "result": "1"})

        ##Edit maintenance end time to now
        AdminPaymentChannelMaintenancePage.EditPaymentChannelMaintenance({"channel":channel, "message":"auto test", "localized_message":"auto test", "start_time":"0", "end_time":"0", "result": "1"})
        AdminPaymentChannelMaintenancePage.ChangeMaintenanceStatus({"channel": channel, "action": "activate", "comment": comment, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.RestoreMaintenance')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReserveMaintenanceStartTime(arg):
        '''
        ReserveMaintenanceStartTime : Reserve date and time
                Input argu :
                    channel - payment channel name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel = arg['channel']

        ##Reserve date and time
        xpath = Util.GetXpath({"locate":"start_time"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"maintenance_starttime", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.ReserveMaintenanceStartTime -> ' + GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReserveMaintenanceEndTime(arg):
        '''
        ReserveMaintenanceEndTime : Reserve date and time
                Input argu :
                    channel - payment channel name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel = arg['channel']

        ##Reserve date and time
        xpath = Util.GetXpath({"locate":"end_time"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"maintenance_endtime", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.ReserveMaintenanceEndTime -> ' + GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompareDateAndTime(arg):
        '''
        CompareDateAndTime : Check if reserved date and time same as date and time that shows on FE
                Input argu :
                        compare - start_time/end_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        compare = arg['compare']

        ##Compare date and time
        xpath = Util.GetXpath({"locate": compare})
        if compare == "start_time":
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._MaintenanceStartTime_, "isfuzzy": "1", "result": "1"})
        elif compare == "end_time":
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._MaintenanceEndTime_, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.CompareDateAndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOperateTime(arg):
        '''
        CheckOperateTime : Check operate time is current time
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current time and avoid minute will be different
        current_time = XtFunc.GetCurrentDateTime("%Y/%m/%d %H:%M", 0, 0, 0)
        current_time = current_time[:-1]

        ##Compare date time
        xpath = Util.GetXpath({"locate": "operate_time"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"3", "result": "1"})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": current_time, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.CheckOperateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAuditLogPage(arg):
        '''
        ChangeAuditLogPage : Change audot log page
                Input argu :
                    page - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page = arg['page']

        ##Input page number
        xpath = Util.GetXpath({"locate":"page_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":page, "result": "1"})
        BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
        BaseUICore.Click({"method": "xpath", "locate": "//span[text()='Page']", "message": "Click outside of the field to make sure change page success", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.ChangeAuditLogPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAuditLogDisplayPerPage(arg):
        '''
        SelectAuditLogDisplayPerPage : Select audot log display per page
                Input argu :
                    number - number of item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        number = arg['number']

        ##Change audit log display per page
        xpath = Util.GetXpath({"locate":"per_page"})
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelMaintenancePage.SelectAuditLogDisplayPerPage')


class AdminPaymentChannelListOverviewPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPaymentListExtraData(arg):
        '''
        EditPaymentListExtraData : Edit extra data field for each payment channel
                Input argu :
                    channel_id - payment channel id
                    extra_data_key - the key name in extra data that you want to edit
                    updated_value - the value of key you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel_id = arg['channel_id']
        extra_data_key = arg['extra_data_key']
        updated_value = arg['updated_value']

        ##Click the edit button for payment channel list
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("channel_id_to_be_replaced", channel_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment list edit button", "result": "1"})

        ##Get original text of extra data, and change the value of key you want to change
        xpath = Util.GetXpath({"locate": "text_area"})
        xpath = xpath.replace("channel_id_to_be_replaced", channel_id)
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        original_text = json.loads(GlobalAdapter.CommonVar._PageAttributes_)
        original_text[extra_data_key] = int(updated_value)
        input_text = json.dumps(original_text)

        ##Input new value to text area
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_text, "result": "1"})

        ##Click the save button for payment channel list
        xpath = Util.GetXpath({"locate":"save_btn"})
        xpath = xpath.replace("channel_id_to_be_replaced", channel_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment list save button", "result": "1"})

        ##Handle popup message
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPaymentChannelListOverviewPage.EditPaymentListExtraData')


class AdminBuyerTxnFeeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewRule(arg):
        '''
        ClickAddNewRule : Click add new rule button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new rule button
        xpath = Util.GetXpath({"locate":"add_new_rule_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickAddNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete button
        xpath = Util.GetXpath({"locate":"delete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnable(arg):
        '''
        ClickEnable : Click enable button
                Input argu :
                    rule_name - name of the rule you want to enable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg['rule_name']

        ##Click enable button
        xpath = Util.GetXpath({"locate":"enable_btn"})
        xpath = xpath.replace("text_to_be_replaced", rule_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickEnable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable button
                Input argu :
                    rule_name - name of the rule you want to disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg['rule_name']

        ##Click disable button
        xpath = Util.GetXpath({"locate":"disable_btn"})
        xpath = xpath.replace("text_to_be_replaced", rule_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click disable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickDisable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePage(arg):
        '''
        ClickChangePage : Click to change page
                Input argu :
                    action - previous / next / first / last
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']

        ##Click to change page
        xpath = Util.GetXpath({"locate":action + "_page"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s page button" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickChangePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnfoldSearchRule(arg):
        '''
        ClickUnfoldSearchRule : Unfold Search Rule
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Unfold Search Rule
        xpath = Util.GetXpath({"locate":"search_rule"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Unfold search rule section", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickUnfoldSearchRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search button
        xpath = Util.GetXpath({"locate":"search_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClearFilter(arg):
        '''
        ClickClearFilter : Click clear filter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click clear filter
        xpath = Util.GetXpath({"locate":"clear_filter"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click clear filter", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickClearFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLongRunning(arg):
        '''
        ClickLongRunning : Click long running
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click long running
        xpath = Util.GetXpath({"locate":"long_running"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click long running", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickLongRunning')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableScope(arg):
        '''
        ClickEnableScope : Click enable scope button in rule setting page
                Input argu :
                    type - min_checkout_amount / min_shop_amount / order_count / seller_status / user_tag / txn_fee_flag / shop_list / item_list / bin_list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click enable scope button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enable scope button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickEnableScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisableScope(arg):
        '''
        ClickDisableScope : Click disable scope button in rule setting page
                Input argu :
                    type - min_checkout_amount / min_shop_amount / order_count / seller_status / user_tag / txn_fee_flag / shop_list / item_list / bin_list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click disable scope button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click disable scope button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeeButton.ClickDisableScope')


class AdminBuyerTxnFeePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePaymentFields(arg):
        '''
        ChoosePaymentFields : Edit buyer txn fee rule payment fields
                Input argu :
                        channel -payment channel
                        option - payment option
                        issuing_bank - issuing bank
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel = arg['channel']
        option = arg["option"]
        issuing_bank = arg["issuing_bank"]

        ##Choose Payment channel
        xpath = Util.GetXpath({"locate":"channel_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click payment channel dropdown", "result": "1"})

        xpath = Util.GetXpath({"locate":"list"})
        xpath = xpath.replace("text_to_be_replaced", channel)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Payment channel: " + channel, "result": "1"})
        time.sleep(2)

        ##Choose option
        if option:
            xpath = Util.GetXpath({"locate":"option_dropdown"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click option dropdown", "result": "1"})

            xpath = Util.GetXpath({"locate":"list"})
            xpath = xpath.replace("text_to_be_replaced", option)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click option: " + option, "result": "1"})
            time.sleep(2)

        ##Choose issuing bank
        if issuing_bank:
            xpath = Util.GetXpath({"locate":"issuing_bank_dropdown"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click issuing bank dropdown", "result": "1"})

            xpath = Util.GetXpath({"locate":"list"})
            xpath = xpath.replace("text_to_be_replaced", issuing_bank)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click issuing bank: " + issuing_bank, "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.ChoosePaymentFields')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseSellerType(arg):
        '''
        ChooseSellerType : Choose seller type
                Input argu :
                    type - all / cb / local
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']

        ##Choose seller type
        xpath = Util.GetXpath({"locate":"seller_type_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click seller type dropdown", "result": "1"})

        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click seller type: " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.ChooseSellerType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleSetting(arg):
        '''
        InputRuleSetting : Edit buyer txn fee rule rate and priority fields
                Input argu :
                    rate_type - Percent Rate / Flat Rate
                    rate - rate(%) or amount(Rp) you want to input
                    priority - priority
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rate_type = arg['rate_type']
        rate = arg["rate"]
        priority = arg["priority"]

        ##Choose rate type
        xpath = Util.GetXpath({"locate":"rate_type_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click rate type dropdown", "result": "1"})

        xpath = Util.GetXpath({"locate":"list"})
        xpath = xpath.replace("text_to_be_replaced", rate_type)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click rate type: " + rate_type, "result": "1"})

        ##Input rate
        xpath = Util.GetXpath({"locate":"rate_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rate, "result": "1"})
        time.sleep(2)

        ##Input priority
        xpath = Util.GetXpath({"locate":"priority_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":priority, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.InputRuleSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputURL(arg):
        '''
        InputURL : Input Learn More URL
                Input argu :
                    url - url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        url = arg['url']

        ##Input Learn More URL
        xpath = Util.GetXpath({"locate":"url"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":url, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.InputURL')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleName(arg):
        '''
        InputRuleName : Input buyer txn fee rule name
                Input argu :
                        rule_name - rule name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_name = arg['rule_name']

        ##Input rule name
        xpath = Util.GetXpath({"locate":"name_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rule_name, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.InputRuleName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTime(arg):
        '''
        InputTime : Input buyer txn fee rule time
                Input argu :
                    start_date - rule start date
                    end_date - rule end date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        start_date = arg['start_date']
        end_date = arg['end_date']

        ##Input rule start time
        xpath = Util.GetXpath({"locate":"start_input"})
        start_time = XtFunc.GetCurrentDateTime("%m%d00%Y%H:%M", days=int(start_date))
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_time + "am", "result": "1"})
        time.sleep(2)

        ##Input rule end time
        if end_date:
            xpath = Util.GetXpath({"locate":"end_input"})
            end_time = XtFunc.GetCurrentDateTime("%m%d00%Y%H:%M", days=int(end_date))
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_time + "am", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.InputTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToRuleDetailPage(arg):
        '''
        GoToRuleDetailPage : Click rule id to go to rule detail page
                Input argu :
                    rule_name - buyer txn fee rule name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg['rule_name']

        ##Click rule id to go to rule detail page
        xpath = Util.GetXpath({"locate":"rule_id"})
        xpath = xpath.replace("rule_name_to_be_replaced", rule_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click rule " + rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.GoToRuleDetailPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchElement(arg):
        '''
        InputSearchElement : Input Search Element
                Input argu :
                    type - rule_id / rule_name / item_id / shop_id / start_time / end_time
                    input_text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']
        input_text = arg['input_text']

        ##Get search element
        xpath = Util.GetXpath({"locate":type})

        ##Input time if type is start_time or end_time
        if type in ("start_time", "end_time"):
            current_time = XtFunc.GetCurrentDateTime("%m%d%Y%H:%M", days=int(input_text))
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":current_time + "am", "result": "1"})

        else:
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":input_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.InputSearchElement')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseSearchElement(arg):
        '''
        ChooseSearchElement : Choose Search Element
                Input argu :
                    type - channelid / user-tag / seller-type / seller-status / status
                    option - target option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        select_type = arg['select_type']
        option = arg['option']

        ##Choose Search Element
        BaseUICore.ExecuteScript({"script":"document.getElementById('filter-" + select_type + "').click()", "result": "1"})
        xpath = Util.GetXpath({"locate":"list"})
        xpath = xpath.replace("text_to_be_replaced", option)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose Search Element: " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.ChooseSearchElement')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ActivateRule(arg):
        '''
        ActivateRule : Modify Rule In DB
                Input argu :
                    db_file - db file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = arg["db_file"]

        ##define assign_list
        assign_list = [{"column": "id", "value_type": "number"}]

        ##Assign rule id to global first
        GlobalAdapter.CommonVar._DynamicCaseData_["id"] = GlobalAdapter.PaymentE2EVar._BuyerTxnFeeRuleID_

        ##Send SQL command
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_payment_id", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.ActivateRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAdditionalScopeSetting(arg):
        '''
        InputAdditionalScopeSetting : Input additional scope setting
                Input argu :
                    type - min_checkout_amount / min_shop_amount / order_count
                    input_text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']
        input_text = arg['input_text']

        ##Input additional scope setting
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":input_text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.InputAdditionalScopeSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAdditionalScopeSetting(arg):
        '''
        SelectAdditionalScopeSetting : Select Additional scope setting by clicking dropdown
                Input argu :
                    type - seller-status / txn-fee-flag / user-tag
                    choice - choice
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']
        choice = arg['choice']

        ##Select additional scope setting
        BaseUICore.ExecuteScript({"script":"document.getElementById('" + type + "').click()", "result": "1"})
        xpath = Util.GetXpath({"locate":"list"})
        xpath = xpath.replace("text_to_be_replaced", choice)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + choice, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.SelectAdditionalScopeSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadAdditionalScopeCSV(arg):
        '''
        UploadAdditionalScopeCSV : Upload additional scope csv file
                Input argu :
                    type - shop / item / bin
                    action - import / remove
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']
        action = arg['action']
        file_name = arg['file_name']

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name

        ##Input path directly to upload csv file
        xpath = Util.GetXpath({"locate": type + "_csv_input_" + action})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
        time.sleep(3)

        ##Click upload button
        xpath = Util.GetXpath({"locate": type + "_" + action + "_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + action + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBuyerTxnFeePage.UploadAdditionalScopeCSV')


class AdminSellerTransactionFeeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeeButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewRule(arg):
        '''
        ClickAddNewRule : Click add new rule button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new rule button
        xpath = Util.GetXpath({"locate":"add_new_rule_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeeButton.ClickAddNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddSpecialSellerGroupRule(arg):
        '''
        ClickAddSpecialSellerGroupRule : Click add special seller group rule button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add special seller group rule button
        xpath = Util.GetXpath({"locate":"add_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add special seller group rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeeButton.ClickAddSpecialSellerGroupRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu :
                    type - general / special_group
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click save button
        xpath = Util.GetXpath({"locate":type + "_save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})

        ##handle popup
        if type == "general" and BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeeButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSummaryLog(arg):
        '''
        ClickSummaryLog : Click summary log
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click summary log button
        xpath = Util.GetXpath({"locate":"summary_log_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click summary log button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeeButton.ClickSummaryLog')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAction(arg):
        '''
        ClickAction : Click action button
                Input argu :
                    type - general / special_group
                    action - delete / enable / disable
                    channel - (only if type is general) Shopee Wallet / Airpay Credit Card / Airpay Credit Card Installment / JKO PAY / JKO COD / JKO BT / Bank Transfer (Taishin VA) / 現付
                    name - (only if type is special_group) Special User Group Name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        action = arg["action"]
        channel = arg["channel"]
        name = arg["name"]

        ##Click action button
        if type == "general":
            xpath = Util.GetXpath({"locate":type + "_" + action + "_btn"})
            xpath = xpath.replace("channel_to_be_replaced", channel)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s button" % action, "result": "1"})
            time.sleep(3)
        elif type == "special_group":
            xpath = Util.GetXpath({"locate":type + "_" + action + "_btn"})
            xpath = xpath.replace("name_to_be_replaced", name)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s button" % action, "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeeButton.ClickAction')


class AdminSellerTransactionFeePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseChannelAndEffectiveDate(arg):
        '''
        ChooseChannelAndEffectiveDate : Edit seller txn fee rule payment fields
                Input argu :
                    channel - payment Channel
                    option - payment option
                    effective_date - rule effective date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel = arg['channel']
        option = arg["option"]
        effective_date = arg["effective_date"]

        ##Choose Payment channel
        xpath = Util.GetXpath({"locate":"channel_list"})
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": channel, "result": "1"})
        time.sleep(5)

        ##Choose option
        if option:
            xpath = Util.GetXpath({"locate":"option_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": option, "result": "1"})

        ##Input effective date
        if effective_date:
            xpath = Util.GetXpath({"locate":"effective_date"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":effective_date, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.ChooseChannelAndEffectiveDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleSetting(arg):
        '''
        InputRuleSetting : Edit rule payment fields
                Input argu :
                    new_rate - New Rate(%)
                    new_rate_flat - New Rate(Flat)
                    new_min_fee - New Min Fee
                    new_cap - New Cap
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        new_rate = arg['new_rate']
        new_rate_flat = arg["new_rate_flat"]
        new_min_fee = arg["new_min_fee"]
        new_cap = arg["new_cap"]

        ##Input new_rate
        xpath = Util.GetXpath({"locate":"new_rate"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_rate, "result": "1"})

        ##Input new_rate_flat
        xpath = Util.GetXpath({"locate":"new_rate_flat"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_rate_flat, "result": "1"})

        ##Input new_min_fee
        xpath = Util.GetXpath({"locate":"new_min_fee"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_min_fee, "result": "1"})

        ##Input new_cap
        xpath = Util.GetXpath({"locate":"new_cap"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_cap, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.InputRuleSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSpecialSellerGroupBasicInfo(arg):
        '''
        InputSpecialSellerGroupBasicInfo : Input special seller group name / payment channel / payment option
                Input argu :
                    name - special seller group name
                    channel - payment channel in drop down list
                    option - payment option in drop down list (only installment has option)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        channel = arg['channel']
        option = arg['option']

        ##Input special seller group name
        xpath = Util.GetXpath({"locate":"name_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})
        time.sleep(5)

        ##Select payment channel
        if channel:
            xpath = Util.GetXpath({"locate":"channel_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": channel, "result": "1"})
            time.sleep(5)

        ##Select payment option
        if option:
            xpath = Util.GetXpath({"locate":"option_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": option, "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.InputSpecialSellerGroupBasicInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSpecialSellerGroupRateSetting(arg):
        '''
        InputSpecialSellerGroupRateSetting : Input special seller group percent rate / flat / rate / minimum fee / cap
                Input argu :
                    percent_rate - percent rate
                    flat_rate - flat rate
                    minimum_fee - minimum fee
                    cap - cap
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        percent_rate = arg['percent_rate']
        flat_rate = arg['flat_rate']
        minimum_fee = arg['minimum_fee']
        cap = arg['cap']

        ##Input percent rate
        if percent_rate:
            xpath = Util.GetXpath({"locate":"percent_rate_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":percent_rate, "result": "1"})

        ##Input flat rate
        if flat_rate:
            xpath = Util.GetXpath({"locate":"flat_rate_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":flat_rate, "result": "1"})

        ##Input minimum fee
        if minimum_fee:
            xpath = Util.GetXpath({"locate":"minimum_fee_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":minimum_fee, "result": "1"})

        ##Input cap
        if cap:
            xpath = Util.GetXpath({"locate":"cap_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":cap, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.InputSpecialSellerGroupRateSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSpecialSellerGroupPriority(arg):
        '''
        InputSpecialSellerGroupPriority : Input special seller group priority
                Input argu :
                    priority - priority (>0)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        priority = arg['priority']

        ##Input priority
        xpath = Util.GetXpath({"locate":"priority_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":priority, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.InputSpecialSellerGroupPriority')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectSpecialSellerGroupType(arg):
        '''
        SelectSpecialSellerGroupType : Select special seller group type
                Input argu :
                    group_type - userid / b2c(cannot fill in userid)
                    user_id - user id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_type = arg['group_type']
        user_id = arg['user_id']

        ##Select group type
        if group_type:
            xpath = Util.GetXpath({"locate":"group_type_list"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "text", "selectkey": group_type, "result": "1"})

        ##Input user id
        if user_id:
            xpath = Util.GetXpath({"locate":"user_id_input"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":user_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.SelectSpecialSellerGroupType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetEffectiveTime(arg):
        '''
        SetEffectiveTime : set seller txn fee effective time in DB
                Input argu :
                    db_file - file name of sql command
                    channelid - payment channel id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = arg["db_file"]
        channelid = arg['channelid']

        ##Store category_name first
        GlobalAdapter.CommonVar._DynamicCaseData_["channelid"] = channelid
        assign_list = [{"column": "channelid", "value_type": "number"}]

        ##Send SQL command process
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_payment_tw", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.SetEffectiveTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSummaryLogRuleId(arg):
        '''
        CheckSummaryLogRuleId : Check rule id is same as setting
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check rule id is same as setting
        xpath = Util.GetXpath({"locate": "rule_id"})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._TransactionFeeRuleID_, "isfuzzy": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.CheckSummaryLogRuleId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTransactionFeeRuleId(arg):
        '''
        GetTransactionFeeRuleId : Get rule id of seller transaction fee
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get rule id of seller transaction fee
        xpath = Util.GetXpath({"locate":"rule_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"transaction_fee_rule_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.GetTransactionFeeRuleId -> ' + GlobalAdapter.PaymentE2EVar._TransactionFeeRuleID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTransactionFeeCoreServerRuleId(arg):
        '''
        GetTransactionFeeCoreServerRuleId : Get core server rule id of seller transaction fee
                Input argu :
                    type - general / special_group
                    channel - payment channel (only if type is general)
                    name - payment channel (only if type is special_group)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        channel = arg["channel"]
        name = arg["name"]

        ##Get core server rule id of seller transaction fee
        xpath = Util.GetXpath({"locate":type + "_cs_rule_id"})
        if type == "general":
            xpath = xpath.replace("channel_to_be_replaced", channel)
        elif type == "special_group":
            xpath = xpath.replace("name_to_be_replaced", name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"transaction_fee_cs_rule_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.GetTransactionFeeCoreServerRuleId -> ' + GlobalAdapter.PaymentE2EVar._TransactionFeeCSRuleID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchSellerType(arg):
        '''
        SwitchSellerType : Switch seller type
                Input argu :
                    seller_type - local / crossborder
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_type = arg["seller_type"]

        ##Switch seller type
        xpath = Util.GetXpath({"locate":"seller_tab"})
        xpath = xpath.replace("seller_type_to_be_replaced", seller_type.upper())
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Switch seller type", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.SwitchSellerType -> ' + seller_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreGeneralTransactionFeeSetting(arg):
        '''
        RestoreGeneralTransactionFeeSetting : Delete general transaction fee setting to restore
                Input argu :
                    channel - payment channel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel = arg["channel"]

        ##Click delete button to restore general transaction fee setting
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace("channel_to_be_replaced", channel)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AdminSellerTransactionFeeButton.ClickEdit({"result": "1"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})
            BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
            time.sleep(3)
        else:
            dumplogger.info("There's no general transaction fee setting need to be restored.")

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.RestoreGeneralTransactionFeeSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreSpecialUserGroup(arg):
        '''
        RestoreSpecialUserGroup : Delete Special User Group to restore
                Input argu :
                    name - Special User Group Name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click delete button to restore Special User Group
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace("name_to_be_replaced", name)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})
            BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
            time.sleep(3)
        else:
            dumplogger.info("There's no special user group need to be restored.")

        OK(ret, int(arg['result']), 'AdminSellerTransactionFeePage.RestoreSpecialUserGroup')


class AdminPaymentChannelBlockingRuleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewRule(arg):
        '''
        ClickAddNewRule : Click add new rule button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new rule button
        xpath = Util.GetXpath({"locate":"add_new_rule_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new rule button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRuleButton.ClickAddNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRuleButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRuleButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExistingRuleID(arg):
        '''
        ClickExistingRuleID : Click existing rule id button
                Input argu :
                    rule_id - Rule ID
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_id = arg["rule_id"]

        ##Click existing rule id button
        xpath = Util.GetXpath({"locate":"rule_id_btn"})
        xpath = xpath.replace('rule_id_to_be_replaced', rule_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click existing rule id button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRuleButton.ClickExistingRuleID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnable(arg):
        '''
        ClickEnable : Click enable button
                Input argu :
                    rule_id - Rule ID
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_id = arg['rule_id']

        ##Click enable button
        xpath = Util.GetXpath({"locate":"enable_btn"})
        xpath = xpath.replace('rule_id_to_be_replaced', rule_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRuleButton.ClickEnable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable button
                Input argu :
                    rule_id - Rule ID
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_id = arg["rule_id"]

        ##Click disable button
        xpath = Util.GetXpath({"locate":"disable_btn"})
        xpath = xpath.replace('rule_id_to_be_replaced', rule_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click disable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRuleButton.ClickDisable')


class AdminPaymentChannelBlockingRulePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPaymentChannelBlockingRuleInfo(arg):
        '''
        InputPaymentChannelBlockingRuleInfo : Input Payment Channel Blocking Rule Info
                Input argu :
                    rule_name - Rule Name
                    seller_type - All / Local / CB
                    start_time - start time
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_name = arg["rule_name"]
        seller_type = arg["seller_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input rule name
        xpath = Util.GetXpath({"locate":"rule_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rule_name, "result": "1"})

        ##Choose seller type
        xpath = Util.GetXpath({"locate":"dropdown_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dropdown icon", "result": "1"})
        xpath = Util.GetXpath({"locate":"seller_type"})
        xpath = xpath.replace('seller_type_to_be_replaced', seller_type)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click seller type", "result": "1"})

        if start_time:
            ##Input start time
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%dT%H:%M", minutes=int(start_time), is_tw_time=1)
            BaseUICore.ExecuteScript({"script": "document.getElementById('start_time').value = '" + start_time + "'", "result": "1"})

        if end_time:
            ##Input end time
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%dT%H:%M", minutes=int(end_time), is_tw_time=1)
            BaseUICore.ExecuteScript({"script": "document.getElementById('end_time').value = '" + end_time + "'", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRulePage.InputPaymentChannelBlockingRuleInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPaymentChannelBlockingRuleDetails(arg):
        '''
        EditPaymentChannelBlockingRuleDetails : Edit Payment Channel Blocking Rule Details
                Input argu :
                    rule_name - Rule Name
                    seller_type - All / Local / CB
                    start_time - start time
                    end_time - end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        rule_name = arg["rule_name"]
        seller_type = arg["seller_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Click edit btn
        AdminPaymentChannelBlockingRuleButton.ClickEdit({"result": "1"})

        ##Input Payment Channel Blocking Rule Info
        AdminPaymentChannelBlockingRulePage.InputPaymentChannelBlockingRuleInfo({"rule_name":rule_name, "seller_type":seller_type, "start_time":start_time, "end_time":end_time, "result": "1"})

        ##Click save btn
        AdminPaymentChannelBlockingRuleButton.ClickSave({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRulePage.EditPaymentChannelBlockingRuleDetails')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadChannelCategoryListCSV(arg):
        '''
        UploadChannelCategoryListCSV : Upload channel and category list CSV
                Input argu :
                    type - channel / category
                    condition - 1 / 2
                    action - import / remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']
        condition = arg['condition']
        action = arg['action']
        file_name = type + "_list" + condition

        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

        ##Input path directly to upload csv file
        xpath = Util.GetXpath({"locate": action + "_" + type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
        time.sleep(3)

        ##Click upload button
        xpath = Util.GetXpath({"locate": action + "_" + type + "_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + action + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentChannelBlockingRulePage.UploadChannelCategoryListCSV')


class AdminProductLevelPaymentButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickImportProductIDs(arg):
        '''
        ClickImportProductIDs : Click import product ids button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click import product ids button
        xpath = Util.GetXpath({"locate":"import_product_ids_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click import product ids button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickImportProductIDs')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemoveProductIDs(arg):
        '''
        ClickRemoveProductIDs : Click remove product ids button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click remove product ids button
        xpath = Util.GetXpath({"locate":"remove_product_ids_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click remove product ids button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickRemoveProductIDs')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseFile(arg):
        '''
        ClickChooseFile : Click choose file button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose file button
        xpath = Util.GetXpath({"locate":"choose_file_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose file button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickChooseFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropdownIcon(arg):
        '''
        ClickDropdownIcon : Click dropdown icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click dropdown icon
        xpath = Util.GetXpath({"locate":"dropdown_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dropdown icon", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickDropdownIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        time.sleep(2)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export button
        xpath = Util.GetXpath({"locate":"export_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close button
        xpath = Util.GetXpath({"locate":"close_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu :
                    row - input the number of row
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row = arg['row']

        ##Click edit button
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace('row_to_be_replaced', row)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu :
                    row - input the number of row
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row = arg['row']

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        xpath = xpath.replace('row_to_be_replaced', row)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button
                Input argu :
                    row - input the number of row
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row = arg['row']

        ##Click delete button
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace('row_to_be_replaced', row)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirmDeletion(arg):
        '''
        ClickConfirmDeletion : Click confirm deletion button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm deletion button
        xpath = Util.GetXpath({"locate":"confirm_deletion_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm deletion button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickConfirmDeletion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseDeletion(arg):
        '''
        ClickCloseDeletion : Click close deletion button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close deletion button
        xpath = Util.GetXpath({"locate":"close_deletion_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click close deletion button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickCloseDeletion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeListPage(arg):
        '''
        ClickChangeListPage : Click to change page
                Input argu :
                    action - previous / next / first / last
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        action = arg['action']

        ##Click to change page
        xpath = Util.GetXpath({"locate":action + "_page"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s page button" % action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentButton.ClickChangeListPage')


class AdminProductLevelPaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePaymentchannel(arg):
        '''
        ChoosePaymentchannel : Choose payment channel
                Input argu :
                    channel_id - all / exclude-all / payment channel id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel_id = arg['channel_id']

        ##Click the payment channel
        xpath = Util.GetXpath({"locate":"payment_channel"})
        xpath = xpath.replace('channel_id_to_be_replaced', channel_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click payment channel:" + channel_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.ChoosePaymentchannel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeletePaymentchannel(arg):
        '''
        DeletePaymentchannel : Click the label to delete the payment channel
                Input argu :
                    channel_id - payment channel id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        channel_id = arg['channel_id']

        ##Click the payment channel label
        xpath = Util.GetXpath({"locate":"payment_channel"})
        xpath = xpath.replace('channel_id_to_be_replaced', channel_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Delete payment channel:" + channel_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.DeletePaymentchannel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckUploadFileResult(arg):
        '''
        CheckUploadFileResult : Check upload file result
                Input argu :
                    string - Result of uploading file
                    isfuzzy - Fuzzy comparison or perfect comparison
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        string = arg["string"]
        isfuzzy = arg["isfuzzy"]

        ##Get value
        GlobalAdapter.CommonVar._PageAttributes_ = str(BaseUICore._WebDriver_.execute_script("return document.querySelector('#upload_plp_result').value"))

        ##Check elements
        BaseUICore.ParseElements({"keyword":string, "isfuzzy":isfuzzy, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.CheckUploadFileResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckDeleteBlacklistResult(arg):
        '''
        CheckDeleteBlacklistResult : Check delete blacklist result
                Input argu :
                    string - Result of deleting a payment channel
                    isfuzzy - Fuzzy comparison or perfect comparison
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        string = arg["string"]
        isfuzzy = arg["isfuzzy"]

        ##Get value
        GlobalAdapter.CommonVar._PageAttributes_ = str(BaseUICore._WebDriver_.execute_script("return document.querySelector('#delete_plp_result').value"))

        ##Check elements
        BaseUICore.ParseElements({"keyword":string, "isfuzzy":isfuzzy, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.CheckDeleteBlacklistResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByProductID(arg):
        '''
        SearchByProductID : Search product id on product level payment page
                Input argu :
                    product_id - product id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        product_id = arg['product_id']

        ##Input product id and search
        xpath = Util.GetXpath({"locate":"product_id_input_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":product_id, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.SearchByProductID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageNumber(arg):
        '''
        ClickPageNumber : Click page number
                Input argu :
                    number - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        number = arg['number']

        ##Change page
        xpath = Util.GetXpath({"locate":"current_page"})
        xpath = xpath.replace("page_to_be_replaced", number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click page number:" + number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.ClickPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemDisplayPerPage(arg):
        '''
        SelectItemDisplayPerPage : Select number of item need to display on list
                Input argu :
                    compare - number of item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        number = arg['number']

        ##Change item display per page
        xpath = Util.GetXpath({"locate":"per_page"})
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductLevelPaymentPage.SelectItemDisplayPerPage')


class AdminPaymentPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByCheckoutID(arg):
        '''
        SearchByCheckoutID : Search checkout id on payment page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input checkout id and search
        xpath = Util.GetXpath({"locate":"search_bar"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._CheckOutID_, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(2)

        ##Click checkout id
        xpath = Util.GetXpath({"locate":"checkout_id"})
        xpath = xpath.replace('replace_text', GlobalAdapter.OrderE2EVar._CheckOutID_)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click target checkout id", "result": "1"})

        ##Switch to the new tab
        BaseUILogic.SwitchBrowserWindow({"isclose": "0", "switchback": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentPage.SearchByCheckoutID -> ' + GlobalAdapter.OrderE2EVar._CheckOutID_)
