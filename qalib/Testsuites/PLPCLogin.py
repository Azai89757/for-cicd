import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCLogin Precondition =="
        dumplogger.info("== Setup PLPCLogin Precondition ==")

    def test_PLPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCLogin-01.xml")

    def test_PLPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCLogin Post condition =="
        dumplogger.info("== Setup PLPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
