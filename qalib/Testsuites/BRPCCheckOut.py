import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCCheckOut Precondition =="
        dumplogger.info("== Setup BRPCCheckOut Precondition ==")

    # def test_BRPCCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/BR/BRPCCheckOut-01.xml")

    # def test_BRPCCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/BR/BRPCCheckOut-02.xml")

    # def test_BRPCCheckOut03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/BR/BRPCCheckOut-03.xml")

    # def test_BRPCCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/BR/BRPCCheckOut-04.xml")

    def test_BRPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCheckOut-05.xml")

    def test_BRPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCheckOut-06.xml")

    def test_BRPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCCheckOut-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCCheckOut Post condition =="
        dumplogger.info("== Setup BRPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
