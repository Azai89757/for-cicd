import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCCancellation Precondition =="
        dumplogger.info("== Setup PLPCCancellation Precondition ==")

    def test_PLPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCancellation-01.xml")

    def test_PLPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCancellation-02.xml")

    def test_PLPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCancellation-03.xml")

    def test_PLPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCancellation-04.xml")

    def test_PLPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCCancellation Post condition =="
        dumplogger.info("== Setup PLPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
