import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class IDPCHomePageBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCHomePageBanner Precondition =="
        dumplogger.info("== Setup IDPCHomePageBanner Precondition ==")

    def test_IDPCHomePageBanner001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-001.xml")

    def test_IDPCHomePageBanner002(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-002.xml")

    def test_IDPCHomePageBanner003(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-003.xml")

    def test_IDPCHomePageBanner004(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-004.xml")

    def test_IDPCHomePageBanner005(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-005.xml")

    def test_IDPCHomePageBanner006(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-006.xml")

    def test_IDPCHomePageBanner007(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-007.xml")

    def test_IDPCHomePageBanner008(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-008.xml")

    def test_IDPCHomePageBanner61(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-61.xml")

    def test_IDPCHomePageBanner62(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePageBanner-62.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCHomePageBanner Post condition =="
        dumplogger.info("== Setup IDPCHomePageBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
