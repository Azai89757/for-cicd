import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCOrder Precondition =="
        dumplogger.info("== Setup PHPCOrder Precondition ==")

    def test_PHPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-01.xml")

    def test_PHPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-02.xml")

    def test_PHPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-03.xml")

    def test_PHPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-04.xml")

    def test_PHPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-05.xml")

    def test_PHPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-06.xml")

    def test_PHPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-07.xml")

    def test_PHPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-08.xml")

    def test_PHPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-09.xml")

    def test_PHPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-10.xml")

    def test_PHPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-11.xml")

    def test_PHPCOrder12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-12.xml")

    def test_PHPCOrder13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-13.xml")

    def test_PHPCOrder14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-14.xml")

    def test_PHPCOrder15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-15.xml")

    def test_PHPCOrder16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-16.xml")

    def test_PHPCOrder17(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-17.xml")

    def test_PHPCOrder18(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-18.xml")

    def test_PHPCOrder19(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCOrder-19.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCOrder Post condition =="
        dumplogger.info("== Setup PHPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
