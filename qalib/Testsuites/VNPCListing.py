import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCListing Precondition =="
        dumplogger.info("== Setup VNPCListing Precondition ==")

    def test_VNPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCListing-01.xml")

    def test_VNPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCListing-02.xml")

    def test_VNPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCListing-03.xml")

    def test_VNPCListing04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCListing-04.xml")

    def test_VNPCListing05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCListing-05.xml")

    def test_VNPCListing06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCListing-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCListing Post condition =="
        dumplogger.info("== Setup VNPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
