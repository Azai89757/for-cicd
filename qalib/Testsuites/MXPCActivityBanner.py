import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class MXPCActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCActivityBanner Precondition =="
        dumplogger.info("== Setup MXPCActivityBanner Precondition ==")

    def test_MXPCActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-01.xml")

    def test_MXPCActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-02.xml")

    def test_MXPCActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-03.xml")

    def test_MXPCActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-04.xml")

    def test_MXPCActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-05.xml")

    def test_MXPCActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-06.xml")

    def test_MXPCActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-07.xml")

    def test_MXPCActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-08.xml")

    def test_MXPCActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCActivityBanner-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCActivityBanner Post condition =="
        dumplogger.info("== Setup MXPCActivityBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
