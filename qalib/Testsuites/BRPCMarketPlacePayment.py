import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCMarketPlacePayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCMarketPlacePayment Precondition =="
        dumplogger.info("== Setup BRPCMarketPlacePayment Precondition ==")

    def test_BRPCMarketPlacePayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMarketPlacePayment-01.xml")

    def test_BRPCMarketPlacePayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMarketPlacePayment-02.xml")

    def test_BRPCMarketPlacePayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMarketPlacePayment-03.xml")

    def test_BRPCMarketPlacePayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMarketPlacePayment-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCMarketPlacePayment Post condition =="
        dumplogger.info("== Setup BRPCMarketPlacePayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
