﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SPMAPIMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import json
import hmac
import hashlib
import random

##Import framework common library
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class SPMAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForPayByBankTransfer(arg):
        '''
        AssignDataForPayByBankTransfer : Pay by Taishin bank transfer
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Insert bank virtual account number into api payload
            GlobalAdapter.APIVar._HttpPayload_["row"]["TRNACTNO"] = GlobalAdapter.PaymentE2EVar._BankVANumber_
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['row']['TRNACTNO'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["row"]["TRNACTNO"]))
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SPMAPI.AssignDataForPayByBankTransfer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForPayByATM(arg):
        '''
        AssignDataForPayByATM : Pay by TH ATM
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Insert ATM reference and sequence into api payload
            GlobalAdapter.APIVar._HttpPayload_["payment"]["reference"] = GlobalAdapter.APIVar._HttpPayload_["payment"]["reference"].replace("reference_to_be_replaced", GlobalAdapter.PaymentE2EVar._ATMRef1_)
            GlobalAdapter.APIVar._HttpPayload_["payment"]["sequence"] = GlobalAdapter.APIVar._HttpPayload_["payment"]["sequence"].replace("num_to_be_replaced", str(random.randrange(1, 10**5)).zfill(5))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))

            ##Insert ATM signature into api headers
            signature = hmac.new("VHom8saD4VbeOsV9z6Ku8YC5RAuPhpKXvEQ57se9RSQTwhARPJ2seTBBugxlEbZx",json.dumps(GlobalAdapter.APIVar._HttpPayload_), hashlib.sha256).hexdigest()
            GlobalAdapter.APIVar._HttpHeaders_["Authorization"] = signature
            dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_ = %s" % (GlobalAdapter.APIVar._HttpHeaders_))

            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SPMAPI.AssignDataForPayByATM')
