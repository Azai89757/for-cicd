import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCMediaStock(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMediaStock Precondition =="
        dumplogger.info("== Setup VNPCMediaStock Precondition ==")

    def test_VNPCMediaStock01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMediaStock-01.xml")

    def test_VNPCMediaStock02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCMediaStock-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCMediaStock Post condition =="
        dumplogger.info("== Setup VNPCMediaStock Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
