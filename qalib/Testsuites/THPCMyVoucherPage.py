import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCMyVoucherPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCMyVoucherPage Precondition =="
        dumplogger.info("== Setup THPCMyVoucherPage Precondition ==")

    def test_THPCMyVoucherPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMyVoucherPage-01.xml")

    def test_THPCMyVoucherPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCMyVoucherPage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCMyVoucherPage Post condition =="
        dumplogger.info("== Setup THPCMyVoucherPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
