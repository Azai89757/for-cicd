import unittest
from Config import dumplogger
from FrameWorkBase import *


class BRAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup BRAPIRestoreHomePopupBanner Precondition ==")

    def test_BRAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/BRAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup BRAPIRestoreHomePopupBanner Post condition ==")
