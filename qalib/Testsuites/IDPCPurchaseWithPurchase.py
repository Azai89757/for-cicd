import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCPurchaseWithPurchase(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPurchaseWithPurchase Precondition =="
        dumplogger.info("== Setup IDPCPurchaseWithPurchase Precondition ==")

    def test_IDPCPurchaseWithPurchase01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPurchaseWithPurchase-01.xml")

    def test_IDPCPurchaseWithPurchase02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPurchaseWithPurchase-02.xml")

    def test_IDPCPurchaseWithPurchase03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPurchaseWithPurchase-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPurchaseWithPurchase Post condition =="
        dumplogger.info("== Setup IDPCPurchaseWithPurchase Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
