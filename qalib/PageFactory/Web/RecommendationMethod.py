﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 RecommendationMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import CommonMethod
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class RecommendationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTrendingKeywordExist(arg):
        '''
        CheckTrendingKeywordExist : to check keyword exist in trending search
                Input argu :
                    key_word - which keyword display in trending search want to click. ex:overlay/rcmd
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        key_word = arg["key_word"]

        ##Check item 10 times to confirm product display normally
        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking Trending Keyword exist" % (retry_count))

            ##check keyword dispaly
            xpath = Util.GetXpath({"locate":key_word})

            ##Whether refresh keyword is exist or not, break the loop as long as the result is expected
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Key word exist!!!!")
                ret = 1
                break
            else:
                dumplogger.info("Key word not exist at %d run..." % (retry_count))
                xpath = Util.GetXpath({"locate":"pts_refresh_button"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click seemore button", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationPage.CheckTrendingKeywordExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSectionExist(arg):
        '''
        CheckSectionExist : Check Section Element display, if not refresh web and find again
                Input argu :
                    element_type -  product_name/trending_search/daily_discover/top_product_section
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        element_type = arg["element_type"]

        ##Check item 10 times to confirm element display normally
        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking element exist" % (retry_count))

            ##check element dispaly
            xpath = Util.GetXpath({"locate":element_type})

            ##Whether refresh button is exist or not, break the loop as long as the result is expected
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("refresh element exist!!!!")
                ret = 1
                break
            else:
                dumplogger.info("refresh element not exist at %d run..." % (retry_count))
                BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
                time.sleep(5)

                if element_type == "daily_discover":
                    ##move to location
                    CommonMethod.ScrollToSection({"section": "mall_section", "result": "1"})
                    time.sleep(5)
                    CommonMethod.ScrollToSection({"section": "rcmd_section", "result": "1"})
                    time.sleep(5)
                    BaseUILogic.MouseScrollEvent({"x": "100", "y": "1400", "result": "1"})

                elif element_type in ("trending_search", "top_product_section"):
                    ##move to location
                    BaseUILogic.MouseScrollEvent({"x": "100", "y": "1200", "result": "1"})
                    time.sleep(5)

        OK(ret, int(arg['result']), 'RecommendationPage.CheckSectionExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductDetailPage(arg):
        ''' GoToProductDetailPage : Click product card and redirect to product detail page
                Input argu :
                    pdc_name - which product card want to click
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        pdc_name = arg["pdc_name"]

        ##click product card and redirect to product detail page
        xpath = Util.GetXpath({"locate":"product_xpath"})
        xpath = xpath.replace("pdc_to_replace", pdc_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click key word", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationPage.GoToProductDetailPage')

class RecommendationButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTrendingKeyword(arg):
        ''' ClickTrendingKeyword : click keyword on trending search to redirect to product landing page
                Input argu :
                    key_word - overlay/rcmd
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        key_word = arg["key_word"]

        ##Click key word
        xpath = Util.GetXpath({"locate":key_word})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click key word", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationButton.ClickTrendingKeyword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        ''' ClickSeeMore : Click see more button to review more product card
                Input argu :
                    button_type - next product/all item/ddtabs_button/topproduct_next_page/top_homepage_product/top_product_in_allpage
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        times = arg['times']

        for click_time in range(int(times)):
            ##Click see more button
            xpath = Util.GetXpath({"locate":button_type})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click seemore button", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToNextShopItem(arg):
        ''' MoveToNextShopItem : click Button to move to next product page
                Input argu :
                    button_type - next_shop_item/prev_shop_item
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        times = arg["times"]

        for move_time in range(int(times)):
            ##Click see more button
            xpath = Util.GetXpath({"locate":button_type})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click seemore button", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationButton.MoveToNextShopItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add product to cart in recommendation module
                Input argu :
                    module - which recommendation module you want
                    product_name - target product in recommendation module
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        module = arg["module"]
        product_name = arg["product_name"]
        ret = 1

        ##Click target product in recommendation module
        xpath = Util.GetXpath({"locate": module})
        xpath = xpath.replace("product_name_to_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product in recommendation page", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductModel(arg):
        '''
        ChooseProductModel : Click to choose product mode on recommendation module
                Input argu :
                    name - product model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click add to cart button
        xpath = Util.GetXpath({"locate":"product_mode"})
        xpath = xpath.replace("product_name_to_replace", name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose product mode on recommendation module", "result": "1"})

        OK(ret, int(arg['result']), 'RecommendationButton.ChooseProductModel')

class DailyDiscoverModuleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetAll(arg):
        ''' ClickGetAll : click get all button in DailyDiscover when product amount more than 60
                Input argu :
                    button_type - which page you need to click
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click get all button
        xpath = Util.GetXpath({"locate":button_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click key word", "result": "1"})

        OK(ret, int(arg['result']), 'DailyDiscoverModuleButton.ClickGetAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFindSimilar(arg):
        ''' ClickFindSimilar : Click find similar and redirect to product landing page
                Input argu :
                    pdc_name - which product card want to click
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        pdc_name = arg["pdc_name"]

        ##click find similar and redirect to product landing page
        xpath = Util.GetXpath({"locate":"product_xpath"})
        xpath = xpath.replace("pdc_to_replace", pdc_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click Find Similar", "result": "1"})

        OK(ret, int(arg['result']), 'DailyDiscoverModuleButton.ClickFindSimilar')

class PtsModulePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPtsImgChanged(arg):
        ''' CheckPtsImgChanged : click button or refresh page and check pts img have been changed
                Input argu :
                    project - OpenCV project path
                    image - OpenCV image to upload
                    browser_refresh - if need to browser refresh and check action, 1=need refresh 0=not need refresh
                    click_action - if click to refresh button and check keyword, 1=click 0=not click
                    env - staging
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 0
        project = arg["project"]
        image = arg["image"]
        browser_refresh = int(arg["browser_refresh"])
        click_action = int(arg["click_action"])
        env = arg["env"]

        ##Check item 10 times to confirm product display normally
        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking product item exist" % (retry_count))

            ##if need browser refresh
            if browser_refresh:
                BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})

                ##move to location, the trending search location in MX, CO and TH is different other country.
                if Config._TestCaseRegion_ in ("MX", "CO", "SG", "PH"):
                    CommonMethod.ScrollToSection({"section": "category_section", "result": "1"})
                    time.sleep(5)
                    CommonMethod.ScrollToSection({"section": "mall_section", "result": "1"})
                    time.sleep(5)
                elif Config._TestCaseRegion_ in ("TH", "BR", "MY", "PL", "CL"):
                    CommonMethod.ScrollToSection({"section": "category_section", "result": "1"})
                    time.sleep(5)
                    CommonMethod.ScrollToSection({"section": "mall_section", "result": "1"})
                    time.sleep(5)
                    CommonMethod.ScrollToSection({"section": "trending_search", "result": "1"})
                    time.sleep(5)
                else:
                    BaseUILogic.MouseScrollEvent({"x": "100", "y": "1400", "result": "1"})
                    time.sleep(5)

            ##Close browser and re-open, then launch to Shopee homepage to check banner dispaly
            time.sleep(15)
            xpath = Util.GetXpath({"locate":"pts_refresh_button"})
            BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

            ##if need to click pts refresh button
            if click_action:
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click pts refresh button", "result": "1"})

            time.sleep(10)

            ##Start to begin image compare by different file types
            ##Means to use Picture compare function
            match_result = XtFunc.MatchPictureImg({"mode":"web", "project":project, "image":image, "env":env, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether banner is exist or not, break the loop as long as the result is expected
            if match_result != ret:
                dumplogger.info("product image matching successful!!!!")
                ret = 1
                break
            else:
                dumplogger.info("product image matching failed at %d run..." % (retry_count))

        OK(ret, int(arg['result']), 'PtsModulePage.CheckPtsImgChanged')

class VariationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickModifyVariationQuantity(arg):
        '''
        ClickModifyVariationQuantity : Click increase/decrease quantity in variation page
                Input argu :
                    action - increase / decrease
                    quantity - increase / decrease quantity
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        quantity = arg["quantity"]

        ##Do action for quantity of time
        for action_time in range(int(quantity)):
            xpath = Util.GetXpath({"locate": action})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + action + " quantity", "result": "1"})
            time.sleep(4)
            dumplogger.info("%s time : %s" % (action, action_time))

        OK(ret, int(arg['result']), 'VariationPage.ClickModifyVariationQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationQuantity(arg):
        '''
        InputVariationQuantity : Input quantity of variation page directly
                Input argu :
                    quantity - quantity of item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        quantity = arg["quantity"]

        ##Input quantity
        xpath = Util.GetXpath({"locate":"quantity_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": quantity, "result": "1"})

        OK(ret, int(arg['result']), 'VariationPage.InputVariationQuantity')

class VariationPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart in variation page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add to cart btn
        xpath = Util.GetXpath({"locate": "add_to_cart_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart btn", "result": "1"})

        OK(ret, int(arg['result']), 'VariationPageButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel in variation page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart btn", "result": "1"})

        OK(ret, int(arg['result']), 'VariationPageButton.ClickCancel')
