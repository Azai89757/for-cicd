import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSignup Precondition =="
        dumplogger.info("== Setup VNPCSignup Precondition ==")

    def test_VNPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSignup Post condition =="
        dumplogger.info("== Setup VNPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
