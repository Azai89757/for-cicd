import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCShopeeVoucher Precondition =="
        dumplogger.info("== Setup FRPCShopeeVoucher Precondition ==")

    def test_FRPCShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCShopeeVoucher-01.xml")

    def test_FRPCShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCShopeeVoucher-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCShopeeVoucher Post condition =="
        dumplogger.info("== Setup FRPCShopeeVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
