import unittest
from Config import dumplogger
from FrameWorkBase import *


class COAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup COAPIRestoreHomePopupBanner Precondition ==")

    def test_COAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/COAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup COAPIRestoreHomePopupBanner Post condition ==")
