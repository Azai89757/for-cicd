import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpShopeeVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpShopeeVoucher Precondition =="
        dumplogger.info("== Setup TWHttpShopeeVoucher Precondition ==")

    def test_TWHttpShopeeVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucher-01.xml")

    def test_TWHttpShopeeVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucher-02.xml")

    def test_TWHttpShopeeVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucher-03.xml")

    def test_TWHttpShopeeVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucher-04.xml")

    def test_TWHttpShopeeVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucher-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpShopeeVoucher Post condition =="
        dumplogger.info("== Setup TWHttpShopeeVoucher Post condition ==")
