import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCCheckOut Precondition =="
        dumplogger.info("== Setup PLPCCheckOut Precondition ==")

    # def test_PLPCCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLPCCheckOut-01.xml")

    def test_PLPCCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCheckOut-02.xml")

    def test_PLPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCheckOut-03.xml")

    def test_PLPCCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCheckOut-04.xml")

    def test_PLPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCheckOut-05.xml")

    # def test_PLPCCheckOut06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLPCCheckOut-06.xml")

    def test_PLPCCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCheckOut-07.xml")

    def test_PLPCCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCCheckOut-08.xml")

    # def test_PLPCCheckOut09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLPCCheckOut-09.xml")

    # def test_PLPCCheckOut10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLPCCheckOut-10.xml")

    # def test_PLPCCheckOut11(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLPCCheckOut-11.xml")

    # def test_PLPCCheckOut12(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PL/PLPCCheckOut-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCCheckOut Post condition =="
        dumplogger.info("== Setup PLPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
