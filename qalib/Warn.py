#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
  Warn.py: Get current line number and print argument with line number
'''
import sys
import inspect
import os

##Global Debug Flag, 0: Disable Debug message, 1: Enable Debug message
DEBUG = 1
##FrameWorkBase.py Debug Flag, show FrameWorkBase debug message only
FrameWorkBase_DEBUG = 0
##XtFunc.py Debug Flag, show XtFunc debug message only
XTFUNC_DEBUG = 1

def warn(var=None):
    ##0 represents this line , 1 represents line at caller
    callerframerecord = inspect.stack()[1]
    frame = callerframerecord[0]
    info = inspect.getframeinfo(frame)
    ##__FILE__
    #print "info.filename"
    flag = 0

    if 1 == OSPREY_DEBUG:
        if 'Osprey' in info.filename:
            flag = 1
    if 1 == FrameWorkBase_DEBUG:
        if 'FrameWorkBase' in info.filename:
            print "FrameWorkBase!"
            os.system("pause")
    if 1 == XTFUNC_DEBUG:
        if 'XtFunc' in info.filename:
            flag = 1

    if var is None:
        if DEBUG or flag:
            ##__FUNCTION__, __LINE__
            print "Warning at Func: %s %d line." % (info.function, info.lineno)
    else:
        if DEBUG or flag:
            ##__FUNCTION__, __LINE__
            print "%s at Func: %s %d line." % (var, info.function, info.lineno)


def Main():
    ##for this line
    warn()
