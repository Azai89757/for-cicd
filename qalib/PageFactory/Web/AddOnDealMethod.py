#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AddOnDealMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def CheckAddOnDealExist(arg):
    '''
    CheckAddOnDealExist : Check if add on deal section exist in pdp
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 0

    for retry_count in range(1, 5):
        dumplogger.info("Start round: %s for checking add on deal section displayed in PDP" % (retry_count))

        ##check if the add on deal section title exist in PDP
        xpath = Util.GetXpath({"locate":"addondeal_title"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ret = 1
            dumplogger.info("Checking add on deal section success!!!!")
            break
        else:
            #if not exist, refresh the browser and check again
            dumplogger.info("Checking add on deal section failed at %d run..." % (retry_count))
            BaseUILogic.BrowserRefresh({"message": "Refresh browser", "result": "1"})
            time.sleep(15)

    OK(ret, int(arg['result']), 'CheckAddOnDealExist')


class AddOnDealSelectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductName(arg):
        '''
        ClickProductName : Click product name
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product name
        xpath = Util.GetXpath({"locate": "product_name_label"})
        xpath_product_name_label = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_product_name_label, "message": "Click product name", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionButton.ClickProductName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductImg(arg):
        '''
        ClickProductImg : Click product img
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product img
        xpath = Util.GetXpath({"locate": "product_img"})
        product_img = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method": "xpath", "locate": product_img, "message": "Click product img", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionButton.ClickProductImg')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add to cart
        xpath = Util.GetXpath({"locate": "aod_button_add_to_cart"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok
        xpath = Util.GetXpath({"locate": "aod_button_ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickQuantity(arg):
        '''
        ClickQuantity : In cart page click quantity
                Input argu :
                    quantity_type - add / minus
                    product_name - product name
                    quantity - how many times to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        quantity_type = arg['quantity_type']
        product_name = arg['product_name']
        quantity = arg['quantity']

        for click_times in range(int(quantity)):
            ##Click quantity
            xpath = Util.GetXpath({"locate":quantity_type})
            xpath_quantity_btn = xpath.replace('product_name_to_be_replace', product_name)
            BaseUICore.Click({"method":"xpath", "locate":xpath_quantity_btn, "message":"click quantity", "result": "1"})
            dumplogger.info("Click quantity %d times" % (click_times))
            time.sleep(1)

        OK(ret, int(arg['result']), 'AddOnDealSelectionButton.ClickQuantity')

class AddOnDealSelectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnItemCheckbox(arg):
        '''
        ClickAddOnItemCheckbox : Click add on item checkbox
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click add on item checkbox
        xpath = Util.GetXpath({"locate":"addon_item_checkbox"})
        xpath_addon_item_checkbox = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_addon_item_checkbox, "message":"Click add on item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionPage.ClickAddOnItemCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductModelDropDown(arg):
        '''
        ClickProductModelDropDown : Click product model dropdown
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product model dropdown
        xpath = Util.GetXpath({"locate":"aod_label_variations"})
        xpath_product_model_dropdown = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_model_dropdown, "message":"Click product model dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionPage.ClickProductModelDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductModel(arg):
        '''
        SelectProductModel : Select product model
                Input argu :
                    product_name - product name
                    model_name - model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']
        model_name = arg['model_name']

        ##Click product model dropdown
        xpath = Util.GetXpath({"locate":"aod_label_variations"})
        xpath_product_model_dropdown = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_model_dropdown, "message":"Click product model dropdown", "result": "1"})

        ##Click product model name button
        xpath = Util.GetXpath({"locate":"product_model_name_btn"})
        xpath_product_model_name_btn = xpath.replace("model_name_to_be_replace", model_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_model_name_btn, "message":"Click product model name button", "result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"aod_button_confirm"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AddOnDealSelectionPage.SelectProductModel')
