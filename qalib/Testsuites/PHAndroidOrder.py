import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *

class PHAndroidOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAndroidOrder Precondition =="
        dumplogger.info("== Setup PHAndroidOrder Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"ph"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_PHAndroidOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-01.xml")

    def test_PHAndroidOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-02.xml")

    def test_PHAndroidOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-03.xml")

    def test_PHAndroidOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-04.xml")

    def test_PHAndroidOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-05.xml")

    def test_PHAndroidOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-06.xml")

    def test_PHAndroidOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-07.xml")

    def test_PHAndroidOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-08.xml")

    def test_PHAndroidOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-09.xml")

    def test_PHAndroidOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-10.xml")

    def test_PHAndroidOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-11.xml")

    def test_PHAndroidOrder12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-12.xml")

    def test_PHAndroidOrder13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-13.xml")

    def test_PHAndroidOrder14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-14.xml")

    def test_PHAndroidOrder15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHAndroidOrder-15.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAndroidOrder Post condition =="
        dumplogger.info("== Setup PHAndroidOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
