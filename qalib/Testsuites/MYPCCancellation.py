import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCCancellation Precondition =="
        dumplogger.info("== Setup MYPCCancellation Precondition ==")

    def test_MYPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCancellation-01.xml")

    def test_MYPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCancellation-02.xml")

    def test_MYPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCancellation-03.xml")

    def test_MYPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCancellation-04.xml")

    def test_MYPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCCancellation Post condition =="
        dumplogger.info("== Setup MYPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
