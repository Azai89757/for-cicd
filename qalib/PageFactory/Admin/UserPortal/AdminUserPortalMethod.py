##Import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def LaunchUserPortalAdmin(arg):
    '''
    LaunchUserPortalAdmin : Go to user portal admin page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    user_portal_admin_cookie = {}
    db_file = "admin_cookie_user_portal"
    url = "https://admin.user.staging.shopee.io/?region=" + Config._TestCaseRegion_

    if 'user_portal_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Get current env and country
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        user_portal_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        user_portal_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        user_portal_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['user_portal_admin'] = [user_portal_admin_cookie]

    ## Go to user portal admin
    BaseUICore.GotoURL({"url": url, "result": "1"})
    dumplogger.info("url = %s" % (url))
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type": "user_portal_admin", "result": "1"})
    time.sleep(5)

    ##Go to User Portal Admin
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})
    time.sleep(10)

    OK(ret, int(arg['result']), 'LaunchUserPortalAdmin')

class AdminUserPortalTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnLeftPanel(arg):
        '''
        ClickTabOnLeftPanel : Click tab on left panel in user portal admin page
                Input argu :
                    tab_type : user_data / me_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": tab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalTab.ClickTabOnLeftPanel -> ' + tab_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserDataSubTab(arg):
        '''
        ClickUserDataSubTab : Click user data sub tab in user portal page left panel
                Input argu :
                    subtab - user_tag
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg['subtab']

        ##Click user data sub tab
        xpath = Util.GetXpath({"locate": tab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user data sub tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalTab.ClickUserDataSubTab -> ' + tab_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMePageSubTab(arg):
        '''
        ClickMePageSubTab : Click me page sub tab in user portal page left panel
                Input argu :
                    subtab - app / pc
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg['subtab']

        ##Click me page sub tab
        xpath = Util.GetXpath({"locate": tab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click me page sub tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalTab.ClickMePageSubTab -> ' + tab_type)


class AdminUserTagPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchUserTag(arg):
        '''
        SearchUserTag : Search for user tag
                Input argu :
                    search_type - tag_id
                    value - value to input in search_type field
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg['search_type']
        value = arg['value']

        ##Select search type
        xpath = Util.GetXpath({"locate": "type_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click type field", "result": "1"})
        xpath = Util.GetXpath({"locate": search_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search type", "result": "1"})
        time.sleep(3)

        ##Search user tag
        xpath = Util.GetXpath({"locate": "input_field"})
        ##Click column to enable input
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input field", "result": "1"})

        ##Input value
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})
        time.sleep(3)

        ##Click tag
        xpath = Util.GetXpath({"locate": 'result_link'})
        xpath = xpath.replace("result_replaced", value)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop ID link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagPage.SearchUserTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UpdateTagSeller(arg):
        '''
        UpdateTagSeller : Add or remove user tag to seller
                Input argu :
                    update_type - add or remove
                    file_name - file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        update_type = arg['update_type']

        ##Get upload csv input field
        xpath = Util.GetXpath({"locate": update_type})

        ##Upload shops id csv file
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate":xpath, "element": "", "element_type": "xpath", "project": Config._TestCaseFeature_, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click submit
        xpath = Util.GetXpath({"locate": "submit"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})
        time.sleep(3)

        ##Click ok
        xpath = Util.GetXpath({"locate": "ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminUserTagPage.UpdateTagSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateUserTag(arg):
        '''
        CreateUserTag : Create new user tag
                Input argu :
                    tag_name - user tag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tag_name = arg['tag_name']

        ##Click submit button
        AdminUserTagButton.ClickCreateNewTag({"result": "1"})
        time.sleep(5)

        ##Input tag name
        xpath = Util.GetXpath({"locate":"tag_name_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":tag_name, "result": "1"})

        ##Input tag description
        xpath = Util.GetXpath({"locate":"tag_description_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":tag_name, "result": "1"})

        ##Click public label
        xpath = Util.GetXpath({"locate": "public_label"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click public label", "result": "1"})

        ##Input attribute description
        xpath = Util.GetXpath({"locate":"attr_description_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":tag_name, "result": "1"})

        ##Input attribute description
        xpath = Util.GetXpath({"locate":"attr_key_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":tag_name, "result": "1"})

        ##Input attribute description
        xpath = Util.GetXpath({"locate":"attr_value_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"True", "result": "1"})

        ##Click submit button
        AdminUserTagButton.ClickSubmit({"result": "1"})

        time.sleep(5)

        ##Check if rule is successfully created
        xpath = Util.GetXpath({"locate":"edit_button"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagPage.CreateUserTag')


class AdminUserTagButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUploadIdType(arg):
        '''
        SearchUser : Select upload id type
                Input argu :
                    id_type - userid / shopid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        id_type = arg['id_type']

        ##Click Id type field
        xpath = Util.GetXpath({"locate": "id_type_filed"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click id type field", "result": "1"})

        ##Select id type
        xpath = Util.GetXpath({"locate": id_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select id type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.SelectUploadIdType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectTagsType(arg):
        '''
        SelectTagsType : Select tags type
                Input argu :
                    tag_type - all_tags
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tag_type = arg['tag_type']

        ##Click Id type field
        xpath = Util.GetXpath({"locate": tag_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tag type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.SelectTagsType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMiddleSectionTab(arg):
        '''
        ClickMiddleSectionTab : Click middle section tab in tag detail page
                Input argu :
                    tab_type - attach_Tag
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click middle section tab in user tag detail page
        xpath = Util.GetXpath({"locate":tab_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click middle section tag detail page => %s" % (tab_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.ClickMiddleSectionTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClearSearch(arg):
        '''
        ClickClearSearch : Click clear search in tag list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click clear search
        xpath = Util.GetXpath({"locate":"clear_search_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click clear search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.ClickClearSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNewTag(arg):
        '''
        ClickCreateNewTag : Click create new tag in user tag page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create button
        xpath = Util.GetXpath({"locate":"create_tag_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click create tag button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.ClickCreateNewTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit button in user tag page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit button
        xpath = Util.GetXpath({"locate":"submit_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteTag(arg):
        '''
        ClickDeleteTag : Click delete tag in user tag page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete button
        xpath = Util.GetXpath({"locate":"delete_tag_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete tag button", "result": "1"})

        ##Click ok button to delete tag
        xpath = Util.GetXpath({"locate":"ok_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserTagButton.ClickDeleteTag')


class AdminUserPortalPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchUser(arg):
        '''
        SearchUser : Search for user
                Input argu :
                    search_type - userid / username / shopid / shopname / email / phone
                    value - value to input in search_type field
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg['search_type']
        value = arg['value']

        ##Click reset button to ensure all the searching column is empty
        xpath = Util.GetXpath({"locate": "reset_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})
        time.sleep(3)

        ##Search user by specific type
        xpath = Util.GetXpath({"locate": search_type})
        ##Click column to enable input
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input field", "result": "1"})
        ##Input value
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
        time.sleep(3)

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})
        time.sleep(5)

        ##Click target user to go to user info page
        xpath = Util.GetXpath({"locate": "target_user"})
        xpath = xpath.replace("value_to_be_replaced", value)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click the user id of the first serching result", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminUserPortalPage.SearchUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchUserByRandomUsername(arg):
        '''
        SearchUserByRandomUsername : Search for random username
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Search random username in admin page
        random_username = GlobalAdapter.GeneralE2EVar._RandomString_
        AdminUserPortalPage.SearchUser({"search_type": "username", "value": random_username, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalPage.SearchUserByRandomUsername -> Search for random username: ' + random_username)


class AdminUserPortalUserInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNewUserName(arg):
        '''
        InputNewUserName : Input new user name
                Input argu :
                    new_username - new user name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        new_username = arg["new_username"]
        ret = 1

        ##Input user name
        xpath = Util.GetXpath({"locate": "username"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": new_username, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoPage.InputNewUserName ->' + new_username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNewEmail(arg):
        '''
        InputNewEmail : Input new email
                Input argu :
                    new_email - new email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        new_email = arg["new_email"]
        ret = 1

        ##Input new email
        xpath = Util.GetXpath({"locate": "new_email"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": new_email, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoPage.InputNewEmail ->' + new_email)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeUserStatus(arg):
        '''
        ChangeUserStatus : Change user status
                Input argu :
                    status - deleted/frozen/banned
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        status = arg["status"]
        ret = 1

        ##Click edit user status btn
        xpath = Util.GetXpath({"locate": "edit_status_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit user status button", "result": "1"})
        time.sleep(3)

        ##Click which user status to change
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click which user status to change", "result": "1"})
        time.sleep(3)

        if status in ("frozen","banned"):
            xpath = Util.GetXpath({"locate": "fraud_tag_close_button"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ##Reomve invalid fraud tag once
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete fraud tag", "result": "1"})

            xpath = Util.GetXpath({"locate": "tag_column"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tag column to open list", "result": "1"})
            time.sleep(3)
            xpath = Util.GetXpath({"locate": "input_tag_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Auto_cc", "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input reason
        xpath = Util.GetXpath({"locate": "reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "auto test", "result": "1"})

        ##Click ok btn to save changes
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn to save changes", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoPage.ChangeUserStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetLastLoginSignupTime(arg):
        '''
        GetLastLoginSignupTime : Get last login time
                Input argu :
                    check_type - login, signup
                    platform - pc, app
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_type = arg['check_type']
        platform = arg['platform']

        ##Get last login / register time
        xpath = Util.GetXpath({"locate": check_type + "_" + platform})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        GlobalAdapter.AccountE2EVar._LoginSignupTime_ = GlobalAdapter.CommonVar._PageAttributes_
        dumplogger.info(GlobalAdapter.AccountE2EVar._LoginSignupTime_)

        OK(ret, int(arg['result']), "AdminUserPortalUserInfoPage.GetLastLoginSignupTime")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLastLoginSignupTime(arg):
        '''
        CheckLastLoginSignupTime : Check register / last login time
                Input argu :
                    check_type - login, signup
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_type = arg['check_type']

        if check_type == 'login':
            ##If didn't pass, time might not update. Refresh browser and check again.
            for check_times in range(10):

                ##Get last login time
                xpath = Util.GetXpath({"locate": Config._TestCasePlatform_.lower()})
                BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
                BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

                ##Get time after login
                after_login = GlobalAdapter.CommonVar._PageAttributes_
                dumplogger.info("after time: %s" % (after_login))

                ##Get time before login
                before_login = GlobalAdapter.AccountE2EVar._LoginSignupTime_
                dumplogger.info("before time: %s" % (before_login))

                ##If date before login was not same as date after login, login successfully
                if after_login != before_login:
                    dumplogger.info("Compare time success")
                    break
                else:
                    dumplogger.error("Compare time failed")
                    ret = 0
                    time.sleep(30)
                    BaseUILogic.BrowserRefresh({"message":"Browser refresh", "result": "1"})

        elif check_type == 'signup':

            ##Get current date
            if Config._TestCaseRegion_ in ("VN", "ID", "TH"):
                current_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", 0, 0, 0)
            elif Config._TestCaseRegion_ == "TW":
                current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)
            elif Config._TestCaseRegion_ == "PH":
                current_time = XtFunc.GetCurrentDateTime("%m/%d/%Y %H:%M", 0, 0, 0)

            current_date = current_time.split()[0]
            dumplogger.info("Current date: %s" % (current_date))

            ##Get signup date
            signup_date = GlobalAdapter.AccountE2EVar._LoginSignupTime_.split()[0]
            dumplogger.info("Signup date: %s" % (signup_date))

            ##compare signup date
            if signup_date == current_date:
                dumplogger.info("Compare time success")
            else:
                dumplogger.error("Compare time failed")
                ret = 0

        else:
            dumplogger.info("Please input login or signup as argument to check time")
            ret = -1

        OK(ret, int(arg['result']), "AdminUserPortalUserInfoPage.CheckLastLoginSignupTime")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeUserStatusToNormal(arg):
        '''
        ChangeUserStatusToNormal : Change any user status to normal
                Input argu :
                    status - deleted / banned / frozen
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg['status']

        ##Check user current status, and change status when current status are not normal
        xpath = Util.GetXpath({"locate": "user_normal_status"})
        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "0"}):
            ##Click edit user status
            xpath = Util.GetXpath({"locate": "status_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user status button", "result": "1"})

            ##Click normal status
            xpath = Util.GetXpath({"locate": "normal"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click normal", "result": "1"})

            xpath = Util.GetXpath({"locate": "fraud_tag_close_button"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ##Reomve invalid fraud tag once
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to delete fraud tag", "result": "1"})

            ##Input reason
            xpath = Util.GetXpath({"locate": "reason_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation Restore", "result": "1"})

            ##Click OK button
            xpath = Util.GetXpath({"locate": "ok_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK", "result": "1"})
            time.sleep(10)

        OK(ret, int(arg['result']), "AdminUserPortalUserInfoPage.ChangeUserStatusToNormal")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UnlinkUserPhone(arg):
        '''
        UnlinkUserPhone : Unlink user phone into without phone status
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Unlink Phone in user detail page
        xpath = Util.GetXpath({"locate": "unlink_phone_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Unlink Phone in user detail page", "result": "1"})

        ##Click OK in popup confirm dialog
        xpath = Util.GetXpath({"locate": "confirm_unlink_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK in popup confirm dialog", "result": "1"})

        OK(ret, int(arg['result']), "AdminUserPortalUserInfoPage.UnlinkUserPhone")

class AdminUserPortalUserInfoButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInfoTab(arg):
        '''
        ClickInfoTab : Click tab to switch info page
                Input argu :
                    type - account / address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click info tab
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click(({"method": "xpath", "locate": xpath, "message": "Click info tab ->" + type, "result": "1"}))

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoButton.ClickInfoTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditGeneralInformation(arg):
        '''
        ClickEditGeneralInformation : Click edit user info button
                Input argu :
                    edit_type - username / email
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        edit_type = arg["edit_type"]
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": edit_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoButton.ClickEditGeneralInformation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditDefaultAddress(arg):
        '''
        ClickEditDefaultAddress : Click edit button of default address in addresses tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoButton.ClickEditDefaultAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturn(arg):
        '''
        ClickReturn : Click return button to main title from user detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return button
        xpath = Util.GetXpath({"locate": "return_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click return button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoButton.ClickReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReveal(arg):
        '''
        ClickReveal : click reveal to see personal info
                Input arg :
                    type - email / phone / birth / gender
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click Reveal button to see private info
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reveal button ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserPortalUserInfoButton.ClickReveal')


class AdminUserInfoComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column (for example, phone_number)
                input_content - the content to input
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoComponent.InputToColumn')

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminUserInfoComponent.ClickOnButton')


class AdminMePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewFeature(arg):
        '''
        ClickAddNewFeature : Click add new feature
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new feature button
        xpath = Util.GetXpath({"locate": "new_feature_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new feature button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickAddNewFeature')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClosePopupWindow(arg):
        '''
        ClickClosePopupWindow : Click close popup window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click x to close popup window
        xpath = Util.GetXpath({"locate": "x_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click x to close popup window", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickClosePopupWindow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePageList(arg):
        '''
        ChangePageList : Change display list in one page
                Input argu :
                    option - 10 / page , 20 / page , 30 / page , 40 / page , 50 / page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click page list
        xpath = Util.GetXpath({"locate": "page_list"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click page list", "result":"1"})
        time.sleep(1)

        xpath = Util.GetXpath({"locate": "page"})
        xpath = xpath.replace("page_to_be_replaced", option)

        ##Choose list in one page
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose -> " + option + "to display", "result":"1"})

        OK(ret, int(arg['result']), 'AdminMePage.ChangePageList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteFeature(arg):
        '''
        ClickDeleteFeature : Click delete feature button
                Input argu :
                    feature_name - feature name
                    type - cancel/confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        feature_name = arg['feature_name']
        type = arg['type']

        ##Get delete button
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace("string_to_be_replaced", feature_name)

        ##Click delete feature button if feature exist
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete feature button", "result": "1"})

            ##Click cancel/confirm button
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + type + " button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickDeleteFeature')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetPCFeatureBasicAttributes(arg):
        '''
        SetPCFeatureBasicAttributes : Set feature basic attributes
                Input argu :
                    start_time - Feature start time
                    end_time - Feature end time
                    system_name - system name
                    display_name - display name
                    display_name_region - display name region
                    redirection_URL - redirection URL
                    show_feature - 1 for enable, 0 for disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        system_name = arg["system_name"]
        display_name = arg["display_name"]
        display_name_region = arg["display_name_region"]
        redirection_URL = arg["redirection_URL"]
        show_feature = arg["show_feature"]
        start_time = int(arg["start_time"])
        end_time = int(arg["end_time"])

        ##Set start time
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=start_time, is_tw_time = 1)
        xpath = Util.GetXpath({"locate": "start"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

        xpath = Util.GetXpath({"locate": "input_time"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Set end time
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=end_time, is_tw_time = 1)
        xpath = Util.GetXpath({"locate": "end"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

        xpath = Util.GetXpath({"locate": "input_time"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Input system name
        if system_name:
            xpath = Util.GetXpath({"locate": "system_name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": system_name, "message": "Input system name", "result": "1"})

        ##Input display name(en)
        if display_name:
            xpath = Util.GetXpath({"locate": "display_name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "message": "Input display name", "result": "1"})

        ##Input display name(region)
        if display_name_region:
            xpath = Util.GetXpath({"locate": "display_name_region"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name_region, "message": "Input display name(region)", "result": "1"})

        ##Input redirection url
        if redirection_URL:
            xpath = Util.GetXpath({"locate": "redirection_url"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirection_URL, "message": "Input redirection url", "result": "1"})

        ##Show feature to end user
        if int(show_feature):
            xpath = Util.GetXpath({"locate": "show_feature"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show feature check box", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetPCFeatureBasicAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetFeatureID(arg):
        '''
        GetFeatureID : Get feature ID in admin mepage
                Input argu :
                    feature_name - system name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        feature_name = arg["feature_name"]
        ret = 1

        ##Get feature ID in admin mepage
        xpath = Util.GetXpath({"locate":"feature_id"})
        xpath = xpath.replace("name_to_be_replaced", feature_name)
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "feature_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMePage.GetFeatureID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReorderGroupButton(arg):
        '''
        ClickReorderGroupButton : Click reorder group button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reorder group button
        xpath = Util.GetXpath({"locate": "reorder_group_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reorder group button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickReorderGroupButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReorderButton(arg):
        '''
        ClickReorderButton : Click reorder button admin pc page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reorder button admin pc page
        xpath = Util.GetXpath({"locate": "reorder_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reorder button admin pc page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickReorderButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReorderFeatureButton(arg):
        '''
        ClickReorderFeatureButton : Click reorder feature button
                Input argu : group
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        group = arg["group"]
        ret = 1

        ##Click reorder feature button
        xpath = Util.GetXpath({"locate": "group"}).replace('group_to_be_replace', group)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reorder feature button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickReorderFeatureButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeGroupOrder(arg):
        '''
        ChangeGroupOrder : Click move up button to move the group up / Click move down button to move the group down
                Input argu :
                            group - group to be moved
                            move_action - move up or move down
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        group = arg["group"]
        move_action = arg["move_action"]
        ret = 1

        ##Click move button to move the feature
        xpath = Util.GetXpath({"locate": move_action})
        xpath = xpath.replace("group_to_be_replaced", group)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click move button to move the feature", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ChangeGroupOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "cancel"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click OK button
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditFeature(arg):
        '''
        ClickEditFeature : Click edit feature button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit feature button
        ##GetFeatureID before using this function
        xpath = Util.GetXpath({"locate": "edit_btn"}).replace("id_to_be_replaced", GlobalAdapter.MePageE2EVar._FeatureID_)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit feature button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickEditFeature')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackMePage(arg):
        '''
        ClickBackMePage : Click back to mepage in add feature page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to mepage
        xpath = Util.GetXpath({"locate": "back_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to mepage", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickBackMePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArchived(arg):
        '''
        ClickArchived : Click Archived tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Archived tab
        xpath = Util.GetXpath({"locate": "archived_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Archived tab", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickArchived')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableSubPages(arg):
        '''
        ClickEnableSubPages : Click enable Sub Pages
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click enable Sub Pages
        xpath = Util.GetXpath({"locate": "subpage_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable Sub Pages", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickEnableSubPages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeSubPageNumber(arg):
        '''
        ClickChangeSubPageNumber : Click '+' to add sub page '-' to delete sub page
                Input argu :
                    action - add/delete
                    action_count - number of sub page to add
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        action = arg["action"]
        action_count = int(arg["action_count"])
        ret = 1

        if action == 'add':
            ##Click + to add sub page
            for page_count in range(action_count):
                xpath = Util.GetXpath({"locate": "plus_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to add sub page", "result": "1"})
                time.sleep(2)

        if action == 'delete':
            ##Click - to delete sub page
            for page_count in range(action_count):
                xpath = Util.GetXpath({"locate": "minus_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete sub page", "result": "1"})
                time.sleep(2)
                AdminMePage.ClickOK({"result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickChangeSubPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteSpecificSubPage(arg):
        '''
        ClickDeleteSpecificSubPage : Click trash can icon to delete sub page
                Input argu :
                    page_name - sub page to delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        page_name = arg["page_name"]
        ret = 1

        ##Click trash can icon to delete sub page
        xpath = Util.GetXpath({"locate": "delete_page"})
        xpath = xpath.replace("page_to_be_replaced", page_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash can icon to delete sub page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickDeleteSpecificSubPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextSubPageButton(arg):
        '''
        ClickNextSubPageButton : Click next SubPage button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next SubPage button
        xpath = Util.GetXpath({"locate": "next_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next SubPage button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickNextSubPageButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreviousSubPageButton(arg):
        '''
        ClickPreviousSubPageButton : Click previous SubPage button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click previous SubPage button
        xpath = Util.GetXpath({"locate": "pre_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click previous SubPage button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickPreviousSubPageButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetPCFeatureIconImage(arg):
        '''
        SetPCFeatureIconImage : Set feature icon image
                Input argu :
                    icon_image_file - icon image file
                    icon_image_type - icon image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_image_file = arg["icon_image_file"]
        icon_image_type = arg["icon_image_type"]

        ##Upload icon image
        xpath = Util.GetXpath({"locate":"image_upload_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click mass upload button", "result": "1"})
        time.sleep(3)
        XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": icon_image_file, "file_type": icon_image_type, "result": "1"})
        dumplogger.info("Success upload image.")

        OK(ret, int(arg["result"]), 'AdminMePage.SetPCFeatureIconImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetPCFeatureUserScope(arg):
        '''
        SetPCFeatureUserScope : Set feature user scope
                Input argu :
                    user_scope - All / Grayscale / Rule set
                    greyscale_start - start number
                    greyscale_end - end number
                    ruleset_number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_scope = arg["user_scope"]
        greyscale_start = arg["greyscale_start"]
        greyscale_end = arg["greyscale_end"]
        ruleset_number = arg["ruleset_number"]

        ##Click user scope dropdown select
        xpath = Util.GetXpath({"locate": "user_scope_select"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown select", "result": "1"})

        ##Click user scope dropdown item
        xpath = Util.GetXpath({"locate": "user_scope_dropdown_item"})
        xpath = xpath.replace("user_scope_name_to_be_replace", user_scope)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

        if user_scope == "Greyscale":

            ##input greyscale(start)
            xpath = Util.GetXpath({"locate": "greyscale_start"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": greyscale_start, "message": "input greyscale(start)", "result": "1"})

            ##input greyscale(end)
            xpath = Util.GetXpath({"locate": "greyscale_end"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": greyscale_end, "message": "input greyscale(end)", "result": "1"})

        if user_scope == "Rule set":
            xpath = Util.GetXpath({"locate": "ruleset_number"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

            ##input rule set name
            xpath = Util.GetXpath({"locate": "ruleset_dropdown_item"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ruleset_number, "message": "input greyscale(end)", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetPCFeatureUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetPCFeatureNewBadge(arg):
        '''
        SetPCFeatureNewBadge : Set feature new badge
                Input argu :
                    click_enable - 1 enable / 0 disable
                    start_time - Feature start time
                    end_time - Feature end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        click_enable = int(arg["click_enable"])
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        ret = 1

        if click_enable:
            ##Click enable new badge
            xpath = Util.GetXpath({"locate": "enable_badge"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable new badge", "result": "1"})

        ##input new badge time
        if start_time:
            start_time = int(start_time)
            ##Set start time
            ##Click to show calendar
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=start_time, is_tw_time = 1)
            xpath = Util.GetXpath({"locate": "start"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            ##input start time
            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

            ##Click ok button to close the calendar
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        if end_time:
            ##Set end time
            ##Click to show calendar
            end_time = int(end_time)
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=end_time, is_tw_time = 1)
            xpath = Util.GetXpath({"locate": "end"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            ##input end time
            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

            ##Click ok button to close the calendar
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetPCFeatureNewBadge')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetSubPageBasicAttributes(arg):
        '''
        SetSubPageBasicAttributes : set Sub Page basic attributes
                Input argu :
                    sub_page - sub page tab
                    start_time - Feature start time
                    end_time - Feature end time
                    system_name - system name
                    display_name - display name
                    display_name_region - display name region
                    redirection_URL - redirection URL
                    show_feature - 1 for enable, 0 for disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        sub_page = arg["sub_page"]
        page_num = int(arg["sub_page"])-1
        system_name = arg["system_name"]
        display_name = arg["display_name"]
        display_name_region = arg["display_name_region"]
        redirection_URL = arg["redirection_URL"]
        show_feature = arg["show_feature"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        ret = 1

        ##click to Sub Page tab
        xpath = Util.GetXpath({"locate": "sub_page"}).replace("page_to_be_replace", sub_page)
        xpath = xpath.replace("[number_to_be_replaced]", sub_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Sub Page tab", "result": "1"})

        ##Set start time
        if start_time:
            start_time = int(start_time)
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=start_time, is_tw_time = 1)
            xpath = Util.GetXpath({"locate": "start"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Set end time
        if end_time:
            end_time = int(end_time)
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=end_time, is_tw_time = 1)
            xpath = Util.GetXpath({"locate": "end"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Input system name
        if system_name:
            xpath = Util.GetXpath({"locate": "system_name"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": system_name, "message": "Input system name", "result": "1"})

        ##Input display name(en)
        if display_name:
            xpath = Util.GetXpath({"locate": "display_name"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "message": "Input display name", "result": "1"})

        ##Input display name(region)
        if display_name_region:
            xpath = Util.GetXpath({"locate": "display_name_region"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name_region, "message": "Input display name(region)", "result": "1"})

        ##Input redirection url
        if redirection_URL:
            xpath = Util.GetXpath({"locate": "redirection_url"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirection_URL, "message": "Input redirection url", "result": "1"})

        ##Show feature to end user
        if int(show_feature):
            xpath = Util.GetXpath({"locate": "show_feature"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(page_num))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show feature check box", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetSubPageBasicAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetSubPageUserScope(arg):
        '''
        SetSubPageUserScope : Set feature user scope
                Input argu :
                    subpage_number
                    user_scope - All / Grayscale / Rule set
                    greyscale_start - start number
                    greyscale_end - end number
                    ruleset_number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subpage_number = arg['subpage_number']
        subpage_number2 = int(arg['subpage_number'])-1
        user_scope = arg["user_scope"]
        greyscale_start = arg["greyscale_start"]
        greyscale_end = arg["greyscale_end"]
        ruleset_number = arg["ruleset_number"]

        ##Click user scope dropdown select
        xpath = Util.GetXpath({"locate": "user_scope_select"})
        xpath = xpath.replace("PAGE_to_be_replaced", str(subpage_number2))
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown select", "result": "1"})

        ##Click user scope dropdown item
        xpath = Util.GetXpath({"locate": "user_scope_dropdown_item"})
        xpath = xpath.replace("user_scope_name_to_be_replaced", user_scope)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

        if user_scope == "Greyscale":

            ##input greyscale(start)
            xpath = Util.GetXpath({"locate": "greyscale_start"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(subpage_number2))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": greyscale_start, "message": "input greyscale(start)", "result": "1"})

            ##input greyscale(end)
            xpath = Util.GetXpath({"locate": "greyscale_end"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(subpage_number2))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": greyscale_end, "message": "input greyscale(end)", "result": "1"})

        if user_scope == "Rule set":
            xpath = Util.GetXpath({"locate": "ruleset_number"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(subpage_number2))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

            ##input rule set name
            xpath = Util.GetXpath({"locate": "ruleset_dropdown_item"})
            xpath = xpath.replace("PAGE_to_be_replaced", str(subpage_number2))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ruleset_number, "message": "input greyscale(end)", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetSubPageUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPFeatureBasicAttributes(arg):
        '''
        SetAPPFeatureBasicAttributes : Set feature basic attributes
                Input argu :
                    start_time - feature start time
                    end_time - feature end time
                    no_end_time - 1 for enable, 0 for disable
                    group_name - group name
                    system_name - system name
                    display_name - display name
                    display_name_region - display name in local language
                    subtitle_text - subtitle text
                    subtitle_text_regoin - subtitle text in local language
                    show_feature - 1 for enable, 0 for disable
                    allow_access - 1 for enable, 0 for disable
                    redirection_url - main redirection URL
                    entrance_bundle - entrance_bundle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        no_end_time = arg["no_end_time"]
        group_name = arg["group_name"]
        system_name = arg["system_name"]
        display_name = arg["display_name"]
        display_name_region = arg["display_name_region"]
        subtitle_text = arg["subtitle_text"]
        subtitle_text_regoin = arg["subtitle_text_regoin"]
        show_feature = arg["show_feature"]
        allow_access = arg["allow_access"]
        redirection_url = arg["redirection_url"]
        entrance_bundle = arg["entrance_bundle"]

        ##Set start time
        if start_time:
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time), is_tw_time=1)
            xpath = Util.GetXpath({"locate": "start"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Set end time
        if end_time:
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time), is_tw_time=1)
            xpath = Util.GetXpath({"locate": "end"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Click no end time toggle
        if no_end_time:
            xpath = Util.GetXpath({"locate": "no_end_time"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click no end time toggle", "result": "1"})

        ##Choose group name
        if group_name:
            xpath = Util.GetXpath({"locate": "group_name_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group name dropdown list", "result": "1"})

            xpath = Util.GetXpath({"locate": "group_name"})
            xpath = xpath.replace("string_to_be_replaced", group_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group name", "result": "1"})

        ##Input system name
        if system_name:
            xpath = Util.GetXpath({"locate": "system_name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": system_name, "message": "Input system name", "result": "1"})

        ##Input display name(english)
        if display_name:
            xpath = Util.GetXpath({"locate": "display_name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "message": "Input display name", "result": "1"})

        ##Input display name(region)
        if display_name_region:
            xpath = Util.GetXpath({"locate": "display_name_region"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name_region, "message": "Input display name(region)", "result": "1"})

        ##Input subtitle text(english)
        if subtitle_text:
            xpath = Util.GetXpath({"locate": "subtitle_text"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": subtitle_text, "message": "Input subtitle text", "result": "1"})

        ##Input subtitle text(region)
        if subtitle_text_regoin:
            xpath = Util.GetXpath({"locate": "subtitle_text_regoin"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": subtitle_text_regoin, "message": "Input subtitle text(region)", "result": "1"})

        ##Show feature to end user
        if show_feature:
            xpath = Util.GetXpath({"locate": "show_feature"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click show feature to end user toggle", "result": "1"})

        ##Allow access to unauthenticated users
        if allow_access:
            xpath = Util.GetXpath({"locate": "allow_access"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click allow access to unauthenticated users toggle", "result": "1"})

        ##Input redirection url
        if redirection_url:
            xpath = Util.GetXpath({"locate": "redirection_url"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirection_url, "message": "Input redirection url", "result": "1"})

        ##Choose entrance bundle
        if entrance_bundle:
            xpath = Util.GetXpath({"locate": "entrance_bundle_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click entrance bundle dropdown list", "result": "1"})

            xpath = Util.GetXpath({"locate": "entrance_bundle"})
            xpath = xpath.replace("string_to_be_replaced", entrance_bundle)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click entrance bundle", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPFeatureBasicAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPFeatureMinimumVersion(arg):
        '''
        SetAPPFeatureMinimumVersion : Set feature minimum version requirements
                Input argu :
                    android_version - minimum android version requirements
                    ios_version - minimum ios version requirements
                    rn_version - minimum rn version requirements
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        android_version = arg["android_version"]
        ios_version = arg["ios_version"]
        rn_version = arg["rn_version"]

        ##Input minimum android version
        if android_version:
            xpath = Util.GetXpath({"locate": "android_version"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": android_version, "message": "Input minimum android version", "result": "1"})

        ##Input minimum ios version
        if ios_version:
            xpath = Util.GetXpath({"locate": "ios_version"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ios_version, "message": "Input minimum ios version", "result": "1"})

        ##Input minimum rn version
        if rn_version:
            xpath = Util.GetXpath({"locate": "rn_version"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rn_version, "message": "Input minimum rn version", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPFeatureMinimumVersion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPFeatureIconImage(arg):
        '''
        SetAPPFeatureIconImage : Set feature icon image
                Input argu :
                    upload_type - upload image by file or image hash
                    icon_image_file - icon image file
                    icon_image_type - icon image type
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        icon_image_file = arg["icon_image_file"]
        icon_image_type = arg["icon_image_type"]
        image_hash = arg["image_hash"]

        ##Upload icon image
        if upload_type == "upload_file":
            xpath = Util.GetXpath({"locate":"image_upload_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})
            time.sleep(3)
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": icon_image_file, "file_type": icon_image_type, "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":
            xpath = Util.GetXpath({"locate": "input_hash"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
            dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPFeatureIconImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteIconImage(arg):
        '''
        ClickDeleteIconImage : Click trash icon to delete icon image
                Input argu :
                    icon_type - feature_icon / circle_icon
                    circle_tab - sub circle tab
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]
        circle_tab = arg["circle_tab"]

        if icon_type == "feature_icon":

            ##Click trash icon to delete icon image
            xpath = Util.GetXpath({"locate": "feature_trash_icon"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash icon to delete feature icon image", "result": "1"})

        elif icon_type == "circle_icon":
            page_num = int(circle_tab)-1

            ##Click trash icon to delete icon image
            xpath = Util.GetXpath({"locate": "circle_trash_icon"})
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash icon to delete circle icon image", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickDeleteIconImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPFeatureUserScope(arg):
        '''
        SetAPPFeatureUserScope : Set feature user scope
                Input argu :
                    user_scope - All / Grayscale / Rule set
                    greyscale_start - start number
                    greyscale_end - end number
                    ruleset_number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_scope = arg["user_scope"]
        greyscale_start = arg["greyscale_start"]
        greyscale_end = arg["greyscale_end"]
        ruleset_number = arg["ruleset_number"]

        ##Click user scope dropdown select
        xpath = Util.GetXpath({"locate": "user_scope_select"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown select", "result": "1"})

        ##Click user scope dropdown item
        xpath = Util.GetXpath({"locate": "user_scope_dropdown_item"})
        xpath = xpath.replace("user_scope_name_to_be_replace", user_scope)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

        if user_scope == "Greyscale":

            ##input greyscale(start)
            xpath = Util.GetXpath({"locate": "greyscale_start"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": greyscale_start, "message": "input greyscale(start)", "result": "1"})

            ##input greyscale(end)
            xpath = Util.GetXpath({"locate": "greyscale_end"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": greyscale_end, "message": "input greyscale(end)", "result": "1"})

        if user_scope == "Rule set":
            xpath = Util.GetXpath({"locate": "ruleset_number"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope dropdown item", "result": "1"})

            ##input rule set name
            xpath = Util.GetXpath({"locate": "ruleset_dropdown_item"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": ruleset_number, "message": "input rule set name", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPFeatureUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPFeatureNewBadge(arg):
        '''
        SetAPPFeatureNewBadge : Set feature new badge
                Input argu :
                    click_enable - 1 enable / 0 disable
                    end_time - Feature end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_enable = int(arg["click_enable"])
        end_time = arg["end_time"]

        if click_enable:
            ##Click enable new badge
            xpath = Util.GetXpath({"locate": "enable_badge"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable new badge", "result": "1"})

        ##Set badge end time
        if end_time:
            datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0, 1)
            xpath = Util.GetXpath({"locate": "badge_end_time"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click badge end time", "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": datetime, "result": "1"})
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        else:
            xpath = Util.GetXpath({"locate": "badge_end_time"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click badge end time", "result": "1"})
            xpath = Util.GetXpath({"locate": "badge_end_time_now"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPFeatureNewBadge')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPCircleAttributes(arg):
        '''
        SetAPPCircleAttributes : set circle attributes
                Input argu :
                    circle_slot - circle slot number
                    circle_tab - sub circle tab
                    start_time - circle start time
                    system_name - system name
                    display_name - display name
                    display_name_region - display name in local language
                    subtitle_text - subtitle text
                    subtitle_text_regoin - subtitle text in local language
                    redirection_url - redirection URL
                    allow_access - 1 for enable, 0 for disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_slot = arg["circle_slot"]
        slot_num = int(arg["circle_slot"]) - 1
        circle_tab = arg["circle_tab"]
        page_num = int(arg["circle_tab"]) - 1
        system_name = arg["system_name"]
        display_name = arg["display_name"]
        display_name_region = arg["display_name_region"]
        subtitle_text = arg["subtitle_text"]
        subtitle_text_regoin = arg["subtitle_text_regoin"]
        redirection_url = arg["redirection_url"]
        allow_access = arg["allow_access"]
        start_time = arg["start_time"]

        ##click to circle tab
        xpath = Util.GetXpath({"locate": "circle_tab"})
        xpath = xpath.replace("slot_to_be_replaced", circle_slot)
        xpath = xpath.replace("page_to_be_replaced", circle_tab)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Sub circle tab", "result": "1"})

        ##Set start time when circle tab is not circle 1
        if start_time and circle_tab > 1:
            start_time = int(start_time)
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=start_time, is_tw_time=1)
            xpath = Util.GetXpath({"locate": "start"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to see calendar", "result": "1"})

            xpath = Util.GetXpath({"locate": "input_time"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok in calendar", "result": "1"})

        ##Input system name
        if system_name:
            xpath = Util.GetXpath({"locate": "system_name"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": system_name, "message": "Input system name", "result": "1"})

        ##Input display name(en)
        if display_name:
            xpath = Util.GetXpath({"locate": "display_name"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name, "message": "Input display name", "result": "1"})

        ##Input display name(region)
        if display_name_region:
            xpath = Util.GetXpath({"locate": "display_name_region"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_name_region, "message": "Input display name(region)", "result": "1"})

        ##Input subtitle text(en)
        if subtitle_text:
            xpath = Util.GetXpath({"locate": "subtitle_text"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": subtitle_text, "message": "Input subtitle text", "result": "1"})

        ##Input subtitle text(region)
        if subtitle_text_regoin:
            xpath = Util.GetXpath({"locate": "subtitle_text_regoin"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": subtitle_text_regoin, "message": "Input subtitle text(region)", "result": "1"})

        ##Input redirection url
        if redirection_url:
            xpath = Util.GetXpath({"locate": "redirection_url"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirection_url, "message": "Input redirection url", "result": "1"})

        ##Allow access to unauthenticated users
        if allow_access:
            xpath = Util.GetXpath({"locate": "allow_access"})
            xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
            xpath = xpath.replace("page_to_be_replaced", str(page_num))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Allow access to unauthenticated users toggle", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPCircleAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAPPCircleIcon(arg):
        '''
        SetAPPCircleIcon : Set circle icon
                Input argu :
                    circle_slot - circle slot number
                    circle_tab - sub circle tab
                    upload_type - upload image by file or image hash
                    icon_image_file - icon image file
                    icon_image_type - icon image type
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_slot = arg["circle_slot"]
        circle_tab = arg["circle_tab"]
        upload_type = arg["upload_type"]
        icon_image_file = arg["icon_image_file"]
        icon_image_type = arg["icon_image_type"]
        image_hash = arg["image_hash"]

        ##Upload icon image
        if upload_type == "upload_file":
            xpath = Util.GetXpath({"locate":"image_upload_button"})
            xpath = xpath.replace("slot_to_be_replaced", circle_slot)
            xpath = xpath.replace("page_to_be_replaced", circle_tab)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload button", "result": "1"})
            time.sleep(3)
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": icon_image_file, "file_type": icon_image_type, "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":
            xpath = Util.GetXpath({"locate": "input_hash"})
            xpath = xpath.replace("slot_to_be_replaced", circle_slot)
            xpath = xpath.replace("page_to_be_replaced", circle_tab)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
            dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminMePage.SetAPPCircleIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableCircles(arg):
        '''
        ClickEnableCircles : Click enable circles toggle
                Input argu :
                    circle_number - number of circle slots
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_number = arg["circle_number"]

        ##Click enable Sub circle
        xpath = Util.GetXpath({"locate": "enable_circle_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable circles toggle", "result": "1"})

        ##Choose number of circles
        xpath = Util.GetXpath({"locate": "number_of_circle_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click number of circle dropdown list", "result": "1"})

        xpath = Util.GetXpath({"locate": "circle_number"})
        xpath = xpath.replace("number_to_be_replaced", str(circle_number))
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click number of circle", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickEnableCircles')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCircles(arg):
        '''
        ClickAddCircles : Click add circles
                Input argu :
                    circle_slot - circle slot number
                    circle_tab - number of circles to add
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_slot = arg["circle_slot"]
        slot_num = int(arg["circle_slot"]) - 1
        circle_tab = arg["circle_tab"]

        ##Locate add circle button
        xpath = Util.GetXpath({"locate": "add_circle_btn"})
        xpath = xpath.replace("slot_to_be_replaced", str(slot_num))

        ##Click add circle button
        for circles in range(int(circle_tab)):
            time.sleep(1)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add circle button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminMePage.ClickAddCircles')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteCircles(arg):
        '''
        ClickDeleteCircles : Click delete circles
                Input argu :
                    circle_slot - circle slot number
                    circle_tab - delete which circle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        circle_slot = arg["circle_slot"]
        slot_num = int(arg["circle_slot"]) - 1
        circle_tab = arg["circle_tab"]
        page_num = int(arg["circle_tab"]) - 1

        ##Click delete circle button
        xpath = Util.GetXpath({"locate": "delete_circle_btn"})
        xpath = xpath.replace("slot_to_be_replaced", str(slot_num))
        xpath = xpath.replace("page_to_be_replaced", str(page_num))
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete circle button", "result": "1"})
        time.sleep(1)

        OK(ret, int(arg["result"]), 'AdminMePage.ClickDeleteCircles')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageNumber(arg):
        '''
        ClickPageNumber : Click page number
                Input argu :
                    page_number - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]

        ##Click page number
        if page_number:
            xpath = Util.GetXpath({"locate": "number_of_page"})
            xpath = xpath.replace("number_to_be_replaced", page_number)

        ##Click last page
        else:
            xpath = Util.GetXpath({"locate": "last_page"})

        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMePage.ClickPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click go to next page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to next page button
        xpath = Util.GetXpath({"locate": "next_page_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to next page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMePage.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrevPage(arg):
        '''
        ClickPrevPage : Click go to previous page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to previous page button
        xpath = Util.GetXpath({"locate": "previous_page_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to previous page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMePage.ClickPrevPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectNumberPerPage(arg):
        '''
        SelectNumberPerPage : Select number per page
                Input argu :
                    number_per_page - 10 / 20 / 30 / 40 / 50
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number_per_page = arg["number_per_page"]

        ##Click number per page drop-down list
        xpath = Util.GetXpath({"locate": "dropdown_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click number per page drop-down list", "result": "1"})
        time.sleep(1)

        ##Click number
        xpath = Util.GetXpath({"locate": "number_per_page"})
        xpath = xpath.replace("page_to_be_replaced", number_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminMePage.SelectNumberPerPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPageNumber(arg):
        '''
        GoToPageNumber : Go to page number
                Input argu :
                    page_number - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]

        ##Input page number
        xpath = Util.GetXpath({"locate": "input_bar"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_number, "result": "1"})
        time.sleep(1)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminMePage.GoToPageNumber')
