#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidShopPageMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidGoToShopPage(arg):
    '''
    AndroidGoToShopPage : Search shop and redirect to shop page
            Input argu :
                shop_name - shop name
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    shop_name = arg["shop_name"]

    ##Check floating banner
    floating_xpath = Util.GetXpath({"locate":"floating_banner_xpath"})

    ##Get search bar's xpath in Top
    if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":floating_xpath, "passok": "0", "result": "1"}):
        xpath = Util.GetXpath({"locate":"search_bar_with_floating"})
    else:
        xpath = Util.GetXpath({"locate":"global_search_bar"})

    ##Click search bar
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click search bar", "result": "1"})
    time.sleep(3)

    ##Input shop name and send enter
    AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":Util.GetXpath({"locate":"typing_search_words"}), "string":shop_name, "result": "1"})
    AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})
    time.sleep(10)

    ##Redirect to shop page
    xpath = Util.GetXpath({"locate":"shop"})
    xpath = xpath.replace("shop_to_be_replace", shop_name)
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"go to shop page => " + shop_name, "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGoToShopPage')

class AndroidShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickQuestionMark(arg):
        '''
        ClickQuestionMark : Click question mark
                Input argu :
                    click_button - ok / read_more
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        click_button = arg["click_button"]

        ##Click question mark
        xpath = Util.GetXpath({"locate":"question_mark"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click question mark", "result": "1"})

        ##Click ok or read more button
        xpath = Util.GetXpath({"locate": click_button + "_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click " + click_button, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPage.ClickQuestionMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductSortTab(arg):
        '''
        SelectProductSortTab : Select product sort tab
                Input argu :
                    sort_tab - popular / latest / top_sales / price_from_high_to_low / price_from_low_to_high
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sort_tab = arg["sort_tab"]

        if sort_tab == "popular":
            ##Click popular tab
            xpath = Util.GetXpath({"locate":"popular_tab"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click popular tab", "result": "1"})

        elif sort_tab == "latest":
            ##Click latest tab
            xpath = Util.GetXpath({"locate":"latest_tab"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click latest tab", "result": "1"})

        elif sort_tab == "top_sales":
            ##Click top sales tab
            xpath = Util.GetXpath({"locate":"top_sales_tab"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click top sales tab", "result": "1"})

        elif sort_tab == "price_from_high_to_low":
            ##Click price from high to low tab
            xpath = Util.GetXpath({"locate":"price_from_high_to_low_tab"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click price from high to low tab", "result": "1"})

        elif sort_tab == "price_from_low_to_high":
            ##Click price from low to high tab
            xpath = Util.GetXpath({"locate":"price_from_low_to_high_tab"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click price from low to high tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPage.SelectProductSortTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBlockConfirmationOptions(arg):
        '''
        ClickBlockConfirmationOptions : Click block confirmation options
                Input argu :
                    options - cancel / block / unblock
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        options = arg["options"]

        ##Click cancel / block / unblock button
        xpath = Util.GetXpath({"locate": options + "_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click " + options + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPage.ClickBlockConfirmationOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShopName(arg):
        '''
        CheckShopName : Check if shop name shows up in shop page
                Input argu :
                    name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Check shop name
        xpath = Util.GetXpath({"locate":"shop_name"})
        xpath = xpath.replace("name_to_be_replaced", name)

        ##If shop name title exists, means enter shop successfully
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Enter shop successfully.")
        else:
            ret = 0
            dumplogger.info("Fail to enter shop page.")

        OK(ret, int(arg['result']), 'AndroidShopPage.CheckShopName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckFollowVoucher(arg):
        '''
        CheckFollowVoucher : Check if follow voucher shows up in shop page
                Input argu :
                    shop_url - shop url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        shop_url = arg["shop_url"]

        for retry_times in range(3):
            ##Check follow voucher
            xpath = Util.GetXpath({"locate":"follow_voucher"})

            if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ret = 1
                dumplogger.info("Follow voucher display successfully.")
                break
            else:
                ##If follow voucher not exists, go to shop page again
                dumplogger.info("Follow voucher not display successfully.")

                ##Use adb command send url to home page
                home_page_url = "https://shopee." + Config._TestCaseRegion_.lower() + " " + Config._DesiredCaps_['appPackage']
                AndroidBaseUICore.SendADBCommand({"command_type":"IntentUrl", "adb_arg":home_page_url, "is_return": "0", "result": "1"})
                time.sleep(15)

                ##Use adb command to launch the seller page
                AndroidBaseUICore.SendADBCommand({"command_type":"IntentUrl", "adb_arg":shop_url + ' ' + Config._DesiredCaps_['appPackage'] + '"', "is_return": "0", "result": "1"})
                time.sleep(15)

        OK(ret, int(arg['result']), 'AndroidShopPage.CheckFollowVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreferredLabel(arg):
        '''
        ClickPreferredLabel : Click preferred label
                Input argu :
                    button_type - ok / read_more
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click preferred label
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image": "preferred_label", "threshold": "0.9", "is_click": "yes", "result": "1"})

        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPage.ClickPreferredLabel')

class AndroidShopPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click on filter button
                Input argu :N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on filter button
        xpath = Util.GetXpath({"locate":"filter_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTopRightFunction(arg):
        '''
        ClickTopRightFunction : Click on function button which is on the top right side of page
                Input argu :
                    shop_name - function button in which shop page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Click on function button
        xpath = Util.GetXpath({"locate":"function_button"})
        xpath = xpath.replace("shop_name_to_be_replace", shop_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click function button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickTopRightFunction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollapseTopRightFunction(arg):
        '''
        ClickCollapseTopRightFunction : Click on any where that out of the top right function to collapse top right function
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click out of the top right function
        xpath = Util.GetXpath({"locate":"collapse_function"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click out of the top right function", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickCollapseTopRightFunction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollow(arg):
        '''
        ClickFollow : Click follow button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click follow button
        xpath = Util.GetXpath({"locate":"follow_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click follow button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFollow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnFollow(arg):
        '''
        ClickUnFollow : Click unfollow button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click unfollow button
        xpath = Util.GetXpath({"locate":"unfollow_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click unfollow button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickUnFollow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChat(arg):
        '''
        ClickChat : Click chat button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click chat button
        xpath = Util.GetXpath({"locate":"chat_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click chat button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickChat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopName(arg):
        '''
        ClickShopName : Click shop name button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shop name button
        xpath = Util.GetXpath({"locate":"drop_down_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click shop name button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickShopName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerVoucherClaim(arg):
        '''
        ClickSellerVoucherClaim : Click seller voucher claim button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click seller voucher claim button
        xpath = Util.GetXpath({"locate":"claim_voucher"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click seller voucher claim button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickSellerVoucherClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollowAndClaimVoucher(arg):
        '''
        ClickFollowAndClaimVoucher : Click follow and claim voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click follow and claim voucher
        xpath = Util.GetXpath({"locate":"follow_and_claim_voucher"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click follow and claim voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFollowAndClaimVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClosePopUpVoucher(arg):
        '''
        ClickClosePopUpVoucher : Click close pop up voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close pop up voucher
        xpath = Util.GetXpath({"locate":"popup_voucher"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click close pop up voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickClosePopUpVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUseVoucher(arg):
        '''
        ClickUseVoucher : Click use voucher
                Input argu :
                    voucher_type - follow_voucher / seller_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]

        ##Click use voucher
        xpath = Util.GetXpath({"locate":voucher_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click use voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickUseVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllProductsTab(arg):
        '''
        ClickAllProductsTab : Click all products tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click all products tab
        xpath = Util.GetXpath({"locate":"all_products_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click all products tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickAllProductsTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryTab(arg):
        '''
        ClickCategoryTab : Click category tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click category tab
        xpath = Util.GetXpath({"locate":"category_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click category tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickCategoryTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRedirectToCategory(arg):
        '''
        ClickRedirectToCategory : Redirect to category page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Redirect to category page
        xpath = Util.GetXpath({"locate":"redirect_category"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"redirect to category page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickRedirectToCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllProductsCategory(arg):
        '''
        ClickAllProductsCategory : Click all products category
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click all products category
        xpath = Util.GetXpath({"locate":"all_products_category"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click all products category", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickAllProductsCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToTopButton(arg):
        '''
        ClickGoToTopButton : Click go to top button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to top button by OpenCV
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode":Config._TestCasePlatform_.lower(), "image":"top_button", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickGoToTopButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeAll(arg):
        '''
        ClickSeeAll : Click see all button
                Input argu :
                    section - category / top_product / hot_deals / flash_sale
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Click see all button
        xpath = Util.GetXpath({"locate": section})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click see all button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickSeeAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleRemindMe(arg):
        '''
        ClickFlashSaleRemindMe : Click flash sale remind me btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click flash sale remind me button
        xpath = Util.GetXpath({"locate":"remind_me_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click remind me button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFlashSaleRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleCancelRemind(arg):
        '''
        ClickFlashSaleCancelRemind : Click flash sale cancel remind btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click flash sale cancel remind button
        xpath = Util.GetXpath({"locate":"cancel_remind_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click cancel remind button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFlashSaleCancelRemind')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleTab(arg):
        '''
        ClickFlashSaleTab : Click flash sale tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Swipe left to click flash sale tab
        xpath = Util.GetXpath({"locate":"tab_horiz_scroll"})
        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":"5", "result": "1"})

        ##Click flash sale tab
        xpath = Util.GetXpath({"locate":"flash_sale_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click flash sale tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFlashSaleTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCampaignTab(arg):
        '''
        ClickCampaignTab : Click campaign tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Swipe left to click campaign tab
        xpath = Util.GetXpath({"locate":"tab_horiz_scroll"})
        AndroidBaseUILogic.AndroidDirectMove({"locate":xpath, "direction":"right", "times":"5", "result": "1"})

        ##Click campaign tab
        xpath = Util.GetXpath({"locate":"campaign_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click campaign tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickCampaignTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Click flash sale buy now btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click flash sale buy now btn
        xpath = Util.GetXpath({"locate":"buy_now_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click flash sale buy now btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseFlashSaleMessage(arg):
        '''
        CloseFlashSaleMessage : Close flash sale message
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Close flash sale message
        xpath = Util.GetXpath({"locate":"popup_window_ok"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"close flash sale message", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.CloseFlashSaleMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleProductCard(arg):
        '''
        ClickFlashSaleProductCard : Click flash sale product card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click flash sale product card
        xpath = Util.GetXpath({"locate":"flash_sale_product_card"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click flash sale product card", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickFlashSaleProductCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShare(arg):
        '''
        ClickShare : Click share button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button
        xpath = Util.GetXpath({"locate":"share"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click share button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickShare')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToBuy(arg):
        '''
        ClickBackToBuy : Click back to buy button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to buy button
        xpath = Util.GetXpath({"locate":"back_to_buy"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click back to buy button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickBackToBuy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReportThisUser(arg):
        '''
        ClickReportThisUser : Click report this user button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click report this user button
        xpath = Util.GetXpath({"locate":"report_this_user"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click report this user button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickReportThisUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNeedHelp(arg):
        '''
        ClickNeedHelp : Click need help button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click need help button
        xpath = Util.GetXpath({"locate":"need_help"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click need help button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickNeedHelp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBlockThisUser(arg):
        '''
        ClickBlockThisUser : Click block this user button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click block this user button
        xpath = Util.GetXpath({"locate":"block_this_user"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click block this user button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickBlockThisUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnBlockThisUser(arg):
        '''
        ClickUnBlockThisUser : Click unblock this user button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click unblock this user
        xpath = Util.GetXpath({"locate":"unblock_this_user"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click unblock this user button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickUnBlockThisUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBanner(arg):
        '''
        ClickBanner : Click banner
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click banner
        xpath = Util.GetXpath({"locate":"banner"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click banner", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewAllProducts(arg):
        '''
        ClickViewAllProducts : Click view all products btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view all products
        xpath = Util.GetXpath({"locate":"view_all_products"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click view all products btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickViewAllProducts')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopTab(arg):
        '''
        ClickShopTab : Click shop tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click shop tab
        xpath = Util.GetXpath({"locate":"shop_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click shop tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickShopTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewTab(arg):
        '''
        ClickNewTab : Click new tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click new tab
        xpath = Util.GetXpath({"locate":"new_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click new tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickNewTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        '''
        ClickCategory : Click category
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg['category_name']

        ##Click category
        xpath = Util.GetXpath({"locate":"category_name"})
        xpath = xpath.replace("name_to_be_replaced", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click category", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageButton.ClickCategory')


class AndroidShopPageFilterButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOptions(arg):
        '''
        ClickOptions : Click on any options that on filter panel
                Input argu :
                    options - click options
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        options = arg["options"]

        ##Click options
        xpath = Util.GetXpath({"locate":"filter_options"})
        xpath = xpath.replace('replaced_text', options)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click options => " + options, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageFilterButton.ClickOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click on reset button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on reset button
        xpath = Util.GetXpath({"locate":"reset_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageFilterButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApply(arg):
        '''
        ClickApply : Click on apply button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on apply button
        xpath = Util.GetXpath({"locate":"apply_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click apply button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageFilterButton.ClickApply')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOutOfFilterPanel(arg):
        '''
        ClickOutOfFilterPanel : Click on any where that out of the search filter panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click out of the search filter panel
        xpath = Util.GetXpath({"locate":"out_of_filter"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click out of the search filter panel", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidShopPageFilterButton.ClickOutOfFilterPanel')
