import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCListing Precondition =="
        dumplogger.info("== Setup ESPCListing Precondition ==")

    def test_ESPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCListing-01.xml")

    def test_ESPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCListing-02.xml")

    def test_ESPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCListing Post condition =="
        dumplogger.info("== Setup ESPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
