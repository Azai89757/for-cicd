import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCHomePage Precondition =="
        dumplogger.info("== Setup THPCHomePage Precondition ==")

    def test_THPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCHomePage-01.xml")

    def test_THPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCHomePage-02.xml")

    '''def test_THPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCHomePage Post condition =="
        dumplogger.info("== Setup THPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
