import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCLogisticPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCLogisticPromotion Precondition =="
        dumplogger.info("== Setup SGPCLogisticPromotion Precondition ==")

    def test_SGPCLogisticPromotion01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCLogisticPromotion-01.xml")

    def test_SGPCLogisticPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCLogisticPromotion-02.xml")

    def test_SGPCLogisticPromotion03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCLogisticPromotion-03.xml")

    def test_SGPCLogisticPromotion04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCLogisticPromotion-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCLogisticPromotion Post condition =="
        dumplogger.info("== Setup SGPCLogisticPromotion Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
