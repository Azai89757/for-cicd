#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminNewPromotionMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time


##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

##Import Admin library
from PageFactory.Admin import AdminCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def LaunchPromotionAdmin(arg):
    ''' LaunchPromotionAdmin : go to promotion admin page
            Input argu :
                N/A
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    ret = 1
    promotion_admin_cookie = {}
    db_file = "admin_cookie_promotion"

    ##Get current env and country
    GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
    GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

    if 'promotion_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        promotion_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        promotion_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        promotion_admin_cookie['domain'] = 'soup.staging.shopee.io'
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['promotion_admin'] = [promotion_admin_cookie]

    ##Go to url
    url = "https://admin.promotion." + GlobalAdapter.UrlVar._Domain_
    BaseUICore.GotoURL({"url":url, "result": "1"})
    dumplogger.info("url = %s" % (url))
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"promotion_admin", "result": "1"})
    time.sleep(5)

    ##Go to Promotion Admin
    BaseUICore.GotoURL({"url":url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchPromotionAdmin')

def GoToPurchaseWithPurchaseDetailPage(arg):
    '''
        GoToPurchaseWithPurchaseDetailPage : Go to PWP detail page
            Input argu :
                pwp_id - purchase with purchase id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    pwp_id = arg["pwp_id"]

    ##Go to PWP detail page
    url = "https://admin.promotion." + GlobalAdapter.UrlVar._Domain_ + "/aod-edit-deal?id=" + pwp_id + "&subType=2&source=0&redirectPath=%2Flocal-aod-deals"
    BaseUICore.GotoURL({"url":url, "result": "1"})

    ##Wait for the page loading
    time.sleep(5)

    OK(ret, int(arg['result']), 'GoToPurchaseWithPurchaseDetailPage')

def GoToAddOnDealDetailPage(arg):
    '''
        GoToAddOnDealDetailPage : Go to AOD detail page
            Input argu :
                aod_id - add on deal id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    aod_id = arg["aod_id"]

    ##Go to AOD detail page
    url = "https://admin.promotion." + GlobalAdapter.UrlVar._Domain_ + "/aod-edit-deal?id=" + aod_id + "&subType=0&source=0&redirectPath=%2Flocal-aod-deals"
    BaseUICore.GotoURL({"url":url, "result": "1"})

    ##wait for the page loading
    time.sleep(5)

    OK(ret, int(arg['result']), 'GoToAddOnDealDetailPage')

class AdminNewPromotionComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSubTab(arg):
        '''
        ClickOnSubTab : Click any tab on left panel in promotion admin page
                Input argu :
                    subtab_type - type of subtab which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab_type = arg["subtab_type"]

        ##Check pop up window in promotion admin
        xpath = Util.GetXpath({"locate": "welcome_pop_up"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click OK button
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK btn", "result": "1"})

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": subtab_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tab: %s, which xpath is %s" % (subtab_type, xpath), "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNewPromotionComponent.ClickOnSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
                replace_text - type of argument which you want to replace(optional)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        if "replace_text" in arg:
            replace_text = arg["replace_text"]
            xpath = Util.GetXpath({"locate": button_type})
            xpath = xpath.replace("replaced_text", replace_text)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (button_type, xpath), "result": "1"})
        else:
            ##Click on button
            xpath = Util.GetXpath({"locate": button_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column
                input_content - input value
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click on checkbox
            Input argu :
                checkbox_type - type of checkbox which defined by caller
                checkbox_value - define which checkbox to click
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]
        checkbox_value = arg["checkbox_value"]

        ##Click on checkbox
        xpath = Util.GetXpath({"locate": checkbox_type})
        xpath = xpath.replace('value_to_be_replaced', checkbox_value)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionComponent.ClickOnCheckbox')


class AdminNewPromotionTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubTabOnLeftPanel(arg):
        '''
        ClickSubTabOnLeftPanel : Click tab on left panel in promotion admin page
                Input argu :
                    subtab - subtab name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg["subtab"]

        ##Check pop up window in promotion admin
        xpath = Util.GetXpath({"locate": "welcome_pop_up"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click OK button
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK btn", "result": "1"})

        ##Click a tab at the left panel
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click a tab at the left panel", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminNewPromotionTab.ClickSubTabOnLeftPanel -> ' + subtab)


class AdminExclusivePricePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoSetting(arg):
        '''InputPromoSetting : Input basic promotion setting config
                Input argu :
                        promo_name - exclusive price promo name
                        start_time_delta - start time delta
                        end_time_delta - end time delta
                        max_po_items - PO Items % = Sum(Confirmed PO) / Sum(All Confirmed Items)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        promo_name = arg["promo_name"]
        start_time_delta = arg["start_time_delta"]
        end_time_delta = arg["end_time_delta"]
        max_po_items = arg["max_po_items"]

        ##Input exclusive price name
        if promo_name:
            xpath = Util.GetXpath({"locate": "promo_name"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promo name field", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promo_name, "message": "Input exclusvie price name", "result": "1"})

        ##Input start time with calculate value by start time delta
        if start_time_delta:
            start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(start_time_delta), 0)
            xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})

            popup_field_xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": start_time_string, "message": "Input start time", "result": "1"})

            ##send selenium key event
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input end time with calculate value by end time delta
        if end_time_delta:
            end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time_delta), 0)
            xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})

            popup_field_xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": end_time_string, "message": "Input end time", "result": "1"})

            ##send selenium key event
            BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})

        ##Input max_po_items
        if max_po_items:
            xpath = Util.GetXpath({"locate": "max_po_items"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_po_items, "message": "Input max po items percnet", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.InputPromoSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoProductSetting(arg):
        ''' InputPromoProductSetting : Input promo products setting config
                Input argu :
                        min_discount_price - min discount price
                        max_discount_price - max discount price
                        min_discount_percent - min discount percent
                        max_discount_percent - max discount percent
                        min_promo_stock - min promo stock
                        min_rebate - min rebate
                        max_rebate - max rebate
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        min_discount_price = arg["min_discount_price"]
        max_discount_price = arg["max_discount_price"]
        min_discount_percent = arg["min_discount_percent"]
        max_discount_percent = arg["max_discount_percent"]
        min_promo_stock = arg["min_promo_stock"]
        min_rebate = arg["min_rebate"]
        max_rebate = arg["max_rebate"]

        if min_discount_price or max_discount_price:
            ##Input min discount price
            xpath = Util.GetXpath({"locate": "min_discount_price_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_discount_price,"message": "Modify product prmotion config", "result": "1"})

            ##Input max discount price
            xpath = Util.GetXpath({"locate": "max_discount_price_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_discount_price,"message": "Modify product prmotion config", "result": "1"})

        if min_discount_percent or max_discount_percent:
            ##Input min discount percent
            xpath = Util.GetXpath({"locate": "min_discount_percent_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_discount_percent,"message": "Modify product prmotion config", "result": "1"})

            ##Input max discount percent
            xpath = Util.GetXpath({"locate": "max_discount_percent_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_discount_percent,"message": "Modify product prmotion config", "result": "1"})

        if min_rebate:
            ##Input min rebate
            xpath = Util.GetXpath({"locate": "min_rebate"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_rebate, "message": "Modify min rebate config","result": "1"})

        if max_rebate:
            ##Input max rebate
            xpath = Util.GetXpath({"locate": "max_rebate"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_rebate, "message": "Modify max rebate config","result": "1"})

        if min_promo_stock:
            ##Input min promo stock
            xpath = Util.GetXpath({"locate": "min_promo_stock"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_promo_stock,"message": "Modify promo stock config", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.InputPromoProductSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UserScopeSetting(arg):
        ''' UserScopeSetting : Choose target user group
                Input argu :
                        user_group - Choose target user group
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        user_group = arg["user_group"]

        ##Click drop down
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down btn", "result": "1"})

        ##wait drop down list
        time.sleep(5)

        ##Click user scope label
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": user_group, "result": "1"})
        time.sleep(5)

        ##send selenium key event
        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.UserScopeSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DisplaySetting(arg):
        ''' DisplaySetting : Input labels for user scope
                Input argu :
                        type - which label you want to display in FE
                        labels - labels for users in user scope
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        type = arg["type"]
        labels = arg["labels"]

        ##scroll to component using javascript
        BaseUICore.ExecuteScript({"script":'document.getElementById("app").getElementsByClassName("_2na3esa_wyhmUdJ80_GoQV _1msC8b_Au_HhKXgsz38VCU kPUiTjDCuEG3Ev17ux3rc ant-layout-content")[0].scrollTo(0,600)', "result": "1"})

        ##Input labels for users in user scope
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": labels,"message": "Input labels for user scope", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.DisplaySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DisplaySettingForAllUsers(arg):
        ''' DisplaySettingForAllUsers :
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click labels for all users toggle
        xpath = Util.GetXpath({"locate": "labels_for_all_users_toggle"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click labels for all users toggle", "result": "1"})

        ##Input labels for users in user scope
        xpath = Util.GetXpath({"locate": "vn_label"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "user can saved {saved_amount}", "message": "Input labels for all users","result": "1"})

        ##Input labels for users in user scope
        xpath = Util.GetXpath({"locate": "en_label"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "user can saved {saved_amount}","message": "Input labels for all users", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.DisplaySettingForAllUsers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadPromotionListCSV(arg):
        ''' UploadPromotionListCSV : Upload promotion list csv
                Input argu :
                        file_name - which csv you want to upload
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        file_name = arg['file_name']

        ##Input csv file
        xpath = Util.GetXpath({"locate": "promotion_list_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.UploadPromotionListCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchApproveItems(arg):
        ''' BatchApproveItems : Upload batch approve items csv
                Input argu :
                        file_name - which csv you want to upload
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        file_name = arg['file_name']

        ##Click to switch to batch approve block
        xpath = Util.GetXpath({"locate": "batch_approve_block"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch approve input", "result": "1"})
        time.sleep(3)

        ##Input csv file
        xpath = Util.GetXpath({"locate": "batch_approve_input"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.BatchApproveItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCheckBox(arg):
        ''' ClickItemCheckBox : Click item checkbox in promotion template page
                Input argu :
                        item_id - choose specific item id checkbox
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click item checkbox
        xpath = Util.GetXpath({"locate": "item_id_checkbox"})
        xpath = xpath.replace('replace_item_id', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.ClickItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupItemCheckBox(arg):
        ''' ClickPopupItemCheckBox : Click item checkbox on confirm popup page
                Input argu :
                        item_id - choose specific item id checkbox
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click item checkbox
        xpath = Util.GetXpath({"locate": "item_id_checkbox"})
        xpath = xpath.replace('replace_item_id', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click popup item checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.ClickPopupItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToHomePage(arg):
        ''' GoToHomePage : Go to promotion homepage
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click promotion bar text and go to homepage
        xpath = Util.GetXpath({"locate": "homepage_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click homepage text", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.GoToHomePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPromotionDetailPage(arg):
        ''' GoToPromotionDetailPage : Click action btn to go to promotion detail page
                Input argu :
                        promo_name - click specific promotion id action btn and go to detail page
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        promo_name = arg["promo_name"]

        ##Click action btn
        xpath = Util.GetXpath({"locate": "promo_id_action"})
        xpath = xpath.replace('replace_promo_name', promo_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promo id action btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.GoToPromotionDetailPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchData(arg):
        ''' InputSearchData : input search data
                Input argu :
                        search_by - shop_id / product_id / model_id / start_time / end_time / promo_id / promo_name
                        value - input data
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        search_by = arg["search_by"]
        value = arg["value"]

        ##Search by start_time or end_time
        if search_by == "start_time" or search_by == "end_time":
            xpath = Util.GetXpath({"locate": search_by})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + search_by + " field", "result": "1"})
            ##If input current time, click "now" btn
            if value == "now":
                xpath = Util.GetXpath({"locate": "now_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click now btn in " + search_by + " field", "result": "1"})
            ##If input specific time
            else:
                BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": value, "result": "1"})
                BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input data in input block
        else:
            xpath = Util.GetXpath({"locate": search_by})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + search_by, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "message": "Input search data", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.InputSearchData')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByScopeOfUser(arg):
        ''' SearchByScopeOfUser : Choose user group in search block
                Input argu :
                        user_group - Choose target user group
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        user_group = arg["user_group"]

        ##Click drop down
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down btn", "result": "1"})
        time.sleep(3)

        ##Click user scope label
        xpath = Util.GetXpath({"locate": "user_label"})
        xpath = xpath.replace("user_label_to_be_replaced", user_group)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user label", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.SearchByScopeOfUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDisplayPerPage(arg):
        ''' ChooseDisplayPerPage : Choose dispalyed data amount per page
                Input argu :
                        displayed_data_amount - data amount per page
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        displayed_data_amount = arg["displayed_data_amount"]

        ##Click displayed data block
        xpath = Util.GetXpath({"locate": "displayed_data_block"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click displayed data block", "result": "1"})
        time.sleep(3)

        ##Click user scope label
        xpath = Util.GetXpath({"locate": "displayed_option"})
        xpath = xpath.replace("data_amount_to_be_replaced", displayed_data_amount)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click displayed data amount option", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.ChooseDisplayPerPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByStatus(arg):
        ''' SearchByStatus : Search by promo status
                Input argu :
                        status - all / scheduled / ongoing / ended / active / inactive
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click drop down
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down btn", "result": "1"})

        ##wait drop down list
        time.sleep(5)

        ##Click user scope label
        xpath = Util.GetXpath({"locate": "status_option"})
        xpath = xpath.replace("status_to_be_replaced", status.title())
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status option", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.SearchByStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTimeIsCurrent(arg):
        ''' CheckTimeIsCurrent : Check start_time / end_time default is current time
                Input argu :
                        column - start_time / end_time
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        column = arg["column"]

        ##Get current time
        current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)[:-1]

        ##Get default time
        xpath = Util.GetXpath({"locate": column})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"get_attribute", "attribute":"value", "mode":"single", "result": "1"})
        default_time = GlobalAdapter.CommonVar._PageAttributes_[:-4]

        ##Compare current time and default time
        if default_time == current_time:
            ret = 1
            dumplogger.info("Current time %s equals to defalut time %s" % (current_time, default_time))
        else:
            ret = 0
            dumplogger.info("Current time %s not equals to defalut time %s" % (current_time, default_time))

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.CheckTimeIsCurrent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HandleServerError(arg):
        ''' HandleServerError : Check if server error popup exists, click cancel btn
                Input argu :
                        N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check if server error popup exists, click cancel btn
        xpath = Util.GetXpath({"locate": "server_error_msg"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "cancel_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn on popup", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePricePage.HandleServerError')


class AdminManageUserGroupPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchData(arg):
        ''' InputSearchData : input search data
                Input argu :
                        search_by - user_group_id / user_group_name / user_tag_name
                        value - input data
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        search_by = arg["search_by"]
        value = arg["value"]

        ##Input data in input block
        xpath = Util.GetXpath({"locate": search_by})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + search_by, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "message": "Input search data", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.InputSearchData')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteUserGroup(arg):
        ''' DeleteUserGroup : Delete user group
                Input argu :
                        user_group_name - user group name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        user_group_name = arg["user_group_name"]

        ##Click delete user group btn
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace("user_group_name_to_be_replaced", user_group_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete user group btn", "result": "1"})
        time.sleep(2)

        ##Click ok btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.DeleteUserGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByStatus(arg):
        ''' SearchByStatus : Search by user group status
                Input argu :
                        status - all / active / inactive
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click drop down
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down btn", "result": "1"})

        ##wait drop down list
        time.sleep(5)

        ##Click user scope label
        xpath = Util.GetXpath({"locate": "status_option"})
        xpath = xpath.replace("status_to_be_replaced", status.title())
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status option", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.SearchByStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNewUserGroupInfo(arg):
        ''' InputNewUserGroupInfo : input new user group info
                Input argu :
                        user_tag_name - user tag name
                        user_group_name - user group name
                        priority_value - priority value 1,2... etc.
                        status - active / inactive
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        user_tag_name = arg["user_tag_name"]
        user_group_name = arg["user_group_name"]
        priority_value = arg["priority_value"]
        status = arg["status"]

        ##If need to fill in user_tag_name
        if user_tag_name:
            xpath = Util.GetXpath({"locate": "user_tag_name"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user tag block", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_tag_name, "message": "Input user_tag_name", "result": "1"})
            time.sleep(2)

        ##If need to fill in user_group_name
        if user_group_name:
            xpath = Util.GetXpath({"locate": "user_group_name"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user group block", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_group_name, "message": "Input user_group_name", "result": "1"})
            time.sleep(2)

        ##If need to fill in priority_value
        if priority_value:
            xpath = Util.GetXpath({"locate": "priority_value"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click proirity value block", "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": priority_value, "message": "Input priority_value", "result": "1"})
            time.sleep(2)

        if status == "active":
            xpath = Util.GetXpath({"locate": "active_switch"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click active switch", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.InputNewUserGroupInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplaySetting(arg):
        ''' InputDisplaySetting : input display setting on add new user group page
                Input argu :
                        display_label_en - display label in English
                        display_label_local - display label in local language
                        mobile_skin_file - mobile skin file
                        pc_skin_file - pc skin file
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        display_label_en = arg["display_label_en"]
        display_label_local = arg["display_label_local"]
        mobile_skin_file = arg["mobile_skin_file"]
        pc_skin_file = arg["pc_skin_file"]

        ##If need to fill in display_label_en
        if display_label_en:
            xpath = Util.GetXpath({"locate": "display_label_en"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_label_en, "message": "Input display_label_en", "result": "1"})
            time.sleep(2)

        ##If need to fill in display_label_local
        if display_label_local:
            xpath = Util.GetXpath({"locate": "display_label_local"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_label_local, "message": "Input display_label_local", "result": "1"})
            time.sleep(2)

        ##Upload mobile_skin_file if needed
        if mobile_skin_file:
            xpath = Util.GetXpath({"locate": "mobile_skin_file"})
            ##If file is GIF
            if mobile_skin_file == "gif":
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": mobile_skin_file, "file_type": "gif", "result": "1"})
            ##If file is jpg
            else:
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": mobile_skin_file, "file_type": "jpg", "result": "1"})
                time.sleep(1)

        ##Upload pc_skin_file if needed
        if pc_skin_file:
            xpath = Util.GetXpath({"locate": "pc_skin_file"})
            ##If file is GIF
            if pc_skin_file == "gif":
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": pc_skin_file, "file_type": "gif", "result": "1"})
            ##If file is jpg
            else:
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": pc_skin_file, "file_type": "jpg", "result": "1"})
                time.sleep(1)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.InputDisplaySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplaySettingForAllUser(arg):
        ''' InputDisplaySettingForAllUser : input display setting for all user on add new user group page
                Input argu :
                        for_all_user_toggle - on / off
                        display_label_en - display label in English
                        display_label_local - display label in local language
                        url - display url
                        display_url_en - display url in English
                        display_url_local - display url in local language
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        for_all_user_toggle = arg["for_all_user_toggle"]
        display_label_local = arg["display_label_local"]
        display_label_en = arg["display_label_en"]
        url = arg["url"]
        display_url_en = arg["display_url_en"]
        display_url_local = arg["display_url_local"]

        time.sleep(3)
        ##Scroll down the page
        BaseUICore.ExecuteScript({"script":'document.getElementById("app").getElementsByClassName("ant-layout-content _26z-xP00r6g08BRAx9NeHe _3JRaEZfygSNhF_YqhFjwrv _2qViSO6M1P5_7zduce7j_y")[0].scrollTo(0,3000)', "result": "1"})
        time.sleep(3)

        ##Get for-all-user toggle status
        xpath = Util.GetXpath({"locate": "for_all_user_toggle"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result":"1"})
        BaseUICore.GetAttributes({"attrtype":"get_attribute","attribute":"class","mode":"single", "result":"1"})

        ##If toggle is "off" and need to turn on
        if for_all_user_toggle == "on":
            if GlobalAdapter.CommonVar._PageAttributes_ == "ant-switch ant-switch-small":
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click for-all-user toggle", "result": "1"})
                time.sleep(5)
        ##If toggle is "on" and need to turn off
        elif for_all_user_toggle == "off":
            if GlobalAdapter.CommonVar._PageAttributes_ == "ant-switch ant-switch-small ant-switch-checked":
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click for-all-user toggle", "result": "1"})
                time.sleep(5)

        ##If need to fill in display_label_en
        if display_label_en:
            xpath = Util.GetXpath({"locate": "display_label_en"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_label_en, "message": "Input display_label_en", "result": "1"})
            time.sleep(2)

        ##If need to fill in display_label_local
        if display_label_local:
            xpath = Util.GetXpath({"locate": "display_label_local"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_label_local, "message": "Input display_label_local", "result": "1"})
            time.sleep(2)

        ##If need to fill in url
        if url:
            xpath = Util.GetXpath({"locate": "url"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "message": "Input url", "result": "1"})
            time.sleep(2)

        ##If need to fill in display_url_en
        if display_url_en:
            xpath = Util.GetXpath({"locate": "display_url_en"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_url_en, "message": "Input display_url_en", "result": "1"})
            time.sleep(2)

        ##If need to fill in display_url_local
        if display_url_local:
            xpath = Util.GetXpath({"locate": "display_url_local"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_url_local, "message": "Input display_url_local", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.InputDisplaySettingForAllUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditUserGroupName(arg):
        ''' EditUserGroupName : Edit user group name
                Input argu :
                        input_group_name - group name to input
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        input_group_name = arg["input_group_name"]

        ##Input group name
        xpath = Util.GetXpath({"locate": "group_name_block"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_group_name, "message": "Input group name", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.EditUserGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeLabelBackgroundColor(arg):
        ''' ChangeLabelBackgroundColor : Change Label Background Color on add new user group page
                Input argu :
                        hex_color - color code in HEX
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        hex_color = arg["hex_color"]

        ##Scroll down the page
        time.sleep(3)
        BaseUICore.ExecuteScript({"script":'document.getElementById("app").getElementsByClassName("ant-layout-content _26z-xP00r6g08BRAx9NeHe _3JRaEZfygSNhF_YqhFjwrv _2qViSO6M1P5_7zduce7j_y")[0].scrollTo(0,2000)', "result": "1"})
        time.sleep(3)

        ##Click set background color
        color_block_xpath = Util.GetXpath({"locate": "color_block"})
        BaseUICore.Click({"method": "xpath", "locate": color_block_xpath, "message": "Click color block", "result": "1"})
        time.sleep(5)

        ##Input HEX code
        hex_code_input_xpath = Util.GetXpath({"locate": "hex_code_input"})
        BaseUICore.ExecuteScript({"script": "arguments[0].setAttribute('style', '')", "xpath": hex_code_input_xpath, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":hex_code_input_xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": hex_code_input_xpath, "string": hex_color, "message": "Input HEX code", "result": "1"})
        time.sleep(2)

        ##Click set background color again to save
        BaseUICore.Click({"method": "xpath", "locate": color_block_xpath, "message": "Click color block", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.ChangeLabelBackgroundColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPriorityValue(arg):
        ''' EditPriorityValue : Edit priority value
                Input argu :
                        priority_value - priority value to input
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        priority_value = arg["priority_value"]

        ##Input priority value
        xpath = Util.GetXpath({"locate": "priority_value_block"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": priority_value, "message": "Input priority value", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.EditPriorityValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteSkinPhoto(arg):
        ''' DeleteSkinPhoto : Delete skin photo setting on add new user group page
                Input argu :
                         N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Get delete btn xpath
        delete_btn_xapth = Util.GetXpath({"locate": "delete_btn"})

        ##Delete mobile skin photo
        mobile_xapth = Util.GetXpath({"locate": "mobile_skin_block"})
        BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": mobile_xapth, "locatehidden": delete_btn_xapth, "result": "1"})
        time.sleep(5)

        ##Delete pc skin photo
        pc_xapth = Util.GetXpath({"locate": "pc_skin_block"})
        BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": pc_xapth, "locatehidden": delete_btn_xapth, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.DeleteSkinPhoto')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchStatusToggle(arg):
        ''' SwitchStatusToggle : Switch status toggle active / inactive
                Input argu :
                        user_group_name - user group name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        user_group_name = arg["user_group_name"]

        ##Click status toggle
        xpath = Util.GetXpath({"locate": "status_toggle"})
        xpath = xpath.replace("group_name_to_be_replaced", user_group_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status toggle", "result": "1"})
        time.sleep(5)

        ##Click ok btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.SwitchStatusToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckDataAmount(arg):
        ''' CheckDataAmount : Check data amount
                Input argu :
                        N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 0

        ##Get reserve element
        total_data_amount = int(GlobalAdapter.PromotionE2EVar._AuditDataInfo_.split(" ")[1])

        ##Check data amount increase 1
        xpath = Util.GetXpath({"locate":"total_data"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        if GlobalAdapter.CommonVar._PageAttributes_.split(" ")[1] == str(total_data_amount + 1):
            dumplogger.info("Check total data increased 1")
            ret = 1
        else:
            dumplogger.info("Total data didn't increase 1")
            ret = 0

        OK(ret, int(arg['result']), 'AdminManageUserGroupPage.CheckDataAmount')


class AdminExclusivePriceButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewPromotion(arg):
        ''' ClickNewPromotion : Click new promotion btn in overview page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click new promotion btn
        xpath = Util.GetXpath({"locate": "new_promotion_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new promotion btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickNewPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewUserGroup(arg):
        ''' ClickAddNewUserGroup : Click add new user group
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click add new user group btn
        xpath = Util.GetXpath({"locate": "add_new_user_group_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new user group btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickAddNewUserGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditUserGroup(arg):
        ''' ClickEditUserGroup : Click Edit User Group btn
                Input argu :
                        user_group_id - user group id
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        user_group_id = arg["user_group_id"]

        ##Click edit user group btn
        xpath = Util.GetXpath({"locate":"edit_btn"})
        xpath = xpath.replace("user_group_id_to_be_replace", user_group_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit user group btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickEditUserGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        ''' ClickSave : Click save btn
                    Input argu :
                        page_type - promo_detail_page / manage_user_group_page
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click save btn
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel btn in promotion setting page
                Input argu :
                    page_type - promo_detail_page / item_warning_popup
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProceed(arg):
        ''' ClickProceed : Click proceed btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click proceed btn
        xpath = Util.GetXpath({"locate": "proceed_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click proceed btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickProceed')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAllCheckbox(arg):
        ''' ClickSelectAllCheckbox : Click select all btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click select all btn
        xpath = Util.GetXpath({"locate": "select_all_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select all btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickSelectAllCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductConfirmation(arg):
        ''' ClickProductConfirmation : Click product confirmation tab
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click product confirmation tab
        xpath = Util.GetXpath({"locate": "product_confirmation_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product confirmation tab", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickProductConfirmation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExclusivePriceDetail(arg):
        ''' ClickExclusivePriceDetail : Click exclusive price detail tab
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click exclusive price detail tab
        xpath = Util.GetXpath({"locate": "exclusive_price_detail_tab"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click exclusive price detail tab", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickExclusivePriceDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchSetting(arg):
        ''' ClickBatchSetting : Click batch setting btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click batch setting btn
        xpath = Util.GetXpath({"locate": "batch_setting_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch setting btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickBatchSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' ClickConfirm : Click Confirm on promo setting page
                Input argu :
                    page_type - confirm_product / promo_detail_page
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReject(arg):
        ''' ClickReject : Click reject item btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click reject item btn
        xpath = Util.GetXpath({"locate": "reject_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reject item btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickReject')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        ''' ClickUpdate : Click Update btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click update btn
        xpath = Util.GetXpath({"locate": "update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        ''' ClickSearch : Click search btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        ''' ClickReset : Click reset btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAdd(arg):
        ''' ClickAdd : Click add btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click add btn
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchEdit(arg):
        ''' ClickBatchEdit : Click batch edit btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click batch edit btn
        xpath = Util.GetXpath({"locate": "batch_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch edit btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickBatchEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        ''' ClickNextPage : Click next page btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click next page btn
        xpath = Util.GetXpath({"locate": "next_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditPromotionDetail(arg):
        ''' ClickEditPromotionDetail : Click edit btn on promotion detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickEditPromotionDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLabelForAllUserToggle(arg):
        ''' ClickLabelForAllUserToggle : Click labels for all users toggle
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click labels for all users toggle
        xpath = Util.GetXpath({"locate": "labels_for_all_users_toggle"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickLabelForAllUserToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditPromotion(arg):
        ''' ClickEditPromotion : Click edit btn
                Input argu : promo_name - name of the exclusive price promo
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        promo_name = arg["promo_name"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("promo_name_to_be_replaced", promo_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickEditPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickManageUserGroup(arg):
        ''' ClickManageUserGroup : Click manage user group btn
                Input argu : NA
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click manage user group btn
        xpath = Util.GetXpath({"locate": "manage_user_group_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click manage user group btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickManageUserGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportList(arg):
        ''' ClickExportList : Click export list btn
                Input argu : NA
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click export list btn
        xpath = Util.GetXpath({"locate": "export_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export list btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminExclusivePriceButton.ClickExportList')


class AdminAddOnDealOverviewPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchField(arg):
        '''
        InputSearchField : Input promotion search field on PWP overview page
                Input argu :
                    promotion_type - all / AOD (AddOnDeal) / PWP (PurchaseWithPurchase) / GWP (GiftWithPurchase)
                    shop_id - shop id
                    promotion_id - promotion id
                    start_time - start time("%Y-%m-%d %H:%M:%S)
                    end_time - end time("%Y-%m-%d %H:%M:%S)
                    status - Scheduled / Ongoing / Ended
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg["promotion_type"]
        shop_id = arg["shop_id"]
        promotion_id = arg["promotion_id"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        status = arg["status"]

        ##Click which sub tab to search
        AdminAddOnDealOverviewButton.ClickToOverviewPage({"promotion_type": promotion_type, "result": "1"})
        time.sleep(2)

        ##Input promotion searching field on the overview page
        ##Search by shop id
        if shop_id:
            xpath = Util.GetXpath({"locate": "shop_id"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "search by shop id", "result": "1"})
            time.sleep(2)

        ##Search by promotion id
        if promotion_id:
            xpath = Util.GetXpath({"locate": "promotion_id"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_id, "message": "search by promotion id", "result": "1"})
            time.sleep(2)

        ##Search by start time
        if start_time:
            ##Click start time column first to prepare to input value
            xpath = Util.GetXpath({"locate": "start_time"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time column", "result": "1"})
            time.sleep(2)

            ##Input start time
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "message": "search by start time", "result": "1"})
            time.sleep(2)

            ##Click start_time calendar ok
            xpath = Util.GetXpath({"locate": "start_ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time calendar ok", "result": "1"})
            time.sleep(2)

        ##Search by end time
        if end_time:
            ##Click end time column first to prepare to input value
            xpath = Util.GetXpath({"locate": "end_time"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time column", "result": "1"})
            time.sleep(2)

            ##Input end time
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "message": "search by end time", "result": "1"})
            time.sleep(2)

            ##Click end time calendar ok
            xpath = Util.GetXpath({"locate": "end_ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time calendar ok", "result": "1"})
            time.sleep(2)

        ##Search by status
        if status:
            ##Click to display status option
            xpath = Util.GetXpath({"locate": "status_drawer"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status to show option", "result": "1"})
            time.sleep(2)

            ##Click to choose status option
            xpath = Util.GetXpath({"locate": status})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status option", "result": "1"})
            time.sleep(2)

        AdminAddOnDealOverviewButton.ClickSearch({"result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewPage.InputSearchField')


class AdminAddOnDealOverviewButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu :
                    promotion_type : AOD (AddOnDeal) / PWP (PurchaseWithPurchase) / GWP (GiftWithPurchase)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        promotion_type = arg["promotion_type"]
        ret = 1

        xpath1 = Util.GetXpath({"locate":"add_new_list"})
        xpath2 = Util.GetXpath({"locate":promotion_type})

        ##Click add new button
        BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath1, "locatehidden":xpath2, "message":"Click to add new promotion", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToOverviewPage(arg):
        '''
        ClickToOverviewPage : Click to overview page
                Input argu :
                    promotion_type : all / AOD (AddOnDeal) / PWP (PurchaseWithPurchase) / GWP (GiftWithPurchase)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        promotion_type = arg["promotion_type"]
        ret = 1

        ##Click to overview page
        xpath = Util.GetXpath({"locate": promotion_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to overview page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickToOverviewPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in the first row
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button to seach promotion
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefreshSearchField(arg):
        '''
        ClickRefreshSearchField : Click refresh button to clean search field
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refresh button to clean search field
        xpath = Util.GetXpath({"locate": "refresh_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refresh button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickRefreshSearchField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUpdateItems(arg):
        '''
        ClickBatchUpdateItems : Click batch update items button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch update items button
        xpath = Util.GetXpath({"locate": "batch_update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch update items button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickBatchUpdateItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassCreate(arg):
        '''
        ClickMassCreate : Click mass create button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mass create button
        xpath = Util.GetXpath({"locate": "mass_create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass create button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealOverviewButton.ClickMassCreate')


class AdminAddOnDealPage:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBasicAddOnDealConfig(arg):
        '''
        InputBasicAddOnDealConfig : Input basic AOD config
            Input argu :
                name - AOD name
                shop_id - shop id
                start_time - start time
                end_time - end time
                purchase_limit - max quantity of add-on items that each user can purchase in one deal
                label_en - EN lable field
                label_region - region lable field
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        name = arg["name"]
        shop_id = arg["shop_id"]
        start_time = int(arg["start_time"])
        end_time = int(arg["end_time"])
        purchase_limit = arg["purchase_limit"]
        label_en = arg["label_en"]
        label_region = arg["label_region"]

        ##Input AOD name
        xpath = Util.GetXpath({"locate": "name_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "message": "Input AOD name", "result": "1"})

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "Input shop id", "result": "1"})

        ##Click start time column first to prepare to input value
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time column", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(3)

        ##Set start time
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=start_time, is_tw_time = 1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
        time.sleep(3)

        ##Click ok on start time calendar
        xpath = Util.GetXpath({"locate": "start_time_ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn on start time calendar", "result": "1"})

        ##Click end time column first to prepare to input value
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time column", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(3)

        ##Set end time
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=end_time, is_tw_time = 1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        time.sleep(3)

        ##Click ok on end time calendar
        xpath = Util.GetXpath({"locate": "end_time_ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn on end time calendar", "result": "1"})

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit", "result": "1"})

        ##Input label(region)
        xpath = Util.GetXpath({"locate": "label_" + Config._TestCaseRegion_.lower()})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": label_region, "message": "Input label(region)", "result": "1"})

        ##Input label(EN)
        xpath = Util.GetXpath({"locate": "label_en_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": label_en, "message": "Input label(EN)", "result": "1"})

        ##MY,SG,TW needs to fill the extra label field
        if Config._TestCaseRegion_ in ("MY", "SG", "TW"):
            xpath = Util.GetXpath({"locate": "label_ch_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "auto test", "message": "Input label(CH)", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealPage.InputBasicAddOnDealConfig')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddMainItem(arg):
        '''
        AddMainItem : Add main item in main item pool
                Input argu :
                    shop_id - main item shop id
                    item_id - main item item id
                    purchase_limit - main product purchase limit
                    is_click - 0 : don't click ok btn / 1 : click ok btn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        item_id = arg["item_id"]
        purchase_limit = arg["purchase_limit"]
        is_click = int(arg["is_click"])

        ##Click add item button
        xpath = Util.GetXpath({"locate": "add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add item button", "result": "1"})
        time.sleep(5)

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "Input shop id", "result": "1"})
        time.sleep(2)

        ##Input item id
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "message": "Input item id", "result": "1"})
        time.sleep(2)

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit","result": "1"})
        time.sleep(2)

        ##Click ok button
        if is_click:
            AdminAddOnDealButton.ClickOK({"page_type": "add_mainitem", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealPage.AddMainItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddSubItem(arg):
        '''
        AddSubItem : Add sub item in sub item pool
                Input argu :
                    shop_id - sub item shop id
                    item_id - sub item item id
                    model_id - sub item model id
                    addon_price - sub item add-on price
                    purchase_limit - sub product purchase limit
                    rebat - rebat
                    is_click - 0 : don't click ok btn / 1 : click ok btn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        item_id = arg["item_id"]
        model_id = arg["model_id"]
        addon_price = arg["addon_price"]
        purchase_limit = arg["purchase_limit"]
        rebat = arg["rebat"]
        is_click = int(arg["is_click"])

        ##Click add item button
        xpath = Util.GetXpath({"locate": "add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add item button", "result": "1"})
        time.sleep(2)

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "Input shop id", "result": "1"})
        time.sleep(2)

        ##Input item id
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "message": "Input item id", "result": "1"})
        time.sleep(2)

        ##Input model id
        xpath = Util.GetXpath({"locate": "modelid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": model_id, "message": "Input model id", "result": "1"})
        time.sleep(2)

        ##Input add-on price
        xpath = Util.GetXpath({"locate": "add_on_price"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": addon_price, "message": "Input add-on price", "result": "1"})
        time.sleep(2)

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit","result": "1"})
        time.sleep(2)

        ##Input rebat
        if rebat:
            xpath = Util.GetXpath({"locate": "rebat_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(1)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rebat, "message": "Input rebat","result": "1"})

        ##Click ok button
        if is_click:
            AdminAddOnDealButton.ClickOK({"page_type": "add_subitem", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealPage.AddSubItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToMainItemSection(arg):
        '''
        MoveToMainItemSection : Move to main product pool section(can show main item button)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Move to main/add on pool section
        xpath = Util.GetXpath({"locate": "main"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), "AdminAddOnDealPage.MoveToMainItemSection")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductPoolCSV(arg):
        '''
        UploadProductPoolCSV : Upload csv file to add on deal item
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        ret = 1

        ##Upload file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminAddOnDealPage.UploadProductPoolCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeItemOrder(arg):
        '''
        ChangeItemOrder : move element to change order
                Input argu :
                    drag_item_id - id of the drag item
                    target_item_id - id of the target item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drag_item_id = arg["drag_item_id"]
        target_item_id = arg["target_item_id"]

        ##move element to change order
        drag_elem = Util.GetXpath({"locate": "drag_elem"})
        drag_elem = drag_elem.replace('id_to_be_replaced', drag_item_id)
        target_elem = Util.GetXpath({"locate": "drop_elem"})
        target_elem = target_elem.replace('id_to_be_replaced', target_item_id)

        ##Drag and drop element
        BaseUICore.DragAndDrop({"drag_elem": drag_elem, "target_elem": target_elem, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealPage.ChangeItemOrder')


class AdminAddOnDealButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click next page on AOD setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next page on AOD setting page
        xpath = Util.GetXpath({"locate": "next_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page on AOD setting page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK button
                Input argu :
                    page_type - add_mainitem / add_subitem / popup_window
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click ok button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackArrow(arg):
        '''
        ClickBackArrow : Click back arrow button on aod page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back arrow button
        xpath = Util.GetXpath({"locate": "back_arrow_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickBackArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button on add main/add-on item page
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click cancel button on add main/add-on item page
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button on add main/add-on item page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableAll(arg):
        '''
        ClickEnableAll : Click enable all btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        item_type = arg["item_type"]
        ret = 1

        ##Click enable all btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable all btn", "result": "1"})
        time.sleep(2)

        ##Click 'enable all' confirm btn
        xpath = Util.GetXpath({"locate": "enable_all_confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable all confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickEnableAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckbox(arg):
        '''
        ClickCheckbox : Click item checkbox in main/addon pool
                Input argu :
                    click_type - one / all
                    item_id - item id
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_type = arg["click_type"]
        item_id = arg["item_id"]
        item_type = arg["item_type"]

        ##If click all
        if click_type == "all":
            xpath = Util.GetXpath({"locate": item_type})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        else:
            ##Click one item's checkbox only
            xpath = Util.GetXpath({"locate": "checkbox"})
            xpath = xpath.replace("id_to_be_replaced", item_id)
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnable(arg):
        '''
        ClickEnable : Click enable btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click enable btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickEnable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click disable btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        ##Click ok btn on popup window
        AdminAddOnDealButton.ClickOK({"page_type": "popup_window", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickDisable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click delete btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        time.sleep(2)

        ##Check if popup confirm window
        xpath = Util.GetXpath({"locate": "popup_confirm_window"})

        ##Click delete confirm btn
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            xpath = Util.GetXpath({"locate": "confirm_delete_btn"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplate(arg):
        '''
        ClickDownloadTemplate : Click download template button
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click download template button
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download template button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickDownloadTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadFile(arg):
        '''
        ClickUploadFile : Click upload file button
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click upload file button
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload file button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickUploadFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export button
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click export button
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeOrder(arg):
        '''
        ClickChangeOrder : Click change add-on item order button
                Input argu :
                    change_type - manual / csv
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        change_type = arg["change_type"]

        ##UI move to change order button so it can be clicked
        xpath = Util.GetXpath({"locate": "change_order_section"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
        time.sleep(2)

        ##Click change add-on item order btn
        xpath = Util.GetXpath({"locate": "change_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change order btn", "result": "1"})
        time.sleep(2)

        ##Click change order by manual/csv
        xpath = Util.GetXpath({"locate": change_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change order type ->" + change_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickChangeOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOpenFileWindow(arg):
        '''
        ClickOpenFileWindow :
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to open file window
        xpath = Util.GetXpath({"locate": "file_window"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to open file window", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminAddOnDealButton.ClickOpenFileWindow')


class AdminPurchaseWithPurchasePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBasicPurchaseWithPurchaseConfig(arg):
        '''
        InputBasicPurchaseWithPurchaseConfig : Input basic PWP config
            Input argu :
                name - PWP name
                shopid - shop id
                start_time - start time
                end_time - end time
                label_en - EN lable field
                label_region - region lable field
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        name = arg["name"]
        shopid = arg["shopid"]
        start_time = int(arg["start_time"])
        end_time = int(arg["end_time"])
        label_en = arg["label_en"]
        label_region = arg["label_region"]

        ##Input PWP name
        xpath = Util.GetXpath({"locate": "name_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "message": "Input PWP name", "result": "1"})

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})

        ##Click start time column first to prepare to input value
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time column", "result": "1"})

        ##Set start time
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=start_time, is_tw_time = 0)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
        time.sleep(3)

        ##Click ok btn on start time calendar
        xpath = Util.GetXpath({"locate": "start_time_ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn on start time calendar", "result": "1"})

        ##Click end time column first to prepare to input value
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time column", "result": "1"})

        ##Set end time
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=end_time, is_tw_time = 0)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        time.sleep(3)

        ##Click ok btn on end time calendar
        xpath = Util.GetXpath({"locate": "end_time_ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn on end time calendar", "result": "1"})

        ##Input label(region)
        xpath = Util.GetXpath({"locate": "label_" + Config._TestCaseRegion_.lower()})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": label_region, "message": "Input label(region)", "result": "1"})

        ##Input label(EN)
        xpath = Util.GetXpath({"locate": "label_en_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": label_en, "message": "Input label(EN)", "result": "1"})

        ##MY,SG needs to fill extra label
        if Config._TestCaseRegion_ in ("MY", "SG"):
            xpath = Util.GetXpath({"locate": "label_ch_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "auto test", "message": "Input label(CH)", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchasePage.InputBasicPurchaseWithPurchaseConfig')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetPromotionCriteria(arg):
        '''
        SetPromotionCriteria : Set promotion criteria
                Input argu :
                    promotion_type - min_spend / min_qty
                    tier_number - promotion setting tier
                    tier_x - min_spend($) or min_qty number(item amount)
                    tier_y - numbers of the add on items one can get
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg["promotion_type"]
        tier_number = int(arg["tier_number"])
        tier_x = arg["tier_x"]
        tier_y = arg["tier_y"]

        ##Click to choose promotion type
        xpath = Util.GetXpath({"locate": promotion_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to choose promotion type", "result": "1"})
        time.sleep(2)

        ##Set tier value
        value_x = tier_x.split(",")
        value_y = tier_y.split(",")

        for number in range(tier_number):
            ##If not first tier, should click 'add tier' first to input value
            if number != 0:
                ##Click 'add tier'
                xpath = Util.GetXpath({"locate": "add_tier_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to add a new tier", "result": "1"})

            ##Input x value(min spend or min qty)
            xpath = Util.GetXpath({"locate": "tier_x_value"}).replace("tier_num_to_be_replaced", str(number+1))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value_x[number], "message": "input x value", "result": "1"})
            time.sleep(2)

            ##Input y value(add on item purchase number)
            xpath = Util.GetXpath({"locate": "tier_y_value"}).replace("tier_num_to_be_replaced", str(number+1))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value_y[number], "message": "input y value", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchasePage.SetPromotionCriteria')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddMainItem(arg):
        '''
        AddMainItem : Add main item in main item pool
                Input argu :
                    shopid - main item shop id
                    itemid - main item item id
                    purchase_limit - main product purchase limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]
        purchase_limit = arg["purchase_limit"]

        ##Click add item button
        xpath = Util.GetXpath({"locate": "add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add item button", "result": "1"})
        time.sleep(5)

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})
        time.sleep(2)

        ##Input itemid
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": itemid, "message": "Input itemid", "result": "1"})
        time.sleep(2)

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit","result": "1"})
        time.sleep(2)

        ##Click ok button
        AdminPurchaseWithPurchaseButton.ClickOK({"page_type": "add_mainitem", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchasePage.AddMainItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddSubItem(arg):
        '''
        AddSubItem : Add sub item in sub item pool
                Input argu :
                    shopid - sub item shop id
                    itemid - sub item item id
                    modelid - sub item model id
                    addon_price - sub item add-on price
                    purchase_limit - sub product purchase limit
                    rebat - rebat
                    is_click - 0 : don't click ok btn / 1 : click ok btn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]
        modelid = arg["modelid"]
        addon_price = arg["addon_price"]
        purchase_limit = arg["purchase_limit"]
        rebat = arg["rebat"]
        is_click = int(arg["is_click"])

        ##Click add item button
        xpath = Util.GetXpath({"locate": "add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add item button", "result": "1"})
        time.sleep(2)

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shop id", "result": "1"})
        time.sleep(2)

        ##Input item id
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": itemid, "message": "Input item id", "result": "1"})
        time.sleep(2)

        ##Input model id
        xpath = Util.GetXpath({"locate": "modelid_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": modelid, "message": "Input model id", "result": "1"})
        time.sleep(2)

        ##Input add-on price
        xpath = Util.GetXpath({"locate": "add_on_price"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": addon_price, "message": "Input add-on price", "result": "1"})
        time.sleep(2)

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit","result": "1"})
        time.sleep(2)

        ##Input rebat
        if rebat:
            xpath = Util.GetXpath({"locate": "rebat_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(1)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rebat, "message": "Input rebat","result": "1"})

        ##Click ok button
        if is_click:
            AdminPurchaseWithPurchaseButton.ClickOK({"page_type": "add_subitem", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchasePage.AddSubItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToMainItemSection(arg):
        '''
        MoveToMainItemSection : Move to main product pool section(can show main item button)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Move to main pool section
        xpath = Util.GetXpath({"locate": "main"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), "AdminPurchaseWithPurchasePage.MoveToMainItemSection")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeItemOrder(arg):
        '''
        ChangeItemOrder : move element to change order
                Input argu :
                    drag_item_id - id of the drag item
                    target_item_id - id of the target item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drag_item_id = arg["drag_item_id"]
        target_item_id = arg["target_item_id"]

        ##Move element to change order
        drag_elem = Util.GetXpath({"locate": "drag_elem"})
        drag_elem = drag_elem.replace('id_to_be_replaced', drag_item_id)
        target_elem = Util.GetXpath({"locate": "drop_elem"})
        target_elem = target_elem.replace('id_to_be_replaced', target_item_id)

        ##Drag and drop element
        BaseUICore.DragAndDrop({"drag_elem": drag_elem, "target_elem": target_elem, "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchasePage.ChangeItemOrder')


class AdminPurchaseWithPurchaseButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrowBack(arg):
        '''
        ClickArrowBack : Click arrow back btn on pwp setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click arrow back btn on pwp setting page
        xpath = Util.GetXpath({"locate": "arrow_back"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click arrow back btn on pwp setting page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickArrowBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click next page on PWP setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next page on PWP setting page
        xpath = Util.GetXpath({"locate": "next_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page on PWP setting page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK button
                Input argu :
                    page_type - add_mainitem / add_subitem / popup_window
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click ok button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button on add main/add-on item page
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click cancel button on add main/add-on item page
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button on add main/add-on item page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnable(arg):
        '''
        ClickEnable : Click enable btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click enable btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickEnable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_type = arg["item_type"]

        ##Click disable btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        ##Click popup ok btn
        AdminPurchaseWithPurchaseButton.ClickOK({"page_type": "popup_window", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickDisable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableAll(arg):
        '''
        ClickEnableAll : Click enable all btn
                Input argu :
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        item_type = arg["item_type"]
        ret = 1

        ##Click enable all btn
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable all btn", "result": "1"})
        time.sleep(2)

        ##Click confirm
        xpath = Util.GetXpath({"locate": "enable_all_confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable all confirm btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickEnableAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckbox(arg):
        '''
        ClickCheckbox : Click item checkbox in main/addon pool
                Input argu :
                    click_type - one / all
                    item_id - item id
                    item_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_type = arg["click_type"]
        item_id = arg["item_id"]
        item_type = arg["item_type"]

        ##If click all
        if click_type == "all":
            xpath = Util.GetXpath({"locate": item_type})

        else:
            ##Click one item checkbox only
            xpath = Util.GetXpath({"locate": "checkbox"})
            xpath = xpath.replace("id_to_be_replaced", item_id)

        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangOrder(arg):
        '''
        ClickChangOrder : Click change add-on item order button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##UI move to change order button so it can be clicked
        xpath = Util.GetXpath({"locate": "change_order_section"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
        time.sleep(3)

        ##Click change add-on item order btn
        xpath = Util.GetXpath({"locate": "change_order_btn"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPurchaseWithPurchaseButton.ClickChangOrder')


class AdminShopeeCoinsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditEarningCoinsLimitControl(arg):
        '''
        EditEarningCoinsLimitControl : Edit earning coins limit control on coin earn settings
                Input argu :
                    max_daily_earn_limit - Max Daily Earn Limit
                    max_weekly_earn_limit - Max Weekly Earn Limit
                    max_order_earn_limit - Max Order Earn Limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        max_daily_earn_limit = arg["max_daily_earn_limit"]
        max_weekly_earn_limit = arg["max_weekly_earn_limit"]
        max_order_earn_limit = arg["max_order_earn_limit"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(3)

        ##Input max daily earn limit
        xpath = Util.GetXpath({"locate": "max_daily_earn_limit_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_daily_earn_limit, "result": "1"})
        time.sleep(3)

        ##Input max weekly earn limit
        xpath = Util.GetXpath({"locate": "max_weekly_earn_limit_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_weekly_earn_limit, "result": "1"})
        time.sleep(3)

        ##Input max order earn limit
        xpath = Util.GetXpath({"locate": "max_order_earn_limit_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_order_earn_limit, "result": "1"})
        time.sleep(3)

        ##Check save button exists and enable to click
        xpath = Util.GetXpath({"locate": "save_btn"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click save
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
            time.sleep(5)

            ##Click cancel if changed value equal to original value
            xpath = Util.GetXpath({"locate": "server_error_msg"})

            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                xpath = Util.GetXpath({"locate": "cancel_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.EditEarningCoinsLimitControl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditEarningDefaultRuleSection(arg):
        '''
        EditEarningDefaultRuleSection : Edit default rule section on coin earn settings
                Input argu :
                    cash_spend - cash spend
                    coin_earn - coin earn
                    rounding_unit - rounding unit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cash_spend = arg["cash_spend"]
        coin_earn = arg["coin_earn"]
        rounding_unit = arg["rounding_unit"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(5)

        ##Input cash spend
        xpath = Util.GetXpath({"locate": "cash_spend_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": cash_spend, "result": "1"})
        time.sleep(3)

        ##Input coin earn
        xpath = Util.GetXpath({"locate": "coin_earn_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": coin_earn, "result": "1"})
        time.sleep(3)

        ##Input rounding unit
        xpath = Util.GetXpath({"locate": "rounding_unit_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rounding_unit, "result": "1"})
        time.sleep(3)

        ##Check save button exists and enable to click
        xpath = Util.GetXpath({"locate": "save_btn"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click save
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
            time.sleep(3)

            ##Click confirm
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
            time.sleep(5)

            ##Click cancel if changed value equal to original value
            xpath = Util.GetXpath({"locate": "server_error_msg"})

            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                xpath = Util.GetXpath({"locate": "cancel_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.EditEarningDefaultRuleSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddSpecificRule(arg):
        '''
        AddSpecificRule : Add specific rule on coin earn settings
                Input argu :
                    rule_name - rule name
                    priority - priority
                    cash_spend - cash spend
                    coin_earn - coin earn
                    start_time - start time
                    end_time - end time
                    rounding_unit - rounding unit
                    scope - default(none) / collection / category
                    id - collection id / category id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg["rule_name"]
        priority = arg["priority"]
        cash_spend = arg["cash_spend"]
        coin_earn = arg["coin_earn"]
        rounding_unit = arg["rounding_unit"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        scope = arg["scope"]
        id = arg["id"]

        ##Click add new btn
        AdminShopeeCoinsComponent.ClickOnButton({"button_type": "add_new_specific_rule", "result": "1"})
        time.sleep(5)

        ##Input rule name
        AdminShopeeCoinsComponent.InputToColumn({"column_type": "specific_rule_rule_name", "input_content": rule_name, "result": "1"})
        time.sleep(3)

        ##Input priority
        AdminShopeeCoinsComponent.InputToColumn({"column_type": "specific_rule_priority", "input_content": priority, "result": "1"})
        time.sleep(3)

        ##Input cash spend
        AdminShopeeCoinsComponent.InputToColumn({"column_type": "specific_rule_cash_spend", "input_content": cash_spend, "result": "1"})
        time.sleep(3)

        ##Input coin earn
        AdminShopeeCoinsComponent.InputToColumn({"column_type": "specific_rule_coin_earn", "input_content": coin_earn, "result": "1"})
        time.sleep(3)

        ##Input rounding unit
        AdminShopeeCoinsComponent.InputToColumn({"column_type": "specific_rule_rounding_unit", "input_content": rounding_unit, "result": "1"})
        time.sleep(3)

        ##Input start time with calculate value by start time
        start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(start_time), 0)
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})

        popup_field_xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": start_time_string, "message": "Input start time", "result": "1"})

        ##send selenium key event
        BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input end time with calculate value by end time
        end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0)
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})

        popup_field_xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": end_time_string, "message": "Input end time", "result": "1"})

        ##send selenium key event
        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})
        time.sleep(5)

        ## Input product scope
        if scope:
            AdminShopeeCoinsPage.InputProductScope({"scope": scope, "id": id, "result":"1"})

        ##Click save btn
        AdminShopeeCoinsComponent.ClickOnButton({"button_type": "save", "result": "1"})
        time.sleep(30)

        ##Double confirm
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.AddSpecificRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoTime(arg):
        '''
        InputPromoTime : Input promo time in add specific rule page
                Input argu :
                    promo_time - start time and end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time with calculate value by start time
        if start_time:
            start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(start_time), 0)
            xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})

            popup_field_xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": start_time_string, "message": "Input start time", "result": "1"})

            ##send selenium key event
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input end time with calculate value by end time
        if end_time:
            end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0)
            xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})

            popup_field_xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": end_time_string, "message": "Input end time", "result": "1"})

            ##send selenium key event
            BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.InputPromoTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductScope(arg):
        '''
        InputProductScope : Input scope of product in add specific rule page
                Input argu :
                    scope - default(none) / collection / category
                    id - collection id / category id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope = arg["scope"]
        id = arg["id"]

        ##Cancel the default checkbox
        xpath = Util.GetXpath({"locate": "all_product_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Cancel all product checkbox", "result": "1"})
        time.sleep(2)

        ##Click and input collection ID
        if scope == "collection":
            ##Click collection ID box
            xpath = Util.GetXpath({"locate": "collection_id_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click collection id checkbox", "result": "1"})
            time.sleep(2)

            ##Input collection ID
            xpath = Util.GetXpath({"locate": "collection_id_input"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click collection id section", "result": "1"})
            time.sleep(2)
            BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": id, "result": "1"})
            time.sleep(2)

            ##Click enter
            BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})
            time.sleep(2)

        if scope == "category":
            xpath = Util.GetXpath({"locate": "global_category_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click global category checkbox", "result": "1"})
            time.sleep(2)

            ##Click global BE category drop down section
            global_section_box = Util.GetXpath({"locate": "global_drop_down"})
            BaseUICore.Click({"method": "xpath", "locate": global_section_box, "message": "Click drop down section", "result": "1"})
            time.sleep(5)

            ##input global BE category id
            BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": id, "result": "1"})
            time.sleep(3)

            ##click target category box
            xpath = Util.GetXpath({"locate": "target_category_box"})
            xpath = xpath.replace("category_to_be_replaced", id)

            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click target category", "result": "1"})

            time.sleep(2)
            BaseUICore.Click({"method": "xpath", "locate": global_section_box, "message": "Click drop down section", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.InputProductScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditSpendingCoinsLimitControl(arg):
        '''
        EditSpendingCoinsLimitControl : Edit spending coins limit control on coin spend settings
                Input argu :
                    max_daily_spend_limit - Max Daily Spend Limit
                    max_weekly_spend_limit - Max Weekly Spend Limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        max_daily_spend_limit = arg["max_daily_spend_limit"]
        max_weekly_spend_limit = arg["max_weekly_spend_limit"]

        ##Check if the sections are equal to argument
        ##Get daily and weekly spend limit
        xpath = Util.GetXpath({"locate":"daily_limit"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        current_daily_text = GlobalAdapter.CommonVar._PageAttributes_

        xpath = Util.GetXpath({"locate":"weekly_limit"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        current_weekly_text = GlobalAdapter.CommonVar._PageAttributes_

        if current_daily_text == max_daily_spend_limit and current_weekly_text == max_weekly_spend_limit:
            dumplogger.info("These sections are same, not need to be edited again.")

        else:
            ##Click edit btn
            xpath = Util.GetXpath({"locate": "edit_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
            time.sleep(3)

            ##Input max daily spend limit
            xpath = Util.GetXpath({"locate": "max_daily_spend_limit_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_daily_spend_limit, "result": "1"})
            time.sleep(3)

            ##Input max weekly spend limit
            xpath = Util.GetXpath({"locate": "max_weekly_spend_limit_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_weekly_spend_limit, "result": "1"})
            time.sleep(3)

            ##Check save button exists and enable to click
            xpath = Util.GetXpath({"locate": "save_btn"})

            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ##Click save
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})

                ##Click confirm
                xpath = Util.GetXpath({"locate": "confirm_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
                time.sleep(5)

                ##Click cancel if changed value equal to original value
                xpath = Util.GetXpath({"locate": "server_error_msg"})

                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    xpath = Util.GetXpath({"locate": "cancel_btn"})
                    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.EditSpendingCoinsLimitControl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditSpendingDefaultRuleSection(arg):
        '''
        EditSpendingDefaultRuleSection : Edit default rule section on coin spend settings
                Input argu :
                    item_max_percent - max % of item cash payout
                    rounding_unit - rounding unit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_max_percent = arg["item_max_percent"]
        rounding_unit = arg["rounding_unit"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(5)

        ##Input max % of item
        xpath = Util.GetXpath({"locate": "item_max_percent_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_max_percent, "result": "1"})
        time.sleep(3)

        ##Input rounding unit
        xpath = Util.GetXpath({"locate": "rounding_unit_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rounding_unit, "result": "1"})
        time.sleep(3)

        ##Check save button exists and enable to click
        xpath = Util.GetXpath({"locate": "save_btn"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click save
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
            time.sleep(5)

            ##Click cancel if changed value equal to original value
            xpath = Util.GetXpath({"locate": "server_error_msg"})

            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                xpath = Util.GetXpath({"locate": "cancel_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.EditSpendingDefaultRuleSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeSpecificRulePriority(arg):
        '''
        ChangeSpecificRulePriority : Change priority of specific rule on coin earn settings
                Input argu :
                    Rule Name - rule name which want to change priority
                    Priority - number of priority
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg["rule_name"]
        priority = arg["priority"]

        ##Get priority element of rule name
        xpath = Util.GetXpath({"locate": "target_priority_field"}).replace("rule_name_to_be_replace", rule_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click priority element", "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": priority, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.ChangeSpecificRulePriority')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDisplayAmount(arg):
        '''
        ChooseDisplayAmount : Choose how many add on deal display in coins earn settings page
                Input argu :
                    displayed_data_amount - 10 / 20 / 30 / 50
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        displayed_data_amount = arg["displayed_data_amount"]

        ##Click amount drop down list
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})
        time.sleep(3)

        ##Click user scope label
        xpath = Util.GetXpath({"locate": "displayed_option"})
        xpath = xpath.replace("data_amount_to_be_replaced", displayed_data_amount)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click displayed data amount option", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopeeCoinsPage.ChooseDisplayAmount')


class AdminShopeeCoinsComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
                Input argu :
                    button_type - type of button which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of column based on arguments
            Input argu :
                column_type - type of column which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s column" % (column_type), "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeCoinsComponent.InputToColumn')


class AdminBundleDealPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBundleID(arg):
        '''
        InputBundleID : Input bundle id as search input in overview page
                Input argu :
                    bundle_id - bundle deal id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        bundle_id = arg["bundle_id"]
        ret = 1

        ##Input bundle id
        xpath = Util.GetXpath({"locate": "bundle_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle id input column", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": bundle_id, "message": "Input bundle id -> " + bundle_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.InputBundleID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputShopID(arg):
        '''
        InputShopID : Input shop id as search input in overview page
                Input argu :
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        shop_id = arg["shop_id"]
        ret = 1

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shop_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop id input column", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "Input shop id -> " + shop_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.InputShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetTimeStamp(arg):
        '''
        SetTimeStamp : Set start or end time and click ok for search or edit bundle deal
                Input argu :
                    time_type - start_time / end_time
                    value - (time format %Y-%m-%d %H:%M:%S)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        time_type = arg["time_type"]
        value = arg["value"]
        ret = 1

        ##Clean and input time stamp
        xpath = Util.GetXpath({"locate": time_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
        AdminBundleDealComponent.ClickOnButton({"button_type": "ok", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.SetTimeStamp ->' + "%s: " % (time_type) + value)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBundleStatus(arg):
        '''
        ChooseBundleStatus : Choose a bundle deal status as search input
                Input argu :
                    status - all / ongoing / expired / scheduled
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        status = arg["status"]
        ret = 1

        ##Choose bundle status
        xpath = Util.GetXpath({"locate": "status"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bundle status type -> " + status, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.ChooseBundleStatus ->' + status)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchData(arg):
        '''
        InputSearchData : Search bundle deal with bundle id
                Input argu :
                    bundle_id - bundle deal id
                    shop_id - shop id
                    start_time - bundle start time after the given start time
                    end_time - bundle end time before the given end time (important)
                    status - all / ongoing / expired / schedueld
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        bundle_id = arg["bundle_id"]
        shop_id = arg["shop_id"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        status = arg["status"]
        ret = 1

        ##Clean and input shop id box
        if bundle_id:
            AdminBundleDealComponent.InputToColumn({"column_type": "bundle_id", "input_content": bundle_id, "result": "1"})

        ##Clean and input bundle id box
        if shop_id:
            AdminBundleDealComponent.InputToColumn({"column_type": "shop_id", "input_content": shop_id, "result": "1"})

        ##Input start time to filiter bundle deal
        if start_time:
            AdminBundleDealComponent.InputToColumn({"column_type": "start_time", "input_content": start_time, "result": "1"})

        ##Input end time to filiter bundle deal
        if end_time:
            AdminBundleDealComponent.InputToColumn({"column_type": "end_time", "input_content": end_time, "result": "1"})

        ##Input status to filter bundle deal
        if status:
            AdminBundleDealPage.ChooseBundleStatus({"status": status, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.InputSearchData')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBundleOption(arg):
        '''
        ChooseBundleOption : Choose one option from three bundle options
                Input argu :
                    option : x_item_at_Y / x_item_y_off / x_item_y_percent_off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        option = arg["option"]
        ret = 1

        ##Move mouse to the component
        xpath = Util.GetXpath({"locate": "option"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        time.sleep(2)

        ##Choose bundle type option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Choose bundle option -> " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.ChooseBundleOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBundleOptionValue(arg):
        '''
        InputBundleOptionValue : Input bundle option x and y in bundle page
                Input argu :
                    x_value - min number of purchase
                    y_value - eligible discount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        x_value = arg["x_value"]
        y_value = arg["y_value"]

        ##Input x value
        xpath = Util.GetXpath({"locate": "x_value"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": x_value, "result": "1"})

        ##Input y value
        xpath = Util.GetXpath({"locate": "y_value"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": y_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.InputBundleOptionValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBundleRebate(arg):
        '''
        InputBundleRebate : Input bundle rebate in edit page
                Input argu :
                    rebate - rebate price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rebate = arg["rebate"]

        ##Input bundle rebate
        xpath = Util.GetXpath({"locate": "rebate_column"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rebate, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBundleDealPage.InputBundleRebate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPurchaseLimit(arg):
        '''
        InputPurchaseLimit : Input purchase limit in edit page
                Input argu :
                    purchase_limit - The max number of set that one account can buy
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        purchase_limit = arg["purchase_limit"]
        ret = 1

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBundleDealPage.InputPurchaseLimit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetBundleLabel(arg):
        '''
        SetBundleLabel : Choose one country label and input label text in edit page
                Input argu :
                    label_text - Input label text for different countries (optional)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        label_text = arg["label_text"]
        ret = 1

        ##Get case region to decide label_tpye
        label_type = "label_" + Config._TestCaseRegion_.lower()

        ##Input bundle label
        xpath = Util.GetXpath({"locate": label_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Modify label text in existing bundle deal
        if label_text:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": label_text, "result": "1"})

        ##Input label text in new bundle deal
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation test case running", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.SetBundleLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateNewBundleDeal(arg):
        '''
        CreateNewBundleDeal : Create new bundle deal
                Input argu :
                    shop_id - the shop id of items you will upload
                    start_time - start time
                    end_time - end time
                    x_value - quantity of item in a bundle deal
                    y_value - discount price / discount percentage
                    rebate - rebate price
                    option - x_items_y_percent_off / x_items_y_off / x_item_at_Y
                    purchase_limit - purchase limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        shop_id = arg["shop_id"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        x_value = arg["x_value"]
        y_value = arg["y_value"]
        option = arg["option"]
        rebate = arg["rebate"]
        purchase_limit = arg["purchase_limit"]

        ##Get regional time and decide start time and end time
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", minutes=int(end_time))

        ##Click add new button to create new bundle deal
        AdminBundleDealComponent.ClickOnButton({"button_type": "add_new", "result": "1"})

        ##First pahse, input basic info
        ##Input bundle name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "TWQA", "result": "1"})

        ##Input shop id of items prepared to be uploaded
        xpath = Util.GetXpath({"locate": "shop_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        ##Input start time and click ok
        AdminBundleDealPage.SetTimeStamp({"time_type": "start_time", "value": start_time, "result": "1"})

        ##Input end time and click ok
        AdminBundleDealPage.SetTimeStamp({"time_type": "end_time", "value": end_time, "result": "1"})

        ##Click bundle type option
        AdminBundleDealPage.ChooseBundleOption({"option": option, "result": "1"})

        ##Input bundle option value
        AdminBundleDealComponent.InputToColumn({"column_type": "x_value", "input_content": x_value, "result": "1"})
        AdminBundleDealComponent.InputToColumn({"column_type": "y_value", "input_content": y_value, "result": "1"})

        ##Input rebate price
        AdminBundleDealComponent.InputToColumn({"column_type": "rebate", "input_content": rebate, "result": "1"})

        ##Input purchase limit
        AdminBundleDealComponent.InputToColumn({"column_type": "purchase_limit", "input_content": purchase_limit, "result": "1"})

        ##Input bundle label
        AdminBundleDealPage.SetBundleLabel({"label_text": "", "result": "1"})

        ##Click next button
        AdminBundleDealComponent.ClickOnButton({"button_type": "next","result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.CreateNewBundleDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBundleDealCsv(arg):
        '''
        UploadBundleDealCsv : Upload bundle deal csv
                Input argu :
                    file_name : file name (without file extension)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        ret = 1

        ##Upload file
        xpath = Util.GetXpath({"locate": "upload_button"})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result":"1"})

        ##Wait page reload info
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBundleDealPage.UploadBundleDealCsv')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBatchUpdateCsv(arg):
        '''
        UploadBatchUpdateCsv : Upload csv file to batch update bundle deal
                Input argu :
                    file_name - file name (without file extension)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        ret = 1

        ##Click batch update button
        AdminBundleDealComponent.ClickOnButton({"button_type": "batch_update", "result": "1"})

        ##Upload file
        xpath = Util.GetXpath({"locate": "drag_down_area"})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result":"1"})

        ##Click confirm button
        AdminBundleDealComponent.ClickOnButton({"button_type": "confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.UploadBatchUpdateCsv')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyBundleDealIDInCsv(arg):
        '''
        ModifyBundleDealIDInCsv : Write the bundle deal id into csv file
                Input argu :
                    input_row - Input the row number where you want to input the bundle deal id in csv
                    file_name - Input the file name of csv file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        input_row = arg["input_row"]
        file_name = arg["file_name"]
        ret = 1

        ##Get the path of csv file and read the file
        slash = Config.dict_systemslash[Config._platform_]

        ##Get the file name and store in global variable
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

        ##Get the global variable "_FilePath_" and read the csv file into list
        data_list = XtFunc.CsvReader(file_path)

        ##Write the order id into csv file
        bundle_id = GlobalAdapter.PromotionE2EVar._BundleDealIDList_[-1]
        XtFunc.CsvWritter({"input_row": input_row, "input_column": "1", "value": bundle_id, "data_list": data_list, "file_path": file_path, "result": arg['result']})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.ModifyBundleDealIDInCsv -> bundle deal id is ' + bundle_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToBundleDealEditPage(arg):
        '''
        GoToBundleDealEditPage : Go to bundle deal edit page with any status
                Input argu :
                    bundle_id : bundle deal id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        bundle_id = arg["bundle_id"]
        ret = 1

        ##Search bundle
        AdminBundleDealComponent.InputToColumn({"column_type": "bundle_id", "input_content": bundle_id, "result": "1"})
        AdminBundleDealComponent.ClickOnButton({"button_type": "search", "result": "1"})

        ##Go to edit page with any status bundle (ongoing / scheduled / expired)
        xpath = Util.GetXpath({"locate": "all"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action to enter edit page", "result": "1"})
        time.sleep(3)
        AdminBundleDealPage.HandleFailToFetch({"result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.GoToBundleDealEditPage ->' + bundle_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToReservedBundleEditPage(arg):
        '''
        GoToReservedBundleEditPage : Go to reserved bundle deal created by case
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Search the bundle just created
        bundle_id = GlobalAdapter.PromotionE2EVar._BundleDealIDList_.pop()
        AdminBundleDealComponent.InputToColumn({"column_type": "bundle_id", "input_content": bundle_id, "result": "1"})
        AdminBundleDealPage.InputBundleID({"bundle_id": bundle_id, "result": "1"})
        AdminBundleDealComponent.ClickOnButton({"button_type": "search", "result": "1"})

        ##Click edit/view to go to bundle edit page
        xpath = Util.GetXpath({"locate": "all"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action to enter edit page", "result": "1"})
        time.sleep(3)
        AdminBundleDealPage.HandleFailToFetch({"result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.GoToReservedBundleEditPage ->' + bundle_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchInItemPool(arg):
        '''
        SearchInItemPool : Search product by shop id or item id
                Input argu :
                    type - shop_id / item_id
                    value - id number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        value = arg["value"]
        ret = 1

        ##Click search icon to popup small input window
        AdminBundleDealComponent.ClickOnIcon({"icon_type": type + "_search_icon", "result": "1"})

        ##Input shop id / item id in small input window
        xpath = Util.GetXpath({"locate": type + "_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        ##Click search button in small input window
        xpath = Util.GetXpath({"locate": type + "_search_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search icon type -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.SearchInItemPool')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DownloadResultCsv(arg):
        '''
        DownloadResultCsv : Download result csv file to check fail reason
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download result button to download reuslt csv
        xpath = Util.GetXpath({"locate": "download_csv_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download results csv", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBundleDealPage.DownloadResultCsv')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemCheckBox(arg):
        '''
        SelectItemCheckBox : Select specific check box of a given item and click them
                Input argu :
                    item_id - item id in detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        item_id = arg["item_id"]
        ret = 1

        #Click one specific checkbox of a given item id
        xpath = Util.GetXpath({"locate": "check_box"})
        xpath = xpath.replace("id_to_be_replaced", item_id)
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click checkbox -> " + item_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.SelectItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EnableItem(arg):
        '''
        EnableItem : Enable one item in edit page
                Input argu :
                    type - single / all
                    item_id - item id exist in bundle deal item pool overview
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        item_id = arg["item_id"]
        ret = 1

        ##Enable one item in edit page
        if type == "single":
            ##Click one item id and do enable
            AdminBundleDealPage.SelectItemCheckBox({"item_id": item_id, "result": "1"})
            AdminBundleDealComponent.ClickOnButton({"button_type": "enable_single", "result": "1"})

        ##Enable all item in edit page
        elif type == "all":
            ##Click enable all and confirm action
            AdminBundleDealComponent.ClickOnButton({"button_type": "enable_all", "result": "1"})
            AdminBundleDealComponent.ClickOnButton({"button_type": "ok", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.EnableItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DisableItem(arg):
        '''
        DisableItem : Disable one item in edit page
                Input argu :
                    type - single / all
                    item_id - item id exist in bundle deal item pool overview
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        item_id = arg["item_id"]
        ret = 1

        ##Disable one item in edit page
        if type == "single":
            ##Click one item id and do disable
            AdminBundleDealPage.SelectItemCheckBox({"item_id": item_id, "result": "1"})
            AdminBundleDealComponent.ClickOnButton({"button_type": "disable_single", "result": "1"})

        ##Disable all item in edit page
        elif type == "all":
            ##Click disable all and confirm action
            AdminBundleDealComponent.ClickOnButton({"button_type": "disable_all", "result": "1"})
            AdminBundleDealComponent.ClickOnButton({"button_type": "ok", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.DisableItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HandleFailToFetch(arg):
        '''
        HandleFailToFetch : It handle fail to fetch item info like shipping channels when entering edit page or save setting
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button when pop up window with fail message
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": "fail_message", "passok": "0", "result": "1"}):
            AdminBundleDealPageButton.ClickOk({"result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPage.HandleFailToFetch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBundleListOrderSorting(arg):
        '''
        CheckBundleListOrderSorting : Check bundle list order sorting is decreasing
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get low / high bundle id from function GetAndReservedElement
        low_bundle_id = int(GlobalAdapter.PromotionE2EVar._BundleDealIDList_.pop())
        high_bundle_id = int(GlobalAdapter.PromotionE2EVar._BundleDealIDList_.pop())

        ##Check default order of bundle id list is decreasing
        if high_bundle_id < low_bundle_id:
            ret = 0

        OK(ret, int(arg['result']), 'AdminBundleDealPage.CheckBundleListOrderSorting')


class AdminBundleDealComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click button: %s which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column which defined by caller

            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Click on column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s column" % (column_type), "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "message": "Input %s which xpath is %s, input content is %s" % (column_type, xpath, input_content), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        InputToColumn : Click any type of checkbox with well defined locator
            Input argu :
                column_type - type of checkbox which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        ##Click on checkbox
        xpath = Util.GetXpath({"locate": checkbox_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox: %s which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnIcon(arg):
        '''
        InputToColumn : Click any type of icon with well defined locator
            Input argu :
                icon_type - type of icon which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        ##Click on icon
        xpath = Util.GetXpath({"locate": icon_type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click icon: %s which xpath is %s" % (icon_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealComponent.ClickOnIcon')


class AdminBundleDealPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click [Add New] Button to create bundle deal
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUpdateItems(arg):
        '''
        ClickBatchUpdateItems : Click batch update items
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch update button
        xpath = Util.GetXpath({"locate": "batch_update_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click batch upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickBatchUpdateItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplate(arg):
        '''
        ClickDownloadTemplate : Click download template button
                Input argu :
                    type - batch_update (can get template in overview page) / upload (can get template in edit page)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click download button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click download button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickDownloadTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button in overview page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAction(arg):
        '''
        ClickAction : Click edit or view button of bundle deal after searching is done
                Input argu :
                    type - edit(ongoing / schedule) / view(expired)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click edit / view button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button type ->" + type, "result": "1"})
        time.sleep(3)
        AdminBundleDealPage.HandleFailToFetch({"result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadFile(arg):
        '''
        ClickUploadFile : Click upload file button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload file button
        xpath = Util.GetXpath({"locate": "upload_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload file button in bundle deal detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickUploadFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrowBack(arg):
        '''
        ClickArrowBack : Click arrow back button on bundle deal detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click arrow back button bundle deal detail page
        xpath = Util.GetXpath({"locate": "arrow_back"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click arrow back button on bundle deal detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickArrowBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCheckBox(arg):
        '''
        ClickItemCheckBox : Click specific check box of a given item
                Input argu :
                    item_id - item id in detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        item_id = arg["item_id"]
        ret = 1

        #Click one specific checkbox of a given item id
        xpath = Util.GetXpath({"locate": "check_box"})
        xpath = xpath.replace("id_to_be_replaced", item_id)
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click checkbox -> " + item_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAllCheckbox(arg):
        '''
        ClickSelectAllCheckbox : Click select all check box in detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select all check box
        xpath = Util.GetXpath({"locate": "select_all_check_box"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click select all check box on bundle deal detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickSelectAllCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnable(arg):
        '''
        ClickEnable : Click enable button of bundle item in detail page
                Input argu :
                    type - single / all
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click enable (all) button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click enable button on bundle deal detail page ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickEnable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable button of bundle item in detail page
                Input argu :
                    type - single / all
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click disable (all) button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click disable button on bundle deal detail page ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickDisable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicate(arg):
        '''
        ClickDuplicate : Click duplicate buttton
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click duplicate button
        xpath = Util.GetXpath({"locate": "duplicate_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click duplicate button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickDuplicate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewBundleCollection(arg):
        '''
        ClickViewBundleCollection : Click view bundle collection button in bundle deal detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view bundle collection button
        xpath = Util.GetXpath({"locate": "view_bundle_collection_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click view bundle collection button in bundle deal detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickViewBundleCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseRegionalLanguage(arg):
        '''
        ChooseRegionalLanguage : Choose regional language after click view bundle collection when case region is with multiple official language
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Choose language when entering FE
        xpath = Util.GetXpath({"locate":"language_" + Config._TestCaseRegion_.lower()})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click language", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ChooseRegionalLanguage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleOrders(arg):
        '''
        ClickBundleOrders : Click bundle orders button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bundle orders button
        xpath = Util.GetXpath({"locate": "bundle_orders_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click bundle orders button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickBundleOrders')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export button
        xpath = Util.GetXpath({"locate": "export_button"})
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchInItemPool(arg):
        '''
        ClickSearchInItemPool : Click search icon in bundle deal item pool
                Input argu :
                    type - shop_id / item_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click search icon to popup small input window
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click search icon type -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickSearchInItemPool')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNext(arg):
        '''
        ClickNext : Click next button in create bundle deal page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next button
        xpath = Util.GetXpath({"locate": "next_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click next button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickNext')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        '''
        ClickOk : Click ok button
                Input argu :  N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button
        xpath = Util.GetXpath({"locate": "ok_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickOk')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu :  N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate": "cancel_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDone(arg):
        '''
        ClickDone : Click done button
                Input argu :  N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done button
        xpath = Util.GetXpath({"locate": "done_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click done button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button in scheduled bundle edit page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset button
                Input argu :
                    type - overview / shop_id / item_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        ret = 1

        ##Click reset button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBundleDealPageButton.ClickReset')


class AdminNewSellerDiscountPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchSellerDiscount(arg):
        '''
        SearchSellerDiscount : Search on seller discount overview page
            Input argu :
                shop_id - shop id
                seller_id - seller id
                promo_id - promotion id
                source - All / Admin Portal / Seller Center / Live Streaming
            Return code :
                 1 - success
                 0 - fail
                 -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        seller_id = arg["seller_id"]
        promo_id = arg["promo_id"]
        source = arg["source"]

        BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
        time.sleep(5)

        ##Search shop ID
        if shop_id:
            AdminNewSellerDiscountComponent.InputToColumn({"column_type": "search_shop_id", "input_content": shop_id, "result": "1"})

        ##Search seller ID
        if seller_id:
            AdminNewSellerDiscountComponent.InputToColumn({"column_type": "search_seller_id", "input_content": seller_id, "result": "1"})

        ##Search promo ID
        if promo_id:
            AdminNewSellerDiscountComponent.InputToColumn({"column_type": "search_promo_id", "input_content": promo_id, "result": "1"})

        ##Search Source
        if source:
            AdminNewSellerDiscountComponent.ClickOnSection({"section_type": "source", "source": source, "result": "1"})

        ##Click search button
        AdminNewSellerDiscountComponent.ClickOnButton({"button_type": "overview_page_search", "promo_name":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountPage.SearchSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateSellerDiscount(arg):
        '''
        CreateSellerDiscount : Create seller discount
                Input argu :
                    shop_id - shop ID
                    start_time - the time of start
                    end_time - the time of end
                    promo_name - the name of promotion
                    file_name - promo csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        promo_name = arg["promo_name"]
        file_name = arg["file_name"]

        ##Click create btn
        AdminNewSellerDiscountComponent.ClickOnButton({"button_type": "overview_page_new_promotion", "promo_name":"", "result": "1"})
        time.sleep(2)

        ##Input promo name
        AdminNewSellerDiscountComponent.InputToColumn({"column_type": "create_promo_name", "input_content": promo_name, "result": "1"})

        ##Input shop ID
        AdminNewSellerDiscountComponent.InputToColumn({"column_type": "create_shop_id", "input_content": shop_id, "result": "1"})

        ##Input start time and end time
        AdminNewSellerDiscountPage.InputPromoTime({"start_time": start_time, "end_time": end_time, "result": "1"})
        time.sleep(3)

        ##Click save and continue
        AdminNewSellerDiscountComponent.ClickOnButton({"button_type": "save", "promo_name":"", "result": "1"})

        ##Click batch setting
        AdminNewSellerDiscountComponent.ClickOnButton({"button_type": "batch_setting", "promo_name":"", "result": "1"})
        time.sleep(2)

        ## Upload seller discount csv file
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result":"1"})

        ##Click update btn
        AdminNewSellerDiscountComponent.ClickOnButton({"button_type": "details_page_update_file", "promo_name":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountPage.CreateSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoTime(arg):
        '''
        InputPromoTime : Input promo time
                Input argu :
                    start_time - the time of start
                    end_time - the time of end
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time with calculate value by start time
        if start_time:
            start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(start_time), 0, is_tw_time=1)
            xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time field", "result": "1"})

            popup_field_xpath = Util.GetXpath({"locate": "start_time_field"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": start_time_string, "message": "Input start time", "result": "1"})

            ##send selenium key event
            BaseUICore.SendSeleniumKeyEvent({"action_type":"enter", "string": "", "result": "1"})

        ##Input end time with calculate value by end time
        if end_time:
            end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0, is_tw_time=1)
            xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time field", "result": "1"})

            popup_field_xpath = Util.GetXpath({"locate": "end_time_field"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":popup_field_xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": popup_field_xpath, "string": end_time_string, "message": "Input end time", "result": "1"})

            ##send selenium key event
            BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountPage.InputPromoTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSellerDiscountFile(arg):
        '''
        UploadSellerDiscountFile : Upload seller discount file
                Input argu :
                    type - upload / remove
                    file_name - promo file name
                    file_type - promo file type
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click batch setting
        AdminNewSellerDiscountComponent.ClickOnButton({"button_type": "batch_setting", "promo_name":"", "result": "1"})
        time.sleep(5)

        if type == "upload":
            xpath = Util.GetXpath({"locate": "upload_btn"})
            BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name":file_name, "file_type":file_type, "result":"1"})

        elif type == "remove":
            xpath = Util.GetXpath({"locate": "remove_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click remove button", "result": "1"})
            XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountPage.UploadSellerDiscountFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDisplayAmount(arg):
        '''
        ChooseDisplayAmount : Choose how many data display in seller discount page
                Input argu :
                    displayed_data_amount - 25 / 50 / 100 / 150 / 200
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        displayed_data_amount = arg["displayed_data_amount"]

        ##Click amount drop down list
        xpath = Util.GetXpath({"locate": "drop_down_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})
        time.sleep(3)

        ##Click user scope label
        xpath = Util.GetXpath({"locate": "displayed_option"})
        xpath = xpath.replace("data_amount_to_be_replaced", displayed_data_amount)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click displayed data amount option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountPage.ChooseDisplayAmount')


class AdminNewSellerDiscountComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
                Input argu :
                    button_type - type of button which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        promo_name = arg["promo_name"]

        xpath = Util.GetXpath({"locate": button_type})

        if button_type in ("overview_page_edit", "overview_page_end", "overview_page_view_details"):
            xpath = xpath.replace("promo_name", promo_name)

        ##Click on button
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
            Input argu :
                checkbox_type - details_page_all_product / details_page_option_product
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]

        xpath = Util.GetXpath({"locate": checkbox_type})

        if checkbox_type in ("details_page_option_product"):
            model_id = arg["model_id"]
            xpath = xpath.replace("model_id_to_be_replaced", model_id)

        ##Click on checkbox
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NavigateToPage(arg):
        '''
        NavigateToPage : Navigate to a page based on page_type
            Input argu :
                page_type - next / prev / page_number
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        page_type = arg['page_type']

        xpath = Util.GetXpath({"locate": page_type})

        if page_type not in ("next", "prev"):
            xpath = Util.GetXpath({"locate": "page_number"})
            xpath = xpath.replace("page_to_be_replaced", page_type)

        ##Click and navigate to page
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Navigate to Page: %s, which xpath is %s" % (page_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountComponent.NavigateToPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of column based on arguments
            Input argu :
                column_type - create_promo_name / create_shop_id / search_shop_id / search_seller_id / search_promo_id / search_model_id / search_product_id
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s column" % (column_type), "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSection(arg):
        '''
        ClickOnSection : Click any type of section with well defined locator
            Input argu :
                section_type - source (All / Admin / Seller Center / Live Streaming)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]
        source = arg["source"]

        ##Select source type
        xpath = Util.GetXpath({"locate": "search_drop_down_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list", "result": "1"})
        time.sleep(5)

        xpath = Util.GetXpath({"locate": "source"})
        xpath = xpath.replace("source_to_be_replaced", source)

        ##Click on section
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click section: %s, which xpath is %s" % (section_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountComponent.ClickOnSection')


class AdminFlashSalePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPromoID(arg):
        '''
        CheckPromoID : Check promotion ID display in Flash Sale Overview list
                Input argu :
                    promotion_name - promotion name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        promotion_name = arg['promotion_name']

        ##Get promotion id from GlobalAdapter.PromotionE2EVar
        upcoming_promo_id = int(GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_[-1]["promotion_id"])

        ##Get promo id displayed content in list
        xpath = Util.GetXpath({"locate":"upcoming_promo_id"}).replace('promotion_name_to_be_replace', promotion_name)
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Check promo id displayed
        if int(GlobalAdapter.CommonVar._PageAttributes_) == upcoming_promo_id:
            dumplogger.info("Promo ID display correctly")
            ret = 1
        else:
            dumplogger.info("Promo ID display incorrectly")
            ret = 0

        OK(ret, int(arg['result']), 'AdminFlashSalePage.CheckPromoID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSessionID(arg):
        '''
        CheckSessionID : Check CFS Session ID display in Flash Sale Overview list
                Input argu :
                    promotion_name - promotion name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_name = arg['promotion_name']

        ##Get session id from GlobalAdapter.PromotionE2EVar
        upcoming_session_id = int(GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_[-1]["session_id"])
        dumplogger.info("Upcoming flash sale cfs session id: %s" % (upcoming_session_id))

        ##Check session id displayed
        xpath = Util.GetXpath({"locate":"upcoming_session_id"}).replace('promotion_name_to_be_replace', promotion_name)
        xpath = xpath.replace('session_id_to_be_replace', str(upcoming_session_id))
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.CheckSessionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSessionStatusDisplay(arg):
        '''
        CheckSessionStatusDisplay : Check flash sale status display in Flash Sale Overview list
                Input argu :
                    promotion_name - promotion name
                    status - upcoming / ongoing / end
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg['status']
        promotion_name = arg['promotion_name']

        ##Using passed in expected status decide session type and its style color
        dumplogger.info("Session type is %s" % (status))
        xpath = Util.GetXpath({"locate":status}).replace('promotion_name_to_be_replace', promotion_name)

        ##Check session status display
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.CheckSessionStatusDisplay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToBannerSetIcon(arg):
        '''
        MoveToBannerSetIcon : Hover over banner set "i" icon button and check popup message
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Hover on element
        XtFunc.MouseAction("click", 1860, 951, 1)
        time.sleep(3)
        xpath = Util.GetXpath({"locate":"banner_set_icon_button"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.MoveToBannerSetIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPromotionRefreshPopupMessage(arg):
        '''
        CheckPromotionRefreshPopupMessage : Check promotion refresh result popup message
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get promotion id from GlobalAdapter.PromotionE2EVar
        refresh_promo_id = int(GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_[-1]["promotion_id"])
        dumplogger.info("Refresh flash sale promotion id: %s" % (refresh_promo_id))

        ##Check refresh popup message
        xpath = Util.GetXpath({"locate": "refresh_popup_message"})
        xpath = xpath.replace('promotion_id_to_be_replace', str(refresh_promo_id))
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.CheckPromotionRefreshPopupMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetSpecificCustomizedColumns(arg):
        '''
        SetSpecificCustomizedColumns : Select specific customized column and save it
                Input argu :
                    select_item - columns item name in customized columns list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        select_item = arg['select_item']

        ##Click manage columns
        xpath = Util.GetXpath({"locate": "manage_columns"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click manage_columns in product level setting tabs", "result": "1"})

        ##Choose column item in customized columns list
        xpath = Util.GetXpath({"locate": "columns_item"}).replace('customize_item_to_be_replace', select_item)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ${select_item} in customized columns list", "result": "1"})

        ##Click save in customized columns popup windows
        xpath = Util.GetXpath({"locate": "save_customize"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save in customized columns popup windows", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.SetSpecificCustomizedColumns')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAllCustomizedColumns(arg):
        '''
        SetAllCustomizedColumns : Select all customized column and save it
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click manage columns
        xpath = Util.GetXpath({"locate": "manage_columns"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click manage_columns in product level setting tabs", "result": "1"})

        ##Get number of available customized columns
        xpath = Util.GetXpath({"locate":"optional_columns"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"multi", "result": "1"})
        optional_columns_numbers = len(GlobalAdapter.CommonVar._PageElements_)
        dumplogger.info("Display optional columns item number : %s" % (optional_columns_numbers))

        ##Select all available customized columns
        for pick_times in range(len(GlobalAdapter.CommonVar._PageElements_)):
            dumplogger.info("pick_times : %s" % (pick_times))
            xpath = Util.GetXpath({"locate": "optional_item"}).replace('serial_to_be_replace', str((pick_times + 1)))
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click optional columns", "result": "1"})

        ##Click save in customized columns popup windows
        xpath = Util.GetXpath({"locate": "save_customize"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save in customized columns popup windows", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.SetAllCustomizedColumns')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveItemIntoManualConfiguration(arg):
        '''
        MoveItemIntoManualConfiguration : Choose item from rcmd list and move it into manual configuration
                Input argu :
                    moving_scope - all / specific
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        moving_scope = arg['moving_scope']

        ##Using moving_scope to decide choose item scope
        xpath = Util.GetXpath({"locate": moving_scope})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + moving_scope + " item in list of order by rcmd tab", "result": "1"})

        ##Click move to manual configuration
        xpath = Util.GetXpath({"locate": "move_to_manual_configuration"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click move to manual configuration", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.MoveItemIntoManualConfiguration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveItemIntoRCMD(arg):
        '''
        MoveItemIntoRCMD : Choose item from manual configuration and move it into rcmd
                Input argu :
                    moving_scope - all / specific
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        moving_scope = arg['moving_scope']

        ##Using moving_scope to decide choose item scope
        xpath = Util.GetXpath({"locate": moving_scope})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + moving_scope + " item in list of order by manual configuration", "result": "1"})

        ##Click move to order by rcmd
        xpath = Util.GetXpath({"locate": "move_to_rcmd"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click move to order by rcmd", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.MoveItemIntoRCMD')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UpdateProductDisplayName(arg):
        '''
        UpdateProductDisplayName : Edit product display name current content and save it
                Input argu :
                    modify_content - modify content wanna increase
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        modify_content = arg['modify_content']

        ##Click product display name field and input modify content
        xpath = Util.GetXpath({"locate": "product_display_name"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click name field", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(2)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": modify_content, "result": "1"})
        time.sleep(2)

        ##Call windows keyboard event and press enter
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.UpdateProductDisplayName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeOverlayImage(arg):
        '''
        ChangeOverlayImage : Click overlay image drop down list and change selected option
                Input argu :
                    product_name - product name
                    overlay_image_name - overlay image option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]
        overlay_image_name = arg["overlay_image_name"]

        ##Click overlay image selection
        xpath = Util.GetXpath({"locate": "overlay_image_selection"}).replace('product_name_to_be_replace', product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click overlay image selection list", "result": "1"})

        ##Click overlay image option item
        xpath = Util.GetXpath({"locate": "overlay_image"}).replace('image_name_to_be_replace', overlay_image_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Selecte overlay image option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ChangeOverlayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeFlashSalesCategory(arg):
        '''
        ChangeFlashSalesCategory : Click overlay image drop down list and change selected option
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click flash sale category selection
        xpath = Util.GetXpath({"locate": "flash_sale_caregory_selection"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click flash sale caregory selection and change selected option", "result": "1"})

        ##Click flash sale category option item
        xpath = Util.GetXpath({"locate": "new_selected_option"}).replace('category_name_to_be_replace', category_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Selected another option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ChangeFlashSalesCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckApprovedItemDisplayOrder(arg):
        '''
        CheckApprovedItemDisplayOrder : Check arrpoved item display order in order by manual configuration
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Checking approved item lists display order in order by manual configuration
        for order in range(10):
            dumplogger.info("Current order number : %s" % (order))
            xpath = Util.GetXpath({"locate": "approved_item_name"}).replace('_order_to_be_replace', str((order + 1)))
            BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.CheckApprovedItemDisplayOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeItemOrder(arg):
        '''
        ChangeItemOrder : move element to change order
                Input argu :
                    drag_item_id - id of the drag item
                    target_item_id - id of the target item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drag_item_id = arg["drag_item_id"]
        target_item_id = arg["target_item_id"]

        ##move element to change order
        drag_elem = Util.GetXpath({"locate": "drag_elem"})
        drag_elem = drag_elem.replace('id_to_be_replaced', drag_item_id)
        target_elem = Util.GetXpath({"locate": "drop_elem"})
        target_elem = target_elem.replace('id_to_be_replaced', target_item_id)

        ##Drag and drop element
        BaseUICore.DragAndDrop({"drag_elem": drag_elem, "target_elem": target_elem, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ChangeItemOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InsertDisplayOrder(arg):
        '''
        InsertDisplayOrder : Insert display order value for specific product item
                Input argu :
                    product_name - product name
                    display_order_value - product name display order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']
        display_order_value = arg['display_order_value']

        ##Insert display order value into specific product item
        xpath = Util.GetXpath({"locate": "display_order_field"}).replace('product_name_to_be_replace', product_name)
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": display_order_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.InsertDisplayOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCarouselBanner(arg):
        '''
        SetCarouselBanner : Setting carousel banner image and url in flash sale landing page
                Input argu :
                    platform_type - PC / App
                    image_file_name - upload image file name
                    image_type - jpg / gif
                    need_insert_url - 1 / 0
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        platform_type = arg["platform_type"]
        image_file_name = arg["image_file_name"]
        image_type = arg["image_type"]
        need_insert_url = arg["need_insert_url"]

        ##Click upload button and choose image
        xpath = Util.GetXpath({"locate": "upload_image_btn"}).replace('platform_type_to_be_replace', platform_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner upload button", "result": "1"})
        time.sleep(1)
        XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": image_file_name, "file_type": image_type, "result": "1"})

        if need_insert_url:
            ##Insert landing page url
            xpath = Util.GetXpath({"locate": "langing_page_url"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "https://www.google.com.tw/", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.SetCarouselBanner')


class AdminFlashSalePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleEdit(arg):
        '''
        ClickFlashSaleEdit : Click flash sale edit button
                Input argu :
                    promo_name - promotion name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promo_name = arg['promo_name']

        ##Click promotion edit button
        xpath = Util.GetXpath({"locate":"promotion_edit_button"}).replace('promo_name_to_be_replace', promo_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion edit button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickFlashSaleEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductLevelSetting(arg):
        '''
        ClickProductLevelSetting : Click product level setting button in edit promotion
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product level setting
        xpath = Util.GetXpath({"locate":"product_level_setting"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product level setting", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickProductLevelSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionRefresh(arg):
        '''
        ClickPromotionRefresh : Click promotion refresh button in flash sale overview list
                Input argu :
                    promotion_name - promotion name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_name = arg['promotion_name']

        ##Click promotion refresh button
        xpath = Util.GetXpath({"locate":"promotion_refresh_button"}).replace('promotion_name_to_be_replace', promotion_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion refresh button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickPromotionRefresh')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductLevelSettingSubTabs(arg):
        '''
        ClickProductLevelSettingSubTabs : Click product level setting sub tabs in promotion edit page
                Input argu :
                    sub_tabs - order_by_rcmd / order_by_manual_confirguation
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sub_tabs = arg['sub_tabs']

        ##Click sub tabs in product level setting
        xpath = Util.GetXpath({"locate": sub_tabs})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ${sub_tabs} in edit promotion page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickProductLevelSettingSubTabs')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassUpload(arg):
        '''
        ClickMassUpload : Click mass upload in edit promotion product level setting tabs
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mass upload button in product level setting tabs
        xpath = Util.GetXpath({"locate": "mass_upload_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass upload button in product level setting tabs", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickMassUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionLevelSetting(arg):
        '''
        ClickPromotionLevelSetting : Click promotion level settings button in edit promotion
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click promotion level settings
        xpath = Util.GetXpath({"locate":"promotion_level_settings"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion level settings", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickPromotionLevelSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
        ClickUpload : Click upload button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_image_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportList(arg):
        '''
        ClickExportList : Click export list btn
                Input argu :
                    split_type - item / model
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        split_type = arg['split_type']

        ##Click export list btn
        xpath = Util.GetXpath({"locate": "export_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export list btn", "result": "1"})
        time.sleep(3)

        ##Click split type
        xpath = Util.GetXpath({"locate": split_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click split type btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickExportList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeOrder(arg):
        '''
        ClickChangeOrder : Click change order button in order by manual configuration tabs
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change order button
        xpath = Util.GetXpath({"locate": "change_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change order button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickChangeOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : click save and continue button in edit promotion page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save and continue button in edit promotion page
        xpath = Util.GetXpath({"locate": "save_and_continue"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save and continue button in edit promotion page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrowBack(arg):
        '''
        ClickArrowBack : Click arrow back btn on product level setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click arrow back btn on product level setting page
        xpath = Util.GetXpath({"locate": "arrow_back"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click arrow back btn on product level setting page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickArrowBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchSettingUpload(arg):
        '''
        ClickBatchSettingUpload : Click upload button in batch setting popup windows
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload icon button in batch setting popup windows
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload icon button in batch setting popup windows", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickBatchSettingUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        '''
        ClickUpdate : Click Update btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click update btn
        xpath = Util.GetXpath({"locate": "update_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update btn", "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel order button to cancel all setting about display order
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel order button
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel order button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickManageColumns(arg):
        '''
        ClickManageColumns : Click manage columns button in edit promotion page to setting customized columns
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click manage columns button
        xpath = Util.GetXpath({"locate": "manage_columns_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click manage columns button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ClickManageColumns')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveDisplayOrderSetting(arg):
        '''
        SaveDisplayOrderSetting : Click Save order button to save all setting about display order
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save order button
        xpath = Util.GetXpath({"locate": "save_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save order button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.SaveDisplayOrderSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetProductPromoPhoto(arg):
        '''
        ResetProductPromoPhoto : Click trash icon to reset product promo photo
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save order button
        xpath = Util.GetXpath({"locate": "reset_promo_photo"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash icon to reset product promo photo", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePageButton.ResetProductPromoPhoto')


class AdminInShopFlashSaleOverviewPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchByInputField(arg):
        '''
        SearchByInputField : Input promotion search field on ISFS overview page
                Input argu :
                    isfs_id - isfs promotion id
                    shop_id - shop id
                    product_id - product id
                    model_id - model id
                    start_time - start time("%Y-%m-%d %H:%M:%S)
                    end_time - end time("%Y-%m-%d %H:%M:%S)
                    status - all / upcoming / ongoing / ended
                    show_inshop - all / enable / disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        isfs_id = arg["isfs_id"]
        shop_id = arg["shop_id"]
        product_id = arg["product_id"]
        model_id = arg["model_id"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        status = arg["status"]
        show_inshop = arg["show_inshop"]

        ##Input promotion searching field on the overview page
        ##Search by isfs promotion id
        if isfs_id:
            AdminNewPromotionComponent.InputToColumn({"column_type": "isfs_id_field", "input_content": isfs_id, "result": "1"})
            time.sleep(2)

        ##Search by shop id
        if shop_id:
            AdminNewPromotionComponent.InputToColumn({"column_type": "isfs_shop_id_field", "input_content": shop_id, "result": "1"})
            time.sleep(2)

        ##Search by product id
        if product_id:
            AdminNewPromotionComponent.InputToColumn({"column_type": "isfs_product_id_field", "input_content": product_id, "result": "1"})
            time.sleep(2)

        ##Search by model id
        if model_id:
            AdminNewPromotionComponent.InputToColumn({"column_type": "isfs_model_id_field", "input_content": model_id, "result": "1"})
            time.sleep(2)

        ##Search by start time
        if start_time:
            ##Click start time column first to prepare to input value
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_start_time_field", "result": "1"})
            time.sleep(2)

            ##Input start time
            AdminNewPromotionComponent.InputToColumn({"column_type": "isfs_start_time_field", "input_content": start_time, "result": "1"})
            time.sleep(2)

            ##Click start_time calendar ok
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_start_time_ok_btn", "result": "1"})
            time.sleep(2)

        ##Search by end time
        if end_time:
            ##Click end time column first to prepare to input value
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_end_time_field", "result": "1"})
            time.sleep(2)

            ##Input end time
            AdminNewPromotionComponent.InputToColumn({"column_type": "isfs_end_time_field", "input_content": end_time, "result": "1"})
            time.sleep(2)

            ##Click end time calendar ok
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_end_time_ok_btn", "result": "1"})
            time.sleep(2)

        ##Search by status
        if status:
            ##Click to display status option
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_status_drawer", "result": "1"})
            time.sleep(2)

            ##Click to choose status option
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_status_"+status, "result": "1"})
            time.sleep(2)

        ##Search by show in shop
        if show_inshop:
            ##Click to display show in shop option
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_show_inshop_drawer", "result": "1"})
            time.sleep(2)

            ##Click to choose show in shop option
            AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_show_inshop_"+show_inshop, "result": "1"})
            time.sleep(2)

        AdminNewPromotionComponent.ClickOnButton({"button_type": "isfs_search_btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminInShopFlashSaleOverviewPage.SearchByInputField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadISFSCSV(arg):
        '''
        UploadISFSCSV - Upload category csv
                Input argu :
                    upload_type - upload / reject
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]

        ##Upload file
        xpath = Util.GetXpath({"locate": upload_type})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminInShopFlashSaleOverviewPage.UploadISFSCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToViewPromotion(arg):
        '''
        GoToViewPromotion : Go to view promotion page
                Input argu :
                    row - which row to enter to in isfs promotion table
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row = arg["row"]

        ##Click 'view' btn on the row
        xpath = Util.GetXpath({"locate": "isfs_view_btn"})
        xpath = xpath.replace('row_to_be_replaced', row)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminInShopFlashSaleOverviewPage.GoToViewPromotion')


class AdminCriteriaSettingsOverviewPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToCriteriaEditPage(arg):
        '''
        GoToCriteriaEditPage : Go to criteria edit page
                Input argu :
                    criteria_type - shop / product
                    row - which row to click to enter to criteria edit page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        criteria_type = arg["criteria_type"]
        row = arg["row"]

        ##Click to shop/product sub page
        AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_"+criteria_type, "result": "1"})
        time.sleep(3)

        ##Click 'edit' btn on the row
        xpath = Util.GetXpath({"locate": criteria_type+"_edit_btn"})
        xpath = xpath.replace('row_to_be_replaced', row)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCriteriaSettingsOverviewPage.GoToCriteriaEditPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveCriteria(arg):
        '''
        RemoveCriteria : remove criteria
                Input argu :
                    criteria_type - shop / product
                    row - which row to click to remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        criteria_type = arg["criteria_type"]
        row = arg["row"]

        ##Click to shop/product sub page
        AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_"+criteria_type, "result": "1"})
        time.sleep(3)

        ##Click 'remove' btn on the row
        xpath = Util.GetXpath({"locate": criteria_type+"_remove_btn"})
        xpath = xpath.replace('row_to_be_replaced', row)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove button", "result": "1"})
        time.sleep(3)

        ##Click yes to Confirm
        AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_yes_btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCriteriaSettingsOverviewPage.RemoveCriteria')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetCriteriaSettingsValue(arg):
        '''
        SetCriteriaSettingsValue : Set value in 'Criteria Settings' section
            Input argu :
                criteria_field - field to set in criteria setting page
                criteria_value - value to input field
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        criteria_field = arg["criteria_field"]
        criteria_value = arg["criteria_value"]

        ##Formats of 'activeness' & 'product_saet_per_shop_per_session' field are different than others
        if criteria_field == "activeness":
            ##Click activeness option drawer
            AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_activeness_selector", "result": "1"})
            time.sleep(2)

            ##Click activeness option
            AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_activeness_"+criteria_value+"_days", "result": "1"})
            time.sleep(2)

        elif criteria_field == "product_saet_per_shop_per_session":
            ##Input value
            AdminNewPromotionComponent.InputToColumn({"column_type": "criteria_settings_overview_"+criteria_field+"_field", "input_content": criteria_value, "result": "1"})
            time.sleep(2)

        else:
            #Click 'no limit' btn
            if criteria_value == 'no limit':
                ##Click no limit in criteria setting
                AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_"+criteria_field+"_nolimit_btn", "result": "1"})
                time.sleep(2)

            ##Needs to set input value
            else:
                ##there are no input fielda and max/min btn with title 'pre-order'
                if criteria_field == 'preorder':
                    ##Click the other option(must not be pre-order)
                    AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_must_not_preorder_btn", "result": "1"})
                    time.sleep(2)

                else:
                    ##Click maximum/minimum btn first to enable input field
                    AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_"+criteria_field+"_max_min_btn", "result": "1"})
                    time.sleep(2)

                    ##there are no input fields with title 'on_shopee_days'
                    if criteria_field == 'on_shopee_days':
                        ##Click option drawer
                        AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_on_shopee_days_selector", "result": "1"})
                        time.sleep(2)

                        ##Click option
                        AdminNewPromotionComponent.ClickOnButton({"button_type": "criteria_settings_overview_on_shopee_days_"+criteria_value, "result": "1"})
                        time.sleep(2)

                    else:
                        ##Input value
                        AdminNewPromotionComponent.InputToColumn({"column_type": "criteria_settings_overview_"+criteria_field+"_field", "input_content": criteria_value, "result": "1"})
                        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminCriteriaSettingsOverviewPage.SetCriteriaSettingsValue')


class AdminSpecialShopListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCategoryCSV(arg):
        '''
        UploadCategoryCSV : Upload category csv
                Input argu :
                    upload_type - add / remove
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]

        ##Upload file
        xpath = Util.GetXpath({"locate": upload_type})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminSpecialShopListPage.UploadCategoryCSV')


class AdminSpecialShopListComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminSpecialShopListComponent.ClickOnButton')


class AdminProductCategoryBlacklistPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCategoryCSV(arg):
        '''
        UploadCategoryCSV : Upload category csv
                Input argu :
                    upload_type - add / remove
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]

        ##Upload file
        xpath = Util.GetXpath({"locate": upload_type})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": "file", "file_name": file_name, "file_type": "csv", "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCategoryBlacklistPage.UploadCategoryCSV')


class AdminProductCategoryBlacklistComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCategoryBlacklistComponent.ClickOnButton')


class AdminMallFlashSalePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImage(arg):
        '''
        UploadImage : Upload image
                Input argu :
                    image_type - banner / image
                    file_name - file name
                    file_type - png / jpg / gif
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        image_type = arg["image_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        ret = 1

        ##Upload image
        xpath = Util.GetXpath({"locate": image_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload button", "result": "1"})
        XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminMallFlashSalePage.UploadImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeOrder(arg):
        '''
        ChangeOrder : move element to change order
                Input argu :
                    drag_id - id of the drag item
                    target_id - id of the target item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        drag_id = arg["drag_id"]
        target_id = arg["target_id"]

        ##move element to change order
        drag_elem = Util.GetXpath({"locate": "drag_elem"})
        drag_elem = drag_elem.replace('id_to_be_replaced', drag_id)
        target_elem = Util.GetXpath({"locate": "drop_elem"})
        target_elem = target_elem.replace('id_to_be_replaced', target_id)

        ##Drag and drop element
        BaseUICore.DragAndDrop({"drag_elem": drag_elem, "target_elem": target_elem, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMallFlashSalePage.ChangeOrder')


class AdminFreeShippingVoucherComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
                Input argu :
                    button_type - type of button which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of column based on arguments
                Input argu :
                    column_type - type of column
                    input_content - input value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s column" % (column_type), "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherComponent.InputToColumn')


class AdminFreeShippingVoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeriod(arg):
        '''
        AssignTimePeriod : Assign Time Period
                Input argu :
                    column_type - start_date, claim_start_date, end_date, claim_end_date
                    minutes - the time that need to adjust from now
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        minutes = arg["minutes"]

        time_stamp_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(minutes), 0)
        ##Locate the column that need to assign time period and delete data first
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)
        ##Input time
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s column" % (column_type), "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.AssignTimePeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetVoucherPromotionID(arg):
        '''
        GetVoucherPromotionID : Get voucher promotion ID and store to global variable
                Input argu :
                    voucher_type - shopee_voucher, free_shipping_voucher
                    voucher_name - voucher name you want to search
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]
        voucher_name = arg["voucher_name"]

        ##Input voucher name
        xpath = Util.GetXpath({"locate": "voucher_name_search_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Get voucher promotion ID and store to global variable
        xpath = Util.GetXpath({"locate": "promotion_id_text"})
        xpath_promotion_id_text = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath_promotion_id_text, "reservetype": voucher_type + "_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.GetVoucherPromotionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadUserIds(arg):
        '''
        UploadUserIds : Upload user id to add or remove user id
                Input argu :
                    file_name - file name that user need to upload
                    method - add, remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        file_name = arg["file_name"]
        method = arg["method"]

        ret = 1

        ##Click upload button
        xpath = Util.GetXpath({"locate": method})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click a upload button", "result": "1"})

        ##Click upload box
        xpath = Util.GetXpath({"locate": method + "_upload_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload box", "result": "1"})

        ##Upload icon
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        ##Click confirm
        xpath = Util.GetXpath({"locate": method + "_confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload box", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.UploadUserIds')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DispatchVoucherToAllUsers(arg):
        '''
        DispatchVoucherToAllUsers : Dispatch voucher to all users
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        ##Click all users
        xpath = Util.GetXpath({"locate": "all_users"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click all users tab", "result": "1"})

        ##Click proceed to review button
        xpath = Util.GetXpath({"locate": "proceed_to_review_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click proceed to review button", "result": "1"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": "popup_box", "passok": "0", "result": "1"})

        ##Click dispatch button
        xpath = Util.GetXpath({"locate": "dispatch_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dispatch button", "result": "1"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": "dispatch_again_btn", "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.DispatchVoucherToAllUsers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeQuotaType(arg):
        '''
        ChangeQuotaType : Change Quota Type
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click quota side bar
        xpath = Util.GetXpath({"locate": "quota"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click quota side bar", "result": "1"})
        time.sleep(3)

        ##Change quota type
        xpath = Util.GetXpath({"locate": "quota_type"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click quota type button", "result": "1"})
        time.sleep(3)
        ##Check change success
        xpath = Util.GetXpath({"locate": "claim_dispatch"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Change quota type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.ChangeQuotaType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCriteriaOption(arg):
        '''
        SelectCriteriaOption : Select criteria option
                Input argu :
                    criteria_type - registration_time, order_live, order_cancelled, phone_number, user_tag
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        criteria_type = arg["criteria_type"]

        ##Click usage rule side bar
        xpath = Util.GetXpath({"locate": "usage_rule"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click usage rule side bar", "result": "1"})
        time.sleep(3)

        ##Click criteria option
        xpath = Util.GetXpath({"locate": "user_scope_criteria"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user scope criteria option", "result": "1"})

        ##Select criteria type
        xpath = Util.GetXpath({"locate": criteria_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select criteria type success!", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.SelectCriteriaOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditErrorMessage(arg):
        '''
        EditErrorMessage : Edit error title and content
                Input argu :
                    title - error message's title
                    content - error message's content
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        content = arg["content"]

        ##Click custom error message
        xpath = Util.GetXpath({"locate": "custom_msg"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom msg button", "result": "1"})

        ##Input title(ID)
        xpath = Util.GetXpath({"locate": "title_ID"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click title field", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Scroll down
        xpath = Util.GetXpath({"locate": "scroll_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Scroll down success", "result": "-1"})

        ##Input Content(ID)
        xpath = Util.GetXpath({"locate": "content_ID"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click content field", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.EditErrorMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditUserLink(arg):
        '''
        EditUserLink : Edit user link
                Input argu :
                    link - the link that 'learn more' will direct to
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        link = arg["link"]

        ##Click edit error message
        xpath = Util.GetXpath({"locate": "custom_link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom msg button", "result": "1"})

        ##Input link
        xpath = Util.GetXpath({"locate": "link_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click link field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.EditUserLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectSpecificPayment(arg):
        '''
        SelectSpecificPayment : Select specific payment that voucher can only use
                Input argu :
                    payment_type  - credit_card
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        payment_type = arg["payment_type"]

        ##Click usage rule side-bar
        xpath = Util.GetXpath({"locate": "usage_rule"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click usage rule side bar", "result": "1"})
        time.sleep(3)

        ##Click specific payment option
        xpath = Util.GetXpath({"locate": "specific_payment"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click specific payment option", "result": "1"})

        if payment_type == "credit_card":
            ##Select credit card option
            xpath = Util.GetXpath({"locate": "credit_card"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select credit card option", "result": "1"})
            ##Select without install option
            xpath = Util.GetXpath({"locate": "without_install"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select without install option", "result": "1"})
            ##Select with install option
            xpath = Util.GetXpath({"locate": "with_install"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select with install option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.SelectSpecificPayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadLogisticPromotionRule(arg):
        '''
        UploadLogisticPromotionRule : Upload Logistic Promotion Rule
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        file_name = arg["file_name"]
        ret = 1

        ##Click usage rule side-bar
        xpath = Util.GetXpath({"locate": "usage_rule"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click usage rule side bar", "result": "1"})
        time.sleep(3)

        ##Click specific logic option
        xpath = Util.GetXpath({"locate": "specific_logic"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click specific logic option", "result": "1"})
        time.sleep(3)

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_file"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload button", "result": "1"})
        time.sleep(3)

        ##Click upload box
        xpath = Util.GetXpath({"locate": "upload_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload box", "result": "1"})

        ##Upload csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        ##Click confirm
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click confirm button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.UploadLogisticPromotionRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditFEDisplay(arg):
        '''
        EditFEDisplay : Edit FE Display
                Input argu :
                    link - the link that 'use' will direct to
                    header - voucher header
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        link = arg["link"]
        header = arg["header"]

        ##Click FE display side-bar
        xpath = Util.GetXpath({"locate": "fe_display"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click FE display side bar", "result": "1"})
        time.sleep(3)

        ##Click custom link option
        xpath = Util.GetXpath({"locate": "custom_link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom link option", "result": "1"})

        ##Input link
        xpath = Util.GetXpath({"locate": "link_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click link field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link, "result": "1"})

        ##Click custom header
        xpath = Util.GetXpath({"locate": "custom_header"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom header option", "result": "1"})

        ##Input header
        xpath = Util.GetXpath({"locate": "header_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click header field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": header, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.EditFEDisplay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditProductTag(arg):
        '''
        EditProductTag : Edit Product Tag
                Input argu :
                    image_id - the image had been upload to promtoion admin
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        image_id = arg["image_id"]

        ##Click FE display side-bar
        xpath = Util.GetXpath({"locate": "fe_display"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click FE display side bar", "result": "1"})
        time.sleep(3)

        ##Click custom tag option
        xpath = Util.GetXpath({"locate": "custom_tag"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click custom tag option", "result": "1"})

        ##Input image ID
        xpath = Util.GetXpath({"locate": "link_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tag field", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_id, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.EditProductTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherView(arg):
        '''
        ClickVoucherView : Click view button in free shipping voucher list page
                Input argu :
                    voucher_name - voucher name you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click view button in free shipping voucher list page
        xpath = Util.GetXpath({"locate": "voucher_view_btn"})
        xpath_voucher_edit_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_voucher_edit_btn, "message": "Click view button in voucher list page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFreeShippingVoucherPage.ClickVoucherView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DisableVoucher(arg):
        '''
        DisableVoucher : Disable Voucher
                Input argu :
                    delete - 0 = just disable, 1 = disable then delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        delete = int(arg['delete'])

        ##Click disable btn
        xpath = Util.GetXpath({"locate": "disable_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable button", "result": "1"})
        time.sleep(1)

        ##Click pop-up disable btn
        xpath = Util.GetXpath({"locate": "popup_disable_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click pop-up disable button", "result": "1"})
        time.sleep(5)

        ##check voucher need to delete or not
        if delete == 0:
            ##check disable label
            BaseUICore.CheckElementExist({"method": "xpath", "locate": "disable_label", "passok": "0", "result": "1"})
            dumplogger.info("Voucher disable!!!")
        elif delete == 1:
            ##click more
            xpath = Util.GetXpath({"locate": "more_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click more button", "result": "1"})
            time.sleep(1)
            ##click delete button
            xpath = Util.GetXpath({"locate": "delete_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})
            time.sleep(1)
            ##click OK
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})
            time.sleep(5)
            ##check delete label
            BaseUICore.CheckElementExist({"method": "xpath", "locate": "delete_label", "passok": "0", "result": "1"})
            dumplogger.info("Voucher delete!!!")

        OK(ret, int(arg["result"]), 'AdminFreeShippingVoucherPage.DisableVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EnableVoucher(arg):
        '''
        EnableVoucher : Enable Voucher
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click enable btn
        xpath = Util.GetXpath({"locate": "enable_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable button", "result": "1"})
        time.sleep(1)

        ##Click pop-up enable btn
        xpath = Util.GetXpath({"locate": "popup_disable_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click pop-up enable button", "result": "1"})
        time.sleep(5)

        ##Check enable label back
        BaseUICore.CheckElementExist({"method": "xpath", "locate": "enable_label", "passok": "0", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFreeShippingVoucherPage.EnableVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExportUserIDsList(arg):
        '''
        ExportUserIDsList : Export User IDs List
                Input argu :
                    user_id - the user id which need to export
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_id = arg['user_id']

        ##Click export user ID list button
        xpath = Util.GetXpath({"locate": "export_user_id_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export user id list button", "result": "1"})
        time.sleep(1)

        if user_id == "":
            ##Click export to all option
            xpath = Util.GetXpath({"locate": "export_all"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export all option", "result": "1"})
            time.sleep(3)
        else:
            ##Click export by user id option
            xpath = Util.GetXpath({"locate": "export_user_id"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export by user id option", "result": "1"})
            time.sleep(3)
            ##Input user id
            xpath = Util.GetXpath({"locate": "input_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user id field", "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_id, "result": "1"})
            time.sleep(1)
            ##Click ok button
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})
            time.sleep(3)

        ##Download exported file
        xpath = Util.GetXpath({"locate": "exported_file"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Exported file success!!!", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminFreeShippingVoucherPage.ExportUserIDsList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MouseHoverToAllUsers(arg):
        '''
        MouseHoverToAllUsers : Hover To All Users Option
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Hover to all user option
        xpath = Util.GetXpath({"locate": "all_user"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
        ##Check hint
        BaseUICore.CheckElementExist({"method": "xpath", "locate": "hint", "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFreeShippingVoucherPage.MouseHoverToAllUsers')
