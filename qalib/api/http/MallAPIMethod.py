#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 MallAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import json
import time
import random
import string
from codecs import encode

##Import Framework common library
import FrameWorkBase
import XtFunc
import Config
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic

##Import db Related Scripts
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class MallLoginAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def Login(arg):
        ''' Login : Login account
                input Argu :
                    username - account username
                    password - account password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        username = arg["username"]
        password = arg["password"]
        ret = 0

        ##Set collection name for mall login
        region = Config._TestCaseRegion_.lower()
        env = Config._EnvType_
        collection = region + "_" + env

        if not username:
            username = GlobalAdapter.CommonVar._DynamicCaseData_["username"]

        ##Reset global value
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"new", "json_data":"common/mall_login", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "username":username, "password":password, "result": "1"})

        ##Hash password
        GlobalAdapter.APIVar._HttpPayload_["password"] = XtFunc.MD5({"string":GlobalAdapter.APIVar._HttpPayload_["password"]})
        GlobalAdapter.APIVar._HttpPayload_["password"] = XtFunc.SHA256({"string":GlobalAdapter.APIVar._HttpPayload_["password"]})

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        for retry_times in range(1, 4):

            ##Send request
            HttpAPICore.SendHttpRequest({"http_method":"post","result": "1"})

            ##Check API response code
            APICommonMethod.CheckAPIResponseCode({"result": "1"})

            if 'SPC_EC' in GlobalAdapter.APIVar._APIResponse_['cookie'].keys():
                ret = 1
                break
            else:
                ##Send request
                dumplogger.info('Login fail! Missing "SPC_EC" in response cookie. Retry %d times' % retry_times)

        ##Check response status
        if ret == 1:
            ##Save cookie to global variable
            GlobalAdapter.APIVar._Csrftoken_ = "csrftoken=2AzWm32UZWjVkDvuVpf8rTRD9piUKqKu"
            for key, value in GlobalAdapter.APIVar._APIResponse_["cookie"].items():
                GlobalAdapter.APIVar._Csrftoken_ += "; " + key + "=" + value

        OK(ret, int(arg['result']), 'MallLoginAPI.Login')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForUserProfile(arg):
        ''' AssignDataForUserProfile:
                Input argu :
                    n/a
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        try:
            # put SPC_ST and SPC_EC and csrf token into cookies for verification
            GlobalAdapter.APIVar._HttpHeaders_['cookie'] = ""
            if "SPC_ST" in GlobalAdapter.APIVar._APIResponse_['cookie'].keys():
                GlobalAdapter.APIVar._HttpHeaders_['cookie'] += "SPC_ST=" + str(GlobalAdapter.APIVar._APIResponse_['cookie']['SPC_ST']) + ";"

            if "SPC_EC" in GlobalAdapter.APIVar._APIResponse_['cookie'].keys():
                GlobalAdapter.APIVar._HttpHeaders_['cookie'] += "SPC_EC=" + str(GlobalAdapter.APIVar._APIResponse_['cookie']['SPC_EC']) + ";"

            if "x-csrftoken" in GlobalAdapter.APIVar._HttpHeaders_.keys():
                GlobalAdapter.APIVar._HttpHeaders_['cookie'] += "csrftoken=" + str(GlobalAdapter.APIVar._HttpHeaders_['x-csrftoken'])

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1
        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'MallLoginAPI.AssignDataForUserProfile')


class MallCartAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveVoucher(arg):
        ''' SaveVoucher : save voucher by voucher code
                Input argu :
                    collection - payload collection to be sent (Optional)
                    voucher_type - shopee_voucher / seller_voucher / dp_voucher / partner_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]
        collection = arg['collection']

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"", "result": "1"})

        ##Get and set Http Api payload for vcode login
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        if voucher_type == "shopee_voucher":
            ##Assign voucher code for shopee voucher
            for voucher in GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_:
                GlobalAdapter.APIVar._HttpPayload_["voucher_code"] = voucher["code"]
        elif voucher_type == "seller_voucher":
            ##Assign voucher code and promotion id for seller voucher
            for voucher in GlobalAdapter.PromotionE2EVar._SellerVoucherList_:
                GlobalAdapter.APIVar._HttpPayload_["voucher_code"] = voucher["code"]
                GlobalAdapter.APIVar._HttpPayload_["promotionid"] = int(voucher["id"])
        elif voucher_type == "dp_voucher":
            ##Assign voucher code for dp voucher
            for voucher in GlobalAdapter.PromotionE2EVar._DPVoucherList_:
                GlobalAdapter.APIVar._HttpPayload_["voucher_code"] = voucher["code"]
        elif voucher_type == "partner_voucher":
            ##Assign voucher code for partner voucher
            for voucher in GlobalAdapter.PromotionE2EVar._PartnerVoucherList_:
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("display_id", GlobalAdapter.PromotionE2EVar._PartnerVoucherList_[-1]["display_id"])
        else:
            dumplogger.error("Not supported yet, voucher type : %s" % (voucher_type))

        ##Set http headers cookie
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send http request with retry method
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        OK(ret, int(arg['result']), 'MallCartAPI.SaveVoucher')


class MallCheckoutAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetPaymentDetailInfo(arg):
        ''' GetPaymentDetailInfo : Get payment detail info from database
                Input argu :
                    payment_channel - payment method to be matched (Please refer to payment_info_dict in data/apidata/http/common/checkout/checkout_data)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        payment_channel = arg["payment_channel"]

        ##Get json data from common folder
        slash = Config.dict_systemslash[Config._platform_]
        json_file = Config._APICaseDataFilePath_ + slash + "http" + slash + "common/checkout/checkout_data"

        ##Get json file to load data
        XtFunc.GetJSONContentFromFile({"jsonfile":json_file, "result": "1"})

        ##Save data from json file
        GlobalAdapter.APIVar._CreateOrderData_["payment_channel_id"] = GlobalAdapter.CommonVar._JSONRawDataFromFile_["payment_info_dict"][str.lower(Config._TestCaseRegion_)][payment_channel]

        ##Assign data list to be assigned in SQL cmd and store data from store list columns
        assign_list = [{"column": "channelid", "value_type": "string"}]
        store_list = ["spm_channel_id", "spm_option_info"]
        GlobalAdapter.CommonVar._DynamicCaseData_["channelid"] = GlobalAdapter.APIVar._CreateOrderData_["payment_channel_id"]

        ##Send SQL cmd
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_payment_backend_" + Config._TestCaseRegion_.lower(), "file_name":"spm_info", "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Prevent empty sql response
        try:
            ##Save result from SQL cmd to global
            GlobalAdapter.APIVar._CreateOrderData_["spm_channel_id"] = GlobalAdapter.CommonVar._DynamicCaseData_["spm_channel_id"]
            GlobalAdapter.APIVar._CreateOrderData_["spm_option_info"] = GlobalAdapter.CommonVar._DynamicCaseData_["spm_option_info"]
            dumplogger.info("GlobalAdapter.CommonVar._DynamicCaseData_: {}".format(GlobalAdapter.CommonVar._DynamicCaseData_))
        except KeyError:
            dumplogger.exception("Encounter KeyError exception")
            ret = -1
        except:
            dumplogger.exception("Encounter unknown exception")
            ret = -1

        OK(ret, int(arg['result']), 'MallCheckoutAPI.GetPaymentDetailInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckoutGet(arg):
        ''' CheckoutGet : Send checkout_get in checkout page to get all the data for an order
                Input argu :
                    voucher_type - shopee_voucher / seller_voucher / multiple_seller_voucher / free_shipping_voucher, or input empty to ignore assign voucher to API payload
                                   If you want to place order with multiple voucher type, you can seperate each voucher type with commas. Example: "shopee_voucher,seller_voucher,free_shipping_voucher"
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        voucher_type = arg["voucher_type"]

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"", "result": "1"})

        ##Get and set Http Api payload for vcode login
        HttpAPICore.GetAndSetHttpAPIData({"collection":"checkout_get", "result": "1"})

        try:
            ##Assign global shopee voucher to API payload
            if "shopee_voucher" in voucher_type:
                ##Replace shopee voucher id
                GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["platform_vouchers"][-1]["promotionid"] = int(GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["id"])
                dumplogger.info("Assigned platform voucher promotion id: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["platform_vouchers"][-1]["promotionid"])

                ##Replace shopee voucher code
                GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["platform_vouchers"][-1]["voucher_code"] = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["code"]
                dumplogger.info("Assigned platform voucher code: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["platform_vouchers"][-1]["voucher_code"])

            ##Assign multiple global seller voucher to API payload
            if "multiple_seller_voucher" in voucher_type:
                for voucher_data_index in range(len(GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"])):
                    ##Replace seller voucher id
                    GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][voucher_data_index]["promotionid"] = int(GlobalAdapter.PromotionE2EVar._SellerVoucherList_[voucher_data_index]["id"])
                    dumplogger.info("Assigned shop voucher promotion id: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][voucher_data_index]["promotionid"])

                    ##Replace seller voucher code
                    GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][voucher_data_index]["voucher_code"] = GlobalAdapter.PromotionE2EVar._SellerVoucherList_[voucher_data_index]["code"]
                    dumplogger.info("Assigned shop voucher code: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][voucher_data_index]["voucher_code"])

            ##Assign global seller voucher to API payload
            elif "seller_voucher" in voucher_type:
                ##Replace seller voucher id
                GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][-1]["promotionid"] = int(GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["id"])
                dumplogger.info("Assigned shop voucher promotion id: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][-1]["promotionid"])

                ##Replace seller voucher code
                GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][-1]["voucher_code"] = GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["code"]
                dumplogger.info("Assigned shop voucher code: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["shop_vouchers"][-1]["voucher_code"])

            ##Assign global free shipping voucher to API payload
            if "free_shipping_voucher" in voucher_type:
                ##Replace free shipping voucher id
                GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["free_shipping_voucher_info"]["free_shipping_voucher_id"] = int(GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"])
                dumplogger.info("Assigned free shipping voucher promotion id: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["free_shipping_voucher_info"]["free_shipping_voucher_id"])

                ##Replace free shipping voucher code
                GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["free_shipping_voucher_info"]["free_shipping_voucher_code"] = "FSV-" + GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"]
                dumplogger.info("Assigned free shipping voucher code: %s" % GlobalAdapter.APIVar._HttpPayload_["promotion_data"]["free_shipping_voucher_info"]["free_shipping_voucher_code"])

            ##Set http headers cookie
            GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

            ##Change payload format to json string
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})

            ##Retry 3 times to check API response
            for retry_times in range(1, 4):
                ##Send http request with common retry method
                ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

                ##Load json response
                response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

                ##Check "can_checkout" message in API response
                if "can_checkout" in response and response["can_checkout"]:
                    dumplogger.info("CheckoutGet API response can_checkout is true, Retry %dth time" % (retry_times))
                    ret = 1
                    break
                else:
                    dumplogger.info("CheckoutGet API response can_checkout is false!!!!!!!!!!!, Retry %dth time" % (retry_times))
                    ret = 0

                ##Sleep 3 second to retry
                time.sleep(3)

            ##Check request status
            if ret == 1:
                ##Save response text to global
                GlobalAdapter.APIVar._CreateOrderData_["checkout_data"] = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
                dumplogger.info("API response checkout_data: {}".format(GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]))
            else:
                dumplogger.info("API response checkout_data not found: response error")

        ##Handling IndexError
        except IndexError:
            dumplogger.exception("Encounter Index error exception")
            ret = -1
        ##Handle other error
        except:
            dumplogger.exception("Encounter unknown exception")
            ret = -1

        OK(ret, int(arg['result']), 'MallCheckoutAPI.CheckoutGet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PlaceOrder(arg):
        ''' PlaceOrder : To place an order with given data
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"", "result": "1"})

        ##Get and set Http Api payload for vcode login
        HttpAPICore.GetAndSetHttpAPIData({"collection":"place_order", "result": "1"})

        ##Set http headers cookie
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

        ##Set http payload
        GlobalAdapter.APIVar._HttpPayload_ = GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send http request with retry method
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        ##Check request status
        if ret == 1:
            try:
                response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
                ##Save data to global
                GlobalAdapter.APIVar._CreateOrderData_["order_id"] = int(response["orderids"][0])
                GlobalAdapter.APIVar._CreateOrderData_["checkout_id"] = response["checkoutid"]
                GlobalAdapter.OrderE2EVar._OrderID_ = int(response["orderids"][0])
                GlobalAdapter.OrderE2EVar._OrderTimestampDict_["place_order"] = GlobalAdapter.APIVar._RequestTimestamp_

                ##Todo: different senario matches different redirect url
                #GlobalAdapter.APIVar._CreateOrderData_["creditcard_url"] = GlobalAdapter.APIVar._CreateOrderData_["response_dict"]['text']["redirect_url"]

                dumplogger.info("_OrderID_: {}".format(GlobalAdapter.APIVar._CreateOrderData_["order_id"]))
                dumplogger.info("_CheckoutID_: {}".format(GlobalAdapter.APIVar._CreateOrderData_["checkout_id"]))
                #dumplogger.info("_CreditcardUrl_: {}".format(GlobalAdapter.APIVar._CreateOrderData_["creditcard_url"]))
            except KeyError:
                dumplogger.exception("Encounter key exception")
                ret = -1
            except:
                dumplogger.exception("Encounter unknown exception")
                ret = -1
        else:
            dumplogger.info("API response order_id, checkout_id not found: response error")

        OK(ret, int(arg['result']), 'MallCheckoutAPI.PlaceOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompleteOrder(arg):
        ''' CompleteOrder : complete order with orderid and shopid
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Init complete order api and set api data
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"complete_order", "result":"1"})

        ##Store shop ID from payload
        shop_id = str(GlobalAdapter.APIVar._HttpPayload_['shopid'])

        ##Bulid request url by orderID
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_ + GlobalAdapter.OrderE2EVar._OrderID_ + "/"

        ##set payload
        boundary = '----WebKitFormBoundary' + ''.join(random.sample(string.ascii_letters + string.digits, 16))
        dataList = []
        dataList.append(encode('--' + boundary))
        dataList.append(encode('Content-Disposition: form-data; name=shopid;'))
        dataList.append(encode(''))
        dataList.append(encode(shop_id))
        dataList.append(encode('--' + boundary + '--'))
        body = b'\r\n'.join(dataList)
        GlobalAdapter.APIVar._HttpPayload_ = body

        ##set header
        GlobalAdapter.APIVar._HttpHeaders_["Content-Type"] += boundary
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] += GlobalAdapter.APIVar._Csrftoken_

        ##Send complete order request
        HttpAPICore.SendHttpRequest({"http_method":"post", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result":"1"})

        ##Deinitial
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'MallCheckoutAPI.CompleteOrder')


class MallAuthenticationAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignPrefixToPhoneNumber(arg):
        ''' AssignPrefixToPhoneNumber : To generate a random number and assign to phone column.
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ## Generate a random number totally 8 digits, e.g. 00849843. And save result to global.
            GlobalAdapter.CommonVar._DynamicCaseData_["phone"] = "001" + XtFunc.GenerateRandomString(6,"numbers")

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'MallAuthenticationAPI.AssignPrefixToPhoneNumber')
