import unittest
from Config import dumplogger
from FrameWorkBase import *


class THAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup THAPIRestoreHomePopupBanner Precondition ==")

    def test_THAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/THAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup THAPIRestoreHomePopupBanner Post condition ==")
