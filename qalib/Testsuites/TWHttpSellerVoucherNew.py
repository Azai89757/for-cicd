import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpSellerVoucherNew(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerVoucherNew Precondition =="
        dumplogger.info("== Setup TWHttpSellerVoucherNew Precondition ==")

    def test_TWHttpSellerVoucherNew01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucherNew-01.xml")

    def test_TWHttpSellerVoucherNew02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucherNew-02.xml")

    def test_TWHttpSellerVoucherNew03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucherNew-03.xml")

    def test_TWHttpSellerVoucherNew04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucherNew-04.xml")

    def test_TWHttpSellerVoucherNew05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerVoucherNew-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerVoucherNew Post condition =="
        dumplogger.info("== Setup TWHttpSellerVoucherNew Post condition ==")
