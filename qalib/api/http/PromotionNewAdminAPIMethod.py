#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 PromotionNewAdminAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time
import json
import datetime

##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminNewPromotionAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreNewAdminPromotionCookie(arg):
        '''
        StoreNewAdminPromotionCookie : Assign Promortion Admin cookie(SSO_C) to API Request header
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get current country
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Get cookie name and value from DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"},{"column": "country", "value_type": "string"}]

        ##Send SQL to get Promotion admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_promotion", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Assign cookie to API request header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.StoreNewAdminPromotionCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCoinsRule(arg):
        '''
        ChangeCoinsRule : Change coins rule
                Input argu :
                    type - earn_limit / earn_rule / spend_limit / spend_rule
                    earn_daily_limit - max daily earn limit
                    earn_weekly_limit - max weekly earn limit
                    order_limit - max order earn limit
                    cash_spend - cash spend
                    coin_earn - coin earn
                    earn_rounding_unit - rounding unit
                    spend_daily_limit - max daily spend limit
                    spend_weekly_limit - max weekly spend limit
                    max_percent - max percent per item
                    spend_rounding_unit - rounding unit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": str("common/Coins/coins_" + type), "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})

        #set coins earn limit
        if type == "earn_limit":
            earn_daily_limit = arg['earn_daily_limit']
            earn_weekly_limit = arg['earn_weekly_limit']
            order_limit = arg['order_limit']
            GlobalAdapter.APIVar._HttpPayload_["earn_limit_default"]["earn_max_daily"] = int(float(earn_daily_limit)*100000)
            GlobalAdapter.APIVar._HttpPayload_["earn_limit_default"]["earn_max_weekly"] = int(float(earn_weekly_limit)*100000)
            GlobalAdapter.APIVar._HttpPayload_["earn_limit_default"]["earn_max_order_limit"] = int(float(order_limit)*100000)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['earn_limit_default']['earn_max_daily'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["earn_limit_default"]["earn_max_daily"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['earn_limit_default']['earn_max_weekly'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["earn_limit_default"]["earn_max_weekly"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['earn_limit_default']['earn_max_order_limit'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["earn_limit_default"]["earn_max_order_limit"]))

        #set coins earn rule
        elif type == "earn_rule":
            cash_spend = arg['cash_spend']
            coin_earn = arg['coin_earn']
            earn_rounding_unit = arg['earn_rounding_unit']
            GlobalAdapter.APIVar._HttpPayload_["earn_rule_default"]["cash_spend"] = int(float(cash_spend)*100000)
            GlobalAdapter.APIVar._HttpPayload_["earn_rule_default"]["coin_earn"] = int(float(coin_earn)*100000)
            GlobalAdapter.APIVar._HttpPayload_["earn_rule_default"]["rounding_unit"] = int(float(earn_rounding_unit)*100000)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['earn_rule_default']['cash_spend'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["earn_rule_default"]["cash_spend"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['earn_rule_default']['coin_earn'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["earn_rule_default"]["coin_earn"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['earn_rule_default']['rounding_unit'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["earn_rule_default"]["rounding_unit"]))

        #set coins spend limit
        elif type == "spend_limit":
            spend_daily_limit = arg['spend_daily_limit']
            spend_weekly_limit = arg['spend_weekly_limit']
            GlobalAdapter.APIVar._HttpPayload_["spend_limit_default"]["spend_max_daily"] = int(float(spend_daily_limit)*100000)
            GlobalAdapter.APIVar._HttpPayload_["spend_limit_default"]["spend_max_weekly"] = int(float(spend_weekly_limit)*100000)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['spend_limit_default']['spend_max_daily'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["spend_limit_default"]["spend_max_daily"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['spend_limit_default']['spend_max_weekly'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["spend_limit_default"]["spend_max_weekly"]))

        #set coins spend rule
        elif type == "spend_rule":
            max_percent = arg['max_percent']
            spend_rounding_unit = arg['spend_rounding_unit']
            GlobalAdapter.APIVar._HttpPayload_["spend_rule_default"]["max_percentage_per_item"] = int(max_percent)
            GlobalAdapter.APIVar._HttpPayload_["spend_rule_default"]["rounding_unit"] = int(float(spend_rounding_unit)*100000)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['spend_rule_default']['max_percentage_per_item'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["spend_rule_default"]["max_percentage_per_item"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['spend_rule_default']['rounding_unit'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["spend_rule_default"]["rounding_unit"]))

        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        dumplogger.info("Response: %s" % (GlobalAdapter.APIVar._APIResponse_["text"]))

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.ChangeCoinsRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateSellerDiscount(arg):
        '''
        CreateSellerDiscount : Create seller discount
                Input argu :
                    start_time - time slot start time
                    end_time - time slot end time
                    title - title
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        shop_id = arg['shop_id']
        title = arg['title']

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/SellerDiscount/create_seller_discount", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        AdminNewPromotionAPI.AssignDataForSellerDiscount({"start_time": start_time, "end_time": end_time, "shop_id": shop_id, "title":title, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.StoreDataFromAPIResponse({"column": "promotion_id", "result": "1"})
        GlobalAdapter.SellerDiscountE2EVar._PromotionID_ = str(GlobalAdapter.CommonVar._DynamicCaseData_["promotion_id"])
        dumplogger.info("_PromotionID_ : %s" % (GlobalAdapter.SellerDiscountE2EVar._PromotionID_))

        HttpAPICore.InitialHttpAPI({"session": "inherit", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "upload_product", "result": "1"})
        AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.SellerDiscountE2EVar._PromotionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        dumplogger.info("Response: %s" % (GlobalAdapter.APIVar._APIResponse_["text"]))
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.CreateSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreatePlatformPromotion(arg):
        '''
        CreatePlatformPromotion : Create platform promotion
                Input argu :
                    start_time - time slot start time
                    end_time - time slot end time

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        ##Add create platform promotion
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "create_platform_promotion", "result": "1"})
        AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        AdminNewPromotionAPI.AssignTimePeriodForPlatformPromotion({"start_time": start_time, "end_time": end_time,"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.StoreDataFromAPIResponse({"column": "promotion_id", "result": "1"})
        GlobalAdapter.SellerDiscountE2EVar._PromotionID_ = str(GlobalAdapter.CommonVar._DynamicCaseData_["promotion_id"])
        dumplogger.info("_PromotionID_ : %s" % (GlobalAdapter.SellerDiscountE2EVar._PromotionID_))

        ##Upload the file
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "upload_product", "result": "1"})
        AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.SellerDiscountE2EVar._PromotionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        dumplogger.info("Response: %s" % (GlobalAdapter.APIVar._APIResponse_["text"]))
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.CreatePlatformPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForSellerDiscount(arg):
        '''
        AssignDataForSellerDiscount : Assign time peroid for create seller discount API
                Input argu :
                    start_time - start time
                    end_time - end time
                    shop_id - shop id
                    promo_name - promo_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg['start_time']
        end_time = arg['end_time']
        shop_id = arg['shop_id']
        title = arg['title']

        try:
            ##Insert promotion shop id and promotion name into create seller discount api payload
            GlobalAdapter.APIVar._HttpPayload_["shop_id"] = int(shop_id)
            GlobalAdapter.APIVar._HttpPayload_["title"] = title
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['shop_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["shop_id"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['title'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["title"]))

            ##Get start time
            start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0, is_tw_time=1)
            start_unix_time = time.mktime(time.strptime(start_datetime, "%Y-%m-%d %H:%M"))

            ##Get end time
            end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0, is_tw_time=1)
            end_unix_time = time.mktime(time.strptime(end_datetime, "%Y-%m-%d %H:%M"))

            ##Insert promotion start time and promotion end time into create seller discount api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.AssignDataForSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeriodForPlatformPromotion(arg):
        '''
        AssignTimePeriodForPlatformPromotion : Assign unix time peroid for create platform promotion
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        try:
            ##Get start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeriodForPlatformPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReplaceSellerDiscountFileName(arg):
        '''
        ReplaceSellerDiscountFileName : Replace the file_link in payload cause path is dynamic
                    Input argu :
                        column - This column value will go take value from GlobalAdapter.CommonVar._DynamicCaseData_[]
                    Return code :
                        1 - success
                        0 - fail
        '''
        ret = 1
        file_link = str(GlobalAdapter.APIVar._JsonCaseData_["payload"]["file_link"])

        ##Replace path to file_link
        if "path_to_be_replaced" in file_link:
            dumplogger.info("Payload before replaced:" + file_link)
            GlobalAdapter.APIVar._JsonCaseData_["payload"]["file_link"] = file_link.replace("path_to_be_replaced", str(GlobalAdapter.CommonVar._DynamicCaseData_["filename"]))
            dumplogger.info("Payload replaced result:" + GlobalAdapter.APIVar._JsonCaseData_["payload"]["file_link"])
        else:
            dumplogger.info("Please check your _DynamicCaseData_ stored is correct or not !!")
            ret = 0

        OK(ret, int(arg['result']), 'ReplaceSellerDiscountFileName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForVoucher(arg):
        '''
        AssignTimePeroidForVoucher : Assign time period for voucher
                    Input argu :
                        voucher_type : seller_voucher / free_shipping_voucher / shopee_voucher
                        start_time : voucher validity start time delta
                        end_time : voucher validity end time delta
                    Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        voucher_type = arg['voucher_type']
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start unix time
            start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0, is_tw_time=1)
            start_unix_time = time.mktime(time.strptime(start_datetime, "%Y-%m-%d %H:%M"))

            ##Get end unix time
            end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0, is_tw_time=1)
            end_unix_time = time.mktime(time.strptime(end_datetime, "%Y-%m-%d %H:%M"))

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["valid_period"]["gte_value"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["valid_period"]["lte_value"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['data']['basic_info']['valid_period']['gte_value'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["valid_period"]["gte_value"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['data']['basic_info']['valid_period']['lte_value'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["valid_period"]["gte_value"]))

            if voucher_type in ("free_shipping_voucher", "shopee_voucher"):
                GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["claim_period"]["gte_value"] = int(start_unix_time)
                GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["claim_period"]["lte_value"] = int(end_unix_time)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['data']['basic_info']['claim_period']['gte_value'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["claim_period"]["gte_value"]))
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['data']['basic_info']['claim_period']['lte_value'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["claim_period"]["gte_value"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.AssignTimePeroidForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignPrefixCodeForVoucher(arg):
        '''
        AssignPrefixCodeForVoucher : Assign prefix code for create voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
        '''
        ret = 1

        ##Generate random string prefix code for create voucher
        GlobalAdapter.GeneralE2EVar._RandomString_ = XtFunc.GenerateRandomString(8, "characters")

        ##Insert prefix code to api payload
        GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["voucher_code"] = GlobalAdapter.GeneralE2EVar._RandomString_
        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['data']['basic_info']['voucher_code'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["data"]["basic_info"]["voucher_code"]))

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignPrefixCodeForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeVoucherStatus(arg):
        '''
        ChangeVoucherStatus : Change all vouchers in global list by Promotion Admin API
                Input argu :
                    status - enable / disable
                    voucher_type - seller_voucher / free_shipping_voucher / shopee_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]
        voucher_type = arg["voucher_type"]

        if voucher_type == "seller_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._SellerVoucherList_
        elif voucher_type == "free_shipping_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
        elif voucher_type == "shopee_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_

        if status == "disable":
            voucher_status = 0
        elif status == "enable":
            voucher_status = 1

        for voucher in voucher_list:
            ##Change voucher by admin API
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/voucher/promotion_admin_change_voucher_status", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "change_voucher_status", "result": "1"})
            AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("region", GlobalAdapter.UrlVar._Domain_)
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("voucher_type", voucher_type)
            GlobalAdapter.APIVar._HttpPayload_["region"] = Config._TestCaseRegion_
            GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(voucher["id"])
            GlobalAdapter.APIVar._HttpPayload_["voucher_status"] = voucher_status
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            APICommonMethod.CheckAPIResponseText({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.ChangeVoucherStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForPromotion(arg):
        '''
        AssignDataForPromotion : Assign data for promotion admin API
                Input argu :
                    promotion_type - seller_voucher / free_shipping_voucher / shopee_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg["promotion_type"]

        try:
            if promotion_type == "seller_voucher" and GlobalAdapter.PromotionE2EVar._SellerVoucherList_:
                ##Replace API url/referer promotion id to last global stored seller voucher promotion id
                GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["id"])
            elif promotion_type == "free_shipping_voucher" and GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_:
                GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"])
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['promotion_id']]= %s" % (GlobalAdapter.APIVar._HttpPayload_["promotion_id"]))

            ##Log API variable
            dumplogger.info("GlobalAdapter.APIVar._HttpUrl_ = %s" % (GlobalAdapter.APIVar._HttpUrl_))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload = %s" % (GlobalAdapter.APIVar._HttpPayload_))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.AssignDataForPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangePlatformPromotionProductStatus(arg):
        '''
        ChangePlatformPromotionProductStatus : Change product status in platform promotion list by api
                Input argu :
                    status - confirm / reject
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        if status == "confirm":
            ##Confirm the product
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "confirm_product", "result": "1"})
            AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
            ##if global promotion id is not empty
            if str(GlobalAdapter.SellerDiscountE2EVar._PromotionID_):
                GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.SellerDiscountE2EVar._PromotionID_)
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            dumplogger.info("Response: %s" % (GlobalAdapter.APIVar._APIResponse_["text"]))
            HttpAPICore.DeInitialHttpAPI({"result": "1"})
        elif status == "reject":
            ##reject the product
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "reject_product", "result": "1"})
            AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
            ##if global promotion id is not empty
            if str(GlobalAdapter.SellerDiscountE2EVar._PromotionID_):
                GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.SellerDiscountE2EVar._PromotionID_)
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            dumplogger.info("Response: %s" % (GlobalAdapter.APIVar._APIResponse_["text"]))
            HttpAPICore.DeInitialHttpAPI({"result": "1"})
        else:
            dumplogger.info("Please check your status is correct or not !!")
            ret = 0

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.ChangePlatformPromotionProductStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeSellerDiscountProductStatus(arg):
        '''
        ChangeSellerDiscountProductStatus : Change product status in seller discount list by api
                Input argu :
                    status - normal / remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        if status == "normal":
            collection_name = "Seller_Discount_admin_add_item"
        elif status == "remove":
            collection_name = "Seller_Discount_admin_remove_item"

        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection_name, "result": "1"})
        AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        ##if global promotion id is not empty
        if str(GlobalAdapter.SellerDiscountE2EVar._PromotionID_):
            GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.SellerDiscountE2EVar._PromotionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        dumplogger.info("Response: %s" % (GlobalAdapter.APIVar._APIResponse_["text"]))
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminNewPromotionAPI.ChangeSellerDiscountProductStatus')
