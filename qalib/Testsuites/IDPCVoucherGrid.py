import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCVoucherGrid(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCVoucherGrid Precondition =="
        dumplogger.info("== Setup IDPCVoucherGrid Precondition ==")

    def test_IDPCVoucherGrid01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCVoucherGrid-01.xml")

    def test_IDPCVoucherGrid02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCVoucherGrid-02.xml")

    def test_IDPCVoucherGrid03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCVoucherGrid-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCVoucherGrid Post condition =="
        dumplogger.info("== Setup IDPCVoucherGrid Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
