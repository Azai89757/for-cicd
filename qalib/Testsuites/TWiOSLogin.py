import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class TWiOSLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWiOSLogin Precondition =="
        dumplogger.info("== Setup TWiOSLogin Precondition ==")

        platform = Config._TestCasePlatform_.lower()

        ##Get device id
        device_id = GetiOSDeviceId("iOS")
        print "Device id : %s" % (device_id)

        #Get device info by device id
        if device_id:
            ##Start Appium
            iOSStartAppium({"result": "1"})

            ##Get iOS version of this device
            device_version = GetiOSDeviceInfo()

            ##Call function from iOSBaseUICore
            print "Device connection check: Pass"
            #print "App installed check: " + CheckMobileAppInstall(platform, device_id, packagename)
            print "Get device version: " + device_version

            ##Assign device platform version & device udid to global var
            Config._iOSDesiredCaps_['udid'] = device_id

            ##Call function from iOSBaseUICore
            iOSInitialDriver()

        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_TWiOSLogin001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWiOSLogin-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWiOSLogin Post condition =="
        dumplogger.info("== Setup TWiOSLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
