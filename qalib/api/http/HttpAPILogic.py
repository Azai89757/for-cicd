#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 HttpAPILogic.py: The def of this file called by other function.
'''
##Import system library
import json
import time
import urllib

##Import common library
import FrameWorkBase
import Config
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter

##Import api library
from api import APICommonMethod
import HttpAPICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def ReserveAPIResponseData(arg):
    ''' ReserveAPIResponseData : Reserve API response data for different requirement
                Input argu :
                    reserve_type - user_info
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''

    ret = 1
    reserve_type = arg["reserve_type"]

    try:
        ##Store response text
        response_text = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        dumplogger.info(response_text)

        if reserve_type == "user_info":
            ##Store UserID, ShopID to global variable
            GlobalAdapter.OrderE2EVar._OrderSNDict_ = str(response_text["data"][0]["userid"])
            GlobalAdapter.AccountE2EVar._ShopID_ = str(response_text["data"][0]["shopid"])
            dumplogger.info("user id -> %s" % (GlobalAdapter.OrderE2EVar._OrderSNDict_))
            dumplogger.info("shop id -> %s" % (GlobalAdapter.AccountE2EVar._ShopID_))
        elif reserve_type == "order_id":
            GlobalAdapter.OrderE2EVar._OrderID_ = str(response_text["data"][0]["orderid"])
            dumplogger.info("order id -> %s" % (GlobalAdapter.OrderE2EVar._OrderID_))
        elif reserve_type == "promotion_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["message"]["promotionid"])
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(promotion_id)
            GlobalAdapter.PromotionE2EVar._FlashSaleIDList_.append(promotion_id)
            dumplogger.info("promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))
        elif reserve_type == "shopee_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["promotionid"])
            ##If last stored shopee voucher code is created random string voucher code, modify this stored shopee voucher id
            if GlobalAdapter.GeneralE2EVar._RandomString_:
                GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"code": GlobalAdapter.GeneralE2EVar._RandomString_, "id": promotion_id})
            ##Append shopee voucher id to list
            else:
                GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"code": "", "id": promotion_id})
            dumplogger.info("shopee voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_))
        elif reserve_type == "promotion_admin_shopee_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["data"]["voucher_identifier"]["promotion_id"])
            ##If last stored shopee voucher code is created random string voucher code, modify this stored shopee voucher id
            if GlobalAdapter.GeneralE2EVar._RandomString_:
                GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"code": GlobalAdapter.GeneralE2EVar._RandomString_, "id": promotion_id})
            ##Append shopee voucher id to list
            else:
                GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"code": "", "id": promotion_id})
            dumplogger.info("shopee voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_))
        elif reserve_type == "free_shipping_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["promotionid"])
            GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_.append({"code": "FSV-" + str(promotion_id), "id": promotion_id})
            dumplogger.info("free shipping voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_))
        elif reserve_type == "promotion_admin_free_shipping_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["data"]["voucher_identifier"]["promotion_id"])
            GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_.append({"code": "FSV-" + str(promotion_id), "id": promotion_id})
            dumplogger.info("free shipping voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_))
        elif reserve_type == "seller_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["promotionid"])
            ##If last stored seller voucher code is created random string voucher code, modify this stored seller voucher id
            if GlobalAdapter.GeneralE2EVar._RandomString_:
                GlobalAdapter.PromotionE2EVar._SellerVoucherList_.append({"code": GlobalAdapter.GeneralE2EVar._RandomString_, "id": promotion_id})
            ##Append seller voucher id to list
            else:
                GlobalAdapter.PromotionE2EVar._SellerVoucherList_.append({"code": "", "id": promotion_id})
            dumplogger.info("seller voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._SellerVoucherList_))
        elif reserve_type == "promotion_admin_seller_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["data"]["voucher_identifier"]["promotion_id"])
            ##If last stored seller voucher code is created random string voucher code, modify this stored seller voucher id
            if GlobalAdapter.GeneralE2EVar._RandomString_:
                GlobalAdapter.PromotionE2EVar._SellerVoucherList_.append({"code": GlobalAdapter.GeneralE2EVar._RandomString_, "id": promotion_id})
            ##Append seller voucher id to list
            else:
                GlobalAdapter.PromotionE2EVar._SellerVoucherList_.append({"code": "", "id": promotion_id})
            dumplogger.info("seller voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._SellerVoucherList_))
        elif reserve_type == "add_on_deal_id":
            ##Store AddOnDealID to global variable
            add_on_deal_id = str(response_text["data"]["add_on_deal_id"])
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(add_on_deal_id)
            dumplogger.info("promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))
        elif reserve_type == "promotion_rule_id":
            ##Store PromotionID to global variable
            promotion_rule_id = str(response_text["id"])
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(promotion_rule_id)
            dumplogger.info("promotion rule id list -> %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))
        elif reserve_type == "inshop_flashsale_id":
            ##Store PromotionID to global variable
            inshop_flashsale_id = str(response_text["data"]["flash_sale_id"])
            GlobalAdapter.APIVar._InShopFlashSaleIDList_.append(inshop_flashsale_id)
            dumplogger.info("inshop_flashsale_id -> %s" % (GlobalAdapter.APIVar._InShopFlashSaleIDList_))
        elif reserve_type == "brand_sale_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["promotionid"])
            GlobalAdapter.PromotionE2EVar._BrandSaleIDList_.append(promotion_id)
            dumplogger.info("brand sale promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._BrandSaleIDList_))
        elif reserve_type == "notification_id":
            ##Store UserID, ShopID to global variable
            GlobalAdapter.NotiE2EVar._NotificationID_ = str(response_text["taskid"])
            dumplogger.info("banner id -> %s" % (GlobalAdapter.NotiE2EVar._NotificationID_))
        elif reserve_type == "upcoming_flash_sale_status":
            if response_text["total_count"] > 0:
                ##Store FlashSaleStatus to global variable
                GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_ = response_text["flash_sales"]
                GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = response_text["flash_sales"][-1]["session_id"]
                dumplogger.info("promotion status list -> %s" % (GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_))
        elif reserve_type == "ongoing_flash_sale_status":
            if response_text["total_count"] == 1:
                ##Store FlashSaleStatus to global variable
                GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_ = response_text["flash_sales"]
                GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = response_text["flash_sales"][0]["session_id"]
                dumplogger.info("promotion status list -> %s" % (GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_))
        elif reserve_type == "banner_campaign_unit_id":
            ##Store Campaign unit id to global variable
            GlobalAdapter.BannerE2EVar._BannerCampaignUnitList_ = response_text["data"]["data"]
            dumplogger.info("campaign unit id list -> %s" % (GlobalAdapter.BannerE2EVar._BannerCampaignUnitList_))
        elif reserve_type == "banner":
            ##Store Campaign unit id to global variable
            GlobalAdapter.BannerE2EVar._BannerList_ = response_text["data"]
            dumplogger.info("banner list -> %s" % (GlobalAdapter.BannerE2EVar._BannerList_))
        elif reserve_type == "offline_payment_voucher_id":
            promotion_id = str(response_text["promotionid"])
            ##Store OfflinePaymentVoucher code to global variable
            if promotion_id:
                GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_.append({"code": "OPV-" + str(promotion_id), "id": promotion_id})
        elif reserve_type == "dp_voucher_id":
            ##Store PromotionID to global variable
            promotion_id = str(response_text["promotionid"])
            ##If last stored shopee voucher code is created random string voucher code, modify this stored shopee voucher id
            if GlobalAdapter.GeneralE2EVar._RandomString_:
                GlobalAdapter.PromotionE2EVar._DPVoucherList_.append({"code": GlobalAdapter.GeneralE2EVar._RandomString_, "id": promotion_id})
            ##Append shopee voucher id to list
            else:
                GlobalAdapter.PromotionE2EVar._DPVoucherList_.append({"code": "", "id": promotion_id})
            dumplogger.info("dp voucher promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._DPVoucherList_))
        elif reserve_type == "partner_voucher_id":
            promotion_id = str(response_text["partner_id"])
            ##Store partner voucher id to global variable
            if promotion_id:
                GlobalAdapter.PromotionE2EVar._PartnerVoucherList_.append({"code": "", "id": promotion_id})
        elif reserve_type == "partner_voucher_display_id":
            display_id = str(response_text["redeem_id"])
            ##Store partner voucher display id to global variable
            if display_id:
                GlobalAdapter.PromotionE2EVar._PartnerVoucherList_.append({"display_id": display_id})
        elif reserve_type == "fraud_tag":
            tag_list = response_text["data"]["tag_info_list"]
            ##Store CONFIRMED_SEVERE tags to global variable
            for tag in tag_list:
                if tag["tag_severity"] == "SEVERE" and tag["tag_type"] == "CONFIRMED":
                    GlobalAdapter.AccountE2EVar._FraudTagList_.append({"tag_id": tag["tag_id"], "tag_name": tag["tag_name"], "tag_severity": 1, "tag_type": 3})
        elif reserve_type == "bundle_deal_id":
            ##Store Bundle Deal ID to global variable
            bundle_deal_id = response_text["data"]["bundle_deal_id"]
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(bundle_deal_id)
            dumplogger.info("promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))
        elif reserve_type == "gift_with_purchase_id":
            ##Store ADD On Deal ID to global variable
            gift_with_purchase_id = response_text["add_on_deal_id"]
            GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(gift_with_purchase_id)
            dumplogger.info("promotion id list -> %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))
        elif reserve_type == "qc_reason":
            ##Store Bundle Deal ID to global variable
            qc_reasons = response_text["data"]["system_fail_reason_profiles"]
            for reason in qc_reasons:
                if reason["description"] == "Auto team test reason":
                    GlobalAdapter.ListingE2EVar._QCReason_ = reason
                    break
            dumplogger.info("qc reason -> %s" % (GlobalAdapter.ListingE2EVar._QCReason_))
        elif reserve_type == "file_path":
            ##Store Bundle Deal ID to global variable
            file_path = str(response_text["path"])
            GlobalAdapter.ListingE2EVar._UploadedFilePath_ = file_path
            dumplogger.info("uploaded file path -> %s" % (GlobalAdapter.ListingE2EVar._UploadedFilePath_))
        else:
            dumplogger.info("Please take sure your reserve_type is correct!!!")
            ret = 0

    ##Handle Key Error
    except KeyError:
        dumplogger.exception("Encounter key error")
        ret = -1

    ##Handle Value Error
    except ValueError:
        dumplogger.exception("Encounter value error")
        ret = -1

    ##Handle Other exception Error
    except:
        dumplogger.exception("Encounter exception error")
        ret = -1

    OK(ret, int(arg['result']), 'ReserveAPIResponseData -> ' + reserve_type)

@DecoratorHelper.FuncRecorder
def ConvertPayloadDataToDoubleQuote(arg):
    ''' ConvertPayloadDataToDoubleQuote : Convert payload data from single quote to double quote if value is string
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''

    ret = 1

    try:
        ##for loop all key/value in payload dictionary
        for key, value in GlobalAdapter.APIVar._HttpPayload_.iteritems():
            ##If value type is string/unicode, begin convert single quote to double quote
            if type(value) is str or type(value) is unicode:
                dumplogger.info("Value before convert -> %s" % (value))
                value = value.replace("'", '"')
                dumplogger.info("Value after convert -> %s" % (value))
                GlobalAdapter.APIVar._HttpPayload_[key] = value
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ -> %s" % (GlobalAdapter.APIVar._HttpPayload_))

    except AttributeError:
        dumplogger.exception("Payload Is Not Dictionary!!!")
        ret = 0

    except:
        dumplogger.exception("Encounter Other Exception!!!")
        ret = 0

    OK(ret, int(arg['result']), 'ConvertPayloadDataToDoubleQuote')

@DecoratorHelper.FuncRecorder
def AssignFileToAPIRequest(arg):
    ''' AssignFileToAPIRequest : Set file to request
            Input argu :
                    file_name - upload file name
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''
    slash = Config.dict_systemslash[Config._platform_]
    ret = 1
    file_name = arg["file_name"]
    file_path = Config._APICaseDataFilePath_ + slash + Config._TestCasePlatform_.lower() + slash + "common" + slash + Config._TestCaseFeature_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_.lower() + slash

    ## open the file and assign to API request
    GlobalAdapter.APIVar._HttpUploadFiles_ = {
        'file': (file_name, open(file_path + file_name,'rb'), 'text/csv')
    }

    OK(ret, int(arg['result']), 'AssignFileToAPIRequest')

def SendHttpRequestWithRetry(arg):
    ''' SendHttpRequestWithRetry : Send Http Request With Retry method
            Input argu :
                    http_method - get / post / put
            Return code :
                    1 - success
                    0 - fail
                    -1 - error
    '''
    ret = 0
    http_method = arg["http_method"]

    for retry_times in range(1, 4):
        ##Send request
        HttpAPICore.SendHttpRequest({"http_method":http_method, "result": "1"})
        dumplogger.info("API response text: {}".format(GlobalAdapter.APIVar._APIResponse_))

        ##Check api response code
        APICommonMethod.CheckAPIResponseCode({"result":"1"})

        try:
            ##Load json response
            response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

            ##Check if error message exist
            if "error" in response:
                ##Check if error message is empty or 0
                if response["error"] is None or response["error"] == 0:
                    dumplogger.info("Found error message in SendHttpRequestWithRetry and error message is None or 0, Retry %dth time" % (retry_times))
                    ret = 1
                    break
                else:
                    dumplogger.info("Found error message in SendHttpRequestWithRetry!!!!!!!!!!! Retry %dth times" % (retry_times))
            ##Check if error column not existed in response message but response message is failed
            elif "message" in response and "failed" in response["message"].lower():
                dumplogger.info("Response message in SendHttpRequestWithRetry is failed!!!!!!!!!!! Retry %dth times" % (retry_times))
            ##Check response status if error column not existed in response message
            elif int(GlobalAdapter.APIVar._JsonCaseData_["response_status"]) == 200:
                dumplogger.info("Error message not in SendHttpRequestWithRetry and response status is 200, Retry %dth time" % (retry_times))
                ret = 1
                break
            else:
                dumplogger.info("Response status is not 200!!!!!!!!!!! Retry %d times" % (retry_times))

        ##Handle Key Error
        except KeyError:
            dumplogger.exception("Encounter key error")
            ret = -1

        ##Handle Unknown Error
        except:
            dumplogger.exception("Encounter exception error")
            ret = -1

        time.sleep(3)

    return ret

@DecoratorHelper.FuncRecorder
def ChangePayloadDictToUrlEncode(arg):
    ''' ChangePayloadDictToUrlEncode : change the payload type from json to urlencode type
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - failed
                    -1 - UnicodeEncodeError when changing payload format / Others: fail
    '''
    ret = 1
    try:
        ##Take value from Global adapter http payload and convert to url encoding style
        for key, value in GlobalAdapter.APIVar._HttpPayload_.iteritems():
            if type(value) == unicode:
                GlobalAdapter.APIVar._HttpPayload_[key] = value.encode("utf-8")

        GlobalAdapter.APIVar._HttpPayload_ = urllib.urlencode(GlobalAdapter.APIVar._HttpPayload_)
        dumplogger.info("urllib.urlencode(GlobalAdapter.APIVar._HttpPayload_) success!")

    except UnicodeEncodeError:
        dumplogger.exception("Encounter UnicodeEncodeError when urllib.urlencode(GlobalAdapter.APIVar._HttpPayload_)!!!")
        ret = -1

    except:
        dumplogger.exception("Encounter unknown Error when urllib.urlencode(GlobalAdapter.APIVar._HttpPayload_)!!!")
        ret = -1

    OK(ret, int(arg['result']), 'ChangePayloadDictToUrlEncode')

@DecoratorHelper.FuncRecorder
def ReplaceURLFromStoredData(arg):
    ''' ReplaceURLFromStoredData : Replace the url if url it's dynamic e.g. api/v0/addresses/{address list no.}/delete/
                Input argu :
                    column = This column value will go take value from GlobalAdapter.CommonVar._DynamicCaseData_[]
                Return code :
                    1 - success
                    0 - failed
                    -1 - KeyError when changing payload format / Others: fail
    '''
    ret = 1
    column = arg["column"]
    url = str(GlobalAdapter.APIVar._JsonCaseData_["url"])

    try:
        ## if there's str_to_be_replaced in url then start to replace it
        if column and "str_to_be_replaced" in url:
            dumplogger.info("URL before replaced:" + url)
            GlobalAdapter.APIVar._HttpUrl_ = url.replace("str_to_be_replaced", str(GlobalAdapter.CommonVar._DynamicCaseData_[column]))
            dumplogger.info("URL replaced result:" + GlobalAdapter.APIVar._HttpUrl_)
        else:
            dumplogger.info("Please check your _DynamicCaseData_ stored is correct or not !!")
            ret = 0

    except KeyError:
        dumplogger.exception("Encounter KeyError !!!")
        ret = -1

    except:
        dumplogger.exception("Encounter unknown Error !!!")
        ret = -1

    OK(ret, int(arg['result']), 'ReplaceURLFromStoredData')
