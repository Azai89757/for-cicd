import unittest
import Config
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCVoucherLandingPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCVoucherLandingPage Precondition =="
        Config.logging.info("== Setup SGPCVoucherLandingPage Precondition ==")

    def test_SGPCVoucherLandingPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCVoucherLandingPage-01.xml")

    def test_SGPCVoucherLandingPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCVoucherLandingPage-02.xml")

    def test_SGPCVoucherLandingPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCVoucherLandingPage-03.xml")

    def test_SGPCVoucherLandingPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCVoucherLandingPage-04.xml")

    def test_SGPCVoucherLandingPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCVoucherLandingPage-05.xml")

    def test_SGPCVoucherLandingPage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCVoucherLandingPage-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCVoucherLandingPage Post condition =="
        Config.logging.info("== Setup SGPCVoucherLandingPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
