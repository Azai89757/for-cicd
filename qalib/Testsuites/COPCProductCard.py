import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCProductCard Precondition =="
        dumplogger.info("== Setup COPCProductCard Precondition ==")

    def test_COPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCProductCard-01.xml")

    def test_COPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCProductCard-02.xml")

    def test_COPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCProductCard-03.xml")

    def test_COPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCProductCard-04.xml")

    def test_COPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCProductCard-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCProductCard Post condition =="
        dumplogger.info("== Setup COPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
