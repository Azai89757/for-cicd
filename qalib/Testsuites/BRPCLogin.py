import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCLogin Precondition =="
        dumplogger.info("== Setup BRPCLogin Precondition ==")

    def test_BRPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCLogin-01.xml")

    def test_BRPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCLogin Post condition =="
        dumplogger.info("== Setup BRPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
