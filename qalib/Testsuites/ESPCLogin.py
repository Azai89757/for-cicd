import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCLogin Precondition =="
        dumplogger.info("== Setup ESPCLogin Precondition ==")

    def test_ESPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCLogin-01.xml")

    def test_ESPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCLogin Post condition =="
        dumplogger.info("== Setup ESPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
