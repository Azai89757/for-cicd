#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 HomepageMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore
import BaseUILogic
import CommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class HomePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchCategoryListToLastPage(arg):
        '''
        SwitchCategoryListToLastPage : Switch category list on Shopee homepage to the last page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Check scroll icon exist or not, if exist, Click switch element.
        xpath = Util.GetXpath({"locate":"right_button"})
        for scroll_time in range(15):
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to switch right", "result": "1"})
                time.sleep(2)
            else:
                dumplogger.info("Scroll to the last category page!")
                break

        OK(ret, int(arg['result']), 'HomePage.SwitchCategoryListToLastPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHomePageCategoryExist(arg):
        '''
        CheckHomePageCategoryExist : Check if category exist
                Input argu :
                    image - OpenCV project file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]

        ##match_result = 1 : Sucessfully matched ; match_result = 0 : Fail to match
        match_result = 0

        for retry_count in range(1, 6):
            dumplogger.info("Start round: %s for checking homepage category" % (retry_count))

            ##Close staging and open
            CommonMethod.ReopenShopeeWebsite({"result": "1"})

            ##Vertical move to category list
            CommonMethod.ScrollToSection({"section":"category_section", "result": "1"})

            ##Swipe the category list
            HomePage.SwitchCategoryListToLastPage({"result": "1"})

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"web", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result": arg['result']})

            ##Whether category is exist or not, break the loop as long as the result is expected
            ##If match_result = 1, then match_result == ret -> sucessfully matched
            if match_result:
                dumplogger.info("Homepage category matching successful!!!!")
                break
            else:
                dumplogger.info("Homepage category matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'HomePage.CheckHomePageCategoryExist')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckHomePageComponents(arg):
        '''
        CheckHomePageComponents : Check if home page components exist
                Input argu :
                    image - OpenCV project file
                    scroll_section - scroll to specific section
                    is_click - click image or not
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg["image"]
        scroll_section = arg["scroll_section"]
        is_click = arg["is_click"]

        ##match_result = 1 : Sucessfully matched ; match_result = 0 : Fail to match
        match_result = 0

        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking home page components" % (retry_count))

            ##Close staging and open
            CommonMethod.ReopenShopeeWebsite({"result": "1"})

            ##Vertical move to specific section
            if scroll_section:
                BaseUILogic.MouseScrollEvent({"x": "0", "y": "1000", "result": "1"})
                BaseUILogic.MouseScrollEvent({"x": "0", "y": "0", "result": "1"})
                CommonMethod.ScrollToSection({"section":scroll_section, "result": "1"})

            ##Click right arrow to show last official shop card
            xpath = Util.GetXpath({"locate":"right_button"})
            if scroll_section == "mall_section" and BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"0", "result": "1"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to switch right", "result": "1"})
                time.sleep(2)

            ##Start to begin image compare
            match_result = XtFunc.MatchPictureImg({"mode":"web", "project": Config._TestCaseFeature_, "image":image, "env": Config._EnvType_, "threshold": "0.9", "expected_result":arg['result']})

            ##Whether home page components is exist or not, break the loop as long as the result is expected
            ##If match_result = 1, then match_result == ret -> sucessfully matched
            if match_result:
                dumplogger.info("PC homepage compoments matching successfully at %d run !!!!" % (retry_count))

                ##If need to click image (and only able to click if we expect image show up)
                if int(is_click) and int(arg['result']):
                    XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": "web", "image": image, "threshold":"0.9", "is_click":"yes", "result": "1"})
                break
            else:
                time.sleep(60)
                dumplogger.info("PC homepage compoments matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'HomePage.CheckHomePageComponents')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCountdownTimerText(arg):
        '''
        CheckCountdownTimerText : Check if count down timer text exist
                Input argu :
                    text - countdown timer text
                    text_color_rgb - text color rgb (ex: rgb(0, 0, 0))
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]
        text_color_rgb = arg["text_color_rgb"]

        ##match_result = 1 : Sucessfully matched ; match_result = 0 : Fail to match
        match_result = 0

        for retry_count in range(1, 7):
            dumplogger.info("Start round: %s for checking countdown timer." % (retry_count))

            ##Close staging and open
            CommonMethod.ReopenShopeeWebsite({"result": "1"})

            ##Check text xpath exists
            xpath = Util.GetXpath({"locate":"countdown_timer_text"})
            xpath = xpath.replace("text_to_be_replaced", text).replace("rgb_to_be_replaced", text_color_rgb)
            match_result = BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": arg['result']})
            dumplogger.info("Countdown timer text match result : %d" % (match_result))

            ##Whether countdown timer text is exist or not, break the loop as long as the result is expected
            ##If match_result = 1, then match_result == ret -> sucessfully matched
            if match_result:
                dumplogger.info("PC countdown timer text matching successfully at %d run !!!!" % (retry_count))
                break

            else:
                time.sleep(60)
                dumplogger.info("PC countdown timer text matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'HomePage.CheckCountdownTimerText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCountdownTimerBlock(arg):
        '''
        CheckCountdownTimerBlock : Check if count down timer block exist
                Input argu :
                    block_color_rgb - block color rgb (ex: rgb(0, 0, 0))
                    block_number_color_rgb - block number color rgb (ex: rgb(0, 0, 0))
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        block_color_rgb = arg["block_color_rgb"]
        block_number_color_rgb = arg["block_number_color_rgb"]

        ##match_result = 1 : Sucessfully matched ; match_result = 0 : Fail to match
        match_result = 0

        for retry_count in range(1, 7):
            dumplogger.info("Start round: %s for checking countdown timer." % (retry_count))

            ##Close staging and open
            CommonMethod.ReopenShopeeWebsite({"result": "1"})

            ##Check countdown timer block xpath exists
            xpath = Util.GetXpath({"locate":"countdown_timer_block"})
            xpath = xpath.replace("block_color_to_be_replaced", block_color_rgb).replace("block_number_color_to_be_replaced", block_number_color_rgb)
            match_result = BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": arg['result']})
            dumplogger.info("Countdown timer block match result : %d" % (match_result))

            ##Whether countdown timer block is exist or not, break the loop as long as the result is expected
            ##If match_result = 1, then match_result == ret -> sucessfully matched
            if match_result:
                dumplogger.info("PC countdown timer block matching successfully at %d run !!!!" % (retry_count))
                break

            else:
                time.sleep(60)
                dumplogger.info("PC countdown timer block matching failed at %d run..." % (retry_count))

        OK(ret, match_result, 'HomePage.CheckCountdownTimerBlock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoFlashSalePage(arg):
        '''
        GotoFlashSalePage : Go to flash sale page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Go to flashsale page
        xpath = Util.GetXpath({"locate":"label_see_all"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Goto flash sale page", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.GotoFlashSalePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        '''
        ClickCategory : Click category
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Get category xpath and replace by category_name
        xpath = Util.GetXpath({"locate":"category"})
        xpath_target_category = xpath.replace("category_name_to_be_replace", category_name)

        ##Click category with category_name
        BaseUICore.Click({"method":"xpath", "locate":xpath_target_category, "message":"click category", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ClickCategory with => ' + category_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRandomUsername(arg):
        '''
        GetRandomUsername : Get random username in homepage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get random username in homepage
        xpath = Util.GetXpath({"locate":"account_name"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        GlobalAdapter.GeneralE2EVar._RandomString_ = GlobalAdapter.CommonVar._PageAttributes_

        OK(ret, int(arg['result']), 'HomePage.GetRandomUsername')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickWelcomePackageBanner(arg):
        '''
        ClickWelcomePackageBanner : Click welcome package banner
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click welcome package banner
        xpath = Util.GetXpath({"locate":"welcomepackage_banner"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click welcome package banner", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ClickWelcomePackageBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCampaignModuleSeeMore(arg):
        '''
        ClickCampaignModuleSeeMore : Click see more of campaign module
                Input argu :
                    title - module title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]

        ##Click see more
        xpath = Util.GetXpath({"locate":"see_more_btn"})
        xpath = xpath.replace("name_to_be_replaced", title)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see more", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ClickCampaignModuleSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeShopeeLanguage(arg):
        '''
        ChangeShopeeLanguage : Change shopee language on the top bar section
                Input argu :
                    language - which language you want to change to
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        language = arg["language"]

        ##Click current language on the top bar section
        xpath = Util.GetXpath({"locate":"current_language"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click current language", "result": "1"})

        ##Click language type on the language list
        xpath = Util.GetXpath({"locate": language})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click language : " + language, "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ChangeShopeeLanguage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupBanner(arg):
        '''
        ClickPopupBanner : Click popup banner
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click popup banner by javascript
        BaseUICore.ExecuteScript({"script": "document.querySelector('shopee-banner-popup-stateful').shadowRoot.querySelector('shopee-banner-simple').shadowRoot.querySelector('img').click()", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ClickPopupBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSkinnyBanner(arg):
        '''
        ClickSkinnyBanner : Click skinny banner
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click skinny banner by javascript
        BaseUICore.ExecuteScript({"script": "document.querySelector('shopee-vertical-stack-ui').shadowRoot.querySelector('shopee-banner-simple').shadowRoot.querySelector('img').click()", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ClickSkinnyBanner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHomeCarouselBanner(arg):
        '''
        ClickHomeCarouselBanner : Click home carousel banner
                Input argu :
                    index - which banner you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg["index"]

        ##Click home carousel banner
        xpath = Util.GetXpath({"locate": "home_carousel_banner"})
        xpath = xpath.replace("index_to_be_replaced", index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click home carousel banner", "result": "1"})

        OK(ret, int(arg['result']), 'HomePage.ClickHomeCarouselBanner')

class HomePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more button
                Input argu :
                    homepage_component - Click the see more button of which home page component
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        homepage_component = arg["homepage_component"]

        ##Click see more button
        xpath = Util.GetXpath({"locate":"see_more"})
        xpath = xpath.replace("name_to_be_replaced", homepage_component)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see more btn", "result": "1"})

        OK(ret, int(arg['result']), 'HomePageButton.ClickSeeMore')
