import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCPLPCProductCard Precondition =="
        dumplogger.info("== Setup PLPCPLPCProductCard Precondition ==")

    def test_PLPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductCard-01.xml")

    def test_PLPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductCard-02.xml")

    def test_PLPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductCard-03.xml")

    def test_PLPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductCard-04.xml")

    def test_PLPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCProductCard-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCProductCard Post condition =="
        dumplogger.info("== Setup PLPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
