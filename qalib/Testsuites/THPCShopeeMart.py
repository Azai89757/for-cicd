import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class THPCShopeeMart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCShopeeMart Precondition =="
        dumplogger.info("== Setup THPCShopeeMart Precondition ==")

    def test_THPCShopeeMart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCShopeeMart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCShopeeMart Post condition =="
        dumplogger.info("== Setup THPCShopeeMart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
