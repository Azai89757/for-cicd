#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import re
import os
import time
import json
import datetime
import csv

##Import Framework common library
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic
import PromotionNewAdminAPIMethod
import OrderAdminAPIMethod
from SpaceAPIMethod import SpaceCronjobAPI

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminCommonAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignAdminCookie(arg):
        '''
        AssignAdminCookie : Assign admin already login cookie into API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = "admin_cookie"

        ##Get current env and country
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        GlobalAdapter.APIVar._HttpCookie_["sessionid"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'AdminCommonAPI.AssignAdminCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCsrfToken(arg):
        '''
        AssignCsrfToken : assign csrftoken to header
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Assign x-csrftoken
        GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpCookie_["csrftoken"] = GlobalAdapter.APIVar._Csrftoken_

        OK(ret, int(arg['result']), 'AdminCommonAPI.AssignCsrfToken')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AdminLogin(arg):
        '''
        AdminLogin : Login admin with API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Create new request
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/adminlogin", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": Config._TestCaseRegion_.lower() + "_" + Config._EnvType_, "result": "1"})

        ##Assing admin cookie
        AdminCommonAPI.AssignAdminCookie({"result": "1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Assign csrftoken to global
        GlobalAdapter.APIVar._Csrftoken_ = GlobalAdapter.APIVar._APIResponse_["cookie"]["csrftoken"]

        ##Assign x-csrftoken
        GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpCookie_["csrftoken"] = GlobalAdapter.APIVar._Csrftoken_

        OK(ret, int(arg['result']), 'AdminCommonAPI.AdminLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCMSAdminCookie(arg):
        '''
        AssignCMSAdminCookie : Assign CMS admin login cookie into API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = "admin_cookie_dl"
        dl_admin_cookie = {}

        ##Get current env and country
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        dl_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        dl_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign CMS admin cookies
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'AdminCommonAPI.AssignCMSAdminCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCMTAdminCookie(arg):
        '''
        AssignCMTAdminCookie : Assign CMT login cookie into API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = "cmt_cookie"

        ##Get current env and country
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Assign CMT admin cookies
        GlobalAdapter.APIVar._Csrftoken_ = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'AdminCommonAPI.AssignCMTAdminCookie')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignUserPortalCookie(arg):
        '''
        AssignUserPortalCookie : Assign User Portal login cookie into API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_portal_admin_cookie = {}
        db_file = "admin_cookie_user_portal"

        ##Get current env and country
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name": "qa", "file_name": db_file, "method": "select", "verify_result": "", "assign_data_list": assign_list, "store_data_list": store_list, "result": "1"})

        ##Store cookie name and value
        user_portal_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        user_portal_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        user_portal_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]

        ##Assign User Portal cookies
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        OK(ret, int(arg['result']), 'AdminCommonAPI.AssignUserPortalCookie')
class AdminPromotionAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreditCoins(arg):
        '''
        CreditCoins : Manual credit coins using Admin API
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Post credit coins data
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "add_credit_coin_request", "result": "1"})
        AdminCommonAPI.AssignAdminCookie({"result": "1"})
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get manual credit coins request id
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_manual_credit_coin_list", "result": "1"})
        AdminCommonAPI.AssignAdminCookie({"result": "1"})
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.StoreDataFromAPIResponse({"column": "request_id", "result": "1"})

        ##Confirm manual credit coins
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "confirm_credit_coin", "result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["request_ids"] = str(GlobalAdapter.CommonVar._DynamicCaseData_["request_id"])
        AdminCommonAPI.AssignAdminCookie({"result": "1"})
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Deinitial api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreditCoins')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetUpcomingFlashSaleResponseStatus(arg):
        '''
        GetUpcomingFlashSaleResponseStatus : Get upcoming flash sale response status by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Get admin upcoming flash sale api response
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/FlashSale/get_upcoming_fs_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        XtFunc.ResetWebElementsList({"reset_element": "upcoming_flashsale_status", "result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "upcoming_flash_sale_status", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.GetUpcomingFlashSaleResponseStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOngoingFlashSaleResponseStatus(arg):
        '''
        GetOngoingFlashSaleResponseStatus : Get ongoing flash sale response status by API
                Input argu :
                    session_type - cfs_session / mfs_session
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        session_type = arg['session_type']

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Get admin ongoing flash sale api response
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/FlashSale/get_ongoing_fs_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        if session_type == "cfs_session":
            ##Assign request headers referer into mfs session
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_type_to_be_replaced", "fs-flash-sale")

        elif session_type == "mfs_session":
            ##Assign request headers referer into mfs session
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_type_to_be_replaced", "fs-mall-flash-sale")

        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        XtFunc.ResetWebElementsList({"reset_element": "ongoing_flashsale_status", "result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "ongoing_flash_sale_status", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.GetOngoingFlashSaleResponseStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateFlashSale(arg):
        '''
        CreateFlashSale : Create flash sale
                Input argu :
                    flash_sale_type - CFS / MFS / ISFS
                    start_time - time slot start time
                    end_time - time slot end time
                    nomination_start_time - nomination start time
                    nomination_end_time - nomination end time
                    name - session name and cluster name
                    description - nomination description
                    upload_product_file_name - upload product file name(.csv)
                    type - all : type all is for daily run cases
                           single : type single is for local cases
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        flash_sale_type = arg["flash_sale_type"]
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        nomination_start_time = arg['nomination_start_time']
        nomination_end_time = arg['nomination_end_time']
        name = arg['name']
        description = arg['description']
        upload_product_file_name = arg['upload_product_file_name']
        type = arg['type']

        ##Get CMT Admin cookie
        AdminCommonAPI.AssignCMTAdminCookie({"result": "1"})

        ##If type is all, restore ongoing flash sale and create new flash sale for other feature that need upload flash sale products
        if type == "all":
            ##Restore ongoing flash sale
            AdminPromotionAPI.RestoreFlashSale({"result": "1"})

            ##Create common flash sale time session by API
            AdminPromotionAPI.CreateCFSSession({"start_time": start_time, "end_time": end_time, "nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "name": name, "description": description, "is_ISFS": 0, "result": "1"})

        ##If type is single, TWQA daily run flash sale upcoming exist and upload flash sale products
        elif type == "single":
            ##Reset global flash sale list
            XtFunc.ResetWebElementsList({"reset_element": "promotionid", "result": "1"})

            ##Get admin ongoing flash sale api response
            AdminPromotionAPI.GetOngoingFlashSaleResponseStatus({"session_type": "cfs_session", "result": "1"})

            ##Get admin upcoming flash sale api response
            AdminPromotionAPI.GetUpcomingFlashSaleResponseStatus({"result": "1"})

            ##Get daily run flash sale id
            for flash_sale in GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_:
                ##Daily run upcoming flash sale status is 1 and name is TWQA auto test for daily run
                if flash_sale['name'] == "TWQA auto test for daily run":
                    GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(str(flash_sale['promotion_id']))
                    GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = flash_sale['session_id']
                    AdminPromotionAPI.SearchClusterId({"result": "1"})
                    dumplogger.info("Daily run upcomiong flash sale id = %s" % GlobalAdapter.PromotionE2EVar._PromotionIDList_)

            if GlobalAdapter.PromotionE2EVar._PromotionIDList_:
                ##Upload, confirm and approve flash sale Product
                AdminPromotionAPI.UploadCFSProducts({"upload_product_file_name": upload_product_file_name, "result": "1"})

                ##Refresh flash sale
                AdminPromotionAPI.RefreshFlashSale({"session_type": "cfs_session","result": "1"})
                dumplogger.info("Daily run upcoming flash sale exist.")

                ##Check if Ongoing flash sale exist
                if GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_:
                    ##Restore ongoing flash sale
                    AdminPromotionAPI.RestoreFlashSale({"result": "1"})
                    dumplogger.info("Restore ongoing flash sale -> %s" % (GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_))

                ##wait flash sale ongoing
                time.sleep(630)
            else:
                ##Daily run upcomong flash sale didn't exist and create new flash sale by API
                if flash_sale_type == "CFS":
                    ##Create session in CFS
                    AdminPromotionAPI.CreateCFSSession({"start_time": start_time, "end_time": end_time, "nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "name": name, "description": description, "is_ISFS": 0, "result": "1"})
                    time.sleep(60)

                    ##Upload, confirm and approve CFS Product
                    AdminPromotionAPI.UploadCFSProducts({"upload_product_file_name": upload_product_file_name, "result": "1"})

                    ##Refresh CFS flash sale
                    AdminPromotionAPI.RefreshFlashSale({"session_type": "cfs_session","result": "1"})

                elif flash_sale_type == "ISFS":
                    ##Create session in CFS and have ISFS
                    AdminPromotionAPI.CreateCFSSession({"start_time": start_time, "end_time": end_time, "nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "name": name, "description": description, "is_ISFS": 1, "result": "1"})
                    time.sleep(60)

                    ##Upload and confirm ISFS products
                    AdminPromotionAPI.UploadISFSProducts({"upload_product_file_name": upload_product_file_name,"result": "1"})

                    ##Refresh CFS flash sale
                    AdminPromotionAPI.RefreshFlashSale({"session_type": "cfs_session","result": "1"})

                elif flash_sale_type == "MFS":
                    ##Create session in MFS and have ISFS
                    AdminPromotionAPI.CreateMFSSession({"start_time": start_time, "end_time": end_time, "nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "name": name, "description": description, "result": "1"})
                    time.sleep(60)

                    ##Upload, confirm and approve MFS Product
                    AdminPromotionAPI.UploadMFSProducts({"upload_product_file_name": upload_product_file_name, "result": "1"})

                    ##Refresh MFS flash sale
                    AdminPromotionAPI.RefreshFlashSale({"session_type": "mfs_session","result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateCFSSession(arg):
        '''
        CreateCFSSession : Create new CFS time slot and session by API
                Input argu :
                    start_time - time slot start time
                    end_time - time slot end time
                    nomination_start_time - nomination start time
                    nomination_end_time - nomination end time
                    name - session name and cluster name
                    description - nomination description
                    is_ISFS - 1(Is in shop flash sale) / 0(Not in shop flash sale)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        nomination_start_time = arg['nomination_start_time']
        nomination_end_time = arg['nomination_end_time']
        name = arg['name']
        description = arg['description']
        is_ISFS = int(arg["is_ISFS"])

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Create timeslot by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_timeslot", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        AdminPromotionAPI.AssignTimePeroidForFlashSale({"start_time": start_time, "end_time": end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._TimeSlotID_ = response["data"]["time_slot_id"]
        dumplogger.info("_TimeSlotID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._TimeSlotID_))
        time.sleep(8)

        ##Create CFS session by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_CFS_session", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["time_slot_id"] = int(GlobalAdapter.FlashSaleE2EVar._TimeSlotID_)
        GlobalAdapter.APIVar._HttpPayload_["session_name"] = name
        GlobalAdapter.APIVar._HttpPayload_["nomination_description"] = description

        ##ISFS does not need to fill in 'shopee nomination' section
        if is_ISFS:
            GlobalAdapter.APIVar._HttpPayload_["is_shopee_nomination"] = 0
        else:
            ##Fill in 'shopee nomination' section other than ISFS
            AdminPromotionAPI.AssignTimePeroidForNominationFlashSale({"nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "result": "1"})

        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = response["data"]["id"]
        dumplogger.info("_CFSSessionID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_))
        time.sleep(8)

        ##Create cluster by API (ISFS does not need)
        if not is_ISFS:
            HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_cluster", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
            GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
            GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
            GlobalAdapter.APIVar._HttpPayload_["cluster_name"] = name
            GlobalAdapter.APIVar._HttpPayload_["nomination_description"] = description
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            APICommonMethod.CheckAPIResponseText({"result": "1"})
            response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
            GlobalAdapter.FlashSaleE2EVar._CFSClusterID_ = response["data"]["id"]
            dumplogger.info("_CFSClusterID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._CFSClusterID_))

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateCFSSession')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateMFSSession(arg):
        '''
        CreateMFSSession : Add mfs session after create flash sale
                Input argu :
                    start_time - time slot start time
                    end_time - time slot end time
                    nomination_start_time - nomination start time
                    nomination_end_time - nomination end time
                    name - session name and cluster name
                    description - nomination description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        nomination_start_time = int(arg['nomination_start_time'])
        nomination_end_time = int(arg['nomination_end_time'])
        name = arg['name']
        description = arg['description']

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Create timeslot by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_timeslot", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        AdminPromotionAPI.AssignTimePeroidForFlashSale({"start_time": start_time, "end_time": end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._TimeSlotID_ = response["data"]["time_slot_id"]
        dumplogger.info("_TimeSlotID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._TimeSlotID_))
        time.sleep(8)

        ##Create CFS session by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_CFS_session", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["time_slot_id"] = int(GlobalAdapter.FlashSaleE2EVar._TimeSlotID_)
        GlobalAdapter.APIVar._HttpPayload_["session_name"] = name
        GlobalAdapter.APIVar._HttpPayload_["nomination_description"] = description
        AdminPromotionAPI.AssignTimePeroidForNominationFlashSale({"nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = response["data"]["id"]
        dumplogger.info("_CFSSessionID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_))
        time.sleep(8)

        ##Create cluster by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_cluster", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["cluster_name"] = name
        GlobalAdapter.APIVar._HttpPayload_["nomination_description"] = description
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._CFSClusterID_ = response["data"]["id"]
        dumplogger.info("_CFSClusterID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._CFSClusterID_))

        ##Create MFS session by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_MFS_session", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["time_slot_id"] = int(GlobalAdapter.FlashSaleE2EVar._TimeSlotID_)
        GlobalAdapter.APIVar._HttpPayload_["session_name"] = name
        GlobalAdapter.APIVar._HttpPayload_["nomination_description"] = description
        AdminPromotionAPI.AssignTimePeroidForNominationFlashSale({"nomination_start_time": nomination_start_time, "nomination_end_time": nomination_end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._MFSSessionID_ = response["data"]["id"]
        dumplogger.info("_MFSSessionID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_))
        time.sleep(8)

        ##Create cluster by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_mfs_cluster", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("mfs_session_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_))
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["cluster_name"] = name
        GlobalAdapter.APIVar._HttpPayload_["nomination_description"] = description
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._MFSClusterID_ = response["data"]["id"]
        dumplogger.info("_MFSClusterID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._MFSClusterID_))

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateMFSSession')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifiedISFSProductCSV(arg):
        ''' ModifiedISFSProductCSV : To write in new cfs promotion id
                Input argu :
                    file_name - file name for uploading
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##File path
        slash = Config.dict_systemslash[Config._platform_]
        file_path = os.path.join(Config._UICaseDataFilePath_, Config._TestCaseRegion_, Config._EnvType_, Config._TestCasePlatform_.lower(), Config._TestCaseFeature_, 'file', file_name + '.csv')

        ##Get promotion id from GlobalAdapter.PromotionE2EVar
        AdminPromotionAPI.GetPromotionId({"session_type": "cfs_session", "result": "1"})
        dumplogger.info("input cfs promotion id: " + GlobalAdapter.FlashSaleE2EVar._PromotionID_)

        ##Modify promotion id in csv(write in the new one)
        data_list = XtFunc.CsvReader(file_path)

        for row in range(2, len(data_list)+1, 1):
            ##rewrite every cfs promotion id field
            XtFunc.CsvWritter({"input_row":row, "input_column":"1", "value":GlobalAdapter.FlashSaleE2EVar._PromotionID_, "data_list":data_list, "file_path":file_path, "result":arg['result']})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.ModifiedISFSProductCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadISFSProducts(arg):
        '''
        UploadISFSProducts : Upload In-Shop flash sale products by API
                Input argu :
                    upload_product_file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_product_file_name = arg['upload_product_file_name']

        ##Get modified csv(modify cfs promotion id field)
        AdminPromotionAPI.ModifiedISFSProductCSV({"file_name": upload_product_file_name, "result": "1"})

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Upload in-shop flash sale products by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/upload_isfs_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})

        ##File path
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + upload_product_file_name + ".csv"

        ##assign file name into payload 'file_name' param
        GlobalAdapter.APIVar._HttpPayload_["file_name"] = upload_product_file_name + ".csv"

        ##Get param: 'models'
        with open(file_path, 'rb') as ReadCSV:
            csv_file = ReadCSV.read()
            row_index = 0
            item_list = []

            for row in csv_file.splitlines():
                item_list_rows = {}
                if row_index != 0:
                    item_list_rows["cfs_promotion_id"] = int(row.split(',')[0])
                    item_list_rows["shop_id"] = int(row.split(',')[1])
                    item_list_rows["item_id"] = int(row.split(',')[2])
                    item_list_rows["model_id"] = int(row.split(',')[3])
                    item_list_rows["promotion_price"] = int(row.split(',')[4])*100000
                    item_list_rows["purchase_limit"] = int(row.split(',')[5])
                    item_list_rows["stock"] = int(row.split(',')[6])
                    item_list.append(item_list_rows)
                row_index += 1

            dumplogger.info("param-models: " + item_list)

        ##After assemble, assign it into http payload 'models' param
        GlobalAdapter.APIVar._HttpPayload_["models"] = item_list
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Cookie
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminPromotionAPI.UploadISFSProducts')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCFSProducts(arg):
        '''
        UploadCFSProducts : Upload flash sale products by API
                Input argu :
                    upload_product_file_name - upload file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_product_file_name = arg['upload_product_file_name']

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Upload flash sale products by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/upload_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["cluster_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSClusterID_)
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + upload_product_file_name + ".csv"
        GlobalAdapter.APIVar._HttpUploadFiles_ = {
            'csv_file': open(file_path, 'rb'),
        }
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._UploadProductList_ = response["data"]["success"]

        ##Confirm flash sale product by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/comfirm_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["cluster_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSClusterID_)
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["products"] = GlobalAdapter.FlashSaleE2EVar._UploadProductList_
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Search flash sale product id by API
        AdminPromotionAPI.GetCFSProductId({"result": "1"})

        ##Approve flash sale products by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/approve_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["ids"] = GlobalAdapter.FlashSaleE2EVar._ProductIDList_
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Approve flash sale products with product discount by API
        if len(GlobalAdapter.FlashSaleE2EVar._ExtraDiscountProductIDList_) > 0:
            HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/approve_extra_discount_product", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
            GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
            GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
            GlobalAdapter.APIVar._HttpPayload_["ids"] = GlobalAdapter.FlashSaleE2EVar._ExtraDiscountProductIDList_
            APICommonMethod.ChangePayloadDictToStr({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.UploadCFSProducts')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadMFSProducts(arg):
        '''
        UploadMFSProducts : Upload mall flash sale products by API
                Input argu :
                    upload_product_file_name - upload file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_product_file_name = arg['upload_product_file_name']

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Upload flash sale products by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/upload_mfs_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)).replace("cluster_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._MFSClusterID_))
        GlobalAdapter.APIVar._HttpPayload_["cluster_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSClusterID_)
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + upload_product_file_name + ".csv"
        GlobalAdapter.APIVar._HttpUploadFiles_ = {
            'csv_file': open(file_path, 'rb'),
        }
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._UploadProductList_ = response["data"]["success"]

        ##Confirm flash sale product by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/comfirm_mfs_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)).replace("cluster_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._MFSClusterID_))
        GlobalAdapter.APIVar._HttpPayload_["cluster_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSClusterID_)
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["products"] = GlobalAdapter.FlashSaleE2EVar._UploadProductList_
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Search flash sale product id by API
        AdminPromotionAPI.GetMFSProductId({"result": "1"})

        ##Approve flash sale products by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/approve_mfs_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_))
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["ids"] = GlobalAdapter.FlashSaleE2EVar._ProductIDList_
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.UploadMFSProducts')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetCFSProductId(arg):
        '''
        GetCFSProductId : Search flash sale product id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Search flash sale product id by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/search_productid", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Get all product id
        XtFunc.ResetWebElementsList({"reset_element": "flash_sale_product_id", "result": "1"})
        if len(response["data"]["list"]) > 0:
            for product in range(len(response["data"]["list"])):
                models = response["data"]["list"][product]["models"]
                GlobalAdapter.FlashSaleE2EVar._ProductIDList_.append(models[0]["id"])

                ##If product has extra discount get product id
                if models[0]["product_has_extra_discount"] == 1:
                    GlobalAdapter.FlashSaleE2EVar._ExtraDiscountProductIDList_.append(models[0]["id"])

            dumplogger.info("_ProductIDList_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._ProductIDList_))
            dumplogger.info("_ExtraDiscountProductIDList_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._ExtraDiscountProductIDList_))

        OK(ret, int(arg['result']), 'AdminPromotionAPI.GetCFSProductId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetMFSProductId(arg):
        '''
        GetMFSProductId : Search mall flash sale product id and save it into product id list
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Search flash sale product id by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/search_productid", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Get all product id
        if len(response["data"]["list"]) > 0:
            for product in range(len(response["data"]["list"])):
                models = response["data"]["list"][product]["models"]
                GlobalAdapter.FlashSaleE2EVar._ProductIDList_.append(models[0]["id"])
            dumplogger.info("_ProductIDList_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._ProductIDList_))

        OK(ret, int(arg['result']), 'AdminPromotionAPI.GetMFSProductId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetPromotionId(arg):
        '''
        GetPromotionId : Search promotion id
                Input argu :
                    session_type - cfs_session / mfs_session
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        session_type = arg['session_type']

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Search promotion id by API and store promotion id
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/search_promotionid", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

        if session_type == "cfs_session":
            ##Assign request headers referer into cfs session
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_type_to_be_replaced", "cfs-sessions")

            ##Assign request payload session id to use cfs session id
            GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
            GlobalAdapter.APIVar._HttpPayload_["session_type"] = 1

        elif session_type == "mfs_session":
            ##Assign request headers referer into mfs session
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_type_to_be_replaced", "mfs-sessions")

            ##Assign request payload session id to use mfs session id
            GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._MFSSessionID_)
            GlobalAdapter.APIVar._HttpPayload_["session_type"] = 2

        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._PromotionID_ = response["data"]["list"][0]["promotion_id"]
        dumplogger.info("_PromotionID_ : %s" % str(GlobalAdapter.FlashSaleE2EVar._PromotionID_))

        OK(ret, int(arg['result']), 'AdminPromotionAPI.GetPromotionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RefreshFlashSale(arg):
        '''
        RefreshFlashSale : Refresh flash sale by API
                Input argu :
                    session_type - cfs_session / mfs_session
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        session_type = arg['session_type']

        ##Get promotion id
        AdminPromotionAPI.GetPromotionId({"session_type": session_type, "result": "1"})

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Refresh flash sale by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/refresh_flashsale", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})

        if session_type == "cfs_session":
            ##Assign request headers referer into mfs session
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_type_to_be_replaced", "fs-flash-sale")

        elif session_type == "mfs_session":
            ##Assign request headers referer into mfs session
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("session_type_to_be_replaced", "fs-mall-flash-sale")

        GlobalAdapter.APIVar._HttpPayload_["flash_sale_id"] = int(GlobalAdapter.FlashSaleE2EVar._PromotionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.RefreshFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchClusterId(arg):
        '''
        SearchClusterId : Search cluster id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Search cluster id by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/search_clusterid", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._CFSClusterID_ = response["data"]["list"][0]["id"]

        OK(ret, int(arg['result']), 'AdminPromotionAPI.SearchClusterId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTimeSlotId(arg):
        '''
        GetTimeSlotId : Get time slot id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Search cluster id by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/get_timeslot_id", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        GlobalAdapter.FlashSaleE2EVar._TimeSlotID_ = response["data"]["list"][0]["time_slot_id"]

        OK(ret, int(arg['result']), 'AdminPromotionAPI.SearchTimeSlotId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RejectFlashSaleProduct(arg):
        '''
        RejectFlashSaleProduct : Reject flash sale product
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##if session value is empty, get value
        if not (GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ and GlobalAdapter.FlashSaleE2EVar._ProductIDList_):
            ##Get ongoing flash sale status
            AdminPromotionAPI.GetOngoingFlashSaleResponseStatus({"session_type": "cfs_session", "result": "1"})
            if GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_:
                for flash_sale in GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_:
                    GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = flash_sale['session_id']
                ##Search need to reject products
                AdminPromotionAPI.GetCFSProductId({"result": "1"})

        ##Reject flash sale product by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/reject_product", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["session_id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        GlobalAdapter.APIVar._HttpPayload_["ids"] = GlobalAdapter.FlashSaleE2EVar._ProductIDList_
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.RejectFlashSaleProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteCFSSession(arg):
        '''
        DeleteCFSSession : Delete CFS session
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Reject flash sale product by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/delete_CFS_session", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpPayload_["id"] = int(GlobalAdapter.FlashSaleE2EVar._CFSSessionID_)
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.DeleteCFSSession')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetTimeSlot(arg):
        '''
        ResetTimeSlot : Reset time slot
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Get time slot id
        GlobalAdapter.CommonVar._DynamicCaseData_["id"] = int(GlobalAdapter.FlashSaleE2EVar._TimeSlotID_)

        ##Define assign list
        assign_list = [{"column": "end_time", "value_type": "cur_unix_time"}, {"column": "id", "value_type": "number"}]

        ##Send SQL command process
        DBCommonMethod.SendSQLCommandProcess({"db_name": "staging_CMT_flash_sale", "file_name": "flash_sale_info", "method": "update", "verify_result": "","assign_data_list": assign_list, "store_data_list": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.ResetTimeSlot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreFlashSale(arg):
        '''
        RestoreFlashSale : Restore flash sale by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get cfs ongoing flash sale status
        AdminPromotionAPI.GetOngoingFlashSaleResponseStatus({"session_type": "cfs_session", "result": "1"})

        ##Check if ongoing flash sale exist
        if GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_:
            dumplogger.info("Ongoing flash sale exist.")

            ##Get CFS session id
            for flash_sale in GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_:
                GlobalAdapter.FlashSaleE2EVar._CFSSessionID_ = flash_sale['session_id']

            ##Get CFS products to reject
            AdminPromotionAPI.GetCFSProductId({"result": "1"})

            if len(GlobalAdapter.FlashSaleE2EVar._ProductIDList_) > 0:
                ##Reject flash sale product to set product to normal status
                AdminPromotionAPI.RejectFlashSaleProduct({"result": "1"})
                time.sleep(3)

            ##Get time slot id
            AdminPromotionAPI.GetTimeSlotId({"result": "1"})

            ##Delete CFS session
            AdminPromotionAPI.DeleteCFSSession({"result": "1"})

            ##Reset time slot
            AdminPromotionAPI.ResetTimeSlot({"result": "1"})
            dumplogger.info("Reject flash sale product and reset flash sale time slot.")
        else:
            dumplogger.info("No ongoing flash sale.")

        OK(ret, int(arg['result']), 'AdminPromotionAPI.RestoreFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateAnimationMessage(arg):
        '''
        CreateAnimationMessage : Create animatio message by API
                Input argu :
                    start_time - time slot start time
                    end_time - time slot end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        ret = 1

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Create animatio message by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/create_animatio_msg", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        AdminPromotionAPI.AssignTimePeroidForFlashSale({"start_time": start_time, "end_time": end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateAnimationMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreLogisticPromotion(arg):
        '''
        RestoreLogisticPromotion : Restore logistic promotion by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get ongoing logistic promotion by admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_ongoing_logistic_promotion", "result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Reserve logistic promotion id
        if GlobalAdapter.PromotionE2EVar._PromotionIDList_:
            dumplogger.info("GlobalAdapter.PromotionE2EVar._PromotionIDList_ = %s" % str(GlobalAdapter.PromotionE2EVar._PromotionIDList_))
        else:
            logistic_promotion_list = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
            for logistic_promotion in logistic_promotion_list:
                if "for_daily_run" in logistic_promotion["description"]:
                    GlobalAdapter.PromotionE2EVar._PromotionIDList_.append(str(logistic_promotion["id"]))
            dumplogger.info("GlobalAdapter.PromotionE2EVar._PromotionIDList_ = %s" % str(GlobalAdapter.PromotionE2EVar._PromotionIDList_))

        ##Delete logistic promotion by admin API
        for promotion_rule_id in GlobalAdapter.PromotionE2EVar._PromotionIDList_:
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "delete_promotion_rule", "result": "1"})
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", promotion_rule_id)
            AdminCommonAPI.AssignCsrfToken({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "delete", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.RestoreLogisticPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForFlashSale(arg):
        '''
        AssignTimePeroidForFlashSale : Assign time peroid for create flash sale API
                Input argu :
                    start_time : flash sale start time, now time stamp add input minute
                    end_time : flash sale end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create flashsale api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForSellerDiscount(arg):
        '''
        AssignTimePeroidForSellerDiscount : Assign time peroid for create seller discount API
                Input argu :
                    promotion_start : seller discount start time, now time add input minute
                    promotion_end : seller discount end time, now time add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start time
            start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)

            ##Get end time
            end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)

            ##Insert promotion start time and promotion end time into create seller discount api payload
            GlobalAdapter.APIVar._HttpPayload_["promotion_start"] = start_datetime
            GlobalAdapter.APIVar._HttpPayload_["promotion_end"] = end_datetime
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['promotion_start'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["promotion_start"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['promotion_end'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["promotion_end"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AssignTimePeroidForSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForNominationFlashSale(arg):
        '''
        AssignTimePeroidForNominationFlashSale : Assign time peroid for create flash sale API
                Input argu :
                    nomination_start_time : flash sale nomination start time, now time stamp add input minute
                    nomination_end_time : flash sale nomination end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        nomination_start_time = int(arg['nomination_start_time'])
        nomination_end_time = int(arg['nomination_end_time'])

        try:
            ##Get nomination start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=nomination_start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get nomination end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=nomination_end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create flashsale api payload
            GlobalAdapter.APIVar._HttpPayload_["shopee_nomination_start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["shopee_nomination_end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['shopee_nomination_start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["shopee_nomination_start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['shopee_nomination_end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["shopee_nomination_end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForSNFlashSale(arg):
        '''
        AssignTimePeroidForDPFlashSale : Assign time peroid for create Allow Self Nomination setting flash sale API
                Input argu :
                    nominate_start_time : flash sale start Nomination time, now time stamp add input minute
                    nominate_end_time : flash sale end Nomination time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        nominate_start_time = int(arg['nominate_start_time'])
        nominate_end_time = int(arg['nominate_end_time'])

        try:
            ##Insert promotion start time and promotion end time into create Allow Digital Product Nomination setting flashsale api payload
            GlobalAdapter.APIVar._HttpPayload_["nominate_start"] = (datetime.datetime.now() + datetime.timedelta(days=0, minutes=nominate_start_time, seconds=0)).strftime("%Y-%m-%d %H:%M")
            GlobalAdapter.APIVar._HttpPayload_["nominate_end"] = (datetime.datetime.now() + datetime.timedelta(days=0, minutes=nominate_end_time, seconds=0)).strftime("%Y-%m-%d %H:%M")
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['nominate_start'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["nominate_start"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['nominate_end'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["nominate_end"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForSNFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForDPFlashSale(arg):
        '''
        AssignTimePeroidForDPFlashSale : Assign time peroid for create Allow Digital Product Nomination setting flash sale API
                Input argu :
                    dp_nominate_start_time : flash sale start Digital Product Nomination time, now time stamp add input minute
                    dp_nominate_end_time : flash sale end Digital Product Nomination time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dp_nominate_start_time = int(arg['dp_nominate_start_time'])
        dp_nominate_end_time = int(arg['dp_nominate_end_time'])

        try:
            ##Insert promotion start time and promotion end time into create Allow Digital Product Nomination setting flashsale api payload
            GlobalAdapter.APIVar._HttpPayload_["dp_nominate_start"] = (datetime.datetime.now() + datetime.timedelta(days=0, minutes=dp_nominate_start_time, seconds=0)).strftime("%Y-%m-%d %H:%M")
            GlobalAdapter.APIVar._HttpPayload_["dp_nominate_end"] = (datetime.datetime.now() + datetime.timedelta(days=0, minutes=dp_nominate_end_time, seconds=0)).strftime("%Y-%m-%d %H:%M")
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['dp_nominate_start'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["dp_nominate_start"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['dp_nominate_end'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["dp_nominate_end"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForDPFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForBrandSale(arg):
        '''
        AssignDataForBrandSale : Assign data for create brand sale API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Replace API payload flash sale promotion id to global stored promotion id
            if GlobalAdapter.PromotionE2EVar._PromotionIDList_:
                GlobalAdapter.APIVar._HttpPayload_["flash_sale_promotionid"] = GlobalAdapter.PromotionE2EVar._PromotionIDList_[0]

            ##Assign API csrftoken
            GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"] = GlobalAdapter.APIVar._HttpCookie_["csrftoken"]

            ##Log API variable
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['flash_sale_promotionid'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["flash_sale_promotionid"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_['x-csrftoken'] = %s" % (GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignDataForBrandSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimeForAddOnDeal(arg):
        '''
        AssignTimeForAddOnDeal : Assign time for add on deal API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Insert add on deal start time and add on deal end time into create add on deal api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = (datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)).strftime("%Y-%m-%d %H:%M")
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = (datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)).strftime("%Y-%m-%d %H:%M")
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimeForAddOnDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForPromotion(arg):
        '''
        AssignDataForPromotion : Assign data for promotion API
                Input argu :
                    promotion_type - seller_voucher / free_shipping_voucher / shopee_voucher / partner_voucher / dp_voucher / redeem_partner_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_type = arg["promotion_type"]

        try:
            if promotion_type == "seller_voucher" and GlobalAdapter.PromotionE2EVar._SellerVoucherList_:
                ##Replace API url/referer promotion id to last global stored seller voucher promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["id"])

            elif promotion_type == "free_shipping_voucher" and GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_:
                ##Replace API url/referer promotion id to last global stored free shipping voucher promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"])

            elif promotion_type == "brand_sale" and GlobalAdapter.PromotionE2EVar._BrandSaleIDList_:
                ##Replace API url/referer promotion id to last global stored brand sale promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._BrandSaleIDList_[-1])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._BrandSaleIDList_[-1])

            elif promotion_type == "shopee_voucher" and GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_:
                ##Replace API url/referer promotion id to last global stored shopee voucher promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["id"])

            elif promotion_type == "dp_voucher" and GlobalAdapter.PromotionE2EVar._DPVoucherList_:
                ##Replace API url/referer promotion id to last global stored dp voucher promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._DPVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._DPVoucherList_[-1]["id"])

            elif promotion_type == "offline_payment_voucher" and GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_:
                ##Replace API url/referer promotion id to last global stored offline payment voucher promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_[-1]["id"])

            elif promotion_type == "partner_voucher" and GlobalAdapter.PromotionE2EVar._PartnerVoucherList_:
                ##Replace API url/referer promotion id to last global stored partner voucher promotion id
                GlobalAdapter.APIVar._HttpPayload_["voucher_id"] = GlobalAdapter.APIVar._HttpPayload_["voucher_id"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._PartnerVoucherList_[-1]["id"])

            elif promotion_type == "redeem_partner_voucher" and GlobalAdapter.PromotionE2EVar._PartnerVoucherList_:
                ##Replace API url/referer promotion id to last global stored partner voucher promotion id
                GlobalAdapter.APIVar._HttpPayload_["redeem_voucher_info"]["voucher_id"] = int(GlobalAdapter.APIVar._HttpPayload_["redeem_voucher_info"]["voucher_id"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._PartnerVoucherList_[-1]["id"]))

            elif promotion_type == "partner_voucher_display_id" and GlobalAdapter.PromotionE2EVar._PartnerVoucherList_:
                ##Replace API url/referer promotion id to last global stored partner voucher display id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("display_id", GlobalAdapter.PromotionE2EVar._PartnerVoucherList_[-1]["display_id"])

            elif GlobalAdapter.PromotionE2EVar._PromotionIDList_:
                ##Replace API url/referer promotion id to last global stored promotion id
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])

            ##Assign API csrftoken
            GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"] = GlobalAdapter.APIVar._HttpCookie_["csrftoken"]

            ##Log API variable
            dumplogger.info("GlobalAdapter.APIVar._HttpUrl_ = %s" % (GlobalAdapter.APIVar._HttpUrl_))
            dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_['referer'] = %s" % (GlobalAdapter.APIVar._HttpHeaders_["referer"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_['x-csrftoken'] = %s" % (GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignDataForPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForPrivateNotification(arg):
        '''
        AssignDataForPrivateNotification : Assign data for private notification when creating vouchers
                Input argu :
                    action_title - action title
                    action_content - action content
                    push_content - push content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_title = arg["action_title"]
        action_content = arg["action_content"]
        push_content = arg["push_content"]

        try:
            ##Replace API payload
            if action_title:
                GlobalAdapter.APIVar._HttpPayload_["action_title"] = action_title
                GlobalAdapter.APIVar._HttpPayload_["action_content"] = action_content
                GlobalAdapter.APIVar._HttpPayload_["push_content"] = push_content
            ##Generate random string for push noti text
            else:
                GlobalAdapter.GeneralE2EVar._RandomString_ = XtFunc.GenerateRandomString(10, "characters")
                GlobalAdapter.APIVar._HttpPayload_["action_title"] = GlobalAdapter.GeneralE2EVar._RandomString_
                GlobalAdapter.APIVar._HttpPayload_["action_content"] = GlobalAdapter.GeneralE2EVar._RandomString_
                GlobalAdapter.APIVar._HttpPayload_["push_content"] = GlobalAdapter.GeneralE2EVar._RandomString_

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignDataForPrivateNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignCustomisedLabelForVoucher(arg):
        '''
        AssignCustomisedLabelForVoucher : Assign customised label for create voucher API
                Input argu :
                    regional_language - 1: have regional language / 0: do not have regional language
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        regional_language = arg['regional_language']

        try:
            ##Generate random string customised label for create voucher
            GlobalAdapter.PromotionE2EVar._CustomisedLabel_ = XtFunc.GenerateRandomString(8, "characters")

            ##Insert data into api payload (Some country don't have regional language)
            if regional_language:
                GlobalAdapter.APIVar._HttpPayload_["customised_1_" + Config._TestCaseRegion_.lower()] = GlobalAdapter.PromotionE2EVar._CustomisedLabel_
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['customised_1_'" + Config._TestCaseRegion_.lower() + "] = %s" % (GlobalAdapter.APIVar._HttpPayload_["customised_1_" + Config._TestCaseRegion_.lower()]))
            ##Both label need to fill in
            GlobalAdapter.APIVar._HttpPayload_["customised_1_en"] = GlobalAdapter.PromotionE2EVar._CustomisedLabel_
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['customised_1_en'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["customised_1_en"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignCustomisedLabelForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeVoucherStatus(arg):
        '''
        ChangeVoucherStatus : Change all vouchers in global list by API
                Input argu :
                    status - enable / disable
                    voucher_type - seller_voucher / free_shipping_voucher / shopee_voucher / offline_payment_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]
        voucher_type = arg["voucher_type"]

        region_url_mapping = {
            "tw": "tw",
            "id": "co.id",
            "vn": "vn",
            "th": "co.th",
            "ph": "ph",
            "mx": "com.mx",
            "br": "com.br",
            "my": "com.my",
            "sg": "sg",
            "co": "com.co",
            "cl": "cl",
            "pl": "pl",
            "es": "es",
            "ar": "com.ar",
        }

        if voucher_type == "seller_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._SellerVoucherList_
        elif voucher_type == "free_shipping_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_
        elif voucher_type == "shopee_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_
        elif voucher_type == "offline_payment_voucher":
            voucher_list = GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_

        for voucher in voucher_list:
            ##Change voucher by admin API
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/voucher/change_voucher_status", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "change_voucher_status", "result": "1"})
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("region", region_url_mapping[Config._TestCaseRegion_.lower()])
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", voucher["id"])
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("status", status)
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("region", region_url_mapping[Config._TestCaseRegion_.lower()])
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", voucher["id"])
            HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.ChangeVoucherStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignNameForVoucher(arg):
        '''
        AssignNameForVoucher : Assign name for create voucher API
                Input argu : name - voucher name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        name = arg['name']

        try:
            ##Insert voucher name to api payload
            GlobalAdapter.APIVar._HttpPayload_["name"] = name
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['name'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["name"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignNameForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignPrefixCodeForVoucher(arg):
        '''
        AssignPrefixCodeForVoucher : Assign prefix code for create voucher API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Generate random string prefix code for create voucher
            GlobalAdapter.GeneralE2EVar._RandomString_ = XtFunc.GenerateRandomString(8, "characters")

            ##Insert promotion start time and promotion end time into create group buy api payload
            GlobalAdapter.APIVar._HttpPayload_["prefix"] = GlobalAdapter.GeneralE2EVar._RandomString_
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['prefix'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["prefix"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignPrefixCodeForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignPrefixCodeForOpenAPIVoucher(arg):
        '''
        AssignPrefixCodeForOpenAPIVoucher : Assign prefix code for create Open API voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Generate random string prefix code for create voucher
            GlobalAdapter.GeneralE2EVar._RandomString_ = XtFunc.GenerateRandomString(5, "characters")

            ##Insert promotion prefix code into payload
            GlobalAdapter.APIVar._HttpPayload_["voucher_code"] = GlobalAdapter.GeneralE2EVar._RandomString_
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['voucher_code'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["voucher_code"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignPrefixCodeForOpenAPIVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignShippingPromotionRuleForVoucher(arg):
        '''
        AssignShippingPromotionRuleForVoucher : Assign shipping promotion rule for create voucher API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Insert promotion start time and promotion end time into create group buy api payload
            GlobalAdapter.APIVar._HttpPayload_["shipping_promotion_rules"] = GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1]
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['shipping_promotion_rules'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["shipping_promotion_rules"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignShippingPromotionRuleForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForVoucher(arg):
        '''
        AssignTimePeroidForVoucher : Assign time peroid for create voucher API
                Input argu :
                    voucher_type : seller_voucher / free_shipping_voucher / shopee_voucher / private_type_shopee_voucher / public_dp_voucher / private_dp_voucher / offline_payment_voucher / partner_voucher / redeem_partner_voucher
                    start_time : voucher validity start time delta
                    end_time : voucher validity end time delta
                    claim_start_time : voucher activity start time delta
                    stop_dispatch_time : voucher activity end time delta
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg['voucher_type']
        start_time = arg['start_time']
        end_time = arg['end_time']
        claim_start_time = arg['claim_start_time']
        stop_dispatch_time = arg['stop_dispatch_time']

        try:
            if voucher_type not in ("offline_payment_voucher", "partner_voucher", "redeem_partner_voucher"):
                ##Insert promotion start time and promotion end time into create voucher api payload
                GlobalAdapter.APIVar._HttpPayload_["start_time"] = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
                GlobalAdapter.APIVar._HttpPayload_["end_time"] = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

            if voucher_type not in ("seller_voucher", "partner_voucher", "redeem_partner_voucher"):
                ##Insert stop dispatch time into create free shipping voucher / shopee voucher api payload
                if stop_dispatch_time:
                    GlobalAdapter.APIVar._HttpPayload_["stop_dispatch_time"] = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(stop_dispatch_time), 0)
                else:
                    GlobalAdapter.APIVar._HttpPayload_["stop_dispatch_time"] = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['stop_dispatch_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["stop_dispatch_time"]))

            if voucher_type in ("free_shipping_voucher", "private_type_shopee_voucher", "private_dp_voucher"):
                ##Insert claim start time into create free shipping voucher api payload
                if claim_start_time:
                    GlobalAdapter.APIVar._HttpPayload_["claim_start_time"] = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(claim_start_time), 0)
                else:
                    GlobalAdapter.APIVar._HttpPayload_["claim_start_time"] = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['claim_start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["claim_start_time"]))

            if voucher_type in "offline_payment_voucher":
                ##Insert claim start date/end date into create offline payment voucher api payload
                GlobalAdapter.APIVar._HttpPayload_["start_date"] = XtFunc.GetCurrentDateTime("%Y-%m-%d", int(start_time), 0, 0)
                GlobalAdapter.APIVar._HttpPayload_["end_date"] = XtFunc.GetCurrentDateTime("%Y-%m-%d", int(end_time), 0, 0)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_date'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_date"]))

            if voucher_type in "partner_voucher":
                ##Insert claim start date/end date into create partner voucher api payload
                GlobalAdapter.APIVar._HttpPayload_["start_date"] = XtFunc.GetCurrentDateTime("%Y-%m-%d" + " 00:00:00", 0, 0, 0)
                GlobalAdapter.APIVar._HttpPayload_["end_date"] = XtFunc.GetCurrentDateTime("%Y-%m-%d" + " 00:00:00", 1, 0, 0)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_date'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_date"]))

            if voucher_type == "redeem_partner_voucher":
                ##Get start unix time
                start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=int(start_time), seconds=0)
                start_unix_time = time.mktime(start_datetime.timetuple())

                ##Get end unix time
                end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=int(end_time), seconds=0)
                end_unix_time = time.mktime(end_datetime.timetuple())

                ##Insert claim start date/end date into create partner voucher api payload
                GlobalAdapter.APIVar._HttpPayload_["redeem_voucher_info"]["start_time"] = int(start_unix_time)
                GlobalAdapter.APIVar._HttpPayload_["redeem_voucher_info"]["end_time"] = int(end_unix_time)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['redeem_voucher_info']['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["redeem_voucher_info"]["start_time"]))

            if voucher_type not in ("seller_voucher", "free_shipping_voucher", "shopee_voucher", "private_type_shopee_voucher", "public_dp_voucher", "private_dp_voucher", "offline_payment_voucher", "partner_voucher", "redeem_partner_voucher"):
                dumplogger.error("voucher type not exist!!! => %s" % voucher_type)
                ret = 0

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForPromotionRule(arg):
        '''
        AssignTimePeroidForPromotionRule : Assign time peroid for create promotion rule by API
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start unix time
            start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0,is_tw_time=1)
            start_unix_time = time.mktime(time.strptime(start_datetime, "%Y-%m-%d %H:%M"))

            ##Get end unix time
            end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0,is_tw_time=1)
            end_unix_time = time.mktime(time.strptime(end_datetime, "%Y-%m-%d %H:%M"))

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["promos"][0]["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["promos"][0]["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["promos"][0]["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["promos"][0]["end_time"]))

            ##Convert payload to json avoid api response "No JSON object could be decoded"
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForPromotionRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UpdateTimePeroidForPromotionRule(arg):
        '''
        UpdateTimePeroidForPromotionRule : Update time peroid for existed promotion rule by API
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Insert promotion id to header and payload
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = "https://admin.promotion.staging.shopee.vn/sp-rule-info?id=" + str(GlobalAdapter.CommonVar._DynamicCaseData_["promo_id"])
            GlobalAdapter.APIVar._HttpPayload_["promo"]["promo_id"] = GlobalAdapter.CommonVar._DynamicCaseData_["promo_id"]

            ##Get start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["promo"]["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["promo"]["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["promo"]["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["promo"]["end_time"]))

            ##Convert payload to json avoid api response "No JSON object could be decoded"
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.UpdateTimePeroidForPromotionRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForPWP(arg):
        '''
        AssignTimePeroidForPWP : Assign unix time peroid for create PWP rule
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start unix time
            start_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
            start_unix_time = time.mktime(time.strptime(start_datetime, "%Y-%m-%d %H:%M"))

            ##Get end unix time
            end_datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)
            end_unix_time = time.mktime(time.strptime(end_datetime, "%Y-%m-%d %H:%M"))

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForPWP')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForExclusivePrice(arg):
        '''
        AssignTimePeroidForExclusivePrice : Assign unix time peroid for create ExclusivePrice rule
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["ep_info"]["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["ep_info"]["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['ep_info']['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["ep_info"]["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['ep_info']['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["ep_info"]["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForExclusivePrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeroidForBundleDeal(arg):
        '''
        AssignTimePeroidForBundleDeal : Assign unix time peroid for create Bundle Deal
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])

        try:
            ##Get start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeroidForBundleDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignTimePeriodForGiftWithPurchase(arg):
        '''
        AssignTimePeroidForGiftWithPurchase : Assign unix time peroid for create Gift With Purchase
                Input argu :
                    start_time : promotion rule start time, now time stamp add input minute
                    end_time : promotion rule end time, now time stamp add input minute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = int(arg['start_time'])
        end_time = int(arg['end_time'])
        try:
            ##Get start unix time
            start_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=start_time, seconds=0)
            start_unix_time = time.mktime(start_datetime.timetuple())

            ##Get end unix time
            end_datetime = datetime.datetime.now() + datetime.timedelta(days=0, minutes=end_time, seconds=0)
            end_unix_time = time.mktime(end_datetime.timetuple())

            ##Insert promotion start time and promotion end time into create promotion rule api payload
            GlobalAdapter.APIVar._HttpPayload_["start_time"] = int(start_unix_time)
            GlobalAdapter.APIVar._HttpPayload_["end_time"] = int(end_unix_time)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['start_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["start_time"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['end_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["end_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignTimePeriodForGiftWithPurchase')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForVoucherDispatchUser(arg):
        '''
        AssignDataForVoucherDispatchUser : Assign data for voucher dispatch user
                Input argu :
                    upload_csv_file_name - upload csv file name
                    voucher_type - shopee_voucher / free_shipping_voucher / offline_payment_voucher / seller_voucher / dp_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_csv_file_name = arg['upload_csv_file_name']
        voucher_type = arg['voucher_type']

        ##Prepare csv file path
        slash = Config.dict_systemslash[Config._platform_]
        csv_file_path = os.path.join(Config._UICaseDataFilePath_, Config._TestCaseRegion_, Config._EnvType_, Config._TestCasePlatform_.lower(), Config._TestCaseFeature_, 'file', upload_csv_file_name + '.csv')
        dumplogger.info("Upload csv file: %s" % (csv_file_path))

        ##Read upload csv file
        csv_file = open(csv_file_path, 'rb')

        try:
            ##Replace API url/referer promotion id to global stored promotion id
            if voucher_type == "free_shipping_voucher" and GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_:
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[-1]["id"])
            elif voucher_type == "seller_voucher" and GlobalAdapter.PromotionE2EVar._SellerVoucherList_:
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["id"])
            elif voucher_type == "shopee_voucher" and GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_:
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["id"])
            elif voucher_type == "offline_payment_voucher" and GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_:
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_[-1]["id"])
            elif voucher_type == "dp_voucher" and GlobalAdapter.PromotionE2EVar._DPVoucherList_:
                GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("promotion_id", GlobalAdapter.PromotionE2EVar._DPVoucherList_[-1]["id"])
                GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promotion_id", GlobalAdapter.PromotionE2EVar._DPVoucherList_[-1]["id"])

            ##Insert x-csrftoken and upload file to API variable
            GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"] = GlobalAdapter.APIVar._HttpCookie_["csrftoken"]
            with open(csv_file_path, 'rb') as ReadCSV:
                csv_file = ReadCSV.read()
                GlobalAdapter.APIVar._HttpUploadFiles_["file"] = csv_file
            dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_['x-csrftoken'] = %s" % (GlobalAdapter.APIVar._HttpHeaders_["x-csrftoken"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignDataForVoucherDispatchUser')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignManualConfigurationFile(arg):
        '''
        AssignManualConfigurationFile : Assign data for manual configuration file
                Input argu :
                    file_name - file_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        file_name = arg["file_name"]

        ##Prepare csv file path
        csv_file_path = os.path.join(Config._UICaseDataFilePath_, Config._TestCaseRegion_, Config._EnvType_, Config._TestCasePlatform_.lower(), Config._TestCaseFeature_, 'file', file_name + '.csv')
        dumplogger.info("Upload csv file: %s" % (csv_file_path))
        dumplogger.info("GlobalAdapter.PromotionE2EVar = %s" % (GlobalAdapter.PromotionE2EVar))
        dumplogger.info("AssignManualConfigurationFile_OngoingFlashSaleList_ = %s" % (GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_))
        dumplogger.info("AssignManualConfigurationFile_ReviewingFlashSaleList_ = %s" % (GlobalAdapter.PromotionE2EVar._ReviewingFlashSaleList_))
        dumplogger.info("AssignManualConfigurationFile_UpcomingFlashSaleList_ = %s" % (GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_))

        ##If there is upcoming flashsale
        if GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_:
            dumplogger.info("Using upcoming flash sale promotion id: %s" % (GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_[-1]["promotion_id"]))

            ##Assign http headers
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promo_id_to_be_replace", str(GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_[-1]["promotion_id"]))
            ##Assign http payload
            GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.PromotionE2EVar._UpcomingFlashSaleList_[-1]["promotion_id"])

        ##If there is ongoing flashsale
        elif GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_:
            dumplogger.info("Using ongoing flash sale promotion id: %s" % (GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_[-1]["promotion_id"]))
            ##Assign http headers
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promo_id_to_be_replace", str(GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_[-1]["promotion_id"]))
            ##Assign http payload
            GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.PromotionE2EVar._OngoingFlashSaleList_[-1]["promotion_id"])
        else:
            dumplogger.info("No ongoing flash sale.")

        try:
            ##Assign http headers
            GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
            ##Assign http payload
            GlobalAdapter.APIVar._HttpPayload_["upload_info"]["file_name"] = csv_file_path
            ##Read upload csv file and assemble item_meta_list content
            with open(csv_file_path, 'rb') as ReadCSV:
                csv_file = ReadCSV.read()
                dumplogger.info(csv_file.splitlines())
                dumplogger.info(len(csv_file.splitlines()))

                row_index = 0
                item_meta_list = []
                dumplogger.info("item_meta_list")
                for row in csv_file.splitlines():
                    item_meta_rows = {}
                    if row_index != 0:
                        item_meta_rows["shop_id"] = int(row.split(',')[0])
                        item_meta_rows["item_id"] = int(row.split(',')[1])
                        item_meta_rows["item_display_name"] = row.split(',')[4]
                        item_meta_rows["overlay_image_id"] = 0
                        item_meta_rows["fs_cat_id"] = int(row.split(',')[3])
                        item_meta_rows["sold_out_auto_move"] = 0
                        item_meta_rows["sort_position"] = row.split(',')[7]
                        item_meta_list.append(item_meta_rows)
                        dumplogger.info("item_meta_list")
                        dumplogger.info(item_meta_list)
                    row_index += 1
            ##After assemble, assign it into http payload item_meta_list param
            GlobalAdapter.APIVar._HttpPayload_["item_meta_list"] = item_meta_list

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPromotionAPI.AssignManualConfigurationFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadFlashSaleManualConfigurationFile(arg):
        '''
        UploadFlashSaleManualConfigurationFile : Upload flash sale manual configuration file by API
                Input argu :
                    file_name - file_name
                    status - Ongoing or Reviewing
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        status = arg["status"]

        ##Get reviewing flashsale status
        if status == "Ongoing":
            AdminPromotionAPI.GetOngoingFlashSaleResponseStatus({"session_type": "cfs_session", "result": "1"})
        elif status == "Reviewing":
            AdminPromotionAPI.GetUpcomingFlashSaleResponseStatus({"result": "1"})

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Send Admin api to upload flashsale config file
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/upload_manual_config", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Upload manual configuration file to assign flash sale product category
        AdminPromotionAPI.AssignManualConfigurationFile({"file_name":file_name, "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        time.sleep(60)

        ##Refresh flash sale
        AdminPromotionAPI.RefreshFlashSale({"session_type": "cfs_session","result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.UploadFlashSaleManualConfigurationFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassUploadMFSBatchSetting(arg):
        '''
        MassUploadMFSBatchSetting : Mass Upload mall flash sale batch setting by API
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]

        ##set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##csv file path
        csv_file_path = os.path.join(Config._UICaseDataFilePath_, Config._TestCaseRegion_, Config._EnvType_, Config._TestCasePlatform_.lower(), Config._TestCaseFeature_, 'file', file_name + '.csv')

        ##Search mfs flash sale to get promotion id by API
        AdminPromotionAPI.GetPromotionId({"session_type": "mfs_session", "result": "1"})

        ##Mass upload flash sale config file by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/mass_upload_mfs_config", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Assign headers and payload
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promo_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._PromotionID_))
        GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = GlobalAdapter.FlashSaleE2EVar._PromotionID_
        GlobalAdapter.APIVar._HttpPayload_["upload_info"]["file_name"] = csv_file_path

        ##Read upload csv file and assemble item_meta_list content
        with open(csv_file_path, 'rb') as ReadCSV:
            csv_file = ReadCSV.read()
            row_index = 0
            item_meta_list = []
            for row in csv_file.splitlines():
                if row_index != 0:
                    item_meta_rows = {}
                    item_meta_rows["shop_id"] = int(row.split(',')[0])
                    item_meta_rows["item_id"] = int(row.split(',')[2])
                    item_meta_rows["item_display_name"] = row.split(',')[4]
                    item_meta_rows["overlay_image_id"] = 0
                    item_meta_rows["sort_position"] = row.split(',')[5]
                    item_meta_list.append(item_meta_rows)
                row_index += 1
        dumplogger.info(item_meta_list)

        ##After assemble, assign it into http payload item_meta_list param
        GlobalAdapter.APIVar._HttpPayload_["item_meta_list"] = item_meta_list
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        time.sleep(60)

        ##Refresh flash sale
        AdminPromotionAPI.RefreshFlashSale({"session_type": "mfs_session","result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.MassUploadMFSBatchSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassUploadMFSShopOrder(arg):
        '''
        MassUploadMFSShopOrder : Mass Upload mall flash sale shop order by API
                Input argu :
                    file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]

        ##set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##csv file path
        csv_file_path = os.path.join(Config._UICaseDataFilePath_, Config._TestCaseRegion_, Config._EnvType_, Config._TestCasePlatform_.lower(), Config._TestCaseFeature_, 'file', file_name + '.csv')

        ##Search mfs flash sale to get promotion id by API
        AdminPromotionAPI.GetPromotionId({"session_type": "mfs_session", "result": "1"})

        ##Mass upload flash sale config file by API
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/upload_mfs_shop_order", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Assign headers and payload
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promo_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._PromotionID_))
        GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = GlobalAdapter.FlashSaleE2EVar._PromotionID_

        ##Read upload csv file and assemble shop_order_info_list content
        with open(csv_file_path, 'rb') as ReadCSV:
            csv_file = ReadCSV.read()
            row_index = 0
            shop_order_info_list = []
            for row in csv_file.splitlines():
                if row_index != 0:
                    shop_order_rows = {}
                    shop_order_rows["shop_id"] = int(row.split(',')[0])
                    shop_order_rows["display_order_str"] = row.split(',')[5]
                    shop_order_rows["sold_out_auto_move"] = 0
                    shop_order_info_list.append(shop_order_rows)
                row_index += 1
        dumplogger.info(shop_order_info_list)

        ##After assemble, assign it into http payload item_meta_list param
        GlobalAdapter.APIVar._HttpPayload_["shop_order_info_list"] = shop_order_info_list
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        time.sleep(60)

        ##Refresh flash sale
        AdminPromotionAPI.RefreshFlashSale({"session_type": "mfs_session","result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.MassUploadMFSShopOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateLocalBundleDeal(arg):
        '''
        CreateLocalBundleDeal : Create local Bundle Deal from Promotion Admin
                Input argu :
                    start_time - promotion start time
                    end_time - promotion end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Create Bundle Deal
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"create_bundle_deal", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        AdminPromotionAPI.AssignTimePeroidForBundleDeal({"start_time": start_time, "end_time": end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "bundle_deal_id", "result": "1"})
        dumplogger.info("Create new bundle deal id = %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1]))

        ##Upload Bundle Deal items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"add_bundle_deal_items", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        for item in GlobalAdapter.APIVar._HttpPayload_["bundle_deal_items"]:
            item["bundle_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        for item in GlobalAdapter.APIVar._JsonCaseData_["response_text"]["data"]["success_items"]:
            item["bundle_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Approve all Bundle Deal items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_all_bundle_deal_item_status", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["bundle_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        for item in GlobalAdapter.APIVar._JsonCaseData_["response_text"]["data"]["success_items"]:
            item["bundle_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateLocalBundleDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateGiftWithPurchase(arg):
        '''
        CreateGiftWithPurchase : Create Gift With Purchase from Promotion Admin
                Input argu :
                    start_time - promotion start time
                    end_time - promotion end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        #Create Gift With Purchase
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"create_gift_with_purchase", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        AdminPromotionAPI.AssignTimePeriodForGiftWithPurchase({"start_time": start_time, "end_time": end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "gift_with_purchase_id", "result": "1"})
        dumplogger.info("Create new add on deal id = %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1]))

        ##Add Gift With Purchase Main Item
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"add_pwp_main_item", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Approve all Gift With Purchase Main Items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_all_pwp_item_status", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Add Gift With Purchase Sub Items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_pwp_sub_item", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Approve all Gift With Purchase Sub Items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_all_pwp_sub_item_status", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateGiftWithPurchase')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateAddOnDeal(arg):
        '''
        CreateAddOnDeal : Create Add On Deal from Promotion Admin
                Input argu :
                    start_time - promotion start time
                    end_time - promotion end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        #Create Create Add On Deal
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"create_add_on_deal", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        AdminPromotionAPI.AssignTimePeriodForGiftWithPurchase({"start_time": start_time, "end_time": end_time, "result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "gift_with_purchase_id", "result": "1"})
        dumplogger.info("Create new add on deal id = %s" % (GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1]))

        ##Add Create Add On Deal Main Item
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"add_pwp_main_item", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Approve all Create Add On Deal Main Items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_all_pwp_item_status", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Add Create Add On Deal Sub Items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_pwp_sub_item", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Approve all Create Add On Deal Sub Items
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_all_pwp_sub_item_status", "result": "1"})
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.CreateAddOnDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadFlashSaleBannerImage(arg):
        '''
        UploadFlashSaleBannerImage : Upload flash sale banner image by API
                Input argu :
                    status - Ongoing or Reviewing
                    name - session name and cluster name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]
        name = arg['name']

        ##Get reviewing flashsale status
        if status == "Ongoing":
            AdminPromotionAPI.GetOngoingFlashSaleResponseStatus({"session_type": "cfs_session", "result": "1"})
        elif status == "Reviewing":
            AdminPromotionAPI.GetUpcomingFlashSaleResponseStatus({"result": "1"})

        ##Set collection name
        collection = Config._TestCaseRegion_.lower() + "_" + Config._EnvType_

        ##Search promotion id by API and store promotion id
        AdminPromotionAPI.GetPromotionId({"session_type": "cfs_session", "result": "1"})

        ##Send Admin api to upload flashsale config file
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common/FlashSale/upload_banner", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Assign http headers
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("promo_id_to_be_replace", str(GlobalAdapter.FlashSaleE2EVar._PromotionID_))

        ##Assign http payload
        GlobalAdapter.APIVar._HttpPayload_["promotion_id"] = int(GlobalAdapter.FlashSaleE2EVar._PromotionID_)

        ##Assign cookie(SSO_C) to http header
        PromotionNewAdminAPIMethod.AdminNewPromotionAPI.StoreNewAdminPromotionCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Refresh flash sale
        time.sleep(3)
        AdminPromotionAPI.RefreshFlashSale({"session_type": "cfs_session","result": "1"})

        OK(ret, int(arg['result']), 'AdminPromotionAPI.UploadFlashSaleBannerImage')

class AdminOrdersAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForUpdateActualShippingFee(arg):
        '''
        AssignDataForUpdateActualShippingFee : Assign data for update Actual Shipping Fee API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            if "order_id" in GlobalAdapter.APIVar._HttpPayload_:
                ##Insert order id into url
                GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))
                GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
            else:
                ##Insert order sn into url
                GlobalAdapter.APIVar._HttpPayload_["update_infos"][0]["order_sn"] = GlobalAdapter.OrderE2EVar._OrderSNDict_[GlobalAdapter.OrderE2EVar._OrderSNDict_.keys()[0]][-1]
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['update_infos'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["update_infos"]))
                GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminOrdersAPI.AssignDataForUpdateActualShippingFee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetEscrowInfoFromCodingMonkey(arg):
        '''
            GetEscrowInfoFromCodingMonkey : Get escrow info from coding monkey
                Input argu:
                    column - key in coding monkey
                Return code:
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column = arg['column']

        ##Get api request info from json file
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_escrow_info", "result":"1"})

        ##Replace coding monkey url with order id
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("order_id", GlobalAdapter.OrderE2EVar._OrderID_)

        ##Assing admin cookie
        AdminCommonAPI.AssignAdminCookie({"result":"1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method":"get", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result":"1"})

        match = re.search(column + r"\D{4}(\d+)", GlobalAdapter.APIVar._APIResponse_["text"])
        dumplogger.info("search pattern:%s " % (column + r"\D{4}(\d+)"))
        if match:
            ##Key `expected_receive_time` in coding monkey equals Estimated Delivery Time in admin ODP escrow table
            if column == "expected_receive_time":
                GlobalAdapter.OrderE2EVar._EstimatedDeliveryTime_ = match.group(1)
                dumplogger.info("Escrow estimate delivery time value found: %s" % (match.group(1)))

            ##Key `release_time` in coding monkey equals Escrow Release Created Time in admin ODP escrow table
            elif column == "release_time":
                GlobalAdapter.OrderE2EVar._EscrowReleaseCreatedTime_ = match.group(1)
                dumplogger.info("Escrow release created time value found: %s" % (match.group(1)))
            else:
                ret = 0
                dumplogger.error("No column support to assign")
        else:
            ret = 0
            dumplogger.error("No match pattern found in text, pattern: %s" % (column + r"\D{4}(\d+)"))

        OK(ret, int(arg['result']), 'AdminOrdersAPI.GetEscrowInfoFromCodingMonkey')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForMassCancellation(arg):
        '''
        AssignDataForMassCancellation : Assign API data for admin order mass cancellation
                Input argu : cancel_type - mass_logistics_cancel_to_logistics_invalid
                                           mass_logistics_cancel_to_request_cancelled
                                           mass_logistics_cancel_to_pick_up_fail
                                           mass_fraud_cancel
                             cancel_reason_code - cancel reason code you want to setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        cancel_type = arg["cancel_type"]
        cancel_reason_code = arg["cancel_reason_code"]
        upload_csv_file_data = ""

        try:
            ##Replace API payload about selection action. It is target cancel type
            GlobalAdapter.APIVar._HttpPayload_["selection_action"] = cancel_type
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['selection_action'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["selection_action"]))

            ##Prepare API files about mass cancellation csv file. Prepare actual order id and cancel reason in csv file
            if "mass_fraud_cancel" in cancel_type:
                ##Fraud mass cancellation csv file header => "orderid,cancellation_reason"
                upload_csv_file_data = ('file', ('auto_test_mass_cancel.csv', "orderid,cancellation_reason\n%s,auto_test" % (GlobalAdapter.OrderE2EVar._OrderID_), 'text/csv'))

                ##Assign mass cancellation upload csv data to API upload files
                GlobalAdapter.APIVar._HttpUploadFiles_.append(upload_csv_file_data)
                dumplogger.info("GlobalAdapter.APIVar._HttpUploadFiles_ = %s" % (GlobalAdapter.APIVar._HttpUploadFiles_))
            elif "mass_logistics_cancel" in cancel_type:
                ##Logistics mass cancellation csv file header => "orderid,reason_code,cancellation_reason"
                upload_csv_file_data = ('file', ('auto_test_mass_cancel.csv', "orderid,reason_code,cancellation_reason\n%s,%s,auto_test" % (GlobalAdapter.OrderE2EVar._OrderID_, cancel_reason_code), 'text/csv'))

                ##Assign mass cancellation upload csv data to API upload files
                GlobalAdapter.APIVar._HttpUploadFiles_.append(upload_csv_file_data)
                dumplogger.info("GlobalAdapter.APIVar._HttpUploadFiles_ = %s" % (GlobalAdapter.APIVar._HttpUploadFiles_))
            else:
                ret = 0
                dumplogger.error("Error!!!! please input correct cancel_type => mass_logistics_cancel_to_logistics_invalid / mass_logistics_cancel_to_request_cancelled / mass_logistics_cancel_to_pick_up_fail / mass_fraud_cancel")

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminOrdersAPI.AssignDataForMassCancellation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderShipByDate(arg):
        '''
        GetOrderShipByDate : Get order's ship by date value
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##retry to avoid get none from api
        for retry_time in range(2):
            ##Get api request info from json file
            HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection":"get_ship_by_date", "result":"1"})

            ##Replace coding monkey url with order id
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("order_id", GlobalAdapter.OrderE2EVar._OrderID_)

            ##Assing admin cookie
            AdminCommonAPI.AssignAdminCookie({"result":"1"})

            ##Send API request
            HttpAPICore.SendHttpRequest({"http_method":"get", "result":"1"})
            APICommonMethod.CheckAPIResponseCode({"result":"1"})

            ##Find ship by date from response
            match = re.search(r"ship_by_date\D{4}(\d+)", GlobalAdapter.APIVar._APIResponse_["text"])
            if match:
                GlobalAdapter.OrderE2EVar._ShipByDate_ = match.group(1)
                dumplogger.info("Ship by date value found: %s" % (match.group(1)))
                break
            else:
                dumplogger.info("There is no ship by date value, run recalculate cronjob now")
                SpaceCronjobAPI.RunShipByDateCronjob({"result": "1"})
                time.sleep(180)

        OK(ret, int(arg['result']), 'AdminOrdersAPI.GetOrderShipByDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOFGID(arg):
        '''
        GetOFGID : Get order's ofg id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get api request info from json file
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_ofg_id", "result":"1"})

        ##Replace url with order id
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("order_id_to_be_replaced", str(GlobalAdapter.OrderE2EVar._OrderID_))
        dumplogger.info("GlobalAdapter.APIVar._HttpUrl_ = %s" % (GlobalAdapter.APIVar._HttpUrl_))

        ##Assign order id to API header
        GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("order_id_to_be_replaced", str(GlobalAdapter.OrderE2EVar._OrderID_))

        ##Assing order admin cookie
        OrderAdminAPIMethod.OrderAdminAPI.StoreOrderAdminCookie({"result":"1"})

        ##Insert order id into payload
        GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['order_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["order_id"]))

        ##Dump json to string
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method":"get", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result":"1"})

        try:
            response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
            dumplogger.info("_response_text_ : %s" % response)

            ##Save data to global
            order_info = response['order_life_cycle_info']["third_party_logistics_status_history"]["forder_logistics_status_history_list"][0]
            GlobalAdapter.OrderE2EVar._OFGID_ = order_info["ofg_id"]
            dumplogger.info("_OFGID_ : %s" % str(GlobalAdapter.OrderE2EVar._OFGID_))

        except KeyError:
            dumplogger.exception("Encounter key exception")
            ret = -1
        except:
            dumplogger.exception("Encounter unknown exception")
            ret = -1

        OK(ret, int(arg['result']), 'AdminOrdersAPI.GetOFGID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLogisticStatus(arg):
        '''
        CheckLogisticStatus : Get order's logistic status
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        for retry_times in range(1, 4):
            ##Get api request info from json file
            HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection":"check_logistic_status", "result":"1"})

            ##Replace url with order id
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("ofg_id_to_be_replaced", str(GlobalAdapter.OrderE2EVar._OFGID_))
            dumplogger.info("GlobalAdapter.APIVar._HttpUrl_ = %s" % (GlobalAdapter.APIVar._HttpUrl_))

            ##Assign order id to API header
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("ofg_id_to_be_replaced", str(GlobalAdapter.OrderE2EVar._OFGID_))

            ##Assing order admin cookie
            OrderAdminAPIMethod.OrderAdminAPI.StoreOrderAdminCookie({"result":"1"})

            ##Insert order id into payload
            GlobalAdapter.APIVar._HttpPayload_["ofg_id"] = int(GlobalAdapter.OrderE2EVar._OFGID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['ofg_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["ofg_id"]))

            ##Dump json to string
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

            ##Send API request
            HttpAPICore.SendHttpRequest({"http_method":"get", "result":"1"})
            APICommonMethod.CheckAPIResponseCode({"result":"1"})

            try:
                response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
                dumplogger.info("_response_text_ : %s" % response)

                ##Save data to global
                logisic_status = response['data']["order_fulfilment_group"]["fulfilment_status"]
                GlobalAdapter.OrderE2EVar._LogisticStatus_ = logisic_status
                dumplogger.info("_LogisticStatus_ : %s" % str(GlobalAdapter.OrderE2EVar._LogisticStatus_))

                if int(status) == GlobalAdapter.OrderE2EVar._LogisticStatus_:
                    dumplogger.info("LogisticStatus is equal as string: %s!!! Retry %dth time" % (status, retry_times))
                    ret = 1
                    break
                else:
                    dumplogger.info("LogisticStatus is not equal as string: %s!!! Retry %dth time" % (status, retry_times))
                    ret = 0
                    time.sleep(10)

            except KeyError:
                dumplogger.exception("Encounter key exception")
                ret = -1
            except:
                dumplogger.exception("Encounter unknown exception")
                ret = -1

        if not ret:
            ##Corresponding status for each number
            logistic_status_code = {"0": "not started", "1": "request created", "2": "outbound successful", "5": "successful", "9": "ready"}

            ##Save expected/actual result for issue report
            XtFunc.SaveCaseInfoForIssueReport({"info_key": "Expected fulfillment status", "info_value": logistic_status_code[status], "result": "1"})
            XtFunc.SaveCaseInfoForIssueReport({"info_key": "Actual fulfillment status", "info_value": logistic_status_code[str(GlobalAdapter.OrderE2EVar._LogisticStatus_)], "result": "1"})

            ##To store data in _FailedCaseInfo_
            APICommonMethod.CheckAPIResponseText({"result":"1"})

        OK(ret, int(arg['result']), 'AdminOrdersAPI.CheckLogisticStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateReceiptIssue(arg):
        '''
        CreateReceiptIssue : Send Api to create r eceipt issue
            Input argu :N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Get receipt_issue_datetime by api
        ##Get api request info from json file
        slash = Config.dict_systemslash[Config._platform_]
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"common" + slash + "order" + slash + "create_receipt_issue", "result":"1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_receipts_by_source_ids", "result":"1"})

        ##Insert order id into payload
        GlobalAdapter.APIVar._HttpPayload_["source_ids"] = [str(GlobalAdapter.OrderE2EVar._OrderID_)]
        dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['source_ids'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["source_ids"]))

        ##Dump json to string
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method":"post", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result":"1"})

        ##Change receipt_issue_datetime
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])
        dumplogger.info("_response_text_ : %s" % response)
        response["receipt_units"][0]["receipt_datum"]["extra_data"]["receipt_issue_datetime"] -= 8000000

        ##Dump json to string
        HttpAPICore.GetAndSetHttpAPIData({"collection":"set_receipt_units", "result":"1"})
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(response)

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method":"post", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result":"1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminOrdersAPI.CreateReceiptIssue')


class AdminShipmentAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForLostParcel(arg):
        '''
        AssignDataForLostParcel : Assign API data for admin shipment lost parcel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Replace API payload orderids to global order id
            GlobalAdapter.APIVar._HttpPayload_["orderids[]"] = GlobalAdapter.OrderE2EVar._OrderID_
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['orderids[]'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["orderids[]"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminShipmentAPI.AssignDataForLostParcel')


class AdminUserAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GenerateRandomStringToPayload(arg):
        '''
        GenerateRandomStringToPayload : To generate a random number or string and assign to the column.
                Input argu :
                    length - random numbers or characters length
                    column - the column which you want to stored into DynamicCaseData
                    method - characters / numbers
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        length = arg["length"]
        column = arg["column"]
        method = arg["method"]

        ##Store random string to api payload
        try:
            GlobalAdapter.APIVar._HttpPayload_[column] = XtFunc.GenerateRandomString(int(length), method)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminUserAPI.GenerateRandomStringToPayload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetFraudTagList(arg):
        '''
        GetFraudTagList : Get list of Fraud Tags in user info page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Setting up for admin api request
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "common/user/modify_user_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_fraud_tag_list", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["country"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpHeaders_["region"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpPayload_["region"] = Config._TestCaseRegion_
        AdminCommonAPI.AssignUserPortalCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        ##Store fraud tags
        XtFunc.ResetWebElementsList({"reset_element": "fraud_tag", "result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "fraud_tag", "result": "1"})

        dumplogger.info("Saved fraud tag: %s" % (GlobalAdapter.AccountE2EVar._FraudTagList_))

        OK(ret, int(arg['result']), 'AdminUserAPI.GetFraudTagList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeAccountStatus(arg):
        '''
        ChangeAccountStatus : Change account status with condition handling
                Input argu :
                    user_id - the user id need to be modified
                    status - normal/deleted/banned/frozen
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_id = arg["user_id"]
        status = arg["status"]

        status_code_mapping = {
            "deleted": 0,
            "normal": 1,
            "banned": 2,
            "frozen": 3
        }

        #Get list of Fraud Tags
        AdminUserAPI.GetFraudTagList({"result": "1"})

        time.sleep(5)

        ##Restore account status to normal
        HttpAPICore.InitialHttpAPI({"session": "inherit", "json_data": "common/user/modify_user_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "modify_user_status", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["country"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpHeaders_["region"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpPayload_["userid"] = int(user_id)
        GlobalAdapter.APIVar._HttpPayload_["status"] = 1
        AdminCommonAPI.AssignUserPortalCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        time.sleep(5)

        ##Change account status
        HttpAPICore.InitialHttpAPI({"session": "inherit", "json_data": "common/user/modify_user_status", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "modify_user_status", "result": "1"})
        GlobalAdapter.APIVar._HttpHeaders_["country"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpHeaders_["region"] = Config._TestCaseRegion_
        GlobalAdapter.APIVar._HttpPayload_["userid"] = int(user_id)
        GlobalAdapter.APIVar._HttpPayload_["status"] = status_code_mapping[status]

        ##If changing to banned/frozen need to add fraud tag
        if status in ("banned","frozen"):
            GlobalAdapter.APIVar._HttpPayload_["fraud_tag_list"].append(GlobalAdapter.AccountE2EVar._FraudTagList_[-1])

        AdminCommonAPI.AssignUserPortalCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        ##Check if response text has error code
        response = GlobalAdapter.APIVar._APIResponse_["text"]
        if "error" in response:
            dumplogger.error("Cannot change account status with error: %s" % (response))
            ret = 0
        else:
            dumplogger.info("Change status completed with no error response")
            ret = 1

        OK(ret, int(arg['result']), 'AdminUserAPI.ChangeAccountStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetSecondAddressID(arg):
        '''
        GetSecondAddressID : To store the second address id after creating a address from User Portal.
                Input argu :
                    var_name - the name of the variable which store the column value.
                Return code :
                    1 - success
        '''
        ret = 1
        var_name = arg["var_name"]

        result = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Go to data > components layer of API response and collect component_id in all components section.
        second_address_id = result["data"]["addresses"][1]["id"]

        dumplogger.info("The filtered data is %s" % (second_address_id))

        ##Store the original list for restoring the order in the end of the test.
        GlobalAdapter.CommonVar._DynamicCaseData_[var_name] = second_address_id

        OK(ret, int(arg['result']), 'AdminUserAPI.GetSecondAddressID')

class AdminFraudAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SettingLogisticFraud(arg):
        '''
        SettingLogisticFraud : setting logistic fraud for limit of using FSV to checkout
                Input argu :
                    daily_limit: Checkout free shipping limit - daily
                    weekly_limit: Checkout free shipping limit - weekly
                    web_daily_limit: Web Checkout free shipping limit - daily
                    web_weekly_limit: Web Checkout free shipping limit - weekly
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        daily_limit = arg['daily_limit']
        weekly_limit = arg['weekly_limit']
        web_daily_limit = arg['web_daily_limit']
        web_weekly_limit = arg['web_weekly_limit']

        ##setting logistic fraud for limit of using FSV to checkout
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "set_logistic_fraud", "result": "1"})
        GlobalAdapter.APIVar._HttpPayload_["CHECKOUT_FREE_SHIPPING_LIMIT_DAILY"] = int(daily_limit)
        GlobalAdapter.APIVar._HttpPayload_["CHECKOUT_FREE_SHIPPING_LIMIT_WEEKLY"] = int(weekly_limit)
        GlobalAdapter.APIVar._HttpPayload_["WEB_CHECKOUT_FREE_SHIPPING_LIMIT_DAILY"] = int(web_daily_limit)
        GlobalAdapter.APIVar._HttpPayload_["WEB_CHECKOUT_FREE_SHIPPING_LIMIT_WEEKLY"] = int(web_weekly_limit)
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminFraudAPI.SettingLogisticFraud')


class AdminPaymentAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForChangeRefundStatus(arg):
        '''
        AssignDataForChangeRefundStatus : Assign data for change refund status by admin API
                Input argu :
                    refund_buyer_user_id - refund buyer user id you want to change it refund status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        refund_buyer_user_id = arg["refund_buyer_user_id"]

        try:
            ##Assign refund buyer user id and refund id in API payload
            GlobalAdapter.APIVar._HttpPayload_["ids"][0].append(int(refund_buyer_user_id))
            GlobalAdapter.APIVar._HttpPayload_["ids"][0].append(int(GlobalAdapter.OrderE2EVar._RefundID_))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['ids'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["ids"]))
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPaymentAPI.AssignDataForChangeRefundStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForSelectRefundBankAccount(arg):
        '''
        AssignDataForSelectRefundBankAccount : Assign data for select refund bank account by admin API
                Input argu :
                    refund_buyer_user_id - refund buyer user id you want to select it refund bank account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        refund_buyer_user_id = arg["refund_buyer_user_id"]

        try:
            ##Assign refund buyer user id and refund id in API url
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("user_id_to_be_replace", refund_buyer_user_id)
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpUrl_ = %s" % (GlobalAdapter.APIVar._HttpUrl_))

            ##Assign refund buyer user id and refund id in API header
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("user_id_to_be_replace", refund_buyer_user_id)
            GlobalAdapter.APIVar._HttpHeaders_["referer"] = GlobalAdapter.APIVar._HttpHeaders_["referer"].replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpHeaders_['referer'] = %s" % (GlobalAdapter.APIVar._HttpHeaders_["referer"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'AdminPaymentAPI.AssignDataForSelectRefundBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MockPayOut(arg):
        '''
        MockPayOut : Mock pay the order
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Admin login to payout request
        AdminCommonAPI.AdminLogin({"result": "1"})
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "to_paid", "result": "1"})
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPILogic.ReplaceURLFromStoredData({"column": "checkoutid", "result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentAPI.MockPayOut')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeRefundStatusToComplete(arg):
        '''
        ChangeRefundStatusToComplete : Change refund status to complete by admin API
                Input argu :
                    refund_buyer_user_id - refund buyer user id you want to change it refund status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        refund_buyer_user_id = arg["refund_buyer_user_id"]

        ##Admin login to get token
        AdminCommonAPI.AdminLogin({"result": "1"})

        ##Search refund created by refund id from admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "search_refund_created", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get API response
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Check refund created search result > 0
        if len(response["data"]) > 0:
            ##Change refund status by admin API (refund created => refund verified)
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "change_refund_from_created_to_verified", "result": "1"})
            AdminPaymentAPI.AssignDataForChangeRefundStatus({"refund_buyer_user_id": refund_buyer_user_id, "result": "1"})
            AdminCommonAPI.AssignCsrfToken({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            dumplogger.info("Successful change refund status from created to verified. Refund id: %s" % (GlobalAdapter.OrderE2EVar._RefundID_))

            ##Sleep 10 second to wait refund status change to verified
            time.sleep(10)
        else:
            dumplogger.info("Refund id %s not in refund created status" % (GlobalAdapter.OrderE2EVar._RefundID_))

        ##Search refund pending by refund id from admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "search_refund_pending", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get API response
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Check refund pending search result > 0
        if len(response["data"]) > 0:
            ##Change refund status by admin API (refund pending => refund verified)
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "change_refund_from_pending_to_verified", "result": "1"})
            AdminPaymentAPI.AssignDataForChangeRefundStatus({"refund_buyer_user_id": refund_buyer_user_id, "result": "1"})
            AdminCommonAPI.AssignCsrfToken({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            dumplogger.info("Successful change refund status from pending to verified. Refund id: %s" % (GlobalAdapter.OrderE2EVar._RefundID_))

            ##Sleep 10 second to wait refund status change to verified
            time.sleep(10)
        else:
            dumplogger.info("Refund id %s not in refund pending status" % (GlobalAdapter.OrderE2EVar._RefundID_))

        ##Search refund verified by refund id from admin API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "search_refund_verified", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get API response
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Check refund verified search result > 0
        if len(response["data"]) > 0:
            ##Change refund status by admin API (refund verified => refund payout)
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "change_refund_from_verified_to_payout", "result": "1"})
            AdminPaymentAPI.AssignDataForChangeRefundStatus({"refund_buyer_user_id": refund_buyer_user_id, "result": "1"})
            AdminCommonAPI.AssignCsrfToken({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            dumplogger.info("Successful change refund status from verified to payout. Refund id: %s" % (GlobalAdapter.OrderE2EVar._RefundID_))

            ##Sleep 60 second to wait refund status change to payout
            time.sleep(60)
        else:
            dumplogger.info("Refund id %s not in refund verified status" % (GlobalAdapter.OrderE2EVar._RefundID_))

        ##Check refund payout search result > 0
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "search_refund_payout", "result": "1"})
        GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("refund_id_to_be_replace", GlobalAdapter.OrderE2EVar._RefundID_)
        AdminCommonAPI.AssignCsrfToken({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Get API response
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Check refund payout search result > 0
        if len(response["data"]) > 0:
            ##Change refund status by admin API (refund payout => refund complete)
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "change_refund_from_payout_to_complete", "result": "1"})
            AdminPaymentAPI.AssignDataForChangeRefundStatus({"refund_buyer_user_id": refund_buyer_user_id, "result": "1"})
            AdminCommonAPI.AssignCsrfToken({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})

            ##Retry change refund status 3 times to avoid change refund status unstable issue
            for retry_times in range(1, 4):
                ##Load json API response
                response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

                ##Check if fail message exist
                if len(response["failed"]):
                    ##Sleep 5 second to retry
                    time.sleep(5)
                    dumplogger.info("Retry change refund status. Retry times %s . Fail message: %s" % (retry_times, response["failed"]))

                    ##Retry change refund status by admin API (refund payout => refund complete)
                    HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
                    APICommonMethod.CheckAPIResponseCode({"result": "1"})
                else:
                    dumplogger.info("Successful change refund status from payout to complete. Refund id: %s" % (GlobalAdapter.OrderE2EVar._RefundID_))
                    break
            else:
                ret = 0
                dumplogger.info("Push refund status fail. Refund id: %s . Fail message: %s" % (GlobalAdapter.OrderE2EVar._RefundID_, response["failed"]))
        else:
            dumplogger.info("Refund id %s not in refund payout status" % (GlobalAdapter.OrderE2EVar._RefundID_))

        ##DeInitial Http Api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'AdminPaymentAPI.ChangeRefundStatusToComplete')


class AdminBannerAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetAllCampaignUnitId(arg):
        '''
        GetAllCampaignUnitId : Get all campaign unit id by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get all home popup space group campaign unit data
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_campaign_unit_id", "result": "1"})
        AdminCommonAPI.AssignCMSAdminCookie({"result": "1"})
        HttpAPICore.SendHttpRequest({"http_method":"get", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        XtFunc.ResetWebElementsList({"reset_element": "banner_campaign_unit", "result": "1"})
        HttpAPILogic.ReserveAPIResponseData({"reserve_type": "banner_campaign_unit_id", "result": "1"})

        ##Get all home popup banner campaign unit id
        for campaign_unit in GlobalAdapter.BannerE2EVar._BannerCampaignUnitList_:
            GlobalAdapter.BannerE2EVar._BannerCampaignUnitIDList_.append(campaign_unit['campaign_unit_id'])
        dumplogger.info("Will remove campaign unit id = %s" % GlobalAdapter.BannerE2EVar._BannerCampaignUnitIDList_)

        OK(ret, int(arg['result']), 'AdminBannerAPI.GetAllCampaignUnitId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteHomePopupBanner(arg):
        '''
        DeleteHomePopupBanner : Delete home popup banner by API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get all home popup banner campaign unit id
        AdminBannerAPI.GetAllCampaignUnitId({"result": "1"})

        ##Get all home popup space group campaign unit data
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_specific_campaign_unit_banner_id", "result": "1"})
        AdminCommonAPI.AssignCMSAdminCookie({"result": "1"})

        ##Assign campaign unit id to url
        for campaign_unit in GlobalAdapter.BannerE2EVar._BannerCampaignUnitList_:
            GlobalAdapter.APIVar._HttpUrl_ = GlobalAdapter.APIVar._HttpUrl_.replace("campaign_id", str(campaign_unit['campaign_unit_id']))
            GlobalAdapter.APIVar._HttpUrlParameter_["campaign_unit_id"] = str(campaign_unit['campaign_unit_id'])
            GlobalAdapter.APIVar._HttpUrlParameter_["campaign_unit_name"] = str(campaign_unit['campaign_unit_name'])
            HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            XtFunc.ResetWebElementsList({"reset_element": "banner", "result": "1"})
            HttpAPILogic.ReserveAPIResponseData({"reserve_type": "banner", "result": "1"})

            ##If campaign unit have banner, will append banner id in to banner id list
            if GlobalAdapter.BannerE2EVar._BannerList_['total'] == 0:
                dumplogger.info("Campaign unit name %s didn't have any banner!!!" % str(campaign_unit['campaign_unit_name']))
            else:
                GlobalAdapter.BannerE2EVar._BannerIDList_.append(GlobalAdapter.BannerE2EVar._BannerList_['data'][0]['banner_id'])
        dumplogger.info("Will delete banner id = %s" % GlobalAdapter.BannerE2EVar._BannerIDList_)

        ##Delete home popup banner in banner id list by api
        for banner_id in GlobalAdapter.BannerE2EVar._BannerIDList_:
            HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "delete_banner", "result": "1"})
            AdminCommonAPI.AssignCMSAdminCookie({"result": "1"})
            GlobalAdapter.APIVar._HttpPayload_["id"] = banner_id

            ##change payload to data format
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'AdminBannerAPI.DeleteHomePopupBanner')
