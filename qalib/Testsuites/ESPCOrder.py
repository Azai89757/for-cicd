import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCOrder Precondition =="
        dumplogger.info("== Setup ESPCOrder Precondition ==")

    def test_ESPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-01.xml")

    def test_ESPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-02.xml")

    def test_ESPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-03.xml")

    def test_ESPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-04.xml")

    def test_ESPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-05.xml")

    def test_ESPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-06.xml")

    def test_ESPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-07.xml")

    def test_ESPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-08.xml")

    def test_ESPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-09.xml")

    def test_ESPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-10.xml")

    def test_ESPCOrder11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCOrder-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCOrder Post condition =="
        dumplogger.info("== Setup ESPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
