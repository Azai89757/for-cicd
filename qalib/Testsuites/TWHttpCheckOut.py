import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpCheckOut Precondition =="
        dumplogger.info("== Setup TWHttpCheckOut Precondition ==")

    def test_TWHttpCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCheckOut-01.xml")

    def test_TWHttpCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCheckOut-02.xml")

    def test_TWHttpCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCheckOut-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpCheckOut Post condition =="
        dumplogger.info("== Setup TWHttpCheckOut Post condition ==")
