import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDHttpChat(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpChat Precondition =="
        dumplogger.info("== Setup IDHttpChat Precondition ==")

    def test_IDHttpChat01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpChat-01.xml")

    def test_IDHttpChat02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpChat-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpChat Post condition =="
        dumplogger.info("== Setup IDHttpChat Post condition ==")
