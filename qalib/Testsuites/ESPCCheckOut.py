import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCCheckOut Precondition =="
        dumplogger.info("== Setup ESPCCheckOut Precondition ==")

    def test_ESPCCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCheckOut-01.xml")

    def test_ESPCCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCheckOut-02.xml")

    def test_ESPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCheckOut-03.xml")

    def test_ESPCCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCheckOut-04.xml")

    def test_ESPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCheckOut-05.xml")

    def test_ESPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCCheckOut-06.xml")

    # def test_ESPCCheckOut07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/ES/ESPCCheckOut-07.xml")

    # def test_ESPCCheckOut08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/ES/ESPCCheckOut-08.xml")

    # def test_ESPCCheckOut09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/ES/ESPCCheckOut-09.xml")

    # def test_ESPCCheckOut10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/ES/ESPCCheckOut-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCCheckOut Post condition =="
        dumplogger.info("== Setup ESPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
