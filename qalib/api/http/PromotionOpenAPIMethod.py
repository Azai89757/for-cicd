#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 PromotionOpenAPIMethod.py: The def of this file called by other function.
'''
##Import system library
import hashlib
import hmac

##Import common library
import XtFunc
import time
import FrameWorkBase
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def ReplacePromotionOpenAPIURL(arg):
    '''
    ReplacePromotionOpenAPIURL : Replace the Promotion Open API url cause timestamp & sign are dynamic
                Input argu :
                    partner_id - your partner_id in your Opan API app
                    api_path - the API path you want to post
                    access_token - the token you get from /api/v2/shop/auth_partner
                    shop_id - the id of your shop
                    partner_key - your partner_key in your Opan API app
                Return code :
                    1 - success
                    0 - failed
    '''
    ret = 1
    partner_id = arg["partner_id"]
    api_path = arg["api_path"]
    access_token = arg["access_token"]
    shop_id = arg["shop_id"]
    partner_key = arg["partner_key"]
    url = str(GlobalAdapter.APIVar._JsonCaseData_["url"])

    ##Get current time
    datetime = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)
    unix_time = time.mktime(time.strptime(datetime, "%Y-%m-%d %H:%M"))
    ##Replace unix_time to url
    if "str_to_be_replaced" in url:
        dumplogger.info("URL before replaced:" + url)
        url = url.replace("str_to_be_replaced", str(int(unix_time)))
        dumplogger.info("URL replaced result:" + GlobalAdapter.APIVar._HttpUrl_)
    ##Replace sign(signature) to url
    if "sign_to_be_replaced" in url:
        dumplogger.info("URL before replaced:" + url)
        base_string = "%s%s%s%s%s" % (partner_id, api_path, int(unix_time), access_token, shop_id)
        sign = hmac.new(partner_key, base_string, hashlib.sha256).hexdigest()
        GlobalAdapter.APIVar._HttpUrl_ = url.replace("sign_to_be_replaced", sign)
        dumplogger.info("URL replaced result:" + GlobalAdapter.APIVar._HttpUrl_)
    else:
        dumplogger.info("Please check your _DynamicCaseData_ stored is correct or not !!")
        ret = 0

    OK(ret, int(arg['result']), 'ReplacePromotionOpenAPIURL')
