import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSignup Precondition =="
        dumplogger.info("== Setup SGPCSignup Precondition ==")

    def test_SGPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSignup Post condition =="
        dumplogger.info("== Setup SGPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
