import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCCart Precondition =="
        dumplogger.info("== Setup PHPCCart Precondition ==")

    def test_PHPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCCart Post condition =="
        dumplogger.info("== Setup PHPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
