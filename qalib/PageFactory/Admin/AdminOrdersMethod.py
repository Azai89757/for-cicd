﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminOrdersMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import re
import time

##Import common library
import Util
import Config
import XtFunc
import datetime
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import API related library
from api import APICommonMethod
from api.http import HttpAPICore
from api.http import SellerCenterAPIMethod

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##Import OMS library
from PageFactory.Admin.Order import AdminOMSMethod

##Import DB library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminOrderPageCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckValidDate(arg):
        '''
        CheckValidDate : Check valid date in all admin order page (Admin Portal → Orders)
                Input argu :
                    location - location element display date time you want to check it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        location = arg['location']

        if Config._TestCaseRegion_ in ("MX", "BR", "MY", "SG", "CO", "CL", "PL", "ES", "FR"):
            ##Admin order date time string (DD-MM-YYYY) for MX region
            date_string_format = "%d-%m-%Y"
            date_string_regex = r"\d{2}-\d{2}-\d{4}"
        else:
            ##Admin order date time string (YYYY-MM-DD) for other region
            date_string_format = "%Y-%m-%d"
            date_string_regex = r"\d{4}-\d{2}-\d{2}"

        ##Get current date (YYYY-MM-DD or DD-MM-YYYY)
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(date_string_format), date_string_format)

        ##Get locate element display date time
        xpath = Util.GetXpath({"locate":location})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Get date time from element text (YYYY-MM-DD or DD-MM-YYYY)
        match = re.match(date_string_regex, GlobalAdapter.CommonVar._PageAttributes_)

        ##Check regex match result
        if match:
            ##Get valid date (YYYY-MM-DD or DD-MM-YYYY)
            valid_date = datetime.datetime.strptime(match.group(0), date_string_format)
            ##Check valid date <= current date
            if valid_date <= current_date:
                dumplogger.info("Locate valid date is less than current date!!")
            else:
                ret = 0
                dumplogger.info("Locate valid date is less than current date!!")
        else:
            ret = 0
            dumplogger.info("No any date time string (YYYY-MM-DD or DD-MM-YYYY) in locate element!!")

        OK(ret, int(arg['result']), 'AdminOrderPageCommon.CheckValidDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderID(arg):
        '''
        CheckOrderID : Check order id in the page.
            Input argu :
                    page - order_page/order_mass_cancellation_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page = arg["page"]

        ##Check order ID in page
        xpath = Util.GetXpath({"locate":page})
        xpath = xpath.replace('order_id_to_be_replace', GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPageCommon.CheckOrderID')


class AdminOrderPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchOnOrderListPage(arg):
        '''
        SearchOnOrderListPage : Search seller on order list page
                Input argu :
                    search_type - seller / buyer
                    username - use seller or buyer account to search for orders
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        search_type = arg["search_type"]
        username = arg["username"]

        ##Input seller name and search
        xpath = Util.GetXpath({"locate":search_type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":username, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.SearchOnOrderListPage -> ' + username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToOrderDetail(arg):
        '''
        GoToOrderDetail : Go to order detail
                Input argu :
                    search_type - seller / buyer
                    username - use seller or buyer account to search for orders
                    row_number - the row number of the order which you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        search_type = arg["search_type"]
        username = arg["username"]
        row_number = arg["row_number"]

        ##Input username and search
        AdminOrderPage.SearchOnOrderListPage({"search_type":search_type, "username":username, "result": "1"})

        ##Check target user on list page
        xpath = Util.GetXpath({"locate":"target_" + search_type + "_name"})
        xpath = xpath.replace('replace_user', username)
        time.sleep(2)

        ##Wait before table reload before next step
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Click order name
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"order_id"})
        xpath = xpath.replace('replace_row_number', str(int(row_number)+1))
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click target table", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.GoToOrderDetail -> ' + username)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToCheckoutDetail(arg):
        '''
        GoToCheckoutDetail : Click Checkout ID for forward to Checkout detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Checkout ID number for redirect to checkout detail page
        xpath = Util.GetXpath({"locate":"checkout_id"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click checkout ID number", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOrderPage.GoToCheckoutDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefundIdLink(arg):
        '''
        ClickRefundIdLink : Go to Refund Request Detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click refund id
        xpath = Util.GetXpath({"locate":"refund_id"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click refund id", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickRefundIdLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToOFGDetail(arg):
        '''
        GoToOFGDetail : Go to ofg detail
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ofg id
        xpath = Util.GetXpath({"locate":"ofg_id"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ofg id", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOrderPage.GoToOFGDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PayoutByBankTransfer(arg):
        '''
        PayoutByBankTransfer : Click Pay btn in Checkout detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        for retry_times in range(3):
            ##Click Checkout ID number for redirect to checkout detail page
            xpath = Util.GetXpath({"locate":"pay_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click pay btn", "result": "1"})

            time.sleep(3)

            ##Check had pay sucessfully in payment log
            xpath = Util.GetXpath({"locate":"success_pay_text"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                dumplogger.info("success pay!")
                ret = 1
                break
            else:
                dumplogger.info("text display error!")
                ret = 0

        OK(ret, int(arg['result']), 'AdminOrderPage.PayoutByBankTransfer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetFOrderID(arg):
        '''
        GetFOrderID : Get FOrder ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Slide down to print screenshot
        BaseUICore.ExecuteScript({"script": 'document.getElementById("article").scrollTo(0,500)', "result": "1"})

        ##Get forder id locate element text
        xpath = Util.GetXpath({"locate":"forder_id_" + Config._TestCaseRegion_.lower()})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Get forder id by regex
        match = re.search(r"FOrder ID.+?(\d+)", GlobalAdapter.CommonVar._PageAttributes_)
        if match:
            GlobalAdapter.OrderE2EVar._FOrderID_ = match.group(1)
            dumplogger.info("FOrder ID: %s" % (GlobalAdapter.OrderE2EVar._FOrderID_))
        else:
            ret = 0
            dumplogger.info("No any FOrder ID in locate element text => \"%s\"" % (GlobalAdapter.CommonVar._PageAttributes_))

        OK(ret, int(arg['result']), 'AdminOrderPage.GetFOrderID-> ' + GlobalAdapter.OrderE2EVar._FOrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetCheckoutID(arg):
        '''
        GetCheckoutID : get checkout ID
                Input argu :
                    seller_name - get order id by search seller name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_name = arg['seller_name']

        time.sleep(2)

        ##Search seller
        xpath = Util.GetXpath({"locate": "seller_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": seller_name, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        time.sleep(10)

        ##Get checkout ID
        xpath = Util.GetXpath({"locate": "id_table"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "checkout_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.GetCheckoutID-> ' + GlobalAdapter.OrderE2EVar._CheckOutID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderID(arg):
        '''
        GetOrderID : Get Order ID in admin orders page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get order id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"order_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"order_id", "result": "1"})

        ##The purpose below is to handle shipping by api because OFG deployed to staging.
        ##Slide down to print screenshot
        BaseUICore.ExecuteScript({"script": 'document.getElementById("article").scrollTo(0,500)', "result": "1"})

        ##Get ofg id locate element text
        xpath = Util.GetXpath({"locate":"ofg_id_" + Config._TestCaseRegion_.lower()})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode":"single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Get ofg id by regex
        match = re.search(r"OFG ID.+?(\d+)", GlobalAdapter.CommonVar._PageAttributes_)
        if match:
            GlobalAdapter.OrderE2EVar._OFGID_ = match.group(1)
            dumplogger.info("OFG ID: %s" % (GlobalAdapter.OrderE2EVar._OFGID_))
        else:
            ret = 0
            dumplogger.info("No any OFG ID in locate element text => \"%s\"" % (GlobalAdapter.CommonVar._PageAttributes_))

        OK(ret, int(arg['result']), 'AdminOrderPage.GetOrderID -> ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderSN(arg):
        '''
        GetOrderSN : Get Order SN in admin orders page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Sleep 5 second to wait order sn element text is shown
        time.sleep(5)

        ##Get order sn
        xpath = Util.GetXpath({"locate":"order_sn"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"ordersn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.GetOrderSN -> %s' % (GlobalAdapter.OrderE2EVar._OrderSNDict_))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetReturnID(arg):
        '''
        GetReturnID : Get Return ID in admin orders page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get order id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"return_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"return_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.GetReturnID -> ' + GlobalAdapter.OrderE2EVar._ReturnID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEscrowAmountButton(arg):
        '''
        ClickEscrowAmountButton : Click escrow amount button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click escrow amount button
        xpath = Util.GetXpath({"locate":"escrow_detail_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click escrow amount button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickEscrowAmountButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckoutIdLink(arg):
        '''
        ClickCheckoutIdLink : Click checkout id link
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click checkout id link
        xpath = Util.GetXpath({"locate":"checkout_id_link"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click checkout id link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickCheckoutIdLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSellerTxnFeeRuleLink(arg):
        '''
        ClickSellerTxnFeeRuleLink : Click seller txn fee rule link
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click seller txn fee rule link
        xpath = Util.GetXpath({"locate":"seller_txn_fee_rule_link"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click seller txn fee rule link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickSellerTxnFeeRuleLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLogisticStatus(arg):
        '''
        CheckLogisticStatus : Check order logistic status in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check logistic status is request created
        xpath = Util.GetXpath({"locate":"logistic_status"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Logistic status is request created.")
        else:
            dumplogger.info("Logistic status is not request created.")
            dumplogger.info("Sending repush API")

            ##Repush arrange shipment by API
            HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})
            HttpAPICore.GetAndSetHttpAPIData({"collection": "repush_arrange_shipment", "result": "1"})
            SellerCenterAPIMethod.SellerCenterOMSAPI.AssignDataForRepushArrangeShipment({"result": "1"})
            HttpAPICore.SendHttpRequest({"http_method":"get", "result": "1"})
            APICommonMethod.CheckAPIResponseCode({"result": "1"})
            HttpAPICore.DeInitialHttpAPI({"result": "1"})

            ##Refresh browser to check logistic status
            BaseUILogic.BrowserRefresh({"message": "Refresh page", "result": "1"})
            time.sleep(10)

            ##Recheck logistic status
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Logistic status is request created.")
            else:
                ##Go to ISC tool to take a screenshot of order status
                dumplogger.info("Logistic status is not request created after repushing order id.")
                AdminOMSMethod.AdminOMSPushStatus.OMSCheckArrangeShipmentStatus({"result": "1"})
                ret = 0

        OK(ret, int(arg['result']), 'AdminOrderPage.CheckLogisticStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPromotionId(arg):
        '''
        CheckPromotionId : Check promotion id in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check promotion id exist in admin order detail page
        xpath = Util.GetXpath({"locate":"promotion_id_text"})
        xpath_promotion_id = xpath.replace("promotion_id_to_be_replace", GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[0]["id"])
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_promotion_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.CheckPromotionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShipByDateTimeDiff(arg):
        '''
        CheckShipByDateTimeDiff : Check ship by date time difference (time difference = X - ship by date)
                Input argu :
                    hours - expect number of hours difference (hours difference = X - ship by date)
                    type - auto_cancel_layer1 / auto_cancel_layer2 / auto_cancel_layer3. Example: If you use auto_cancel_layer1 means => (time difference = auto cancel layer1 date - ship by date)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        hours = arg["hours"]
        type = arg["type"]

        ##Get target element to calculate time difference
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        ##Get date time from element text (YYYY-MM-DD HH:MM)
        target_calculate_date_match = re.match(r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}", GlobalAdapter.CommonVar._PageAttributes_)

        ##Get ship by date element
        xpath = Util.GetXpath({"locate":"ship_by_date_text"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        ##Get date time from element text (YYYY-MM-DD HH:MM)
        ship_by_date_match = re.match(r"\d{4}-\d{2}-\d{2} \d{2}:\d{2}", GlobalAdapter.CommonVar._PageAttributes_)

        ##Check regex match result
        if ship_by_date_match and target_calculate_date_match:
            ##Get target calculate date time from element text (YYYY-MM-DD HH:MM)
            target_calculate_date = datetime.datetime.strptime(target_calculate_date_match.group(0), "%Y-%m-%d %H:%M")
            dumplogger.info("Target calculate date time => %s" % (target_calculate_date))

            ##Get ship by date time from element text (YYYY-MM-DD HH:MM)
            ship_by_date = datetime.datetime.strptime(ship_by_date_match.group(0), "%Y-%m-%d %H:%M")
            dumplogger.info("Ship by date time => %s" % (ship_by_date))

            ##Convert expect result to hours time delta
            expect_hours_delta = datetime.timedelta(hours = int(hours))
            dumplogger.info("Expect hours time delta => %s" % (expect_hours_delta))

            ##Compare expect hours difference and actual hours difference
            if expect_hours_delta == (target_calculate_date - ship_by_date):
                dumplogger.info("Expect hours difference is actual hours difference!!")
            else:
                ret = 0
                dumplogger.info("Expect hours difference is not actual hours difference!!")
        else:
            ret = 0
            dumplogger.info("No any date time string (YYYY-MM-DD HH:MM) in locate element!!")

        OK(ret, int(arg['result']), 'AdminOrderPage.CheckShipByDateTimeDiff')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionInfo(arg):
        '''
            ClickPromotionInfo : Click promotion info inside order detail page
                Input argu :
                    column - column's value to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column = arg['column']

        ##Click promotion info in admin order detail page
        xpath = Util.GetXpath({"locate":column})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message":"Click " + column, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickPromotionInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSellerTransactionFeeRuleId(arg):
        '''
        CheckSellerTransactionFeeRuleId : Check seller transaction rule id in admin order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check seller transaction rule id in admin order detail page
        xpath = Util.GetXpath({"locate":"seller_transaction_rule_id"})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._TransactionFeeCSRuleID_, "isfuzzy": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderPage.CheckSellerTransactionFeeRuleId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRevealPaymentInfo(arg):
        '''
        ClickRevealPaymentInfo : Click reveal button of grand total and payment type in order detail page for ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click reveal button of grand total
        xpath = Util.GetXpath({"locate": "grand_total_reveal"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reveal button of grand total", "result": "1"})
        #Click reveal button of payment type
        xpath = Util.GetXpath({"locate": "payment_type_reveal"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reveal button of payment type", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickRevealPaymentInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCodingMonkey(arg):
        '''
        ClickCodingMonkey : Click coding monkey button in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click coding monkey button
        xpath = Util.GetXpath({"locate": "coding_monkey_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click coding monkey button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOrderPage.ClickCodingMonkey')


class AdminOrderMassCancellationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetLogID(arg):
        '''
        GetLogID : Check log ID in mass cancellation page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get log id locate element
        xpath = Util.GetXpath({"locate":"mass_cancel_history_logid"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Get log id by regex
        match = re.match(r".+\"logid\": (\d+)", GlobalAdapter.CommonVar._PageAttributes_)

        ##Check regex match result
        if match:
            ##Save logid in global _CancelLogID_
            GlobalAdapter.OrderE2EVar._CancelLogID_ = match.group(1)
            dumplogger.info("Suceessful save in global _CancelLogID_ => %s" % (GlobalAdapter.OrderE2EVar._CancelLogID_))
        else:
            ret = 0
            dumplogger.info("Can't find logid in locate element!!!")

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.GetLogID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderSN(arg):
        '''
        CheckOrderSN : Check order SN in mass cancellation page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check order SN in mass cancellation page
        xpath = Util.GetXpath({"locate":"mass_cancel_result_order_sn"})
        xpath = xpath.replace('order_sn_to_be_replace', GlobalAdapter.OrderE2EVar._OrderSNDict_["ordersn"][-1])
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.CheckOrderSN')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLogID(arg):
        '''
        CheckLogID : Check log id in mass cancellation page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check log ID in mass cancellation page
        xpath = Util.GetXpath({"locate":"mass_cancel_result_log_id"})
        xpath = xpath.replace('log_id_to_be_replace', GlobalAdapter.OrderE2EVar._CancelLogID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.CheckLogID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchMassCancelResult(arg):
        '''
        SearchMassCancelResult : Search mass cancel result by global order ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Search mass cancel result by global order id
        xpath = Util.GetXpath({"locate":"order_id_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.SearchMassCancelResult -> order ID: %s' % (GlobalAdapter.OrderE2EVar._OrderID_))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyOrderidInCSV(arg):
        '''
        ModifyOrderidInCSV : Write the order id into csv file
                Input argu :
                            input_row - input the row number where you want to input the order id in csv
                            filename - input the file name of csv file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_row = arg['input_row']
        filename = arg['filename']

        ##Get the path of csv file and read the file
        slash = Config.dict_systemslash[Config._platform_]

        ##get the file name and store in global variable
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + filename + ".csv"

        ##Get the global variable "_FilePath_" and read the csv file into list
        data_list = XtFunc.CsvReader(file_path)

        ##Write the order id into csv file
        XtFunc.CsvWritter({"input_row":input_row, "input_column":"1", "value":GlobalAdapter.OrderE2EVar._OrderID_, "data_list":data_list, "file_path":file_path, "result":arg['result']})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.ModifyOrderidInCSV -> order id is ' + GlobalAdapter.OrderE2EVar._OrderID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadMassCancelCSV(arg):
        '''
        UploadMassCancelCSV : Upload csv file for mass cancellation
                Input argu :
                        file_name - file name
                        type_choose - choose the type what you want to do for the order:
                                      "logistics_invalid" or "fraud_cancel" = '0'
                                      "request_cancelled" = '1'
                                      "pick_up_fail" = '2'
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        type_choose = arg['type_choose']

        ##Click choose file button
        xpath = Util.GetXpath({"locate": "choose_file_btn"})
        xpath = xpath.replace("replace_number", type_choose)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})
        time.sleep(5)

        ##Upload user csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_btn"})
        xpath = xpath.replace("replace_number", type_choose)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})
        time.sleep(3)

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.UploadMassCancelCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancellationResultButton(arg):
        '''
        ClickCancellationResultButton : Click "Cancellation Result" Button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(3)

        ##Select rule status filter
        xpath = Util.GetXpath({"locate": "cancellation_result_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancellation result button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.ClickCancellationResultButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStopUploadcsvButton(arg):
        '''
        ClickStopUploadcsvButton : Click "Stop" Button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(3)

        ##Select rule status filter
        xpath = Util.GetXpath({"locate": "red_stop_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click red stop button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderMassCancellationPage.ClickStopUploadcsvButton')


class AdminEscrowDetailsWindow:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckEscrowCalculation(arg):
        '''
        CheckEscrowCalculation : Check the sum of each field is equal to escrow amount
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get titles of each field
        xpath = Util.GetXpath({"locate":"titles"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "multi", "result": "1"})
        headers = GlobalAdapter.CommonVar._PageAttributesList_

        ##Get values of each field
        xpath = Util.GetXpath({"locate":"values"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "multi", "result": "1"})
        values = GlobalAdapter.CommonVar._PageAttributesList_

        ##Convert two list into a dictionary
        escrow_details = {}
        for index in range(len(headers)):
            escrow_details[headers[index]] = float(values[index].replace(',',''))

        ##Check if calculation correct
        escrow_calculation = escrow_details['order_price']\
            + escrow_details['order_voucher']\
            + escrow_details['order_coins_offset']\
            + escrow_details['credit_card_promotion_offset']\
            + escrow_details['order_rebate']\
            + escrow_details['order_shipping']\
            - escrow_details['order_cc_txn_fee']\
            - escrow_details['order_commission_fee']\
            - escrow_details['order_tax']\
            - escrow_details['escrow_tax_amount']\
            - escrow_details['order_service_fee']\
            - escrow_details['order_insurance_subtotal']
        dumplogger.info("The result of calculation is " + str(escrow_calculation))
        dumplogger.info("The value in \'escrow_amount\' field is " + str(escrow_details['escrow_amount']))

        ##Check if the result is equal to the value in 'escrow_amount' field
        if abs(escrow_calculation - escrow_details['escrow_amount']) < 0.001:
            dumplogger.info("The result is equal to the value in \'escrow_amount\' field.")
        else:
            ret = 0
            dumplogger.info("The result is not equal to the value in \'escrow_amount\' field!!")

        OK(ret, int(arg['result']), 'AdminEscrowDetailsWindow.CheckEscrowCalculation')


class AdminOrderCommissionFeePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click Local / Local-C2C / Local-B2C / CrossBorder tab
                Input argu :
                    tab - local / c2c / b2c / cb
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        tab = arg["tab"]

        ##Click 'Local' tab first before clicking c2c and b2c
        if tab in ("c2c","b2c"):
            xpath = Util.GetXpath({"locate":"local"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click the tab -> " + tab, "result": "1"})

        ##Click the tab
        xpath = Util.GetXpath({"locate":tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click the tab -> " + tab, "result": "1"})

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.ClickTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleNameField(arg):
        '''
        InputRuleNameField : Input rule name in 'Commission Fee Settings - Create' page
                Input argu :
                    rule_name - commission fee rule name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg["rule_name"]

        ##Input rule name
        xpath = Util.GetXpath({"locate":"rule_name_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.InputRuleNameField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTimePeriodField(arg):
        '''
        InputTimePeriodField : Input start and end period of the rule in 'Commission Fee Settings - Create' page
                Input argu :
                    start_day - rule validity start time delta
                    end_day- rule  validity end time delta
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_day = arg["start_day"]
        end_day = arg["end_day"]

        ##Input start period
        start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", int(start_day), 0, 0)
        xpath = Util.GetXpath({"locate":"start_period_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":start_date, "result": "1"})

        ##Input end period
        end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", int(end_day), 0, 0)
        xpath = Util.GetXpath({"locate":"end_period_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":end_date, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.InputTimePeriodField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRateChargedField(arg):
        '''
        InputRateChargedField : Input rate charged field in 'Commission Fee Settings - Create' page
                Input argu :
                    rate_charged - commission fee rule rate charged
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rate_charged = arg["rate_charged"]

        ##Input rate charged
        xpath = Util.GetXpath({"locate":"rate_charged_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":rate_charged, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.InputRateChargedField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectShopCategory(arg):
        '''
        SelectShopCategory : Select shop category in 'Commission Fee Settings - Create' page
                Input argu :
                    category_type - c2c / b2c / cb
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["category_type"]

        category_type = {
            "c2c": "0",
            "b2c": "1",
            "cb": "2"
        }

        time.sleep(3)

        ##Click shop category field
        xpath = Util.GetXpath({"locate":"dropdown_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click drop dwon icon", "result": "1"})

        time.sleep(3)

        ##Select shop category
        xpath = Util.GetXpath({"locate": "select_icon"})
        xpath = xpath.replace('value_to_be_replace', category_type[type])
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select shop catagory", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.SelectShopCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSellerShopsidsCsv(arg):
        '''
        UploadSellerShopsidsCsv : Upload seller shopsids csv file in 'Commission Fee Settings - Create' page
                Input argu :
                    upload_csv - upload csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_csv = arg["upload_csv"]

        ##Upload seller shopsids csv file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "import_sellerids", "element_type": "id", "project": Config._TestCaseFeature_, "action": "file", "file_name": upload_csv, "file_type": "csv", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.UploadSellerShopsidsCsv -> ' + upload_csv)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetRuleID(arg):
        '''
        GetRuleID : Get commission fee rule ID
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get service fee rule ID
        xpath = Util.GetXpath({"locate":"rule_id"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"commission_fee_rule_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.GetRuleID -> ' + GlobalAdapter.CommissionFeeE2EVar._RuleID_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchRule(arg):
        '''
        SearchRule : Input information to search rule in commission fee page
                Input argu :
                    search_type - search by rule_id / group_id / rule_name / status
                    input_content - search keyword, must be included when search_type is not rule_id or status
                    status_type - any / active / upcoming / expired / deleted, only needed when search_type is status
                    is_click - 1 - click rule ID
                               0 - do not click rule ID
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        input_content = arg["input_content"]
        status_type = arg["status_type"]
        is_click = arg["is_click"]

        status_index = {
            "any": "~~~",
            "active": "0",
            "upcoming": "1",
            "expired": "2",
            "deleted": "3"
        }

        ##Search rule by specific search type
        if search_type == "status" and status_type:
            ##Click drop down list
            xpath = Util.GetXpath({"locate": "status_drop_down"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule status drop down", "result": "1"})

            ##Click the status
            xpath = Util.GetXpath({"locate": "status"})
            xpath = xpath.replace("status_type", status_index[status_type])
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click the status", "result": "1"})
        else:
            ##Get ruleID by GetRuleID function
            if not input_content:
                input_content = GlobalAdapter.CommissionFeeE2EVar._RuleID_

            ##Search with input_content
            xpath = Util.GetXpath({"locate": search_type})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Wait for page loading
        time.sleep(5)

        ##Click RuleID to information page
        if int(is_click):
            xpath = Util.GetXpath({"locate": "search_result"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click group ID link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePage.SearchRule')


class AdminOrderCommissionFeePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNewRule(arg):
        '''
        ClickNewRule : Click new rule btn in order commission fee page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click new rule tab
        xpath = Util.GetXpath({"locate":"new_rule_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click new rule tab", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePageButton.ClickNewRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadSellerShopids(arg):
        '''
        ClickUploadSellerShopids : Click upload seller shopids button in 'Commission Fee Settings - Create' page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload seller shopids button
        xpath = Util.GetXpath({"locate": "upload_seller_shopids_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePageButton.ClickUploadSellerShopids')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreate(arg):
        '''
        ClickCreate : Click create button in 'Commission Fee Settings - Create' page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create button
        xpath = Util.GetXpath({"locate":"create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create button", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePageButton.ClickCreate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportSellerShopIds(arg):
        '''
        ClickExportSellerShopIds : Click export seller shop Ids button in rule setting page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Wait for web loading
        time.sleep(3)

        ##Click export seller shop Ids button
        xpath = Util.GetXpath({"locate":"export_seller_shop_ids_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click export seller shop Ids button", "result": "1"})

        ##Wait for download
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePageButton.ClickExportSellerShopIds')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadChangeLog(arg):
        '''
        ClickDownloadChangeLog : Click download button in change log of rule setting page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download button in change log
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download change log btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOrderCommissionFeePageButton.ClickDownloadChangeLog')
