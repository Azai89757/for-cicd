import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCMePage Precondition =="
        dumplogger.info("== Setup TWPCMePage Precondition ==")

    def test_TWPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCMePage-01.xml")

    def test_TWPCMePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCMePage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCMePage Post condition =="
        dumplogger.info("== Setup TWPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
