#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 GatewayAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import json

##Import Framework common library
import FrameWorkBase
import Config
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter
import XtFunc

##Import api library
from api import APICommonMethod
import HttpAPICore
import HttpAPILogic
import DLAdminAPIMethod

##Import DB library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class GatewayProductDetailPageAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddProductToCart(arg):
        '''
        AddProductToCart : add specific product to cart by API
                Input argu :
                    collection - payload collection to be sent
                    shopid - shop id of product which want to add to cart in payload
                    itemid - item id of product which want to add to cart in payload
                    modelid - model id of product which want to add to cart in payload
                    quantity - quantity of product which want to add to cart in payload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        collection = arg["collection"]
        shopid = arg['shopid']
        itemid = arg['itemid']
        modelid = arg['modelid']
        quantity = arg['quantity']

        ##Use old method, set feature json as template to send request
        if collection:

            ##Initial Http Api
            HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"", "result": "1"})

            ##Load Api data to be sent
            HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})

            try:
                ##Replace add_on_deal_id from 0 to global variable when prmotion items
                if "orders" in GlobalAdapter.APIVar._HttpPayload_:
                    for index in range(0, len(GlobalAdapter.APIVar._HttpPayload_["orders"][0]["items"])):
                        if not GlobalAdapter.APIVar._HttpPayload_["orders"][0]["items"][index]["add_on_deal_id"]:
                            GlobalAdapter.APIVar._HttpPayload_["orders"][0]["items"][index]["add_on_deal_id"] = int(GlobalAdapter.PromotionE2EVar._PromotionIDList_[-1])
            ##Exception Handle
            except KeyError:
                dumplogger.exception("Encounter Key Error!!!")
                ret = -1
            except:
                dumplogger.exception("Encounter Other Error!!!")
                ret = -1

        ##Use new method, set common json as template to send request
        elif not collection and itemid and shopid:

            ##Set collection name for mall login
            region = Config._TestCaseRegion_.lower()
            env = Config._EnvType_
            collection = region + "_" + env

            ##Initial Http Api
            HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"common/checkout/add_to_cart_common", "result": "1"})

            ##Load Api data to be sent
            HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})

            ##Assign shopid and itemid from argument to global
            GlobalAdapter.CommonVar._DynamicCaseData_["shopid"] = int(shopid)
            GlobalAdapter.CommonVar._DynamicCaseData_["itemid"] = int(itemid)
            GlobalAdapter.CommonVar._DynamicCaseData_["quantity"] = int(quantity)

            ##Set payload value from global
            APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "shopid", "target": "shopid", "time_deviation": "", "result": "1"})
            APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "itemid", "target": "itemid", "time_deviation": "", "result": "1"})
            APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "quantity", "target": "quantity", "time_deviation": "", "result": "1"})

            ##Check there is model id or not
            if modelid:
                ##Assign modelid from argument to global and Set payload value from global
                GlobalAdapter.CommonVar._DynamicCaseData_["modelid"] = int(modelid)
                APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "modelid", "target": "modelid", "time_deviation": "", "result": "1"})
            else:
                ##Popup modelid column in payload
                dumplogger.info("Do not set modelid argument, popup modelid column in payload")
                del GlobalAdapter.APIVar._HttpPayload_["modelid"]

        else:
            ret = -1
            dumplogger.info("Please check your argument, if need set specific collection as request, input collection argument. Or just input shopid/itemid/modelid to use common collection as request")

        ##Set request cookie
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

        ##Value in dictionary of payload in add_to_cart_batch api has to be string, not dict
        if "add_to_cart_batch" in GlobalAdapter.APIVar._HttpUrl_:
            ##Set add_to_cart_batch payload value to string
            for key, value in GlobalAdapter.APIVar._HttpPayload_.items():
                GlobalAdapter.APIVar._HttpPayload_[key] = json.dumps(value)

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send http request with retry method
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        OK(ret, int(arg['result']), 'GatewayProductDetailPageAPI.AddProductToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetShopInformation(arg):
        '''
        GetShopInformation : get shop information
                Input argu :
                        account_db - which database does your account belongs to
                Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        account_db = arg['account_db']

        ##Initial Http API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get json data
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_shop_info", "result": "1"})

        ##Access database to get the last active time for comparing local json data with api response
        DBCommonMethod.InitialSQL({"db_name":account_db, "result": "1"})

        DBCommonMethod.GetAndSetSQLData({"file_name": "get_shop_last_active_time", "result": "1"})

        DBCommonMethod.SendSQLCommandToDB({"method": "select", "result": "1"})

        DBCommonMethod.StoreDataFromSQLResult({"column": "web_last_login", "result": "1"})

        APICommonMethod.AssignDataToAPIRequest({"assign_to": "response_text", "source": "web_last_login", "target": "last_active_time", "time_deviation": "5", "result": "1"})

        ##If we get the data from database, then excute the comparing stage
        if GlobalAdapter.DBVar._DBResult_:

            ##Send API to get response
            HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})

            ##Compare response code
            APICommonMethod.CheckAPIResponseCode({"result": "1"})

            ##Compare response text
            APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Data not found from database,Save the information and dump message
        else:
            XtFunc.SaveCaseInfoForIssueReport({"info_key": "DB_issue", "info_value": "Last active time not found", "result": "1"})
            dumplogger.info("Data not found, the _DBResult_ is empty")

        OK(ret, int(arg['result']), 'GatewayProductDetailPageAPI.GetShopInformation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetItemInformation(arg):
        '''
        GetItemInformation : get the item information
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get json data
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_item", "result": "1"})

        ##Send API to get response
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})

        ##Compare response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Compare response text
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayProductDetailPageAPI.GetItemInformation')


class GatewayCartAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def Checkout(arg):
        '''
        Checkout : Send cart_checkout api from cart page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"", "result": "1"})

        ##Get and set Http Api payload
        HttpAPICore.GetAndSetHttpAPIData({"collection":"cart_checkout", "result": "1"})

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Set http headers cookie
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

        ##Send http request with retry method
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        OK(ret, int(arg['result']), 'GatewayCartAPI.Checkout')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CartGet(arg):
        '''
        CartGet : Send cart_get api from cart page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Set collection name for mall login
        region = Config._TestCaseRegion_.lower()
        env = Config._EnvType_
        collection = region + "_" + env

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"common/checkout/cart_get", "result": "1"})

        ##Get and set Http Api payload
        HttpAPICore.GetAndSetHttpAPIData({"collection": collection, "result": "1"})

        ##Set http headers cookie
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

        ##Send http request with retry method
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        OK(ret, int(arg['result']), 'GatewayCartAPI.CartGet')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanCart(arg):
        '''
        CleanCart : Send cart_update api from cart page to clean cart
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Set collection name for mall login
        region = Config._TestCaseRegion_.lower()
        env = Config._EnvType_
        collection = region + "_" + env

        ##Get product info in global and assign to product_info
        response_text = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Check get product or not
        if response_text["data"]["shop_order_ids"]:

            ##There is any product in cart, delete product by api
            for shop in response_text["data"]["shop_order_ids"]:
                for item in shop["item_briefs"]:

                    ##Initial Http Api
                    HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"common/checkout/cart_update", "result": "1"})

                    ##Get and set Http Api payload
                    HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})

                    ##assign data from checkout get into global
                    GlobalAdapter.CommonVar._DynamicCaseData_["shopid"] = int(shop['shopid'])
                    GlobalAdapter.CommonVar._DynamicCaseData_["itemid"] = int(item['itemid'])
                    if item['modelid']:
                        GlobalAdapter.CommonVar._DynamicCaseData_["modelid"] = int(item['modelid'])
                    else:
                        GlobalAdapter.CommonVar._DynamicCaseData_["modelid"] = None
                    if item['item_group_id']:
                        GlobalAdapter.CommonVar._DynamicCaseData_["item_group_id"] = int(item['item_group_id'])
                    else:
                        GlobalAdapter.CommonVar._DynamicCaseData_["item_group_id"] = None

                    ##Set payload value from global
                    ##Change shopid in cart update payload data['data'][shop_order_ids]
                    APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "shopid", "target": "shopid", "time_deviation": "", "result": "1"})
                    ##Change shopid in cart update payload data['data'][shop_order_ids][item_briefs]
                    APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "shopid", "target": "shopid", "time_deviation": "", "result": "1"})
                    ##Change itemid in cart update payload data['data'][shop_order_ids][item_briefs]
                    APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "itemid", "target": "itemid", "time_deviation": "", "result": "1"})
                    ##Change modelid in cart update payload data['data'][shop_order_ids][item_briefs]
                    APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "modelid", "target": "modelid", "time_deviation": "", "result": "1"})
                    ##Change item_group_id in cart update payload data['data'][shop_order_ids][item_briefs]
                    APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "item_group_id", "target": "item_group_id", "time_deviation": "", "result": "1"})

                    ##Change payload format to json string
                    APICommonMethod.ChangePayloadDictToStr({"result": "1"})

                    ##Set http headers cookie
                    GlobalAdapter.APIVar._HttpHeaders_["cookie"] = GlobalAdapter.APIVar._Csrftoken_

                    ##Send http request with retry method
                    ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        else:
            ##There is not product in cart
            dumplogger.info("There is not any product in cart")
            ret = 1

        OK(ret, int(arg['result']), 'GatewayCartAPI.CleanCart')


class GatewayMallPageAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetMallPageCategoryInfo(arg):
        '''
        GetMallPageCategoryInfo : Get mall page category info
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check enter mall page category api info
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "enter_mall_page_first_time", "result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayMallPageAPI.GetMallPageCategoryInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOfficialShopOfCategory(arg):
        '''
        GetOfficialShopOfCategory : Get official shop info of category
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Enter category of mall page and get official shop info
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "enter_mall_page_categories_tab", "result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayMallPageAPI.GetOfficialShopOfCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetSpecificCategoryBrandInfo(arg):
        '''
        GetSpecificCategoryBrandInfo : Get specific category tab brand info
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Enter category of mall page and get brand info
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "click_specific_category_tab", "result": "1"})
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayMallPageAPI.GetSpecificCategoryBrandInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UpdateCategoryTextColor(arg):
        '''
        UpdateCategoryTextColor : Update category text color
                Input argu :
                    N/A
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Update category text color
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "update_category_text_color", "result": "1"})
        DLAdminAPIMethod.DLAdminAPI.StoreDLAdminCookie({"result": "1"})
        GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayMallPageAPI.UpdateCategoryTextColor')


class GatewayHomePageAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckNewUserZoneModule(arg):
        '''
        CheckNewUserZoneModule : Check new user zone module in FE
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})

        ##Get and set Http Api payload
        HttpAPICore.GetAndSetHttpAPIData({"collection":"new_user_zone_get_modules", "result": "1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Deinitial api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayHomePageAPI.CheckNewUserZoneModule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCategoryModule(arg):
        '''
        CheckCategoryModule : Check category module in FE
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})

        ##Get and set Http Api payload
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_pc_category", "result": "1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})

        ##Check response code and text
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Deinitial api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayHomePageAPI.CheckCategoryModule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckMallShops(arg):
        '''
        CheckMallShops : Check mall shops module in FE
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})

        ##Get and set Http Api payload
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_mall_shops", "result": "1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})

        ##Check response code and text
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Deinitial api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayHomePageAPI.CheckMallShops')


class GatewayMicrositeAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetCampaignSitePage(arg):
        '''
        GetCampaignSitePage : Get microsite/campaign_site_page api from microsite
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})

        ##Set up API request
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_campaign_site_page_in_pc", "result": "1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayMicrositeAPI.GetCampaignSitePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateCampaignSitePage(arg):
        '''
        CreateCampaignSitePage : Create campaign site page using api
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result": "1"})

        ##Set up API request
        HttpAPICore.GetAndSetHttpAPIData({"collection":"microsite_create_page", "result": "1"})
        DLAdminAPIMethod.DLAdminAPI.StoreDLAdminCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send API request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})
        APICommonMethod.CheckAPIResponseCode({"result": "1"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayMicrositeAPI.CreateCampaignSitePage')


class GatewayProductCardAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetSearchItems(arg):
        '''
        GetSearchItems : Get seach items
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get json data
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_items", "result": "1"})

        ##Send API to get response
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})

        ##Compare response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Compare response text
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayProductCardAPI.GetSearchItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetProductCollectionItems(arg):
        '''
        GetProductCollectionItems : Get product collection items
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get json data
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_product_collection_items", "result": "1"})

        ##Send API to get response
        HttpAPICore.SendHttpRequest({"http_method": "get", "result": "1"})

        ##Compare response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Compare response text
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayProductCardAPI.GetProductCollectionItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetItemList(arg):
        '''
        GetItemList : Get item list
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Initial Http API
        HttpAPICore.InitialHttpAPI({"session": "unused", "json_data": "", "result": "1"})

        ##Get json data
        HttpAPICore.GetAndSetHttpAPIData({"collection": "get_item_list", "result": "1"})

        ##Set API payload
        DLAdminAPIMethod.DLAdminAPI.StoreDLAdminCookie({"result": "1"})
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send API to get response
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        ##Compare response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        ##Compare response text
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        OK(ret, int(arg['result']), 'GatewayProductCardAPI.GetItemList')
