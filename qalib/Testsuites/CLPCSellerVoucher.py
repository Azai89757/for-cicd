import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCSellerVoucher Precondition =="
        dumplogger.info("== Setup CLPCSellerVoucher Precondition ==")

    def test_CLPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCSellerVoucher-01.xml")

    def test_CLPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCSellerVoucher-02.xml")

    # def test_CLPCSellerVoucher03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CL/CLPCSellerVoucher-03.xml")

    # def test_CLPCSellerVoucher04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CL/CLPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCSellerVoucher Post condition =="
        dumplogger.info("== Setup CLPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
