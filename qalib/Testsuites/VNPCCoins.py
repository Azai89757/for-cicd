import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class VNPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCoins Precondition =="
        dumplogger.info("== Setup VNPCCoins Precondition ==")

    def test_VNPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-01.xml")

    def test_VNPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-02.xml")

    def test_VNPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-03.xml")

    def test_VNPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-04.xml")

    def test_VNPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-05.xml")

    def test_VNPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-06.xml")

    def test_VNPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-07.xml")

    def test_VNPCCoins08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-08.xml")

    def test_VNPCCoins09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-09.xml")

    def test_VNPCCoins10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-10.xml")

    def test_VNPCCoins11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCoins-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCoins Post condition =="
        dumplogger.info("== Setup VNPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
