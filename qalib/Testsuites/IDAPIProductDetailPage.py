import unittest
from Config import dumplogger
from FrameWorkBase import *
import GlobalAdapter


class IDAPIProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIProductDetailPage Precondition =="
        dumplogger.info("== Setup IDAPIProductDetailPage Precondition ==")

    def test_IDAPIProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIProductDetailPage-01.xml")

    def tearDown(self):
        print GlobalAdapter.CommonVar._TestLinkID_
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIProductDetailPage Post condition =="
        dumplogger.info("== Setup IDAPIProductDetailPage Post condition ==")
