import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCLogOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCLogOut Precondition =="
        dumplogger.info("== Setup COPCLogOut Precondition ==")

    def test_COPCLogOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCLogOut-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCLogOut Post condition =="
        dumplogger.info("== Setup CONPCLogOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
