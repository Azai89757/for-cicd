import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCProductCard Precondition =="
        dumplogger.info("== Setup ESPCProductCard Precondition ==")

    def test_ESPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCProductCard-01.xml")

    def test_ESPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCProductCard-02.xml")

    def test_ESPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCProductCard-03.xml")

    def test_ESPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCProductCard-04.xml")

    def test_ESPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCProductCard-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCProductCard Post condition =="
        dumplogger.info("== Setup ESPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
