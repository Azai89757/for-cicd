import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCReturnRefund Precondition =="
        dumplogger.info("== Setup MYPCReturnRefund Precondition ==")

    def test_MYPCReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCReturnRefund-01.xml")

    def test_MYPCReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCReturnRefund-02.xml")

    def test_MYPCReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCReturnRefund-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCReturnRefund Post condition =="
        dumplogger.info("== Setup MYPCReturnRefund Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
