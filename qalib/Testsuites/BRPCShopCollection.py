import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCShopCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCShopCollection Precondition =="
        dumplogger.info("== Setup BRPCShopCollection Precondition ==")

    def test_BRPCShopCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCShopCollection-01.xml")

    def test_BRPCShopCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCShopCollection-02.xml")

    def test_BRPCShopCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCShopCollection-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCShopCollection Post condition =="
        dumplogger.info("== Setup BRPCShopCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
