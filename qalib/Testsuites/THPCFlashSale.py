import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCFlashSale Precondition =="
        dumplogger.info("== Setup THPCFlashSale Precondition ==")

    def test_THPCFlashSale001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFlashSale-001.xml")

    def test_THPCFlashSale101(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCFlashSale-101.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCFlashSale Post condition =="
        dumplogger.info("== Setup THPCFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
