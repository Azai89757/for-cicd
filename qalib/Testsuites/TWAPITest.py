import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWAPITest(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPITest Precondition =="
        dumplogger.info("== Setup TWAPITest Precondition ==")

    def test_TWAPITest001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWAPITest-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWAPITest Post condition =="
        dumplogger.info("== Setup TWAPITest Post condition ==")
