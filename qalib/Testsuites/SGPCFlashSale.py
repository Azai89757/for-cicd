import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCFlashSale Precondition =="
        dumplogger.info("== Setup SGPCFlashSale Precondition ==")

    def test_SGPCFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCFlashSale-01.xml")

    def test_SGPCFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCFlashSale-02.xml")

    def test_SGPCFlashSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCFlashSale-03.xml")

    def test_SGPCFlashSale04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCFlashSale-04.xml")

    def test_SGPCFlashSale05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCFlashSale-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCFlashSale Post condition =="
        dumplogger.info("== Setup SGPCFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
