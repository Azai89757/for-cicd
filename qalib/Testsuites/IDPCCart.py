import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCart Precondition =="
        dumplogger.info("== Setup IDPCCart Precondition ==")

    def test_IDPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-01.xml")

    def test_IDPCCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-02.xml")

    def test_IDPCCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-03.xml")

    def test_IDPCCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-04.xml")

    def test_IDPCCart05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-05.xml")

    def test_IDPCCart06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-06.xml")

    def test_IDPCCart07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCCart-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCCart Post condition =="
        dumplogger.info("== Setup IDPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
