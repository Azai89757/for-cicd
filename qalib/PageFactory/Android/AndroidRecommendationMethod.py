#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidRecommendationMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic

##Import selenium related library
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def HorizontalMoveAndCheckElement(arg):
    '''
    HorizontalMoveAndCheckElement : Scroll Horizontal section until find target product card
            Input argu :
                direction - move action ex: left / right
                scroll_section - horizontal scroll location ex: horiz_scroll_in_from_same / horiz_scroll_in_similar / horiz_scroll_in_top_product
                target_product_name - target product card
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    direction = arg["direction"]
    scroll_section = arg["scroll_section"]
    target_product_name = arg["target_product_name"]
    ret = 0

    try:
        ##Get initial element location for reference
        scroll_xpath = Util.GetXpath({"locate":scroll_section})
        AndroidBaseUICore.AndroidGetElements({"locate":scroll_xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})

        ##Get Horizontal element location x and y value on screen
        locate_x = int(GlobalAdapter.CommonVar._PageAttributes_['x'])
        locate_y = int(GlobalAdapter.CommonVar._PageAttributes_['y'])

        ##Get Horizontal element size x and y value on screen
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"size", "mode":"single", "result": "1"})
        locate_x = locate_x + int(GlobalAdapter.CommonVar._PageAttributes_['width']) / 2
        locate_y = locate_y + int(GlobalAdapter.CommonVar._PageAttributes_['height']) / 2
        end_x = int(locate_x * 0.2)

    except KeyError:
        dumplogger.info("Element doesn't exist at the current screen, please check your Horizontal element")

    ##Move and check Horizontal view to find target element
    for times in range(15):
        try:
            ##WebDriverWait to find target element
            time.sleep(2)
            xpath = Util.GetXpath({"locate":target_product_name})
            WebDriverWait(AndroidBaseUICore._AndroidDriver_, 5).until(EC.presence_of_element_located((By.XPATH, (xpath))))
            ret = 1
            break

        except TimeoutException:
            ##if target element doesn't exist at the current screen..start to swipe
            dumplogger.info("Element doesn't exist at the current screen..start to swipe")
            if direction == "right":
                AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":locate_x, "locate_y":locate_y, "movex":end_x, "movey":locate_y, "result": "1"})
            elif direction == "left":
                AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":end_x, "locate_y":locate_y, "movex":locate_x, "movey":locate_y, "result": "1"})

    OK(ret, int(arg['result']), 'HorizontalMoveAndCheckElement')

@DecoratorHelper.FuncRecorder
def HorizontalMoveElementToPosition(arg):
    '''
    HorizontalMoveElementToPosition : Move target product card to specific width of screen in Horizontal section
            Input argu :
                scroll_section - horizontal scroll location horiz_scroll_in_from_same/horiz_scroll_in_similar/horiz_scroll_in_top_product
                target_product_name - target product card
                ratio - move element to specific width ratio of the screen (0 ~ 1)
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    scroll_section = arg["scroll_section"]
    target_product_name = arg["target_product_name"]
    ratio = arg['ratio']

    try:
        ##target element's coordinates
        xpath = Util.GetXpath({"locate":target_product_name})
        AndroidBaseUICore.AndroidGetElements({"locate":xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})
        x_cor = int(GlobalAdapter.CommonVar._PageAttributes_['x'])

        ##Get window location for reference
        window_size = AndroidBaseUICore._AndroidDriver_.get_window_size()
        locate_x1 = int(window_size['width'])

        ##Get target x location value of screen percentage
        end_x = int(locate_x1 * float(ratio))

        ##Get Horizontal element location for reference
        scroll_section_xpath = Util.GetXpath({"locate":scroll_section})
        AndroidBaseUICore.AndroidGetElements({"locate":scroll_section_xpath, "mode":"single", "result": "1"})
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})

        ##Get Horizontal element's current location y value on screen
        locate_y = int(GlobalAdapter.CommonVar._PageAttributes_['y'])

        ##Get Horizontal element's size y value on screen
        AndroidBaseUICore.AndroidGetAttributes({"attrtype":"size", "mode":"single", "result": "1"})
        locate_y = locate_y + int(GlobalAdapter.CommonVar._PageAttributes_['height']) - 15

    except KeyError:
        dumplogger.info("Element doesn't exist at the current screen, please check your Horizontal element")
        ret = 0

    ##Move element to specific ratio of the screen
    AndroidBaseUICore.AndroidRelativeSwipe({"locate_x":x_cor, "locate_y":locate_y, "movex":end_x, "movey":locate_y, "result": "1"})

    OK(ret, int(arg['result']), 'HorizontalMoveElementToPosition -->' + target_product_name)


class AndroidRecommendationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTrendingSearchSlotItem(arg):
        '''
        CheckTrendingSearchSlotItem : Check recall amount/keyword in trending search slot.
                Input argu :
                    slot_item - the recall amount/keyword display in trending search slot
                                ex: more_than_kilo / more_than_10kilo / more_than_million /overlay / rcmd
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        slot_item = arg["slot_item"]

        ##Confirm slot item display normally
        for retry_count in range(1, 5):
            dumplogger.info("Start round: %s for checking slot item exist" % (retry_count))

            ##check slot item dispaly
            xpath = Util.GetXpath({"locate":slot_item})

            ##Whether refresh slot item is exist or not, break the loop as long as the result is expected
            if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Key word exist!!!!")
                ret = 1
                break
            else:
                dumplogger.info("Slot item not exist at %d run..." % (retry_count))
                xpath = Util.GetXpath({"locate":"pts_refresh_button"})
                AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click refresh button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationPage.CheckTrendingSearchSlotItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductDetailPage(arg):
        '''
        GoToProductDetailPage : Click product card and redirect to product detail page
                Input argu :
                    pdc_name - which product card name need to be click.
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        pdc_name = arg["pdc_name"]

        ##Click key word
        xpath = Util.GetXpath({"locate":pdc_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click key word", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationPage.GoToProductDetailPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCardInRCMD(arg):
        '''
        ClickProductCardInRCMD : Click product card in rcmd section
                Input argu :
                    section_name - rcmd section, ex:top_product/from_same_shop/you_may_also_like
                    product_card - click product card, which display in rcmd section
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section_name = arg["section_name"]
        product_card = arg["product_card"]

        ##Click product card in rcmd section
        xpath = Util.GetXpath({"locate":"product_card"})
        xpath = xpath.replace("section_name_to_replace", section_name)
        xpath = xpath.replace("product_card_name_to_replace", product_card)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click rcmd see more button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationPage.ClickProductCardInRCMD')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAndCheckPtsImgChanged(arg):
        '''
        ClickAndCheckPtsImgChanged : click seemore button exist and check pts img have been change
                Input argu :
                    item_type - product item/amount
                    image - OpenCV image to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        item_type = arg["item_type"]
        image = arg["image"]

        time.sleep(15)

        ##Check item 7 times to confirm pts img display normally
        for retry_count in range(1, 8):
            dumplogger.info("Start round: %s for checking product item exist" % (retry_count))

            ##Click pts refresh button
            xpath = Util.GetXpath({"locate":"pts_refresh_button"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click pts refresh button", "result": "1"})

            time.sleep(15)
            ##Check img is exist
            match_result = XtFunc.MatchPictureImg({"mode":"android", "project": Config._TestCaseFeature_, "image": image, "env": Config._EnvType_, "threshold": "0.9", "expected_result": arg['result']})

            ##Whether img is exist, break the loop as long as the result is expected
            if match_result == ret:
                dumplogger.info("product image matching successful!!!!")
                ret = 1
                break
            else:
                dumplogger.info("product image matching failed at %d run..." % (retry_count))

        OK(ret, int(arg['result']), 'AndroidRecommendationPage.ClickAndCheckPtsImgChanged -> ' + item_type)


class AndroidRecommendationButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more button to review more product card
                Input argu :
                    button_type - recommendation section name
                    times - click times
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        times = arg['times']

        for click_time in range(int(times)):
            ##Click see more button
            xpath = Util.GetXpath({"locate":button_type})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click seemore button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickSeeMore -> ' + button_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreInPDP(arg):
        '''
        ClickSeeMoreInPDP : Click see more button to review more product card in product detail page
                Input argu :
                    section_name - Section button in product detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section_name = arg["section_name"]

        ##Click see more button of section in product deatil page
        xpath = Util.GetXpath({"locate":"see_more_button"})
        xpath = xpath.replace("section_name_to_replace", section_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click " + section_name + " see more button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickSeeMoreInPDP')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToTop(arg):
        '''
        ClickBackToTop : Click back to top button in landing page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to top button in landing page
        xpath = Util.GetXpath({"locate":"back_top_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click back to top button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickBackToTop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownWards(arg):
        '''
        ClickDownWards : Click downwards arrow button in Top Product page
                Input argu :
                    action - choose open or close category. ex:open/close
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Click downwards arrow button in Top Product page
        xpath = Util.GetXpath({"locate": action})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click downwards arrow button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickDownWards')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTopUp(arg):
        '''
        ClickTopUp : Click top up button in recommendation page
                Input argu :
                    section_name - top up in which recommendation page, ex: dd_feature_collection / dd_top_product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        section_name = arg["section_name"]
        ret = 1

        ##Click top up button
        xpath = Util.GetXpath({"locate":section_name})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click top up button in recommendation page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickTopUp')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add product to cart in recommendation module
                Input argu :
                    product_name - target product in recommendation module
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        product_name = arg["product_name"]
        ret = 1

        ##Click target product in recommendation module
        xpath = Util.GetXpath({"locate":"target_icon"})
        xpath = xpath.replace("product_name_to_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click product in recommendation page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductModel(arg):
        '''
        ChooseProductModel : Click to choose product mode on recommendation module
                Input argu :
                    name - product model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Choose product model
        xpath = Util.GetXpath({"locate":"product_mode"})
        xpath = xpath.replace("product_name_for_replace", name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose product mode on recommendation module", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ChooseProductModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseVariation(arg):
        '''
        ClickCloseVariation : Click close variation
                Input argu : N/A
                Retutn code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1

        ##Click close variation
        xpath = Util.GetXpath({"locate": "close_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click close variation", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidRecommendationButton.ClickCloseVariation')


class AndroidTopProductButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreCategory(arg):
        '''
        ClickSeeMoreCategory : Click see more category to review more category in top product
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see more button
        xpath = Util.GetXpath({"locate":"top_right_see_more"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click top right see more button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTopProductButton.ClickSeeMoreCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTargetCategory(arg):
        '''
        ClickTargetCategory : Click target category in top product list page.
                Input argu :
                    category_name - target category which top product list page, ex:dd_feature_collection/dd_top_product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        category_name = arg["target_category"]
        ret = 1

        ##Click target category in top product
        xpath = Util.GetXpath({"locate":"target_category"})
        xpath = xpath.replace("category_name_to_replace", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click target category in top product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTopProductButton.ClickTargetCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTargetTab(arg):
        '''
        ClickTargetTab : Click target tab in top product
                Input argu :
                    target_tab - target category which top product page, ex:best_selling/best_price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        target_tab = arg["target_tab"]
        ret = 1

        ##Click target tab in top product
        xpath = Util.GetXpath({"locate":target_tab})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click target tab in top product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTopProductButton.ClickTargetTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryTab(arg):
        '''
        ClickCategoryTab : Click category tab in top product
                Input argu :
                    category_name - target category which display in top product page.
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        category_name = arg["category_name"]
        ret = 1

        ##Click category tab in top product
        xpath = Util.GetXpath({"locate":"category_tab"})
        xpath = xpath.replace("category_name_to_replace", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click target tab in top product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTopProductButton.ClickCategoryTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCluster(arg):
        '''
        ClickCluster : Click cluster in top product
                Input argu :
                    cluster_name - target cluster which display in top product page.
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        cluster_name = arg["cluster_name"]
        ret = 1

        ##Click cluster tab in top product
        xpath = Util.GetXpath({"locate":"cluster_tab"})
        xpath = xpath.replace("cluster_name_to_replace", cluster_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cluster in top product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTopProductButton.ClickCluster')


class AndroidCollectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToTop(arg):
        '''
        ClickBackToTop : Click back to top button in collection landing page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to top button in collection landing page
        xpath = Util.GetXpath({"locate":"back_top_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click back to top button in collection landing page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCollectionButton.ClickBackToTop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollection(arg):
        '''
        ClickCollection : Click add collection in collection landing page
                Input argu :
                    collection_name - target collection in collection landing page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        collection_name = arg["collection_name"]
        ret = 1

        ##Click target collection in collection landing page
        xpath = Util.GetXpath({"locate":"target_collection"})
        xpath = xpath.replace("collection_name_to_replace", collection_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click target collection in collection landing page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCollectionButton.ClickCollection')


class AndroidPDPModuleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more card to review more product card in product detail page module
                Input argu :
                    section_name - Section button in product detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section_name = arg["section_name"]

        ##Click see more card to review more product card in product detail page module
        xpath = Util.GetXpath({"locate": "see_more_button"})
        xpath = xpath.replace("section_name_to_replace", section_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click see more card to review more product card in product detail page module", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPDPModuleButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTopProduct(arg):
        '''
        ClickTopProduct : Click top product in product detail page module
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click top product in product detail page module in product detail page module
        xpath = Util.GetXpath({"locate": "top_product"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click top product in product detail page module", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPDPModuleButton.ClickTopProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickModule(arg):
        '''
        ClickModule : Click target module which display in PDP rcmd
                Input argu :
                    module_name - module name which display in PDP rcmd
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        module_name = arg["module_name"]

        ##Click target module which display in PDP rcmd
        xpath = Util.GetXpath({"locate": "module_location"})
        xpath = xpath.replace("module_name_to_replace", module_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click target module which display in PDP rcmd", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidPDPModuleButton.ClickModule')


class AndroidDailyDiscoverButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click target tab which display in daily discover
                Input argu :
                    tab_name - tab name which display in daily discover header
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg["tab_name"]

        ##Click target tab which display in daily discover
        xpath = Util.GetXpath({"locate": "daily_discover_tab"})
        xpath = xpath.replace("tab_name_to_replace", tab_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click target tab which display in daily discover", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidDailyDiscoverButton.ClickTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickModule(arg):
        '''
        ClickModule : Click target module which display in daily discover
                Input argu :
                    module_name - module name which display in daily discover
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        module_name = arg["module_name"]

        ##Click target module which display in daily discover
        xpath = Util.GetXpath({"locate": "module_location"})
        xpath = xpath.replace("module_name_to_replace", module_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click target module which display in daily discover", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidDailyDiscoverButton.ClickModule')


class AndroidTrendingSearchButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTrendingKeyword(arg):
        '''
        ClickTrendingKeyword : click keyword on trending search to redirect to product landing page
                Input argu :
                    key_word - overlay/rcmd
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        key_word = arg["key_word"]

        ##Click key word
        xpath = Util.GetXpath({"locate":key_word})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click key word", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidTrendingSearchButton.ClickTrendingKeyword -> ' + key_word)
