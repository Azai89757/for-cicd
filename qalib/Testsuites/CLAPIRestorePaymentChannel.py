import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLAPIRestorePaymentChannel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIRestorePaymentChannel Precondition =="
        dumplogger.info("== Setup CLAPIRestorePaymentChannel Precondition ==")

    def test_CLAPIRestorePaymentChannel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/CLAPIRestorePaymentChannel-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIRestorePaymentChannel Post condition =="
        dumplogger.info("== Setup CLAPIRestorePaymentChannel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
