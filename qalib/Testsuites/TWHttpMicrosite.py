import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWHttpMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpMicrosite Precondition =="
        dumplogger.info("== Setup TWHttpMicrosite Precondition ==")

    def test_TWHttpMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpMicrosite-01.xml")

    def test_TWHttpMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpMicrosite-02.xml")

    def test_TWHttpMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpMicrosite-03.xml")

    def test_TWHttpMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpMicrosite-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpMicrosite Post condition =="
        dumplogger.info("== Setup TWHttpMicrosite Post condition ==")
