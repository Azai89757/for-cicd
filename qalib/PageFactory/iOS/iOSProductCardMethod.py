﻿
#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSProductCardMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import iOSBaseUICore
import iOSBaseUILogic
import DecoratorHelper

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class iOSMainPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProduct(arg):
        ''' SearchProduct : Search product from home page
                Input argu :
                            name - product name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click search bar
        iOSBaseUICore.iOSClick({"method":"id", "locate": Util.GetXpath({"locate": "global_search_bar"}), "message":"Click search bar","result": "1"})
        time.sleep(3)

        ##Input keyword and click enter
        iOSBaseUICore.iOSInput({"method":"xpath", "locate": Util.GetXpath({"locate": "typing_search_words"}), "string":name, "result": "1"})
        iOSBaseUICore.iOSClick({"method":"id", "locate": Util.GetXpath({"locate": "keyboard_search"}), "message":"Click key board search","result": "1"})

        time.sleep(3)
        OK(ret, int(arg['result']), 'iOSMainPage.SearchProduct')
