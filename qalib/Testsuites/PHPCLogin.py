import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCLogin Precondition =="
        dumplogger.info("== Setup PHPCLogin Precondition ==")

    def test_PHPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCLogin-01.xml")

    def test_PHPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCLogin Post condition =="
        dumplogger.info("== Setup PHPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
