import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCAccountManagement(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCAccountManagement Precondition =="
        dumplogger.info("== Setup IDPCAccountManagement Precondition ==")

    def test_IDPCAccountManagement01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCAccountManagement-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCAccountManagement Post condition =="
        dumplogger.info("== Setup IDPCAccountManagement Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
