import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpBundleDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpBundleDeal Precondition =="
        dumplogger.info("== Setup TWHttpBundleDeal Precondition ==")

    def test_TWHttpBundleDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-01.xml")

    def test_TWHttpBundleDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-02.xml")

    def test_TWHttpBundleDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-03.xml")

    def test_TWHttpBundleDeal04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-04.xml")

    def test_TWHttpBundleDeal05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-05.xml")

    def test_TWHttpBundleDeal06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-06.xml")

    def test_TWHttpBundleDeal07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-07.xml")

    def test_TWHttpBundleDeal08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-08.xml")

    def test_TWHttpBundleDeal09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpBundleDeal-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpBundleDeal Post condition =="
        dumplogger.info("== Setup TWHttpBundleDeal Post condition ==")
