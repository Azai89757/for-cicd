import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCMarketPlacePayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCMarketPlacePayment Precondition =="
        dumplogger.info("== Setup SGPCMarketPlacePayment Precondition ==")

    def test_SGPCMarketPlacePayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCMarketPlacePayment-01.xml")

    def test_SGPCMarketPlacePayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCMarketPlacePayment-02.xml")

    def test_SGPCMarketPlacePayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCMarketPlacePayment-03.xml")

    def test_SGPCMarketPlacePayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCMarketPlacePayment-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCMarketPlacePayment Post condition =="
        dumplogger.info("== Setup SGPCMarketPlacePayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
