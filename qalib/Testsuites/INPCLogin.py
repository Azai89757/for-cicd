import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCLogin Precondition =="
        dumplogger.info("== Setup INPCLogin Precondition ==")

    def test_INPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCLogin-01.xml")

    def test_INPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCLogin Post condition =="
        dumplogger.info("== Setup INPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
