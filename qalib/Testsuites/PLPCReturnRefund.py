import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCReturnRefund Precondition =="
        dumplogger.info("== Setup PLPCReturnRefund Precondition ==")

    def test_PLPCReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCReturnRefund-01.xml")

    def test_PLPCReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCReturnRefund-02.xml")

    def test_PLPCReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCReturnRefund-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCReturnRefund Post condition =="
        dumplogger.info("== Setup PLPCReturnRefund Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
