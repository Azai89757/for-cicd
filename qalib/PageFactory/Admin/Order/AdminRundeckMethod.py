#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminRundeckMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time
import re

##Import common library
import Config
import Util
import FrameWorkBase
import DecoratorHelper
import GlobalAdapter
import XtFunc
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Web import CommonMethod

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def LaunchRundeckAdmin(arg):
    ''' LaunchRundeckAdmin : go to rundeck admin page
            Input argu :
                project_name - project name in rundeck
                job_id - job id of cronjob
                show_id - show id of cronjob
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    ret = 1
    env = Config._EnvType_.lower()
    rundeck_cookie = {}
    project_name = arg["project_name"]
    job_id = arg["job_id"]
    show_id = arg["show_id"]

    if 'rundeck' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":"rundeck_cookie", "method":"select", "verify_result":"", "assign_data_list":"", "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        rundeck_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        rundeck_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        rundeck_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['rundeck'] = [rundeck_cookie]

    ##Exact jod id provided, Go to Rundeck page
    time.sleep(3)
    if job_id:
        url = "http://rundeck." + env + ".shopeemobile.com/project/" + project_name + "/job/show/" + job_id

    elif show_id:
        url = "http://rundeck." + env + ".shopeemobile.com/project/" + project_name + "/jobs/" + show_id

    ##Get execution job url from regex result
    else:
        regex_str = "((www|http:|https:)+[^\s]+[\w])"
        exec_url = re.search(regex_str, GlobalAdapter.CommonVar._DynamicCaseData_["success"])
        if exec_url:
            ##encode url from unicode to string
            url = exec_url.group().encode('utf-8')
        else:
            ##No result found after regex
            dumplogger.info("No result match with regex_str, please check you root data")
            url = None
            ret = 0

    ##Go to rundeck url only when url is valid
    if url:
        BaseUICore.GotoURL({"url":url, "result": "1"})
        time.sleep(5)

        ##Set cookies to login
        BaseUICore.SetBrowserCookie({"storage_type":"rundeck", "result": "1"})
        time.sleep(5)

        ##Go to rundeck admin
        BaseUICore.GotoURL({"url":url, "result": "1"})
        BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchRundeckAdmin')

class AdminRundeckRunCronJob:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RunRecalculateCronJob(arg):
        '''RunRecalculateCronJob : Run recalculate cronjob to recalculate order ship by date value
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Go to cronjob page
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"country_runjob"})
        xpath = xpath.replace("region_to_be_replaced", Config._TestCaseRegion_.lower())
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to cronjob page", "result": "1"})
        BaseUICore.PageHasLoaded({"result": "1"})

        ##Input data_fix
        xpath = Util.GetXpath({"locate":"input_data_fix"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "-data_fix true", "result": "1"})

        BaseUILogic.MouseScrollEvent({"x": "100", "y": "700", "result": "1"})
        time.sleep(5)

        ##Input order ID
        xpath = Util.GetXpath({"locate":"input_order_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "-order_ids " + GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

        ##Click run job now button
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"run_job_now_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})

        ##Check if run cronjob successfully
        for retry_times in range(0, 10):
            BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
            BaseUICore.PageHasLoaded({"result": "1"})
            time.sleep(45)
            ##Check if succeeded text exists
            xpath = Util.GetXpath({"locate": "succeeded_text"})

            ##Break the loop directly if succeed
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                break
            else:
                ##Check if failed text exists
                xpath = Util.GetXpath({"locate": "failed_text"})

                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    ##If failed, click run again button
                    xpath = Util.GetXpath({"locate":"run_again_dropdown"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again dropdown", "result": "1"})
                    xpath = Util.GetXpath({"locate":"run_again_btn"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again button", "result": "1"})

                    ##Input order id
                    xpath = Util.GetXpath({"locate":"input_order_id"})
                    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

                    ##Click run job now button
                    time.sleep(2)
                    xpath = Util.GetXpath({"locate":"run_job_now_btn"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})
                else:
                    ##Check if any execution is tunning
                    xpath = Util.GetXpath({"locate": "alert_text"})
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                        ##Go to the execution
                        xpath = Util.GetXpath({"locate":"execution_number"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click execution number", "result": "1"})

                        ##Click kill job button
                        xpath = Util.GetXpath({"locate":"kill_job_btn"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click kill job button", "result": "1"})
                        time.sleep(10)

                        ##In test env, run again button in a drop down list
                        if Config._EnvType_ == "test":
                            xpath = Util.GetXpath({"locate":"run_again_dropdown"})
                            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again dropdown", "result": "1"})

                        ##Click run again button
                        xpath = Util.GetXpath({"locate":"run_again_btn"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again button", "result": "1"})

                        ##Input order id
                        xpath = Util.GetXpath({"locate":"input_order_id"})
                        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

                        ##Click run job now button
                        time.sleep(2)
                        xpath = Util.GetXpath({"locate":"run_job_now_btn"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRundeckRunCronJob.RunRecalculateCronJob')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RunCODCronJob(arg):
        ''' RunCODCronJob : Run COD cronjob to push cod status
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Input order ID
        xpath = Util.GetXpath({"locate":"input_order_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

        ##Input region
        xpath = Util.GetXpath({"locate":"region_dropdown_list"})
        BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": Config._TestCaseRegion_.lower(), "result": "1"})

        ##Click run job now button
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"run_job_now_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})

        ##Check if run cronjob successfully
        for retry_times in range(0, 10):
            BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
            BaseUICore.PageHasLoaded({"result": "1"})
            time.sleep(45)
            ##Check if succeeded text exists
            xpath = Util.GetXpath({"locate": "succeeded_text"})

            ##Break the loop directly if succeed
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                break
            else:
                ##Check if failed text exists
                xpath = Util.GetXpath({"locate": "failed_text"})

                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    ##If failed, click run again button
                    xpath = Util.GetXpath({"locate":"run_again_dropdown"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again dropdown", "result": "1"})
                    xpath = Util.GetXpath({"locate":"run_again_btn"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again button", "result": "1"})

                    ##Input order id
                    xpath = Util.GetXpath({"locate":"input_order_id"})
                    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

                    ##Click run job now button
                    time.sleep(2)
                    xpath = Util.GetXpath({"locate":"run_job_now_btn"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})
                else:
                    ##Check if any execution is tunning
                    xpath = Util.GetXpath({"locate": "alert_text"})
                    if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                        ##Go to the execution
                        xpath = Util.GetXpath({"locate":"execution_number"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click execution number", "result": "1"})

                        ##Click kill job button
                        xpath = Util.GetXpath({"locate":"kill_job_btn"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click kill job button", "result": "1"})
                        time.sleep(10)

                        ##In test env, run again button in a drop down list
                        if Config._EnvType_ == "test":
                            xpath = Util.GetXpath({"locate":"run_again_dropdown"})
                            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again dropdown", "result": "1"})

                        ##Click run again button
                        xpath = Util.GetXpath({"locate":"run_again_btn"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again button", "result": "1"})

                        ##Input order id
                        xpath = Util.GetXpath({"locate":"input_order_id"})
                        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._OrderID_, "result": "1"})

                        ##Click run job now button
                        time.sleep(2)
                        xpath = Util.GetXpath({"locate":"run_job_now_btn"})
                        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRundeckRunCronJob.RunCODCronJob')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RunLostParcelCronJob(arg):
        ''' RunLostParcelCronJob : Run lost parcel cronjob to cancel LOGISTIC_LOST order
                Input argu : seconds_to_search - How many seconds ago that order changed to LOGISTIC_LOST we want to search
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        seconds_to_search = arg["seconds_to_search"]

        ##Input seconds to search (How many seconds ago that order changed to LOGISTIC_LOST we want to search)
        xpath = Util.GetXpath({"locate":"seconds_to_search_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": seconds_to_search, "result": "1"})

        ##Run cron job
        AdminRundeckRunCronJob.RunCronJob({"result": "1"})

        OK(ret, int(arg['result']), 'AdminRundeckRunCronJob.RunLostParcelCronJob')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RunCronJob(arg):
        ''' RunCronJob : Run cronjob
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click run job now button
        xpath = Util.GetXpath({"locate":"run_job_now_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})

        ##Check if run cronjob successfully
        for retry_times in range(0, 10):
            BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
            BaseUICore.PageHasLoaded({"result": "1"})
            time.sleep(45)
            ##Check if succeeded text exists
            xpath = Util.GetXpath({"locate": "succeeded_text"})

            ##Break the loop directly if succeed
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                break
            else:
                ##Check if failed text exists
                xpath = Util.GetXpath({"locate": "failed_text"})

                if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                    ##If failed, click run again button
                    xpath = Util.GetXpath({"locate":"run_again_dropdown"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again dropdown", "result": "1"})
                    xpath = Util.GetXpath({"locate":"run_again_btn"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run again button", "result": "1"})

                    ##Click run job now button
                    time.sleep(2)
                    xpath = Util.GetXpath({"locate":"run_job_now_btn"})
                    BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click run job now button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminRundeckRunCronJob.RunCronJob')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckJobExecutionProgress(arg):
        ''' CheckJobExecutionProgress : verify rundeck job execution process triggered by API
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                          -1 - error
        '''
        ret = 1
        ##Check if run cronjob successfully refresh page to check succeeded text
        ##Loop ten times will wait max 600 sec
        for retry_times in range(0, 10):
            BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
            BaseUICore.PageHasLoaded({"result": "1"})
            time.sleep(60)
            xpath_for_success_text = Util.GetXpath({"locate": "succeeded_text"})
            xpath_for_failed_text = Util.GetXpath({"locate": "failed_text"})

            ##Break the loop directly if succeed
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_for_success_text, "passok": "0", "result": "1"}):
                dumplogger.info("Job Succeeded")
                break

            ##If failed, break the loop as well because rundeck job failed ret = 0
            elif BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_for_failed_text, "passok": "0", "result": "1"}):
                dumplogger.info("Job Failed, Please Check output on Rundeck Admin")
                ret = 0
                break

        dumplogger.info("Job finished, Total loop time count = %s", retry_times)
        OK(ret, int(arg['result']), 'AdminRundeckRunCronJob.CheckJobExecutionProgress')
