#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidGroupBuyMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import AndroidBaseUICore
import AndroidBaseUILogic
import DecoratorHelper

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidGroupBuyButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOpenGroupBuy(arg):
        ''' ClickOpenGroupBuy : Click open group buy on pdp page
                    Input argu : N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click open group buy button
        xpath = Util.GetXpath({"locate":"open_group_buy_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click open_group_buy_btn button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickOpenGroupBuy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPositive(arg):
        ''' ClickPositive : Click positive button on popup
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click positive button
        locate = Util.GetXpath({"locate":"positive_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click positive button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickPositive')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickComfirmBuy(arg):
        ''' ClickComfirmBuy : Click Comfirm Buy button on variation scroll up
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Comfirm Buy button
        locate = Util.GetXpath({"locate":"comfirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click Comfirm Buy button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickComfirmBuy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareWithFriends(arg):
        ''' ClickShareWithFriends : Click ShareWithFriends button on group buy done page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Comfirm Buy button
        locate = Util.GetXpath({"locate":"invite_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click ShareWithFriends button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickShareWithFriends')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContactAccessButton(arg):
        ''' ClickContactAccessButton : Click button on contact block
                Input argu : btn_type - allow_contact_access / how_shopee_protect_my_data
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        btn_type = arg["btn_type"]

        ##Click next button
        xpath = Util.GetXpath({"locate": btn_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button on contact block", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickContactAccessButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllowContact(arg):
        ''' ClickAllowContact : Click allow contact on popup
                Input argu : btn_type = allow / disallow
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        btn_type = arg["btn_type"]

        ##Click next button
        xpath = Util.GetXpath({"locate": btn_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button on popup", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickAllowContact')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInviteContact(arg):
        ''' ClickInviteContact : Click invite contact
                Input argu : NA
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click next button
        xpath = Util.GetXpath({"locate": "invite_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click invite button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidGroupBuyButton.ClickInviteContact')
