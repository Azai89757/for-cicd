import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPayment Precondition =="
        dumplogger.info("== Setup IDPCPayment Precondition ==")

    def test_IDPCPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-01.xml")

    def test_IDPCPayment02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-02.xml")

    def test_IDPCPayment03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-03.xml")

    def test_IDPCPayment04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-04.xml")

    def test_IDPCPayment05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-05.xml")

    def test_IDPCPayment06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-06.xml")

    def test_IDPCPayment07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-07.xml")

    def test_IDPCPayment08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCPayment-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCPayment Post condition =="
        dumplogger.info("== Setup IDPCPayment Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
