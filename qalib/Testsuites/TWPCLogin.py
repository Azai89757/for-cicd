import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCLogin Precondition =="
        dumplogger.info("== Setup TWPCLogin Precondition ==")

    def test_TWPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCLogin-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCLogin Post condition =="
        dumplogger.info("== Setup TWPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
