import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCLogisticPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCLogisticPromotion Precondition =="
        dumplogger.info("== Setup VNPCLogisticPromotion Precondition ==")

    # def test_VNPCLogisticPromotion01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-01.xml")
    #
    def test_VNPCLogisticPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCLogisticPromotion-02.xml")

    # def test_VNPCLogisticPromotion03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-03.xml")
    #
    # def test_VNPCLogisticPromotion04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-04.xml")
    #
    # def test_VNPCLogisticPromotion05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-05.xml")
    #
    # def test_VNPCLogisticPromotion06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-06.xml")
    #
    # def test_VNPCLogisticPromotion07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-07.xml")
    #
    # def test_VNPCLogisticPromotion08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/VN/VNPCLogisticPromotion-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCLogisticPromotion Post condition =="
        dumplogger.info("== Setup VNPCLogisticPromotion Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
