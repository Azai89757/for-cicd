﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import copy
import Config
from Config import dumplogger


# ##Log file path
# _logfile_ = "RRBot.log"

# ##Set logging config
# hdlr = dumplogger.FileHandler(_logfile_)
# hdlr = dumplogger.handlers.RotatingFileHandler(_logfile_, maxBytes=20024*20024, backupCount=5)
# dumplogger.basicConfig(format="%(asctime)s-[%(process)d][%(thread)d]|[%(levelname)s]|[%(filename)s:%(lineno)d][%(funcName)s]|%(message)s",
#                     level=20,
#                     filename=_logfile_,
#                     filemode="a",
#                     stream=sys.stdout)

def LoadCorrelation():
    '''
    LoadCorrelation : Load correlation json file
            Input argu : N/A
            Return code :
                output - output file in json format
    '''

    ##Load RerunSequence.json to get correlation cases
    slash = Config.dict_systemslash[Config._platform_]
    json_path = Config._DataFilePath_ + slash + "common" + slash + "RerunSequence.json"

    with open(json_path, 'rb') as f:
        output = json.load(f)
    return output

class Node:
    '''Create new node - No use
    '''

    def __init__(self, value):
        self.prev = None
        self.data = value
        self.next = None


class DoubleLink:
    '''Create double linked list - No use
    '''

    def __init__(self):
        self.head = None

    ##Push the data to front of list
    def push(self, value):
        new_node = Node(value)
        new_node.next = self.head
        if self.head is not None:
            self.head.prev = new_node
        self.head = new_node

    ##Define the method to print
    def listprint(self, node):
        while (node is not None):
            print(node.data),
            last = node
            node = node.next

class RerunCalculate:

    def __init__(self):

        ##Get RerunSequence.json file
        self.rerun_json = LoadCorrelation()

    def SearchCorrelationCaseList(self, fail_case):
        '''
        SearchCorrelationCaseList : 
                Input argu : 
                    fail_case - fail case
                Return code :
                    correlation_case_list - case relationship list
        '''
        correlation_case_list = []

        ##Get fail case feature name
        feature_name = fail_case.split("-")[0]

        ##Load rerun json
        for feature in self.rerun_json:
            
            ##Find if fail case feature name in json
            if feature_name == feature["feature_name"]:
                dumplogger.debug("Feature name : %s" % (feature["feature_name"]))

                ##Load each group in feature groups
                for group in feature["groups"]:
                    dumplogger.debug("group : %s" % (group))

                    ##Find fail case correlation case list
                    if fail_case in group["correlation_case_list"]:
                        correlation_case_list = group["correlation_case_list"]
                        break
                break

        if len(correlation_case_list) == 0:
            dumplogger.info("Fail case not in correlation list")
        else:
            dumplogger.info("Fail case correlation case list : %s" % (correlation_case_list))

        return correlation_case_list

    def RRBotMain(self, fail_case_list):
        '''
        RRBotMain : Set json data to cache memory to speed up search
                Input argu :
                    fail_case_list - failed case stored in list
                Return code :
                    rr_tri_case_list - final case list
        '''
        ##====================For debug usage====================
        ##Please get all of fail cases from framrwork parameters
        #_RRFailCaseList_ = ["VNPCOrder-080102", "VNPCOrder-080101"]
        ##=======================================================

        dumplogger.info("fail_case_list = %s" % (fail_case_list))
        #print ("fail_case_list = %s" % (fail_case_list))
        ##All fail case list
        rerun_case_list = []

        ####====================Impoart##====================
        ##Load all of fail cases from list
        ##Here you just get correlation cases by each faile case. So you can get all of correlation cases in to list(append or extent)
        for each_case in fail_case_list:
            dumplogger.info("Fail case : %s" % (each_case))

            ##Find fail case correlation list and extend in rerun list
            rerun_case_list.extend(RerunCalculate.SearchCorrelationCaseList(self, each_case))
        ####====================####=========================

        return rerun_case_list
