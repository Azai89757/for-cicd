import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCCancellation Precondition =="
        dumplogger.info("== Setup FRPCCancellation Precondition ==")

    def test_FRPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCancellation-01.xml")

    def test_FRPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCancellation-02.xml")

    def test_FRPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCancellation-03.xml")

    def test_FRPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCancellation-04.xml")

    def test_FRPCCancellation05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCCancellation Post condition =="
        dumplogger.info("== Setup FRPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
