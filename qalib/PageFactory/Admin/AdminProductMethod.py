﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminProductMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from AdminCommonMethod import GoogleAccountLogin


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminProductListingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeProductStock(arg):
        '''
        ChangeProductStock : Search for product ID and change the product stock
                Input argu :
                    pid - product ID
                    count - product count
                    model_id - if model is multi then need to input model id, if is single just input blank value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pid = arg["pid"]
        product_count = arg["count"]
        model_id = arg["model_id"]
        ret = 1

        ##Check page had redirect to product detail page
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "target_user_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pid, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Click target product into detail page.
        xpath = Util.GetXpath({"locate": "pid"})
        xpath = xpath.replace('replace_pid', pid)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target pid product", "result": "1"})
        time.sleep(3)

        ##Check page had redirect to product detail page
        xpath2 = Util.GetXpath({"locate": "product_detail_title"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath2, "result": "1"})

        ##Click Edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Edit", "result": "1"})
        time.sleep(5)

        if model_id:
            ##Modify Variation product stock
            xpath = Util.GetXpath({"locate": "modify_variation_stock"})
            xpath = xpath.replace("model_id_to_be_replaced", model_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": product_count, "result": "1"})
        else:
            ##Modify Non-Variation product stock
            xpath = Util.GetXpath({"locate": "modify_non_variation_stock"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": product_count, "result": "1"})

        ##Click Submit
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Submit", "result": "1"})

        ##Confirm change product stock alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminProductListingPage.ChangeProductStock -> Change product stock:' + product_count)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PurgeCSCache(arg):
        '''
        PurgeCSCache : Purge CS cache
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Purge CS cache
        xpath = Util.GetXpath({"locate": "purge_cs_cache_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click purge CS cache", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductListingPage.PurgeCSCache')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeProductPrice(arg):
        '''
        ChangeProductPrice : Search for product ID and change the product price
                Input argu :
                    pid - product ID
                    product_price - product price
                    model_id - if model is multi then need to input model id, if is single just input blank value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pid = arg["pid"]
        product_price = arg["product_price"]
        model_id = arg["model_id"]
        ret = 1

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "target_user_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pid, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Click target product into detail page.
        xpath = Util.GetXpath({"locate": "pid"})
        xpath = xpath.replace('replace_pid', pid)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target pid product", "result": "1"})
        time.sleep(3)

        ##Check page had redirect to product detail page
        xpath2 = Util.GetXpath({"locate": "product_detail_title"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath2, "result": "1"})

        ##Click Edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Edit", "result": "1"})
        time.sleep(10)

        if model_id:
            ##Modify Variation product price
            xpath = Util.GetXpath({"locate": "modify_variation_price"})
            xpath = xpath.replace("model_id_to_be_replaced", model_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": product_price, "result": "1"})
        else:
            ##Modify Non-Variation product price
            xpath = Util.GetXpath({"locate": "modify_non_variation_price"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": product_price, "result": "1"})

        ##Click Submit
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Submit", "result": "1"})

        ##Confirm change product stock alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminProductListingPage.ChangeProductPrice -> Change product price:' + product_price)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeProductCondition(arg):
        '''
        ChangeProductCondition : Search for product ID and change the product condition
                Input argu :
                    pid - product ID
                    status
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pid = arg["pid"]
        status = arg["status"]
        ret = 1

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##From user id table, search for target ID/Email
        xpath = Util.GetXpath({"locate": "target_user_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pid, "result": "1"})
        time.sleep(5)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Click target product into detail page.
        xpath = Util.GetXpath({"locate": "pid"})
        xpath = xpath.replace('replace_pid', pid)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click target pid product", "result": "1"})
        time.sleep(5)

        ##Check page had redirect to product detail page
        xpath = Util.GetXpath({"locate": "product_detail_title"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##Click Edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Edit", "result": "1"})
        time.sleep(5)

        ##Click condition select icon to open selection
        xpath = Util.GetXpath({"locate": "select_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click select icon", "result": "1"})

        ##Select status
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "choose status", "result": "1"})

        ##Click Submit
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click Submit", "result": "1"})

        ##Confirm change product stock alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminProductListingPage.ChangeProductCondition -> Change product condition:' + status)


class AdminListingSellerUploadControlButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLocalUploadControlEdit(arg):
        '''
        ClickLocalUploadControlEdit : Click local upload control edit button
                Input argu :
                    seller_type - normal/preferred/official
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_type = arg["seller_type"]

        ##Click edit button of local upload control
        xpath = Util.GetXpath({"locate": seller_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button of local upload control", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickLocalUploadControlEdit -> ' + seller_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadControlTab(arg):
        '''
        ClickUploadControlTab : Click upload control tab
                Input argu :
                    type - detail_config_page/category_upload_page/brand_upload_page/dts_setting
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click upload control tab
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload control tab", "result": "1"})

        ##sleep time to wait page load
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickUploadControlTab -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDetailPageEdit(arg):
        '''
        ClickDetailPageEdit : Click edit button in Listing Upload Control Detail Config Page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button in Listing Upload Control Detail Config Page
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button in Listing Upload Control Detail Config Page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDetailPageEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDetailPageSubmit(arg):
        '''
        ClickDetailPageSubmit : Click submit button in Listing Upload Control Detail Config Page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit button in Listing Upload Control Detail Config Page
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit button in Listing Upload Control Detail Config Page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDetailPageSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectCategory(arg):
        '''
        ClickSelectCategory : Click select category button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select category button
        xpath = Util.GetXpath({"locate": "select_category_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select category button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickSelectCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategorySubmit(arg):
        '''
        ClickCategorySubmit : Click Category Submit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select category button
        xpath = Util.GetXpath({"locate": "category_submit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickCategorySubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryUploadPageAdd(arg):
        '''
        ClickCategoryUploadPageAdd : Click category upload page add button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click category upload page add button
        xpath = Util.GetXpath({"locate": "add_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category upload page add button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickCategoryUploadPageAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryOK(arg):
        '''
        ClickCategoryOK : Click category ok button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click category ok button
        xpath = Util.GetXpath({"locate": "ok_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickCategoryOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSSettingAdd(arg):
        '''
        ClickDTSSettingAdd : Click DTS setting add button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click DTS setting add button
        xpath = Util.GetXpath({"locate": "add_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click DTS setting add button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDTSSettingAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSSettingEdit(arg):
        '''
        ClickDTSSettingEdit : Click DTS setting edit button
                Input argu :
                    dts_setting_name - dts setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_setting_name = arg["dts_setting_name"]

        ##Click DTS setting edit button
        xpath = Util.GetXpath({"locate": "dts_edit_btn"})
        xpath_dts_edit = xpath.replace("dts_name_to_be_replace", dts_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_dts_edit, "message": "Click DTS setting edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDTSSettingEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSSettingSave(arg):
        '''
        ClickDTSSettingSave : Click DTS setting save button
                Input argu :
                    dts_setting_name - dts setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_setting_name = arg["dts_setting_name"]

        ##Click DTS setting save button
        xpath = Util.GetXpath({"locate": "dts_save_btn"})
        xpath_dts_save = xpath.replace("dts_name_to_be_replace", dts_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_dts_save, "message": "Click DTS setting save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDTSSettingSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSSettingRemove(arg):
        '''
        ClickDTSSettingRemove : Click DTS setting remove button
                Input argu :
                    dts_setting_name - dts setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_setting_name = arg["dts_setting_name"]

        ##Click DTS setting remove button
        xpath = Util.GetXpath({"locate": "dts_remove_btn"})
        xpath_dts_remove = xpath.replace("dts_name_to_be_replace", dts_setting_name)

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_dts_remove, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath_dts_remove, "message": "Click DTS setting remove button", "result": "1"})
        else:
            dumplogger.info("This DTS does not exist")

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDTSSettingRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSSettingExport(arg):
        '''
        ClickDTSSettingExport : Click DTS setting export button
                Input argu :
                    dts_setting_name - dts setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_setting_name = arg["dts_setting_name"]

        ##Click DTS setting export button
        xpath = Util.GetXpath({"locate": "dts_export_btn"})
        xpath_dts_export = xpath.replace("dts_name_to_be_replace", dts_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_dts_export, "message": "Click DTS setting export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDTSSettingExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSSelectCategory(arg):
        '''
        ClickDTSSelectCategory : Click DTS select category button
                Input argu :
                    dts_setting_name - dts setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_setting_name = arg["dts_setting_name"]

        ##Click DTS select category button
        xpath = Util.GetXpath({"locate": "dts_category_btn"})
        xpath_dts_category = xpath.replace("dts_name_to_be_replace", dts_setting_name)
        BaseUICore.Click({"method": "javascript", "locate": xpath_dts_category, "message": "Click DTS select category button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickDTSSelectCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProhibitedBrandAdd(arg):
        '''
        ClickProhibitedBrandAdd : Click Prohibited Brand add button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(5)

        ##Click DTS setting add button
        xpath = Util.GetXpath({"locate": "add_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Prohibited Brand add button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickProhibitedBrandAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProhibitedBrandRemove(arg):
        '''
        ClickProhibitedBrandRemove : Click Prohibited Brand remove button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click prohibited brand remove button
        xpath = Util.GetXpath({"locate": "remove_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Prohibited Brand remove button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickProhibitedBrandRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProhibitedBrandRemoveAll(arg):
        '''
        ClickProhibitedBrandRemoveAll : Click Prohibited Brand remove all button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click prohibited brand remove all button
        xpath = Util.GetXpath({"locate": "remove_all_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Prohibited Brand remove all button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickProhibitedBrandRemoveAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategoryExport(arg):
        '''
        ClickCategoryExport : Click Category export button
                Input argu :
                    dts_setting_name - dts setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dts_setting_name = arg["dts_setting_name"]

        ##Click group seller setting export button
        xpath = Util.GetXpath({"locate": "dts_setting_export_btn"})
        xpath_dts_export = xpath.replace("dts_setting_to_be_replace", dts_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_dts_export, "message": "Click Category export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlButton.ClickCategoryExport')


class AdminListingSellerUploadControlPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTitleMinLength(arg):
        '''
        InputTitleMinLength : Input title min length
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input title min length
        xpath = Util.GetXpath({"locate": "input_box_of_title_min_length"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputTitleMinLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTitleMaxLength(arg):
        '''
        InputTitleMaxLength : Input title max length
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input title max length
        xpath = Util.GetXpath({"locate": "input_box_of_title_max_length"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputTitleMaxLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMinImages(arg):
        '''
        InputMinImages : Input min images
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input min images
        xpath = Util.GetXpath({"locate": "input_box_of_min_images"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputMinImages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageSizeX(arg):
        '''
        InputImageSizeX : Input image size X
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input image size X
        xpath = Util.GetXpath({"locate": "input_box_of_image_size_x"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputImageSizeX')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageSizeY(arg):
        '''
        InputImageSizeY : Input image size Y
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input image size Y
        xpath = Util.GetXpath({"locate": "input_box_of_image_size_y"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputImageSizeY')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDescMinLength(arg):
        '''
        InputDescMinLength : Input desc min length
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input desc min length
        xpath = Util.GetXpath({"locate": "input_box_of_desc_min_length"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputDescMinLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDescMaxLength(arg):
        '''
        InputDescMaxLength : Input desc max length
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input desc max length
        xpath = Util.GetXpath({"locate": "input_box_of_desc_max_length"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputDescMaxLength')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMinPrice(arg):
        '''
        InputMinPrice : Input min price
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input min price
        xpath = Util.GetXpath({"locate": "input_box_of_min_price"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputMinPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMaxPrice(arg):
        '''
        InputMaxPrice : Input max price
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input max price
        xpath = Util.GetXpath({"locate": "input_box_of_max_price"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputMaxPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMinStock(arg):
        '''
        InputMinStock : Input min stock
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input min stock
        xpath = Util.GetXpath({"locate": "input_box_of_min_stock"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputMinStock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMaxStock(arg):
        '''
        InputMaxStock : Input max stock
                Input argu :
                    text - input text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input max stock
        xpath = Util.GetXpath({"locate": "input_box_of_max_stock"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputMaxStock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCategory(arg):
        '''
        SelectCategory : Select category
                Input argu :
                    category_L1_name - category L1 name
                    category_L2_name - category L2 name
                    category_L3_name - category L3 name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_L1_name = arg["category_L1_name"]
        category_L2_name = arg["category_L2_name"]
        category_L3_name = arg["category_L3_name"]

        ##Select category L1
        xpath = Util.GetXpath({"locate": "category_L1"})
        xpath_category = xpath.replace("category_name_to_be_replace", category_L1_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_category, "message": "Select category L1", "result": "1"})

        ##Select category L2, if you need to select
        if category_L2_name:
            xpath = Util.GetXpath({"locate": "category_L2"})
            xpath_category = xpath.replace("category_name_to_be_replace", category_L2_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath_category, "message": "Select category L2", "result": "1"})

        ##Select category L3, if you need to select
        if category_L3_name:
            category_L3_name = arg["category_L3_name"]
            xpath = Util.GetXpath({"locate": "category_L3"})
            xpath_category = xpath.replace("category_name_to_be_replace", category_L3_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath_category, "message": "Select category L3", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.SelectCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveCategory(arg):
        '''
        RemoveCategory : Remove category
                Input argu :
                    category_name - category name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click category remove button
        xpath = Util.GetXpath({"locate": "category_remove_btn"})
        xpath_category_remove = xpath.replace("category_name_to_be_replace", category_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_category_remove, "message": "Click category remove button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.RemoveCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveAllCategory(arg):
        '''
        RemoveAllCategory : Remove All category
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click category remove button
        category_remove_all_btn = Util.GetXpath({"locate": "category_remove_all_btn"})
        BaseUICore.Click({"method": "xpath", "locate": category_remove_all_btn, "message": "Click category remove all button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.RemoveAllCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPreOrderRange(arg):
        '''
        InputPreOrderRange : Input preorder range
                Input argu :
                    from_value - input value to setting preorder start range
                    to_value - input value to setting preorder end range
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        from_value = arg["from_value"]
        to_value = arg["to_value"]

        ##Input preorder start value to setting preorder range
        xpath = Util.GetXpath({"locate": "input_box_of_preorder_range_from"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": from_value, "result": "1"})

        ##Input preorder end value to setting preorder range
        xpath = Util.GetXpath({"locate": "input_box_of_preorder_range_to"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": to_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputPreOrderRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchNonPreorderDTS(arg):
        '''
        SwitchNonPreorderDTS : Switch non preorder DTS radio button
                Input argu : day - 1 / 2 / 3
                Return code :
                            1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        dts_day = arg["day"]

        ##Click DTS radio button
        xpath = Util.GetXpath({"locate": "non_preorder_dts_radio_button"})
        xpath = xpath.replace("dts_day_to_be_replaced", dts_day)
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        time.sleep(2)

        xpath = Util.GetXpath({"locate": "save_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.SwitchNonPreorderDTS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProhibitedBrandID(arg):
        '''
        InputProhibitedBrandID : Input Prohibited Brand ID
                Input argu :
                    brand_id - input brand id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        brand_id = arg["brand_id"]

        ##Input preorder brand
        xpath = Util.GetXpath({"locate": "input_box_of_prohibited_brand"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": brand_id, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputProhibitedBrandID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWholesalePrice(arg):
        '''
        InputWholesalePrice : Input Whole sale price
                Input argu :
                    percent - input percent
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        percent = arg["percent"]

        ##Input min price
        xpath = Util.GetXpath({"locate": "input_box_of_wholesale_price"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": percent, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.InputWholesalePrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBlacklistedFile(arg):
        '''
        UploadBlacklistedFile : Upload Group Seller file
                Input argu :
                    file_name - which file we want to upload in local path
                    file_type - which file type we will upload in local path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click choose file button
        choose_file_btn = Util.GetXpath({"locate":"choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": choose_file_btn, "message": "Click Blacklisted file", "result": "1"})
        time.sleep(3)

        ##Click and upload seller file
        upload_path = Util.GetXpath({"locate":"upload_path"})
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate": upload_path, "element": "input-upload-to-add-0", "element_type": "id", "project": "Listing", "action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(3)

        ##Click upload button
        upload_btn = Util.GetXpath({"locate":"upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": upload_btn, "message": "Click Upload Blacklisted file", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.UploadBlacklistedFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCategoryFile(arg):
        '''
        UploadCategoryFile : Upload Category file
                Input argu :
                    file_name - which file we want to upload in local path
                    file_type - which file type we will upload in local path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click choose file button
        choose_file_btn = Util.GetXpath({"locate":"choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": choose_file_btn, "message": "Click Blacklisted file", "result": "1"})
        time.sleep(3)

        ##Click and upload seller file
        upload_path = Util.GetXpath({"locate":"upload_path"})
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate": upload_path, "element": "input-upload-to-add-0", "element_type": "id", "project": "Listing", "action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(3)

        ##Click upload button
        upload_btn = Util.GetXpath({"locate":"upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": upload_btn, "message": "Click Upload Blacklisted file", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminListingSellerUploadControlPage.UploadCategoryFile')


class AdminListingGroupUploadControlPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditGroupName(arg):
        '''
        EditGroupName : Edit Group Name
                Input argu :
                    old_group_name - old seller group name
                    new_group_name - new seller group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        old_group_name = arg["old_group_name"]
        new_group_name = arg["new_group_name"]

        ##Input min price
        xpath = Util.GetXpath({"locate": "input_box_of_group_name"})
        input_xpath = xpath.replace("group_seller_name_to_be_replace", old_group_name)
        BaseUICore.Input({"method": "xpath", "locate": input_xpath, "string": new_group_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.EditGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGroupName(arg):
        '''
        InputGroupName : Input Group Name
                Input argu :
                    group_name - input group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_name = arg["group_name"]

        ##Input min price
        xpath = Util.GetXpath({"locate": "input_box_of_group_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.InputGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddGroupBtn(arg):
        '''
        ClickAddGroupBtn : Click Add Group button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add group button
        xpath = Util.GetXpath({"locate": "add_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Prohibited Brand add button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.ClickAddGroupBtn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadGroupSellerFile(arg):
        '''
        UploadGroupSellerFile : Upload Group Seller file
                Input argu :
                    group_seller_name - group seller name
                    input_id - group seller upload element located id
                    file_name - which file we want to upload in local path
                    file_type - which file type we will upload in local path
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_seller_name = arg["group_seller_name"]
        input_id = arg["input_id"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click choose file button
        xpath = Util.GetXpath({"locate": "choose_file_btn_by_seller"})
        choose_file_btn = xpath.replace("group_name_to_be_replace", group_seller_name)
        BaseUICore.Click({"method": "xpath", "locate": choose_file_btn, "message": "Click Upload group seller file", "result": "1"})
        time.sleep(3)

        ##Click and upload seller file
        xpath = Util.GetXpath({"locate": "upload_path_by_seller"})
        upload_path = xpath.replace("group_name_to_be_replace", group_seller_name).replace("input_id_to_be_replace", input_id)
        BaseUILogic.UploadFileWithPath({"method": "xpath", "locate": upload_path, "element": input_id, "element_type": "id", "project": "Listing", "action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(3)

        ##Click upload button
        element_id = input_id.replace("input-upload-to-add-", "button-file-upload-")
        xpath = Util.GetXpath({"locate": "upload_btn_by_seller"})
        upload_btn = xpath.replace("group_name_to_be_replace", group_seller_name).replace("button_id_to_be_replace", element_id)
        BaseUICore.Click({"method": "xpath", "locate": upload_btn, "message": "Click Upload group seller file", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.UploadGroupSellerFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupSellerEditBtn(arg):
        '''
        ClickGroupSellerEditBtn : Click Group Seller Edit button
                Input argu :
                    group_seller_setting_name - group seller setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_seller_setting_name = arg["group_seller_setting_name"]

        ##Click Group Seller remove button
        xpath = Util.GetXpath({"locate": "group_seller_name"})
        group_seller_id = xpath.replace("group_seller_name_to_be_replace", group_seller_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": group_seller_id, "message": "Click Group Seller Edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.ClickGroupSellerEditBtn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupSellerExportBtn(arg):
        '''
        ClickGroupSellerExportBtn : Click Group Seller export button
                Input argu :
                    group_seller_name - group seller name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_seller_name = arg["group_seller_name"]

        ##Click group seller setting export button
        xpath = Util.GetXpath({"locate": "group_seller_export_btn"})
        xpath_dts_export = xpath.replace("group_name_to_be_replace", group_seller_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_dts_export, "message": "Click Group Seller export button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.ClickGroupSellerExportBtn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupSellerIDSettingBtn(arg):
        '''
        ClickGroupSellerIDSettingBtn : Click Group Seller ID setting button
                Input argu :
                    group_seller_setting_name - group seller setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_seller_setting_name = arg["group_seller_setting_name"]

        ##Click group seller name setting button
        xpath = Util.GetXpath({"locate": "group_seller_name"})
        group_seller_id = xpath.replace("group_seller_name_to_be_replace", group_seller_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": group_seller_id, "message": "Click Group Seller ID setting button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.ClickGroupSellerIDSettingBtn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupSellerRemoveBtn(arg):
        '''
        ClickGroupSellerRemoveBtn : Click Group Seller Remove button
                Input argu :
                    group_seller_name - group seller name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_seller_name = arg["group_seller_name"]

        ##Click Group Seller remove button
        xpath = Util.GetXpath({"locate": "group_seller_name"})
        group_seller_id = xpath.replace("group_seller_name_to_be_replace", group_seller_name)
        BaseUICore.Click({"method": "xpath", "locate": group_seller_id, "message": "Click Group Seller remove button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.ClickGroupSellerRemoveBtn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupSellerSaveBtn(arg):
        '''
        ClickGroupSellerSaveBtn : Click Group Seller Save button
                Input argu :
                    group_seller_setting_name - group seller setting name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_seller_setting_name = arg["group_seller_setting_name"]

        ##Click Group Seller remove button
        xpath = Util.GetXpath({"locate": "group_seller_name"})
        group_seller_id = xpath.replace("group_seller_name_to_be_replace", group_seller_setting_name)
        BaseUICore.Click({"method": "xpath", "locate": group_seller_id, "message": "Click Group Seller save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminListingGroupUploadControlPage.ClickGroupSellerSaveBtn')


class AdminUpdateFreeShippingFlagPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCsvFile(arg):
        '''
        UploadCsvFile : Upload Mass Add / Remove Shops csv file
                Input argu :
                            type - add / remove
                            file_name - csv file name you want to upload
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        type = arg["type"]
        file_name = arg["file_name"]

        ##Click choose file button
        xpath = Util.GetXpath({"locate": type + "_choose_file_btn"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        time.sleep(5)

        ##Upload promotion rule csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})
        time.sleep(5)

        ##Click choose file button
        xpath = Util.GetXpath({"locate": type + "_submit_btn"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminUpdateFreeShippingFlagPage.UploadCsvFile')
