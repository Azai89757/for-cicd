import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSignup Precondition =="
        dumplogger.info("== Setup PHPCSignup Precondition ==")

    def test_PHPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCSignup Post condition =="
        dumplogger.info("== Setup PHPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
