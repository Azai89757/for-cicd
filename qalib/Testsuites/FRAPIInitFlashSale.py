import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRAPIInitFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRAPIInitFlashSale Precondition =="
        dumplogger.info("== Setup FRAPIInitFlashSale Precondition ==")

    def test_FRAPIInitFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/FRAPIInitFlashSale-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRAPIInitFlashSale Post condition =="
        dumplogger.info("== Setup FRAPIInitFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
