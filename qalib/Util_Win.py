'''
 Util_Win.py: The def in this file is common utility of FrameWorkBase(For windows only)
          Most of them are used internally.
          If your def is used by multiple other def, please place it here.
          If your def is used by XML, place place to XtFunc.py.
'''
##Import common library
import os
import time
import ctypes
import re
import Config
from Config import dumplogger
import subprocess

##import win32 library
import win32con
import win32gui
import win32process

##import framework library
import FrameWorkBase
import BaseUICore
import AndroidBaseUICore


def CleanUpRegistry(arg):
    '''CleanUpRegistry: Delete the specific registry key value (XML version)
        Input argu : key - prefix key name
                     value - the key which need to modify
        Return code:  1 - success modified
                     -1 - Fail
        Note: It's called by XML only, similar func: setRegistry'''
    ret = -1
    key = None
    ##Replace / to \
    arg['key'] = arg['key'].replace('/', '\\')
    nPos = arg['key'].index('HKEY_LOCAL_MACHINE')
    if nPos != -1:
        arg['key'] = arg['key'].strip('HKEY_LOCAL_MACHINE\\')
    if (Util.get_OS_64_32() == "64Bit"):
        key = OpenKey(HKEY_LOCAL_MACHINE,
                      arg['key'], 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY)
    else:
        key = OpenKey(HKEY_LOCAL_MACHINE, arg['key'], 0, KEY_ALL_ACCESS)
    if key is not None:
        value = ctypes.c_int()
        aa = QueryValueEx(key, arg['value'])
        DeleteValue(key, arg['value'])
        try:
            value, type = QueryValueEx(key, arg['value'])
        except:
            OK(ret, int(arg['result']), 'CleanUpRegistry ' + arg['value'])
        CloseKey(key)
    else:
        OK(-1, int(arg['result']), 'CleanUpRegistry ' + arg['value'])
        CloseKey(key)


def CheckRegistryValueDataExist(arg):
    '''CheckRegistryValueDataExist: Check if the specific registry key exist
Input argu : key - prefix key path. You can enter "current_time" to search current time.
             name - the key name need to modify
             type - the type of reg key
             value - the final value of key needs to modify
            Sample : arg = {"key":"HKEY_LOCAL_MACHINE\SOFTWARE\QATEST",
                                            "name":"TEST2",
                                            "type":"REG_DWORD",
                                            "value":"100"}
Return code :	0	- Success find
                                    1	- Find the key but value of key is not same as Input argument
                                    -1	- Cannot find the key
    Note : None'''

    dumplogger.info("Enter CheckRegistryValueDataExist")

    ret = 1
    regkey = arg["key"]
    regname = arg["name"]
    regvalue = arg["value"]
    root_key = None

    # Replace /
    if "/" in regkey:
        regkey = regkey.replace("/", "\\")
        dumplogger.info("After replace / = %s" % (regkey))
    # Delete HKEY_LOCAL_MACHINE for winreg OpenKey
    nPos = regkey.index('HKEY_LOCAL_MACHINE')
    if nPos != -1:
        regkey = regkey.replace('HKEY_LOCAL_MACHINE\\', '')
        dumplogger.info(
            "Locate position after HKEY_LOCAL_MACHINE = %s" % (regkey))

    if "\\current_time" in regkey:
        regkey = regkey.replace('\\current_time', '')
        regkey = regkey + "\\" + Config._CurDateTime_
        dumplogger.info("keypath+current_time = " + regkey)

    # Open key
    try:
        # Get handler
        root_key = OpenKey(HKEY_LOCAL_MACHINE, regkey, 0, KEY_READ)
        try:
            # Query registry value and name
            [receive_regvalue, receive_regtype] = (
                QueryValueEx(root_key, regname))
            dumplogger.info("receive_regtype = %s" % (receive_regtype))
            dumplogger.info("receive_regvalue = %s" % (receive_regvalue))
            dumplogger.info("_CurRegistryNameValue_[value] = %s" % (
                _CurRegistryNameValue_["value"]))
            dumplogger.info("receive_regvalue = %s" % (str(receive_regvalue)))

            # Compare registry value
            if str(_CurRegistryNameValue_["value"]) == str(receive_regvalue):
                # print "Compare registry value and memory - success"
                dumplogger.info(
                    "Compare registry value and memory - success")
            elif str(regvalue) == str(receive_regvalue):
                # print "Compare registry value - success"
                dumplogger.info("Compare registry value - success")
            else:
                # print "Compare registry value - fail"
                dumplogger.info("Compare registry value - fail")
                ret = 0

            CloseKey(root_key)

        except WindowsError:
            # print "Do not find registry name !!?"
            dumplogger.info("Do not find registry name !!?")
            CloseKey(root_key)
            ret = -1
            OK(ret, int(
                arg['result']), 'CheckRegistryValueDataExist Exception' + regname + ' - ' + "not found")

    except:
        print "Do not find registry key !!?"
        dumplogger.info("Do not find registry key !!?")
        CloseKey(root_key)
        ret = -1
        OK(ret, int(arg['result']), 'CheckRegistryValueDataExist Exception' + regkey + ' - ' + "not found")

    # print "====Registry in Global===="
    dumplogger.info("====Registry in Global====")
    dumplogger.info(_CurRegistryNameValue_)
    dumplogger.info("==========================")
    # print "=========================="
    OK(ret, int(arg['result']), 'CheckRegistryValueDataExist ' + regkey + '\\' + regname + '=' + regvalue)


def SetRegistryValue(arg):
    '''SetRegistryValue : Set the specific registry key value (XML version)
Input argu : key - prefix key path. You can enter "current_time" to search current time.
             name - the key name need to modify
             type - the type of reg key
             value - the final value of key needs to modify
            Sample : arg = {"key":"HKEY_LOCAL_MACHINE\SOFTWARE\QATEST1",
                                            "name":"TEST8",
                                            "type":"REG_DWORD",
                                            "value":"10"}
Return code :  	0	- Success modified
                                    -1	- Fail
    Note : It's called by XML only, similar func: setRegistry'''

    dumplogger.info("Enter SetRegistryValue")
    ret = 1
    regkey = arg["key"]
    regname = arg["name"]
    regvalue = arg["value"]
    regtype = arg["type"]

    global _CurRegistryNameValue_
    _CurRegistryNameValue_ = {}

    # Replace /
    if "/" in regkey:
        regkey = regkey.replace("/", "\\")
        dumplogger.info("After replace / = %s" % (regkey))

    # Delete HKEY_LOCAL_MACHINE for winreg OpenKey
    nPos = regkey.index('HKEY_LOCAL_MACHINE')
    if nPos != -1:
        regkey = regkey.replace('HKEY_LOCAL_MACHINE\\', '')
        dumplogger.info(
            "Locate position after HKEY_LOCAL_MACHINE = %s" % (regkey))

    if "\\current_time" in regkey:
        regkey = regkey.replace('\\current_time', '')
        regkey = regkey + "\\" + Config._CurDateTime_
        # print "keypath+current_time = " + regkey
        dumplogger.info("keypath+current_time = " + regkey)

    if regvalue == "current_time":
        regvalue = Config._CurDateTime_
        dumplogger.info("current_time = %s" % (Config._CurDateTime_))
    # Check key is ixist
    try:
        reg = ConnectRegistry(None, HKEY_LOCAL_MACHINE)
        root_key = OpenKey(reg, regkey)
        CloseKey(root_key)
        # print "Registry key exist"
        dumplogger.info("Registry key exist -> %s" % (regkey))

    # Help to create key
    except WindowsError:
        # print "Do not find registry key - Help to create"
        dumplogger.info("Do not find registry key - Help to create")
        CreateKey(HKEY_LOCAL_MACHINE, regkey)
    # Create reg name and value
    finally:
        root_key = OpenKey(HKEY_LOCAL_MACHINE, regkey, 0, KEY_WRITE)

        try:
            if regtype == "REG_DWORD":
                SetValueEx(root_key, regname, 0, REG_DWORD, int(regvalue))

            elif regtype == "REG_SZ":
                SetValueEx(root_key, regname, 0, REG_SZ, str(regvalue))

            elif regtype == "REG_BINARY":
                SetValueEx(root_key, regname, 0,
                           REG_BINARY, bin(int(regvalue)))

            # Assign registry name and value to global variable
            _CurRegistryNameValue_["value"] = regvalue
            _CurRegistryNameValue_["name"] = regname
            dumplogger.info("====Registry in Global====")
            dumplogger.info(_CurRegistryNameValue_)
            dumplogger.info("==========================")

        except WindowsError:
            # print "Create registry key fail !!?"
            dumplogger.info("Create registry key fail !!?")
            ret = -1

    CloseKey(root_key)
    OK(ret, int(arg['result']), 'SetRegistryValue ' + arg['value'])


def DelRegistryKey(arg):
    '''DelRegistryKey : Directly del path, do not del value
        Input argu : key - prefix key path
             name - the key name need to modify
             type - the type of reg key
             value - the final value of key needs to modify
            Sample : arg = {"key":"HKEY_LOCAL_MACHINE\SOFTWARE\QATEST2",
                                            "subkey":"QATEST3"}
        Return code :  	0	- Success modified
                        -1	- Fail
    '''
    import traceback
    ret = 1
    regkey = arg["key"]
    regsubkey = arg["subkey"]
    # Check key is ixist
    if "/" in regkey:
        regkey = regkey.replace("/", "\\")
    # Delete HKEY_LOCAL_MACHINE for winreg OpenKey
    nPos = regkey.index('HKEY_LOCAL_MACHINE')
    if nPos != -1:
        regkey = regkey.replace('HKEY_LOCAL_MACHINE\\', '')
    # Get rootkey handle
    root_key = OpenKey(HKEY_LOCAL_MACHINE, "SOFTWARE")
    try:
        reg = ConnectRegistry(None, HKEY_LOCAL_MACHINE)
        root_key = OpenKey(reg, regkey)
        # Try to del subkey
        try:
            DeleteKey(root_key, regsubkey)

        except WindowsError:
            print "Do not find registry sub key !!?"
            print traceback.print_exc()
            dumplogger.exception('Got exception error')
            ret = -1
            OK(ret, int(arg['result']), 'CheckRegistryValueDataExist Exception' + regsubkey + ' - ' + "not found")

        finally:
            CloseKey(root_key)

    except WindowsError:
        print "Do not find registry key !!?"
        print traceback.print_exc()
        dumplogger.exception('Got exception error')
        ret = -1
        OK(ret, int(arg['result']), 'CheckRegistryValueDataExist Exception' + regkey + ' - ' + "not found")
    finally:
        CloseKey(root_key)

    OK(ret, int(arg['result']), 'CheckRegistryValueDataExist - ' + regsubkey + ' - ' + "del")


def setRegistry(regkey, value, type, data):
    '''setRegistry: Set the specific registry key value (Internal version)
                    If the key not exist, the function will create this registry key.
    Input argu : key - prefix key name, please add prefix 'r', ex: r'SOFTWARE\\...'
                 value - the key which need to modify
                 type - the type of reg key
                 data - the final value of key needs to modify
    Return code:  1 - success
                  not 1 - Fail
    Note: It should be called by internal function, don't use it on XML, similar func: SetRegistryValue'''

    if (Config._Platform_ == "x64"):
        key = OpenKey(HKEY_LOCAL_MACHINE, regkey, 0, KEY_ALL_ACCESS | KEY_WOW64_64KEY)
    else:
        key = OpenKey(HKEY_LOCAL_MACHINE, regkey, 0, KEY_ALL_ACCESS)

    ##Translate type to _winreg type
    if 'REG_DWORD' in type:
        type = REG_DWORD
    elif 'REG_BINARY' in type:
        type = REG_BINARY

    if key is not None:

        if REG_BINARY == type:
            temp = int(data, 16)
            temp = hex(temp)
            data = unhexlify(temp)

        ret = SetValueEx(key, value, 0, type, data)
        result_value = ctypes.c_int()
        result_value, result_type = QueryValueEx(key, value)

        ##Type is REG_DWORD
        if result_type == 4:
            data = 0 if '' else data
            CloseKey(key)

            if data == result_value:
                return 1
        else:
            CloseKey(key)
            return 1
    else:
        CloseKey(key)
        return -1


def Get_hwnds_for_pid(pid):
    ''' Get_hwnds_for_pid : Use process id to get windows handle
            Input argu :
                name - process name
            Return code : 1 - success
                         0 - fail
            Note : None  '''
    def callback(hwnd, hwnds):
        if win32gui.IsWindowVisible(hwnd) and win32gui.IsWindowEnabled(hwnd):
            _, found_pid = win32process.GetWindowThreadProcessId(hwnd)
            if found_pid == pid:
                hwnds.append(hwnd)
            return True
    hwnds = []
    win32gui.EnumWindows(callback, hwnds)
    return hwnds


def GetWindowLocationAndSize(hwnd):
    ''' GetWindowLocationAndSize : Get window location and size
            Input argu :
                name - process name
            Return code : 1 - success
                         0 - fail
            Note : None  '''

    rect = win32gui.GetWindowRect(hwnd)
    x = rect[0]
    y = rect[1]
    w = rect[2] - x
    h = rect[3] - y
    print "Window %s:" % win32gui.GetWindowText(hwnd)
    print "\tLocation: (%d, %d)" % (x, y)
    print "\t    Size: (%d, %d)" % (w, h)


def InitialPositionAndSize(project, foreground=1, max=0, getls=0, setls=0):

    if project == "wow":
        process_name = "WowI.exe"
    if project == "hs":
        process_name = "Hearthstone.exe"
        time.sleep(10)
    if project == "win7_photo_viewer":
        process_name = "rundll32.exe"

    proc_id = FrameWorkBase.PNameQueryPID(process_name)
    time.sleep(2.0)

    if len(Get_hwnds_for_pid(proc_id)) == 0:
        print "Can't find process!!"
    else:
        for hwnd in Get_hwnds_forr_pid(proc_id):
            #Print handle number
            print "Hwnd ID :" + win32gui.GetWindowText(hwnd), "=>", hwnd

            #Set foreground
            if foreground == 1:
                win32gui.SetForegroundWindow(hwnd)

            #Maximize window
            if max == 1:
                win32gui.ShowWindow(hwnd, win32con.SW_MAXIMIZE)

            #Get window location
            if getls == 1:
                GetWindowLocationAndSize(hwnd)

            #Set window position and size
            #  PS : Please refer "win32gui.SetWindowPos" windows API if you interested for this.
            if setls == 1 and project == "wow":
                win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, 64, 1, 1792, 1037, win32con.SWP_SHOWWINDOW)
            if setls == 1 and project == "hs":
                win32gui.SetWindowPos(hwnd, win32con.HWND_TOPMOST, 64, 1, 1792, 1010, win32con.SWP_SHOWWINDOW)


class GetImgCoordinate():

    def __init__(self):

        dumplogger.info("Enter GetImgCoordinate initial")
        ##Defind the groud value for x, y
        self.ismatch = 0
        self.x_on_windows = 0
        self.y_on_windows = 0
        self.final_x = 0
        self.final_y = 0
        self.initial_x = 745
        self.initial_y = 140
        ##Defind the ratio of the screen between windows and phone
        #self.ratio_x = 2.7
        #self.ratio_y = 2.6
        self.ratio_x = 0.0
        self.ratio_y = 0.0
        ##File paths
        self.imgfile = ""
        self.sikuli_log = ""

    def GetDeviceScreenRadio(self):
        ''' GetDeviceScreenRadio : Get device screen radio
                int Argu : N/A
            Note : None'''

        ## Compare device id from GlobalAdapter and get x,y radio
        device_id = AndroidBaseUICore.GetMobileDeviceId("android")
        if device_id in Config._MobileDeviceInfo_:
            try:
                self.ratio_x = float(Config._MobileDeviceInfo_[device_id]["device_screen_radio_x"])
                self.ratio_y = float(Config._MobileDeviceInfo_[device_id]["device_screen_radio_y"])
            except:
                dumplogger.exception("Please check error message")

            dumplogger.info(self.ratio_x)
            dumplogger.info(self.ratio_y)

    def GetScreenShot(self, mode):
        ''' GetScreenShot : Get current screenshot
                Input argu :
                    mode - web / android
            Note : None'''

        output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]
        ##If output folder not exist, created !!
        if os.path.exists(output_casename_folder):
            dumplogger.info(output_casename_folder)
        else:
            command = "mkdir " + output_casename_folder
            dumplogger.info("command = " + command)
            os.system(command)

        ##Save the screen shot by WebDriver
        try:
            ##For web driver
            if mode == "web":
                print "Enter webdriver"
                BaseUICore.GetBrowserScreenShot(output_casename_folder + "ForSikuli" + ".png")
                self.imgfile = output_casename_folder + "ForSikuli" + ".png"
            ##For android driver
            elif mode == "android":
                print "Enter android"
                AndroidBaseUICore._AndroidDriver_.get_screenshot_as_file(output_casename_folder + "ForOpenCV.png")
                self.imgfile = output_casename_folder + "ForOpenCV.png"
            else:
                dumplogger.error("Please check your mode")

        except:
            dumplogger.error("Image save error !!")
            dumplogger.exception('Got exception error')

    def OpenPhoto(self):
        ''' OpenPhoto : Open photo by Windows Photo Viewer
                Input argu : N/A
            Note : None'''

        print self.imgfile

        try:
            proc = subprocess.Popen(["rundll32.exe", "%ProgramFiles%\Windows Photo Viewer\PhotoViewer.dll", ",", "ImageView_Fullscreen", self.imgfile], shell=True)
            InitialPositionAndSize("win7_photo_viewer")
        except:
            dumplogger.exception('Got exception error')

    def GetCoordinateBySikuli(self, project, act):
        '''
        GetCoordinateBySikuli : Get coordinate by sikuli
                Input argu :
                    project - project name
                    act - action folder
                Note : None
        '''

        x_cor = ''
        y_cor = ''

        SIKULI_debuglog_path = Config._CurDataDIR_ + '\\data\\' + \
            project + "\\" + "sikuli_" + project + '_log'
        print SIKULI_debuglog_path

        #SIKULI_suite_path = 'C:\\QATest\\data\\' + project + "\\" + act + ".sikuli"
        SIKULI_suite_path = Config._CurDataDIR_ + \
            '\\data\\' + project + "\\" + act + ".sikuli"
        #SIKULI_suite_path = Config._CurDataDIR_ + '\\data\\' + project + "\\" + act + ".skl"
        print SIKULI_suite_path

        # You can add more script for any project
        #command = Config._CurDataDIR_ + '\\tools\Sikuli\\runIDE.cmd -d 2 -f ' + SIKULI_debuglog_path + ' -r ' + SIKULI_suite_path + ' --args ' + act
        #command = Config._CurDataDIR_ + '\\tools\\Sikuli\\runIDE.cmd -d 2 -r ' + SIKULI_suite_path + ' --args ' + act + ' > ' + SIKULI_debuglog_path
        command = Config._CurDataDIR_ + '\\tools\\Sikuli\\runsikulix.cmd -d 2 -r ' + \
            SIKULI_suite_path + ' --args ' + act + ' > ' + SIKULI_debuglog_path
        print command
        dumplogger.info("Command = " + command)
        os.system(command)
        time.sleep(4)

        ##Get coordinate from log
        file = open(SIKULI_debuglog_path, 'r')
        line = file.readlines()
        for eachline in line:
            if 'x' in eachline:
                x_cor = [int(s) for s in re.findall(r'\b\d+\b', eachline)]
            elif 'y' in eachline:
                y_cor = [int(s) for s in re.findall(r'\b\d+\b', eachline)]
            elif 'matched' in eachline:
                self.ismatch = 1

        self.x_on_windows = x_cor[0]
        self.y_on_windows = y_cor[0]

        #print self.x_on_windows, self.y_on_windows
        print "ismatch = %d" % (self.ismatch)
        return self.ismatch

    def TransferCoordinates(self):
        ''' TransferCoordinates : Transfer coordinates from windows to phone screen
                Input argu : N/A
            Note : None'''

        self.final_x = (self.x_on_windows - self.initial_x) * self.ratio_x
        self.final_y = (self.y_on_windows - self.initial_y) * self.ratio_y

        print int(self.final_x)
        print int(self.final_y)
        return int(self.final_x), int(self.final_y)

    def ClosePhoto(self):
        ''' ClosePhoto : Close photo by killing it's parent procecss
                Input argu : N/A
            Note : None'''
        try:
            os.system("taskkill -f -im rundll32.exe")
        except:
            dumplogger.exception('Got exception error')
