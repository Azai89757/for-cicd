import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCSellerOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSellerOrder Precondition =="
        dumplogger.info("== Setup IDPCSellerOrder Precondition ==")

    def test_IDPCSellerOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerOrder-01.xml")

    def test_IDPCSellerOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerOrder-02.xml")

    def test_IDPCSellerOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerOrder-03.xml")

    def test_IDPCSellerOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerOrder-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSellerOrder Post condition =="
        dumplogger.info("== Setup IDPCSellerOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
