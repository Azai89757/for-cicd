﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 PaymentMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class PaymentMainPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVcode(arg):
        '''
        InputVcode : Input vcode for phone number verification
            Input argu :
                vcode - vcode for phone number verification
                finish - no (don't press finish) / yes (press finish)
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        vcode = arg["vcode"]
        finish = arg["finish"]
        ret = 1

        ##Input Vcode
        xpath = Util.GetXpath({"locate":"vcode_input_column"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":vcode, "result": "1"})

            if finish:
                ##Click finish
                xpath = Util.GetXpath({"locate":"submit_button"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})
                time.sleep(3)

        OK(ret, int(arg['result']), 'PaymentMainPage.InputVcode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBankAccount(arg):
        '''
        DeleteBankAccount : Click to delete a bank account
            Input argu :
                password - account password
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        password = arg['password']

        ##Click to delete a bank account
        xpath = Util.GetXpath({"locate":"delete_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to delete a bank account", "result": "1"})

        if Config._TestCaseRegion_ == "IN":
            ##Input password if need it (specific bank account)
            xpath = Util.GetXpath({"locate":"input_password"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        ##Click confirm delete btn
        xpath = Util.GetXpath({"locate":"confirm_delete"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm delete btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'PaymentMainPage.DeleteBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreBankAccountStatus(arg):
        '''
        RestoreBankAccountStatus : Restore bank account status by deleting them all
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Get number of how many bank accounts in the page
        xpath = Util.GetXpath({"locate":"bank_account_content"})
        BaseUILogic.GetAndReserveElements({"locate":xpath, "reservetype":"count", "result": "1"})

        for retry_count in range(1, GlobalAdapter.GeneralE2EVar._Count_+1):
            ##Click to delete a bank account
            PaymentMainPage.DeleteBankAccount({"result": "1"})

        OK(ret, int(arg['result']), 'PaymentMainPage.RestoreBankAccountStatus')


class CreditCardPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCreditCardInfo(arg):
        '''
        InputCreditCardInfo : Input credit card info
                Input argu :
                    name - name
                    card_number - card_number
                    expire_date - expire_date
                    cvv - cvv
                    state - state
                    city - city
                    address - address
                    street - street_number
                    postal - postal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg['name']
        card_number = arg['card_number']
        expire_date = arg['expire_date']
        cvv = arg['cvv']
        state = arg['state']
        city = arg['city']
        address = arg['address']
        street = arg['street']
        postal = arg['postal']

        ##Add credit card page is new page and related elements are in another iframe, need to switch to related iframe first
        if Config._TestCaseRegion_ in ("ES", "PL", "FR"):
            ##Switch to credit card frame
            xpath = Util.GetXpath({"locate":"add_card_frame"})
            BaseUICore.SwitchToFrame({"frame":xpath, "result": "1"})

        if name:
            ##Input name
            xpath = Util.GetXpath({"locate":"name"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        if card_number:
            ##Input cc number
            xpath = Util.GetXpath({"locate":"cc_number"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":card_number, "result": "1"})

        if expire_date:
            ##Input expire date
            xpath = Util.GetXpath({"locate":"expire_date"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":expire_date, "result": "1"})

        if cvv:
            ##Input cvv
            xpath = Util.GetXpath({"locate":"cvv"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":cvv, "result": "1"})

        ##Card number and cvv dat stuff are in another iframe, need to switch back to main iframe to input the others info
        if Config._TestCaseRegion_ in ("PL", "ES", "FR"):
            BaseUICore.SwitchToOriginalFrame({"result": "1"})
            time.sleep(5)

        if state:
            xpath = Util.GetXpath({"locate":"state"})

            if Config._TestCaseRegion_ == "CL":
                ##Click state dropdown and select state
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click state dropdown", "result": "1"})
                BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": state, "result": "1"})

            else:
                ##Input state
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":state, "result": "1"})

        if city:
            xpath = Util.GetXpath({"locate":"city"})

            if Config._TestCaseRegion_ == "CL":
                ##Click city dropdown and select city
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click city dropdown", "result": "1"})
                BaseUICore.SelectDropDownList({"locate": xpath, "selecttype": "value", "selectkey": city, "result": "1"})
            else:
                ##Input city
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":city, "result": "1"})

        if address:
            ##Input address
            xpath = Util.GetXpath({"locate":"address"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":address, "result": "1"})

        if street:
            ##Input street number
            xpath = Util.GetXpath({"locate":"street"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":street, "result": "1"})

        if postal:
            ##Input postal code
            xpath = Util.GetXpath({"locate":"postal"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":postal, "result": "1"})
            time.sleep(5)

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"done_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click done btn", "result": "1"})

        OK(ret, int(arg['result']), 'CreditCardPage.InputCreditCardInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteCreditCard(arg):
        '''
        DeleteCreditCard : Click to delete a credit card
            Input argu :
                card_number - last four number of credit card
                password - login password
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        card_number = arg['card_number']
        password = arg['password']

        ##Click to delete a credit card
        xpath = Util.GetXpath({"locate":"delete_icon"})
        xpath = xpath.replace("card_number_to_be_replaced", card_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to delete a bank account", "result": "1"})

        ##Input login password
        xpath = Util.GetXpath({"locate":"input_password"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        ##Click confirm delete btn
        xpath = Util.GetXpath({"locate":"finish_btn_disabled"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Button is not clickable.")
        else:
            xpath = Util.GetXpath({"locate":"finish_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click finish btn", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'CreditCardPage.DeleteCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreCreditCard(arg):
        '''
        RestoreCreditCard : Restore to delete credit card
            Input argu :
                card_number - last four number of credit card
                password - login password
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        card_number = arg['card_number']
        password = arg['password']

        ##Delete the credit card if the card exist
        xpath = Util.GetXpath({"locate":"delete_icon"})
        xpath = xpath.replace("card_number_to_be_replaced", card_number)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            CreditCardPage.DeleteCreditCard({"card_number":card_number, "password":password, "result": "1"})

        OK(ret, int(arg['result']), 'CreditCardPage.RestoreCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel for add credit card page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click cancel btn", "result":"1"})

        OK(ret, int(arg['result']), 'CreditCardPage.ClickCancel')

class BankAccountInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserInformation(arg):
        '''
        InputUserInformation : Input user information when create bank account on shopee
                Input argu :
                    city - city name
                    area - area name
                    name - user name
                    personal_id - personal id
                    birth - user birthday
                    address - user address
                    postcode - user postcode
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        city = arg["city"]
        area = arg["area"]
        name = arg["name"]
        personal_id = arg["id"]
        birth = arg["birth"]
        address = arg["address"]
        postcode = arg["postcode"]
        ret = 1

        ##Input user information
        ##Select City
        if city:
            xpath = Util.GetXpath({"locate":"city_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click city drop down menu", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate":"city_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', city)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which city", "result": "1"})

        ##Select Area
        if area:
            xpath = Util.GetXpath({"locate":"area_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click area drop down menu", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate":"area_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', area)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which area", "result": "1"})

        ##Input name
        if name:
            xpath = Util.GetXpath({"locate":"name_input_column"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        ##Input personal id
        if personal_id:
            xpath = Util.GetXpath({"locate":"personal_id_input_column"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":personal_id, "result": "1"})

        ##Input birthday
        if birth:
            xpath = Util.GetXpath({"locate":"birthday_input_column"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":birth, "result": "1"})

        ##Input address
        if address:
            xpath = Util.GetXpath({"locate":"address_input_column"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":address, "result": "1"})

        ##Input postcode
        if postcode:
            xpath = Util.GetXpath({"locate":"postcode"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":postcode, "result": "1"})

        ##Click finish
        xpath = Util.GetXpath({"locate":"submit_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'BankAccountInfoPage.InputUserInformation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBankInformation(arg):
        '''
        InputBankInformation : Input bank information when create bank account on shopee
                Input argu :
                    bank_name - bank name
                    area - area of the bank
                    division - division of the bank
                    account_name - bank account name
                    account_number - bank account number
                    edit - no (normal add account mode) / yes (edit mode)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        bank_name = arg["bank_name"]
        area = arg["area"]
        division = arg["division"]
        account_name = arg["account_name"]
        account_number = arg["account_number"]
        edit = arg["edit"]
        ret = 1

        ##Input bank information
        ##Select Bank name
        if bank_name:
            if edit:
                xpath = Util.GetXpath({"locate":"bank_drop_down_edit"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bank name drop down menu", "result": "1"})
            else:
                xpath = Util.GetXpath({"locate":"bank_drop_down"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bank name drop down menu", "result": "1"})

            time.sleep(1)
            xpath = Util.GetXpath({"locate":"bank_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', bank_name)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which bank name", "result": "1"})

        ##Input area
        if area:
            if Config._TestCaseRegion_ == "ID":
                xpath = Util.GetXpath({"locate":"area_drop_down"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":area, "result": "1"})
            else:
                xpath = Util.GetXpath({"locate":"area_drop_down"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click area drop down menu", "result": "1"})
                time.sleep(1)
                xpath = Util.GetXpath({"locate":"area_choose"})
                xpath_replace = xpath.replace('string_to_be_replaced', area)
                BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which area", "result": "1"})

        ##Select Division/branch name
        if Config._TestCaseRegion_ == "TW":
            xpath = Util.GetXpath({"locate":"division_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click area drop down menu", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate":"division_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', division)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which division", "result": "1"})
        elif Config._TestCaseRegion_ in ("VN", "ID"):
            xpath = Util.GetXpath({"locate":"division_choose"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":division, "result": "1"})

        ##Input account name
        xpath = Util.GetXpath({"locate":"name_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":account_name, "result": "1"})

        ##Input account number
        xpath = Util.GetXpath({"locate":"account_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":account_number, "result": "1"})

        ##Click submit
        xpath = Util.GetXpath({"locate":"submit_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        ##Click confirm
        time.sleep(3)
        xpath = Util.GetXpath({"locate":"confirm_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'BankAccountInfoPage.InputBankInformation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddBankInformationFET(arg):
        '''
        AddBankInformationFET : FET case of adding bank information when create bank account on shopee
                Input argu :
                    bank_name - bank name
                    area - area of the bank
                    name - account name
                    division - account address division
                    account - account number
                    scenario - print the scenario
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        bank_name = arg["bank_name"]
        area = arg["area"]
        division = arg["division"]
        name = arg["name"]
        account = arg["account"]
        scenario = arg["scenario"]
        ret = 1

        ##Input bank information
        if bank_name:
            ##Select Bank name
            xpath = Util.GetXpath({"locate":"bank_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bank name drop down menu", "result": "1"})
            xpath = Util.GetXpath({"locate":"bank_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', bank_name)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which bank name", "result": "1"})

        ##Select Area
        if area:
            if Config._TestCaseRegion_ == "ID":
                xpath = Util.GetXpath({"locate":"area_drop_down"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":area, "result": "1"})
            else:
                xpath = Util.GetXpath({"locate":"area_drop_down"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click area drop down menu", "result": "1"})
                xpath = Util.GetXpath({"locate":"area_choose"})
                xpath_replace = xpath.replace('string_to_be_replaced', area)
                BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which area", "result": "1"})

        ##Select Division
        if Config._TestCaseRegion_ == "TW":
            xpath = Util.GetXpath({"locate":"division_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click area drop down menu", "result": "1"})
            xpath = Util.GetXpath({"locate":"division_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', division)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which division", "result": "1"})
            time.sleep(1)
        else:
            xpath = Util.GetXpath({"locate":"division_choose"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":division, "result": "1"})

        ##Input bank information
        ##Input name
        xpath = Util.GetXpath({"locate":"name_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        ##Input account number
        xpath2 = Util.GetXpath({"locate":"account_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath2, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath2, "string":account, "result": "1"})

        if Config._TestCaseRegion_ not in ("ID", "TH"):
            PaymentButton.ClickFinish({"type":"addbank","result": "1"})
        PaymentButton.ClickSubmit({"result": "1"})

        OK(ret, int(arg['result']), 'BankAccountInfoPage.AddBankInformationFET -> ' + scenario)


class PaymentButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFinish(arg):
        '''
        ClickFinish : Click finish button
            Input argu :
                type - page type of finish button
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click finish button
        xpath = Util.GetXpath({"locate": type + "_finish_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click finish button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickFinish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate":"cancel_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddBankAccount(arg):
        '''
        ClickAddBankAccount : Click to add a bank account
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to add a bank account
        xpath = Util.GetXpath({"locate":"add_bank_account_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to add a bank account", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickAddBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCreditCard(arg):
        '''
        ClickAddCreditCard : Click to add a credit card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to add a credit card
        xpath = Util.GetXpath({"locate":"add_credit_card_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to add a credit card", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickAddCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetDefaultBank(arg):
        '''
        ClickSetDefaultBank : Switch default bank account
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to switch default bank account
        xpath = Util.GetXpath({"locate":"set_default_tag"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Set another bank account as default", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickSetDefaultBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetDefaultCreditCard(arg):
        '''
        ClickSetDefaultCreditCard : Switch default credit card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to switch default credit card
        xpath = Util.GetXpath({"locate":"set_default"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Set another credit card as default", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickSetDefaultCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditBankAccount(arg):
        '''
        ClickEditBankAccount : Click to edit a bank account
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to edit a bank account
        xpath = Util.GetXpath({"locate":"edit_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to delete a bank account", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickEditBankAccount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"payment_label_submit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentButton.ClickSubmit')

class PaymentSafePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEmail(arg):
        '''
        InputEmail : Input email address
                Input argu :
                    email - email address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        email = arg["email"]
        ret = 1

        ##Input email
        xpath = Util.GetXpath({"locate": "email_column"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":email, "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafePage.InputEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWalletPinCode(arg):
        '''
        InputWalletPinCode : Input wallet pin code
                Input argu :
                    pin_code - pin code of wallet
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        pin_code = arg["pin_code"]
        ret = 1

        ##Use javascript to let digital keyboard show
        BaseUICore.ExecuteScript({"script": 'document.getElementsByClassName("digit-keyboard")[0].setAttribute("style", "display: block;")',"result": "1"})
        time.sleep(2)

        ##Click pin code
        for pin in pin_code:
            xpath = Util.GetXpath({"locate": "web_keyboard_number"})
            xpath = xpath.replace("number_to_replaced", pin)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click password", "result": "1"})
            dumplogger.info("Click password %s " % (pin))
            time.sleep(1)

        OK(ret, int(arg['result']), 'PaymentSafePage.InputWalletPinCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetBankVirtualAccount(arg):
        '''
        GetBankVirtualAccount : Get bank virtual account number in safe page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get bank virtual account number
        xpath = Util.GetXpath({"locate":"bank_va_number"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"bank_va_number", "result":"1"})

        OK(ret, int(arg['result']), 'PaymentSafePage.GetBankVirtualAccount -> ' + GlobalAdapter.PaymentE2EVar._BankVANumber_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadReceiptImage(arg):
        '''
        UploadReceiptImage : Upload receipt image in bank transfer info page
                Input argu :
                    file_name - upload image file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Click and upload receipt image
        ##Click upload csv button
        xpath = Util.GetXpath({"locate":"upload_receipt"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click upload image", "result": "1"})

        ##Upload file
        XtFunc.UploadFileWithWindowsAutoIt({"file_name": file_name, "file_type": "png", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'PaymentSafePage.UploadReceiptImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTransferInfo(arg):
        '''
        InputTransferInfo : Input bank transfer receipt information if click upload receipt now
                Input argu :
                    name - user name/receipt name
                    from_bank - bank name bank transfer from
                    to_bank - bank name bank transfer to
                    account - account number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        from_bank = arg["from_bank"]
        to_bank = arg["to_bank"]
        account = arg["account"]
        ret = 1

        ##Input name
        if name:
            xpath = Util.GetXpath({"locate":"name"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        if from_bank:
            ##Input bank
            xpath = Util.GetXpath({"locate":"from_bank"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bank drop down menu", "result":"1"})
            time.sleep(3)
            ##Switch to bank frame
            new_frame = Util.GetXpath({"locate":"new_frame"})
            BaseUICore.SwitchToFrame({"frame":new_frame, "result":"1"})
            xpath = Util.GetXpath({"locate":"bank_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', from_bank)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which bank", "result":"1"})
            time.sleep(1)

        if to_bank:
            ##Input bank
            xpath = Util.GetXpath({"locate":"to_bank"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bank drop down menu", "result":"1"})
            time.sleep(3)
            ##Switch to bank frame
            new_frame = Util.GetXpath({"locate":"new_frame"})
            BaseUICore.SwitchToFrame({"frame":new_frame, "result":"1"})
            xpath = Util.GetXpath({"locate":"bank_choose"})
            xpath_replace = xpath.replace('string_to_be_replaced', to_bank)
            BaseUICore.Click({"method":"xpath", "locate":xpath_replace, "message":"Choose which bank", "result":"1"})
            time.sleep(1)

        if Config._TestCaseRegion_ == "TH":
            ##Input date
            xpath = Util.GetXpath({"locate":"date"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"0020201212", "result":"1"})
            ##Input time
            xpath = Util.GetXpath({"locate":"time"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":" ", "result":"1"})
            BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result":"1"})

        ##Input account
        if account:
            xpath = Util.GetXpath({"locate":"account"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":account, "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafePage.InputTransferInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseManualBankTransferPopup(arg):
        '''
        CloseManualBankTransferPopup : Click manual bank transfer popup dismiss button (ID only)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click manual bank transfer popup dismiss button
        xpath = Util.GetXpath({"locate": "bt_popup_dismiss"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click manual bank transfer popup dismiss button", "result": "1"})
        else:
            dumplogger.info("There's no manual bank transfer popup.")

        OK(ret, int(arg['result']), 'PaymentSafePage.CloseManualBankTransferPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetATMRef1(arg):
        '''
        GetATMRef1 : Get ATM ref1 in safe page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get ATM ref1
        xpath = Util.GetXpath({"locate":"atm_ref1"})
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath, "reservetype":"atm_ref1", "result":"1"})

        OK(ret, int(arg['result']), 'PaymentSafePage.GetATMRef1 -> ' + GlobalAdapter.PaymentE2EVar._ATMRef1_)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWalletVcode(arg):
        '''
        InputWalletVcode : Input vcode for verification
                Input argu :
                   vcode - vcode for phone number verification
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        vcode = arg["vcode"]
        ret = 1

        ##Check vcode popup display or not
        xpath = Util.GetXpath({"locate":"send_vcode_alert"})
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click send vcode alert
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click OK button", "result": "1"})

            ##Check direct to otp page or not
            xpath = Util.GetXpath({"locate":"vcode_input_column"})
            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ##Input Vcode
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":vcode, "result": "1"})

                ##Click finish
                xpath = Util.GetXpath({"locate":"submit_button"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})
                time.sleep(3)

        OK(ret, int(arg['result']), 'PaymentSafePage.InputWalletVcode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCVV(arg):
        '''
        InputCVV : Input cvv for ID credit card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ## Input CVV code before click pay button (for ID, so far)
        xpath = Util.GetXpath({"locate":"cvv_code"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"123", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'PaymentSafePage.InputCVV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectBank(arg):
        '''
        SelectBank : Select Wybierz payment bank
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ## Select Wybierz bank
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"new_frame"})
        BaseUICore.SwitchToFrame({"frame":xpath, "result":"1"})
        ## Drop down Wybierz bank select menu
        xpath = Util.GetXpath({"locate":"wybierz_bank"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Drop down select menu", "result": "1"})

        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        xpath = Util.GetXpath({"locate":"new_frame2"})
        BaseUICore.SwitchToFrame({"frame":xpath, "result":"1"})
        ## Select Alior Bank
        xpath = Util.GetXpath({"locate":"alior_bank"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Select Alior Bank", "result": "1"})

        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafePage.SelectBank')

class PaymentSafeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel btn in credit card intermediate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPay(arg):
        '''
        ClickPay : Click pay btn in credit card intermediate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pay btn
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"pay_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click pay button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickPay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPayAgain(arg):
        '''
        ClickPayAgain : Click pay again in credit card intermediate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click pay again
        xpath = Util.GetXpath({"locate":"pay_again"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click pay again button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickPayAgain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreviousPage(arg):
        '''
        ClickPreviousPage : Click previous page in credit card intermediate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click previuos page btn
        xpath = Util.GetXpath({"locate":"previos_page"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click previuos page button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickPreviousPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStay(arg):
        '''
        ClickStay : Click stay button in credit card intermediate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click stay btn
        xpath = Util.GetXpath({"locate":"stay_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click stay button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickStay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        '''
        ClickAgree : Click agree btn in credit card intermediate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click agree btn
        xpath = Util.GetXpath({"locate":"agree_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click agree button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok btn
                Input argu :
                    channel - payment channel name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        channel = arg['channel']

        ##Click ok button in payment safe page
        xpath = Util.GetXpath({"locate": channel + "_ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadReceipt(arg):
        '''
        ClickUploadReceipt : Click upload receipt now or later button
                Input argu :
                    type -  now / later
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click upload receipt btn
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload receipt {0} button".format(type), "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickUploadReceipt')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit to submit receipt in bank transfer info page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit in bank transfer info page", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackIcon(arg):
        '''
        ClickBackIcon : Click back icon in safe page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back icon
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"back_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back icon in safe page", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickBackIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLeave(arg):
        ''' ClickLeave : Click leave button in payment confirm page
                    Input argu : None
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click leave button in payment confirm page
        xpath = Util.GetXpath({"locate":"leave_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click leave button in payment confirm page", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentSafeButton.ClickLeave')

class PaymentThirdPartyPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputIBankingInfo(arg):
        '''
        InputIBankingInfo : Input ibanking 3PP info
                Input argu :
                    card_number - NCB card number
                    card_date - card date
                    card_holder - card holder name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']
        card_date = arg['card_date']
        card_holder = arg['card_holder']

        ##Input card number
        xpath = Util.GetXpath({"locate":"ncb_card_number"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result":"1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":card_number, "result":"1"})

        ##Input card expired date
        xpath = Util.GetXpath({"locate":"ncb_card_date"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click card expired date", "result":"1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":card_date, "result":"1"})

        ##Input card holder name
        xpath = Util.GetXpath({"locate":"ncb_card_holder"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":card_holder, "result":"1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyPage.InputIBankingInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputIBankingOTP(arg):
        '''
        InputIBankingOTP : Input ibanking otp
                Input argu :
                    otp - otp
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        otp = arg['otp']

        ##Input card number
        xpath = Util.GetXpath({"locate":"otp_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":otp, "result":"1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyPage.InputIBankingOTP')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelIBankingPayment(arg):
        '''
        CancelIBankingPayment : Cancel ibanking payment
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel
        locate = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":locate, "message":"Click cancel", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyPage.CancelIBankingPayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectIbankingStatus(arg):
        '''
        SelectIbankingStatus : Select Ibanking Status
                Input argu :
                    status - 0(success) / 1(fail)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg['status']

        ##Choose code for success or fall (for TH, so far)
        xpath = Util.GetXpath({"locate":"dropdown_code"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click dropdown code to choose", "result": "1"})

        ##Click status
        xpath = Util.GetXpath({"locate":"status"})
        xpath = xpath.replace("status_to_be_replaced", status)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click status button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyPage.SelectIbankingStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CanceliPay88OnlineBanking(arg):
        '''
        ClickCancel : Click cancel btn in ipay88 online banking third party page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result":"1"})
        BaseUILogic.PopupHandle({"handle_action":"accept", "result": "0"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'PaymentThirdPartyPage.CanceliPay88OnlineBanking')

class PaymentThirdPartyButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit btn in credit card/ibanking third party page
                Input argu :
                    type - credit_card / ibanking / bank_transfer(PL WybierzBank)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click submit btn
        xpath = Util.GetXpath({"locate":type + "_submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'PaymentThirdPartyButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel btn in credit card third party page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result":"1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click ibanking confirm
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm
        locate = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.Click({"method":"xpath", "locate":locate, "message":"Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click OK in ibanking transaction page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok
        locate = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":locate, "message":"Click ok", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAgree(arg):
        '''
        ClickAgree : Click ibanking/credit card agree btn
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1

        ##Click agree btn
        xpath = Util.GetXpath({"locate":"agree_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click agree button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyButton.ClickAgree')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnHome(arg):
        '''
        ClickReturnHome : Click ibanking return home btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return home button
        xpath = Util.GetXpath({"locate":"return_home"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return home button", "result": "1"})

        OK(ret, int(arg['result']), 'PaymentThirdPartyButton.ClickReturnHome')
