import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCMediaStock(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCMediaStock Precondition =="
        dumplogger.info("== Setup COPCMediaStock Precondition ==")

    def test_COPCMediaStock01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCMediaStock-01.xml")

    def test_COPCMediaStock02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCMediaStock-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCMediaStock Post condition =="
        dumplogger.info("== Setup COPCMediaStock Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
