﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidReturnRefundMethod.py: The def of this file called by XML mainly.
'''
##import system library
import time
import re
import datetime

##Import framework common library
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import DecoratorHelper
import GlobalAdapter
import XtFunc

##Import android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidReturnRefundButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        ''' ClickSubmit : Click submit return refund button in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click submit return refund button in R/R detail page
        xpath = Util.GetXpath({"locate":"click_submit"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click submit return refund button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm return refund button in R/R detail page
                Input argu :
                    button_type - confirm_return_refund / confirm_shipping / confirm_cancel_return_refund / confirm_question / confirm_continue_with_return
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click confirm return refund button in R/R detail page
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm return refund button in R/R detail page => %s" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVisitShop(arg):
        ''' ClickVisitShop : Click visit shop button in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click visit shop button in R/R detail page
        xpath = Util.GetXpath({"locate":"click_visit_shop"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click visit shop button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickVisitShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel button in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                             0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click cancel button in R/R detail page
        xpath = Util.GetXpath({"locate":"click_cancel"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cancel button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDiscuss(arg):
        ''' ClickDiscuss : Click discuss button in R/R detail page
                Input argu :
                        button_type - chat / history
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click discuss button in R/R detail page
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click discuss button in R/R detail page => %s" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickDiscuss')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseReason(arg):
        ''' ClickChooseReason : Click choose return refund reason in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click choose return refund reason in R/R detail page
        xpath = Util.GetXpath({"locate":"choose_reason_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose return refund reason in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickChooseReason')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReasonOption(arg):
        ''' ClickReasonOption : Click return refund reason option
                Input argu : reason_option - non_received_items / received_wrong_product / not_match_description / incomplete_product
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        reason_option = arg["reason_option"]

        ##Click return refund reason option
        xpath = Util.GetXpath({"locate":reason_option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click return refund reason option => %s" % (reason_option), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickReasonOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseSolution(arg):
        ''' ClickChooseSolution : Click return refund solution in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click choose return refund solution in R/R detail page
        xpath = Util.GetXpath({"locate":"solution_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose return refund solution in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickChooseSolution')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSolutionOption(arg):
        ''' ClickSolutionOption : Click return refund solution option
                Input argu : solution_option - refund_only / return_and_refund
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        solution_option = arg["solution_option"]

        ##Click return refund solution option
        xpath = Util.GetXpath({"locate":solution_option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click return refund solution option => %s" % (solution_option), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickSolutionOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseShipping(arg):
        ''' ClickChooseShipping : Click return refund shipping in R/R detail page
                Input argu : N/A
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click choose return refund shipping in R/R detail page
        xpath = Util.GetXpath({"locate":"shipping_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose return refund shipping in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickChooseShipping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingOption(arg):
        ''' ClickShippingOption : Click return refund shipping option
                Input argu : shipping_option - self_arrange
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        shipping_option = arg["shipping_option"]

        ##Click return refund shipping option
        xpath = Util.GetXpath({"locate":shipping_option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click return refund shipping option => %s" % (shipping_option), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickShippingOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickQuestionMark(arg):
        ''' ClickQuestionMark : Click question mark in request R/R page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click question mark in request R/R page
        xpath = Util.GetXpath({"locate":"question_mark_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click question mark in request R/R page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickQuestionMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewExample(arg):
        ''' ClickViewExample : Click view example in request R/R page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click view example in request R/R page
        xpath = Util.GetXpath({"locate":"view_example_link"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click view example in request R/R page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickViewExample')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLearnMore(arg):
        ''' ClickLearnMore : Click learn more in request R/R page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click learn more in request R/R page
        xpath = Util.GetXpath({"locate":"learn_more_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click learn more in request R/R page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickLearnMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContinueWithReturn(arg):
        '''
        ClickContinueWithReturn : Click continue with return in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click continue with return in R/R detail page
        xpath = Util.GetXpath({"locate":"continue_with_return_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click continue with return in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickContinueWithReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnInstructions(arg):
        '''
        ClickReturnInstructions : Click return instructions in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return instructions in R/R detail page
        xpath = Util.GetXpath({"locate":"return_instructions_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click return instructions in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickReturnInstructions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHowToReturn(arg):
        '''
        ClickHowToReturn : Click how to return
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click how to return
        xpath = Util.GetXpath({"locate":"how_to_return_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click how to return", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickHowToReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAcceptProposal(arg):
        '''
        ClickAcceptProposal : Click accept proposal in discuss page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click accept proposal in discuss page
        xpath = Util.GetXpath({"locate":"accept_proposal_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click accept proposal", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickAcceptProposal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpand(arg):
        '''
        ClickExpand : Click expand button in R/R detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click expand button in R/R detail page
        xpath = Util.GetXpath({"locate":"expand_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click expand button in R/R detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundButton.ClickExpand')


class AndroidReturnRefundPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadReturnRefundPhoto(arg):
        '''
        UploadReturnRefundPhoto : Upload photo in R/R page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to upload image
        xpath = Util.GetXpath({"locate":"add_photo"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})
        time.sleep(2)

        ##Click to upload from album
        xpath = Util.GetXpath({"locate":"from_album"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})
        time.sleep(2)

        ##Click first image
        xpath = Util.GetXpath({"locate":"image"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})
        time.sleep(2)

        ##Click next
        xpath = Util.GetXpath({"locate":"next"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidReturnRefundPage.UploadReturnRefundPhoto')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRefundAmount(arg):
        ''' InputRefundAmount : Input refund amount
                Input argu :
                    amount - R/R amount you want to input
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        amount = arg["amount"]

        ##Input return refund amount
        xpath = Util.GetXpath({"locate":"refund_amount_input_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":amount, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundPage.InputRefundAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReturnRefundDescription(arg):
        ''' InputReturnRefundDescription : Input return/refund description
                Input argu : description - R/R description
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        description = arg['description']

        ##Input R/R description
        xpath = Util.GetXpath({"locate":"desc_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":description, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundPage.InputReturnRefundDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReturnRefundEmail(arg):
        ''' InputReturnRefundEmail : Input return refund email address
                Input argu :
                    email - email you want to input
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        email = arg["email"]

        ##Input return refund email address
        xpath = Util.GetXpath({"locate":"email_field"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":email, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidReturnRefundPage.InputReturnRefundEmail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundProductCheckbox(arg):
        ''' ClickReturnRefundProductCheckbox : Click return refund product checkbox
                Input argu :
                    product_name - product name
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click return refund product checkbox
        xpath = Util.GetXpath({"locate":"product_name_checkbox"})
        xpath_product_name_checkbox = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_product_name_checkbox, "message":"Click return refund product checkbox", "result": "1"})

        ##Record return product name to prepare retry return refund
        GlobalAdapter.OrderE2EVar._ReturnProductNameList_.append(product_name)

        OK(ret, int(arg['result']), 'AndroidReturnRefundPage.ClickReturnRefundProductCheckbox')
