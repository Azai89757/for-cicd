#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminDMSMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchDMSAdmin(arg):
    ''' LaunchDMSAdmin : Launch and go to dms admin (dispute management system)
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1
    db_file = "cs_admin_cookie"
    admin_cookie = {}

    ##If cookie is not stored for the first time, get cookie from db
    if 'cs_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}, {"column": "country", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['cs_admin'] = [admin_cookie]

    ##Get DMS (dispute management system) admin url
    url = "https://cs." + GlobalAdapter.UrlVar._Domain_

    ##Go to DMS (dispute management system) admin
    BaseUICore.SetBrowserCookie({"storage_type":"cs_admin", "result": "1"})
    time.sleep(3)
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchDMSAdmin')

@DecoratorHelper.FuncRecorder
def GoToDmsDisputeDetail(arg):
    ''' GoToDmsDisputeDetail : Go to specific return id dispute detail in DMS (dispute management system)
            Input argu : N/A
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1

    ##Get specific return id dispute detail url from global variable _ReturnID_
    url = "https://cs." + GlobalAdapter.UrlVar._Domain_ + "/dispute/detail/" + GlobalAdapter.OrderE2EVar._ReturnID_

    ##Go to specific return id dispute detail page
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'GoToDmsDisputeDetail')
