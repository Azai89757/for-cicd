##import common library
import sys
import os
import json
import logging
import logging.config
import elasticsearch
import cmreslogging.handlers

##import framework library
import Config

##Define variables
slash = Config.dict_systemslash[Config._platform_]

##Define logger variables
logger_dict = {}
handler_dict = {}
formatter_dict = {}

#########################
##Terminal Log Class
#########################
class TerminalOutput(object):

    def __init__(self):
        self.terminal = sys.stdout
        if os.path.exists(Config._LogFile_):
            self.log = open(Config._LogFile_, "a")
        else:
            self.log = open(Config._LogFile_, "w")

    def __del__(self):
        if self.log:
            self.log.close()

    def flush(self):
        pass

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)


#########################
##Debug Log Function
#########################
def InitialDebugLog(logger_name, logger_type, level=logging.INFO, maxBytes=524280000, backupCount=5):
    ''' InitialDebugLog : Declare log file path and initial log algorithmic
            Input argu :
                logger_name - logger file name (Dump_QATest / Api_QATest)
                logger_type - logger type for logging hanlders (RotatingFileHandler / CMRESHandler)
                level - logger level (logging.DEBUG / logging.INFO / logging.WARNING / logging.ERROR / logging.CRITICAL)
                maxBytes - max bytes of single log file for file size management
                backupCount - back up log file count
            Return code:
                logger - return logger instance
            Note: None
    '''

    ##handlers definition
    handlers_template = {
        'RotatingFileHandler': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': Config._OutputFolderPath_ + slash + logger_name + ".log",
            'formatter': 'standard',
            'level': logging.NOTSET,
            'maxBytes': maxBytes,
            'backupCount': backupCount
            },
        'CMRESHandler': {
            'class': 'cmreslogging.handlers.CMRESHandler',
            'hosts': [{'host': '10.44.107.15', 'port': 9200}],
            'auth_type': cmreslogging.handlers.CMRESHandler.AuthType.NO_AUTH,
            'es_index_name': 'autolog',
            'es_additional_fields': {'Platform': 'Windows', 'Environment': 'Staging'}
            }
        }

    ##Loggers definition
    loggers_template = {
        logger_name: {
            'level': level,
            'handlers': [logger_name]
        }
    }

    ##Assign hanlder based on input parameters
    handler_dict[logger_name] = handlers_template[logger_type]

    ##Assign logger based on input parameters
    logger_dict[logger_name] = loggers_template[logger_name]

    ##Assign dict value to logging config
    logging_config = dict(
                            version = 1,
                            formatters = {'standard': {'format':'%(asctime)s-[%(process)d][%(thread)d]|[%(levelname)s]|[%(filename)s:%(lineno)d][%(funcName)s]|%(message)s'}},
                            handlers = handler_dict,
                            loggers = logger_dict
                            #root = {
                            #            'handlers': ['Dump_QATest'],
                            #            'level': logging.INFO,
                            #            },
                        )

    ##Set debug log config
    logging.config.dictConfig(logging_config)

    ##Return logger instance
    return logging.getLogger(logger_name)
