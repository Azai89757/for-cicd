﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSBaseUILogic.py: The def of this file called by XML mainly.
'''

##Import common library
import sys
import time
import FrameWorkBase
from Config import dumplogger
import Util
import traceback
import GlobalAdapter
import iOSBaseUICore
import DecoratorHelper
from Config import _GeneralWaitingPeriod_, _AverageWaitingPeriod_, _ShortWaitingPeriod_, _InstantWaitingPeriod_

##Import selenium related module
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def iOSGetAndCheckElements(arg):
    ''' iOSGetAndCheckElements : Get the elements and reserve to global variable
            Input argu :
                method - xpath, id, name, classname, css
                locate - path
                reserve_type - which one you would like to reserve
                isfuzzy - Fuzzy comparison or perfect comparison
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    dumplogger.info("Enter iOSGetAndCheckElements")

    ret = 1
    method = arg["method"]
    string = arg["string"]
    locate = arg["locate"]
    isfuzzy = ""

    ##Error handle for variable
    try:
        isfuzzy = arg["isfuzzy"]
    except KeyError:
        isfuzzy = "0"
        dumplogger.exception('KeyError - fuzzy default is 0')
    except:
        isfuzzy = "0"
        dumplogger.exception('Other error - fuzzy default is 0')
        print traceback.print_exc()
    finally:
        dumplogger.info("isfuzzy = %s" % (isfuzzy))

    ##Get elements
    iOSBaseUICore.iOSGetElements({"method":"xpath", "locate":locate, "result": "1"})
    iOSBaseUICore.iOSGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
    ##Check elements
    iOSBaseUICore.iOSParseElements({"keyword":string, "isfuzzy":isfuzzy, "result": "1"})

    OK(ret, int(arg['result']), 'iOSGetAndCheckElements->' + "pass")

@DecoratorHelper.FuncRecorder
def iOSGetAndCompareAttribute(arg):
    ''' iOSGetAndCompareAttribute : Get the elements and compare attribute with expected value
            Input argu :
                method - xpath, id
                locate - path
                attrtype - text, location, get_attribute
                attribute - optional, only if you using 'get_attribute'
                value - expeted value for compare usage
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    ret = 1

    try:
        attribute = arg["attribute"]
    except KeyError:
        attribute = ""
        dumplogger.info('KeyError - attribute default is 0')
    except:
        attribute = ""
        dumplogger.info('Other error - attribute default is 0')
    finally:
        dumplogger.info("attribute = %s" % (attribute))

    method = arg["method"]
    locate = arg["locate"]
    attrtype = arg["attrtype"]
    value = arg["value"]

    ##Get elements
    iOSBaseUICore.iOSGetElements({"method":method, "locate":locate, "result": "1"})
    iOSBaseUICore.iOSGetAttributes({"attrtype":attrtype, "mode":"single", "attribute":attribute, "result": "1"})

    dumplogger.info("Actuall value : %s, Expected value : %s" % (GlobalAdapter.CommonVar._PageAttributes_, value))

    ##Compare if attribute matches value
    if value == GlobalAdapter.CommonVar._PageAttributes_:
        dumplogger.info("value matched!")
    else:
        dumplogger.info("value not matched!")

    OK(ret, int(arg['result']), 'iOSGetAndCompareAttribute')

@DecoratorHelper.FuncRecorder
def iOSMoveAndCheckElement(arg):
    ''' iOSMoveAndCheckElement : Swipe screen until element appears
            Input argu :
                locate - element
                method - xpath, id
                direction - left/right/up/down
                isclick : 1 - won't click locate, just check
                          0 - click locate and check
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    locate_x1 = 0
    locate_y1 = 0

    locate = arg["locate"]
    method = arg["method"]
    direction = arg["direction"]
    isclick = arg["isclick"]

    window_size = iOSBaseUICore._iOSDriver_.get_window_size()

    ret = 1

    try:
        for key,value in window_size.iteritems():
            if key == 'width':
                locate_x1 = int(value)
            elif key == 'height':
                locate_y1 = int(value)

        start_x = int(locate_x1 * 0.5)
        start_y = int(locate_y1 * 0.5)
        end_x = int(locate_x1 * 0.2)
        end_y = int(locate_y1 * 0.2)

    except:
        dumplogger.exception("Encounter error!")

    ##Move Screen
    count = 0

    for count in range(1,6):
        try:
            iOSBaseUICore._iOSDriver_.implicitly_wait(5)
            if method == "id":
                element = WebDriverWait(iOSBaseUICore._iOSDriver_, 5).until(EC.presence_of_element_located((By.ID, (locate))))
            elif method == "xpath":
                element = WebDriverWait(iOSBaseUICore._iOSDriver_, 5).until(EC.presence_of_element_located((By.XPATH, (locate))))
            if isclick == "1":
                element.click()
            dumplogger.info("Element exists at the current screen... will break the loop and go next")
            break

        except:
            dumplogger.info("Element doesn't exist at the current screen after swipe %s times ..start to swipe" % (count))

            if direction == "right":
                iOSBaseUICore.iOSRelativeSwipe({"locate_x":start_x, "locate_y":start_y, "movex":end_x, "movey":start_y, "result": "1"})
            if direction == "left":
                iOSBaseUICore.iOSRelativeSwipe({"locate_x":end_x, "locate_y":start_y, "movex":start_x, "movey":start_y, "result": "1"})
            if direction == "down":
                iOSBaseUICore.iOSRelativeSwipe({"locate_x":start_x, "locate_y":start_y, "movex":start_x, "movey":end_y, "result": "1"})
            if direction == "up":
                iOSBaseUICore.iOSRelativeSwipe({"locate_x":start_x, "locate_y":end_y, "movex":start_x, "movey":start_y, "result": "1"})
            ret = 0

    OK(ret, int(arg['result']), 'iOSMoveAndCheckElement')

@DecoratorHelper.FuncRecorder
def iOSRelativeMove(arg):
    ''' iOSRelativeMove : Swipe screen until element appears
            Input argu :
                direction - left/right/up/down
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    locate_x1 = 0
    locate_y1 = 0
    direction = arg["direction"]

    window_size = iOSBaseUICore._iOSDriver_.get_window_size()

    ret = 1

    if "times" in arg:
        times = arg['times']
    else:
        times = "1"

    try:
        for key,value in window_size.iteritems():
            if key == 'width':
                locate_x1 = int(value)
            elif key == 'height':
                locate_y1 = int(value)

        start_x = int(locate_x1 * 0.5)
        start_y = int(locate_y1 * 0.5)
        end_x = int(locate_x1 * 0.2)
        end_y = int(locate_y1 * 0.2)
    except:
        dumplogger.exception("Encounter error!")

    for count in range(int(times)):
        if direction == "right":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":start_x, "locate_y":start_y, "movex":end_x, "movey":start_y, "result": "1"})
        if direction == "left":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":end_x, "locate_y":start_y, "movex":start_x, "movey":start_y, "result": "1"})
        if direction == "down":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":start_x, "locate_y":start_y, "movex":start_x, "movey":end_y, "result": "1"})
        if direction == "up":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":start_x, "locate_y":end_y, "movex":start_x, "movey":start_y, "result": "1"})

    OK(ret, int(arg['result']), 'iOSRelativeMove')

@DecoratorHelper.FuncRecorder
def iOSDirectMove(arg):
    ''' iOSDirectMove : Direct swipe screen from an initial element position and you can swipe many times
            Input argu :
                locate - initial element for swipe
                direction - left/right/up/down
                times - how many times you want to swipe
            Return code : 1 - success
                        0 - fail
                        -1 - error
    '''
    locate = arg["locate"]
    direction = arg["direction"]
    times = int(arg["times"])
    ret = 1

    ##Get initial element location for reference
    iOSBaseUICore.iOSGetElements({"method":"xpath", "locate":locate, "result": "1"})
    iOSBaseUICore.iOSGetAttributes({"attrtype":"location", "mode":"single", "result": "1"})

    for key,value in GlobalAdapter.CommonVar._PageAttributes_.iteritems():
        if key == 'x':
            locate_x = value

        elif key == 'y':
            locate_y = value

    count = 1

    while count <= times:
        if direction == "right":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":locate_x, "locate_y":locate_y, "movex":"40", "movey":"0", "result": "1"})
        if direction == "left":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":locate_x, "locate_y":locate_y, "movex":"-40", "movey":"0", "result": "1"})
        if direction == "down":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":locate_x, "locate_y":locate_y, "movex":"0", "movey":"40", "result": "1"})
        if direction == "up":
            iOSBaseUICore.iOSRelativeSwipe({"locate_x":locate_x, "locate_y":locate_y, "movex":"0", "movey":"-40", "result": "1"})
        count = count + 1

    OK(ret, int(arg['result']), 'iOSDirectMove')

@DecoratorHelper.FuncRecorder
def iOSGetAndReserveElements(arg):
    ''' GetAndReserveElements : Get the elements and reserve to global list
            Input argu :
                method - method to find element
                locate - path
                reserve_type - which one you would like to reserve
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    locate = arg["locate"]
    reservetype = arg["reservetype"]
    method = arg["method"]
    ret = 1

    try:
        ##Reserve elements
        ##if send ordersn, reservetype must be shop_a_ordersn, shop_b_ordersn, etc..
        if "ordersn" in reservetype:
            ##Get elements and save to global
            iOSBaseUICore.iOSGetElements({"method":method, "locate":locate, "mode":"single", "result": "1"})
            iOSBaseUICore.iOSGetAttributes({"attrtype":"get_attribute", "attribute":"name", "result": "1"})
            ##Check key is in order sn dict
            if reservetype in GlobalAdapter.OrderE2EVar._OrderSNDict_:
                dumplogger.info("%s key already in ordersn dict" % (reservetype))
            else:
                ##Assign key
                GlobalAdapter.OrderE2EVar._OrderSNDict_[reservetype] = []
            GlobalAdapter.OrderE2EVar._OrderSNDict_[reservetype].append(GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info(GlobalAdapter.OrderE2EVar._OrderSNDict_)

        elif reservetype == "count":
            ##Get elements
            iOSBaseUICore.iOSGetElements({"method":method, "locate":locate, "mode":"multi", "result": "1"})
            iOSBaseUICore.iOSGetAttributes({"attrtype":"count", "mode":"multi", "result": "1"})
            GlobalAdapter.GeneralE2EVar._Count_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.GeneralE2EVar._Count_)

        elif reservetype == "top_up_id":
            ##Get elements and save to global
            iOSBaseUICore.iOSGetElements({"method":method, "locate":locate, "mode":"single", "result": "1"})
            iOSBaseUICore.iOSGetAttributes({"attrtype":"get_attribute", "attribute":"name", "mode":"single", "result": "1"})
            GlobalAdapter.PaymentE2EVar._TopUpID_ = GlobalAdapter.CommonVar._PageAttributes_.split()[2]
            dumplogger.info(GlobalAdapter.PaymentE2EVar._TopUpID_)

        elif reservetype == "random_username":
            ##Get elements and save to global
            iOSBaseUICore.iOSGetElements({"method":method, "locate":locate, "result": "1"})
            iOSBaseUICore.iOSGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
            GlobalAdapter.GeneralE2EVar._RandomString_ = GlobalAdapter.CommonVar._PageAttributes_
            dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
    except:
        dumplogger.exception('Other error - fuzzy default is 0')
        ret = -1

    OK(ret, int(arg['result']), 'iOSGetAndReserveElements')

@DecoratorHelper.FuncRecorder
def iOSHandleAlert(arg):
    ''' iOSHandleAlert : Handle system alert
            Input argu :
                type - accept/dismiss/other...
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    dumplogger.info("Enter iOSHandleAlert")

    handle_type = arg["type"]
    ret = 1

    if handle_type == "accept":
        iOSBaseUICore._iOSDriver_.switch_to_alert().accept()
    elif handle_type == "dismiss":
        iOSBaseUICore._iOSDriver_.switch_to_alert().dismiss()

    OK(ret, int(arg['result']), 'iOSHandleAlert')

@DecoratorHelper.FuncRecorder
def iOSGetAndClickElementByCoordinate(arg):
    '''iOSGetAndClickElements : Get and click elements
            Input argu : method - method
                         locate - locate
            Return code: 1 - success
                         0 - fail
                         -1 - error
    '''
    ret = 1
    method = arg['method']
    locate = arg['locate']

    ##Get elements coordinate and click
    iOSBaseUICore.iOSGetElements({"method":method, "locate":locate, "mode":"single", "result": "1"})
    element_location = GlobalAdapter.CommonVar._PageElements_.location
    iOSBaseUICore.iOSClickCoordinates({"x_cor":element_location['x'], "y_cor":element_location['y'], "result": "1"})

    OK(ret, int(arg['result']), 'iOSGetAndClickElementByCoordinate')
