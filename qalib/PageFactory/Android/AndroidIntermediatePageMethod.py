﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidIntermediatePageMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
import Util
import XtFunc
import AndroidBaseUICore
from Config import dumplogger
import DecoratorHelper


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def GoToIntermediatePage(arg):
    ''' GoToIntermediatePage : Go to intermediate page from forbidden zone
            Input argu :
                        itemid - item id
                        shopid - shop id
                        page_type - promotion / affiliate
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    ret = 1

    itemid = arg['itemid']
    shopid = arg['shopid']
    page_type = arg['page_type']

    if page_type == "promotion":
        url = "itemid=" + itemid + "&shopid=" + shopid + "&pageType=1"

    elif page_type == "affiliate":
        url = "itemid=" + itemid + "&shopid=" + shopid + "&pageType=2"

    ##Long press to enter forbidden zone
    xpath = Util.GetXpath({"locate":"long_press_btn"})
    AndroidBaseUICore.AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

    ##Go to RN forbidden zone page
    xpath = Util.GetXpath({"locate":"rn_forbidden_zone_btn"})
    AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click RN forbidden zone button", "result": "1"})

    ##Go to Navigator page
    xpath = Util.GetXpath({"locate":"navigator_btn"})
    AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click navigator button", "result": "1"})

    ##Input Module name
    xpath = Util.GetXpath({"locate":"input_module_name"})
    AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":"INTERMEDIATE_PAGE_PROMOTION", "result": "1"})

    ##Input Parameters
    xpath = Util.GetXpath({"locate":"input_parameters"})
    AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":url, "result": "1"})

    ##Click Navigate button
    xpath = Util.GetXpath({"locate":"navigate_btn"})
    AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click navigate button", "result": "1"})
    time.sleep(5)

    ##Back to last page
    AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"back", "result": "1"})
    time.sleep(5)

    ##Click Navigate button
    xpath = Util.GetXpath({"locate":"navigate_btn"})
    AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click navigate button", "result": "1"})
    time.sleep(5)

    OK(ret, int(arg['result']), 'GoToIntermediatePage')


class AndroidIntermediatePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        ''' ClickBuyNow : Click buy now button
                    Input argu : N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click buy now button
        xpath = Util.GetXpath({"locate":"buy_now_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click buy now button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidIntermediatePageButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductName(arg):
        ''' ClickProductName : Click product name
                    Input argu :
                                product_name - product name
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product name
        xpath = Util.GetXpath({"locate":"product_name"})
        xpath = xpath.replace("product_name_to_be_replaced", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click product name", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidIntermediatePageButton.ClickProductName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirmAge(arg):
        ''' ClickConfirmAge : Click confirm already 18 years old
                    Input argu :
                                N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click confirm already 18 years old
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click confirm already 18 years old", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidIntermediatePageButton.ClickConfirmAge')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClaimVoucher(arg):
        ''' ClickClaimVoucher : Click claim voucher button
                    Input argu :
                                N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click claim voucher button
        xpath = Util.GetXpath({"locate":"claim_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click claim voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidIntermediatePageButton.ClickClaimVoucher')
