import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCart Precondition =="
        dumplogger.info("== Setup THPCCart Precondition ==")

    def test_THPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCCart Post condition =="
        dumplogger.info("== Setup THPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
