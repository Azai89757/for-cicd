﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSOrderMethod.py: The def of this file called by XML mainly.
'''
##Import system library
from datetime import datetime
import re
import time

##Import common library
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import DecoratorHelper
import Config
import GlobalAdapter
import iOSBaseUICore
import iOSBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def iOSSearchProduct(arg):
    ''' iOSSearchProduct : Search keyword and go to the first item's detail page from the search result
            Input argu :
                searchkey - search keyword given
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    search_key = arg["search_key"]

    ret = 1
    ##Click Search bar
    time.sleep(3)
    locate = Util.GetXpath({"locate":"search_bar"})
    iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click Search bar", "result": "1"})

    ##Input Search item
    locate = Util.GetXpath({"locate":"text_search_input_column"})
    iOSBaseUICore.iOSInput({"method":"xpath", "locate":locate, "string":search_key, "result": "1"})

    ##Press Enter key
    time.sleep(1)
    iOSBaseUICore.iOSKeyboardAction({"actiontype":"enter", "result": "1"})

    #Will select the first item in the result
    time.sleep(10)
    locate = Util.GetXpath({"locate":"choose_a_product"})
    iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click product name in the search result", "result": "1"})

    OK(ret, int(arg['result']), 'iOSSearchProduct')


class iOSOrderButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToOrderDetail(arg):
        ''' ClickGoToOrderDetail : Click go to order detail btn after payout by airpay wallet
                Input argu :
                            N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to order detail btn after payout by airpay wallet
        xpath = Util.GetXpath({"locate":"order_detail_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click order detail btn in airpay page", "result":"1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickGoToOrderDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToHomePage(arg):
        '''ClickGoToHomePage : Click home page btn after payout by airpay wallet
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to home page after payout by airpay wallet
        locate = Util.GetXpath({"locate":"home_page_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click home page btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSAirPayButton.ClickGoToHomePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReceivedCoin(arg):
        '''ClickReceivedCoin : Click received coin now btn
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click receive coin button
        locate = Util.GetXpath({"locate":"receive_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"click receive coin button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickReceivedCoin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def iOSArrowBack(arg):
        ''' iOSArrowBack : Click Back(<-) button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click close button
        locate = Util.GetXpath({"locate":"back_icon"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"click close button", "result": "1"})

        time.sleep(3)
        OK(ret, int(arg['result']), 'iOSOrderButton.iOSArrowBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' ClickConfirm : Click confirm button
                Input argu : type - order_received, buyer_rating, seller_rating
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Check confirm button exist
        xpath = Util.GetXpath({"locate":type})
        if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})
        else:
            dumplogger.info("No confirm popup.")
            ret = 0

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        ''' ClickCancel : Click cancel button
                Input argu : type - order_received, buyer_rating, seller_rating
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Check confirm button exist
        xpath = Util.GetXpath({"locate":type})
        if iOSBaseUICore.iOSCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})
        else:
            dumplogger.info("No confirm popup.")
            ret = 0

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ArrowBackFromOrderListPage(arg):
        ''' ArrowBackFromOrderListPage : Click arrow back from order list page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click confirm button
        locate = Util.GetXpath({"locate":"back_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click back button on order list page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ArrowBackFromOrderListPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def iOSClickRejectBuyerCancel(arg):
        ''' iOSClickRejectBuyerCancel : Click reject button on order request cancellation page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click to reject order cancel
        locate = Util.GetXpath({"locate":"request_cancel_order_detail_reject_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click to reject order cancel", "result": "1"})

        ##Click reject in popup window
        locate = Util.GetXpath({"locate":"reject_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click reject in popup window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.iOSClickRejectBuyerCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def iOSClickAcceptBuyerCancel(arg):
        ''' iOSClickAcceptBuyerCancel : Click accept button on order request cancellation page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click to accept order cancel
        locate = Util.GetXpath({"locate":"request_cancel_order_detail_accept_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click to accept order cancel", "result": "1"})

        ##Click accept in popup window
        locate = Util.GetXpath({"locate":"accept_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click accept in popup window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.iOSClickAcceptBuyerCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def iOSClickResponse(arg):
        ''' iOSClickResponse : Click response button
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click respond to order cancel button
        locate = Util.GetXpath({"locate":"reply_icon"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click respond to order cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.iOSClickResponse')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def iOSClickCancelReturn(arg):
        ''' iOSClickCancelReturn : Click cancel return button on order detail
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click order cancel button
        locate = Util.GetXpath({"locate":"cancel_return_refund_request_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click CancelReturn button", "result": "1"})

        ##Click
        locate = Util.GetXpath({"locate":"yes_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click yes", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.iOSClickCancelReturn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderRating(arg):
        ''' ClickOrderRating : Click order rating
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Order rating
        xpath = Util.GetXpath({"locate":"btn_rate"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click Order rating", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickOrderRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderReceived(arg):
        ''' ClickOrderReceived : Click order received on order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click order receive btn
        locate = Util.GetXpath({"locate":"receive_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click received button in order detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickOrderReceived')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRequestCancelOrder(arg):
        ''' ClickRequestCancelOrder : Click request cancel order button on order detail
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click request cancel button
        xpath = Util.GetXpath({"locate":"request_cancel_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click request cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickRequestCancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditAddress(arg):
        ''' ClickEditAddress : Click edit address in order detail page
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click edit address button
        xpath = Util.GetXpath({"locate":"edit_address"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click edit address button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickEditAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddAddress(arg):
        ''' ClickAddAddress : Click add an address in my address page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click add address btn
        locate = Util.GetXpath({"locate": "add_address_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate": locate, "message":"Click add new address", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickAddAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyAddress(arg):
        ''' ClickCopyAddress : Click copy address btn in odp
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click copy address btn
        locate = Util.GetXpath({"locate": "copy_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate": locate, "message":"Click copy address btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickCopyAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelfDeliver(arg):
        ''' ClickSelfDeliver : Click self deliver button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click self deliver
        xpath = Util.GetXpath({"locate":"ship"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click self deliver button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickSelfDeliver')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewRating(arg):
        ''' ClickViewRating : Click view rating
                Input argu : type - shop, buyer
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click view rating
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click view rating:" + type, "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickViewRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePayment(arg):
        ''' ClickChangePayment : Click change payment button in order detail page
                Input argu : type - payment_section / bottom_section
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click change payment button in order detail page
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click change payment button in order detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickChangePayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBankTransferNow(arg):
        ''' ClickBankTransferNow : Click bank transfer now in order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Banktransfer now btn
        xpath = Util.GetXpath({"locate": "bank_transfer_btn"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click bank transfer btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickBankTransferNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeGuarantee(arg):
        ''' ClickShopeeGuarantee : Click Shopee Guarantee in order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click Shopee guarantee icon
        xpath = Util.GetXpath({"locate": "shopee_guarantee"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click Shopee guarantee icon", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickShopeeGuarantee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopeeGuaranteeOptions(arg):
        ''' ClickShopeeGuaranteeOptions : Click Shopee Guarantee options in order list page
                Input argu :
                    action - confirm / learn_more
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Click Shopee guarantee icon
        if action == "confirm":
            iOSOrderButton.ClickConfirm({"type":"confirm_button", "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": action})
            iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click Shopee guarantee icon", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickShopeeGuaranteeOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContactSeller(arg):
        ''' ClickContactSeller : Click Contact seller button in order detail page
                Input argu : seller_name - seller name to contact
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        seller_name = arg["seller_name"]

        ##Click contact btn
        xpath = Util.GetXpath({"locate": "contact_seller_btn"})
        xpath = xpath.replace('seller_name_to_replaced', seller_name)
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click contact btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickContactSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRate(arg):
        ''' ClickRate : Click rate button in order detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click rate btn
        xpath = Util.GetXpath({"locate": "rate_btn"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click rate btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickRate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandOrder(arg):
        ''' ClickExpandOrder : Click expand order button on shop
                Input argu : shop_name - which shop order to expand
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Click contact btn
        xpath = Util.GetXpath({"locate": "expand_order_button"})
        xpath = xpath.replace('seller_name_to_replaced', shop_name)
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click expand order btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickExpandOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaynow(arg):
        ''' ClickPaynow : Click pay now button in order detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click pay btn
        locate = Util.GetXpath({"locate":"pay_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click pay now button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickPaynow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOrder(arg):
        ''' ClickCancelOrder : Click cancel order button on the bottom of order detail page
                Input argu : type - all / single
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click cancel order btn
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click contact btn", "result":"1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickCancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        ''' ClickClose : Click x btn to close
                Input argu : type - bill_info / cancel_panel
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click x btn on cancel panel
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click x btn to close " + type, "result":"1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickCloseCancelPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExpandBillInfo(arg):
        '''ClickExpandBillInfo : Click ＾ btn to expand bill info
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ＾ btn on expand bill info
        xpath = Util.GetXpath({"locate":"expand_arrow"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click to expand bill info", "result":"1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickExpandBillInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyOrderID(arg):
        '''ClickCopyOrderID : Click to copy order id
                Input argu : shop_name - shop name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        shop_name = arg["shop_name"]

        ##Click ＾ btn on expand bill info
        xpath = Util.GetXpath({"locate":"copy_btn"})
        xpath = xpath.replace("shop_name_to_replaced", shop_name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click to copy order id", "result":"1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickCopyOrderID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShop(arg):
        ''' ClickGoShop : Click go to shop in order detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click goto shop btn
        xpath = Util.GetXpath({"locate": "go_shop"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click shop", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickGoShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNote(arg):
        ''' ClickAddNote : Click add note in order detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click add note btn
        xpath = Util.GetXpath({"locate": "add_note"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click add note btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickAddNote')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        ''' ClickOK : Click ok btn
                Input argu : type - ship_ok_btn, add_note_ok, ok, ReturnRefundOK
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click ok btn
        xpath = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefund(arg):
        ''' ClickReturnRefund : Buyer click return refund button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click return btn
        locate = Util.GetXpath({"locate":"order_detail_return_refund_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Buyer click return refund button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmitRating(arg):
        ''' ClickSubmitRating : Click rating submit button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click return btn
        locate = Util.GetXpath({"locate":"submit_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click rating submit button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickSubmitRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefundReason(arg):
        ''' ClickRefundReason : Click refund reason in return/refund page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click refund reason
        xpath = Util.GetXpath({"locate": "choose_reason"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click refund reason", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'iOSOrderButton.ClickRefundReason')


class iOSOrderListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderListProduct(arg):
        '''ClickOrderListProduct : Click order product
                Input argu :
                    number - number of order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg['number']

        ##Click coordinate of first order
        if number == "1":
            iOSBaseUICore.iOSClickCoordinates({"x_cor":"185", "y_cor":"250", "result": "1"})
        elif number == "2":
            iOSBaseUICore.iOSClickCoordinates({"x_cor":"195", "y_cor":"500", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderListPage.ClickOrderListProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchOrderTab(arg):
        ''' SwitchOrderTab : Switch tab in MyPurchase list
                Input argu :
                    type - which progress tab
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click view all
        locate = Util.GetXpath({"locate":"view_all"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click to view all tab page", "result": "1"})
        time.sleep(5)

        ##avoid cancelled tab not in page
        locate = Util.GetXpath({"locate": "toreceive"})
        iOSBaseUICore.iOSClick({"method": "id", "locate": locate, "message": "Click to shipping tab", "result": "1"})
        time.sleep(5)

        ##Click complete to make to return tab show on screen
        if type == "toreturn":
            locate = Util.GetXpath({"locate": "completed"})
            iOSBaseUICore.iOSClick({"method": "id", "locate": locate, "message": "Click to complete tab", "result": "1"})
            time.sleep(5)
        else:
            dumplogger.info("%s tab is on the screen" % (type))

        ##Click status tab
        locate = Util.GetXpath({"locate":type})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click " + type + " tab on status page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderListPage.SwitchOrderTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ContactSellerButton(arg):
        ''' ContactSellerButton : Click contact seller button in order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click contact seller tab
        locate = Util.GetXpath({"locate":"contact_seller_icon"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click contact seller button in order list page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderListing.ContactSellerButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckBuyerRatingButton(arg):
        ''' CheckBuyerRatingButton : Click check buyer rating button in order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click buyer rating btn
        locate = Util.GetXpath({"locate":"check_buyer_rating_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click check buyer rating button in order list page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderListing.CheckBuyerRatingButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSellerRatingButton(arg):
        ''' CheckSellerRatingButton : Click check seller rating button in order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click seller rating btn
        locate = Util.GetXpath({"locate":"check_seller_rating_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click check seller rating button in order list page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderListing.CheckSellerRatingButton')


class iOSOrderDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeIncomeDetails(arg):
        ''' ClickSeeIncomeDetails : Click see income details in order detail page to my income page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click see income details in order detail page to my income page
        xpath = Util.GetXpath({"locate":"see_income_details"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click see income details text", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.ClickSeeIncomeDetails')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderValidDate(arg):
        '''CheckOrderValidDate : Check order valid date
                Input argu :
                    status - status of an order
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Get valid date
        valid_date = XtFunc.GetCurrentDateTime("%d-%m-%Y")

        ##Locate valid time element
        locate = Util.GetXpath({"locate":"valid_time_%s" % (status)})
        iOSBaseUICore.iOSGetElements({"method":"xpath", "locate":locate, "result": "1"})
        iOSBaseUICore.iOSGetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Match order ship by date datetime bigger than current datetime
        match_pattern = r"(.+)\s([0-9-]+)"
        match = re.search(match_pattern, GlobalAdapter.CommonVar._PageAttributes_)
        if match:
            datetime_to_match = match.group(2)
            if datetime_to_match < valid_date:
                dumplogger.error("ship by date must bigger than order date")
                ret = 0
        else:
            dumplogger.info("There's no order ship date found.")
            ret = 0

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.CheckOrderValidTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseReturnRefund(arg):
        ''' ChooseReturnRefund : Choose return/refund
                Input argu : reason - reason
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        reason = arg['reason']

        ##Click to chose reason
        xpath = Util.GetXpath({"locate":"reason"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click to chose reason", "result": "1"})

        ##Click a reason
        xpath = Util.GetXpath({"locate":reason})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click a reason", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.ChooseReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReturnRefundInfo(arg):
        ''' InputReturnRefundInfo : Input return/refund info
                Input argu : reason - reason
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        reason = arg['reason']

        ##Click to chose reason
        xpath = Util.GetXpath({"locate":"reason"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click to chose reason", "result": "1"})
        time.sleep(3)

        ##Click a reason
        xpath = Util.GetXpath({"locate":reason})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click a reason", "result": "1"})
        time.sleep(3)

        ##Click to upload image
        xpath = Util.GetXpath({"locate":"add_photo"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})

        ##Click to upload from album
        xpath = Util.GetXpath({"locate":"from_album"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})

        ##Click first image
        xpath = Util.GetXpath({"locate":"image_check"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})

        ##Click check
        xpath = Util.GetXpath({"locate":"submit"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click check", "result": "1"})

        if reason != "reason1" and reason != "reason2":
            ##Select shipping option
            xpath = Util.GetXpath({"locate":"shipping_option"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click shipping option", "result": "1"})

            time.sleep(5)

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        time.sleep(3)

        ##Swipe down to the button of the page
        iOSBaseUILogic.iOSRelativeMove({"direction":"down", "times":"2", "result": "1"})

        ##Input email
        xpath = Util.GetXpath({"locate":"email"})
        iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":"test@test.com", "result": "1"})

        ##Click submit
        iOSOrderButton.ClickConfirm({"type": "finish_button", "result": "1"})

        ##Click ok
        iOSOrderButton.ClickOK({"type": "ReturnRefundOK", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.InputReturnRefundInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundDetail(arg):
        ''' ClickReturnRefundDetail : Click return refund detail
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click rr detail btn
        xpath = Util.GetXpath({"locate": "return_refund_detail"})
        iOSBaseUICore.iOSClick({"method": "xpath", "locate": xpath, "message": "Click return refund detail", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.ClickReturnRefundDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelReturnRequest(arg):
        ''' CancelReturnRequest : Click cancel return button on order detail
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                             -1 - error
        '''
        ret = 1

        ##Click order cancel return button
        xpath = Util.GetXpath({"locate":"cancel_return_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click Cancel return button", "result": "1"})
        time.sleep(2)

        ##Check ok button exist
        iOSOrderButton.ClickConfirm({"type":"confirm_btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.CancelReturnRequest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRating(arg):
        ''' ClickRating : Click rating button in order list page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click rating btn
        locate = Util.GetXpath({"locate":"give_rating_icon"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click rating button in order list page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.ClickRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CancelOrder(arg):
        ''' CancelOrder : Buyer cancel the order in order detail
                Input argu :
                    order_status - order current status
                    reason - which reason for cancel: reason1, reason2 ... to reason7
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        order_status = arg["order_status"]
        reason = arg['reason']

        ##Click order cancel button
        xpath = Util.GetXpath({"locate":"cancel_order"})
        iOSBaseUILogic.iOSRelativeMove({"direction":"down","times":"3", "result": "1"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"ClickOrderCancel button", "result": "1"})

        if order_status == "topay":
            ##Choose cancel reason
            xpath = Util.GetXpath({"locate":"buyer_cancel_reason_topay"})
            xpath = xpath.replace('reason_to_replace', reason)
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Choose cancel reason", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"buyer_cancel_order_topay"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"ClickOrderCancel confirm", "result": "1"})

        elif order_status == "toship":
            ##Choose cancel reason
            xpath = Util.GetXpath({"locate":"buyer_cancel_reason_toship"})
            xpath = xpath.replace('reason_to_replace', reason)
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Choose cancel reason", "result": "1"})

            ##Click cancel confirm
            xpath = Util.GetXpath({"locate":"buyer_cancel_order_toship"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"ClickOrderCancel confirm", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

            ##Click submit
            time.sleep(10)
            xpath = Util.GetXpath({"locate":"submit"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click submit", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        else:
            dumplogger.error("Not valid order status")
            ret = 0

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.CancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseAddress(arg):
        ''' ChooseAddress : Choose specific address
                Input argu : address_name - address name to be choose
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        address_name = arg["address_name"]
        ret = 1

        ##Choose Address
        locate = Util.GetXpath({"locate":"set_address"})
        locate = locate.replace("address_name_to_replaced", address_name)
        dumplogger.info(locate)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Choose specific address", "result": "1"})

        ##Click confirm
        iOSOrderButton.ClickConfirm({"type":"confirm_button", "result": "1"})

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.ChooseAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDeliverInfo(arg):
        ''' InputDeliverInfo : Fill deliver info
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        length = arg["length"]

        GlobalAdapter.GeneralE2EVar._RandomString_ = XtFunc.GenerateRandomString(int(length), "characters")

        ##TW region need to select self deliver method first, it's different with onther countries
        if Config._TestCaseRegion_ == "TH":
            ##Click shipping
            xpath = Util.GetXpath({"locate":"ship"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click deliver", "result": "1"})
        else:
            ##Input trace number
            xpath = Util.GetXpath({"locate":"shipping_traceno"})
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.GeneralE2EVar._RandomString_, "result": "1"})

            ##Press enter to dismiss keyboard
            iOSBaseUICore.iOSKeyboardAction({"actiontype":"finish", "result": "1"})
            time.sleep(5)

            ##Click shipping
            xpath = Util.GetXpath({"locate":"ship"})
            iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click deliver", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.InputDeliverInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadRatingPhoto(arg):
        ''' UploadRatingPhoto : Upload photo in rating page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click to upload image
        xpath = Util.GetXpath({"locate":"add_photo"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})
        time.sleep(2)

        ##Click to upload from album
        xpath = Util.GetXpath({"locate":"from_album"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})
        time.sleep(2)

        iOSBaseUILogic.iOSHandleAlert({"type":"accept", "result": "1"})

        ##Click first image
        xpath = Util.GetXpath({"locate":"image"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})
        time.sleep(2)

        ##Click next
        xpath = Util.GetXpath({"locate":"next"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click next", "result": "1"})
        time.sleep(2)

        ##Click upload
        xpath = Util.GetXpath({"locate":"upload"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":xpath, "message":"Click upload", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.UploadRatingPhoto')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckDaysToShip(arg):
        '''CheckDaysToShip : Check days to ship time is bigger than current time
                Input argu : N/A
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        date_format = "%d-%m-%Y"
        re_pattern = r"(.+)(\d{2}-\d{2}-\d{4})"

        xpath = Util.GetXpath({"locate":"order_status"})
        iOSBaseUICore.iOSGetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        iOSBaseUICore.iOSGetAttributes({"attrtype":"get_attribute", "attribute":"label", "result": "1"})

        ##Get current date, format: 12-12-2020
        current_date = XtFunc.GetCurrentDateTime(date_format)

        ##Find date string
        match = re.search(re_pattern, GlobalAdapter.CommonVar._PageAttributes_)

        ##Compare date
        if match:
            dts_date = match.group(2)

            if datetime.strptime(current_date, date_format) < datetime.strptime(dts_date, date_format):
                dumplogger.info("dts date is bigger than current date")
            else:
                dumplogger.info("dts date is lesser than current date")
                ret = 0
        else:
            dumplogger.error("Didn't find any string that contains dts")
            ret = 0

        OK(ret, int(arg['result']), 'iOSOrderDetailPage.CheckDaysToShip')
