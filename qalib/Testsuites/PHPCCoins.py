import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class PHPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCCoins Precondition =="
        dumplogger.info("== Setup PHPCCoins Precondition ==")

    def test_PHPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-01.xml")

    def test_PHPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-02.xml")

    def test_PHPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-03.xml")

    def test_PHPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-04.xml")

    def test_PHPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-05.xml")

    def test_PHPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-06.xml")

    def test_PHPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCoins-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCCoins Post condition =="
        dumplogger.info("== Setup PHPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
