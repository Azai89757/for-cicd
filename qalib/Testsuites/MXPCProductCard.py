import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCProductCard Precondition =="
        dumplogger.info("== Setup MXPCProductCard Precondition ==")

    def test_MXPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-01.xml")

    def test_MXPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-02.xml")

    def test_MXPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-03.xml")

    def test_MXPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-04.xml")

    def test_MXPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-05.xml")

    def test_MXPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-06.xml")

    def test_MXPCProductCard07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-07.xml")

    def test_MXPCProductCard08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-08.xml")

    def test_MXPCProductCard09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-09.xml")

    def test_MXPCProductCard10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-10.xml")

    def test_MXPCProductCard11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCard-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCProductCard Post condition =="
        dumplogger.info("== Setup MXPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
