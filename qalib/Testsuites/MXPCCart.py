import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCart Precondition =="
        dumplogger.info("== Setup MXPCCart Precondition ==")

    def test_MXPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCCart Post condition =="
        dumplogger.info("== Setup MXPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
