﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidWelcomePackageMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import Util
import Config
import FrameWorkBase
import DecoratorHelper
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidWelcomePackageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCart(arg):
        '''
        ClickCart : Click cart button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart button
        xpath = Util.GetXpath({"locate":"cart_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Cart Icon", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidWelcomePackageButton.ClickCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart button
        xpath = Util.GetXpath({"locate":"add_to_cart"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add to cart button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidWelcomePackageButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExclusiveDeal(arg):
        '''
        ClickExclusiveDeal : Click exclusive deal button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exclusive deal button
        xpath = Util.GetXpath({"locate":"exclusive_deal"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click exclusive deal button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidWelcomePackageButton.ClickExclusiveDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFreeGift(arg):
        '''
        ClickFreeGift : Click free gift button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exclusive deal button
        xpath = Util.GetXpath({"locate":"free_gift"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click free gift button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidWelcomePackageButton.ClickFreeGift')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click new user zone see more button
                Input argu :
                    title - new user zone title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]

        ##Click see more button
        xpath = Util.GetXpath({"locate":"more_button"})
        xpath = xpath.replace("title_to_be_replaced", title)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see more button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidWelcomePackageButton.ClickSeeMore')
