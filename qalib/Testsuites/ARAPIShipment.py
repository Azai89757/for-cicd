import unittest
from Config import dumplogger
from FrameWorkBase import *


class ARAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIShipment Precondition =="
        dumplogger.info("== Setup ARAPIShipment Precondition ==")

    def test_ARAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/ARAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARAPIShipment Post condition =="
        dumplogger.info("== Setup ARAPIShipment Post condition ==")
