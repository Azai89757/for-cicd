import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class THAndroidMyVoucherPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidMyVoucherPage Precondition =="
        dumplogger.info("== Setup THAndroidMyVoucherPage Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses Espresso driver
        Config._DesiredCaps_['automationName'] = "Espresso"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"th"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_THAndroidMyVoucherPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-01.xml")

    def test_THAndroidMyVoucherPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-02.xml")

    def test_THAndroidMyVoucherPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-03.xml")

    def test_THAndroidMyVoucherPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-04.xml")

    def test_THAndroidMyVoucherPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-05.xml")

    def test_THAndroidMyVoucherPage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-06.xml")

    def test_THAndroidMyVoucherPage07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THAndroidMyVoucherPage-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAndroidMyVoucherPage Post condition =="
        dumplogger.info("== Setup THAndroidMyVoucherPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        #Kill Appium
        KillAppium({"isFail":"0"})
