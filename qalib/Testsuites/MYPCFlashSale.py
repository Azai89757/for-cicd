import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCFlashSale(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCFlashSale Precondition =="
        dumplogger.info("== Setup MYPCFlashSale Precondition ==")

    def test_MYPCFlashSale01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCFlashSale-01.xml")

    def test_MYPCFlashSale02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCFlashSale-02.xml")

    def test_MYPCFlashSale03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCFlashSale-03.xml")

    def test_MYPCFlashSale04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCFlashSale-04.xml")

    def test_MYPCFlashSale05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCFlashSale-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCFlashSale Post condition =="
        dumplogger.info("== Setup MYPCFlashSale Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
