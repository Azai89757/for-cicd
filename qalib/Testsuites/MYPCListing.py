import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCListing Precondition =="
        dumplogger.info("== Setup MYPCListing Precondition ==")

    def test_MYPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCListing-01.xml")

    def test_MYPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCListing-02.xml")

    def test_MYPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCListing Post condition =="
        dumplogger.info("== Setup MYPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
