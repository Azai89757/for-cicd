import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCMePage Precondition =="
        dumplogger.info("== Setup ARPCMePage Precondition ==")

    def test_ARPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCMePage Post condition =="
        dumplogger.info("== Setup ARPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
