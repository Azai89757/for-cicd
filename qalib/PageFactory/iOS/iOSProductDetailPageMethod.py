﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSProductDetailPageMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
from Config import dumplogger
import Util
import XtFunc
import iOSBaseUICore
import iOSBaseUILogic
import DecoratorHelper

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class iOSProductDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseProductChoosePanel(arg):
        ''' CloseProductChoosePanel : Close product choose panel
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        iOSBaseUICore.iOSClickCoordinates({"x_cor":"150", "y_cor": "120", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.CloseProductChoosePanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackIcon(arg):
        ''' ClickBackIcon : Click back on product detail page
                Input argu :N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        locate = Util.GetXpath({"locate":"back_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click back", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickBackIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLikeProduct(arg):
        ''' ClickLikeProduct : Click like product in product detail page
                Input argu :
                    N/A

                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        click_type = arg["type"]

        ##Click like btn on product detail page
        if click_type == "like":
            locate = Util.GetXpath({"locate":"like_btn"})
        else:
            locate = Util.GetXpath({"locate":"unlike_btn"})

        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click " + click_type + " btn on product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickLikeProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchBackToVideoImage(arg):
        ''' SwitchBackToVideoImage : Switch back to product video by clicking btn on the left-hand side of product image
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click the video label on the left hand side of the image
        iOSBaseUICore.iOSClickCoordinates({"x_cor":20, "y_cor":230, "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.SwitchBackToVideoImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToRatingPageFromPDP(arg):
        ''' GoToRatingPageFromPDP : Click star to get to rating page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click star in order to go to rating page from pdp
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"rating_star", "threshold":"0.9", "is_click":"yes", "result": "1"})
        OK(ret, int(arg['result']), 'iOSProductDetailPage.GoToRatingPageFromPDP')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectModel(arg):
        ''' ClickSelectModel : Click select model btn in product detail page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click select model block
        locate = Util.GetXpath({"locate":"go_to_select_model"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click select model btn in product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickSelectModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectModel(arg):
        ''' SelectModel : Select model in product detail page
                Input argu : model - product model
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        model = arg["model"]

        ##Select model
        locate = Util.GetXpath({"locate":"model"}).replace("model_name_to_be_replace", model)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Select model in product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.SelectModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemQuantity(arg):
        ''' SelectItemQuantity : Select item quantity in product detail page
                Input argu : type - add or dec
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        select_type = arg["type"]
        times = arg["times"]

        ##Select item quantity
        locate = Util.GetXpath({"locate":select_type})
        for click in range(int(times)):
            iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click to " + select_type + " item quantity", "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'iOSProductDetailPage.SelectItemQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetVoucher(arg):
        ''' ClickGetVoucher : Click get voucher
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check get voucher
        locate = Util.GetXpath({"locate":"get_shop_voucher"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click get voucher in product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickGetVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseGetVoucherPanel(arg):
        ''' CloseGetVoucherPanel : Click to close get voucher panel
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check close
        locate = Util.GetXpath({"locate":"close_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click close voucher in product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.CloseGetVoucherPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DirectBuy(arg):
        ''' DirectBuy : Click Direct Buy button on product detail page
                Input argu :N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        time.sleep(1)
        ##Click Direect Buy button on detail page
        locate = Util.GetXpath({"locate":"direct_buy_button"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Direct Buy button on product detail page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.DirectBuy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        ''' ClickChatIcon : Click chat icon
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check if chat icon exists
        locate = Util.GetXpath({"locate":"chat_icon"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click chat icon", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickChatIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMoreProductInfo(arg):
        ''' ClickSeeMoreProductInfo : Click see more product info
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click see more product info
        locate = Util.GetXpath({"locate":"see_more"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click see more btn", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickSeeMoreProductInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddProductToCart(arg):
        ''' AddProductToCart : Click add product to cart
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click add product to cart
        locate = Util.GetXpath({"locate":"add_cart"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click add to cart", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.AddProductToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ShareProductLink(arg):
        ''' ShareProductLink : Click share link btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check if chat icon exists
        locate = Util.GetXpath({"locate":"share_link_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click share link icon", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ShareProductLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToCart(arg):
        ''' GoToCart : Click cart icon in top bar of pdp
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check if chat icon exists
        locate = Util.GetXpath({"locate":"cart_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click cart icon in top bar", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.GoToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDownMenu(arg):
        ''' ClickDropDownMenu : Click menu icon on top bar
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Check menu icon
        locate = Util.GetXpath({"locate":"menu_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click menu icon in top bar", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickDropDownMenu')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReportProduct(arg):
        ''' ClickReportProduct : Click report product
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click report product
        locate = Util.GetXpath({"locate":"report"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click report product", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickReportProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToHomePage(arg):
        ''' ClickGoToHomePage : Click go to home page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click back to home page
        locate = Util.GetXpath({"locate":"home"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click go to home page", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickGoToHomePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatPopupOK(arg):
        ''' ClickChatPopupOK : Click chat popup ok btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ok
        locate = Util.GetXpath({"locate":"ok_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click ok btn on chat popup window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickChatPopupOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingFeePopupOK(arg):
        ''' ClickShippingFeePopupOK : Click shipping fee popup ok btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ok
        locate = Util.GetXpath({"locate":"ok_btn"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click ok btn on shipping fee popup window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickShippingFeePopupOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def Click4hPopupContinue(arg):
        ''' Click4hPopupContinue : Click 4h shipping continue btn
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ok
        locate = Util.GetXpath({"locate":"continue_btn"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click continue btn on 4h shipping popup window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.Click4hPopupContinue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareWithSMS(arg):
        ''' ClickShareWithSMS : Click share with SMS
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ok
        locate = Util.GetXpath({"locate":"sms_icon"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate":locate, "message":"Click share with SMS on share window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickShareWithSMS')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelShareWithSMS(arg):
        ''' ClickCancelShareWithSMS : Click cancel share with SMS
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ok
        locate = Util.GetXpath({"locate":"cancel"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":locate, "message":"Click cancel share with SMS on share window", "result": "1"})

        OK(ret, int(arg['result']), 'iOSProductDetailPage.ClickCancelShareWithSMS')

class iOSSearchResultPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductDetailPage(arg):
        ''' GoToProductDetailPage : Go to product detail page
                Input argu :
                            name - product name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click product detail page
        locate = Util.GetXpath({"locate":"product_card"}).replace("product_name_be_replace", name)
        iOSBaseUICore.iOSClick({"method":"xpath", "locate": locate, "message":"Click product","result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSSearchResultPage.GoToProductDetailPage')

class iOSShopPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackIcon(arg):
        ''' ClickBackIcon : Click back from shop page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click product detail page
        locate = Util.GetXpath({"locate":"back"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate": locate, "message":"Click back form shop page","result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSShopPage.ClickBackIcon')

class iOSRatingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchRatingTab(arg):
        ''' SwitchRatingTab : Switch rating tab
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        rating_type = arg['type']

        ##Click rating tab
        locate = Util.GetXpath({"locate":rating_type})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate": locate, "message":"Click rating type","result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSShopPage.SwitchRatingTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackIcon(arg):
        ''' ClickBackIcon : Click back from rating page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click product detail page
        locate = Util.GetXpath({"locate":"back"})
        iOSBaseUICore.iOSClick({"method":"xpath", "locate": locate, "message":"Click back","result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'iOSShopPage.ClickBackIcon')
