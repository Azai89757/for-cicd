import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCMePage Precondition =="
        dumplogger.info("== Setup CLPCMePage Precondition ==")

    def test_CLPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCMePage Post condition =="
        dumplogger.info("== Setup CLPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
