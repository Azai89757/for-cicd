﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidBundleDealMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper

##Import Android library
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidBundleDealButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShare(arg):
        '''
        ClickShare : Click share button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button
        xpath = Util.GetXpath({"locate":"share_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click share button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickShare')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBack(arg):
        '''
        ClickBack : Click back button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click share button
        xpath = Util.GetXpath({"locate":"back_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click back button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleDealPicture(arg):
        '''
        ClickBundleDealPicture : Click bundle deal picture in bundle deal landing page
                Input argu :
                    product_name - bundle deal product name which you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click bundle deal picture in bundle deal landing page
        xpath = Util.GetXpath({"locate": "bundle_deal_picture"})
        xpath_bundle_deal_picture = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath_bundle_deal_picture, "message": "Click bundle deal picture","result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickBundleDealPicture')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoToCart(arg):
        '''
        ClickGoToCart : Click go to cart
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cart button
        xpath = Util.GetXpath({"locate":"cart_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click cart button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickGoToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click add to cart
        xpath = Util.GetXpath({"locate":"add_to_cart_btn"})
        xpath_add_to_cart_btn = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_add_to_cart_btn, "message":"Click add to cart", "result": "1"})

        ##Sleep to wait add bundle deal product to cart is complete
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStickTopProduct(arg):
        '''
        ClickStickTopProduct : Click stick top product
                Input argu :
                    index - index of product (left to right)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg["index"]

        ##Click stick top product
        xpath = Util.GetXpath({"locate":"stick_top_product_btn"})
        xpath_stick_top_product_btn = xpath.replace("index_to_be_replace", index)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_stick_top_product_btn, "message":"Click stick top product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickStickTopProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectedItemOK(arg):
        '''
        ClickSelectedItemOK : Click selected item ok
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click selected item ok
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click selected item ok", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickSelectedItemOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectedItemGarbage(arg):
        '''
        ClickSelectedItemGarbage : Click selected item garbage
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click selected item garbage
        xpath = Util.GetXpath({"locate":"garbage_icon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click selected item garbage", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickSelectedItemGarbage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupCancel(arg):
        '''
        ClickPopupCancel : Click popup cancel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click popup cancel
        xpath = Util.GetXpath({"locate":"popup_cancel_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click popup cancel", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickPopupCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupRemove(arg):
        '''
        ClickPopupRemove : Click popup remove
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click popup remove
        xpath = Util.GetXpath({"locate":"popup_remove_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click popup remove", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickPopupRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExclamationMark(arg):
        '''
        ClickExclamationMark : Click exclamation mark
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exclamation mark
        xpath = Util.GetXpath({"locate":"exclamation_mark_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click exclamation mark", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickExclamationMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleDealPicture(arg):
        '''
        ClickBundleDealPicture : Click bundle deal picture in bundle deal landing page
                Input argu :
                    product_name - bundle deal product name which you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click bundle deal picture in bundle deal landing page
        xpath = Util.GetXpath({"locate": "bundle_deal_picture"})
        xpath_bundle_deal_picture = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath_bundle_deal_picture, "message": "Click bundle deal picture","result": "1"})

        OK(ret, int(arg['result']), 'AndroidBundleDealButton.ClickBundleDealPicture')


class AndroidBundleDealPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductModel(arg):
        '''
        SelectProductModel : Select product model
                Input argu :
                    model_name - model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        model_name = arg["model_name"]

        ##Click model name
        xpath = Util.GetXpath({"locate":"model_name_btn"})
        xpath_model_name_btn = xpath.replace("model_name_to_be_replace", model_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_model_name_btn, "message":"Click model name", "result": "1"})
        time.sleep(5)

        ##Click add to cart
        xpath = Util.GetXpath({"locate":"add_to_cart_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add to cart", "result": "1"})

        ##Sleep to wait add bundle deal product to cart is complete
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidBundleDealPage.SelectProductModel')
