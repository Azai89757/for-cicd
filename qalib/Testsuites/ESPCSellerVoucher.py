import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCSellerVoucher Precondition =="
        dumplogger.info("== Setup ESPCSellerVoucher Precondition ==")

    def test_ESPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCSellerVoucher-01.xml")

    def test_ESPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCSellerVoucher-02.xml")

    # def test_ESPCSellerVoucher03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/ES/ESPCSellerVoucher-03.xml")

    # def test_ESPCSellerVoucher04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/ES/ESPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCSellerVoucher Post condition =="
        dumplogger.info("== Setup ESPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
