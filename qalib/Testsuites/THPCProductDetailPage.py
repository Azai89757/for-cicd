import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCProductDetailPage Precondition =="
        dumplogger.info("== Setup THPCProductDetailPage Precondition ==")

    def test_THPCProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductDetailPage-01.xml")

    def test_THPCProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductDetailPage-02.xml")

    def test_THPCProductDetailPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductDetailPage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCProductDetailPage Post condition =="
        dumplogger.info("== Setup THPCProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
