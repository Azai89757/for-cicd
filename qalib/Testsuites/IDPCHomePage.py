import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCHomePage Precondition =="
        dumplogger.info("== Setup IDPCHomePage Precondition ==")

    def test_IDPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePage-01.xml")

    def test_IDPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePage-02.xml")

    def test_IDPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCHomePage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCHomePage Post condition =="
        dumplogger.info("== Setup IDPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
