import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COAPIRestorePaymentChannel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIRestorePaymentChannel Precondition =="
        dumplogger.info("== Setup COAPIRestorePaymentChannel Precondition ==")

    def test_COAPIRestorePaymentChannel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/COAPIRestorePaymentChannel-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIRestorePaymentChannel Post condition =="
        dumplogger.info("== Setup COAPIRestorePaymentChannel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
