import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class PLPCActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCActivityBanner Precondition =="
        dumplogger.info("== Setup PLPCActivityBanner Precondition ==")

    def test_PLPCActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-01.xml")

    def test_PLPCActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-02.xml")

    def test_PLPCActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-03.xml")

    def test_PLPCActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-04.xml")

    def test_PLPCActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-05.xml")

    def test_PLPCActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-06.xml")

    def test_PLPCActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-07.xml")

    def test_PLPCActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-08.xml")

    def test_PLPCActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCActivityBanner-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCActivityBanner Post condition =="
        dumplogger.info("== Setup PLPCActivityBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
