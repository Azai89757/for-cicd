import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpAccount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpAccount Precondition =="
        dumplogger.info("== Setup TWHttpAccount Precondition ==")

    def test_TWHttpAccount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-01.xml")

    def test_TWHttpAccount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-02.xml")

    def test_TWHttpAccount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-03.xml")

    def test_TWHttpAccount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-04.xml")

    def test_TWHttpAccount05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-05.xml")

    def test_TWHttpAccount06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-06.xml")

    def test_TWHttpAccount07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-07.xml")

    def test_TWHttpAccount08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-08.xml")

    def test_TWHttpAccount09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-09.xml")

    def test_TWHttpAccount10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-10.xml")

    def test_TWHttpAccount11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-11.xml")

    def test_TWHttpAccount12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-12.xml")

    def test_TWHttpAccount13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-13.xml")

    def test_TWHttpAccount14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-14.xml")

    def test_TWHttpAccount15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-15.xml")

    def test_TWHttpAccount16(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-16.xml")

    def test_TWHttpAccount17(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-17.xml")

    def test_TWHttpAccount18(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-18.xml")

    def test_TWHttpAccount19(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-19.xml")

    def test_TWHttpAccount20(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-20.xml")

    def test_TWHttpAccount21(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-21.xml")

    def test_TWHttpAccount22(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-22.xml")

    def test_TWHttpAccount23(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-23.xml")

    def test_TWHttpAccount24(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-24.xml")

    def test_TWHttpAccount25(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-25.xml")

    def test_TWHttpAccount26(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-26.xml")

    def test_TWHttpAccount27(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpAccount-27.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpAccount Post condition =="
        dumplogger.info("== Setup TWHttpAccount Post condition ==")
