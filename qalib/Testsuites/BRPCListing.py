import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCListing Precondition =="
        dumplogger.info("== Setup BRPCListing Precondition ==")

    def test_BRPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCListing-01.xml")

    def test_BRPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCListing-02.xml")

    def test_BRPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCListing-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCListing Post condition =="
        dumplogger.info("== Setup BRPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
