import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCCart Precondition =="
        dumplogger.info("== Setup MYPCCart Precondition ==")

    def test_MYPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCCart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCCart Post condition =="
        dumplogger.info("== Setup MYPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
