#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminProductCollectionMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import re
import time

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminItemRuleSetPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerate(arg):
        '''
        ClickGenerate : Click Generate Group & Inside Collections button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Generate Group & Inside Collections button
        xpath = Util.GetXpath({"locate": "generate_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Generate button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminItemRuleSetPageButton.ClickGenerate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerateGroup(arg):
        '''
        ClickGenerateGroup : Click Generate Group & Inside Collections button
                Input argu :
                    mode - confirm/cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mode = arg["mode"]

        ##Click Generate Group & Inside Collections button
        xpath = Util.GetXpath({"locate": "generate_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Generate Group & Inside Collections button", "result": "1"})
        time.sleep(5)

        xpath = Util.GetXpath({"locate": mode})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + mode + " button after clicking generate", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleSetPageButton.ClickGenerateGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleSetPageButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickView(arg):
        '''
        ClickView : Click view button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view button
        xpath = Util.GetXpath({"locate": "view"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleSetPageButton.ClickView')


class AdminItemRuleSetPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDropdownPage(arg):
        '''
        SelectDropdownPage : Select dropdown page and select the items displayed on each page
                Input argu :
                    items_per_page - select items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        items_per_page = arg['items_per_page']

        ##Click dropdown page button
        xpath = Util.GetXpath({"locate": "dropdown_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown page button", "result": "1"})

        ##Click items per page button
        xpath = Util.GetXpath({"locate": "items_per_page"})
        xpath = xpath.replace("string_to_be_replaced", items_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click items per page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleSetPage.SelectDropdownPage')


class AdminItemRuleEditPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelIcon(arg):
        '''
        ClickCancelIcon : Click cancel icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel icon
        xpath = Util.GetXpath({"locate": "cancel"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPageButton.ClickCancelIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerate(arg):
        '''
        ClickGenerate : Click go to generate btn
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to generate btn
        xpath = Util.GetXpath({"locate": "generate"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to generate btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPageButton.ClickGenerate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropDownList(arg):
        '''
        ClickDropDownList : Click drop down list
                Input argu :
                    type - campaign / session / product_label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click drop down list
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list: " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPageButton.ClickDropDownList')


class AdminItemRuleEditPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRuleNameFET(arg):
        '''
        InputRuleNameFET : Input rule name FET
                Input argu :
                    rule_name - rule name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_name = arg["rule_name"]

        ##Input rule name
        xpath = Util.GetXpath({"locate": "rule_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rule_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputRuleNameFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDescriptionFET(arg):
        '''
        InputDescriptionFET : Input description FET
                Input argu :
                    description - description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        description = arg["description"]

        ##Input description
        xpath = Util.GetXpath({"locate": "description"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputDescriptionFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemSource(arg):
        '''
        SelectItemSource : Select item source
                Input argu :
                    option - camapign_management / product_label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select item source
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item source: " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.SelectItemSource')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCampaignName(arg):
        '''
        SelectCampaignName : Select campaign name
                Input argu :
                    campaign - campaign name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign = arg["campaign"]

        ##Select campaign name
        xpath = Util.GetXpath({"locate": "campaign"})
        xpath = xpath.replace("string_to_be_replaced", campaign)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click campaign name: " + campaign, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.SelectCampaignName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCampaignName(arg):
        '''
        InputCampaignName : Input campaign name
                Input argu :
                    campaign - campaign name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        campaign = arg["campaign"]

        ##Click campaign name drop down list
        AdminItemRuleEditPageButton.ClickDropDownList({"type": "campaign", "result": "1"})

        ##Input campaign name
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": campaign, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputCampaignName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSessionName(arg):
        '''
        InputSessionName : Input session name
                Input argu :
                    session - session name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        session = arg["session"]

        ##Click session name drop down list
        AdminItemRuleEditPageButton.ClickDropDownList({"type": "session", "result": "1"})

        ##Input session name
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": session, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputSessionName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectLabelName(arg):
        '''
        SelectLabelName : Select label name
                Input argu :
                    label - label name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        label = arg["label"]

        ##Select campaign name
        xpath = Util.GetXpath({"locate": "label"})
        xpath = xpath.replace("string_to_be_replaced", label)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click label name: " + label, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.SelectLabelName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLabelName(arg):
        '''
        InputLabelName : Input label name
                Input argu :
                    label - label name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        label = arg["label"]

        ##Click session name drop down list
        AdminItemRuleEditPageButton.ClickDropDownList({"type": "product_label", "result": "1"})

        ##Input label name
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": label, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputLabelName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPriceRange(arg):
        '''
        InputPriceRange : Input price range
                Input argu :
                    min_value - min value
                    max_value - max value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        min_value = arg["min_value"]
        max_value = arg["max_value"]

        ##Input min
        if min_value:
            xpath = Util.GetXpath({"locate": "min_price"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_value, "result": "1"})

        ##Input max
        if max_value:
            xpath = Util.GetXpath({"locate": "max_price"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputPriceRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDiscountPercentageRange(arg):
        '''
        InputDiscountPercentageRange : Input discount percentage range
                Input argu :
                    min_value - min value
                    max_value - max value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        min_value = arg["min_value"]
        max_value = arg["max_value"]

        ##Input min
        if min_value:
            xpath = Util.GetXpath({"locate": "min_percentage"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_value, "result": "1"})

        ##Input max
        if max_value:
            xpath = Util.GetXpath({"locate": "max_percentage"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputDiscountPercentageRange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBrand(arg):
        '''
        InputBrand : Input brand id
                Input argu :
                    brand_id - brand id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        brand_id = arg["brand_id"]

        ##Input brand id
        xpath = Util.GetXpath({"locate": "brand"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": brand_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputBrand')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCategory(arg):
        '''
        InputCategory : Input category
                Input argu :
                    category_id - category id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        category_id = arg["category_id"]

        ##Input category id
        xpath = Util.GetXpath({"locate": "category"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": category_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.InputCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectRating(arg):
        '''
        SelectRating : Select rating
                Input argu :
                    rating_stars - 5_stars / 4_stars / 3_stars / 2_stars / 1_stars / no_limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rating_stars = arg["rating_stars"]

        ##Select rating
        xpath = Util.GetXpath({"locate": rating_stars})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select rating: " + rating_stars, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.SelectRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectLeaveOption(arg):
        '''
        SelectLeaveOption : Select leave option
                Input argu :
                    option - cancel / leave
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select leave option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.SelectLeaveOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectFieldsLimit(arg):
        '''
        SelectFieldsLimit : Select fields limit
                Input argu :
                    field - field
                    main_toggle - has_limit / no_limit
                    sub_toggle - sub toggle
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field = arg["field"]
        main_toggle = arg["main_toggle"]
        sub_toggle = arg["sub_toggle"]

        ##Select field's main toggle
        xpath = Util.GetXpath({"locate": main_toggle})
        xpath = xpath.replace("string_to_be_replaced", field)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select " + field + " , and click " + main_toggle, "result": "1"})

        ##Select sub toggle
        if sub_toggle:
            xpath = Util.GetXpath({"locate": "sub_toggle"})
            xpath = xpath.replace("string_to_be_replaced", sub_toggle)
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleEditPage.SelectFieldsLimit')


class AdminItemRuleGeneratePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerateGroupAndInsideCollections(arg):
        '''
        ClickGenerateGroupAndInsideCollections : Click generate group and inside collections
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click generate group and inside collections
        xpath = Util.GetXpath({"locate": "generate_group_and_inside_collections"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click generate group and inside collections", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.ClickGenerateGroupAndInsideCollections')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToItemRuleInfo(arg):
        '''
        ClickBackToItemRuleInfo : Click back to item rule info
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to item rule info
        xpath = Util.GetXpath({"locate": "back_to_item_rule_info"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to item rule info", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.ClickBackToItemRuleInfo')


class AdminItemRuleGeneratePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectGenerateMethod(arg):
        '''
        SelectGenerateMethod : Select generate method
                Input argu :
                    main_generate_method - by_categories / by_shops
                    sub_generate_method - L1_categories / L2_categories / L3_categories / all_shops
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        main_generate_method = arg["main_generate_method"]
        sub_generate_method = arg["sub_generate_method"]

        ##Select main generate method
        xpath = Util.GetXpath({"locate": main_generate_method})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        ##Select sub generate method
        if sub_generate_method:
            xpath = Util.GetXpath({"locate": sub_generate_method})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.SelectGenerateMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDataUpdatingMethod(arg):
        '''
        SelectDataUpdatingMethod : Select data updating method
                Input argu :
                    method - one_off_generating / auto_updating
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method = arg["method"]

        ##Select data updating method
        xpath = Util.GetXpath({"locate": method})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.SelectDataUpdatingMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectGeneratePopupOption(arg):
        '''
        SelectGeneratePopupOption : Select generate popup option
                Input argu :
                    option - cancel / confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Select generate popup option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select generate popup option: " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.SelectGeneratePopupOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectEndTime(arg):
        '''
        SelectEndTime : Select end time
                Input argu :
                    end_year - end year
                    end_month - end month
                    end_date - end date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_year = arg["end_year"]
        end_month = arg["end_month"]
        end_date = arg["end_date"]

        ##Select end year
        if end_year:
            xpath = Util.GetXpath({"locate": "edit_end_year"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit end year", "result": "1"})
            xpath = Util.GetXpath({"locate": "select_end_year"})
            xpath = xpath.replace("year_to_be_replaced", end_year)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select end year", "result": "1"})

        ##Select end month
        if end_month:
            xpath = Util.GetXpath({"locate": "edit_end_month"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit end month", "result": "1"})
            xpath = Util.GetXpath({"locate": "select_end_month"})
            xpath = xpath.replace("month_to_be_replaced", end_month)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select end month", "result": "1"})

        ##Select end date
        if end_date:
            xpath = Util.GetXpath({"locate": "select_end_date"})
            xpath = xpath.replace("date_to_be_replaced", end_date)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select end date", "result": "1"})

        ##Select current end date
        else:
            xpath = Util.GetXpath({"locate": "current_end_date"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select current end date", "result": "1"})

        ##Click ok
        xpath = Util.GetXpath({"locate": "ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.SelectEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectStatus(arg):
        '''
        SelectStatus : Select status
                Input argu :
                    status - all / pending / processing / succeed / failed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Select status
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status: " + status, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.SelectStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGroupID(arg):
        '''
        ClickGroupID : Click Group ID
                Input argu :
                    group_id - group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_id = arg["group_id"]

        ##Click Group ID
        xpath = Util.GetXpath({"locate": "group_id"})
        xpath = xpath.replace("group_id_to_be_replaced", group_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Group ID: " + group_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminItemRuleGeneratePage.ClickGroupID')


class AdminProductCollectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DoubleClick(arg):
        '''
        DoubleClick : Double Click button
                Input argu :
                    button_name - button to be clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]

        ##Get button location and double click button
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.DoubleClick({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})

        ##Check if popup message appear
        time.sleep(15)
        if BaseUILogic.DetectPopupWindow():
            dumplogger.info("popup found!")
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "1"})
        else:
            dumplogger.info("no popup found!")

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.DoubleClick')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCorner(arg):
        '''
        ClickCorner : Click on corner of button
                Input argu :
                    button_name - button to be clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]

        ##Get button location
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "location", "mode": "single", "result": "1"})
        location = GlobalAdapter.CommonVar._PageAttributes_

        ##Click element by offset
        BaseUICore.DragAndDropByOffset({"drag_elem": xpath, "x_offset": location['x'] + 1, "y_offset": location['y'] + 1, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.ClickCorner')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HoldAndSwipe(arg):
        '''
        HoldAndSwipe : Hold button and swipe
                Input argu :
                    button_name - button to be clicked
                    target_name - target to be dragged to
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]
        target_name = arg["target_name"]

        ##Get element to be dragged and target elemment to be dragged to xpath
        drag_elem_xpath = Util.GetXpath({"locate": button_name})
        target_elem_xpath = Util.GetXpath({"locate": target_name})

        ##Drag element to target element
        BaseUICore.DragAndDrop({"drag_elem": drag_elem_xpath, "target_elem": target_elem_xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.HoldAndSwipe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click go to next page
                Input argu :
                    tab - collection_info / item_info / item_rule_list_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click go to next page button
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to next page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrevPage(arg):
        '''
        ClickPrevPage : Click go to previous page
                Input argu :
                    tab - collection_info / item_info / item_rule_list_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click go to previous page button
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to previous page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.ClickPrevPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToCollectionList(arg):
        '''
        ClickBackToCollectionList : Click back to collection list button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to collection list page
        xpath = Util.GetXpath({"locate": "back_to_collection_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to collection list button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.ClickBackToCollectionList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        ##If filter still exist, press again
        xpath = Util.GetXpath({"locate": "pclp_body"})

        if not BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": int(arg['result'])}):
            xpath = Util.GetXpath({"locate": "search_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionButton.ClickSearch')


class AdminProductCollectionCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckElementStyle(arg):
        '''
        CheckElementStyle : Check element style
                Input argu :
                    locate - element xpath
                    style - element style
                    attribute - element attribute
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        element = arg["element"]
        style = arg['style']
        attribute = arg['attribute']

        ##Check element style attribut
        xpath = Util.GetXpath({"locate": element})
        BaseUICore.GetAndCheckValueOfCSSProperty({"locate": xpath, "style": style, "attribute": attribute, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.CheckElementStyle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HoverOverButton(arg):
        '''
        HoverOverButton : Hover over button and check button style changed with corresponding attribute
                Input argu :
                    button_name - button element to be compared
                    style - element style to be compared
                    attribute - element style corresponding attribute to be compared
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg['button_name']
        style = arg['style']
        attribute = arg['attribute']

        ##Hover on element
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.CheckMouseHover({"locate": xpath, "style": style, "attribute": attribute, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.HoverOverButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPageNumber(arg):
        '''
        GoToPageNumber : Go to page number
                Input argu :
                    page_number - page number
                    page_number_input_tab - collection_info_tab / item_info_tab / item_rule_list_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]
        page_number_input_tab = arg["page_number_input_tab"]

        ##Page number
        xpath = Util.GetXpath({"locate": page_number_input_tab})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_number, "result": "1"})
        time.sleep(1)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.GoToPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckResultCount(arg):
        '''
        CheckResultCount : Check result total count match item rows
                Input argu :
                    row_element - rows in table
                    last_page_btn - last page button
                    items_per_page - items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row_element = arg['row_element']
        items_per_page = arg['items_per_page']
        last_page_btn = arg['last_page_btn']

        ##Get the number of page
        xpath = Util.GetXpath({"locate": row_element})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            last_page_row_count = GlobalAdapter.GeneralE2EVar._Count_
        else:
            ret = 0
            dumplogger.info("No row found on page %s" % (xpath))

        ##Count number of data row
        records_count = GlobalAdapter.ProductCollectionE2EVar._RecordsCount_
        dumplogger.info("Total records : %s" % (records_count))
        dumplogger.info("Last page records : %s" % (last_page_row_count))
        dumplogger.info("Expected last page records : %f" % (int(records_count) % int(items_per_page)))

        ##get page count
        xpath = Util.GetXpath({"locate": last_page_btn})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        page_count = GlobalAdapter.CommonVar._PageAttributes_
        dumplogger.info("Page Count : %s" % (page_count))

        if int(records_count) == (int(page_count) - 1) * int(items_per_page) + int(last_page_row_count):
            ret = 1
        else:
            ret = 0

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.CheckResultCount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRowCount(arg):
        '''
        CheckRowCount : Check row count of table
                Input argu :
                    row_element - rows in table
                    items_per_page - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row_element = arg['row_element']
        items_per_page = arg["items_per_page"]

        ##Count number of data row
        xpath = Util.GetXpath({"locate": row_element})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            number = GlobalAdapter.GeneralE2EVar._Count_
            dumplogger.info("Selected items per page : %s, rows found on page : %s" % (items_per_page, number))

            ##Check if row number match selected items per page
            if str(number) == items_per_page:
                ret = 1
            else:
                ret = 0
        else:
            ret = 0
            dumplogger.info("No row found on page %s" % (row_element))

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.CheckRowCount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLastPageNumberOfRecords(arg):
        '''
        CheckLastPageNumberOfRecords : Check last page number of records logic
                Input argu :
                    row_element - rows in table
                    items_per_page - items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row_element = arg['row_element']
        items_per_page = arg["items_per_page"]

        ##Get the number of page
        xpath = Util.GetXpath({"locate": row_element})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            last_page_row_count = GlobalAdapter.GeneralE2EVar._Count_
        else:
            ret = 0
            dumplogger.info("No row found on page %s" % (xpath))

        ##Count number of data row
        records_count = GlobalAdapter.ProductCollectionE2EVar._RecordsCount_
        dumplogger.info("Total records : %s" % (records_count))
        dumplogger.info("Items per page : %s" % (items_per_page))
        dumplogger.info("Last page records : %s" % (last_page_row_count))

        if int(last_page_row_count) == int(records_count) % int(items_per_page) or int(last_page_row_count) == int(items_per_page):
            ret = 1
        else:
            ret = 0

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.CheckLastPageNumberOfRecords')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetNumberOfRecords(arg):
        '''
        GetNumberOfRecords : Get records count and store in global
                Input argu :
                    locate_element - element xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_element = arg['locate_element']

        ##Get elements
        xpath = Util.GetXpath({"locate": locate_element})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)

        try:
            GlobalAdapter.ProductCollectionE2EVar._RecordsCount_ = re.findall(r"\d+", GlobalAdapter.CommonVar._PageAttributes_)[0]
            dumplogger.info(GlobalAdapter.ProductCollectionE2EVar._RecordsCount_)
        except:
            GlobalAdapter.ProductCollectionE2EVar._RecordsCount_ = 0
            dumplogger.info("Reocrds count is 0!")

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.GetNumberOfRecords')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CompareElementText(arg):
        '''
        CompareElementText : Compare element text
                Input argu :
                    locate_element - element xpath
                    text - input text to be compared
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_element = arg['locate_element']
        text = arg['text']

        ##Get elements
        xpath = Util.GetXpath({"locate": locate_element})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)

        if text == "product_collection_old_name":
            GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageAttributes_.replace("Name: ", "")

            if GlobalAdapter.ProductCollectionE2EVar._OldCollectionName_ not in GlobalAdapter.CommonVar._PageAttributes_:
                dumplogger.info("product_collection_old_name mismatch: %s, %s" % (GlobalAdapter.ProductCollectionE2EVar._OldCollectionName_, GlobalAdapter.CommonVar._PageAttributes_))
                ret = 0

        elif text == "product_collection_new_name":
            GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageAttributes_.replace("Name: ", "")

            if GlobalAdapter.ProductCollectionE2EVar._NewCollectionName_ not in GlobalAdapter.CommonVar._PageAttributes_:
                dumplogger.info("product_collection_new_name mismatch: %s, %s" % (GlobalAdapter.ProductCollectionE2EVar._NewCollectionName_, GlobalAdapter.CommonVar._PageAttributes_))
                ret = 0

        elif text == "records_found":
            if str(GlobalAdapter.ProductCollectionE2EVar._RecordsCount_) not in GlobalAdapter.CommonVar._PageAttributes_:
                dumplogger.info("records_found count mismatch: %s, %s" % (GlobalAdapter.ProductCollectionE2EVar._RecordsCount_, GlobalAdapter.CommonVar._PageAttributes_))
                ret = 0

        elif text == "product_collection_old_update_time":
            if GlobalAdapter.ProductCollectionE2EVar._UpdateTime_ not in GlobalAdapter.CommonVar._PageAttributes_:
                dumplogger.info("product_collection_old_update_time mismatch: %s, %s" % (GlobalAdapter.ProductCollectionE2EVar._UpdateTime_, GlobalAdapter.CommonVar._PageAttributes_))
                ret = 0

        else:
            if text not in GlobalAdapter.CommonVar._PageAttributes_:
                ret = 0

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.CompareElementText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTimeFormat(arg):
        '''
        InputTimeFormat : Input time format
                Input argu :
                    start_date - start date
                    end_date - end date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_date = arg["start_date"]
        end_date = arg["end_date"]

        ##Input start date
        xpath = Util.GetXpath({"locate": "start_date_box"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": start_date, "result": "1"})
        xpath = Util.GetXpath({"locate": "start_date_select"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select start date", "result": "1"})

        ##Input end date
        xpath = Util.GetXpath({"locate": "end_date_box"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": end_date, "result": "1"})
        time.sleep(5)

        ##Click ok
        xpath = Util.GetXpath({"locate": "ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.InputTimeFormat')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterIcon(arg):
        '''
        ClickFilterIcon : Click on filter icon
                Input argu :
                    filter_type - rule_id / rule_name / update_time / create_time / operator / status / generated_group_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        filter_type = arg["filter_type"]

        ##Click on filter icon
        xpath = Util.GetXpath({"locate": filter_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.ClickFilterIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchBox(arg):
        '''
        InputSearchBox : Input search box
                Input argu :
                    search_type - rule_id / rule_name / last_operator / generated_group_id
                    value - value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        value = arg["value"]

        ##Input value
        xpath = Util.GetXpath({"locate": search_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.InputSearchBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterBoxSearch(arg):
        '''
        ClickFilterBoxSearch : Click on search button
                Input argu :
                    search_type - rule_id / rule_name / last_operator / generated_group_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]

        ##Click on search button
        xpath = Util.GetXpath({"locate": search_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.ClickFilterBoxSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterBoxReset(arg):
        '''
        ClickFilterBoxReset : Click on reset button
                Input argu :
                    search_type - rule_id / rule_name / last_operator / generated_group_id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]

        ##Click on reset button
        xpath = Util.GetXpath({"locate": search_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.ClickFilterBoxReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTimeBox(arg):
        '''
        ClickTimeBox : Click on time box
                Input argu :
                    filter_type - update_time / create_time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        filter_type = arg["filter_type"]

        ##Click on time box
        xpath = Util.GetXpath({"locate": filter_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on time box", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.ClickTimeBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCalendar(arg):
        '''
        EditCalendar : Edit calendar
                Input argu :
                    start_year - start year
                    start_month - start month
                    start_date - start date
                    end_year - end year
                    end_month - end month
                    end_date - end date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_year = arg["start_year"]
        start_month = arg["start_month"]
        start_date = arg["start_date"]
        end_year = arg["end_year"]
        end_month = arg["end_month"]
        end_date = arg["end_date"]

        ##Select start year
        if start_year:
            xpath = Util.GetXpath({"locate": "edit_start_year"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit start year", "result": "1"})
            xpath = Util.GetXpath({"locate": "select_start_year"})
            xpath = xpath.replace("year_to_be_replaced", start_year)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select start year", "result": "1"})

        ##Select start month
        if start_month:
            xpath = Util.GetXpath({"locate": "edit_start_month"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit start month", "result": "1"})
            xpath = Util.GetXpath({"locate": "select_start_month"})
            xpath = xpath.replace("month_to_be_replaced", start_month)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select start month", "result": "1"})

        ##Select start date
        if start_date:
            xpath = Util.GetXpath({"locate": "select_start_date"})
            xpath = xpath.replace("date_to_be_replaced", start_date)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select start date", "result": "1"})

        ##Select current start date
        else:
            xpath = Util.GetXpath({"locate": "current_start_date"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select current start date", "result": "1"})

        ##Select end year
        if end_year:
            xpath = Util.GetXpath({"locate": "edit_end_year"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit end year", "result": "1"})
            xpath = Util.GetXpath({"locate": "select_end_year"})
            xpath = xpath.replace("year_to_be_replaced", start_year)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select end year", "result": "1"})

        ##Select end month
        if end_month:
            xpath = Util.GetXpath({"locate": "edit_end_month"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit end month", "result": "1"})
            xpath = Util.GetXpath({"locate": "select_end_month"})
            xpath = xpath.replace("month_to_be_replaced", end_month)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select end month", "result": "1"})

        ##Select end date
        xpath = Util.GetXpath({"locate": "select_end_date"})
        xpath = xpath.replace("date_to_be_replaced", end_date)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select end date", "result": "1"})

        ##Click ok
        xpath = Util.GetXpath({"locate": "ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.EditCalendar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseIcon(arg):
        '''
        ClickCloseIcon : Click close icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close icon
        xpath = Util.GetXpath({"locate": "close_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.ClickCloseIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToItemRuleList(arg):
        '''
        ClickBackToItemRuleList : Click back to item rule list
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to item rule list
        xpath = Util.GetXpath({"locate": "back"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to item rule list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.ClickBackToItemRuleList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputActiveEndDate(arg):
        '''
        InputActiveEndDate : Input active end date
                Input argu :
                    active_end_date - input active end date
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        active_end_date = arg["active_end_date"]

        ##Click active end date input box
        xpath = Util.GetXpath({"locate": "end_date_input_box"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date input box", "result": "1"})

        ##Input active end date
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.SendSeleniumKeyEvent({"action_type": "string", "string": active_end_date, "result": "1"})

        ##Click ok button in calender
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionCommon.InputActiveEndDate')


class AdminProductCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSetColumnsTitle(arg):
        '''
        CheckSetColumnsTitle : Check set columns title
                Input argu :
                    number - number of total to be compared
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg['number']

        ##Select all column
        xpath = Util.GetXpath({"locate": "set_column_title"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": "Set Columns ( %s / 23 )" % (number), "isfuzzy": "1", "result": "1"})

        else:
            ret = 0
            dumplogger.info("No set columns title !!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.CheckSetColumnsTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAllColumn(arg):
        '''
        SelectAllColumn : Select all column in set column from CMS Admin home page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select all column
        xpath = Util.GetXpath({"locate": "unchecked_column"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})

            ##Select all detected unchecked column
            for select_time in range(int(GlobalAdapter.GeneralE2EVar._Count_)):
                xpath = Util.GetXpath({"locate": "unchecked_column"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select column" + str(select_time + 1), "result": "1"})

        else:
            dumplogger.info("No unchecked column exists !!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.SelectAllColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectSpecificColumn(arg):
        '''
        SelectSpecificColumn : Select specific column in set column from CMS Admin home page
                Input argu :
                    column_name - column name in dropdown menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_name = arg['column_name']

        ##Select specific column
        xpath = Util.GetXpath({"locate": column_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select column " + str(column_name), "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.SelectSpecificColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UnselectSpecificColumn(arg):
        '''
        UnselectSpecificColumn : Unselect specific column in set column from CMS Admin home page
                Input argu :
                    column_name - column name in dropdown menu
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_name = arg['column_name']

        ##Select specific column
        xpath = Util.GetXpath({"locate": column_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select column " + str(column_name), "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.UnselectSpecificColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UnselectAllColumn(arg):
        '''
        UnselectAllColumn : Unselect all column in set column from CMS Admin home page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Unselect all column
        xpath = Util.GetXpath({"locate": "checked_column"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})

            ##Select all detected unchecked column
            for select_time in range(int(GlobalAdapter.GeneralE2EVar._Count_)):
                xpath = Util.GetXpath({"locate": "checked_column"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Unselect column" + str(select_time + 1), "result": "1"})

        else:
            dumplogger.info("No checked column exists!!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.UnselectAllColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCollectionName(arg):
        '''
        ClickProductCollectionName : Click product collection name
                Input argu :
                    name - name of the collection
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click collection name
        xpath = Util.GetXpath({"locate": "collection_name"})
        xpath = xpath.replace("string_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click collection name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ClickProductCollectionName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductCollectionTitle(arg):
        '''
        ClickProductCollectionTitle : Click product collection title
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product collection title
        xpath = Util.GetXpath({"locate": "product_collection_title"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product collection title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ClickProductCollectionTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickColumnTitle(arg):
        '''
        ClickColumnTitle : Click column title
                Input argu :
                    name - name of the title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click collection name
        xpath = Util.GetXpath({"locate": "column_title"})
        xpath = xpath.replace("string_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click column title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ClickColumnTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyColumnPanel(arg):
        '''
        ModifyColumnPanel : Modify column panel
                Input argu :
                    product_overlay_image - product overlay image checkbox
                    banner - banner checkbox
                    top_image - top image checkbox
                    subtitle - subtitle checkbox
                    create_time - create time checkbox
                    updated_time - updated time checkbox
                    last_operator - last operator checkbox
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_overlay_image = arg["product_overlay_image"]
        banner = arg["banner"]
        top_image = arg["top_image"]
        subtitle = arg["subtitle"]
        create_time = arg["create_time"]
        updated_time = arg["updated_time"]
        last_operator = arg["last_operator"]

        ##Click set column btn
        xpath = Util.GetXpath({"locate": "set_column_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set column dropdown", "result": "1"})

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        ##Check if columns need to be checked
        if product_overlay_image:
            xpath = Util.GetXpath({"locate": "product_overlay_image_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show product overlay image column", "result": "1"})

        if banner:
            xpath = Util.GetXpath({"locate": "banner_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show banner column", "result": "1"})

        if top_image:
            xpath = Util.GetXpath({"locate": "top_image_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show top image column", "result": "1"})

        if subtitle:
            xpath = Util.GetXpath({"locate": "subtitle_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show subtitle column", "result": "1"})

        if create_time:
            xpath = Util.GetXpath({"locate": "create_time_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show create time column", "result": "1"})

        if updated_time:
            xpath = Util.GetXpath({"locate": "updated_time_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show updated time column", "result": "1"})

        if last_operator:
            xpath = Util.GetXpath({"locate": "last_operator_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Show last operator column", "result": "1"})

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ModifyColumnPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddProductCollection(arg):
        '''
        AddProductCollection : Add a new collection
                Input argu :
                    name - collection name
                    description - collection description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        description = arg["description"]
        active_end_date = arg["active_end_date"]

        ##Click add new btn
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new", "result": "1"})
        time.sleep(5)

        ##Input name
        xpath = Util.GetXpath({"locate": "name_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        ##Input description
        xpath = Util.GetXpath({"locate": "description_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})
        time.sleep(2)

        ##Input active end time
        AdminProductCollectionCommon.InputActiveEndDate({"active_end_date": active_end_date, "result": "1"})
        time.sleep(2)

        ##Save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
        time.sleep(5)

        ##Get element attribute
        xpath = Util.GetXpath({"locate": "collection_id"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Store collection id in global
        GlobalAdapter.ProductCollectionE2EVar._CollectionID_ = GlobalAdapter.CommonVar._PageAttributes_

        ##Back to list
        xpath = Util.GetXpath({"locate": "back_to_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.AddProductCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteCollection(arg):
        '''
        ClickDeleteCollection : Click delete collection
                Input argu :
                    name - collection name (must enter)
                    description - collection description (must enter)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        description = arg["description"]

        if name and description:

            ##Click filter
            AdminProductCollectionPageButton.ClickFilter({"result": "1"})

            ##Input name
            xpath = Util.GetXpath({"locate": "name_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})
            time.sleep(1)

            ##Input description
            xpath = Util.GetXpath({"locate": "description_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})
            time.sleep(1)

            ##Click search btn
            AdminProductCollectionButton.ClickSearch({"result": "1"})
            time.sleep(10)

            ##Move to more btn and click delete
            xpath = Util.GetXpath({"locate": "more_btn"})
            xpath = xpath.replace("name_to_be_replaced", name)
            xpath = xpath.replace("description_to_be_replaced", description)

            ##Move to the button and click delete
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
            time.sleep(1)
            xpath = Util.GetXpath({"locate": "delete_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete", "result": "1"})

        else:
            ret = 0
            dumplogger.info("Both name and description should be inserted.")

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ClickDeleteCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDropdownPage(arg):
        '''
        SelectDropdownPage : Select Dropdown Page
                Input argu :
                    items_per_page = select items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        items_per_page = arg['items_per_page']

        ##Click dropdown page button
        xpath = Util.GetXpath({"locate": "dropdown_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown page button", "result": "1"})

        ##Click items per page button
        xpath = Util.GetXpath({"locate": "items_per_page"})
        xpath = xpath.replace("string_to_be_replaced", items_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click items per page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.SelectDropdownPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterIDFET(arg):
        '''
        FilterIDFET : Filter ID FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Clear field
        xpath = Util.GetXpath({"locate": "id_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterMenuIDFET(arg):
        '''
        FilterMenuIDFET : Filter ID FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]
        locate_icon = arg["locate_icon"]

        ##Clear field
        xpath = Util.GetXpath({"locate": locate_icon})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterMenuIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterNameFET(arg):
        '''
        FilterNameFET : Filter name FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Clear field
        xpath = Util.GetXpath({"locate": "name_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterNameFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterDescriptionFET(arg):
        '''
        FilterDescriptionFET : Filter description FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Clear field
        xpath = Util.GetXpath({"locate": "description_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterDescriptionFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterActiveStatusFET(arg):
        '''
        FilterActiveStatusFET : Filter active status FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterActiveStatusFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterTotalItemQuantityFET(arg):
        '''
        FilterTotalItemQuantityFET : Filter total item quantity value FET
                Input argu :
                    input_type - input type (min / max)
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": input_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterTotalItemQuantityFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterNormalItemQuantityFET(arg):
        '''
        FilterNormalItemQuantityFET : Filter normal item quantity value FET
                Input argu :
                    input_type - input type (min / max)
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": input_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterNormalItemQuantityFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterPOItemRatioFET(arg):
        '''
        FilterPOItemRatioFET : Filter PO item ratio FET
                Input argu :
                    input_type - input type (min / max)
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": input_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterPOItemRatioFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterCODItemRatioFET(arg):
        '''
        FilterCODItemRatioFET : Filter COD item ratio FET
                Input argu :
                    input_type - input type (min / max)
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": input_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterCODItemRatioFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterSourceRuleFET(arg):
        '''
        FilterSourceRuleFET : Filter Source Rule FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": "source_rule_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterSourceRuleFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterSourceTaskFET(arg):
        '''
        FilterSourceTaskFET : Filter Source Task FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": "source_task_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterSourceTaskFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterHomepageFeatureCollectionFET(arg):
        '''
        FilterHomepageFeatureCollectionFET : Filter homepage feature collection FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterHomepageFeatureCollectionFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterLastOperatorFET(arg):
        '''
        FilterLastOperatorFET : Filter last operator FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": "operator_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterLastOperatorFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterItemIDFET(arg):
        '''
        FilterItemIDFET : Filter item ID FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": "itemid_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterItemIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterShopIDFET(arg):
        '''
        FilterShopIDFET : Filter shop ID FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": "shopid_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterShopIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterRiskProductsFET(arg):
        '''
        FilterRiskProductsFET : Filter risk products FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.FilterRiskProductsFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyFilterInput(arg):
        '''
        ModifyFilterInput : Modify input filter
                Input argu :
                    collection_id - input collection id
                    name - input name
                    description - input description
                    last_operator - last operator
                    feature_collection - feature collection in homepage (true / false)
                    tracking_start_time - tracking start time
                    tracking_end_time - tracking end time
                    create_start_time - create start time
                    create_end_time - create end time
                    item_id - item id
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id = arg["collection_id"]
        name = arg["name"]
        description = arg["description"]
        last_operator = arg["last_operator"]
        feature_collection = arg["feature_collection"]
        tracking_start_time = arg["tracking_start_time"]
        tracking_end_time = arg["tracking_end_time"]
        create_start_time = arg["create_start_time"]
        create_end_time = arg["create_end_time"]
        item_id = arg["item_id"]
        shop_id = arg["shop_id"]

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})
        time.sleep(2)

        ##Get collection_id from Global variable
        if GlobalAdapter.ProductCollectionE2EVar._CollectionID_ and not collection_id:
            collection_id = GlobalAdapter.ProductCollectionE2EVar._CollectionID_

        ##Input collection_id
        if collection_id:
            xpath = Util.GetXpath({"locate": "ID_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collection_id, "result": "1"})
            time.sleep(1)

        ##Input name
        if name:
            xpath = Util.GetXpath({"locate": "name_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})
            time.sleep(1)

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})
            time.sleep(1)

        ##Input last operator
        if last_operator:
            xpath = Util.GetXpath({"locate": "operator_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": last_operator, "result": "1"})
            time.sleep(1)

        ##Input item id
        if item_id:
            xpath = Util.GetXpath({"locate": "itemid_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "result": "1"})
            time.sleep(1)

        ##Input shop id
        if shop_id:
            xpath = Util.GetXpath({"locate": "shopid_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(2)
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})
            time.sleep(1)

        if create_start_time and create_end_time:

            ##Input Start Time
            xpath = Util.GetXpath({"locate": "create_time_datetime_start"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": str(create_start_time), "result": "1"})

            ##Get start time value
            BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
            BaseUICore.GetAttributes({"attrtype": "get_attribute", "attribute": "value", "mode": "single", "result": "1"})
            dumplogger.info("create start time value : %s" % (GlobalAdapter.CommonVar._PageAttributes_))

            ##Check input time
            if GlobalAdapter.CommonVar._PageAttributes_ != str(create_start_time):
                dumplogger.info("second input date : %s" % (GlobalAdapter.CommonVar._PageAttributes_))

                ##Clear input and input again
                BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": str(create_start_time), "result": "1"})

            ##Click OK
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

            ##Input End Time
            xpath = Util.GetXpath({"locate": "create_time_datetime_end"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": str(create_end_time), "result": "1"})

            ##Click OK
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})
            time.sleep(1)

        ##Click filter calender of tracking time
        if tracking_start_time and tracking_end_time:

            ##Input Tracking Start Time
            xpath = Util.GetXpath({"locate": "tracking_time_datetime_start"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": str(tracking_start_time), "result": "1"})

            ##Click OK
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

            ##Input Tracking End Time
            xpath = Util.GetXpath({"locate": "tracking_time_datetime_end"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": str(tracking_end_time), "result": "1"})

            ##Click OK
            xpath = Util.GetXpath({"locate": "ok_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})
            time.sleep(1)

        ##Input feature collection
        if feature_collection:
            ##Click list
            xpath = Util.GetXpath({"locate": "list_bar"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

            ##Click specific element in list
            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("string_to_be_replaced", feature_collection)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ModifyFilterInput')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyFilterQuantityInput(arg):
        '''
        ModifyFilterQuantityInput : Modify filter quantity input
                Input argu :
                    total_item_min -  total item quantity min value
                    total_item_max - total item quantity max value
                    normal_item_min - normal item quantity min value
                    normal_item_max - notmal item quantity max value
                    po_ratio_min - min value of PO ratio
                    po_ratio_max - max value of PO ratio
                    cod_ratio_min - min value of COD ratio
                    cod_ratio_max - max value of COD ratio
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        total_item_min = arg["total_item_min"]
        total_item_max = arg["total_item_max"]
        normal_item_min = arg["normal_item_min"]
        normal_item_max = arg["normal_item_max"]
        po_ratio_min = arg["po_ratio_min"]
        po_ratio_max = arg["po_ratio_max"]
        cod_ratio_min = arg["cod_ratio_min"]
        cod_ratio_max = arg["cod_ratio_max"]

        ##Input total item quantity
        xpath = Util.GetXpath({"locate": "total_item_min_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": total_item_min, "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "total_item_max_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": total_item_max, "result": "1"})
        time.sleep(1)

        ##Input normal item quantity
        xpath = Util.GetXpath({"locate": "normal_item_min_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": normal_item_min, "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "normal_item_max_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": normal_item_max, "result": "1"})
        time.sleep(1)

        ##Input po item ratio
        xpath = Util.GetXpath({"locate": "po_ratio_min_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": po_ratio_min, "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "po_ratio_max_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": po_ratio_max, "result": "1"})
        time.sleep(1)

        ##Input cod item ratio
        xpath = Util.GetXpath({"locate": "cod_ratio_min_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": cod_ratio_min, "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": "cod_ratio_max_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": cod_ratio_max, "result": "1"})
        time.sleep(1)

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ModifyFilterQuantityInput')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCalenderDateRandomly(arg):
        '''
        ChooseCalenderDateRandomly : Choose calender date randomly
                Input argu :
                    end_date_bar - end date inputbox in filter
                    ok_btn - ok button in calender
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_date_bar = arg["end_date_bar"]

        ##Click start date
        xpath = Util.GetXpath({"locate": "start_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date", "result": "1"})
        time.sleep(3)

        ##Click OK
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        ##Click end date
        xpath = Util.GetXpath({"locate": end_date_bar})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date bar", "result": "1"})

        xpath = Util.GetXpath({"locate": "end_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date", "result": "1"})
        time.sleep(3)

        ##Click OK
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ChooseCalenderDateRandomly')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckFields(arg):
        '''
        CheckFields : Check fields content
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get elements
        xpath = Util.GetXpath({"locate": "first_row"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        dumplogger.info("fields content test : %s" % (GlobalAdapter.CommonVar._PageAttributes_))
        try:
            fields = re.findall(r"\d{4} Featured from", GlobalAdapter.CommonVar._PageAttributes_)
            dumplogger.info("fields content matched : %s" % (fields))
        except:
            ret = 0
            dumplogger.info("fields content not matched : %s" % (GlobalAdapter.CommonVar._PageAttributes_))

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.CheckFields')


class AdminProductCollectionPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete btn
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set column btn
        xpath = Util.GetXpath({"locate": "filter_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMask(arg):
        '''
        ClickMask : Click mask
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set column btn
        xpath = Util.GetXpath({"locate": "mask"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mask", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickMask')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewCollection(arg):
        '''
        ClickAddNewCollection : Click add new collection
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set column btn
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickAddNewCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUpload(arg):
        '''
        ClickBatchUpload : Click batch upload btn
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch upload btn
        xpath = Util.GetXpath({"locate": "batch_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch upload", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickBatchUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFindCollectionbyItemID(arg):
        '''
        ClickFindCollectionbyItemID : Click find collection by item ID button from DL admin product collection list page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click find collection by item id btn
        xpath = Util.GetXpath({"locate": "find_collection_by_item_id_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click find collection by item ID", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickFindCollectionbyItemID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCollectionInfo(arg):
        '''
        ClickEditCollectionInfo : Click edit info of product collection
                Input argu :
                    collection_name - collection name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_name = arg["collection_name"]

        ##Click edit info button of specific collection name
        if collection_name:
            xpath = Util.GetXpath({"locate": "edit_info_btn"})
            xpath = xpath.replace("name_to_be_replaced", collection_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit info", "result": "1"})

        ##Click the last edit info btn
        else:
            xpath = Util.GetXpath({"locate": "last_edit_info_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit info", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickEditCollectionInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCollectionItem(arg):
        '''
        ClickEditCollectionItem : Click edit item of product collection
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit item btn
        xpath = Util.GetXpath({"locate": "edit_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit item", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickEditCollectionItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportResult(arg):
        '''
        ClickExportResult : Click export result
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export result
        xpath = Util.GetXpath({"locate": "export_result_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export result", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickExportResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLastPage(arg):
        '''
        ClickLastPage : Click go to last page
                Input argu :
                    NA
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click go to last page button
        xpath = Util.GetXpath({"locate": "last_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to last page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickLastPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetColumnDropdown(arg):
        '''
        ClickSetColumnDropdown : Click set column dropdown
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set column dropdown
        xpath = Util.GetXpath({"locate": "set_column_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set column dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickSetColumnDropdown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMenu(arg):
        '''
        ClickFilterMenu : Click filter menu
                Input argu :
                    locate_button - id_filter/last_operator_filter/name_filter
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_button = arg["locate_button"]

        ##Click set column btn
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickFilterMenu')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMenuSearch(arg):
        '''
        ClickFilterMenuSearch : Click search icon on dropdown search column
                Input argu :
                    locate_button - locate button xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_button = arg["locate_button"]

        ##Click search button on dropdown column
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickFilterMenuSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMenuReset(arg):
        '''
        ClickFilterMenuReset : Click reset icon on dropdown search column
                Input argu :
                    locate_button - locate button xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_button = arg["locate_button"]

        ##Click reset button on dropdown column
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickFilterMenuReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateTimeCalender(arg):
        '''
        ClickCreateTimeCalender : Click calender of create time in the filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click calender icon of "create time"
        xpath = Util.GetXpath({"locate": "calender_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click calender", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickCreateTimeCalender')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTrackingTimeCalender(arg):
        '''
        ClickTrackingTimeCalender : Click calender of tracking time in the filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click calender icon of "tracking time"
        xpath = Util.GetXpath({"locate": "calender_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click calender", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickTrackingTimeCalender')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCMSAdminTitle(arg):
        '''
        ClickCMSAdminTitle : Click CMS admin title
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click CMS Admin title
        xpath = Util.GetXpath({"locate": "admin_title"})
        xpath = xpath.replace("region_to_be_replaced", Config._TestCaseRegion_.upper())
        xpath = xpath.replace("env_to_be_replaced", Config._EnvType_.lower())
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click CMS Admin title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickCMSAdminTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSourceRuleLink(arg):
        '''
        ClickSourceRuleLink : Click Source Rule by column order
                Input argu :
                    number - column number in table
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg['number']

        ##Click source rule link
        xpath = Util.GetXpath({"locate": "source_rule_link"})
        xpath = xpath.replace("string_to_be_replaced", number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click source rule link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ClickSourceRuleLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCollectionMoreOption(arg):
        '''
        ChooseCollectionMoreOption : Choose option in collection more options button
                Input argu :
                    option - choose option (recalculate_metric / clear_cache / delete / audit_log_page)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Move to more btn and choose option
        xpath = Util.GetXpath({"locate": "more_btn"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
        time.sleep(1)
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose option ->" + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ChooseCollectionMoreOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDeleteCollectionOption(arg):
        '''
        ChooseDeleteCollectionOption : Select option in delete collection popup window
                Input argu :
                    option - cancel / delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Choose option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPageButton.ChooseDeleteCollectionOption')


class AdminBatchUploadPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadedFilesTitle(arg):
        '''
        ClickUploadedFilesTitle : Click uploaded files title
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product collection title
        xpath = Util.GetXpath({"locate": "uploaded_files_title"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product collection title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickUploadedFilesTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickColumnTitle(arg):
        '''
        ClickColumnTitle : Click column title
                Input argu :
                    name - name of the title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click collection name
        xpath = Util.GetXpath({"locate": "column_title"})
        xpath = xpath.replace("string_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click column title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickColumnTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToUpload(arg):
        '''
        ClickToUpload : Click "click to upload" button
                Input argu :
                    button_name - click_to_upload_create/click_to_upload_add/click_to_upload_remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]

        ##Click back to collection list page
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click click to upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickToUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplate(arg):
        '''
        ClickDownloadTemplate : Click download template button
                Input argu :
                    button_name - download_template_create / download_template_add / download_template_remove / download_template_change / download_template_delete / download_template_overwrite
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]

        ##Click back to collection list page
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click click to upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickDownloadTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadBUPFile(arg):
        '''
        ClickDownloadBUPFile : Click to download specific file in BUP page
                Input argu :
                    download_type - source_file / result_info / collection_id
                    file_name - file name
                    file_type - file type
                    upload_type - upload type (Mass_add_items / Mass_remove_items / Mass_add_collection_id)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        download_type = arg["download_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        upload_type = arg["upload_type"]

        ##Download specific source file
        xpath = Util.GetXpath({"locate": download_type})
        xpath = xpath.replace("file_name_to_be_replaced", file_name)
        xpath = xpath.replace("file_type_to_be_replaced", file_type)
        xpath = xpath.replace("upload_type_to_be_replaced", upload_type)
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickDownloadBUPFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadFirstBUPFile(arg):
        '''
        ClickDownloadFirstBUPFile : Click to download first file in BUP page
                Input argu :
                    download_type - download_source_file / download_result_info
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        download_type = arg["download_type"]

        ##Download specific source file
        xpath = Util.GetXpath({"locate": download_type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickDownloadFirstBUPFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchFileNameIcon(arg):
        '''
        ClickSearchFileNameIcon : Click icon of upload file for batch create new collection IDs
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click icon of search filename
        xpath = Util.GetXpath({"locate": "magnify_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSearchFileNameIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchTaskIDIcon(arg):
        '''
        ClickSearchTaskIDIcon : Click icon of search task id filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click icon of search filename
        xpath = Util.GetXpath({"locate": "magnify_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSearchTaskIDIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchOperatorIcon(arg):
        '''
        ClickSearchOperatorIcon : Click icon of search operator filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click icon of search filename
        xpath = Util.GetXpath({"locate": "magnify_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSearchOperatorIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchFileName(arg):
        '''
        ClickSearchFileName : Click button of search files by file name
                Input argu :
                    button_type - search / reset
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click apply btn of search file
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s button" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSearchFileName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchTaskID(arg):
        '''
        ClickSearchTaskID : Click button of search files by task ID
                Input argu :
                    button_type - search / reset
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click apply btn of search file
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s button" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSearchTaskID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchOperator(arg):
        '''
        ClickSearchOperator : Click button of search files by operator
                Input argu :
                    button_type - search / reset
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click apply btn of search file
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s button" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSearchOperator')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterTypeIcon(arg):
        '''
        ClickFilterTypeIcon : Click type filter icon of upload file for batch create new collection IDs
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter type icon
        xpath = Util.GetXpath({"locate": "funnel_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickFilterTypeIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterStatusIcon(arg):
        '''
        ClickFilterStatusIcon : Click status filter icon of upload file for batch create new collection IDs
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter status icon
        xpath = Util.GetXpath({"locate": "funnel_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickFilterStatusIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterUploadTimeIcon(arg):
        '''
        ClickFilterUploadTimeIcon : Click upload time filter icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter upload icon
        xpath = Util.GetXpath({"locate": "funnel_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickFilterUploadTimeIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterUploadTimeCalender(arg):
        '''
        ClickFilterUploadTimeCalender : Click upload time filter calender
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click calender
        xpath = Util.GetXpath({"locate": "calender_popup"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click calender", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickFilterUploadTimeCalender')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectTime(arg):
        '''
        ClickSelectTime : Click "select time" on calender
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select time btn
        xpath = Util.GetXpath({"locate": "select_time_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select time button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickSelectTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCalenderOK(arg):
        '''
        ClickCalenderOK : Click "OK" on calender
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickCalenderOK')


class AdminBatchUploadPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchUploadCSV(arg):
        '''
        BatchUploadCSV : Upload file for each csv input in BUP page
                Input argu :
                    upload_type - add_collection / add_item / remove_item / change_item / delete_collection / overwrite_collection
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        for upload_times in range(2):

            ##Upload csv with collections
            xpath = Util.GetXpath({"locate": upload_type})
            BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

            if upload_type == "delete_collection":
                ##Click "Confirm" on popup window
                xpath = Util.GetXpath({"locate": "confirm"})
                BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click confirm button", "result": "1"})

            elif upload_type in ("add_item", "overwrite_collection"):
                ##Click continue upload btn
                xpath = Util.GetXpath({"locate": "continue_upload_btn"})
                BaseUICore.Click({"method": "javascript", "locate": xpath, "message": "Click continue upload button", "result": "1"})

            time.sleep(5)

            ##Check if file upload successfully
            xpath = Util.GetXpath({"locate": "uploaded_files"})
            xpath = xpath.replace("string_to_be_replaced", file_name)

            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}) or file_type != "csv":
                dumplogger.info("Upload file.")
                ret = 1
                break

            ##Upload file again
            else:
                ret = 0
                BaseUILogic.BrowserRefresh({"message": "Refresh browser.", "result": "1"})
                time.sleep(5)
                dumplogger.info("Fail to upload file.")

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.BatchUploadCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchText(arg):
        '''
        InputSearchText : Input contents to input bar of upload file for batch create new collection IDs
                Input argu :
                    input_type - filename/operator/task_id
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": input_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.InputSearchText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectFilterTypeDropdown(arg):
        '''
        SelectFilterTypeDropdown : Select type in type filter dropdown
                Input argu :
                    type - all / mass_add_collection_id / mass_add_items / mass_remove_items
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click specific type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose type ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.SelectFilterTypeDropdown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectFilterStatusDropdown(arg):
        '''
        SelectFilterStatusDropdown : Select status in status filter dropdown
                Input argu :
                    status - all / pending / processing / succeed / failed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click filter status dropdown
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose type ->" + status, "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.SelectFilterStatusDropdown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterUploadTimeCalenderDateRandomly(arg):
        '''
        ClickFilterUploadTimeCalenderDateRandomly : Click two date of the calender randomly
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click start date
        xpath = Util.GetXpath({"locate": "start_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date", "result": "1"})

        ##Click end date
        xpath = Util.GetXpath({"locate": "end_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPageButton.ClickFilterUploadTimeCalenderDateRandomly')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStartEndTime(arg):
        '''
        InputStartEndTime : Input start time and end time of upload time filter
                Input argu :
                    start_time - input start time
                    end_time - input end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
        time.sleep(3)

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.InputStartEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDropdownPage(arg):
        '''
        SelectDropdownPage : Select Dropdown Page
                Input argu :
                    items_per_page - select items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        items_per_page = arg['items_per_page']

        ##Click dropdown page button
        xpath = Util.GetXpath({"locate": "dropdown_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown page button", "result": "1"})

        ##Click items per page button
        xpath = Util.GetXpath({"locate": "items_per_page"})
        xpath = xpath.replace("string_to_be_replaced", items_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click items per page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.SelectDropdownPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanFilterUploadTimeDate(arg):
        '''
        CleanFilterUploadTimeDate : Clean filter upload calender time
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Hover on calender popup
        xpath = Util.GetXpath({"locate": "calender_popup"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})
        time.sleep(2)

        ##Click "x" if there is one
        xpath = Util.GetXpath({"locate": "cross_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click clean date", "result": "1"})

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.CleanFilterUploadTimeDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveTables(arg):
        '''
        SaveTables : Save rows of tables to list
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        GlobalAdapter.ProductCollectionE2EVar._TemplateList_ = []

        xpath = Util.GetXpath({"locate": "row_element"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
        BaseUILogic.FilterStringToGlobal({"type": "table", "object": "", "result": "1"})

        GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_ = list(GlobalAdapter.ProductCollectionE2EVar._TemplateList_)
        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._TemplateList_))
        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_))

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.SaveTables')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTaskID(arg):
        '''
        CheckTaskID : Check task id shown in order on bup
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_):
            if row[0] <= GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_[count + 1][0]:
                ret = 0
                dumplogger.info("Task id not in order : %s, %s" % (row[0], GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_[count + 1][0]))
                break

            if count == len(GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_) - 2:
                break

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.CheckTaskID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckUploadTime(arg):
        '''
        CheckUploadTime : Check upload time shown in order on bup
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_):
            try:
                previous_time = re.findall("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4}), ([0-1][0-9]|[0-9]):([0-5][0-9]):([0-5][0-9]) (AM|PM)", row[3])[0]
                next_time = re.findall("([0-9]{1,2})/([0-9]{1,2})/([0-9]{4}), ([0-1][0-9]|[0-9]):([0-5][0-9]):([0-5][0-9]) (AM|PM)", GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_[count + 1][3])[0]
                dumplogger.info("Regular expression time : %s, %s" % (previous_time, next_time))
            except:
                dumplogger.info("Regular expression cannot match time : %s, %s" % (row[3], GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_[count + 1][3]))
                break

            ##Compare time, compare date first
            if previous_time[0] >= next_time[0] or previous_time[1] >= next_time[1] or previous_time[2] >= next_time[2]:

                ##Compare time, if date is the same, compare time
                if previous_time[0] == next_time[0] or previous_time[1] == next_time[1] or previous_time[2] == next_time[2]:

                    ##Compare time if is the same time in the morning or at night
                    if previous_time[6] == next_time[6]:

                        ##Compare time finally
                        if previous_time[3] >= next_time[3] or previous_time[4] >= next_time[4] or previous_time[5] >= next_time[5]:
                            ret = 1

                        else:
                            ret = 0
                            dumplogger.info("Time not in order : %s, %s" % (previous_time, next_time))
                            break

                    else:
                        if previous_time[3] == "AM" and next_time[3] == "PM":
                            ret = 1

                        else:
                            ret = 0
                            dumplogger.info("Time not in order : %s, %s" % (previous_time, next_time))
                            break
                else:
                    ret = 1

            else:
                ret = 0
                dumplogger.info("Time not in order : %s, %s" % (previous_time, next_time))
                break

            if count == len(GlobalAdapter.ProductCollectionE2EVar._BatchUploadList_) - 2:
                break

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.CheckUploadTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckElementWithRefresh(arg):
        '''
        CheckElementWithRefresh : Check the status of upload file
                Input argu :
                    locate - position in web
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg["locate"]

        for refresh_times in range(5):

            #Find the status of upload file
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": locate, "passok": "0", "result": "1"}):
                ret = 1
                break

            else:
                ret = 0
                dumplogger.info("Cannot find file status.")
                BaseUILogic.BrowserRefresh({"message": "Refresh browser.", "result": "1"})
                time.sleep(5)

        OK(ret, int(arg['result']), 'AdminBatchUploadPage.CheckElementWithRefresh')


class AdminProductCollectionInfoPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCollectionNameFET(arg):
        '''
        EditCollectionNameFET : Edit collection name FET
                Input argu :
                    type - default_language / other_language
                    input_content - input content
                    random - 0/1
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        input_content = arg["input_content"]
        random = int(arg['random'])

        if random:
            input_content = XtFunc.GenerateRandomString(20, "characters")

        ##Input name
        if input_content:
            xpath = Util.GetXpath({"locate": type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)

            ##Store input product name to global
            GlobalAdapter.ProductCollectionE2EVar._NewCollectionName_ = input_content
            dumplogger.info(GlobalAdapter.ProductCollectionE2EVar._NewCollectionName_)

        ##Exist condition that name is blank
        else:
            xpath = Util.GetXpath({"locate": type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.EditCollectionNameFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCollectionDescriptionFET(arg):
        '''
        EditCollectionDescriptionFET : Edit collection description FET
                Input argu :
                    type - default_language / other_language
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        input_content = arg["input_content"]
        random = int(arg['random'])

        if random:
            input_content = XtFunc.GenerateRandomString(20, "characters")

        ##Input content
        if input_content:
            xpath = Util.GetXpath({"locate": type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)

        ##Exist condition that description is blank
        else:
            xpath = Util.GetXpath({"locate": type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.EditCollectionDescriptionFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditCollectionSubtitleFET(arg):
        '''
        EditCollectionSubtitleFET : Edit collection description FET
                Input argu :
                    type - default_language / other_language
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        input_content = arg["input_content"]

        ##Clear input field
        xpath = Util.GetXpath({"locate": type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##Input content
        if input_content:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.EditCollectionSubtitleFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditFooterHeadingFET(arg):
        '''
        EditFooterHeadingFET : Edit footer heading FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        if input_content:
            xpath = Util.GetXpath({"locate": "footer_heading_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)

        ##Exist condition that description is blank
        else:
            xpath = Util.GetXpath({"locate": "footer_heading_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.EditFooterHeadingFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditFooterContentFET(arg):
        '''
        EditFooterContentFET : Edit footer content FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        if input_content:
            xpath = Util.GetXpath({"locate": "footer_content_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)

        ##Exist condition that description is blank
        else:
            xpath = Util.GetXpath({"locate": "footer_content_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.EditFooterContentFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImage(arg):
        '''
        UploadImage : Upload image
                Input argu :
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        image_field = arg["image_field"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload file
        xpath = Util.GetXpath({"locate": image_field})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.UploadImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddNewFooterWithData(arg):
        '''
        AddNewFooterWithData : Add new footer with data
                Input argu :
                    heading - footer heading
                    content - footer content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        heading = arg["heading"]
        content = arg["content"]

        ##Input heading
        xpath = Util.GetXpath({"locate": "heading_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": heading, "result": "1"})
        time.sleep(3)

        ##Input content
        xpath = Util.GetXpath({"locate": "content_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": content, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.AddNewFooterWithData')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanAllFooter(arg):
        '''
        CleanAllFooter : Clean all footer in info page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = 0

        ##Count number of remove btn
        xpath = Util.GetXpath({"locate": "remove_btn"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})

            ##Remove all footers
            for select_time in range(int(GlobalAdapter.GeneralE2EVar._Count_)):
                xpath = Util.GetXpath({"locate": "remove_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Remove " + str(select_time + 1) + " footer", "result": "1"})

        else:
            dumplogger.info("There's no footer exists!!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.CleanAllFooter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanAllImage(arg):
        '''
        CleanAllImage : Clean all footer in info page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Count number of remove btn
        xpath = Util.GetXpath({"locate": "remove_btn"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})

            ##Remove all footers
            for select_time in range(int(GlobalAdapter.GeneralE2EVar._Count_)):
                xpath = Util.GetXpath({"locate": "remove_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Remove " + str(select_time + 1) + " footer", "result": "1"})

        else:
            dumplogger.info("There's no images exists!!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.CleanAllImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanSpecificImage(arg):
        '''
        CleanSpecificImage : Clean specific image on info page
                Input argu :
                    image_type - delete button to be clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_type = arg["image_type"]

        ##Find specific image and click delete button
        xpath = Util.GetXpath({"locate": image_type})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Remove " + str(image_type), "result": "1"})
        else:
            dumplogger.info("There's no images exists!!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.CleanSpecificImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseLeaveWithoutSaveOption(arg):
        '''
        ChooseLeaveWithoutSaveOption : Select option when leave page without save window popup
                Input argu :
                    option - cancel / leave
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Choose option of leave without save popup
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.ChooseLeaveWithoutSaveOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCalenderDateRandomly(arg):
        '''
        ChooseCalenderDateRandomly : Choose calender date randomly
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click start date
        xpath = Util.GetXpath({"locate": "start_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date", "result": "1"})
        time.sleep(3)

        ##Click OK
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        ##Click tracking time end date
        xpath = Util.GetXpath({"locate": "tracking_time_end_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date bar", "result": "1"})

        ##Click end date
        xpath = Util.GetXpath({"locate": "end_date"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date", "result": "1"})
        time.sleep(3)

        ##Click OK
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OK button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.ChooseCalenderDateRandomly')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchCampaignTag(arg):
        '''
        SearchCampaignTag : Search campaign tag
                Input argu :
                    input_content - campaign tag input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input search campaign
        xpath = Util.GetXpath({"locate": "input_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input bar", "result": "1"})

        xpath = Util.GetXpath({"locate": "input_list"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.SearchCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCampaignTag(arg):
        '''
        ChooseCampaignTag : Choose campaign tag
                Input argu :
                    name - campaign tag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Choose search camapign
        xpath = Util.GetXpath({"locate": "result_tag"})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input bar", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.ChooseCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveCampaignTag(arg):
        '''
        RemoveCampaignTag : Remove campaign tag
                Input argu :
                    name - campaign tag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click remove campaign
        xpath = Util.GetXpath({"locate": "remove_btn"})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPage.RemoveCampaignTag')


class AdminProductCollectionInfoPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewFooter(arg):
        '''
        ClickAddNewFooter : Click add new footer
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new footer btn
        xpath = Util.GetXpath({"locate": "add_new_footer_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new footer btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickAddNewFooter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveAndGoToEditItem(arg):
        '''
        ClickSaveAndGoToEditItem : Click save and go to edit item
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save and edit item btn
        xpath = Util.GetXpath({"locate": "save_edit_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save and go to edit item btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickSaveAndGoToEditItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItem(arg):
        '''
        ClickEditItem : Click edit item
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit item btn
        xpath = Util.GetXpath({"locate": "edit_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit item btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickEditItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisplayTransparentImageToggle(arg):
        '''
        ClickDisplayTransparentImageToggle : Click display transparent image Toggle
                Input argu :
                    toggle - toggle on / off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Click toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle ->" + toggle, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickDisplayTransparentImageToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRecommendedToggle(arg):
        '''
        ClickRecommendedToggle : Click Recommended In Featured Collections Entrance
                Input argu :
                    toggle - toggle on / off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Click toggle
        xpath = Util.GetXpath({"locate": toggle})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click toggle ->" + toggle, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickRecommendedToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemoveFooter(arg):
        '''
        ClickRemoveFooter : Click remove footer
                Input argu :
                    heading - footer heading
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        heading = arg["heading"]

        ##Click remove footer btn
        xpath = Util.GetXpath({"locate": "remove_footer_btn"})
        xpath = xpath.replace("string_to_be_replaced", heading)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove footer btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickRemoveFooter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTrackingTimeCalender(arg):
        '''
        ClickTrackingTimeCalender : Click calender of tracking time in the filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click tracking time calender
        xpath = Util.GetXpath({"locate": "calender_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click calender", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickTrackingTimeCalender')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClearTrackingTime(arg):
        '''
        ClickClearTrackingTime : Click clear calender of tracking time
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click tracking time calender
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickClearTrackingTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click tab
                Input argu :
                    tab - general_information / microsite_page / auto_collection_page / collection_landing_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click tab
        xpath = Util.GetXpath({"locate": tab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tab: " + tab, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionInfoPageButton.ClickTab')


class AdminProductCollectionItemPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetAndStoreCollectionID(arg):
        '''
        GetAndStoreCollectionID : Get and store collection id
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get element attribute
        xpath = Util.GetXpath({"locate": "collection_id"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Handle correct collection id format
        GlobalAdapter.ProductCollectionE2EVar._CollectionID_ = GlobalAdapter.CommonVar._PageAttributes_
        try:
            GlobalAdapter.ProductCollectionE2EVar._CollectionID_ = GlobalAdapter.ProductCollectionE2EVar._CollectionID_.split(' ')[-1]
        except:
            dumplogger.info("cannot split collection id!!!!!!!!!!!!!!!!!!")
        dumplogger.info("Product Collection ID: %s" % (GlobalAdapter.ProductCollectionE2EVar._CollectionID_))

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.GetAndStoreCollectionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionIDTitle(arg):
        '''
        ClickCollectionIDTitle : Click collection id title
                Input argu :
                    name - name of the title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click collection name
        xpath = Util.GetXpath({"locate": "column_title"})
        xpath = xpath.replace("string_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click column title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.ClickCollectionIDTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickColumnTitle(arg):
        '''
        ClickColumnTitle : Click column title
                Input argu :
                    name - name of the title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click collection name
        xpath = Util.GetXpath({"locate": "column_title"})
        xpath = xpath.replace("string_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click column title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.ClickColumnTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCollectionTitle(arg):
        '''
        ClickEditCollectionTitle : Click edit item collection title
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product collection title
        xpath = Util.GetXpath({"locate": "item_collection_title"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product collection title", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionPage.ClickEditCollectionTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputChangePosition(arg):
        '''
        InputChangePosition : Input change position
                Input argu :
                    number - position number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input position number
        xpath = Util.GetXpath({"locate": "position_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": number, "result": "1"})

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.InputChangePosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemID(arg):
        '''
        ClickItemID : Click item id
                Input argu :
                    item_id - find item_id to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']

        ##Click promotion boost
        xpath = Util.GetXpath({"locate": "item_id_btn"})
        xpath = xpath.replace("string_to_be_replaced", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item id button", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.ClickItemID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterMenuInput(arg):
        '''
        FilterMenuInput : Input content in specific filter menu
                Input argu :
                    input_content - input content
                    locate_button - locate filter area to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]
        locate_button = arg["locate_button"]

        ##Input content
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.FilterMenuInput')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddItemsType(arg):
        '''
        ClickAddItemsType : Click opload type in add items
                Input argu :
                    upload_type - mass_upload/manual_upload
                    mass_upload_type - add_items_into_collection/overwrite_collection
                    action - image/file
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg['upload_type']
        mass_upload_type = arg['mass_upload_type']
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        if upload_type == "mass_upload":
            ##Click add items icon
            xpath = Util.GetXpath({"locate": "mass_upload"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass upload button", "result": "1"})

            if mass_upload_type == "add_items_into_collection":
                xpath = Util.GetXpath({"locate": "add_items_into_collection"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add items into collection button", "result": "1"})

                xpath = Util.GetXpath({"locate": "input_text"})
                BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

            elif mass_upload_type == "overwrite_collection":
                xpath = Util.GetXpath({"locate": "overwrite_collection"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click overwrite collection button", "result": "1"})

                xpath = Util.GetXpath({"locate": "input_text"})
                BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

            else:
                dumplogger.info("There's no corresponding mass_upload_type.")

        elif upload_type == "manual_upload":
            ##Click add items icon
            xpath = Util.GetXpath({"locate": "manual_upload"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass upload button", "result": "1"})

        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.ClickAddItemsType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputItemIDShopID(arg):
        '''
        InputItemIDShopID : Input Item ID and Shop ID on Add items popup Manual upload
                Input argu :
                    item_id - item_id to input
                    shop_id - shop id to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg['item_id']
        shop_id = arg['shop_id']

        ##Get input item id column xpath and input item id
        xpath = Util.GetXpath({"locate": "item_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "result": "1"})

        ##Get input shop id column xpath and input shop id
        xpath = Util.GetXpath({"locate": "shop_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.InputItemIDShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMultiItemIDShopID(arg):
        '''
        InputMultiItemIDShopID : Input multiple Item IDs and Shop IDs on Add items popup Manual upload
                Input argu :
                    item_id_string - a list of item id
                    shop_id_string - a list of shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id_string = arg['item_id']
        shop_id_string = arg['shop_id']
        item_ids = []
        shop_ids = []

        ##Append item id from string to list
        for item_id in item_id_string.split(","):
            item_ids.append(item_id)

        for shop_id in shop_id_string.split(","):
            shop_ids.append(shop_id)

        if len(item_id) != len(shop_id):
            dumplogger.info("Input shop id and item id count not matched")
            ret = 0

        ##Add 10 items
        dumplogger.info("len current item : %d %d" % (len(item_ids), len(shop_ids)))
        for item_count in range(len(item_ids)):
            dumplogger.info("current item : %d" % (item_count))

            ##Check if trash icon is existed
            xpath = Util.GetXpath({"locate": "trash_icon"})

            ##Get input item id column location and input item id
            xpath = Util.GetXpath({"locate": "item_id"})
            xpath = xpath.replace("string_to_be_replaced", str(item_count))
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_ids[item_count], "result": "1"})

            ##Get input shop id column location and input shop id
            xpath = Util.GetXpath({"locate": "shop_id"})
            xpath = xpath.replace("string_to_be_replaced", str(item_count))
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_ids[item_count], "result": "1"})

            #Click add more item button
            if item_count != 9:
                xpath = Util.GetXpath({"locate": "add_more_item_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add more item button", "result": "1"})

            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.InputMultiItemIDShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckDeleteIconCount(arg):
        '''
        CheckDeleteIconCount : Check delete icon number
                Input argu :
                    count - delete icon count(1-10)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        count = int(arg['count'])

        xpath = Util.GetXpath({"locate": "trash_icon"})
        ##Check if trash icon is existed
        if count == 1:
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ret = 0
                dumplogger.info("1 item only but appear trash icon")

        else:
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            if GlobalAdapter.GeneralE2EVar._Count_ != count:
                ret = 0
                dumplogger.info("More than one item exist but does not match trash icons %d, %s" % (count, GlobalAdapter.GeneralE2EVar._Count_))

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.CheckDeleteIconCount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteAllItems(arg):
        '''
        DeleteAllItems : Delete all items on product collection item page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Unselect all column
        xpath = Util.GetXpath({"locate": "delete_btn"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})

            ##Click all delete button and confirm
            for select_time in range(int(GlobalAdapter.GeneralE2EVar._Count_)):
                xpath = Util.GetXpath({"locate": "delete_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete item " + str(select_time + 1), "result": "1"})

                ##Click confirm button
                xpath = Util.GetXpath({"locate": "confirm_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

                time.sleep(3)

        else:
            dumplogger.info("No delete button exists!!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.DeleteAllItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBoostDatetime(arg):
        '''
        InputBoostDatetime : Input boost date and time
                Input argu :
                    date - input boost date
                    time - input boost time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        date = arg["date"]
        time = arg["time"]

        ##Insert boost time
        xpath = Util.GetXpath({"locate": "boost_time_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click boost time bar", "result": "1"})
        xpath = Util.GetXpath({"locate": "time_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Insert boost date
        xpath = Util.GetXpath({"locate": "boost_date_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click boost date bar", "result": "1"})
        xpath = Util.GetXpath({"locate": "calender_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": date, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.InputBoostDatetime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCurrentDateBoostTime(arg):
        '''
        CheckCurrentDateBoostTime : Check current date boost time
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current date
        time_stamp = XtFunc.GetCurrentDateTime("%d-%m-%Y", 0, 0, 0)

        ##Check promotion boost time
        xpath = Util.GetXpath({"locate": "promotion_boost_time"})
        BaseUILogic.GetAndCheckElements({"method": "xpath", "locate": xpath, "string": time_stamp, "isfuzzy": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.CheckCurrentDateBoostTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectEditItemAllColumn(arg):
        '''
        SelectEditItemAllColumn : Select edit item all column
                Input argu :
                    select - 1(Select)/0(Unselect)
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        select = arg['select']
        number = 0

        if select == "1":
            ##Select all column
            xpath = Util.GetXpath({"locate": "unchecked_column"})
        else:
            ##Remove all column
            xpath = Util.GetXpath({"locate": "checked_column"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            number = GlobalAdapter.GeneralE2EVar._Count_

            ##Select all detected unchecked column
            for select_time in range(number):
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select column" + str(select_time + 1), "result": "1"})

        else:
            dumplogger.info("There's no unchecked column!!!")

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.SelectEditItemAllColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditUploadedItem(arg):
        '''
        EditUploadedItem : Edit uploaded item
                Input argu :
                    action - action (change_position / edit / delete)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Choose item action
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Edit uploaded item -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditUploadedItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEditItemInfo(arg):
        '''
        InputEditItemInfo : Input Edit item information
                Input argu :
                    column - display_name_default_language / display_name_other_language / campaign_stock
                    input_content - content to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column = arg['column']
        input_content = arg['input_content']

        ##Clear column
        xpath = Util.GetXpath({"locate": column})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

        ##input content
        if input_content:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "tab", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.InputEditItemInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItemActions(arg):
        '''
        ClickEditItemActions : Click edit item actions button
                Input argu :
                    button_name - save/cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg['button_name']

        ##Click edit item actions button
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s button" % (button_name), "result": "1"})
        #BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.ClickEditItemActions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassUploadItems(arg):
        '''
        MassUploadItems : Mass upload items
                Input argu :
                    upload_option - add / overwrite
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_option = arg["upload_option"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Click mass upload button
        xpath = Util.GetXpath({"locate": "mass_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass upload button", "result": "1"})

        ##Click upload option
        xpath = Util.GetXpath({"locate": upload_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose upload option -> " + upload_option, "result": "1"})

        ##Upload file
        xpath = Util.GetXpath({"locate": "input_text"})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(10)

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        ##Click continue upload btn
        xpath = Util.GetXpath({"locate": "continue_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click continue upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.MassUploadItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterItemIDFET(arg):
        '''
        EditItemFilterItemIDFET : Filter item ID FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "id_input"})

        ##Input content
        if input_content:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterItemIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterShopIDFET(arg):
        '''
        EditItemFilterShopIDFET : Filter shop ID FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "id_input"})

        ##Input content
        if input_content:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterShopIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterCategoryFET(arg):
        '''
        EditItemFilterCategoryFET : Filter category FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "category_input"})

        ##Input content
        if input_content:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterCategoryFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterProductNameFET(arg):
        '''
        EditItemFilterProductNameFET : Filter product name FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "name_input"})

        ##Input content
        if input_content:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterProductNameFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterCurrentPriceFET(arg):
        '''
        EditItemFilterCurrentPriceFET : Filter current price FET in edit item
                Input argu :
                    input_content - input current price
                    price_type - Min/Max
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]
        price_type = arg["price_type"]

        xpath = Util.GetXpath({"locate": price_type})

        ##Input content
        if input_content:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)
        else:
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterCurrentPriceFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterStatusFET(arg):
        '''
        EditItemFilterStatusFET : Filter Status FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterStatusFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterInPromotionFET(arg):
        '''
        EditItemFilterInPromotionFET : Filter in promotion FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterInPromotionFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterDTSFET(arg):
        '''
        EditItemFilterDTSFET : Filter DTS FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterDTSFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterCODFET(arg):
        '''
        EditItemFilterCODFET : Filter COD FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterCODFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterLPGFET(arg):
        '''
        EditItemFilterLPGFET : Filter LPG FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterLPGFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterSBSFET(arg):
        '''
        EditItemFilterSBSFET : Filter SBS FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterSBSFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterWholesaleFET(arg):
        '''
        EditItemFilterWholesaleFET : Filter wholesale FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterWholesaleFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterStarSellerFET(arg):
        '''
        EditItemFilterStarSellerFET : Filter star seller FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterStarSellerFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterOfficialShopFET(arg):
        '''
        EditItemFilterOfficialShopFET : Filter official shop FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterOfficialShopFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterCrossborderSellerFET(arg):
        '''
        EditItemFilterCrossborderSellerFET : Filter crossborder seller FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterCrossborderSellerFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterRiskItemsFET(arg):
        '''
        EditItemFilterRiskItemsFET : Filter risk items FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterRiskItemsFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilter3PLFET(arg):
        '''
        EditItemFilter3PLFET : Filter 3PL FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})

        ##Input text
        BaseUICore.SendSeleniumKeyEvent({"action_type":"string", "string": input_content, "result": "1"})

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilter3PLFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditItemFilterProductLabelFET(arg):
        '''
        EditItemFilterProductLabelFET : Filter product label FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        if input_content:
            xpath = Util.GetXpath({"locate": "name_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)
        else:
            xpath = Util.GetXpath({"locate": "name_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.EditItemFilterProductLabelFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyEditItemFilter(arg):
        '''
        ModifyEditItemFilter : Click edit item filter
                Input argu :
                    item_id - item id
                    shop_id - shop id
                    category_id - category id
                    product_name - product name
                    current_min - current min price
                    current_max - current max price
                    status - item status
                    in_promotion - in promotion (True / False)
                    dts - preorder or not (Pre-order / Non-preorder)
                    cod - COD item or not (True / False)
                    lpg - LPG (True / False)
                    sbs - SBS (True / False)
                    wholesale - wholesale or not (True / False)
                    star_seller - star seller or not (True / False)
                    official_shop - official shop or not (True / False)
                    crossborder - croAdminProductCollectionItemPage.MassUploadItems
                    third_PL - choose 3-person-logistics
                    product_label - product label
                    invert_selection - get the result outside of filter selection (yes / no)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]
        shop_id = arg["shop_id"]
        category_id = arg["category_id"]
        product_name = arg["product_name"]
        current_min = arg["current_min"]
        current_max = arg["current_max"]
        status = arg["status"]
        in_promotion = arg["in_promotion"]
        dts = arg["dts"]
        cod = arg["cod"]
        lpg = arg["lpg"]
        sbs = arg["sbs"]
        wholesale = arg["wholesale"]
        star_seller = arg["star_seller"]
        official_shop = arg["official_shop"]
        crossborder = arg["crossborder"]
        three_PL = arg["three_PL"]
        product_label = arg["product_label"]
        invert_selection = arg["invert_selection"]

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})
        time.sleep(3)

        ##Input item id
        if item_id:
            xpath = Util.GetXpath({"locate": "item_id_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "result": "1"})

        ##Input shop id
        if shop_id:
            xpath = Util.GetXpath({"locate": "shop_id_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        ##Input category id
        if category_id:
            xpath = Util.GetXpath({"locate": "category_id_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": category_id, "result": "1"})

        ##Input product name
        if product_name:
            xpath = Util.GetXpath({"locate": "product_name_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": product_name, "result": "1"})

        ##Input product name
        if current_min:
            xpath = Util.GetXpath({"locate": "current_min_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": current_min, "result": "1"})

        ##Input product name
        if current_max:
            xpath = Util.GetXpath({"locate": "current_max_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": current_max, "result": "1"})

        ##Choose status
        if status:
            xpath = Util.GetXpath({"locate": "status_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click status list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "status_list")
            xpath = xpath.replace("string_to_be_replaced", status)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose in_promotion
        if in_promotion:
            xpath = Util.GetXpath({"locate": "in_promotion_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click in promotion list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "in_promotion_list")
            xpath = xpath.replace("string_to_be_replaced", in_promotion)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose DTS
        if dts:
            xpath = Util.GetXpath({"locate": "dts_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click DTS list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "dts_list")
            xpath = xpath.replace("string_to_be_replaced", dts)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose COD
        if cod:
            xpath = Util.GetXpath({"locate": "cod_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click COD list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "cod_list")
            xpath = xpath.replace("string_to_be_replaced", cod)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose LPG
        if lpg:
            xpath = Util.GetXpath({"locate": "lpg_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click LPG list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "lpg_list")
            xpath = xpath.replace("string_to_be_replaced", lpg)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose SBS
        if sbs:
            xpath = Util.GetXpath({"locate": "sbs_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click SBS list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "sbs_list")
            xpath = xpath.replace("string_to_be_replaced", sbs)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose wholesale
        if wholesale:
            xpath = Util.GetXpath({"locate": "wholesale_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click wholesale list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "wholesale_list")
            xpath = xpath.replace("string_to_be_replaced", wholesale)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose star_seller
        if star_seller:
            xpath = Util.GetXpath({"locate": "star_seller_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click star seller list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "star_seller_list")
            xpath = xpath.replace("string_to_be_replaced", star_seller)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose official_shop
        if official_shop:
            xpath = Util.GetXpath({"locate": "official_shop_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click official shop list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "official_shop_list")
            xpath = xpath.replace("string_to_be_replaced", official_shop)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose crossborder
        if crossborder:
            xpath = Util.GetXpath({"locate": "cross_border_seller_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click official shop list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "cross_border_seller_list")
            xpath = xpath.replace("string_to_be_replaced", crossborder)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Choose three PL
        if three_PL:
            xpath = Util.GetXpath({"locate": "three_pl_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click 3PL list", "result": "1"})

            xpath = Util.GetXpath({"locate": "list_input"})
            xpath = xpath.replace("id_to_be_replaced", "three_pl_list")
            xpath = xpath.replace("string_to_be_replaced", three_PL)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        ##Input product label
        if product_label:
            xpath = Util.GetXpath({"locate": "product_label_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": product_label, "result": "1"})

        ##Choose invert_selection
        if invert_selection:
            xpath = Util.GetXpath({"locate": invert_selection})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Invert selection ->" + invert_selection, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.ModifyEditItemFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveTables(arg):
        '''
        SaveTables : Save rows of tables to list
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        GlobalAdapter.ProductCollectionE2EVar._TemplateList_ = []

        ##Select all column
        xpath = Util.GetXpath({"locate": "last_page"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        page_count = int(GlobalAdapter.CommonVar._PageAttributes_)

        for page_num in range(page_count):
            dumplogger.info("current page : %d" % (page_num))

            #for select_time in range(number):
            xpath = Util.GetXpath({"locate": "row_element"})
            BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
            BaseUILogic.FilterStringToGlobal({"type": "table", "object": "", "result": "1"})

            if page_num != page_count - 1:
                xpath = Util.GetXpath({"locate": "next_page_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page button", "result": "1"})

            time.sleep(7)

        GlobalAdapter.ProductCollectionE2EVar._ItemEditList_ = list(GlobalAdapter.ProductCollectionE2EVar._TemplateList_)
        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._TemplateList_))
        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._ItemEditList_))

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.SaveTables')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckItemIDOrder(arg):
        '''
        CheckItemIDOrder : Check item id order in item collection page
                Input argu :
                    item_id - item id order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_ids = arg['item_ids']

        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._ItemEditList_):
            dumplogger.info("Testing : %d, %s, %s" % (count, row, item_ids.split(",")[count]))
            if row[0].replace(" ", "") != item_ids.split(",")[count]:
                ret = 0
                dumplogger.info("Not in shop metrics shop ids : %s, %s" % (row[0], item_ids.split(",")[count]))
                break

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPage.CheckItemIDOrder')


class AdminProductCollectionItemPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToCollectionInfo(arg):
        '''
        ClickBackToCollectionInfo : Click back to collection info
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to collection info btn
        xpath = Util.GetXpath({"locate": "back_to_collection_info_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to collection info btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickBackToCollectionInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportResults(arg):
        '''
        ClickExportResults : Click export results
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export item page result
        xpath = Util.GetXpath({"locate": "export_result_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export result", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickExportResults')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteResults(arg):
        '''
        ClickDeleteResults : Click delete results
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete result
        xpath = Util.GetXpath({"locate": "delete_result_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export result", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickDeleteResults')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HoverOnBatchActions(arg):
        '''
        HoverOnBatchActions : Hover mouse on batch actions
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Hover on batch action btn
        xpath = Util.GetXpath({"locate": "batch_action_btn"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.HoverOnBatchActions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionBoost(arg):
        '''
        ClickPromotionBoost : Click promotion boost icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click promotion boost
        xpath = Util.GetXpath({"locate": "promotion_boost_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion boost button", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickPromotionBoost')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClosePromotionBoost(arg):
        '''
        ClosePromotionBoost : Click promotion boost close icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click promotion boost close icon
        xpath = Util.GetXpath({"locate": "promotion_boost_close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion boost close button", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClosePromotionBoost')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterReset(arg):
        '''
        ClickFilterReset : Click filter reset button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFilterReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterSearch(arg):
        '''
        ClickFilterSearch : Click filter search button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        ##If filter still exist, press again
        xpath = Util.GetXpath({"locate": "pclp_body"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": int(arg['result'])}):
            xpath = Util.GetXpath({"locate": "search_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFilterSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddItemsIcon(arg):
        '''
        ClickAddItemsIcon : Click add items icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add items icon
        xpath = Util.GetXpath({"locate": "add_items_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add items button", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickAddItemsIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplate(arg):
        '''
        ClickDownloadTemplate : Click download template button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to collection list page
        xpath = Util.GetXpath({"locate": "download_template_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download template button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickDownloadTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMoreDetails(arg):
        '''
        ClickMoreDetails : Click more details link
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click more details button
        xpath = Util.GetXpath({"locate": "more_details_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click more details button", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickMoreDetails')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteBoost(arg):
        '''
        ClickDeleteBoost : Click delete boost button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete boost button
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.MoveToElementCoordinate({"is_click": "1", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickDeleteBoost')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseEditItemFilter(arg):
        '''
        CloseEditItemFilter : Close edit item filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close filter button
        xpath = Util.GetXpath({"locate": "close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.CloseEditItemFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMask(arg):
        '''
        ClickFilterMask : Click filter mask
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter mask
        xpath = Util.GetXpath({"locate": "mask"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFilterMask')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItemFilter(arg):
        '''
        ClickEditItemFilter : Click edit item filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit item filter
        xpath = Util.GetXpath({"locate": "filter_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickEditItemFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseEditItemOption(arg):
        '''
        ChooseEditItemOption : Choose option after editting a item
                Input argu :
                    option - cancel / save
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Choose an option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose option -> " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ChooseEditItemOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseChangePositionItemListAction(arg):
        '''
        ChooseChangePositionItemListAction : Choose action after confirming change position
                Input argu :
                    action - action after confirm changing position (close / redirect)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Choose action
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose action -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ChooseChangePositionItemListAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBoostOption(arg):
        '''
        ChooseBoostOption : Choose promotion boost option
                Input argu :
                    option - boost_now / boost_later
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Choose boost option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose boost option -> " + option, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ChooseBoostOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMenu(arg):
        '''
        ClickFilterMenu : Click filter menu
                Input argu :
                    locate_button - locate filter button to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_button = arg["locate_button"]

        ##Click filter menu button
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFilterMenu')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMenuSearch(arg):
        '''
        ClickFilterMenuSearch : Click search icon on dropdown search column
                Input argu :
                    locate_button - locate button xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_button = arg["locate_button"]

        ##Click search button on dropdown column
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFilterMenuSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMenuReset(arg):
        '''
        ClickFilterMenuReset : Click reset icon on dropdown search column
                Input argu :
                    locate_button - locate button xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_button = arg["locate_button"]

        ##Click reset button on dropdown column
        xpath = Util.GetXpath({"locate": locate_button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFilterMenuReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemovePromotionBoost(arg):
        '''
        RemovePromotionBoost : Remove promotion boost by clicking trash icon
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click trash icon to remove promotion boost (For restoration)
        xpath = Util.GetXpath({"locate": "trash_icon"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click trash icon", "result": "1"})

        else:
            dumplogger.info("There's no promotion boost.")

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.RemovePromotionBoost')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSpecificDeleteIcon(arg):
        '''
        ClickSpecificDeleteIcon : Click delete icon at specific number
                Input argu :
                    item_order - number of item to click delete icon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_order = arg['item_order']

        ##Click delete icon at (item_order)th
        xpath = Util.GetXpath({"locate": "delete_icon"})
        xpath = xpath.replace("string_to_be_replaced", str(item_order))
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete icon at item %sth" % (item_order), "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickSpecificDeleteIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddItems(arg):
        '''
        ClickAddItems : Click add items submit icon
                Input argu :
                    button_name - close/cancel/submit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg['button_name']

        ##Click add items close icon
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add items %s button" % (button_name), "result": "1"})
        time.sleep(8)

        if button_name == "submit":
            ##Click continue upload btn
            xpath = Util.GetXpath({"locate": "continue_upload_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click continue upload button", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickAddItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFailedToUpload(arg):
        '''
        ClickFailedToUpload : Click failed to upload popup button
                Input argu :
                    button - close/download_result_file
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button = arg['button']

        ##Click failed to upload popup button
        xpath = Util.GetXpath({"locate": button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click %s button" % (button), "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickFailedToUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteItem(arg):
        '''
        ClickDeleteItem : Click delete item button
                Input argu :
                    button_name - cancel/confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg['button_name']

        ##Click add items close icon
        xpath = Util.GetXpath({"locate": "delete_btn"})
        ##BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add items close button", "result": "1"})
        BaseUICore.MoveToElementCoordinate({"is_click": "1", "locate": xpath, "result": "1"})

        if button_name == "cancel":
            xpath = Util.GetXpath({"locate": "cancel_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        elif button_name == "confirm":
            xpath = Util.GetXpath({"locate": "confirm_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        else:
            dumplogger.info("There's no such button : %s" % (button_name))

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickDeleteItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDropdownColumns(arg):
        '''
        ClickDropdownColumns : Click Dropdown Column btn
                Input argu :
                    button_name - button name to click (Reset/Confirm)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg['button_name']

        ##Click dropdown column btn
        xpath = Util.GetXpath({"locate": button_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set column dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickDropdownColumns')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditItemSetColumnDropdown(arg):
        '''
        ClickEditItemSetColumnDropdown : Click edit item set column dropdown
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set column btn
        xpath = Util.GetXpath({"locate": "set_column_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set column dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickEditItemSetColumnDropdown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseMassUploadFileFailOption(arg):
        '''
        ChooseMassUploadFileFailOption : Choose option after upload file fails
                Input argu :
                    option - option choosing upload file failure (close / download_result_file)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Choose option of upload file fail option
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose option -> " + option, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ChooseMassUploadFileFailOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDeleteItemOption(arg):
        '''
        ChooseDeleteItemOption : Select option in delete item popup window
                Input argu :
                    option - cancel / delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Choose option of delete item popup
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose option", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ChooseDeleteItemOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPersonalizedItemDisplayToggle(arg):
        '''
        ClickPersonalizedItemDisplayToggle : Click personalized item display toggle
                Input argu :
                    toggle - on/off
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        toggle = arg["toggle"]

        ##Check if toggle is "already" on/off, if not switch it
        xpath = Util.GetXpath({"locate": toggle})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": int(arg['result'])}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click personalized item display to: " + toggle, "result": "1"})
        else:
            dumplogger.info("Maybe toggle is already on")

        OK(ret, int(arg['result']), 'AdminProductCollectionItemPageButton.ClickPersonalizedItemDisplayToggle')


class AdminCampaignTagPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchCampaignTag(arg):
        '''
        SearchCampaignTag : Search campaign tag using different search method
                Input argu :
                    method - tag_id / tag_name / tag_use_case
                    text - search text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method = arg["method"]
        text = arg["text"]

        ##Click search icon
        xpath = Util.GetXpath({"locate": method})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click campaign tag search icon -> " + method, "result": "1"})
        time.sleep(3)

        if method == "tag_id" or method == "tag_name":

            ##Input search bar
            xpath = Util.GetXpath({"locate": method + "_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "result": "1"})
            time.sleep(3)

            ##Click search btn
            xpath = Util.GetXpath({"locate": "search_btn"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
            time.sleep(3)

        elif method == "tag_use_case":

            ##Choose use case
            xpath = Util.GetXpath({"locate": text})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose tag use case -> " + text, "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPage.SearchCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddNewTagNameFET(arg):
        '''
        AddNewTagNameFET : Do FET when adding new tag name
                Input argu :
                    input_content -  input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Input content
        xpath = Util.GetXpath({"locate": "tag_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
        time.sleep(5)

        ##If input content is null, should add click save btn (just for this case)
        if input_content == "":
            xpath = Util.GetXpath({"locate": "save_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminCampaignTagPage.AddNewTagNameFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddNewTagUseCaseFET(arg):
        '''
        AddNewTagUseCaseFET : Do FET when adding new tag use case
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        ##Click list
        xpath = Util.GetXpath({"locate": "list_bar"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list", "result": "1"})
        time.sleep(3)

        ##Click specific element in list
        xpath = Util.GetXpath({"locate": "list_input"})
        xpath = xpath.replace("string_to_be_replaced", input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click list input", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignTagPage.AddNewTagUseCaseFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteCampaignTag(arg):
        '''
        DeleteCampaignTag : Delete specific campaign tag
                Input argu :
                    tag_name - delete tag name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tag_name = arg["tag_name"]

        ##Click delete btn
        xpath = Util.GetXpath({"locate": "delete_btn"})
        xpath = xpath.replace("string_to_be_replaced", tag_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete campaign tag ->" + tag_name, "result": "1"})
        time.sleep(3)

        ##Click confirm btn
        xpath = Util.GetXpath({"locate": "confirm_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPage.DeleteCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCampaignNumberInPage(arg):
        '''
        SelectCampaignNumberInPage : Select the number campaign tag show in one page
                Input argu :
                    number - number of campaign tag in page (10/20/30/40)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Click dropdown menu
        xpath = Util.GetXpath({"locate": "dropdown_menu"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown menu", "result": "1"})
        time.sleep(3)

        ##Choose number of element in page
        xpath = Util.GetXpath({"locate": "option"})
        xpath = xpath.replace("string_to_be_replaced", number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose number of tag in page", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPage.SelectCampaignNumberInPage')


class AdminCampaignTagPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddCampaignTag(arg):
        '''
        ClickAddCampaignTag : Add new campaign tag
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new campaign tag
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Add new campaign tag", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickAddCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCampaignTag(arg):
        '''
        ClickEditCampaignTag : Click edit campaign tag
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit campaign tag", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickEditCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteCampaignTag(arg):
        '''
        ClickDeleteCampaignTag : Click delete campaign tag
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete btn
        xpath = Util.GetXpath({"locate": "delete_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete campaign tag", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickDeleteCampaignTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickResetCampaignTagSearchBar(arg):
        '''
        ClickResetCampaignTagSearchBar : Reset campaign tag search bar
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset button
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset campaign tag search bar", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickResetCampaignTagSearchBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToNextPage(arg):
        '''
        ClickToNextPage : Click to next page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next page btn
        xpath = Util.GetXpath({"locate": "next_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to next page", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickToNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToPreviousPage(arg):
        '''
        ClickToPreviousPage : Click to previous page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click previous page btn
        xpath = Util.GetXpath({"locate": "previous_page"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to previous page", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickToPreviousPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchTagIcon(arg):
        '''
        ClickSearchTagIcon : Click icon of different type of search tag
                Input argu :
                    type - icon type (tag_id / tag_name / tag_use_case)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tag_type = arg["type"]

        ##Click icon
        xpath = Util.GetXpath({"locate": tag_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search tag type -> " + tag_type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickSearchTagIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchTagIDIcon(arg):
        '''
        ClickSearchTagIDIcon : Click icon of search tag ID
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click icon
        xpath = Util.GetXpath({"locate": "tag_id_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search tag ID icon", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickSearchTagIDIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchTagNameIcon(arg):
        '''
        ClickSearchTagNameIcon : Click icon of search tag name
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click icon
        xpath = Util.GetXpath({"locate": "tag_name_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search tag name icon", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickSearchTagNameIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearchTagUseCaseIcon(arg):
        '''
        ClickSearchTagUseCaseIcon : Click icon of search tag use case
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click icon
        xpath = Util.GetXpath({"locate": "tag_use_case_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search tag use case icon", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ClickSearchTagUseCaseIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseCancelEditTagOption(arg):
        '''
        ChooseCancelEditTagOption : Choose option when cancel editting tag
                Input argu :
                    option - option (yes / cancel)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click icon
        xpath = Util.GetXpath({"locate": option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose cancle edit tag option ->" + option, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignTagPageButton.ChooseCancelEditTagOption')


class AdminAuditLogPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckElementColor(arg):
        '''
        CheckElementColor : Check element color
                Input argu :
                    button_name - button to be checked
                    style - element style
                    attribute - corresponding attribute to stlye
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_name = arg["button_name"]
        style = arg['style']
        attribute = arg['attribute']

        ##Get element location
        xpath = Util.GetXpath({"locate": button_name})

        ##Check element style attribut
        BaseUICore.GetAndCheckValueOfCSSProperty({"locate": xpath, "style": style, "attribute": attribute, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckElementColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDropdownPage(arg):
        '''
        SelectDropdownPage : Select Dropdown Page
                Input argu :
                    dropdown_page_btn - collection_info_tab / item_info_tab
                    items_per_page - select items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        dropdown_page_btn = arg["dropdown_page_btn"]
        items_per_page = arg['items_per_page']

        ##Click dropdown page button
        xpath = Util.GetXpath({"locate": dropdown_page_btn})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown page button", "result": "1"})

        ##Click items per page button
        xpath = Util.GetXpath({"locate": "items_per_page"})
        xpath = xpath.replace("string_to_be_replaced", items_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click items per page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogPage.SelectDropdownPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPersonalizedItemDisplayResult(arg):
        '''
        CheckPersonalizedItemDisplayResult : Check Personalized Item Display result
                Input argu :
                    file_name - Check filename
                    old_value - Turn off/Turn on
                    new_value - Turn off/Turn on
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        old_value = arg['old_value']
        new_value = arg['new_value']

        ##Check Boost scheduling result
        content = "," + old_value + "," + new_value + "," + GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_.replace(' ', '.').lower()+"@shopee.com"

        XtFunc.CheckDownloadCsvContent({"file_name": file_name, "check_content": content, "check_row_position": "", "isfuzzy": "1", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckPersonalizedItemDisplayResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckChangePositionOfItemResult(arg):
        '''
        CheckChangePositionOfItemResult : Check change position of item result
                Input argu :
                    file_name - Check filename
                    item_id - item_id
                    old_value - old_value
                    new_value - new_value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        item_id = arg['item_id']
        old_value = arg['old_value']
        new_value = arg['new_value']

        ##Check Boost scheduling result
        content = item_id + "," + old_value + "," + new_value + "," + GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_ + "@shopee.com"

        XtFunc.CheckDownloadCsvContent({"file_name": file_name, "check_content": content, "check_row_position": "", "isfuzzy": "1", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckChangePositionOfItemResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckEditItemInfoResult(arg):
        '''
        CheckEditItemInfoResult : Check edit item info result
                Input argu :
                    file_name - Check filename
                    item_id - item_id
                    old_value - old_value
                    new_value - new_value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        item_id = arg['item_id']
        old_value = arg['old_value']
        new_value = arg['new_value']

        ##Check Boost scheduling result
        content = item_id + "," + old_value + "," + new_value + "," + GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_.replace(' ', '.').lower()+"@shopee.com"

        XtFunc.CheckDownloadCsvContent({"file_name": file_name, "check_content": content, "check_row_position": "", "isfuzzy": "1", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckEditItemInfoResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckEventIDOrder(arg):
        '''
        CheckEventIDOrder : Check event id order in item collection page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_list = []
        dumplogger.info("Testing : %s" % (GlobalAdapter.ProductCollectionE2EVar._AuditLogList_))
        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._AuditLogList_):
            dumplogger.info("Testing : %d, %s" % (count, row))
            if int(row[0]) < int(GlobalAdapter.ProductCollectionE2EVar._AuditLogList_[count + 1][0]):
                ret = 0
                dumplogger.info("Event ID not in order : %s, %s" % (row[0], GlobalAdapter.ProductCollectionE2EVar._AuditLogList_[count + 1][0]))
                break

            if int(row[0]) not in check_list:
                check_list.append(int(row[0]))
            else:
                ret = 0
                dumplogger.info("Shop id duplicate : %s, %s" % (row[0], GlobalAdapter.ProductCollectionE2EVar._AuditLogList_[count + 1][0]))
                break

            if count == len(GlobalAdapter.ProductCollectionE2EVar._AuditLogList_) - 2:
                break

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckEventIDOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreAndCompareUpdateTime(arg):
        '''
        StoreAndCompareUpdateTime : Check update time with two timezones
                Input argu :
                    compare - 1/0 (1:compare with previous stored updated time/0:store update_time)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        compare = arg['compare']

        ##Get current update time
        xpath = Util.GetXpath({"locate": "update_time"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        update_time = GlobalAdapter.CommonVar._PageAttributes_
        dumplogger.info("Update time : %s" % (update_time))

        try:
            update_time = re.findall(r"\d{1,2}\:\d{2}:\d{2}", update_time)[0]
        except:
            ret = 0
            dumplogger.info("Update time not found : %s" % (update_time))

        if compare:
            ##Check if update time has changed
            if update_time == GlobalAdapter.ProductCollectionE2EVar._UpdateTime_:
                ret = 0
                dumplogger.info("Update time has not changed.")

            else:
                dumplogger.info("Update time has changed.")

        else:
            GlobalAdapter.ProductCollectionE2EVar._UpdateTime_ = update_time

        OK(ret, int(arg['result']), 'AdminAuditLogPage.StoreAndCompareUpdateTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToAuditLogItemPage(arg):
        '''
        GoToAuditLogItemPage : Go to audit log item tab page
                Input argu :
                    collection_id - input collection id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id = arg["collection_id"]

        ##Get collection_id from Global variable
        if GlobalAdapter.ProductCollectionE2EVar._CollectionID_ and not collection_id:
            collection_id = GlobalAdapter.ProductCollectionE2EVar._CollectionID_

        ##Input collection_id
        if collection_id:
            url = "https://dl-admin."+GlobalAdapter.UrlVar._Domain_+"/product-audit-log?collection_id="+collection_id+"&active_key=Item&page=1"
            BaseUICore.GotoURL({"url": url, "result": "1"})
            BaseUICore.PageHasLoaded({"result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogPage.GoToAuditLogItemPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckLastOperator(arg):
        '''
        CheckLastOperator : Check last operator
                Input argu :
                    tab - collection_info/item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg['tab']

        ##Get current update time
        xpath = Util.GetXpath({"locate": tab})

        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        operator = GlobalAdapter.CommonVar._PageAttributes_

        ##Check if update time has changed
        if operator == GlobalAdapter.ProductCollectionE2EVar._GoogleAccount_.replace(' ', '.').lower()+"@shopee.com":
            dumplogger.info("Last operator matched")

        else:
            ret = 0
            dumplogger.info("Last operator not matched")

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckLastOperator')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckActions(arg):
        '''
        CheckActions : Check actions to corresponding event
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._AuditLogList_):
            dumplogger.info("current count : %d, current row : %s" % (count, row))

            if row[2] == "Delete Boost" and row[4] == "":
                ret = 1
            elif row[2] == "Boost scheduling" and row[4] == "Download Result":
                ret = 1
            elif row[2] == "Delete items" and row[4] == "Download Source":
                ret = 1
            elif row[2] == "Add items" and row[4] == "Download Source":
                ret = 1
            else:
                ret = 0
                dumplogger.info("ALP page event and action match failed : %s" % (row))
                break

        OK(ret, int(arg['result']), 'AdminAuditLogPage.CheckActions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveTables(arg):
        '''
        SaveTables : Save rows of tables to list
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        GlobalAdapter.ProductCollectionE2EVar._TemplateList_ = []

        ##Select all column
        xpath = Util.GetXpath({"locate": "last_page"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        page_count = int(GlobalAdapter.CommonVar._PageAttributes_)

        xpath = Util.GetXpath({"locate": "row_element"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
        BaseUILogic.FilterStringToGlobal({"type": "table", "object": "", "result": "1"})

        GlobalAdapter.ProductCollectionE2EVar._AuditLogList_ = list(GlobalAdapter.ProductCollectionE2EVar._TemplateList_)
        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._AuditLogList_))

        OK(ret, int(arg['result']), 'AdminAuditLogPage.SaveTables')


class AdminAuditLogPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadSource(arg):
        '''
        ClickDownloadSource : Click download source button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download source button
        xpath = Util.GetXpath({"locate": "download_source_btn"})
        BaseUICore.MoveToElementCoordinate({"is_click": "1", "locate": xpath, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminAuditLogPageButton.ClickDownloadSource')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadResult(arg):
        '''
        ClickDownloadResult : Click download result button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download result button
        xpath = Util.GetXpath({"locate": "download_result_btn"})
        BaseUICore.MoveToElementCoordinate({"is_click": "1", "locate": xpath, "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminAuditLogPageButton.ClickDownloadResult')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAuditPageTab(arg):
        '''
        ClickAuditPageTab : Click tab on Audit page
                Input argu :
                    button_name - button tab to be clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg["tab_name"]

        ##Click tab on Audit page
        xpath = Util.GetXpath({"locate": tab_name})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click audit page tab", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogPageButton.ClickAuditPageTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGotoPageNum(arg):
        '''
        ClickGotoPageNum : Click go to page number
                Input argu :
                    page_num - Click go to page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_num = arg['page_num']

        ##Click go to specific page number button
        xpath = Util.GetXpath({"locate": "page_btn"})
        xpath = xpath.replace("string_to_be_replaced", page_num)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to specific page number button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAuditLogPageButton.ClickGotoPageNum')


class AdminShopMetricsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectDropdownPage(arg):
        '''
        SelectDropdownPage : Select Dropdown Page
                Input argu :
                    items_per_page - select items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        items_per_page = arg['items_per_page']

        ##Click dropdown page button
        xpath = Util.GetXpath({"locate": "dropdown_page_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dropdown page button", "result": "1"})

        ##Click items per page button
        xpath = Util.GetXpath({"locate": "items_per_page"})
        xpath = xpath.replace("string_to_be_replaced", items_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click items per page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.SelectDropdownPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditFilterItemIDFET(arg):
        '''
        EditFilterItemIDFET : Filter item ID FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "id_input"})

        ##Clear field
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        if input_content:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.EditFilterItemIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditFilterShopIDFET(arg):
        '''
        EditFilterShopIDFET : Filter shop ID FET in edit item
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "id_input"})

        ##Clear field
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        if input_content:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.EditFilterShopIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def FilterMenuIDFET(arg):
        '''
        FilterMenuIDFET : Filter ID FET
                Input argu :
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg["input_content"]

        xpath = Util.GetXpath({"locate": "shop_id_magnifier"})

        ##Clear field
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(5)

        ##Input content
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.FilterMenuIDFET')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SaveTables(arg):
        '''
        SaveTables : Save rows of tables to list
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        GlobalAdapter.ProductCollectionE2EVar._TemplateList_ = []

        ##Select all column
        xpath = Util.GetXpath({"locate": "last_page"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        page_count = int(GlobalAdapter.CommonVar._PageAttributes_)

        for page_num in range(page_count - 1):
            dumplogger.info("current page : %d" % (page_num))

            #for select_time in range(number):
            xpath = Util.GetXpath({"locate": "row_element"})
            BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})
            BaseUILogic.FilterStringToGlobal({"type": "table", "object": "", "result": "1"})

            xpath = Util.GetXpath({"locate": "next_page_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page button", "result": "1"})
            time.sleep(7)

        GlobalAdapter.ProductCollectionE2EVar._ShopMetricsList_ = list(GlobalAdapter.ProductCollectionE2EVar._TemplateList_)
        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._ShopMetricsList_))

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.SaveTables')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRowsSorted(arg):
        '''
        CheckRowsSorted : Check rows in table are being sorted
                Input argu :
                    sort_style - asce/desc
                    sort_column - number of column to be sorted
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        sort_style = arg["sort_style"]
        sort_column = int(arg["sort_column"])

        ##Check every row in the table
        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._TemplateList_):
            dumplogger.info("current count : %d, current row : %s" % (count, row))

            ##First shop info
            shop_po = row[sort_column].split("%")[0]
            ##Second shop info
            shop_po_to_be_compared = GlobalAdapter.ProductCollectionE2EVar._TemplateList_[count + 1][sort_column].split("%")[0]

            ##Compare by sorting style
            if sort_style == "asce":
                if float(shop_po) <= float(shop_po_to_be_compared):
                    ret = 1
                else:
                    ret = 0
                    break

            elif sort_style == "desc":
                if float(shop_po) >= float(shop_po_to_be_compared):
                    ret = 1
                else:
                    ret = 0
                    break

            else:
                dumplogger.info("No corresponding sorting style!!!!!!")
                ret = 0
                break

            ##Exit loop when reaches end of table
            if count == len(GlobalAdapter.ProductCollectionE2EVar._TemplateList_) - 2:
                break

        dumplogger.info("All table: %s" % (GlobalAdapter.ProductCollectionE2EVar._TemplateList_))

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.CheckRowsSorted')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShopID(arg):
        '''
        CheckShopID : Check shop id exists in shop metrics page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_metrics_shopids = []

        ##Save all shop metrics table id into shop_metrics_shopids list
        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._ShopMetricsList_):
            dumplogger.info("current count : %d, current row : %s" % (count, row))
            shop_metrics_shopids.append(row[0])

        ##Check if id of GlobalAdapter.ProductCollectionE2EVar._ItemEditList_ is in shop_metrics_shopids list
        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._ItemEditList_):
            if row[2] not in shop_metrics_shopids:
                ret = 0
                dumplogger.info("Not in shop metrics shop ids : %s, %s" % (row[2], shop_metrics_shopids))
                break

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.CheckShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckPercentage(arg):
        '''
        CheckPercentage : Check shop PO percentage is correct
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_metrics_data = {}

        ##Save all items info in GlobalAdapter.ProductCollectionE2EVar._ItemEditList_ into corresponding shop
        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._ItemEditList_):
            dumplogger.info("current count : %d, current row : %s" % (count, row))

            shop_id = row[2]
            shop_id_data = {"item_count": 1, "in_promotion": 0, "cod": 0}
            dumplogger.info("_ItemEditTable_ current row data : %s-%s, %s-%s" % (type(row[11]), row[11], type(row[13]), row[13]))

            ##check if shop id is already in shop_metrics_data dict
            if shop_id in shop_metrics_data:
                shop_metrics_data[shop_id]["item_count"] += 1
                ##Append to corresponding shop_id in dictionary if is ture
                if row[12] == "Pre-order":
                    shop_metrics_data[shop_id]["in_promotion"] += 1
                if row[13] == "True":
                    shop_metrics_data[shop_id]["cod"] += 1

            else:
                shop_metrics_data[shop_id] = shop_id_data
                if row[12] == "Pre-order":
                    shop_metrics_data[shop_id]["in_promotion"] += 1
                if row[13] == "True":
                    shop_metrics_data[shop_id]["cod"] += 1

            dumplogger.info("shop id data : %s" % (shop_metrics_data))

        ##Compare info in GlobalAdapter.ProductCollectionE2EVar._ShopMetricsList_ according to previously saved shop_metrics_data
        for count, row in enumerate(GlobalAdapter.ProductCollectionE2EVar._ShopMetricsList_):
            dumplogger.info("_ShopMetricsTable_ current count : %d, current row : %s" % (count, row))

            shop_id = row[0]
            shop_po = row[1]
            shop_cod = row[2]
            po_enabled_items = int(row[3])
            cod_enabled_items = int(row[4])
            shop_items = shop_metrics_data[shop_id]["item_count"]

            ##Check if data matched to previous saved data
            if po_enabled_items != shop_metrics_data[shop_id]["in_promotion"]:
                dumplogger.info("po_enabled_items error %s, %s" % (shop_metrics_data[shop_id]["in_promotion"], row))
                ret = 0
                break
            elif cod_enabled_items != shop_metrics_data[shop_id]["cod"]:
                dumplogger.info("cod_enabled_items error %s, %s" % (shop_metrics_data[shop_id]["cod"], row))
                ret = 0
                break
            elif len(shop_po.split(".")[-1]) != 3:
                dumplogger.info("shop_po not ends with 2 decimal %s" % (shop_po))
                ret = 0
                break
            elif len(shop_cod.split(".")[-1]) != 3:
                dumplogger.info("shop_cod not ends with 2 decimal %s" % (shop_cod))
                ret = 0
                break

            ##Check percentage according to previous saved data
            if po_enabled_items == 0:
                if not shop_po == "0.00%":
                    dumplogger.info("shop_po not 0.00% %s" % (shop_po))
                    ret = 0
                    break
            else:
                if not shop_po.startswith(str(round((float(po_enabled_items)/float(shop_items)), 4)*100)):
                    dumplogger.info("po_enabled_items: " + str(po_enabled_items))
                    dumplogger.info("shop_items: " + str(shop_items))
                    dumplogger.info("shop_po not startswith %f, %s" % (round((float(po_enabled_items)/float(shop_items)), 4)*100, shop_po))
                    ret = 0
                    break

            ##Check percentage according to previous saved data
            if cod_enabled_items == 0:
                if not shop_cod == "0.00%":
                    dumplogger.info("shop_cod not 0.00% %s" % (shop_cod))
                    ret = 0
                    break
            else:
                if not shop_cod.startswith(str(round((float(cod_enabled_items)/float(shop_items)), 4)*100)):
                    dumplogger.info("cod_enabled_items: " + str(cod_enabled_items))
                    dumplogger.info("shop_items: " + str(shop_items))
                    dumplogger.info("shop_cod not startswith %f, %s" % (round((float(cod_enabled_items)/float(shop_items)), 4)*100, shop_cod))
                    ret = 0
                    break

        OK(ret, int(arg['result']), 'AdminShopMetricsPage.CheckPercentage')


class AdminShopMetricsPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToItemEdit(arg):
        '''
        ClickBackToItemEdit : Click back to Item Edit
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to collection list page
        xpath = Util.GetXpath({"locate": "back_to_item_edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to Item Edit", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickBackToItemEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilter(arg):
        '''
        ClickFilter : Click filter
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter btn
        xpath = Util.GetXpath({"locate": "filter_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click reset button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset btn
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterMask(arg):
        '''
        ClickFilterMask : Click filter mask
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click filter mask
        xpath = Util.GetXpath({"locate": "mask"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickFilterMask')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCloseFilter(arg):
        '''
        ClickCloseFilter : Click close filter button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close filter button
        xpath = Util.GetXpath({"locate": "close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close filter button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickCloseFilter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterShopIDSearch(arg):
        '''
        ClickFilterShopIDSearch : Click search icon on dropdown search column
                Input argu :
                    locate_button - locate button xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search button on dropdown column
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickFilterShopIDSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterShopIDReset(arg):
        '''
        ClickFilterShopIDReset : Click reset icon on dropdown search column
                Input argu :
                    locate_button - locate button xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click reset button on dropdown column
        xpath = Util.GetXpath({"locate": "reset_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickFilterShopIDReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReorganizeShopCOD(arg):
        '''
        ReorganizeShopCOD : Click Shop COD reorganize button
                Input argu :
                    button - up/down
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button = arg['button']

        ##Click shop COD reorganize button
        xpath = Util.GetXpath({"locate": button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Shop COD reorganize button %s " % (button), "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ReorganizeShopCOD')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ReorganizeShopPO(arg):
        '''
        ReorganizeShopPO : Click Shop PO reorganize button
                Input argu :
                    button - up / down
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button = arg['button']

        ##Click shop PO reorganize button
        xpath = Util.GetXpath({"locate": button})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Shop PO reorganize button %s " % (button), "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ReorganizeShopPO')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopIDFilter(arg):
        '''
        ClickShopIDFilter : Filter ID FET
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input content
        xpath = Util.GetXpath({"locate": "shop_id_magnifier"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopMetricsPageButton.ClickShopIDFilter')


class AdminFindCollectionbyItemIDPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifySearchInput(arg):
        '''
        ModifySearchInput : Modify input in search bar
                Input argu :
                    item_id - item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Clear search bar
        xpath = Util.GetXpath({"locate": "search_bar"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(1)

        ##Input item id
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": item_id, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPage.ModifySearchInput')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCollectionIdSorted(arg):
        '''
        CheckCollectionIdSorted : Check if collection id in search result are sorted in descending order
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id_list = []

        ##Get number of page
        xpath = Util.GetXpath({"locate": "last_page"})
        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})
        page_count = int(GlobalAdapter.CommonVar._PageAttributes_)

        ##Get collection id in each row
        for page_num in range(page_count - 1):
            dumplogger.info("current page : %d" % (page_num))

            xpath = Util.GetXpath({"locate": "row_element"})
            BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})

            for collection_id in GlobalAdapter.CommonVar._PageElements_:
                collection_id_list.append(collection_id.text)

            xpath = Util.GetXpath({"locate": "next_page_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page button", "result": "1"})
            time.sleep(5)

        ##Compare each row
        for row_num in range(len(collection_id_list)):
            if int(collection_id_list[row_num]) >= int(collection_id_list[row_num + 1]):
                ret = 1
            else:
                ret = 0
                break

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPage.CheckCollectionIdSorted')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionName(arg):
        '''
        ClickCollectionName : Click collection name
                Input argu :
                    collection_name - find collection name to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_name = arg['collection_name']

        ##Click collection name
        xpath = Util.GetXpath({"locate": "collection_name_btn"})
        xpath = xpath.replace("string_to_be_replaced", collection_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click collection name button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPage.ClickCollectionName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCollection(arg):
        '''
        SelectCollection : Select collection by collection id
                Input argu :
                    collection_id - select which collection
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id = arg['collection_id']

        ##Select collection
        xpath = Util.GetXpath({"locate": "collection_checkbox"})
        xpath = xpath.replace("string_to_be_replaced", collection_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select collection", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPage.SelectCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAllCollection(arg):
        '''
        SelectAllCollection : Click header checkbox to select all collection
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click header checkbox
        xpath = Util.GetXpath({"locate": "header_checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click header checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPage.SelectAllCollection')


class AdminFindCollectionbyItemIDPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPageButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClearSearch(arg):
        '''
        ClearSearch : Click close button to clear search input
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close btn
        xpath = Util.GetXpath({"locate": "close_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPageButton.ClearSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove button
                Input argu :
                    collection_id - remove item from which collection
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id = arg['collection_id']

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        xpath = xpath.replace("string_to_be_replaced", collection_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPageButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassRemove(arg):
        '''
        ClickMassRemove : Click mass remove button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click mass remove button
        xpath = Util.GetXpath({"locate": "mass_remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass remove button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFindCollectionbyItemIDPageButton.ClickMassRemove')
