import unittest
from Config import dumplogger
from FrameWorkBase import *
from api.spex import SpexAPICore
import Config


class TWSpexCollectionGroup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexCollectionGroup Precondition =="
        dumplogger.info("== Setup TWSpexCollectionGroup Precondition ==")

        ##Tail log file to grep "Local connections to /run/spex/spex.sock:-2 forwarded to remote address /run/spex/spex.sock"
        ##If Spex InitResult is True, will not call RegisterToSpex() again
        if SpexAPICore.InspectSpexConnection():
            ##Register to Spex with specific type of spex configuration for the first time
            if not Config._SpexRegResult_:
                if SpexAPICore.GetSpexConfig('Api_Test_Staging_TW'):
                    Config._SpexRegResult_ = SpexAPICore.RegisterToSpex()

                    ##Check outcome after register
                    if Config._SpexRegResult_:
                        print "== Setup TWSpexCollectionGroup Register Success =="
                        dumplogger.info("== Setup TWSpexCollectionGroup Register Success ==")

                    else:
                        print ("Spex Register Failed!!!")
                        dumplogger.error("Spex Register Failed")
                        ##Directly Leave Setup
                        self.skipTest("")
                else:
                    dumplogger.error("Get Spex config from ini failed!!!")
                    self.skipTest("")
            else:
                dumplogger.info("Spex is already registered.")

        else:
            print "== Setup TWSpexCollectionGroup Precondition Fail by No Spex Agent Connection =="
            dumplogger.error("== Setup TWSpexCollectionGroup Precondition Fail by No Spex Agent Connection ==")
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWSpexCollectionGroup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionGroup-01.xml")

    def test_TWSpexCollectionGroup02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexCollectionGroup-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexCollectionGroup Post condition =="
        dumplogger.info("== Setup TWSpexCollectionGroup Post condition ==")
