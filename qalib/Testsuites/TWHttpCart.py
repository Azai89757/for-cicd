import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpCart Precondition =="
        dumplogger.info("== Setup TWHttpCart Precondition ==")

    def test_TWHttpCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-01.xml")

    def test_TWHttpCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-02.xml")

    def test_TWHttpCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-03.xml")

    def test_TWHttpCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-04.xml")

    def test_TWHttpCart05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-05.xml")

    def test_TWHttpCart06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-06.xml")

    def test_TWHttpCart07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-07.xml")

    def test_TWHttpCart08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-08.xml")

    def test_TWHttpCart09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpCart-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpCart Post condition =="
        dumplogger.info("== Setup TWHttpCart Post condition ==")
