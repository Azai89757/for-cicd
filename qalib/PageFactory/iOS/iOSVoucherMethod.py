#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 iOSVoucherMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import framework common library
import FrameWorkBase
import Util
import DecoratorHelper
import GlobalAdapter
from Config import dumplogger

##Import ios library
import iOSBaseUICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class iOSVoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherCode(arg):
        '''InputVoucherCode : Input voucher code in voucher selection page
                Input argu :
                        voucher_type - shopee_voucher / seller_voucher
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]

        ##Get voucher input field XPath
        xpath = Util.GetXpath({"locate":"voucher_input"})

        ##Input shopee voucher code
        if voucher_type == "shopee_voucher":
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[0]["code"], "result":"1"})

        ##Input seller voucher code
        elif voucher_type == "seller_voucher":
            iOSBaseUICore.iOSInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._SellerVoucherList_[0]["code"], "result":"1"})

        else:
            dumplogger.error("Voucher type string is not correct, please use shopee_voucher or seller_voucher string!!!")

        ##Sleep 10 second to wait voucher claim toast is disappear
        time.sleep(10)

        ##Click voucher to use
        xpath = Util.GetXpath({"locate":"voucher_use"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":xpath, "message":"Click voucher use", "result":"1"})
        time.sleep(15)

        ##Click submit
        xpath = Util.GetXpath({"locate":"voucher_submit"})
        iOSBaseUICore.iOSClick({"method":"id", "locate":xpath, "message":"Click voucher submit", "result":"1"})

        OK(ret, int(arg['result']), 'iOSVoucherPage.InputVoucherCode')
