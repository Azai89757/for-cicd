import unittest
from Config import dumplogger
from FrameWorkBase import *


class VNAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup VNAPIRestoreHomePopupBanner Precondition ==")

    def test_VNAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup VNAPIRestoreHomePopupBanner Post condition ==")
