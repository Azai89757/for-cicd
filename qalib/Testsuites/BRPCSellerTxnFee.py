import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCSellerTxnFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSellerTxnFee Precondition =="
        dumplogger.info("== Setup BRPCSellerTxnFee Precondition ==")

    def test_BRPCSellerTxnFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerTxnFee-01.xml")

    def test_BRPCSellerTxnFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerTxnFee-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSellerTxnFee Post condition =="
        dumplogger.info("== Setup BRPCSellerTxnFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
