import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpProductPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductPromotion Precondition =="
        dumplogger.info("== Setup TWHttpProductPromotion Precondition ==")

    def test_TWHttpProductPromotion01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductPromotion-01.xml")

    def test_TWHttpProductPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductPromotion-02.xml")

    def test_TWHttpProductPromotion03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductPromotion-03.xml")

    def test_TWHttpProductPromotion04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpProductPromotion-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpProductPromotion Post condition =="
        dumplogger.info("== Setup TWHttpProductPromotion Post condition ==")
