import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCMePage Precondition =="
        dumplogger.info("== Setup BRPCMePage Precondition ==")

    def test_BRPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCMePage Post condition =="
        dumplogger.info("== Setup BRPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
