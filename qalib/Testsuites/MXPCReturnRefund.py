import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCReturnRefund Precondition =="
        dumplogger.info("== Setup MXPCReturnRefund Precondition ==")

    def test_MXPCReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCReturnRefund-01.xml")

    def test_MXPCReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCReturnRefund-02.xml")

    def test_MXPCReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCReturnRefund-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCReturnRefund Post condition =="
        dumplogger.info("== Setup MXPCReturnRefund Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
