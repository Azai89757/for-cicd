﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 XtFunc.py: The def of this file called by XML mainly.
'''

##import system library
import sys
import os
import time
import shutil
import re
import itertools
import socket
import datetime
import traceback
import cv2
import numpy as np
import json
import glob
import random
import string
import csv
from lxml import etree
from pykeyboard import PyKeyboard
from pymouse import PyMouse
import hashlib
import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import pytz
import subprocess
import zipfile
import base64

##Import common library
import FrameWorkBase
import Parser
import Config
import Util
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Android import AndroidBaseUICore
from PageFactory.iOS import iOSBaseUICore

##Import selenium related module
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException

##Import this  only when system is windows
if Config._platform_ == "windows":
    ##Import autoit to handle windows upload file
    import autoit

##Import this only when system env is not ubuntu
if Config._platform_ != "linux" and Config._platform_ != "linux2":
    from PIL import ImageGrab
    from pyzbar.pyzbar import decode
    ##Declare windows keyboard and mouse action
    keyboard = PyKeyboard()
    mouse = PyMouse()

_DMODE_ = 0
_OCRValue_ = 0


hostsFileName = Config._SysDIR_ + '\\system32\\drivers\\etc\\hosts'
hostsFileNameBak = Config._SysDIR_ + '\\system32\\drivers\\etc\\hosts_bak'
hostsFileNameOrg = Config._CurDIR_ + '\\data\\HOSF\\hosts_org'
URLFFileNameOrg = Config._SysDIR_ + '\\system32\\drivers\\etc\\hosts.urlf'

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


def Suspend(arg):
    import msvcrt
    print "Press any key to continue..."
    msvcrt.getch()


def DDriven(arg):
    global _DMODE_
    _DMODE_ = 1
    argFiles = arg['argv'].split(',')
    dataLists = []
    cmbiLists = []
    cmbiCount = 0
    for argFile in argFiles:
        if os.path.exists(Config._CurDIR_ + '/data/ddriven/' + argFile):
            fname = os.path.basename(argFile)
            func = fname[:fname.index('-')]
            if func in Parser._XtFuncs_:
                # File content list
                lines = []
                for line in open(Config._CurDIR_ + '/data/ddriven/' + argFile):
                    line = line.strip()
                    if line:
                        lines.append(line)
                dataLists.append([func, lines])
                # File line number list
                lineLists = []
                for length in xrange(0, len(lines)):
                    lineLists.append(length)
                cmbiLists.append(lineLists)
            else:
                OK(1, 0)
                return
    # All combinations
    cmbiRest = list(itertools.product(*cmbiLists))
    for cmbine in cmbiRest:
        cmbiCount = cmbiCount + 1
        print "\t  DataDriven Combination #%d ---%s" % (cmbiCount, repr(cmbine))
        for length in xrange(0, len(cmbine)):
            func = dataLists[length][0]
            para = dataLists[length][1][cmbine[length]]
            print "\t\t" + func + "(" + para + ") in file[/data/ddriven/" + argFiles[length] + "] line(" + str(cmbine[length] + 1) + ")"
            Parser._CASE_['oktime'] = time.time()
            eval(func + "(" + para + ")")
    _DMODE_ = 0


def WGetURL(arg):
    '''WGetURL: Use wget.exe to browse URL given by URL and search specific keyword in web page by grep.exe
    Input argu : url - Web address
                 flag - control flag for wget.exe
                 value - keyword need to search
    Return code:  1 - Find keyword successfully
                 0 - Open browser successfully but cannot find keyword
                 -1 - Cannot open browser
                 others - return code of wget.exe
    Note: None'''
    ret = -1
    if 'flag' in arg:
        if 0 == int(arg['flag']):
            wgetcmd = Config._CurDIR_ + '\\bin\\x86\\wget.exe ' + \
                arg['url'] + ' -O web.html -o web.log'
        elif 1 == int(arg['flag']):
            wgetcmd = Config._CurDIR_ + \
                '\\bin\\x86\\wget.exe ' + arg['url']
        elif 2 == int(arg['flag']):
            wgetcmd = Config._CurDIR_ + '\\bin\\x86\\wget11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111.exe ' + \
                arg['url'] + ' -O web.html -o web.log'
        elif 3 == int(arg['flag']):
            wgetcmd = Config._CurDIR_ + '\\bin\\x86\\中文wget.exe ' + \
                arg['url'] + ' -O web.html -o web.log'
    else:
        wgetcmd = Config._CurDIR_ + '\\bin\\x86\\wget.exe ' + \
            arg['url'] + ' -O web.html -o web.log'
    os.system(wgetcmd)
    cmd = Config._CurDIR_ + '\\bin\\x86\\grep.exe -q -e "' + \
        arg['value'] + '" web.html'
    ret = os.system(cmd)
    if os.path.exists("web.html"):
        os.unlink("web.html")
    if os.path.exists("web.log"):
        os.unlink("web.log")
    OK(ret, int(arg['result']), 'WGetURL: ' + arg['url'])


def CURL(arg):
    '''CURL: Use curl.exe to browse URL given by URL and search specific keyword in web page by grep.exe
    Input argu : url - Web address
                 flag - control flag for curl.exe
                 value - keyword need to search
    Return code:  1 - Find keyword successfully
                 0 - Open browser successfully but cannot find keyword
                 -1 - Cannot open browser
                 others - return code of wget.exe
    Note: None'''
    ret = -1
    if 'flag' in arg:
        if 0 == int(arg['flag']):
            cmd = Config._CurDIR_ + \
                '\\bin\\x86\\curl.exe  -o web.html -L ' + arg['url']
            ret = os.system(cmd)
            if 'value' in arg:
                cmd = Config._CurDIR_ + '\\bin\\x86\\grep.exe -q -e ' + \
                    arg['value'] + ' web.html'
                ret = os.system(cmd)
            if os.path.exists("web.html"):
                os.unlink("web.html")
        elif 2 == int(arg['flag']):
            fileName = arg['url'].split("/")
            print fileName[-1]
            cmd = Config._CurDIR_ + '\\bin\\x86\\curl.exe  -o ' + \
                fileName[-1] + ' ' + arg['url']
            os.system(cmd)
            if os.path.exists(fileName[-1]):
                ##"File download successfully"
                ret = 1
                os.unlink(fileName[-1])
            else:
                ##"File download failed"
                ret = -1
    else:  # Default action, same as flag 0
        cmd = Config._CurDIR_ + \
            '\\bin\\x86\\curl.exe  -o web.html -L ' + arg['url']
        os.system(cmd)
        if 'value' in arg:
            cmd = Config._CurDIR_ + '\\bin\\x86\\grep.exe -q -e "' + \
                arg['value'] + '" web.html'
            ret = os.system(cmd)
        if os.path.exists("web.html"):
            os.unlink("web.html")
    OK(ret, int(arg['result']), 'CURL: ' + arg['url'])


def SHA1(arg):
    '''SHA1: Use hashlib module to calculate file's SHA1
        Input argu : result - 0
                     filepath - process file path
        Return code:  1 - Calculate success
                     0 - Cannot find file
        Note: None'''

    ret = 1
    sha1 = ''
    if os.path.isfile(arg['filepath']):
        filepath = arg['filepath']
        file = open(filepath, 'rb')
        hash = hashlib.sha1()
        hash.update(file.read())
        sha1 = hash.hexdigest()
        file.close
    else:
        filepath = 'File not exist !!'
        ret = 0
    OK(ret, int(arg['result']), 'Filepath:' + filepath + ' -> SHA1: ' + sha1)

def SHA256(arg):
    '''SHA256: Use hashlib module to calculate string's SHA256
        Input argu : string - the string that want to br encode by SHA256
        Return code: N/A
        Note: None'''

    sha256 = ""
    string = arg['string']
    dumplogger.info("Hash string : %s to SHA256" % (string))

    ##use sha256 in hashlib module to calculate string
    hash = hashlib.sha256()
    hash.update(string.encode('utf-8'))
    sha256 = hash.hexdigest()

    return sha256

def MD5(arg):
    '''MD5: Use hashlib module to calculate string's MD5
        Input argu : string - the string that want to br encode by MD5
        Return code: N/A
        Note: None'''
    string = arg['string']
    dumplogger.info("Hash string : %s to MD5" % (string))

    ##use MD5 in hashlib module to calculate string
    hash = hashlib.md5()
    hash.update(string)
    md5 = hash.hexdigest()

    return md5

def GetURL(arg):
    '''GetURL: Use selenium to open Browser to browse URL given by URL and search specific keyword in web page
    Input argu : url - Web address
                 type - Browser type, default is IE if it not given
                 value - keyword need to search
    Return code:  1 - Find keyword successfully
                 0 - Open browser successfully but cannot find keyword
                 -1 - Cannot open browser
                 99 - Maybe occured windows error
    Note: None'''
    from selenium import webdriver
    global _DMODE_
    driver = None
    ret = -1
    url = None
    msg = None
    contextMatch = None
    titleMatch = None
    getURLRet = ''
    titleMath_msg = ''
    contextMatch_msg = ''

    try:
        # Check browser type and assign driver(IE,Firefox,Chrome)
        if 'type' in arg:
            if 'IE' in arg['type']:
                iedriver = Config._CurDIR_ + '/bin/' + Config._Platform_ + '/IEDriverServer.exe'
                driver = webdriver.Ie(iedriver)
            elif 'Firefox' in arg['type']:
                path = Config._CurDIR_ + '/data/HTTPS/Firefox/'
                profile = webdriver.FirefoxProfile(path)
#               profile.default_preferences["webdriver_assume_untrusted_issuer"] = 'false'
#               profile.set_preference("webdriver_accept_untrusted_certs", True)
#               profile.set_preference("webdriver_assume_untrusted_issuer", True)
#               profile.update_preferences()
                driver = webdriver.Firefox(profile)
            elif 'Chrome' in arg['type']:
                path = Config._CurDIR_ + '/data/HTTPS/Chrome/'
                options = webdriver.ChromeOptions()
                options.add_argument('--allow-running-insecure-content')
                options.add_argument('--disk-cache-size=1')
                options.add_argument('--load-extension=c:/OPTest_Python/data/HTTPS/ChromeExt/')
                argu = '--user-data-dir=' + path
                options.add_argument(argu)
                options.add_argument('--enable-extensions')
                chromedriver = Config._CurDIR_ + '/bin/'
                Config._Platform_ + '/chromedriver.exe'
                driver = webdriver.Chrome(chromedriver, chrome_options=options)
        ##Default is IE
        else:
            iedriver = Config._CurDIR_ + '/bin/' + Config._Platform_ + '/IEDriverServer.exe'
            driver = webdriver.Ie(iedriver)

        time.sleep(2)
        ##Check driver have initialed
        if driver:
            driver.implicitly_wait(10)
            socket.setdefaulttimeout(80)

            try:
                retry = 0

                ##Note :Because open url will occured 99 error about "urlopen error [Errno 10061]No Connection could be made baceuse the target machine actively refuse it."
                ##So need add retry to open url
                while retry < 3:
                    # If open success, break loop!!
                    try:
                        getURLRet = driver.get(arg['url'])
                        break

                    ##If occurd 99 error will retry
                    except:
                        time.sleep(5)

                        ##Close and exit driver setting
                        driver.quit()

                        if 'type' in arg:
                            ##Because driver quit, so need re setting browser driver"
                            if 'IE' in arg['type']:
                                iedriver = Config._CurDIR_ + '/bin/' + Config._Platform_ + '/IEDriverServer.exe'
                                driver = webdriver.Ie(iedriver)
                            elif 'Firefox' in arg['type']:
                                path = Config._CurDIR_ + '/data/HTTPS/Firefox/'
                                profile = webdriver.FirefoxProfile(path)
                                driver = webdriver.Firefox(profile)

                        ##Default is IE
                        else:
                            iedriver = Config._CurDIR_ + '/bin/' + Config._Platform_ + '/IEDriverServer.exe'
                            driver = webdriver.Ie(iedriver)

                        retry += 1

                if 'action' in arg:
                    driver.implicitly_wait(10)
                    socket.setdefaulttimeout(80)
                    link = str(arg['link'])
                    continue_link = driver.find_element_by_partial_link_text(
                        link).click()
#                   continue_link.click()
            except socket.timeout:
                driver.quit()

            ##Open url success action, get title and body tag
            if not getURLRet:
                ##Because title tag sometimes will can't get, so need catch it.
                try:
                    titleElements = driver.title
                    if not titleElements:
                        titleElements = driver.find_elements_by_tag_name(
                            "title")
                    contextElements = driver.find_elements_by_tag_name("head")
                except:
                    titleElements = None
                    contextElements = None

                ##Note : Please unlock annotate as below, if you need debug senlinum get what's element.
                #p#rint titleElements
                ##print contextElements

            ##Open url fail action, retry again
            else:
                driver.implicitly_wait(10)
                socket.setdefaulttimeout(80)
                try:
                    getURLRet = driver.get(arg['url'])
                except socket.timeout:
                    driver.quit()

                ##Because title tag sometimes will can't get, so need catch it.
                if driver:
                    try:
                        titleElements = driver.title
                        if not titleElements:
                            titleElements = driver.find_elements_by_tag_name(
                                "title")
                        contextElements = driver.find_elements_by_tag_name(
                            "body")
                    except:
                        titleElements = None
                        contextElements = None

                ##Note : Please unlock annotate as below, if you need debug senlinum get what's element.
                ##print titleElements
                ##print contextElements

        if _DMODE_ == 1:
            print "\t\tDDriven Mode"
            OK(arg['pass'], titleElements, 'GetURL')
        else:
            while (not titleElements) and (not contextElements):
                driver.implicitly_wait(10)
                socket.setdefaulttimeout(80)

                try:
                    getURLRet = driver.get(arg['url'])
                except:
                    driver.quit()
                    time.sleep(5)
                    getURLRet = driver.get(arg['url'])

                ##Due to find_elements_by_tag_name perhaps occur exception, we need to use try & excpet to catch error
                try:
                    titleElements = driver.title
                    if not titleElements:
                        titleElements = driver.find_elements_by_tag_name(
                            "title")
                    contextElements = driver.find_elements_by_tag_name("body")
                except:
                    titleElements = None
                    contextElements = None
            ##Check what's code about url.
            if isinstance(titleElements, (unicode, str)):
                ##Check expect value occured in website title.
                titleMatch = re.search(
                    str(arg['value']), titleElements, re.IGNORECASE)

            else:
                ##Check expect value occured in website title.
                if titleElements:
                    try:
                        titleMatch = re.search(
                            str(arg['value']), titleElements, re.IGNORECASE)
                    except:
                        titleMath_msg = "No titile"

            ##Check expect value occured in website context.
            if contextElements:
                try:
                    contextMatch = re.search(
                        str(arg['value']), contextElements[0].text, re.IGNORECASE)
                except:
                    contextMatch_msg = "No context"

            ret = 1 if titleMatch or contextMatch else 0

        ##If website have trandition chinese !! But sometimes not stable
        if 'big5' in arg:
            if titleElements:
                if isinstance(titleElements, (unicode, str)):
                    msg = titleElements.encode('big5')
                else:
                    msg = titleElements.encode('big5')
            elif contextElements:
                msg = contextElements[0].text.encode('big5')
            else:
                msg = 'None'

            url = arg['url'].encode('big5')

        else:
            if titleElements:
                if isinstance(titleElements, (unicode, str)):
                    msg = titleElements
                else:
                    try:
                        msg = titleElements
                    except:
                        msg = "No title, so msg will empty"

            if contextElements:
                msg = contextElements[0].text
            else:
                msg = 'No context, so msg will empty'

            url = arg['url']

        if driver:
            driver.quit()
            driver = None

        if ret == 1:
            OK(ret, int(arg['result']), 'GetURL: ' + url)
        else:
            if 'big5' in arg:
                errStr = 'Expect keyword ' + arg['value'] + ' != '
                fromStr = ' from '
                err = errStr.encode('big5') + msg + \
                    fromStr.encode('big5') + url
                OK(ret, int(arg['result']), err)
            else:
                OK(ret, int(arg['result']), 'Expect keyword ' + arg['value'] + ' != ' + msg + ' from ' + url)
    finally:
        if driver:
            driver.quit()


def CheckLogData(arg):
    '''CheckLogData: Check specific keyword will appear in log
        Input argu : module - Web address
                     keyword - Browser type, default is IE if it not given
        Return code:  1 - Find keyword successfully
                      0 - Open browser successfully but cannot find keyword
                     -1 - Cannot open browser
                     99 - Maybe occured windows error
        Note: None'''

    module = arg['module']
    keyword = arg['key']
    rtn = arg['result']
    ret = 1
    message = ""

    ##default value is ""
    if 'value' in arg:
        value = arg['value']
    else:
        value = ""

    ##Please remember assign file path
    filepath = ""
    if os.path.isfile(filepath):
        for temp in open(filepath, "r"):
            if temp.find(keyword) != -1:
                message = keyword + value + " => Find keyword"
                if value != "":
                    if temp.find(value) != -1:
                        message = keyword + value + " => Find keyword and value"
                    else:
                        ret = 0
                        message = keyword + value + " => Find keyword but no value"
        if message == "":
            ret = 0
            message = keyword + " => Can't find keyword"
    else:
        message = filepath + " Log File not exist"
        ret = 2
    OK(ret, int(arg['result']), 'CheckLogData -> ' + message)


def IsServiceRunning(arg):
    '''IsServiceRunning: identify windows service status
    Input argu : name - service name
                 status - expect status
    Return code:  1 - success
                 -1 - Fail
    Note: The service nams must be exist, or testing will return 99 and exist'''
    import win32serviceutil
    ret = -1
    expectStatus = None
    serviceStatus = win32serviceutil.QueryServiceStatus(arg['name'])
    if arg['status'] == 'running':
        expectStatus = 4
    elif arg['status'] == 'stopped':
        expectStatus = 1
    elif arg['status'] == 'stop_pending':
        expectStatus = 3
    else:  # default exam status is running
        expectStatus = 4
    if serviceStatus[1] == expectStatus:
        ret = 1
    OK(ret, int(arg['result']), 'IsServiceRunning: service ' + arg['name'] + '\'s status is ' + str(serviceStatus[1]))

@DecoratorHelper.FuncRecorder
def CheckFileExistInPath(arg):
    '''CheckFileExistInPath: check if file exist in specific path
    Input argu :
                item - file type
                filetype - pdf / png / jpg
    Return code : 1 - success
                  0 - fail
                -1 - error
    '''
    ret = 1

    item = arg['item']
    filetype = arg['filetype']

    ##Setting path for all system
    slash = Config.dict_systemslash[Config._platform_]

    ##Setting file path
    if item == "invoice":
        file_name = item + "_" + GlobalAdapter.OrderE2EVar._OrderID_ + "." + filetype
        file_path = Config._CurDIR_ + slash + file_name

    dumplogger.info("FileName = %s" % (file_name))
    dumplogger.info("FilePath = %s" % (file_path))

    if os.path.exists(file_path):
        dumplogger.info("File exists.")
    else:
        ret = 0
        dumplogger.info("File NOT exists. please check your file path!")

    OK(ret, int(arg['result']), 'CheckFileExistInPath->')

@DecoratorHelper.FuncRecorder
def SleepTime(arg):
    '''
    SleepTime : Sleep specific time
            Input argu :
                second - sleep time, measure time by the second
            Return code :
                N/A
            Note: None
    '''
    ret = 1

    #print "Sleep %s seconds...  " % arg['second']
    time.sleep(int(arg['second']))

    OK(ret, int(arg['result']), 'SleepTime ' + arg['second'] + ' seconds')


def RevertHosts(arg={}):
    '''RevertHosts: replace specific host file on WINDIR\system32\etc\host
    Input argu : type - specific case
    Return code:  1 - success
                 -1 - Fail
    Note: None'''
    ret = -1
    if 'type' in arg:
        if 'H' in arg['type']:
            ret = shutil.copy(hostsFileNameOrg, hostsFileName)
        elif 'U' in arg['type']:
            ret = shutil.copy(hostsFileNameBak, hostsFileName)
    else:
        ret = shutil.copy(hostsFileNameBak, hostsFileName)

    time.sleep(5)
    if os.path.exists(hostsFileNameBak):
        os.unlink(hostsFileNameBak)

    if not ret:
        ret = 1
    OK(ret, int(arg['result']), 'RevertHost')


def AddHosts(arg):
    '''RevertHosts: replace specific host file on WINDIR\system32\etc\host
    Input argu : type - specific case
    Return code:  it always return Success
    Note: None'''
    ret = 1
    fileName = None
    if not os.path.exists(hostsFileName):
        RevertHosts()
    fileName = URLFFileNameOrg if ('type' in arg) and (
        'URLF' in arg['type']) else hostsFileNameBak
    shutil.copy(hostsFileName, fileName)
    szValue = arg['value'].split(",")
    with open(hostsFileName, "a") as f:
        for i in xrange(0, len(szValue)):
            f.write(szValue[i])
    f.close()
    OK(ret, int(arg['result']), 'AddHosts ' + arg['value'])


def AddDNServer(arg):
    '''AddDnsServer: Add DNS server ip
            Input argu : result - 0
                         action - add
                                - del
                         ip
            Return code:  1 - success
                         0 - Wrong action
            Note: None'''

    ip = arg['ip']
    action = arg['action']
    command = ''
    message = ''
    ret = 1

    if action == 'add':
        command = 'ipconfig /flushdns'
        os.system(command)
        command = 'netsh interface ip add dns name="esxi-vlab" addr=%s index=1' % (ip)
        os.system(command)
        message = 'Add Success'
    elif action == 'del':
        command = 'netsh interface ip delete dnsservers "esxi-vlab" %s' % (ip)
        os.system(command)
        message = 'Del Success'
    else:
        message = 'Wrong action'
        ret = 0

    OK(ret, int(arg['result']), 'Action: ' + action + ' -> DNS Server:' + ip + ' -> ' + message)


def CRC(arg):
    '''CRC: Use binascii module to calculate file or string CRC
        Input argu : result - 0
                     value - expect CRC value
                     objective(flag=0)- 1 -> string (default)
                              (flag=1)-0 -> file
        Return code:  1 - Calculate success
                     -1 - Compare fail
                     0 - Cannot find file
        Note: None'''

    import binascii
    import zlib

    objective = arg['objective']
    value = arg['value']
    flag = '0'
    temp = ''
    unsignedcrcHex = ''
    ret = 1

    if 'flag' in arg:
        flag = arg['flag']

        if(flag == '0'):
            signedcrc = binascii.crc32(objective)
            unsignedcrcHex = '0x%x' % (signedcrc & 0xffffffff)
        elif(flag == '1'):
            if os.path.isfile(objective):
                file = open(objective, 'rb')
                signedcrc = binascii.crc32(file.read())
                unsignedcrcHex = '0x%x' % (signedcrc & 0xffffffff)
            else:
                unsignedcrcHex = 'File not exist !!'
                ret = 0
    else:
        signedcrc = binascii.crc32(objective)
        unsignedcrcHex = '0x%x' % (signedcrc & 0xffffffff)

    if unsignedcrcHex == value:
        ret = 1
    else:
        ret = -1

    OK(ret, int(arg['result']), 'Flag:' + flag + ' -> Objective:' + objective + ' -> CRC:' + unsignedcrcHex + ' -> value:' + value)


def SHA1(arg):
    '''SHA1: Use hashlib module to calculate file's SHA1
        Input argu : result - 0
                     value - expect SHA1 value
                     objective(flag=0) - 1 -> file (default)
                              (flag=1) -0 -> string
        Return code:  1 - Calculate success
                     -1 - Compare fail
                     0 - Cannot find file
        Note: None'''

    objective = arg['objective']
    value = arg['value']
    ret = 1
    sha1 = ''
    flag = '0'

    if 'flag' in arg:
        flag = arg['flag']

        if(flag == '0'):
            if os.path.isfile(objective):
                file = open(objective, 'rb')
                hash = hashlib.sha1()
                hash.update(file.read())
                sha1 = hash.hexdigest()
                file.close
            else:
                filepath = 'File not exist !!'
                ret = 0
        elif(flag == '1'):
            hash.update(objective)
            sha1 = hash.hexdigest()
    else:
        if os.path.isfile(objective):
            file = open(objective, 'rb')
            hash = hashlib.sha1()
            hash.update(file.read())
            sha1 = hash.hexdigest()
            file.close
        else:
            filepath = 'File not exist !!'
            ret = 0

    if sha1 == value:
        ret = 1
    else:
        ret = -1

    OK(ret, int(arg['result']), 'Flag:' + flag + ' -> Objective:' + objective + ' -> SHA1: ' + sha1 + ' -> value:' + value)

def MouseAction(action, x, y, button):
    ''' MouseAction : Use PyUserInput module to simulate mouse event
            Input argu :
                action - move, click, release, current position
                x - coordinates
                y - coordinates
                button - 1(left), 2(right)
            Return code : N/A
            Note : None  '''

    ##取得目前滑鼠座標位置
    ##mouse.position()
    if "move" in action:
        ##滑鼠移動到x,y位置
        mouse.move(x, y)
    elif "click" in action:
        mouse.click(x, y, button)
    elif "release" in action:
        mouse.click(x, y, button)
    elif "scroll" in action:
        mouse.scroll(x, y)

def WindowsKeyboardEvent(action=None, value=None, number=None, interval=None):
    ''' WindowsKeyboardEvent : Use PyUserInput module to simulate keyboard event
        Input argu :
            action - press, release, tap, press_keys, type_string, p_shift, r_shift
            value - string
            number -
            interval -
        Return code : N/A
        Note : None  '''

    dumplogger.info("Enter WindowsKeyboardEvent")
    try:
        if 'press' in action:
            keyboard.press_key(value)
        elif 'release' in action:
            keyboard.release_key(value)
        elif 'tap' in action:
            keyboard.tap_key(value, number,float(interval))
        elif 'type_string' in action:
            keyboard.type_string(value, float(interval))
        elif 'p_shift' in action:
            keyboard.press_key(keyboard.shift_key)
        elif 'r_shift' in action:
            keyboard.release_key(keyboard.shift_key)
        elif 'enter' in action:
            keyboard.press_key(keyboard.enter_key)
        elif 'esc' in action:
            keyboard.press_key(keyboard.escape_key)

    except:
        print traceback.print_exc()
        dumplogger.exception("Got exception error")

@DecoratorHelper.FuncRecorder
def PrintScreen(arg):
    ''' PrintScreen : Print screen for captcha
            Input argu :
                file_name - Print screen output file name contain this string
                mode - web / web_opencv / android / android_opencv / ios / ios_opencv / element
            Return code : 1 - success
                         0 - fail
                         -1 - error
            Note : None  '''
    dumplogger.info(arg)

    file_name = arg['file_name']
    mode = arg['mode']
    ret = 1
    message = ""
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##If output folder not exist, created !!
    if os.path.exists(output_casename_folder):
        dumplogger.info(output_casename_folder)
    else:
        if Config._platform_ is "darwin":
            command = "mkdir -p " + output_casename_folder
        else:
            command = "mkdir " + output_casename_folder

        dumplogger.info("command = " + command)
        os.system(command)

    ##Save the screen shot by Windows or WebDriver
    try:
        ##Prepare output file path for opencv
        if "opencv" in mode:
            output_file_path = output_casename_folder + "ForOpenCV.png"
        ##Prepare output file path for test case
        else:
            output_file_path = output_casename_folder + Parser._CASE_['id'] + "_" + file_name + ".png"

        ##PC web print screen
        if "web" in mode:
            BaseUICore.GetBrowserScreenShot(output_file_path)
        ##Android print screen
        elif "android" in mode:
            AndroidBaseUICore._AndroidDriver_.get_screenshot_as_file(output_file_path)
        ##iOS print screen
        elif "ios" in mode:
            iOSBaseUICore._iOSDriver_.get_screenshot_as_file(output_file_path)
        ##Screenshot specific element only
        elif mode == "element":
            GlobalAdapter.CommonVar._PageElements_.screenshot(output_casename_folder + "element.png")
        else:
            message = "Please check your mode"
            ret = 0

    except:
        ret = -1
        message = "Image save error"
        dumplogger.exception('Got exception error')

    OK(ret, int(arg['result']), 'PrintScreen:' + message)


def HandleNonBig5Encode(arg):
    ''' HandleNonBig5Encode : Will help to remove non big5 string in list
            Input argu :
                string - list
            Return code : 1 - success
                         0 - fail
                         -1 - error
            Note : None  '''

    ret = 1
    string = arg
    # print "------------HandleNonBig5Encode------------------"
    # Remove non big5 encode string
    try:
        exclude_list = ['‧', '～', '裏', '™', '´', '恒', '。', '®', '﹑', '©', '碁',
                        '銹', '墻', '粧', '嫺', '한국어', 'ñ', 'ê', 'ç', 'и', 'й', 'к', 'Р', '∗']
        for exclude_list_temp in exclude_list:
            if exclude_list_temp in string:
                string = string.replace(exclude_list_temp, ' ')
    except:
        ret = 0
    # print "--------------------------------------------W-----"
    OK(ret, 0, 'HandleNonBig5Encode:')
    print "QQQQ"
    return string


def OCR(arg):
    ''' OCR : Identify captcha file and return result
            Input argu :
                string - list
            Return code : numer - result
            Note : None  '''

    # Assign OCR number to global
    global _OCRValue_
    ret = 1
    rtn = 0
    filepath = arg

    try:
        import pytesseract
        from PIL import Image
    except ImportError:
        print "Error, Please install pytesseract and PIL"
        raise SystemExit

    # Open file and identify it
    image = Image.open(filepath)
    number = pytesseract.image_to_string(image)
    print "The captcha is:%s" % (number)

    _OCRValue_ = number
    return number
    #OK(ret, rtn, 'OCR:' + str(number))


def RunSIKULI(arg):
    ''' RunSIKULI : Launch Sikuli to identify specific pict, but you need create sikuli script first.
            Input argu :
                project - Your project name
                                act - Your act
            Return code : 1 - success
                         0 - fail
                         -1 - error
            Note : None  '''

    ret = 1
    project = arg['project']
    act = arg['action']

    ##Run lt_wow.SIKULI and parse log
    #SIKULI_debuglog_path = 'C:\\QATest\\data\\' + project + "\\" + "sikuli_" + project + '_log.txt'
    SIKULI_debuglog_path = Config._CurDIR_ + '\\data\\' + \
        project + "\\" + "sikuli_" + project + '_log'
    print SIKULI_debuglog_path

    #SIKULI_suite_path = 'C:\\QATest\\data\\' + project + "\\" + act + ".sikuli"
    SIKULI_suite_path = Config._CurDIR_ + \
        '\\data\\' + project + "\\" + act + ".sikuli"
    #SIKULI_suite_path = Config._CurDIR_ + '\\data\\' + project + "\\" + act + ".skl"
    print SIKULI_suite_path

    # You can add more script for any project
    #command = Config._CurDIR_ + '\\tools\Sikuli\\runIDE.cmd -d 2 -f ' + SIKULI_debuglog_path + ' -r ' + SIKULI_suite_path + ' --args ' + act
    #command = Config._CurDIR_ + '\\tools\\Sikuli\\runIDE.cmd -d 2 -r ' + SIKULI_suite_path + ' --args ' + act + ' > ' + SIKULI_debuglog_path
    command = Config._CurDIR_ + '\\tools\\Sikuli\\runsikulix.cmd -d 2 -r ' + \
        SIKULI_suite_path + ' --args ' + act + ' > ' + SIKULI_debuglog_path
    print command
    dumplogger.info("Command = " + command)
    os.system(command)
    time.sleep(4)

    #MouseAction("click", 896, 213, 1)
    # time.sleep(1)

    ##parse Sikuli log messages
    if "key" in arg.keys():
        key = arg['key']
        ret = ParseSikuliLog(SIKULI_debuglog_path, project, key)

        ##Do not pass from ok
        if "passok" in arg.keys():
            dumplogger.info("Do not pass from ok.")
        else:
            OK(ret, int(arg['result']), 'RunSIKULI')
    else:
        print "Skipping to parse Sikuli log messages"

        if "passok" in arg.keys():
            dumplogger.info("Do not pass from ok.")
        else:
            OK(ret, int(arg['result']), 'RunSIKULI')


def ParseSikuliLog(SIKULI_debuglog_path, project, key):
    ''' ParseSikuliLog : Parse third-party log for sikuli
                    Input argu :
                            string - list
                            Return code :   1 - success
                                           0 - fail
                                            -1 - error
                            key - keyword
                    Note : None  '''

    ret = 1
    SIKULI_debuglog_path = Config._CurDIR_ + '\\data\\' + \
        project + "\\" + "sikuli_" + project + '_log'

    f = open(SIKULI_debuglog_path)
    lines = f.read().splitlines()
    message = ""
    '''
    for line in lines:
        print line
        if key in line:
            ret = 1
            print "Find PASS -> " + line
        else:
            ret = 0
            print "Find Fail -> " + line
    '''
    if key in lines:
        ret = 1
        print "Find PASS -> " + key
    else:
        ret = 0
        print "Find Fail -> " + "Fail to recognize"

        '''
        if 'user' in line:
            print "[SIKULI][user] " + line.encode("big5")
        elif 'error' in line:
            print "[SIKULI][error] " + line.encode("big5")
            #ret = 0
        '''

    f.close()
    return ret

def GetCurFunctionName():
    ''' GetCurFunctionName : Get currrent function name
            Input argu :
            Return code : 	1 - success
                           0 - fail
                           -1 - error
            Note : None  '''

    import inspect

    ##Get caller function name
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    dumplogger.info("caller name:" + calframe[1][3])
    return calframe[1][3]

@DecoratorHelper.FuncRecorder
def GetJSONContentFromFile(arg):
    ''' GetJSONContentFromFile : Get json content from file
            Input argu :
                    jsonfile - file path name to read and store
            Return code :
                    1 - success
                   0 - fail
                    -1 - error
    '''
    ret = 1
    dumplogger.info("Enter GetJSONContentFromFile")
    jsonfile = arg["jsonfile"] + ".json"

    try:
        ##Assign json data to global variable
        with open(jsonfile) as data_file:
            GlobalAdapter.CommonVar._JSONRawDataFromFile_ = json.load(data_file)

        dumplogger.info("After assign -> _JSONRawDataFromFile_ = %s" % (GlobalAdapter.CommonVar._JSONRawDataFromFile_))

    except IOError:
        dumplogger.exception("Don't find json file -> " + jsonfile)
        ret = 0

    except:
        dumplogger.exception("Load json file error -> " + jsonfile)
        ret = 0

    ##Return result
    OK(ret, int(arg['result']), 'GetJSONContentFromFile - ' + jsonfile)

@DecoratorHelper.FuncRecorder
def ResetWebElementsList(arg):
    ''' ResetWebElementsList : To reset global adapter WebElementsList
            Input argu : reset_element - specific element you want to clear, leave blank to clear all.
                                        - orderid, sellerid, productid, payment, count, chat, personalinfo
            Return code : 1 - success
                       0 - fail
                        -1 - error
    '''

    ret = 1
    reset_element = arg["reset_element"]
    dumplogger.info("reset_element = " + reset_element)

    if reset_element:
        ##Reset the element by the element user given
        if reset_element == 'ordersn':
            GlobalAdapter.OrderE2EVar._OrderSNDict_ = {}
        elif reset_element == 'order_timestamp':
            GlobalAdapter.OrderE2EVar._OrderTimestampDict_ = {}
        elif reset_element == 'promotionid':
            GlobalAdapter.PromotionE2EVar._PromotionIDList_ = []
        elif reset_element == 'return_product_name':
            GlobalAdapter.OrderE2EVar._ReturnProductNameList_ = []
        elif reset_element == 'count':
            GlobalAdapter.GeneralE2EVar._Count_ = []
        elif reset_element == 'flashsale':
            GlobalAdapter.PromotionE2EVar._FlashSaleIDList_ = []
        elif reset_element == 'personalinfo':
            GlobalAdapter.AccountE2EVar._PersonalInfoList_ = []
        elif reset_element == 'shopee_voucher':
            GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_ = []
        elif reset_element == 'seller_voucher':
            GlobalAdapter.PromotionE2EVar._SellerVoucherList_ = []
        elif reset_element == 'free_shipping_voucher':
            GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_ = []
        elif reset_element == 'dp_voucher':
            GlobalAdapter.PromotionE2EVar._DPVoucherList_ = []
        elif reset_element == 'offline_payment_voucher':
            GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_ = []
        elif reset_element == 'records_count':
            GlobalAdapter.ShopCollectionE2EVar._RecordsCount_ = []
        elif reset_element == 'product_collection_id':
            GlobalAdapter.ProductCollectionE2EVar._CollectionID_ = ""
        elif reset_element == 'fraud_tag':
            GlobalAdapter.AccountE2EVar._FraudTagList_ = []
        else:
            dumplogger.info("reset_element not defined")

        dumplogger.info('ResetWebElementsList ->' + reset_element)

    else:
        ##Clear all elements to default
        GlobalAdapter.OrderE2EVar._OrderSNDict_ = {}
        GlobalAdapter.OrderE2EVar._OrderTimestampDict_ = {}
        GlobalAdapter.PromotionE2EVar._PromotionIDList_ = []
        GlobalAdapter.OrderE2EVar._ReturnProductNameList_ = []
        GlobalAdapter.GeneralE2EVar._Count_ = []
        GlobalAdapter.PromotionE2EVar._FlashSaleIDList_ = []
        GlobalAdapter.AccountE2EVar._PersonalInfoList_ = []
        GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_ = []
        GlobalAdapter.PromotionE2EVar._SellerVoucherList_ = []
        GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_ = []
        GlobalAdapter.AccountE2EVar._FraudTagList_ = []

        dumplogger.info('Reset All WebElementsList')

    OK(ret, int(arg['result']), 'ResetWebElementsList ->' + reset_element)


import inspect
def caller_name(skip=2):
    """Get a name of a caller in the format module.class.method

       `skip` specifies how many levels of stack to skip while getting caller
       name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.
       An empty string is returned if skipped levels exceed stack height
    """
    stack = inspect.stack()
    start = 0 + skip
    if len(stack) < start + 1:
        return ''
    parentframe = stack[start][0]

    name = []
    module = inspect.getmodule(parentframe)
    ##`modname` can be None when frame is executed directly in console
    ##TODO(techtonik): consider using __main__
    if module:
        name.append(module.__name__)
    ##detect classname
    if 'self' in parentframe.f_locals:
        ##I don't know any way to detect call from the object method
        ##XXX: there seems to be no way to detect static method call - it will
        ##     be just a function call
        name.append(parentframe.f_locals['self'].__class__.__name__)
    codename = parentframe.f_code.co_name

    ##Top level usually
    if codename != '<module>':
        ##Function or a method
        name.append(codename)

    del parentframe
    return ".".join(name)

def CallMe(arg):
    #GetElementPath(GetCurFunctionName(), "bank")
    global _CurrentFunction_

    import inspect
    print os.path.basename(__file__)
    print type(os.path.basename(__file__))
    temp = os.path.basename(__file__)
    list_tmp = temp.split(".")
    filename = list_tmp[0]
    print filename

    _CurrentFunction_ = GetCurFunctionName()
    GetElementPath(filename, "bank")

    print "---------------"
    print Parser._CASE_['funcname']
    print "---------------"


def NewCall(arg):
    ret = 1
    print "Enter NewCall"
    print arg

    OK(ret, int(arg['result']), 'NewCall ->')

def GetCurrentDateTime(format_string, days=0, minutes=0, seconds=0, is_tw_time=0):
    ''' GetCurrentDateTime : Get current date time or Get future/pass date time
            Input argu :
                format_string - datetime format string
        Note : None
    '''

    if is_tw_time:
        region = "TW"
    else:
        region = Config._TestCaseRegion_
    dumplogger.info("Time zone: " + region)

    ##Set timezone
    regional_time_zone = pytz.timezone(Config._TimeZone_[region])
    datetime_string = (regional_time_zone.normalize(datetime.datetime.now(pytz.utc) + datetime.timedelta(days=days, minutes=minutes, seconds=seconds))).strftime(format_string)
    dumplogger.info("Time: " + datetime_string)
    return datetime_string

@DecoratorHelper.FuncRecorder
def ControlInternetByVPN(arg):
    ''' ControlInternetByVPN : Control whether to use VPN (Cisco AnyConnect) in the internet
            Input argu :
                control - connect/disconnect
            Note : None
    '''
    dumplogger.info("Enter ControlInternetByVPN")

    ret = 1

    control = arg["control"]

    vpn_info = Config._CurDIR_ + r"\data\Common\Cisco_AnyConnect_Info.txt"

    ##Connect or disconnect
    if control == "connect":
        dumplogger.info("Start to connect VPN...")
        vpn_cmd = r'"C:\Program Files (x86)\Cisco\Cisco AnyConnect Secure Mobility Client\vpncli.exe" -s < ' + vpn_info

    elif control == "disconnect":
        dumplogger.info("Start to disconnect VPN...")
        vpn_cmd = r'"C:\Program Files (x86)\Cisco\Cisco AnyConnect Secure Mobility Client\vpncli.exe" disconnect'

    os.system(vpn_cmd)

    OK(ret, int(arg['result']), 'ControlInternetByVPN ->' + control)

def CreateRGBHist(image):
    ''' CreateRGBHist : Create RGB hist image
            Input argu :
                image - resized image
            Note : None
    '''
    height, width, color = image.shape

    ##Create（16*16*16,1) for initial matrix, also for hist matrix
    rgbhist = np.zeros([16 * 16 * 16, 1], np.float32)
    bsize = 256 / 16
    for row in range(height):
        for col in range(width):
            blue = image[row, col, 0]
            green = image[row, col, 1]
            red = image[row, col, 2]

            ##Build hist index manually
            index = int(blue / bsize) * 16 * 16 + int(green / bsize) * 16 + int(red / bsize)
            rgbhist[int(index), 0] += 1
    plt.ylim([0, 10000])
    plt.grid(color='r', linestyle='--', linewidth=0.5, alpha=0.3)
    return rgbhist

def HistCompare(hist1, hist2):
    ''' HistCompare : Hist compare function
            Input argu :
                hist1 - hist for image
                hist2 - hist for target
            Note : None
    '''
    ##Should create hist image initially
    ##Use algorithm to compare two hist image (The more similarity, the more coefficient closer to 1)
    match = cv2.compareHist(hist1, hist2, cv2.HISTCMP_CORREL)

    return match

def HandleImage(image):
    ''' HandleImage : Resize and gray scale the image
            Input argu :
                hist1 - hist for image
                hist2 - hist for target
            Note : None
    '''
    ##Handle image to specific size and create specific hist
    image = cv2.resize(image, (100, 100))
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    image[:, :, 2] = cv2.equalizeHist(image[:, :, 2])
    image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)
    return image

def MatchImgByOpenCV(arg):
    ''' MatchImgByOpenCV : Using OpenCV + numpy to identify two image are match or not.
            Input argu: env - staging/test
                        image - image name
                        threshold - coefficient value
                        is_click - no/yes (default is no, means do not click)
                        mode - android/web/ios
                        Return code:1 - success
                                   0 - fail
                                    -1 - error
    '''

    env = arg['env']
    image = arg['image']
    ##Coefficient value
    threshold = arg['threshold']
    is_click = arg['is_click']
    mode = arg['mode']

    if "passok" in arg:
        passok = arg['passok']
    else:
        passok = "1"

    ##Hold on 2 secs for waiting page has load complete
    time.sleep(2)

    ##Setting path for all system
    slash = Config.dict_systemslash[Config._platform_]
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##The image that will be compared with screenshot
    image_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + env + slash + Config._TestCasePlatform_.lower() + slash + Config._TestCaseFeature_ + slash + 'compare' + slash + image + '.png'
    dumplogger.info(image_path)

    ##Get ScreenShot
    dumplogger.info('Starting screenshot current page.')
    PrintScreen({"file_name": "", "mode": mode + "_opencv", "result": "1"})

    ##The save path of screenshot image
    screenshot_path = output_casename_folder + "ForOpenCV.png"
    dumplogger.info(screenshot_path)

    ##Read image from selenium or file
    dumplogger.info('Reading screenshot image.')
    img_rgb = cv2.imread(screenshot_path)
    ##When file load from OpenCV, it will be stored by BGR(Not RGB)
    dumplogger.info('Change img color from rgb to gray scale.')
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

    ##Read target image that you want to compare
    dumplogger.info('Reading target image that we want to compared with.')
    template = cv2.imread(image_path, 0)

    ##Get shape for img, so we can get weight and height
    weight, height = template.shape[::-1]

    ##Match pic by Coefficient
    dumplogger.info('Starting compare two images.')
    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)

    ##Get position
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    ##Calculate position for picture
    left_top = max_loc
    right_bottom = (left_top[0] + weight, left_top[1] + height)

    ##Get the center point in picture
    dumplogger.info('Calculating center point in screenshot.')
    cent_x = (left_top[0] + right_bottom[0]) / 2
    cent_y = (left_top[1] + right_bottom[1]) / 2

    ##Get match area if greater than coefficient value
    dumplogger.info('Checking match area rate is greater than coefficient value.')
    loc = np.where(res >= float(threshold))

    ##Draw rectangle when got match from picture
    ret = 0
    crop_img = None
    pt_count = 0
    crop_limit = 8
    for pt in zip(*loc[::-1]):
        ret = 1
        crop_img = img_rgb[pt[1]:pt[1]+height, pt[0]:pt[0]+weight]
        if pt_count < crop_limit:
            cv2.imwrite(output_casename_folder + 'ForOpenCV_' + str(pt_count) + '.png', crop_img)
        pt_count += 1

    ##Init crop count and crop list
    lst_filename = []
    total_crop_count = 0

    ##Count total crop image number
    for count in range(pt_count-1, -1, -1):
        lst_filename.append('ForOpenCV_' + str(count) + '.png')
        total_crop_count += 1
    dumplogger.info("Total crop count: %d" % (total_crop_count))

    ##Color compare with crop image
    p2_ret = 0
    boundary = max(total_crop_count-crop_limit,0)
    for crop_count in range(total_crop_count-1, boundary-1, -1):
        ##Create same size image for crop images
        dumplogger.info(lst_filename[crop_count])
        img1 = cv2.imread(output_casename_folder + lst_filename[crop_count])
        img1 = HandleImage(img1)

        ##Create same size image for target image
        dumplogger.info(image_path)
        img2 = cv2.imread(image_path)
        img2 = HandleImage(img2)

        ##Create hist for both images
        hist1 = CreateRGBHist(img1)
        hist2 = CreateRGBHist(img2)
        dumplogger.info('Hist compare coefficient: %f' % (HistCompare(hist1, hist2)))

        img1 = cv2.imread(output_casename_folder + lst_filename[crop_count])
        img2 = cv2.imread(image_path)

        dumplogger.info('PSNR score: %f' % (cv2.PSNR(img1, img2)))

        if HistCompare(hist1, hist2) > 0.4 or cv2.PSNR(img1, img2) > 15:
            dumplogger.info('Correlation')
            p2_ret = 1
            break
        else:
            dumplogger.info('Not correlation')
            p2_ret = 0

    ##Must match both check
    dumplogger.info('Shape match result: %d' % (ret))
    dumplogger.info('Color compare result: %d' % (p2_ret))
    ret = ret * p2_ret

    ##If matching image failed in retry scenario and (result = 1 or passok = 0), go to recropping stage.passok=0(which means it won't do the retry)
    if ret == 0 and int(arg['result']) == 1 and (Parser._CASE_['status'] == 1 or int(passok) == 0):
        ##Dump message go to recropping stage and show the max threshold
        dumplogger.info('Go into recropping stage, current max threshold: ' + str(max_val))

        ##Check if the output of recropped image fold exists
        if not os.path.exists((output_casename_folder + '\\' + Parser._CASE_['id'])):
            ##Create a folder for storing the images
            os.makedirs((output_casename_folder + '\\' + Parser._CASE_['id']))

        recropping_image_output_path = output_casename_folder + '\\' + Parser._CASE_['id'] + '\\'

        try:
            ##Get max threshold x y
            recrop_x = max_loc[0]
            recrop_y = max_loc[1]

            ##Draw a rectangle of recropping scope, expand the scope 10 pixels prevent the border blocks some pixels
            cv2.rectangle(img_rgb, (recrop_x - 10, recrop_y - 10), (recrop_x + weight + 10, recrop_y + height + 10), (0,0,255), 2)

            ##Recrop the image
            recrop_img = img_rgb[recrop_y:recrop_y + height, recrop_x:recrop_x + weight]

            ##Write it to the output
            cv2.imwrite(recropping_image_output_path + 'Rectangle_Recropping_Scope' + '.png', img_rgb)
            cv2.imwrite(recropping_image_output_path + image + '.png', recrop_img)

            ##Get origin target image and recrop target image with RGB
            new_template_path = recropping_image_output_path + image + '.png'
            new_template = cv2.imread(new_template_path)
            origin = cv2.imread(image_path)

            ##Find the difference between this two image
            difference = cv2.subtract(origin, new_template)
            difference_gray = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
            mask = cv2.threshold(difference_gray, 0, 255,cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

            ##Mask the difference on origin image
            origin[mask != 255] = [0, 0, 255]

            ##Write it to the output
            cv2.imwrite(recropping_image_output_path + 'Origin_Difference_Mask' + '.png', origin)

        except cv2.error:
            dumplogger.exception("Occur exception about cv2 error!!")

        except:
            dumplogger.exception("Some errors occur while recropping image")

    ##Check if you want to click the target image for redirect to other page (Only click if match successfully)
    if is_click == "yes" and ret == 1:
        if mode == "web":
            ##Click by selenium if using remote driver
            if Config._RemoteSeleniumDriver_:
                dumplogger.info('Move mouse to appoint position and click by selenium.')
                BaseUICore.ClickByElementXYCoordinates({"locate_x":int(cent_x), "locate_y":int(cent_y), "result": "1"})
            ##For local user, stay with PyMouse click function
            else:
                dumplogger.info('Move mouse to appoint position and click by PyMouse.')
                MouseAction("click", int(cent_x), int(cent_y), 1)
        elif mode == "android":
            dumplogger.info('Click Coordinates on Android device.')
            ret = AndroidBaseUICore.AndroidClickCoordinates({"x_cor":int(cent_x), "y_cor":int(cent_y)})
            dumplogger.info('The result of click coordinates: %s, 1 means pass and -1 means failed' % (ret))
        #elif mode == "ios":
            #dumplogger.info('Calculating new x,y for ios device.')

            ##Get screen resolution x, y point on appium
            #device_x = Config._MobileDeviceInfo_[Config._DeviceID_]['device_x']
            #device_y = Config._MobileDeviceInfo_[Config._DeviceID_]['device_y']

            ##Get screen resloution x, y point on ios
            #resolution_x = Config._MobileDeviceInfo_[Config._DeviceID_]['resolution_x']
            #resolution_y = Config._MobileDeviceInfo_[Config._DeviceID_]['resolution_y']

            ##Calculating new x and y point
            #new_cent_x = (int(cent_x) * int(device_x)) / int(resolution_x)
            #new_cent_y = (int(cent_y) * int(device_y)) / int(resolution_y)
            #dumplogger.info('Got new coordinate -> x: %s, y: %s' % (new_cent_x, new_cent_y))

            #dumplogger.info('Click Coordinates on ios device.')
            #print "x:",str(int(new_cent_x)),"y:",str(int(new_cent_y))
            #iOSBaseUICore.iOSClickCoordinates({"x_cor":int(new_cent_x), "y_cor":int(new_cent_y), "result": "1"})
    elif is_click == "no":
        dumplogger.info("No need to click target image.")
    time.sleep(2)

    if passok == "1":
        OK(ret, int(arg['result']), 'MatchImgByOpenCV -> ' + image + '.png')
    else:
        return ret

@DecoratorHelper.FuncRecorder
def InputFilePathWithWindowsAutoIt(arg):
    ''' InputFilePathWithWindowsAutoIt : Using autoit to input file path on windows environment
            Input argu: action - image/file
                        file_name - file name
                        file_type - file type
                        window_title - window title (ex: [Class:#32770; Title:開啟])
                        input_field - file path input field (ex: [Class:Edit; Instance:1])
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    ret = 1
    action = arg["action"]
    file_name = arg["file_name"]
    file_type = arg["file_type"]
    window_title = arg["window_title"]
    input_field = arg["input_field"]

    ##Setting path for all system
    slash = Config.dict_systemslash[Config._platform_]

    ##Setting file path
    file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_.lower() + slash + Config._TestCaseFeature_ + slash + action + slash + file_name + '.' + file_type

    try:
        ##Wait 10 second to wait until upload window is appear
        autoit.win_wait(window_title, 10)
        dumplogger.info("Upload window appear within 10 second")

        ##Input file path in upload window
        autoit.control_set_text(window_title, input_field, file_path)
        dumplogger.info("Input file path in upload window success => %s" % (file_path))

    except WindowsError:
        dumplogger.exception("Occur exception about windows exception!!!")
        ret = -1

    except:
        dumplogger.exception("Occur other exception!!!")
        ret = -1

    OK(ret, int(arg['result']), 'InputFilePathWithWindowsAutoIt')

@DecoratorHelper.FuncRecorder
def ClickButtonWithWindowsAutoIt(arg):
    ''' ClickButtonWithWindowsAutoIt : Using autoit to click button in windows environment
            Input argu: window_title - window title (ex: [Class:#32770; Title:開啟])
                        button_name - button name to click (ex 開啟: [Class:Button; Instance:1] / 取消: [Class:Button; Instance:2])
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    ret = 1
    window_title = arg["window_title"]
    button_name = arg["button_name"]

    try:
        ##Wait 10 second to wait until upload window is appear
        autoit.win_wait(window_title, 10)
        dumplogger.info("Upload window appear within 10 second")

        ##Click button in upload window
        autoit.control_click(window_title, button_name)
        dumplogger.info("Click button success")

    except WindowsError:
        dumplogger.exception("Occur exception about windows exception!!!")
        ret = -1

    except:
        dumplogger.exception("Occur other exception!!!")
        ret = -1

    OK(ret, int(arg['result']), 'ClickButtonWithWindowsAutoIt')

@DecoratorHelper.FuncRecorder
def UploadFileWithWindowsAutoIt(arg):
    ''' UploadFileWithWindowsAutoIt : Using autoit to upload file in windows environment
            Input argu: project - your project name
                        action - image/file
                        file_name - file name
                        file_type - file type
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''

    ret = 1
    action = arg["action"]
    file_name = arg["file_name"]
    file_type = arg["file_type"]

    ##Input file path in upload window
    InputFilePathWithWindowsAutoIt({"action":action, "file_name":file_name, "file_type":file_type, "window_title":u"[Class:#32770; Title:開啟]", "input_field":"[Class:Edit; Instance:1]", "result": "1"})

    ##Click open button in upload window
    ClickButtonWithWindowsAutoIt({"window_title":u"[Class:#32770; Title:開啟]", "button_name":"[Class:Button; Instance:1]", "result": "1"})

    OK(ret, int(arg['result']), 'UploadFileWithWindowsAutoIt')

def MatchPictureImg(arg):
    ''' MatchPictureImg : Check picture banner exist or not
            Input argu :
                image - OpenCV image to upload
                env - staging / uat / test
                expected_result - 1 (match) / 0 (not match)
            Return code : 1 - success
                          0 - fail
                         -1 - error
    '''
    dumplogger.info("Enter MatchPictureImg!")
    mode = arg["mode"]
    image = arg["image"]
    env = arg["env"]
    threshold = arg["threshold"]
    expected_result = arg["expected_result"]

    ##Check banner exist with OpenCV
    pic_match_result = 1
    match_time = 0

    ##If image campare result is expected, return True
    dumplogger.info("expected_result -> %s" % (expected_result))
    for match_retry_times in range(1, 11):
        is_picture_match = MatchImgByOpenCV({"env":env, "mode":mode, "image":image, "threshold":threshold, "is_click":"no", "result": "1", "passok": "0"})
        if is_picture_match == int(expected_result):
            dumplogger.info("is_picture_match -> %d" % (is_picture_match))
            dumplogger.info("result is the same!")
            pic_match_result = 1
            match_time = match_retry_times

            ##Expect exist : match success one time is enough
            if int(expected_result) == 1:
                break
        else:
            dumplogger.info("is_picture_match -> %d" % (is_picture_match))
            dumplogger.info("result is not the same!")
            pic_match_result = 0
            match_time = match_retry_times

            ##Expect not exist : if image exist once, then match shouldn't pass
            if int(expected_result) == 0:
                break

    ##Output match time
    dumplogger.info("The frame match time -> %d" % (match_time))

    return pic_match_result

def MatchGIFImg(arg):
    ''' MatchGIFImg : Check gif banner exist or not
            Input argu :
                image1 - first frame of the GIF
                image2 - second frame of the GIF
                env - staging / uat / test
                expected_result - 1 (match) / 0 (not match)
            Return code : 1 - success
                          0 - fail
                        -1 - error
    '''
    dumplogger.info("Enter MatchGIFImg!")
    mode = arg["mode"]
    image1 = arg["image1"]
    image2 = arg["image2"]
    env = arg["env"]
    expected_result = arg["expected_result"]

    ##Check gif frame exist with OpenCV
    gif_match_result = 1
    first_retry_time = 0
    second_retry_time = 0
    is_first_frame_match = 0
    is_second_frame_match = 0

    ##Check if both images show up, if one shows up, we won't check that again
    for gif_match_retry_times in range(1, 11):
        ##Check gif first frame
        if not is_first_frame_match:
            is_first_frame_match = MatchImgByOpenCV({"env":env, "mode":mode, "image":image1, "threshold":"0.9", "is_click":"no", "result": "1", "passok": "0"})
            dumplogger.info("is_first_frame_match -> %d" % (is_first_frame_match))
            first_retry_time = gif_match_retry_times
        else:
            first_retry_time = gif_match_retry_times
            break

    for gif_match_retry_times in range(1, 11):
        ##Check gif second frame
        if not is_second_frame_match:
            is_second_frame_match = MatchImgByOpenCV({"env":env, "mode":mode, "image":image2, "threshold":"0.9", "is_click":"no", "result": "1", "passok": "0"})
            dumplogger.info("is_second_frame_match -> %d" % (is_second_frame_match))
            second_retry_time = gif_match_retry_times
        else:
            second_retry_time = gif_match_retry_times
            break

    ##If both frame match expect result, set ret to 0
    if is_first_frame_match == int(expected_result) and is_second_frame_match == int(expected_result):
        gif_match_result = 1
        dumplogger.info("The first frame match time -> %d" % (first_retry_time))
        dumplogger.info("The second frame match time -> %d" % (second_retry_time))
    else:
        dumplogger.info("The first frame match time -> %d" % (first_retry_time))
        dumplogger.info("The second frame match time -> %d" % (second_retry_time))
        dumplogger.info("The first frame match result -> %d" % (is_first_frame_match))
        dumplogger.info("The second frame match result -> %d" % (is_second_frame_match))
        dumplogger.info("Gif match result -> %d" % (gif_match_result))
        gif_match_result = 0
    time.sleep(1)

    return gif_match_result

@DecoratorHelper.FuncRecorder
def CheckDownloadCsvContent(arg):
    ''' CheckDownloadCsvContent : Check download csv content
            Input argu :
                file_name - Part of file name
                check_content - String you want to check in csv
                                If you want to check multiple csv column, you need to seperated each column with a comma (example: 1001,1002,1003....)
                check_row_position - Which row you want to check in csv
                isfuzzy - 1 - Fuzzy comparison
                          0 - Perfect comparison
            Return code : 1 - success
                          0 - fail
    '''
    ret = 1
    file_name = arg["file_name"]
    check_content = arg["check_content"]
    check_row_position = arg["check_row_position"]
    isfuzzy = arg["isfuzzy"]

    ##Get download file from selenium grid server to local first
    if Config._RemoteSeleniumDriver_:
        ##Get download data with re-write it in local file with test case number as file name
        file_name = BaseUILogic.GetDownloadedDataFromRemoteDriver(Parser._CASE_['id'])

    ##If return = 0, means get download data failed
    if file_name:
        ##Get all complete file name with part of file name and sort by date
        file_name_list = glob.glob("*" + file_name + "*")
        file_name_list.sort(key=os.path.getmtime)
        dumplogger.info("All file name list -> %s" % (file_name_list))

        ##If have file in file list
        if file_name_list:
            ##Get rid of different file name
            while(file_name not in file_name_list[-1]):
                file_name_list.pop(-1)
            ##Get newest file name
            file_name = file_name_list[-1]
            dumplogger.info("Newest file name -> %s" % (file_name))

            ##Read download csv data by CsvReader
            slash = Config.dict_systemslash[Config._platform_]
            data_list = CsvReader(file_path=Config._CurDIR_ + slash + file_name)

            ##Convert each csv row data to string list, string will seperate csv column with comma (example: 1001,1002,1003....)
            csv_data_list = []
            for data in data_list:
                data = ",".join(data)
                csv_data_list.append(data)

            if check_row_position:
                ##Check specific csv row data
                csv_data = csv_data_list[int(check_row_position)-1]
                if CompareString({"actual_string": csv_data, "expect_string": check_content, "isfuzzy": isfuzzy}):
                    ret = 1
                    dumplogger.info("Check data exist in csv file")
                    message = "Check data exist in csv file"
                else:
                    ret = 0
                    dumplogger.info("Check data not exist in csv file")
                    message = "Check data not exist in csv file"
            else:
                ##Check all csv file data
                for csv_data in csv_data_list:
                    if CompareString({"actual_string": csv_data, "expect_string": check_content, "isfuzzy": isfuzzy}):
                        ret = 1
                        dumplogger.info("Check data exist in csv file")
                        message = "Check data exist in csv file"
                        break
                    else:
                        ret = 0
                        dumplogger.info("Check data not exist in csv file")
                        message = "Check data not exist in csv file"
        else:
            dumplogger.info("No file be downloaded.")
            message = "FileNotExist -> Fail"
            ret = 0
    else:
        dumplogger.info("No file be downloaded.")
        message = "FileNotExist -> Fail"
        ret = 0

    OK(ret, int(arg['result']), 'CheckDownloadCsvContent -> ' + message)

def GenerateRandomString(length, random_type):
    '''GenerateRandomString : Generate random characters or numbers
        Input argu :
            length - string length
            random_type - characters / numbers
        Return code : a random string
    '''
    random_string = ""

    for index in range(length):
        if random_type == "characters":
            random_string += random.choice(string.ascii_uppercase + string.digits)

        elif random_type == "numbers":
            random_string += random.choice(string.digits)

    #print random_string
    dumplogger.info("Generated %s : %s, length: %s" % (random_type, random_string, length))
    return random_string

def GenerateCurrentTimeString(format):
    '''GenerateTimeString : Generate random time string with specific format
        Input argu :
            format - time format
        Return code : a random string
    '''

    ##Generate current time string
    datetime_string = datetime.datetime.now().strftime(format)
    return datetime_string

def CsvReader(file_path):
    ''' CsvReader : Read the csv file
            Input argu : file_path - the file path of csv file
            Return code : data_list - which is a two-dimensional array and read from csv file
    '''
    data_list = []

    ##Use "with open" can close file automatically and read the target csv file
    with open(file_path, 'rb') as openfile:
        readfile = csv.reader(openfile)
        for row in readfile:
            data_list.append(row)

    return data_list

@DecoratorHelper.FuncRecorder
def CsvWritter(arg):
    ''' CsvWritter : Write data into specific row and column of csv file
            Input argu :
                        input_row - input the row number where you want to input the order id in csv
                        input_column - input the column number where you want to input the order id in csv
                        value - value you want to input
                        data_list - which is returned from function CsvReader
                        file_path - the file path of csv file
            Return code : 1 - success
                       0 - fail
                        -1 - error
    '''
    ret = 1
    input_row = arg['input_row']
    input_column = arg['input_column']
    value = arg['value']
    data_list = arg['data_list']
    file_path = arg['file_path']

    ##Input value in list which is returned from function CsvReader, and data_list is a two-dimensional array
    data_list[int(input_row)-1][int(input_column)-1] = value

    ##Write the list into the csv file
    with open(file_path, 'wb') as openfile:
        writefile = csv.writer(openfile)
        writefile.writerows(data_list)

    OK(ret, int(arg['result']), 'CsvWritter')

def CompareString(arg):
    ''' CompareString : Compare string
            Input argu :
                        actual_string - actual string you want to compare
                        expect_string - expect string you want to compare
                        isfuzzy - 1 - Fuzzy comparison
                                  0 - Perfect comparison
            Return code : True / False
    '''
    actual_string = arg['actual_string']
    expect_string = arg['expect_string']
    isfuzzy = arg['isfuzzy']

    if int(isfuzzy):
        ##Fuzzy compare string
        if expect_string in actual_string:
            dumplogger.info("Expect string is -> %s" % (expect_string))
            dumplogger.info("Actual string is -> %s" % (actual_string))
            dumplogger.info("Expect string exist in actual string")
            return True
    else:
        ##Perfect compare string
        if expect_string == actual_string:
            dumplogger.info("Expect string is -> %s" % (expect_string))
            dumplogger.info("Actual string is -> %s" % (actual_string))
            dumplogger.info("Expect string is equal to actual string")
            return True

    ##Expect string not exist in actual string
    dumplogger.info("Expect string is -> %s" % (expect_string))
    dumplogger.info("Actual string is -> %s" % (actual_string))
    dumplogger.info("Expect string not exist in actual string")
    return False

@DecoratorHelper.FuncRecorder
def DecodeAndCheckQRCode(arg):
    ''' DecodeAndCheckQRCode : Decode qrcode and check url
            Input argu :
                    compare_url - the expected qrcode url
            Return code : 1 - success
                         0 - fail
                          -1 - error
    '''
    ret = 1
    compare_url = arg["compare_url"]
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##If user has give url arg, then use user defined url to compare, otherwise, use Form QR Code url to compare url.
    if compare_url:
        dumplogger.info("Use user define url to compare.")
    else:
        dumplogger.info("Use Form QRCode image compose url to compare.")
        compare_url = GlobalAdapter.FormE2EVar._QRCodeURL_

    dumplogger.info("Compare_url : %s" % (compare_url))

    ##screenshot qrcode element
    PrintScreen({"file_name": "", "mode": "element", "result": "1"})

    ##Read screenshot image
    inputImage = cv2.imread(output_casename_folder + "element.png")

    ##Decode qrcode image by opencv and get the url
    qrcode_url = decode(inputImage)[0][0]
    dumplogger.info("Decoded Data : %s" % (qrcode_url))

    ##Compare url
    if qrcode_url == compare_url:
        dumplogger.info("Compare QRcode url success")
    else:
        dumplogger.info("Compare QRcode url fail")
        ret = 0

    OK(ret, int(arg['result']), 'DecodeAndCheckQRCode')

@DecoratorHelper.FuncRecorder
def GetCurrentTimeStamp(arg):
    ''' GetCurrentTimeStamp : Get current time stamp and store it to local
            Input argu :
                N/A
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 1
    GlobalAdapter.TrackerVar._CurUnixTime_ = int(time.time())
    dumplogger.info("GlobalAdapter.TrackerVar._CurUnixTime_: %d" % (GlobalAdapter.TrackerVar._CurUnixTime_))

    OK(ret, int(arg['result']), 'GetCurrentTimeStamp -> ' + str(GlobalAdapter.TrackerVar._CurUnixTime_))

def SetTimezone(arg):
    ''' SetTimezone : Set time to specific timezone
            Input argu :
                timezone - timezone to be set
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 1
    timezone = arg['timezone']

    os.system('tzutil /s "{}"'.format(timezone))
    dumplogger.info("timezone set to : {}".format(timezone))

    OK(ret, int(arg['result']), 'SetTimezone -> ' + str(timezone))

@DecoratorHelper.FuncRecorder
def ScrollPageToTop(arg):
    ''' ScrollPageToTop : Scroll web page to top
            Input argu :
                N/A
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 1

    ## Execute js script to scroll page
    BaseUICore.ExecuteScript({"script": 'window.scrollTo(0, document.body.scrollHeight)', "result": "1"})

    OK(ret, int(arg['result']), 'ScrollPageToTop')


def CompareJSONFile(arg):
    ''' CompareJSONFile : Compare json file content
            Input argu :
                target_file - the json file need to be compared
                source_file - expect json file
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 0
    slash = Config.dict_systemslash[Config._platform_]
    ##Get file name for target and source
    target_file_name = arg['target_file'] + '.json'
    source_file_name = arg['source_file'] + '.json'

    ##Bulid file path
    target_file_path = Config._DataFilePath_ + slash + target_file_name
    source_file_path = Config._APICaseDataFilePath_ + slash + Config._TestCasePlatform_ + slash + "common" + slash + Config._TestCaseFeature_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_.lower() + slash + source_file_name

    ##Load json content from both file
    with open(source_file_path) as data_file:
        expect_json = json.load(data_file)

    with open(target_file_path) as data_file:
        actual_json = json.load(data_file)

    dumplogger.info("Expect json is -> %s" % (expect_json))
    dumplogger.info("Actual json is -> %s" % (actual_json))

    ##Compare 2 json content
    if expect_json == actual_json:
        dumplogger.info("json file match!")
        ret = 1

    OK(ret, int(arg['result']), 'CompareJSONFile')

@DecoratorHelper.FuncRecorder
def ExecuteCMDInContainer(arg):
    ''' ExecuteCMDInContainer : Execute CMD in container
            Input argu :
                upload_type - which cdn you want to upload
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''

    ret = 1
    upload_type = arg["upload_type"]

    ##upload container cdn command
    if upload_type == "banner_cdn":
        cmd = "smc -e staging services run discoverlanding-bannercore-staging-%s './service/bin/discoverlanding-banneruploadcdn-manual'" % (Config._TestCaseRegion_.lower())
    elif upload_type == "campaign_label":
        cmd = "smc -e staging services run bannermanagement-admin-staging-%s './bin/campaignlabelgenerate'" % (Config._TestCaseRegion_.lower())

    ##Command enter to DL banner containers
    os.system(cmd)
    dumplogger.info("Enter bannercore container and upload cdn: %s" % (cmd))

    OK(ret, int(arg['result']), 'ExecuteCMDInContainer')

def ParseElementTreeAndFind(arg):
    ''' ParseElementTreeAndFind : Parse element tree and find with xpath's on both PC/Android
            Input argu :
                dom_source - dom source tree to be paresd
                target - target name/id/xpath to be find
            Return code :
                1 - return 1 for successfully find node address or ignore situation
                0 - return 0 if not match
    '''
    dom_source = arg["dom_source"]
    target = arg["target"]

    #dumplogger.info(GlobalAdapter.CommonVar._MemoryStack_)
    #dumplogger.info("Failed action on UI: %s" % (GlobalAdapter.CommonVar._MemoryStack_[-1]))

    ##Parse element tree by platform, GlobalAdapter.CommonVar._MemoryStack_[-1] is the caller of this function
    if GlobalAdapter.CommonVar._MemoryStack_:
        try:
            if "Android" in GlobalAdapter.CommonVar._MemoryStack_[-1]:
                ##Parse element tree for Android
                elem_tree = etree.parse(dom_source)
            else:
                ##Parse element tree for PC
                elem_tree = etree.parse(dom_source, etree.HTMLParser())

            ##Find target by name/id/xpath
            elem_list = elem_tree.find(target)
            if elem_list:
                #uni_path = elem_tree.getpath(elem_list)
                #dumplogger.info("With the unique xpath provided if you need: %s" % (uni_path))
                dumplogger.info("Element: %s found in DOM-Tree, with the unique xpath provided if you need: %s" % (target))
                return 1
            else:
                dumplogger.info("Element: %s" % (target))
                dumplogger.info("NOT found in DOM-Tree!!!!!!")
                return 0

        ##Syntax Error can ignore
        except SyntaxError:
            dumplogger.exception("%s has 'contain' in xpath, not able to locate by this checker, please ignore!!!" % (target))
            return 1
        ##Unknown can ignore, usually xpath is not supported by this lib
        except:
            dumplogger.exception("%s has 'contain' in xpath, not able to locate by this checker, please ignore!!!" % (target))
            return 1
    else:
        dumplogger.info("Not able to locate by this checker, please ignore!!!")
        return 1

@DecoratorHelper.FuncRecorder
def SaveCaseInfoForIssueReport(arg):
    ''' SaveCaseInfoForIssueReport : Save info for reporting issue, such as orderid/ account...
            Input argu :
                info_key - any info you want to save (orderid / returnid / checkoutid / buyer / seller...)
                info_value - the value of issue report info (ex. if want to save order id, then value will be GlobalAdapter.OrderE2EVar._OrderID_)
            Return code : 1 - success
                          0 - fail
                          -1 - error
    '''
    ret = 1
    info_key = arg["info_key"]
    info_value = arg["info_value"]

    ##Save info for reporting issue
    if info_key == "returnid":
        GlobalAdapter.CommonVar._FailedCaseInfo_["note"]["returnid"] = GlobalAdapter.OrderE2EVar._ReturnID_
    elif info_key == "orderid":
        GlobalAdapter.CommonVar._FailedCaseInfo_["note"]["orderid"] = GlobalAdapter.OrderE2EVar._OrderID_
    elif info_key == "order_timestamp":
        GlobalAdapter.CommonVar._FailedCaseInfo_["note"]["Order related timestamp"] = ", "
        for timestamp in GlobalAdapter.OrderE2EVar._OrderTimestampDict_:
            GlobalAdapter.CommonVar._FailedCaseInfo_["note"]["Order related timestamp"] = GlobalAdapter.CommonVar._FailedCaseInfo_["note"]["Order related timestamp"] + "|" + timestamp + "|" + GlobalAdapter.OrderE2EVar._OrderTimestampDict_[timestamp] + "|, "
    else:
        GlobalAdapter.CommonVar._FailedCaseInfo_["note"][info_key] = info_value

    OK(ret, int(arg['result']), 'SaveCaseInfoForIssueReport')

@DecoratorHelper.FuncRecorder
def CheckDownloadFile(arg):
    '''
    CheckDownloadFile : Check download file
            Input argu :
                target_path - selenium default download path -> ..\QATest\ , if path is \QATest\file.zip, please set 'file.zip'
                check_file - check file, if need check multiple file then need to use ',' to seperate each file
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    target_path = arg["target_path"]
    check_file = arg["check_file"]
    ret = 1

    ##Setting path for all system
    slash = Config.dict_systemslash[Config._platform_]

    ##Assemble file path
    file_path = os.path.join(Config._CurDIR_, target_path)

    ##Check file exist
    if os.path.exists(file_path):
        dumplogger.info("File exists.")

        ##If need check file, then excute following scritp
        if check_file:

            ##Check multiple file then seperate each file and save in list
            check_file_list = check_file.split(",")

            ##Get file name and file type
            file_name = file_path.split(slash)[-1].split(".")[0]
            file_type = file_path.split(slash)[-1].split(".")[-1]

            ##Get all file from zip archive
            if file_type == "zip":
                with zipfile.ZipFile(file_path, 'r') as zip_ref:

                    ##Unzip to the following path
                    upzip_file_path = os.path.join(Config._CurDIR_, file_name)
                    zip_ref.extractall(upzip_file_path)

                    ##Get all file list within the zip archive
                    get_file_list = os.listdir(upzip_file_path)
                    dumplogger.info("File within the zip archive -> %s" % (get_file_list))

            ##Get all file name from folder
            else:
                get_file_list = os.listdir(file_path)

            ##Check file exist in download archive
            for check_file in check_file_list:
                if check_file not in get_file_list:
                    dumplogger.info("File NOT exists in the downloaded %s archive -> %s" % (file_type, check_file))
                    ret = 0
                    break
                else:
                    dumplogger.info("File exists in the downloaded %s archive -> %s" % (file_type, check_file))
    else:
        ret = 0
        dumplogger.info("File NOT exists. please check your file path!")

    OK(ret, int(arg['result']), 'CheckDownloadFile')

def SendSMCCommand(arg):
    '''
    SendSMCCommand : Send smc command
            Input argu :
                env - staging / test
                smc_arg - arguments for the smc command (ex: logs, stats, ls ...)
                services - services (ex: noti-core-staging-sg)
                avil_cmd - available command(ex: run noti-core-staging-sg "your command")
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    env = arg["env"]
    smc_arg = arg["smc_arg"]
    services = arg["services"]
    avil_cmd = arg["avil_cmd"]

    dumplogger.info("Enter SendSMCCommand")
    response = []

    ##Send SMC Command
    command = "smc -e " + env + " services " + smc_arg + " " + services
    if avil_cmd:
        command = command + " " + avil_cmd

    dumplogger.info(command)
    cmd_result = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)

    ##Save the response result
    response = cmd_result.stdout.readlines()

    return response

@DecoratorHelper.FuncRecorder
def GetAndCheckContainerData(arg):
    '''
    GetAndCheckContainerData : Get and check container data logs
            Input argu :
                env - staging / test
                services - services (ex: noti-core-staging-sg)
                log_path - log path (ex: log/data.log)
                keyword - keyword (Multiple keyword need to use ',' to split it. ex: userid:101705125,taskid:9917 )
                check_string - check string
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    env = arg["env"]
    services = arg["services"]
    log_path = arg["log_path"]
    keyword = arg["keyword"]
    check_string = arg["check_string"]
    ret = 0

    ##Assemble keyword
    keyword_list = keyword.split(",")
    keyword_str = keyword_list[0]
    for times in range(len(keyword_list) - 1):
        keyword_str = keyword_str + ".*" + keyword_list[times + 1]

    ##Assemble and send command
    avil_cmd = "\"cat " + log_path + " | grep '" + keyword_str + "'\""

    ##Get response
    response = SendSMCCommand({"env": env, "services": services, "smc_arg": "run", "avil_cmd": avil_cmd, "result": "1"})

    ##Check string exist or not
    for row_data in response:

        ##The result of the comparison is not necessarily a single
        if check_string in row_data:

            ##Record comparison results
            dumplogger.info("checking" + row_data)

            ##One of the matches will be returned 1
            ret = 1
            break

    OK(ret, int(arg["result"]), "GetAndCheckContainerData")

def WriteFunctionInfoToImg(arg):
    '''
    WriteFunctionInfoToImg : Write fail case function information to image
            Input argu :
                status - normal / retry
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    status = arg["status"]
    if status == "normal":
        img_list = GlobalAdapter.CommonVar._Start_Img_
        function_list = GlobalAdapter.CommonVar._Function_Name_
    else:
        img_list = GlobalAdapter.CommonVar._Retry_Start_Img_
        function_list = GlobalAdapter.CommonVar._Retry_Function_Name_

    if len(img_list) >= 3:
        ##Concat image list
        start_list = img_list
        img = cv2.hconcat(img_list)

        ##percent of original size
        scale_percent = 50
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)
        dim = (width, height)

        ##resize image
        resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
        width = resized.shape[1]
        height = resized.shape[0]

        ##write information to image
        for count_image in range(1,len(img_list)):
            if count_image < len(img_list):
                cv2.putText(resized, "After execute:", ((width/len(start_list))*count_image + 60, (height/12)-80), cv2.FONT_HERSHEY_SIMPLEX,0.75, (67,255,255), 2, cv2.LINE_AA)
                cv2.putText(resized, function_list[count_image-1], ((width/len(start_list))*count_image + 60, (height/12)-40), cv2.FONT_HERSHEY_SIMPLEX,0.75, (67,255,255), 2, cv2.LINE_AA)
                resized = cv2.arrowedLine(resized, ((width/len(start_list))*count_image - 30, (height/2)), ((width/len(start_list))*count_image + 20, (height/2)),(0, 255, 0), 8,tipLength = 0.5)
            if count_image == 3:
                resized = cv2.arrowedLine(resized, ((width/len(start_list))*count_image - 30, (height/2)), ((width/len(start_list))*count_image + 20, (height/2)),(255, 255, 0), 8,tipLength = 0.6)
                cv2.putText(resized, 'Retry ', ((width/len(start_list))*count_image - 30, (height/2)+30), cv2.FONT_HERSHEY_SIMPLEX,0.75, (67,255,255), 2, cv2.LINE_AA)
            else:
                resized = cv2.arrowedLine(resized, ((width/len(start_list))*count_image - 30, (height/2)), ((width/len(start_list))*count_image + 20, (height/2)),(0, 255, 0), 8,tipLength = 0.5)
        if status == 'retry':
            cv2.putText(resized, 'Retry start ', (30, 30),cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2,cv2.LINE_AA)
        cv2.putText(resized, 'Fail : ', ((width/3)*2+60, (height-50)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,255), 2, cv2.LINE_AA)
        cv2.putText(resized, function_list[-1], ((width/3)*2+60, (height-25)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0,0,255), 2, cv2.LINE_AA)
        dumplogger.info("Write informaion to image success")

        return resized

def OutputAndroidRowImage(arg):
    '''
    OutputAndroidRowImage : Output Android row image to folder
            Input argu :
                path - image path
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    path = arg["path"]

    ##Output Android Row Image
    if len(GlobalAdapter.CommonVar._Start_Img_) >= 3:
        normal_action = WriteFunctionInfoToImg({"status": "normal"})
        output = normal_action
        if len(GlobalAdapter.CommonVar._Retry_Start_Img_) >= 3:
            retry_action = WriteFunctionInfoToImg({"status": "retry"})
            normal_action = cv2.copyMakeBorder(normal_action, 0, 20, 0, 0, cv2.BORDER_CONSTANT, None, value = [50, 50, 50])
            output = cv2.vconcat([normal_action,retry_action])
        dumplogger.info("Case fail - Android driver take row screenshot success")
        cv2.imwrite(path + Parser._CASE_['id'] + "_row_android.png", output)

def DecodeImage(arg):
    '''
    DecodeImage : Decode image to uint8
            Input argu :
                image - decode image
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    image = arg["image"]

    ## decode image to uint8
    img_decode = base64.b64decode(image)
    nparr = np.frombuffer(img_decode, np.uint8)
    image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    cv2.rectangle(image, (0, 2200), (1080, 2340), (0, 0, 0), -1)
    image = cv2.copyMakeBorder(image, 150, 20, 120, 120, cv2.BORDER_CONSTANT, None, value = 0)
    dumplogger.info("Get and decode image success")

    return image
