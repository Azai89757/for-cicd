import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class THPCActivityBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCActivityBanner Precondition =="
        dumplogger.info("== Setup THPCActivityBanner Precondition ==")

    def test_THPCActivityBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-01.xml")

    def test_THPCActivityBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-02.xml")

    def test_THPCActivityBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-03.xml")

    def test_THPCActivityBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-04.xml")

    def test_THPCActivityBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-05.xml")

    def test_THPCActivityBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-06.xml")

    def test_THPCActivityBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-07.xml")

    def test_THPCActivityBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-08.xml")

    def test_THPCActivityBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-09.xml")

    def test_THPCActivityBanner10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-10.xml")

    def test_THPCActivityBanner11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-11.xml")

    def test_THPCActivityBanner12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-12.xml")

    def test_THPCActivityBanner13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-13.xml")

    def test_THPCActivityBanner14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-14.xml")

    def test_THPCActivityBanner15(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCActivityBanner-15.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCActivityBanner Post condition =="
        dumplogger.info("== Setup THPCActivityBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
