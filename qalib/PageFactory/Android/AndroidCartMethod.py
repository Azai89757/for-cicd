#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidCartMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def AndroidGoToCartPage(arg):
    '''
    AndroidGoToCartPage : Go to cart page from different page
            Input argu :
                from_page - feed/notification/metab/homepage/bundle_landing/chat
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    from_page = arg["from_page"]

    ##Goto cart page
    xpath = Util.GetXpath({"locate":from_page})
    AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Goto cart page", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGoToCartPage')


class AndroidCartButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnDeal(arg):
        '''
        ClickAddOnDeal : Click add btn for add on deal
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close button
        xpath = Util.GetXpath({"locate":"add_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add button to add on deal page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickAddOnDeal')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopCheckBox(arg):
        '''
        ClickShopCheckBox : Click shop checkbox in cart pagr
                Input argu :
                    shop_name - shop name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_name = arg['shop_name']

        xpath = Util.GetXpath({"locate": "shop_label"})
        xpath = xpath.replace('replaced_text', shop_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click checkbox for " + shop_name, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickShopCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductVariationDropDown(arg):
        '''
        ClickProductVariationDropDown : Click product variation drop down button
                Input argu :
                    product_name - the product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click certain product variation dropdown btn
        xpath = Util.GetXpath({"locate":"dropdown_btn"})
        xpath = xpath.replace('replaced_text', product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click product variation drop down button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickProductVariationDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductModelChange(arg):
        '''
        ClickProductModelChange : Click product model change btn
                Input argu : product_name - the product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product model change btn
        xpath = Util.GetXpath({"locate": "model_change_btn"})
        xpath = xpath.replace('replaced_text', product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click product model change btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickProductModelChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingFeeDropDown(arg):
        '''
        ClickShippingFeeDropDown : Click shipping fee drop down in cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click shipping fee drop down
        xpath = Util.GetXpath({"locate":"ship_fee_drop_down"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click shipping fee drop down", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickShippingFeeDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button in product variation window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm or cancel btn in product variation window
        xpath = Util.GetXpath({"locate": "variation_confirm"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click product variation dropdown btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAll(arg):
        '''
        ClickSelectAll : In cart page click select all
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select all checkbox
        xpath = Util.GetXpath({"locate": "select_all_checkbox"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click select all checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickSelectAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMoveToMyLikes(arg):
        '''
        ClickMoveToMyLikes : Move cart item to my likes page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click move to my likes button
        xpath = Util.GetXpath({"locate": "move_to_my_likes_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Move cart item to my likes page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickMoveToMyLikes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def TapsVoucherSection(arg):
        '''
        TapsVoucherSection : Taps on voucher section in cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Taps on voucher section in cart page
        xpath = Util.GetXpath({"locate": "voucher_section"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click voucher section", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.TapsVoucherSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyNow(arg):
        '''
        ClickBuyNow : Click purchase btn on Cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy now button
        xpath = Util.GetXpath({"locate":"purchase"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click buy now button on Cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit product
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit btn
        xpath = Util.GetXpath({"locate":"edit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit btn on Cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditInvalidItemsBlock(arg):
        '''
        ClickEditInvalidItemsBlock : Click edit invalid items block
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit btn
        xpath = Util.GetXpath({"locate":"edit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit invalid items block", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickEditInvalidItemsBlock')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageLevelEdit(arg):
        '''
        ClickPageLevelEdit : Click page level edit product at the upper right corner in cart
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit btn
        xpath = Util.GetXpath({"locate":"page_level_edit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click page level edit btn on Cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickPageLevelEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageLevelDelete(arg):
        '''
        ClickPageLevelDelete : Click delete button beside move to my likes button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete btn
        xpath = Util.GetXpath({"locate":"page_level_delete_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete button beside move to my likes button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickPageLevelDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDone(arg):
        '''
        ClickDone : Click done button in page level edit mode
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done button in page level edit mode
        xpath = Util.GetXpath({"locate":"page_level_done_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click done button in page level edit mode", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        '''
        ClickOk : Click edit ok
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click OK
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit btn on Cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickOk')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickModelOptionCancel(arg):
        '''
        ClickModelOptionCancel : Click cancel icon in choose model mode
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"cancel_icon", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickModelOptionCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteProduct(arg):
        '''
        ClickDeleteProduct : Click delete product
                Input argu :
                        index - which index in add on deal group
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Click delete product
        xpath = Util.GetXpath({"locate":"delete_btn"})
        xpath = xpath.replace("index_to_be_replace", index)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete btn on Cart page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickDeleteProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteBesideProduct(arg):
        '''
        ClickDeleteBesideProduct : Click delete button beside product
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete product
        xpath = Util.GetXpath({"locate":"delete_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete btn on Cart page beside product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickDeleteBesideProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchShopeeCoinToggle(arg):
        '''
        SwitchShopeeCoinToggle : Click use shopee coin toggle
                Input argu : switch_type - open/close
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        switch_type = arg['switch_type']

        ##Switch close/open for shopee coin toggle
        xpath = Util.GetXpath({"locate":switch_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Switch button to" + switch_type, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCartButton.SwitchShopeeCoinToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGetSellerVoucher(arg):
        '''
        ClickGetSellerVoucher : Click get seller voucher
                Input argu : button_type - more_voucher / claim / non_discoverable_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click get seller voucher
        xpath = Util.GetXpath({"locate":button_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click get seller voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickGetSellerVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleDealAddMore(arg):
        '''
        ClickBundleDealAddMore : Click bundle deal add more
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click bundle deal add more
        xpath = Util.GetXpath({"locate":"bundle_add_more_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click bundle deal add more", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickBundleDealAddMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionChange(arg):
        '''
        ClickPromotionChange : Click promotion change btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click promotion change btn
        xpath = Util.GetXpath({"locate": "promotion_change_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click promotion change btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickPromotionChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseVoucher(arg):
        '''
        ClickChooseVoucher : Click choose voucher button in cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose voucher button
        xpath = Util.GetXpath({"locate":"voucher_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click choose voucher button", "result": "1"})

        ##Due to ticket : SPPT-18597, add sleep time to prevent fail
        ##After ticket fixed will remove this section
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickChooseVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDiscountTab(arg):
        '''
        ClickDiscountTab : Click discount tab in cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click discount tab
        xpath = Util.GetXpath({"locate":"discount_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click discount tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickDiscountTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyAgainTab(arg):
        '''
        ClickBuyAgainTab : Click buy again tab beside discount tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click buy again tab
        xpath = Util.GetXpath({"locate":"buy_again_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click buy again tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickBuyAgainTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllProductTab(arg):
        '''
        ClickAllProductTab : Click all product tab in cart page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click all product tab
        xpath = Util.GetXpath({"locate":"all_product_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click all product tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickAllProductTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFindSimilar(arg):
        '''
        ClickFindSimilar : Click Find Similar beside item
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Find Similar beside item
        xpath = Util.GetXpath({"locate":"similar_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Find Similar beside item", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickFindSimilar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickYes(arg):
        '''
        ClickYes : Click Yes button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click yes button
        xpath = Util.GetXpath({"locate":"yes_text"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click yes button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickYes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNo(arg):
        '''
        ClickNo : Click No button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click no button
        xpath = Util.GetXpath({"locate":"no_text"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click no button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickNo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyAgainProduct(arg):
        '''
        ClickBuyAgainProduct : Click product in buy again tab
                Input argu : product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click certain product name in buy again tab
        xpath = Util.GetXpath({"locate":"buy_again_item"})
        xpath = xpath.replace('replaced_text', product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click product in buy again tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickBuyAgainProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyAgainAddToCart(arg):
        '''
        ClickBuyAgainAddToCart : Click product add to cart button in buy again tab
                Input argu : product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click certain product name in buy again tab
        xpath = Util.GetXpath({"locate":"buy_again_item_addtocart"})
        xpath = xpath.replace('replaced_text', product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click product add to cart button in buy again tab", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickBuyAgainAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPlatformVoucher(arg):
        '''
        ClickPlatformVoucher : Click Platform Voucher Section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click platform voucher section text
        xpath = Util.GetXpath({"locate":"platform_voucher_section"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Platform Voucher", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickPlatformVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickInvalidSectionEdit(arg):
        '''
        ClickInvalidSectionEdit : Click invalid section edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click invalid section edit button
        xpath = Util.GetXpath({"locate":"invalid_section_edit"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click invalid section edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickInvalidSectionEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackButton(arg):
        '''
        ClickBackButton : Click cancel icon in choose model mode
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":"back_button", "threshold":"0.9", "is_click":"yes", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickBackButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectGWPGift(arg):
        '''
        ClickSelectGWPGift : Select GWP gift
                Input argu : N/A
                Retutn code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select gift
        xpath = Util.GetXpath({"locate": "gwp_gift_button"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click select gift button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickSelectGWPGift')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirmGWPGift(arg):
        '''
        ClickConfirmGWPGift : Confirm GWP gift
                Input argu : N/A
                Retutn code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Confirm gift
        xpath = Util.GetXpath({"locate": "confirm_gift"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm gift", "result": "1"})

        time.sleep(10)

        xpath = Util.GetXpath({"locate": "back_to_cart"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click go back to cart", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickConfirmGWPGift')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionBreakdown(arg):
        '''
        ClickPromotionBreakdown : Click Promotion Breakdown in cart
                Input argu : N/A
                Retutn code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Promotion Breakdown arrow
        xpath = Util.GetXpath({"locate": "promotion_breakdown_arrow"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click promotion breakdown arrow", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartButton.ClickPromotionBreakdown')


class AndroidCartPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckMakePurchaseStatus(arg):
        '''
        CheckMakePurchaseStatus : Check made purchase status and retry 3 times if doesn't succeed
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        ##Check made purchase status and retry 3 times if doesn't succeed
        xpath = Util.GetXpath({"locate":"shipping_mode"})
        for retry_times in range(1, 4):
            if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Make purchase successful!")
                ret = 1
                break
            else:
                dumplogger.info("Make purchase unsuccessful, retry %s times", retry_times)
                AndroidCartButton.ClickBuyNow({"result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartPage.CheckMakePurchaseStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteProductInCart(arg):
        '''
        DeleteProductInCart : Delete product in Cart page
                Input argu :
                    index - first item index is 2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Click edit
        AndroidCartButton.ClickEdit({"result": "1"})

        time.sleep(2)

        ##Click delete
        AndroidCartButton.ClickDeleteProduct({"index":index,"result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartPage.DeleteProductInCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckAndCleanProductInCart(arg):
        '''
        CheckAndCleanProductInCart : Check if there is any product in cart, if so, delete it.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check if invalid items block exists, delete invalid items
        xpath = Util.GetXpath({"locate":"invalid_items_block"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            dumplogger.info("There are some invalid items in cart")
            AndroidCartButton.ClickEditInvalidItemsBlock({"result": "1"})
            AndroidCartButton.ClickDeleteProduct({"index":"1","result": "1"})
            AndroidBaseUILogic.AndroidRelativeMove({"direction":"up", "times":"2", "result": "1"})

        ##Check if cart is empty
        xpath = Util.GetXpath({"locate":"msg_cart_empty"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            dumplogger.info("No product in cart, leave function.")
        else:
            ##Click edit -> select all -> delete product
            dumplogger.info("Products exist in cart.. start to purge.")
            AndroidCartButton.ClickPageLevelEdit({"result": "1"})
            AndroidCartButton.ClickSelectAll({"result": "1"})
            AndroidCartButton.ClickPageLevelDelete({"result": "1"})
            AndroidCartButton.ClickYes({"result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartPage.CheckAndCleanProductInCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddQuantity(arg):
        '''
        ClickAddQuantity : In cart page click add quantity btn in specific product
                Input argu :
                        product_name - product name
                        click_time - click time (number)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]
        click_time = int(arg["click_time"])

        for quantity in range(click_time):
            ##Click add button
            xpath = Util.GetXpath({"locate":"add_btn"})
            xpath_target_product = xpath.replace("product_name_to_be_replace", product_name)
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_target_product, "message":"click add button", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCartPage.ClickAddQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubQuantity(arg):
        '''
        ClickSubQuantity : In cart page click sub quantity btn
                Input argu :
                        product_name - product name
                        click_time - click time (number)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]
        click_time = arg["click_time"]

        for quantity in range(int(click_time)):
            ##Click sub button
            xpath = Util.GetXpath({"locate": "sub_btn"})
            xpath_target_product = xpath.replace("product_name_to_be_replace", product_name)
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath_target_product, "message": "click sub button", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCartPage.ClickSubQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckBoxOfItem(arg):
        '''
        ClickCheckBoxOfItem : Click check box of item in cart page
                Input argu : product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click check box for certain product name
        xpath = Util.GetXpath({"locate":"product_label"})
        xpath = xpath.replace('replaced_text', product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click checkbox for ", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartPage.ClickCheckBoxOfItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopName(arg):
        '''
        ClickShopName : Click shop name
                Input argu :
                    position - shop name position
                Retutn code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        shop_name = arg['shop_name']

        ##Click shop name in cart item row
        xpath = Util.GetXpath({"locate": "shop_name"})
        xpath = xpath.replace('replaced_text', shop_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click shop name", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartPage.ClickShopName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputQuantity(arg):
        '''
        InputQuantity : Input product quantity
                Input argu :
                    product_name - product name
                    quantity - quantity to checkout
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]
        quantity = arg['quantity']

        ##Input product quantity
        xpath = Util.GetXpath({"locate":"quantity_input_area"})
        xpath = xpath.replace('product_name_to_be_replaced', product_name)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":quantity, "result": "1"})
        AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"enter", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCartPage.InputQuantity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductModel(arg):
        '''
        ChooseProductModel : Click to choose product mode on cart page
                Input argu :
                    name - product model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click product model
        xpath = Util.GetXpath({"locate": "product_mode"}).replace("product_name_for_replace", name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Choose product mode","result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartPage.ChooseProductModel')

class AndroidCartComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
                replace_text - type of argument which you want to replace
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]
        replace_text = arg["replace_text"]

        xpath = Util.GetXpath({"locate": button_type})
        if replace_text:
            xpath = xpath.replace("replaced_text", replace_text)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
            Input argu :
                checkbox_type - select_all_checkbox / shop_checkbox
                replace_text - type of argument which you want to replace
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]
        replace_text = arg["replace_text"]

        xpath = Util.GetXpath({"locate": checkbox_type})
        if replace_text:
            xpath = xpath.replace("replaced_text", replace_text)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSection(arg):
        '''
        ClickOnSection : Click any type of section with well defined locator
            Input argu :
                section_type - more_voucher / non_discoverable_voucher / platform_voucher_section / promotion_breakdown / voucher_section
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]

        ##Click on section
        xpath = Util.GetXpath({"locate": section_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click section: %s, which xpath is %s" % (section_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCartComponent.ClickOnSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnTextField(arg):
        '''
        ClickOnTextField : Click any type of text field with well defined locator
            Input argu :
                field_type - shopee_voucher / seller_voucher
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 0
        field_type = arg["field_type"]
        xpath = Util.GetXpath({"locate": field_type})

        if field_type == "shopee_voucher":
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["code"], "result": "1"})

        ##Input 2nd shopee voucher code
        elif field_type == "2nd_shopee_voucher":
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-2]["code"], "result": "1"})

        ##Input seller voucher code
        elif field_type == "seller_voucher":
            AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":GlobalAdapter.PromotionE2EVar._SellerVoucherList_[-1]["code"], "message": "Click text field: %s, which xpath is %s" % (field_type, xpath), "result": "1"})

        else:
            ret = -1
            dumplogger.error("Voucher type string is not correct, please use shopee_voucher or seller_voucher string!!!")

        ##Sleep 10 second to wait voucher claim toast is disappear
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidCartComponent.ClickOnTextField')
