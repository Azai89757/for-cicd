import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCMission(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCMission Precondition =="
        dumplogger.info("== Setup TWPCMission Precondition ==")

    def test_TWPCMission01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCMission-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCMission Post condition =="
        dumplogger.info("== Setup TWPCMission Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
