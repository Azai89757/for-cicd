import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSellerVoucher Precondition =="
        dumplogger.info("== Setup IDHttpSellerVoucher Precondition ==")

    def test_IDHttpSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerVoucher-01.xml")

    def test_IDHttpSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerVoucher-02.xml")

    def test_IDHttpSellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerVoucher-03.xml")

    def test_IDHttpSellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerVoucher-04.xml")

    def test_IDHttpSellerVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerVoucher-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSellerVoucher Post condition =="
        dumplogger.info("== Setup IDHttpSellerVoucher Post condition ==")
