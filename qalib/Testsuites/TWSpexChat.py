#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from Config import dumplogger
from FrameWorkBase import *
from api.spex import SpexAPICore
import Config

class TWSpexChat(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexChat Precondition =="
        dumplogger.info("== Setup TWSpexChat Precondition ==")

        ##Tail log file to grep "Local connections to /run/spex/spex.sock:-2 forwarded to remote address /run/spex/spex.sock"
        ##If Spex InitResult is True, will not call RegisterToSpex() again
        if SpexAPICore.InspectSpexConnection():
            ##Register to Spex with specific type of spex configuration for the first time
            if not Config._SpexRegResult_:
                if SpexAPICore.GetSpexConfig('Api_Test_Staging_SG'):
                    Config._SpexRegResult_ = SpexAPICore.RegisterToSpex()

                    ##Check outcome after registere
                    if Config._SpexRegResult_:
                        print "== Setup TWSpexChat Register Success =="
                        dumplogger.info("== Setup TWSpexChat Register Success ==")

                    else:
                        print ("Spex Register Failed!!!")
                        dumplogger.error("Spex Register Failed")
                        ##Directly Leave Setup
                        self.skipTest("")
                else:
                    dumplogger.error("Get Spex config from ini failed!!!")
                    self.skipTest("")
            else:
                dumplogger.info("Spex is already registered.")

        else:
            print "== Setup TWSpexChat Precondition Fail by No Spex Agent Connection =="
            dumplogger.error("== Setup TWSpexChat Precondition Fail by No Spex Agent Connection ==")
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWSpexChat01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexChat-01.xml")

    def test_TWSpexChat02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexChat-02.xml")

    def test_TWSpexChat03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexChat-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexChat Post condition =="
        dumplogger.info("== Setup TWSpexChat Post condition ==")
