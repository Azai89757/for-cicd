import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class VNPCHomePageBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCHomePageBanner Precondition =="
        dumplogger.info("== Setup VNPCHomePageBanner Precondition ==")

    def test_VNPCHomePageBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-01.xml")

    ##Homepage campaign spec change
    def test_VNPCHomePageBanner02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-02.xml")

    def test_VNPCHomePageBanner03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-03.xml")

    def test_VNPCHomePageBanner04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-04.xml")

    def test_VNPCHomePageBanner05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-05.xml")

    '''def test_VNPCHomePageBanner06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-06.xml")'''

    def test_VNPCHomePageBanner07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-07.xml")

    ##Category not at the bottom of management page
    def test_VNPCHomePageBanner08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-08.xml")

    '''def test_VNPCHomePageBanner09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-09.xml")

    def test_VNPCHomePageBanner10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-10.xml")

    def test_VNPCHomePageBanner11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-11.xml")

    def test_VNPCHomePageBanner12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-12.xml")'''

    def test_VNPCHomePageBanner13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCHomePageBanner-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCHomePageBanner Post condition =="
        dumplogger.info("== Setup VNPCHomePageBanner Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
