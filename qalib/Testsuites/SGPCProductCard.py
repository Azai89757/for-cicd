import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCSGPCProductCard Precondition =="
        dumplogger.info("== Setup SGPCSGPCProductCard Precondition ==")

    def test_SGPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCard-01.xml")

    def test_SGPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCard-02.xml")

    def test_SGPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCard-03.xml")

    def test_SGPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCard-04.xml")

    def test_SGPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCard-05.xml")

    def test_SGPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCProductCard Post condition =="
        dumplogger.info("== Setup SGPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
