﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 OrderMethod.py: The def of this file called by XML mainly.
'''
##import python library
import re
import time
import datetime

##Import framework common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class OrderButton:
    ''' OrderButton : All of Page could inherit the Button class to handle event'''

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderDetailProduct(arg):
        '''
        ClickOrderDetailProduct : Click order detail product
                Input argu :
                    product_name - which product name you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click order detail product
        xpath = Util.GetXpath({"locate":"order_detail_product_section"})
        xpath_product_name = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_name, "message":"Click order detail product", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickOrderDetailProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMore(arg):
        '''
        ClickMore : Click more button in order list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click more button in order list page
        xpath = Util.GetXpath({"locate":"more_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click more button in order list page", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderCancel(arg):
        '''
        ClickOrderCancel : Click OrderCancel button
                Input argu :
                button_type - label_btn_cancel_order / label_odp_btn_cancel_all_order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click order cancel button
        xpath_cancel_icon = Util.GetXpath({"locate":button_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath_cancel_icon, "message":"Click Order Cancel button => %s" % (button_type), "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickOrderCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePayment(arg):
        '''
        ClickChangePayment : Click Change Payment button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Call Click to change payment
        xpath = Util.GetXpath({"locate": "label_btn_change_payment_method"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Change Payment button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickChangePayment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickArrowBack(arg):
        '''
        ClickArrowBack : Click arrow back button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click arrow back button
        xpath = Util.GetXpath({"locate":"arrow_back"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click arrow back button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickArrowBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOrderOptions(arg):
        '''
        ClickCancelOrderOptions : Click popup of Yes/No after click OrderCancel button
                Input argu :
                    type - yes, no
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ## Click popup of Yes/No button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click popup of Yes/No button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickCancelOrderOptions : ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExtendGuarantee(arg):
        '''
        ClickExtendGuarantee : Click extend guarantee button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##click extned guarantee button
        xpath = Util.GetXpath({"locate":"guarantee_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickExtendGuarantee button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickExtendGuarantee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExtendGuaranteeOption(arg):
        '''
        ClickExtendGuaranteeOption : Click popup of Yes/No after click ExtentGuarantee button
                Input argu :
                    type - yes, no
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ## Click popup of Yes/No button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + type, "result": "1"})

        OK(ret, int(arg['result']), 'ClickExtendGuaranteeOption -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewShop(arg):
        '''
        ClickViewShop : Click View Shop button
                Input argu :
                    click_type - order_list / order_detail
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_type = arg['click_type']

        ##Click View Shop button
        xpath = Util.GetXpath({"locate":click_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click View Shop button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickViewShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok button in order page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button in order page
        xpath = Util.GetXpath({"locate": "order_purchase_label_ok"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button in order page", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLearnMore(arg):
        '''
        ClickLearnMore : Click learn more button in order page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click learn more button in order page
        xpath = Util.GetXpath({"locate": "order_purchase_label_learn_more"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click learn more button in order page", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickLearnMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderCompleted(arg):
        '''
        ClickOrderCompleted : Click button of order completed
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click to click order complete button
        xpath = Util.GetXpath({"locate": "label_btn_order_received"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Order Completed button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickOrderCompleted')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderCompletedOptions(arg):
        '''
        ClickOrderCompletedOptions : Click order completed yes/no button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click order completed yes/no button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click order completed yes/no button ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickOrderCompletedOptions : ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefund(arg):
        '''
        ClickReturnRefund : Click button of order completed
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click to click extned guarantee button
        xpath = Util.GetXpath({"locate":"return_refund_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickReturnRefund button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEvaluation(arg):
        '''
        ClickEvaluation : Click button of Evaluation
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click to click button of Evaluation
        xpath = Util.GetXpath({"locate":"label_btn_rate"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Evaluation button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickEvaluation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductRateStar(arg):
        '''
        ClickProductRateStar : Click product rate star
                Input argu :
                    star_rate - 1 / 2 / 3 / 4 / 5
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        star_rate = arg["star_rate"]
        product_name = arg["product_name"]

        ##Click product rate star
        xpath = Util.GetXpath({"locate":"star_rate_element"})
        xpath_star = xpath.replace("star_rate_to_be_replace", star_rate)
        xpath_product_name_star = xpath_star.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_name_star, "message":"Click product rate star", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickProductRateStar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelThisRequest(arg):
        '''
        ClickCancelThisRequest : Click button to cancel this request
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click to click cancel this request
        xpath = Util.GetXpath({"locate":"cancel_request_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickCancelThisRequest button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickCancelThisRequest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopUpConfirm(arg):
        '''
        ClickPopUpConfirm : Click confirm on popup window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click to click popup confirm
        xpath = Util.GetXpath({"locate":"popup_confirm_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickPopUpConfirm button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickPopUpConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewReturnRefundDetail(arg):
        '''
        ClickViewReturnRefundDetail : Click view return refund detail button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click to view return/refund detail
        xpath = Util.GetXpath({"locate":"order_purchase_label_btn_view_refund_return_detail"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickViewReturnRefundDetail button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickViewReturnRefundDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToCheckSellerRatings(arg):
        '''
        ClickToCheckSellerRatings : Click to check seller ratings
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to check seller ratings
        xpath = Util.GetXpath({"locate":"order_purchase_label_btn_view_shop_rating"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to check seller ratings", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickToCheckSellerRatings')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickToCheckBuyerRatings(arg):
        '''
        ClickToCheckBuyerRatings : Click to check buyer ratings
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to check buyer ratings
        xpath = Util.GetXpath({"locate":"label_btn_view_buyer_rating"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to check buyer ratings", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickToCheckBuyerRatings')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit btn
        xpath = Util.GetXpath({"locate":"order_purchase_label_submit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click finish button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditAddress(arg):
        '''
        ClickEditAddress : Click edit To change address
                Input argu :
                    position - click which edit button depends on number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        position = arg['position']

        ##Click edit button in order detail page
        xpath = Util.GetXpath({"locate":"edit_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath + "[" + position + "]", "message":"Click edit address button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickEditAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddAddress(arg):
        '''
        ClickAddAddress : Click add To add address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click add To add address
        xpath = Util.GetXpath({"locate":"add_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add address btn", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickAddAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditAddressPopup(arg):
        '''
        ClickEditAddressPopup : Click confirm in pop up after change address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click confirm in popup
        xpath = Util.GetXpath({"locate": "pop_up_confirm"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit address pop up", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickEditAddressPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPayNow(arg):
        '''
        ClickPayNow : Click pay now button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Click pay now button
        xpath = Util.GetXpath({"locate":"label_btn_pay_now"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click pay button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickPayNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyAgain(arg):
        '''
        ClickBuyAgain : Click buy again button
                Input argu :
                    type - order_list_page / order_detail_page, which page to click buy again
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click buy again btn in order detail page
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click buy again btn", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickBuyAgain')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBuyAgainVerifyOptions(arg):
        '''
        ClickBuyAgainVerifyOptions : Click options in buy again dialogue (Dialogue will be show when there is an invalid item in order)
                Input argu :
                    action - cancel / confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']

        ##Click option in buy again dialogue
        xpath = Util.GetXpath({"locate":action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click option btn: " + action, "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickBuyAgainVerifyOptions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGuaranteeWindowOptions(arg):
        '''
        ClickGuaranteeWindowOptions : Click guarantee window btn
                Input argu :
                    type - understood, learn_more
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click guarantee window options
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click btn in guarantee pop up window", "result": "1"})

        OK(ret, int(arg["result"]), "OrderButton.ClickGuaranteeWindowOptions")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTransferNow(arg):
        '''
        ClickTransferNow : Click transfer now btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click transfer btn
        xpath = Util.GetXpath({"locate":"transfer_now_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click transfer now btn", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickTransferNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickContactSeller(arg):
        '''
        ClickContactSeller : Click contact seller
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        #Click chat icon
        xpath = Util.GetXpath({"locate":"label_btn_contact_seller"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click contact seller btn", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickContactSeller')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChatIcon(arg):
        '''
        ClickChatIcon : Click chat btn
                Input argu :
                    type - order_list, order_detail
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        #Click chat icon
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click chat btn", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickChatIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGuaranteeIcon(arg):
        '''
        ClickGuaranteeIcon : Click shopee guarantee icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click guarantee icon
        xpath = Util.GetXpath({"locate":"guarantee_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click shopee guarantee btn", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickGuaranteeIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentMethod(arg):
        '''ClickPaymentMethod : Click payment method button
                Input argu :
                            method - payment method you want to click
                Return code : 1 - success
                             0 - fail
                              -1 - error
        '''
        ret = 1
        method = arg['method']

        ##Click payment method button
        xpath = Util.GetXpath({"locate":method})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click payment method button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickPaymentMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentSave(arg):
        '''
        ClickPaymentSave : Click payment save button in order payment change popup window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click payment save button
        xpath = Util.GetXpath({"locate":"payment_label_save"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click payment save button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickPaymentSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreditCard(arg):
        '''
        ClickCreditCard : Click credit card
                Input argu :
                    card_number - credit card number you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_number = arg['card_number']

        ##Click credit card
        xpath = Util.GetXpath({"locate":"credit_card_info"})
        xpath_credit_card_info = xpath.replace("card_number_to_be_replace", card_number)
        BaseUICore.Click({"method":"xpath", "locate":xpath_credit_card_info, "message":"click credit card", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickCreditCard')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickIBankingBank(arg):
        '''
        ClickIBankingBank : Click ibanking bank
                Input argu :
                    bank_name - bank name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank_name = arg['bank_name']

        ##Click ibanking bank
        xpath = Util.GetXpath({"locate":"bank"})
        xpath = xpath.replace("bank_name_to_be_replaced", bank_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ibanking bank", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickIBankingBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBank(arg):
        '''
        ClickBank : Click bank
                Input argu :
                    bank - bank you want to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bank = arg['bank']

        ##Click bank
        xpath = Util.GetXpath({"locate":"bank_info"})
        xpath_credit_card_info = xpath.replace("bank_to_be_replace", bank)
        BaseUICore.Click({"method":"xpath", "locate":xpath_credit_card_info, "message":"click bank", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickBank')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAkulakuPeriod(arg):
        '''
        ClickAkulakuPeriod : Click Akulaku installment
                Input argu :
                    period - akulaku installment period
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        period = arg['period']

        ##Click akulaku installment
        xpath = Util.GetXpath({"locate":"akulaku_period"})
        xpath_period = xpath.replace("period_to_be_replace", period)
        BaseUICore.Click({"method":"xpath", "locate":xpath_period, "message":"click akulaku period", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickAkulakuPeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickKredivoOption(arg):
        '''
        ClickKredivoOption : Click Kredivo option
                Input argu :
                    option - kredivo option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Click kredivo option
        xpath = Util.GetXpath({"locate":"kredivo_option"})
        xpath_option = xpath.replace("option_to_be_replace", option)
        BaseUICore.Click({"method":"xpath", "locate":xpath_option, "message":"click kredivo option", "result": "1"})

        OK(ret, int(arg['result']), 'OrderButton.ClickKredivoOption')


class OrderDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelReturnRefundRequest(arg):
        '''
        ClickCancelReturnRefundRequest : Click cancel return /refund button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Call Click cancel return /refund button
        xpath = Util.GetXpath({"locate":"cancel_request_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"ClickCancelReturnRefundRequest button", "result": "1"})

        ##Click ok button
        xpath = Util.GetXpath({"locate":"ok_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Ok button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderDetailPage.ClickCancelReturnRefundRequest')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseNewAddress(arg):
        '''
        ChooseNewAddress : Choose new address after adding address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        ##Choose new address
        xpath = Util.GetXpath({"locate": "new_address"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new address", "result": "1"})
        time.sleep(2)

        ##Click confirm button
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm", "result": "1"})
        time.sleep(2)

        ##Handle alert
        xpath = Util.GetXpath({"locate": "pop_up_confirm"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose new address pop up", "result": "1"})

        OK(ret, int(arg['result']), 'OrderDetailPage.ChooseNewAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditAddress(arg):
        '''
        EditAddress : Click edit edit address in order detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit address in order detail page
        xpath = Util.GetXpath({"locate":"label_edit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click edit address button", "result": "1"})
        time.sleep(5)

        ##Change current address to another address
        xpath = Util.GetXpath({"locate":"another_address"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"change to another address", "result": "1"})
        time.sleep(3)

        ##Click submit after changing address
        xpath = Util.GetXpath({"locate":"label_submit"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click submit button", "result": "1"})
        time.sleep(10)

        ##Click confirm btn
        xpath = Util.GetXpath({"locate":"label_confirm"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click comfirm change address button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderDetailPage.EditAddress')


class OrderListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def PaymentMethodChange(arg):
        '''
        PaymentMethodChange : Goto order detail page and change payment method
                Input argu :
                    method - payment method
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method = arg["method"]

        time.sleep(5)

        ##Call GetSameClassName to detect payment method and click method
        xpath = Util.GetXpath({"locate":method})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click a payment method", "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click save button", "result": "1"})

        ##Click pop up ok button
        xpath = Util.GetXpath({"locate":"ok_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderListPage.PaymentMethodChange')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchTab(arg):
        '''
        SwitchTab : Switch tab in OrderListPage
                Input argu :
                    page - label_to_pay/label_to_ship/label_to_receive/label_completed/label_cancelled
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page = arg["page"]

        time.sleep(2)

        ##Click tab
        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":page}), "message":"click button", "result": "1"})

        time.sleep(5)
        OK(ret, int(arg['result']), 'OrderListPage.SwitchTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderContent(arg):
        '''
        ClickOrderContent : In OrderListPage list, click order content to see more datail info
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        time.sleep(5)
        if "position" in arg:
            ##Click order content
            position = arg["position"]
            xpath = Util.GetXpath({"locate":"order_content"})
            BaseUICore.Click({"method":"xpath", "locate":xpath + "[" + position + "]", "message":"click order content", "result": "1"})
        else:
            ##Click order content
            xpath = Util.GetXpath({"locate":"order_content"})
            BaseUICore.Click({"method":"xpath", "locate":xpath + "[1]", "message":"click order content", "result": "1"})

        time.sleep(5)
        OK(ret, int(arg['result']), 'OrderListPage.ClickOrderContent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreOrderInToPay(arg):
        '''
        RestoreOrderInToPay : Clean up to pay tab until there's no order
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for count in range(10):
            ##Check if there's any topay order
            xpath = Util.GetXpath({"locate":"order_in_topay"})

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ##Click order more btn
                OrderButton.ClickMore({"result": "1"})

                ##Click cancel order btn (Get cancel_single_order and cancel_all_order transify key)
                xpath_cancel_single_order = Util.GetXpath({"locate":"label_btn_cancel_order"})
                xpath_cancel_all_order = Util.GetXpath({"locate":"label_odp_btn_cancel_all_order"})
                xpath_cancel_btn = xpath_cancel_single_order + " | " + xpath_cancel_all_order
                BaseUICore.Click({"method": "xpath", "locate": xpath_cancel_btn, "message": "Click Cancel order button", "result": "1"})

                ##Choose cancel reason and confirm
                OrderCancelPage.SelectCancelReason({"type":"popup", "reason":"1", "result": "1"})
                time.sleep(3)

                ##Confirm cancel order
                OrderButton.ClickCancelOrderOptions({"type":"yes", "result": "1"})
                time.sleep(3)

                ##Refresh browser and switch order to pay
                BaseUILogic.BrowserRefresh({"message":"Browser refresh", "result": "1"})
                time.sleep(3)
                OrderListPage.SwitchTab({"page":"label_to_pay", "result": "1"})
                time.sleep(3)

            else:
                dumplogger.info("No order in to pay, leave function.")
                ret = 1
                break

        OK(ret, int(arg['result']), 'OrderListPage.RestoreOrderInToPay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreOrderInToShip(arg):
        '''
        RestoreOrderInToShip : Clean up to ship tab until there's no order
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0

        for order_count in range(10):
            ##Check if there's any toship order
            xpath = Util.GetXpath({"locate":"order_in_toship"})

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                ##Click cancel order btn
                cancel_order_xpath = Util.GetXpath({"locate":"cancel_order"})
                BaseUICore.Click({"method":"xpath", "locate":cancel_order_xpath, "message":"Click cancel order button", "result": "1"})
                time.sleep(3)

                ##If popups "you have already applied cancel once", break out from the loop
                already_cancel_once_xpath = Util.GetXpath({"locate":"already_cancel_once"})
                if BaseUICore.CheckElementExist({"method":"xpath", "locate":already_cancel_once_xpath, "passok": "0", "result": "1"}):
                    ret = 1
                    break

                ##Choose a reason
                reason_xpath = Util.GetXpath({"locate":"reason1"})
                BaseUICore.Click({"method":"xpath", "locate":reason_xpath, "message":"click cancel reason", "result": "1"})

                ##Click confirm cancel btn
                OrderButton.ClickCancelOrderOptions({"type":"yes", "result": "1"})
                time.sleep(3)

                ##Refresh the page
                BaseUILogic.BrowserRefresh({"message":"Browser refresh", "result": "1"})
                time.sleep(3)
                OrderListPage.SwitchTab({"page":"label_to_ship", "result": "1"})
                dumplogger.info("Cancel %d order" % (order_count))
                ret = 1

            ##When there is no order left, break out from the loop
            else:
                dumplogger.info("No order in to ship, leave function.")
                ret = 1
                break

        OK(ret, int(arg['result']), 'OrderListPage.RestoreOrderInToShip')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditShopRatings(arg):
        '''
        EditShopRatings : Edit shop ratings
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see shop ratings
        xpath = Util.GetXpath({"locate":"see_shop_ratings"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see shop ratings button", "result": "1"})

        ##Click edit
        xpath = Util.GetXpath({"locate":"edit_ratings"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit ratings button", "result": "1"})

        ##Enter text
        xpath2 = Util.GetXpath({"locate":"buyer_text_content"})
        BaseUICore.Input({"method":"xpath", "locate":xpath2, "string":"Edit ratings", "result": "1"})

        ##Click ok button
        xpath = Util.GetXpath({"locate":"ok_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        ##Click finish button
        xpath = Util.GetXpath({"locate":"finish_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click finish button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderListPage.EditShopRatings')


class OrderPageCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderValidDate(arg):
        '''
        CheckOrderValidDate : Check order valid date
                Input argu :
                    locate - locate of element
                    string_format - string format of datetime
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        string_format = arg['string_format']
        locate = arg['locate']

        ##Get valid date
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(string_format), string_format)

        ##Get and check order valid time
        BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Get date from text
        ##VN - %H:%M %d-%m-%Y, %d-%m-%Y
        ##TH - %d-%m-%Y %H:%M, %d-%m-%Y
        ##Maybe have better pattern, but I can't find one now...
        match_pattern = r"(\d{2}:\d{2}\s\d{2}-\d{2}-\d{4}|\d{2}-\d{2}-\d{4}\s\d{2}:\d{2}|\d{2}-\d{2}-\d{4}|\d{2}\/\d{2}\/\d{4}\s\d{2}:\d{2}|\d{2}\/\d{2}\/\d{4})"
        match = re.match(match_pattern, GlobalAdapter.CommonVar._PageAttributes_)
        if match:
            valid_date = datetime.datetime.strptime(match.group(1), string_format)

        ##Check if DTS > current date
        if valid_date >= current_date:
            dumplogger.info("dts date is bigger than current date!!")
        else:
            ret = 0
            dumplogger.info("dts date is less than current date!!")

        OK(ret, int(arg['result']), 'OrderPageCommon.CheckOrderValidDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderTotalPrice(arg):
        '''
        CheckOrderTotalPrice : Check order total price in all order processing page
                Input argu :
                    locate - locate of element you want to check it display total price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate = arg['locate']

        ##Get element display text (order total price)
        BaseUICore.GetElements({"method": "xpath", "locate": locate, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Filter order total price by regex. Order total price string rule => Mixed number (0-9) and dot (.) and comma (,) Example: 12,345.00
        ##Regex group 1 => Part of order total price (integer)
        ##Regex group 2 => Part of order total price (decimal)
        ##Regex group 3 => Order total price string boundary
        ##Regex group 4 => Order total price only have integer price
        dumplogger.info("Locate element order total price string => \"%s\"" % (GlobalAdapter.CommonVar._PageAttributes_))
        match = re.search(r"([,\.\d]+)[,\.](\d\d)(\D|\b)|([,\.\d]+)", GlobalAdapter.CommonVar._PageAttributes_)

        ##Check regex is match
        if match:
            ##Regex group 2 will match decimal price. If match, this total price have decimal number. Example: 12,345.00
            if match.group(2):
                ##Remove dot and comma from integer price, and add decimal price. Example: 12,345.00 => 12345.00
                order_total_price_str = match.group(1).replace(",", "").replace(".", "") + "." + match.group(2)

            ##This total price only have integer number. Example: 12,345
            else:
                ##Remove dot and comma from integer price. Example: 12,345 => 12345
                order_total_price_str = match.group(4).replace(",", "").replace(".", "")

            dumplogger.info("After remove dot and comma from integer price => \"%s\"" % (order_total_price_str))

            ##Check checkout price data in global create order data
            if "checkout_price_data" in GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]:

                ##Because checkout get API response will receive additional "00000" number, so we will *100000 to handle it. Example: FE display "12,345" but checkout get API response "1234500000"
                order_total_price = int(float(order_total_price_str) * 100000)

                ##Checkout data total price need to round. Example: 56593810 => 56594000 . Because FE display round number
                dumplogger.info("Checkout data price before round: \"%s\"" % (GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]["checkout_price_data"]["total_payable"]))
                checkout_data_total_price = int(round(GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]["checkout_price_data"]["total_payable"], -3))

                ##Check locate element order total price = checkout data total price
                if order_total_price == checkout_data_total_price:
                    ret = 1
                    dumplogger.info("Check order price pass!!! Locate element price is \"%s\". Checkout data price is \"%s\"" % (order_total_price, GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]["checkout_price_data"]["total_payable"]))
                else:
                    ret = 0
                    dumplogger.info("Check order price fail!!! Locate element price is \"%s\". Checkout data price is \"%s\"" % (order_total_price, GlobalAdapter.APIVar._CreateOrderData_["checkout_data"]["checkout_price_data"]["total_payable"]))
            else:
                ret = 0
                dumplogger.info("No any checkout price data in Global variable _CreateOrderData_, maybe your create order by API not run or fail")
        else:
            ret = 0
            dumplogger.info("No any order price string in locate element!!, Please check your locate element have mixed number (0-9) and dot (.) and comma (,)")

        OK(ret, int(arg['result']), 'OrderPageCommon.CheckOrderTotalPrice')


class OrderEvaluationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEvaluation(arg):
        '''
        ClickEvaluation : Click button of Evaluation
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        time.sleep(3)
        if type == "yes_buyer":
            ##Click 5 star
            xpath = Util.GetXpath({"locate":"buyer_five_star"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click 5 star button", "result": "1"})

            ##Enter text
            xpath2 = Util.GetXpath({"locate":"buyer_text_content"})
            BaseUICore.Input({"method":"xpath", "locate":xpath2, "string":"For TWQA buyer test", "result": "1"})

            ##Click finish button
            xpath = Util.GetXpath({"locate":"ok_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click 5 star button", "result": "1"})

        elif type == "yes_seller":
            ##Click 5 star
            xpath = Util.GetXpath({"locate":"seller_five_star"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click 5 star button", "result": "1"})

            ##Enter text
            xpath2 = Util.GetXpath({"locate":"seller_text_content"})
            BaseUICore.Input({"method":"xpath", "locate":xpath2, "string":"For TWQA seller test", "result": "1"})

            ##Click finish button
            time.sleep(3)
            xpath3 = Util.GetXpath({"locate":"finish_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath3, "message":"Click finish button", "result": "1"})

        elif type == "no":
            ##Call Click to click extned guarantee button
            xpath = Util.GetXpath({"locate":"label_cancel"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click no button", "result": "1"})

        OK(ret, int(arg['result']), 'OrderEvaluationPage.ClickEvaluation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEvaluationMessage(arg):
        '''
        InputEvaluationMessage : Input evaluation message
                Input argu :
                    message - message text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        message = arg["message"]

        ##Input evaluation message
        xpath = Util.GetXpath({"locate":"input_erea"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":message, "result": "1"})

        OK(ret, int(arg['result']), 'OrderEvaluationPage.InputEvaluationMessage')

class OrderCancelPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCancelReason(arg):
        '''
        SelectCancelReason : select reason for cancel order
                Input argu :
                    reason - reasons
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason = arg["reason"]

        ##Click cancel reason
        reason_xpath = Util.GetXpath({"locate":"reason{0}".format(reason)})
        BaseUICore.Click({"method":"xpath", "locate":reason_xpath, "message":"click cancel reason", "result": "1"})

        OK(ret, int(arg['result']), 'OrderCancelPage.SelectCancelReason -> ' + reason)

class OrderReturnRefundPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundProductCheckbox(arg):
        '''
        ClickReturnRefundProductCheckbox : Click return refund product checkbox
                Input argu :
                    product_name - product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click return refund product checkbox
        xpath = Util.GetXpath({"locate":"product_name_checkbox"})
        xpath_product_name_checkbox = xpath.replace("product_name_to_be_replace", product_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_product_name_checkbox, "message":"Click return refund product checkbox", "result": "1"})

        ##Record return product name to prepare retry return refund
        GlobalAdapter.OrderE2EVar._ReturnProductNameList_.append(product_name)

        OK(ret, int(arg['result']), 'OrderReturnRefundPage.ClickReturnRefundProductCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnRefundShopCheckbox(arg):
        '''
        ClickReturnRefundShopCheckbox : Click return refund shop checkbox
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return refund shop checkbox
        xpath = Util.GetXpath({"locate":"shop_checkbox"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click return refund shop checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'OrderReturnRefundPage.ClickReturnRefundShopCheckbox')
