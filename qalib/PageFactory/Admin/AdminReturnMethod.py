﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminReturnMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import re
import time
import datetime

##Import framework common library
import Util
import XtFunc
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminReturnRefundPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRefundID(arg):
        '''
        CheckRefundID : Check refund ID in admin return refund page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check refund ID in admin return refund page
        xpath = Util.GetXpath({"locate":"refund_id_txt"})
        xpath = xpath.replace('refund_id_to_be_replace', GlobalAdapter.OrderE2EVar._RefundID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundPage.CheckRefundID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckReturnID(arg):
        '''
        CheckReturnID : Check return ID in all admin return refund page
                Input argu :
                    page_type - return_refund_detail_page / return_refund_work_station_page / dispute_manage_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Check return ID in all admin return refund page
        xpath = Util.GetXpath({"locate":page_type})
        xpath = xpath.replace('return_id_to_be_replace', GlobalAdapter.OrderE2EVar._ReturnID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundPage.CheckReturnID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckOrderID(arg):
        '''
        CheckOrderID : Check order ID in all admin return refund page
                Input argu :
                    page_type - return_refund_detail_page / return_refund_work_station_page / dispute_manage_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Check order ID in all admin return refund page
        xpath = Util.GetXpath({"locate":page_type})
        xpath = xpath.replace('order_id_to_be_replace', GlobalAdapter.OrderE2EVar._OrderID_)
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundPage.CheckOrderID')


class AdminReturnRefundDetailButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDrcBackend(arg):
        '''
        ClickDrcBackend : Click drc backend button in return refund detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click drc backend button in return refund detail page
        xpath = Util.GetXpath({"locate": "drc_be_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drc backend button in return refund detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailButton.ClickDrcBackend')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit to change return status
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit to change status
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAttachment(arg):
        '''
        ClickAttachment : Click attechment in return refund detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click attechment in return refund detail page
        xpath = Util.GetXpath({"locate": "attachment_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click attechment in return refund detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailButton.ClickAttachment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnStatusList(arg):
        '''
        ClickReturnStatusList : Click return status list in return refund detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click return status list in return refund detail page
        xpath = Util.GetXpath({"locate": "select_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click return status list in return refund detail page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailButton.ClickReturnStatusList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCodingMonkey(arg):
        '''
        ClickCodingMonkey : Click coding monkey button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click coding monkey button
        xpath = Util.GetXpath({"locate": "coding_monkey_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click coding monkey button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailButton.ClickCodingMonkey')


class AdminReturnRefundDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectReturnStatus(arg):
        '''
        SelectReturnStatus : Click edit to change status
                Input argu :
                    return_status - return status will be selected, ex.return_cancelled, return_judging, return_closed, return_accepted
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        return_status = arg["return_status"]
        ret = 1

        ##Click select list
        xpath = Util.GetXpath({"locate": "return_status"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"text", "selectkey":return_status.upper(), "result": "1"})

        ##Enter reason
        xpath = Util.GetXpath({"locate": "change_status_reason"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Automation Restore", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailPage.SelectReturnStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckReturnDate(arg):
        '''
        CheckReturnDate : Check return valid date
                Input argu :
                    location - return date display in which location.  ex:detail_page/log_table
                    return_state - which state display in return log.  ex.return_requested/return_cancelled/return_judging/return_refund_paid/return_accepted
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        location = arg["location"]
        return_state = arg["return_state"]

        if Config._TestCaseRegion_ == "PH":
            ##For PH region, date display is "MM/DD/YYYY"
            date_format = "%m/%d/%Y"
            date_regex = r"\/\d\d\/"
        else:
            ##For other region, date display is "DD-MM-YYYY"
            date_format = "%d-%m-%Y"
            date_regex = r"\-\d\d\-"

        ##Get current date
        current_date = datetime.datetime.strptime(XtFunc.GetCurrentDateTime(date_format), date_format)

        ##Get and check return valid time
        xpath = Util.GetXpath({"locate":location})

        ##If return_state not empty need replace string
        if return_state:
            xpath = xpath.replace('state_to_replace', return_state.upper())

        BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype": "text", "mode": "single", "result": "1"})

        ##Get date from text
        date_start = re.search(date_regex, GlobalAdapter.CommonVar._PageAttributes_).span()[0] - 2
        date_end = re.search(date_regex, GlobalAdapter.CommonVar._PageAttributes_).span()[1] + 4
        valid_date = datetime.datetime.strptime(GlobalAdapter.CommonVar._PageAttributes_[date_start:date_end], date_format)

        ##Check if admin display vaild date <= current date
        if valid_date <= current_date:
            dumplogger.info("admin display vaild date is equal or less than current date!!")
        else:
            ret = 0
            dumplogger.info("admin display vaild date is bigger than current date!!")

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailPage.CheckReturnDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCheckOutID(arg):
        '''
        CheckCheckOutID : Check check out ID
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"checkout_id"})
        xpath = xpath.replace('replace_checkout_id', str(GlobalAdapter.OrderE2EVar._CheckOutID_))
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailPage.CheckCheckOutID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckReturnLogState(arg):
        '''
        CheckReturnLogState : check return log state
                Input argu :
                    return_state - which state display in return log ex.return_requested/return_cancelled/return_judging/return_refund_paid/return_accepted
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        return_state = arg['return_state']
        ret = 1

        ##Get xpath by return state
        xpath = Util.GetXpath({"locate":"state_xpath"})
        xpath = xpath.replace('state_to_replace', return_state.upper())
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDetailPage.CheckReturnLogState')


class AdminReturnRefundRequestPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetReturnSN(arg):
        '''
        GetReturnSN : get return sn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input return id to search for return detail
        for retry_times in range(1, 6):

            ##Click notice popup close button
            xpath = Util.GetXpath({"locate":"notice_popup_close_btn"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click notice popup close button", "result": "1"})

            ##Input return id
            xpath = Util.GetXpath({"locate": "input_return_id"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.OrderE2EVar._ReturnID_, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

            ##Wait for page load
            time.sleep(5)

            ##Get refund sn from the searching result
            xpath = Util.GetXpath({"locate": "return_sn"})
            xpath = xpath.replace('replaced_text', GlobalAdapter.OrderE2EVar._ReturnID_)

            if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info("Return SN found!")
                BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "return_sn", "result": "1"})
                break
            elif retry_times == 5:
                dumplogger.info("Return SN not found after querying %d times...." % (retry_times))
                ret = 0
            else:
                dumplogger.info("Return SN not found after querying %d times...." % (retry_times))
                BaseUILogic.BrowserRefresh({"message":"Refresh for querying return sn in admin", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundRequestPage.GetReturnSN')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToReturnRefundDetail(arg):
        '''
        GoToReturnRefundDetail : Go to Return Refund detail
                Input argu :
                    search_type - seller / buyer
                    username - use seller or buyer account to search for orders
                    row_number - the row number of the order which you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        search_type = arg["search_type"]
        username = arg["username"]
        row_number = arg["row_number"]

        ##Click notice popup close button
        xpath = Util.GetXpath({"locate":"notice_popup_close_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click notice popup close button", "result": "1"})

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##Input seller name and search
        xpath = Util.GetXpath({"locate":search_type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":username, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        ##Check target user on list page
        xpath = Util.GetXpath({"locate":"target_" + search_type + "_name"})
        xpath = xpath.replace('replace_user', username)
        time.sleep(2)

        ##Wait before table reload before next step
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number":"5", "result": "1"})

        ##Click return id
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"return_id"})
        xpath = xpath.replace('replace_row_number', str(int(row_number)+1))
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click target table", "result": "1"})

        ##Click notice popup close button
        xpath = Util.GetXpath({"locate":"notice_popup_close_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click notice popup close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundRequestPage.GoToReturnRefundDetail -> ' + username)


class AdminReturnRefundDisputeManageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReturnInfoTab(arg):
        '''
        ClickReturnInfoTab : Click return info tab
                Input argu :
                    role_type - buyer / seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        role_type = arg["role_type"]

        ##Click save button
        xpath = Util.GetXpath({"locate": role_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click return info tab => %s" % (role_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminReturnRefundDisputeManageButton.ClickReturnInfoTab')
