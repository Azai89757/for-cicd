import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCFreeShippingVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCFreeShippingVoucher Precondition =="
        dumplogger.info("== Setup VNPCFreeShippingVoucher Precondition ==")

    def test_VNPCFreeShippingVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCFreeShippingVoucher-01.xml")

    def test_VNPCFreeShippingVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCFreeShippingVoucher-02.xml")

    def test_VNPCFreeShippingVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCFreeShippingVoucher-03.xml")

    def test_VNPCFreeShippingVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCFreeShippingVoucher-04.xml")

    def test_VNPCFreeShippingVoucher05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCFreeShippingVoucher-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCFreeShippingVoucher Post condition =="
        dumplogger.info("== Setup VNPCFreeShippingVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
