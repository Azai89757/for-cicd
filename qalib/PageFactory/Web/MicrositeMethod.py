#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 MicroSiteMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class MicrositePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionGridSeeAllAndCheckUrl(arg):
        '''
        ClickCollectionGridSeeAllAndCheckUrl : Click see all and check url
                Input argu :
                    check_url - check url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        check_url = arg['check_url']

        #Click see all btn
        xpath = Util.GetXpath({"locate":"see_all"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see all btn", "result": "1"})

        ##Check url
        BaseUILogic.GetAndCheckBrowserCurrentUrl({"url":check_url, "result": "1"})

        OK(ret, int(arg['result']), 'MicrositePage.ClickCollectionGridSeeAllAndCheckUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAnchorBarComponent(arg):
        '''
        ClickAnchorBarComponent : Click anchor bar component
                Input argu :
                    component - component
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component = arg['component']

        ##Click dropdown and click component
        xpath_dropdown = Util.GetXpath({"locate":"dropdown"})
        xpath_drawer_component = Util.GetXpath({"locate":"dropdown_item"})
        xpath_tab_component = Util.GetXpath({"locate":"tab_item"})

        ##Check if dropdown exist, if not just click component
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath_dropdown, "passok": "0", "result": "1"}):
            ##Click drop down menu
            BaseUICore.Click({"method":"xpath", "locate":xpath_dropdown, "message":"Click drop down menu", "result": "1"})
            time.sleep(10)

            ##Click drawer component
            xpath_drawer_component = xpath_drawer_component.replace('replaced_text', component)
            BaseUICore.Click({"method":"xpath", "locate":xpath_drawer_component, "message":"Click %s from drop down menu" % component, "result": "1"})
            time.sleep(5)

        else:
            xpath_tab_component = xpath_tab_component.replace('replaced_text', component)
            BaseUICore.Click({"method":"xpath", "locate":xpath_tab_component, "message":"Click {0}".format(component), "result": "1"})

        OK(ret, int(arg['result']), 'MicrositePage.ClickAnchorBarComponent')

class MicrositeButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProduct(arg):
        '''
        ClickProduct : Click product
                Input argu :
                    name - name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click product
        xpath = Util.GetXpath({"locate":"product"})
        xpath = xpath.replace("name_to_be_replace", name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product", "result":"1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotLink(arg):
        '''
        ClickHotspotLink : Click hot spot link in image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]

        ##Click hot spot link
        xpath = Util.GetXpath({"locate": "hotspot_link"})
        xpath = xpath.replace("order_to_be_replace", order)
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message": "Click hotspot link", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickHotspotLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotPopup(arg):
        '''
        ClickHotspotPopup : Click hotspot popup in image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot popup
        xpath = Util.GetXpath({"locate": "hotspot_popup"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspot popup", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickHotspotPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotReminder(arg):
        '''
        ClickHotspotReminder : Click hotspot reminder in image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot reminder
        xpath = Util.GetXpath({"locate": "hotspot_reminder"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspot reminder", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickHotspotReminder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more button
                Input argu :
                    promotion_name - promotion name to be click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_name = arg["promotion_name"]

        ##Click see more
        xpath = Util.GetXpath({"locate": "see_more_btn"})
        xpath = xpath.replace("name_to_be_replaced", promotion_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click see more", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTab(arg):
        '''
        ClickTab : Click tab
                Input argu :
                    tab - tab name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab = arg["tab"]

        ##Click tab
        xpath = Util.GetXpath({"locate": "tab_buttonn"})
        xpath = xpath.replace("tab_name_to_be_replaced", tab)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tab", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAnchorTabBarMore(arg):
        '''
        ClickAnchorTabBarMore : Click anchor tab bar more button
                Input argu :
                    action - open / close
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]

        ##Click anchor tab bar more button
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click anchor tab bar more button", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickAnchorTabBarMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickIamgeTab(arg):
        '''
        ClickIamgeTab : Click image tab
                Input argu :
                    tab_index - image tab index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_index = arg["tab_index"]

        ##Click image tab
        xpath = Util.GetXpath({"locate": "tab_button"})
        xpath = xpath.replace("tab_index_to_be_replaced", tab_index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click image tab", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickIamgeTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeAll(arg):
        '''
        ClickSeeAll : Click see all
                Input argu :
                    type - header / footer
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click see all
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click see all", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickSeeAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCartIcon(arg):
        '''
        ClickAddToCartIcon : Click product
                Input argu :
                    product_name - product_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click product
        xpath = Util.GetXpath({"locate": "cart_icon"})
        xpath = xpath.replace("name_to_be_replace", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickAddToCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotAddToCart(arg):
        '''
        ClickHotspotAddToCart : Click hotspot add to cart
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot add to cart
        xpath = Util.GetXpath({"locate": "hotspot_add_to_cart"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspot add to cart", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickHotspotAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHotspotCopy(arg):
        '''
        ClickHotspotCopy : Click hotspot copy
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click hotspot copy
        xpath = Util.GetXpath({"locate": "hotspot_copy"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click hotspot copy", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickHotspotCopy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBundleItem(arg):
        '''
        ClickBundleItem : Click item in bundledeal component
                Input arg :
                    index - the order of items from left to right (value is from 1 to 3)
                    bundle_name - bundle name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1
        index = arg["index"]
        bundle_name = arg["bundle_name"]

        ##Click item
        xpath = Util.GetXpath({"locate":"bundle_item"})
        xpath = xpath.replace("bundle_name_to_be_replaced", bundle_name)
        xpath = xpath.replace("index_to_be_replaced", index)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click bundle item ", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickBundleItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherTAndC(arg):
        '''
        ClickVoucherTAndC : Click Voucher T&C
                Input argu :
                    index - several voucher name index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Click voucher T&C
        xpath = Util.GetXpath({"locate":"t&c_btn"})
        xpath_t_and_c_btn = xpath.replace("index_to_be_replace", index)
        BaseUICore.Click({"method":"xpath", "locate":xpath_t_and_c_btn, "message":"Click voucher T&C", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickVoucherTAndC')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectLanguage(arg):
        '''
        SelectLanguage : Select Language
                Input argu :
                    language - select lauguage
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        language = arg['language']

        ##Click multi language drop down and select language
        xpath_drop_down = Util.GetXpath({"locate": "drop_down"})
        xpath_select_language = Util.GetXpath({"locate": "select_language"})
        xpath_select_language = xpath_select_language.replace('replaced_text', language)
        BaseUICore.Move2ElementAndClick({"method": "xpath", "locate": xpath_drop_down, "locatehidden": xpath_select_language, "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.SelectLanguage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGridCardAddToCartIcon(arg):
        '''
        ClickGridCardAddToCartIcon : Click grid card add to cart icon
                Input argu :
                    product_name - product_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click grid card add to cart icon
        xpath = Util.GetXpath({"locate": "cart_icon"})
        xpath = xpath.replace("name_to_be_replace", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click grid card add to cart icon ", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickGridCardAddToCartIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseCookiesMessage(arg):
        '''
        CloseCookiesMessage : Close cookies message
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate": "cookies_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close cookies message", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.CloseCookiesMessage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTextsLink(arg):
        '''
        ClickTextsLink : Click texts link in image
                Input argu :
                    text - text to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Click texts link
        xpath = Util.GetXpath({"locate": "texts_link"})
        xpath = xpath.replace("replaced_text", text)
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message": "Click texts link", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickTextsLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRightArrow(arg):
        '''
        ClickRightArrow : Click right arrow
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click right arrow
        xpath = Util.GetXpath({"locate": "right_arrow"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click right arrow", "result": "1"})

        OK(ret, int(arg['result']), 'MicrositeButton.ClickRightArrow')
