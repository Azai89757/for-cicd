import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCHomePage Precondition =="
        dumplogger.info("== Setup ESPCHomePage Precondition ==")

    def test_ESPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCHomePage-01.xml")

    def test_ESPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCHomePage-02.xml")

    def test_ESPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCHomePage-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCHomePage Post condition =="
        dumplogger.info("== Setup ESPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
