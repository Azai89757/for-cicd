import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCCartTK(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCartTK Precondition =="
        dumplogger.info("== Setup VNPCCartTK Precondition ==")

    def test_VNPCCartTK01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCCartTK-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCCartTK Post condition =="
        dumplogger.info("== Setup VNPCCartTK Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
