#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCampaignModuleMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import Util
import XtFunc
import GlobalAdapter

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic
from PageFactory.Admin import AdminCommonMethod

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminCampaignModulePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyVisualComponent(arg):
        '''
        ModifyVisualComponent : Modify visual component content
                Input argu :
                    title - visual title (if no input, then no action)
                    mobile_image_name - mobile image file name (if no input, then no action)
                    mobile_image_type - mobile image file type
                    pc_image_name - pc image file name (if no input, then no action)
                    pc_image_type - pc image file type
                    visibility_type - visible / invisible (if no input, then no action)
                    start_time - component start time (if no input, then no action)
                    end_time - component end time (if no input, then no action)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        title = arg["title"]
        mobile_image_name = arg["mobile_image_name"]
        mobile_image_type = arg["mobile_image_type"]
        pc_image_name = arg["pc_image_name"]
        pc_image_type = arg["pc_image_type"]
        visibility_type = arg["visibility_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Component title
        if title:
            AdminCampaignModulePage.ModifyComponentTitle({"title": title, "result": "1"})

        ##Mobile image upload
        if mobile_image_name:
            xpath = Util.GetXpath({"locate":"mobile_image_upload"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name":mobile_image_name, "file_type":mobile_image_type, "result":"1"})
            else:
                dumplogger.info("Not able to find this xpath!!!")
                ret = 0
            time.sleep(10)

        ##PC image upload
        if pc_image_name:
            xpath = Util.GetXpath({"locate":"pc_image_upload"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name":pc_image_name, "file_type":pc_image_type, "result":"1"})
            else:
                dumplogger.info("Not able to find this xpath!!!")
                ret = 0
            time.sleep(10)

        ##Choose visibility
        if visibility_type:
            AdminCampaignModulePage.ModifyComponentVisibility({"visibility_type": visibility_type, "result": "1"})

        ##Input start / end time
        if start_time or end_time:
            AdminCampaignModulePage.ModifyComponentTime({"start_time": start_time, "end_time": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyVisualComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUrl(arg):
        '''
        InputUrl : Input url (only for top visual)
                Input argu :
                    url - url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        url = arg["url"]

        ##Input url
        xpath = Util.GetXpath({"locate":"url_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate": xpath, "string": url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.InputUrl')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddHomepageComponent(arg):
        '''
        AddHomepageComponent : Add and insert component basic input
                Input argu :
                    type - component type (feature_products / feature_collections / feature_shops / top_visual / bottom_visual)
                    component_title - component title name
                    visibility_type - visible / invisible
                    start_time - component start time
                    end_time - component end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        component_title = arg["component_title"]
        visibility_type = arg["visibility_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Click add new component
        AdminCampaignModulePageButton.ClickAddNewComponent({"result": "1"})

        ##Click choose component type
        AdminCampaignModulePage.ModifyComponentType({"type": type, "result": "1"})

        ##Input title name
        AdminCampaignModulePage.ModifyComponentTitle({"title": component_title, "result": "1"})

        ##Input visibility
        AdminCampaignModulePage.ModifyComponentVisibility({"visibility_type": visibility_type, "result": "1"})

        ##Modify component start/end time
        AdminCampaignModulePage.ModifyComponentTime({"start_time": start_time, "end_time": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.AddHomepageComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyHomepageComponent(arg):
        '''
        ModifyHomepageComponent : Modify homepage component content
                Input argu :
                    type - component type (use this for distinguish component)
                    product_option - product option (fully_manual / fully_system / partial_manual)
                    visibility_type - visible / invisible (if no input, then no action)
                    file_name - upload file name (if no input, then no action)
                    file_type - upload file type (if file_name does not input, then no action)
                    header_text_en - header text in English (if no input, then no input)
                    header_text_zh - header text in Simplified Chinese (if no input, then no input)
                    header_text_others - header text in language other than English and Simplified Chinese (if no input, then no input)
                    start_time - feature start time (if no input, then no action)
                    end_time - feature end time (if no input, then no action)
                    link - redirection link (if no input, then regard as hidden link)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        product_option = arg["product_option"]
        visibility_type = arg["visibility_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]
        header_text_en = arg["header_text_en"]
        header_text_zh = arg["header_text_zh"]
        header_text_others = arg["header_text_others"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        link = arg["link"]

        ##Choose visibility
        if visibility_type:
            AdminCampaignModulePage.ModifyComponentVisibility({"visibility_type": visibility_type, "result": "1"})

        ##Choose product option
        xpath = Util.GetXpath({"locate":product_option})
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message":"Choose product option -> " + product_option, "result": "1"})

        if file_name and product_option != "fully_system":
            ##Upload csv file
            xpath = Util.GetXpath({"locate":"file_upload"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "file", "file_name":file_name, "file_type":file_type, "result":"1"})
            else:
                dumplogger.info("Not able to find this xpath!!!")
                ret = 0
            time.sleep(10)

        ##Input header text
        if header_text_en or header_text_zh or header_text_others:
            ##Input header text in different language
            AdminCampaignModulePage.InputHeaderText({"text_en": header_text_en, "text_zh": header_text_zh, "text_others": header_text_others, "result": "1"})

        ##Input start / end time
        if start_time or end_time:
            AdminCampaignModulePage.ModifyComponentTime({"start_time": start_time, "end_time": end_time, "result": "1"})

        ##Input redirection link if needed
        if link:
            ##Check the show btn
            xpath = Util.GetXpath({"locate":"show_btn"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click":"1", "result": "1"})

            ##Input link url
            xpath = Util.GetXpath({"locate":"input_link"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string":link, "result": "1"})
            time.sleep(5)

            ##Set see more type to "text"
            AdminCampaignModulePage.ModifyComponentSeeMoreType({"see_more_type": "text", "static_upload_type": "", "static_image_name": "", "static_image_type": "", "gif_upload_type": "", "gif_image_name": "", "gif_image_type": "", "result": "1"})
        else:
            ##If the state is "show" initially, then delete url and make state be "hidden"
            xpath = Util.GetXpath({"locate":"checked_show_btn"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                xpath = Util.GetXpath({"locate":"input_link"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":"", "result": "1"})

            ##Check the hidden btn
            xpath = Util.GetXpath({"locate":"hidden_btn"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click":"1", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyHomepageComponent -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCampaignBackground(arg):
        '''
        ModifyCampaignBackground : Modify campaign background content
                Input argu :
                    two_row_image_name - 2-row image name
                    two_row_image_type - 2-row image type
                    one_row_image_name - 1-row image name
                    one_row_image_type - 1-row image type
                    pc_image_name - pc image name
                    pc_image_type - pc image type
                    visibility_type - visible / invisible
                    start_time - feature start time
                    end_time - feature end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        two_row_image_name = arg["two_row_image_name"]
        two_row_image_type = arg["two_row_image_type"]
        one_row_image_name = arg["one_row_image_name"]
        one_row_image_type = arg["one_row_image_type"]
        pc_image_name = arg["pc_image_name"]
        pc_image_type = arg["pc_image_type"]
        visibility_type = arg["visibility_type"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Choose visibility type
        if visibility_type:
            AdminCampaignModulePage.ModifyComponentVisibility({"visibility_type": visibility_type, "result": "1"})

        ##Upload 1-row image
        if one_row_image_name:
            xpath = Util.GetXpath({"locate":"one_row_image_input"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name":one_row_image_name, "file_type":one_row_image_type, "result":"1"})
            else:
                dumplogger.info("Not able to find this xpath!!!")
                ret = 0
            time.sleep(10)

        ##Upload 2-row image
        if two_row_image_name:
            xpath = Util.GetXpath({"locate":"two_row_image_input"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name":two_row_image_name, "file_type":two_row_image_type, "result":"1"})
            else:
                dumplogger.info("Not able to find this xpath!!!")
                ret = 0
            time.sleep(10)

        ##Upload pc image
        if pc_image_name:
            xpath = Util.GetXpath({"locate":"pc_image_input"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name":pc_image_name, "file_type":pc_image_type, "result":"1"})
            else:
                dumplogger.info("Not able to find this xpath!!!")
                ret = 0
            time.sleep(10)

        ##Adjust start time and end time
        if start_time or end_time:
            AdminCampaignModulePage.ModifyComponentTime({"start_time": start_time, "end_time": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyCampaignBackground')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentType(arg):
        '''
        ModifyComponentType : Modify homepage component type
                Input argu :
                    type - component type (bottom_visual / campaign_background / feature_collections / feature_products / feature_shops / top_visual)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click component type dropdown
        xpath = Util.GetXpath({"locate":"type_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message":"Click type dropdown", "result": "1"})

        ##Choose type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message":"Choose type -> " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyComponentType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentTitle(arg):
        '''
        ModifyComponentTitle : Modify homepage component title
                Input argu :
                    title - component title
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Do FET of title name
        title = arg["title"]
        xpath = Util.GetXpath({"locate":"title_name"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate": xpath, "string": title, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyComponentTitle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentVisibility(arg):
        '''
        ModifyComponentVisibility : Modify homepage component visibility
                Input argu :
                    visibility_type - visible / invisible
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        visibility_type = arg["visibility_type"]

        ##Choose visible/not visible
        xpath = Util.GetXpath({"locate":visibility_type})
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message":"Choose visibility", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyComponentVisibility')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentTime(arg):
        '''
        ModifyComponentTime : Do FET on homepage component time
                Input argu :
                    start_time - component start display time (if no input, then no action)
                    end_time - component end display time (if no input, then no action)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Do FET of start time and end time
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time
        ##Click start time calender
        if start_time:
            xpath = Util.GetXpath({"locate":"start_time_calender"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click start time calender", "result": "1"})

            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M:%S", 0, int(start_time), 0)

            ##Input promotion start date
            xpath = Util.GetXpath({"locate":"start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(3)

        ##Input end time
        ##Click end time calender
        if end_time:
            xpath = Util.GetXpath({"locate":"end_time_calender"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click end time calender", "result": "1"})

            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M:%S", 0, int(end_time), 0)

            ##Input promotion end date
            xpath = Util.GetXpath({"locate":"end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyComponentTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTextColorHex(arg):
        '''
        InputTextColorHex : Input text color hex
                Input argu :
                    type - header / price / default_promotion / collection
                    color_hex - color hex
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        color_hex = arg["color_hex"]

        ##Input header text color hex
        xpath = Util.GetXpath({"locate": type})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate": xpath, "string": color_hex, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.InputTextColorHex')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseHeaderBackgroundColor(arg):
        '''
        ChooseHeaderBackgroundColor : Choose header background color
                Input argu :
                    type - transparent / color
                    color_hex - color hex (if no input, then no action)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        color_hex = arg["color_hex"]

        ##Click choose color type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose color type", "result": "1"})

        ##Input color hex
        if type == "color" and color_hex:
            xpath = Util.GetXpath({"locate": "hex_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": color_hex, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ChooseHeaderBackgroundColor')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputManualComponentNumber(arg):
        '''
        InputManualComponentNumber : Input manual component number (only for partial manual upload)
                Input argu :
                    number - manual component number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input manual component number
        xpath = Util.GetXpath({"locate":"manual_number"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate": xpath, "string": number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.InputManualComponentNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAssetGroupID(arg):
        '''
        InputAssetGroupID : Input asset group id (only for feature collection / feature shop)
                Input argu :
                    type - feature_collections / feature_shops
                    method - auto / manual (determine whether input automatically or manually)
                    asset_group_id - asset group id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        method = arg["method"]
        asset_group_id = arg["asset_group_id"]

        ##Asset id list (for auto input)
        shop_asset_group_id = {
            "VN": "5",
            "PH": "9",
            "MX": "9",
            "BR": "4",
            "MY": "5",
            "SG": "20",
            "TH": "14",
            "CO": "10001",
            "CL": "10001",
            "PL": "2",
            "ES": "2",
            "IN": "2",
            "FR": "2",
            "AR": "1",
            "TW": "10012",
            "ID": "10431"
        }
        collection_asset_group_id = {
            "VN": "7",
            "PH": "4",
            "MX": "2",
            "BR": "3",
            "MY": "2",
            "SG": "20",
            "TH": "1",
            "CO": "3",
            "CL": "15",
            "PL": "3",
            "ES": "2",
            "IN": "2",
            "FR": "2",
            "AR": "1",
            "TW": "1596",
            "ID": "247"
        }

        ##Clear asset group id input
        xpath = Util.GetXpath({"locate":"asset_id_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})

        ##If input in manual, input the parameter; Otherwise enter given auto id
        if method == "manual":
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": asset_group_id, "result": "1"})
        elif method == "auto":
            if type == "feature_collections":
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": collection_asset_group_id[Config._TestCaseRegion_], "result": "1"})
            elif type == "feature_shops":
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": shop_asset_group_id[Config._TestCaseRegion_], "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.InputAssetGroupID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputHeaderText(arg):
        '''
        InputHeaderText : Input header text in campaign module page
                Input argu :
                    text_en - English header text
                    text_zh - Simplified Chinese header text
                    text_others - Language other than English and simplified Chinese header text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text_en = arg["text_en"]
        text_zh = arg["text_zh"]
        text_others = arg["text_others"]

        ##Input header text
        if text_en:
            xpath = Util.GetXpath({"locate":"text_en_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_en, "result": "1"})

        if text_zh:
            xpath = Util.GetXpath({"locate":"text_zh_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_zh, "result": "1"})

        if text_others:
            xpath = Util.GetXpath({"locate":"text_others_input"})
            BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_others, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.InputHeaderText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDefaultPromotionText(arg):
        '''
        InputDefaultPromotionText : Input default promotion text (only for feature shop)
                Input argu :
                    text - default promotion text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg["text"]

        ##Input default promotion text
        xpath = Util.GetXpath({"locate":"text_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.InputDefaultPromotionText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductLabel(arg):
        '''
        ChooseProductLabel : Choose product label (only for feature shop / feature product)
                Input argu :
                    label_name - label name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        label_name = arg["label_name"]

        ##Click label dropdown
        xpath = Util.GetXpath({"locate":"product_label_dropdown"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product label dropdown", "result": "1"})
        time.sleep(1)

        ##Input label name
        xpath = Util.GetXpath({"locate":"product_label_input"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate": xpath, "string": label_name, "result": "1"})
        time.sleep(1)

        ##Click choose label search result
        xpath = Util.GetXpath({"locate":"product_label_result"})
        xpath = xpath.replace("name_to_be_replaced", label_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose label", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ChooseProductLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CleanHomepageComponent(arg):
        '''
        CleanHomepageComponent : Clean homepage component
                Input argu :
                    component_type - component type (top_visual / bottom_visual / campaign_background / feature_products / feature_collections / feature_shops)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        component_type = arg["component_type"]

        ##Get quantity of specific component
        xpath = Util.GetXpath({"locate":component_type})
        BaseUILogic.GetAndReserveElements({"locate": xpath, "reservetype": "count", "result": "1"})
        component_count = GlobalAdapter.GeneralE2EVar._Count_

        for delete_time in range(component_count):
            ##Click delete component
            xpath = Util.GetXpath({"locate":component_type})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete component", "result": "1"})

            ##Click confirm
            xpath = Util.GetXpath({"locate":"confirm_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})
            time.sleep(10)

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.CleanHomepageComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCountDownTimerSetting(arg):
        '''
        ModifyCountDownTimerSetting : Modify content of count down timer
                Input argu :
                    countdown_toggle_on - countdown timer toggle on or not (1 / 0)
                    countdown_color_hex - color hex of countdown timer
                    countdown_block_number_color_hex - color hex of countdown timer block number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        countdown_toggle_on = arg["countdown_toggle_on"]
        countdown_color_hex = arg["countdown_color_hex"]
        countdown_block_number_color_hex = arg["countdown_block_number_color_hex"]

        if int(countdown_toggle_on):
            ##Turn on the toggle if initially toggle off
            xpath = Util.GetXpath({"locate":"toggle_off"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to toggle on countdown timer", "result": "1"})
                time.sleep(1)

            ##If there's input in countdown color hex
            if countdown_color_hex:
                xpath = Util.GetXpath({"locate":"countdown_color_hex_input"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": countdown_color_hex, "result": "1"})
                time.sleep(1)

            ##If there's input in countdown block number color hex
            if countdown_block_number_color_hex:
                xpath = Util.GetXpath({"locate":"countdown_block_number_color_hex_input"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": countdown_block_number_color_hex, "result": "1"})
                time.sleep(1)

        else:
            ##Turn off the toggle if initially toggle on
            xpath = Util.GetXpath({"locate":"toggle_on"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to toggle off countdown timer", "result": "1"})
                time.sleep(1)

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyCountDownTimerSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCustomTextBeforeCountDownTimerSetting(arg):
        '''
        ModifyCustomTextBeforeCountDownTimerSetting : Modify custom text before count down timer content
                Input argu :
                    countdown_before_text_toggle_on - text before countdown timer toggle on or not (1 / 0)
                    text_content_en - Text content in English
                    text_content_zh - Text content in Simplified Chinese
                    text_content_others - Text content in language other than English and Simplified Chinese
                    text_content_color_hex - color hex of text content color
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        countdown_before_text_toggle_on = arg["countdown_before_text_toggle_on"]
        text_content_en = arg["text_content_en"]
        text_content_zh = arg["text_content_zh"]
        text_content_others = arg["text_content_others"]
        text_content_color_hex = arg["text_content_color_hex"]

        if int(countdown_before_text_toggle_on):
            ##Turn on the toggle if initially toggle off
            xpath = Util.GetXpath({"locate":"toggle_off"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to toggle on custom text before countdown timer", "result": "1"})
                time.sleep(1)

            ##Input text content in English
            if text_content_en:
                xpath = Util.GetXpath({"locate":"text_en_input"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_content_en, "result": "1"})
                time.sleep(1)

            ##Input text content in Simplified Chinese
            if text_content_zh:
                xpath = Util.GetXpath({"locate":"text_zh_input"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_content_zh, "result": "1"})
                time.sleep(1)

            ##Input text content in language other than English and Simplified Chinese
            if text_content_others:
                xpath = Util.GetXpath({"locate":"text_others_input"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_content_others, "result": "1"})
                time.sleep(1)

            ##Input custom text hex input
            if text_content_color_hex:
                xpath = Util.GetXpath({"locate":"custom_text_color_hex_input"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate": xpath, "string": text_content_color_hex, "result": "1"})
                time.sleep(1)

        else:
            ##Turn off the toggle if initially toggle on
            xpath = Util.GetXpath({"locate":"toggle_on"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to toggle off custom text before countdown timer", "result": "1"})
                time.sleep(1)

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyCustomTextBeforeCountDownTimerSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyAnimatedTopVisual(arg):
        '''
        ModifyAnimatedTopVisual : Modify animated top visual content
                Input argu :
                    display_toggle - turn on display toggle or not (1 / 0)
                    upload_type - upload type (file / image)
                    file_name - uploaded file name
                    file_type - uploaded file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        display_toggle = arg["display_toggle"]
        upload_type = arg["upload_type"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        if int(display_toggle):
            ##Turn on the toggle if initially toggle off
            xpath = Util.GetXpath({"locate":"toggle_off"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to toggle on animated top visual", "result": "1"})
                time.sleep(1)

            ##Upload image
            if upload_type and file_name and file_type:
                locate = Util.GetXpath({"locate":"gif_image_input"})
                BaseUICore.UploadFileWithCSS({"locate": locate, "action": upload_type, "file_name": file_name, "file_type": file_type, "result": "1"})
        else:
            ##Turn off the toggle if initially toggle on
            xpath = Util.GetXpath({"locate":"toggle_on"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to toggle off animated top visual", "result": "1"})
                time.sleep(1)

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyAnimatedTopVisual')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyComponentSeeMoreType(arg):
        '''
        ModifyComponentSeeMoreType : Modify component "see more" type (text / gif)
                Input argu :
                    see_more_type - see more type (text / gif)
                    static_upload_type - static image upload type (file / image)
                    static_image_name - static image name of gif see more type
                    static_image_type - static image type of gif see more type
                    gif_upload_type - gif image upload type (file / image)
                    gif_image_name - gif image name
                    gif_image_type - gif image type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        see_more_type = arg["see_more_type"]
        static_upload_type = arg["static_upload_type"]
        static_image_name = arg["static_image_name"]
        static_image_type = arg["static_image_type"]
        gif_upload_type = arg["gif_upload_type"]
        gif_image_name = arg["gif_image_name"]
        gif_image_type = arg["gif_image_type"]

        if see_more_type == "gif":
            ##Check the gif checkbox initially
            xpath = Util.GetXpath({"locate":"gif_checkbox"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click check gif checkbox", "result": "1"})
            time.sleep(3)

            ##Upload static image
            if static_upload_type and static_image_name and static_image_type:
                locate = Util.GetXpath({"locate":"static_image_input"})
                BaseUICore.UploadFileWithCSS({"locate": locate, "action": static_upload_type, "file_name": static_image_name, "file_type": static_image_type, "result": "1"})

            ##Upload static image
            if gif_upload_type and gif_image_name and gif_image_type:
                locate = Util.GetXpath({"locate":"gif_image_input"})
                BaseUICore.UploadFileWithCSS({"locate": locate, "action": gif_upload_type, "file_name": gif_image_name, "file_type": gif_image_type, "result": "1"})
        elif see_more_type == "text":
            ##Check the text checkbox initially
            xpath = Util.GetXpath({"locate":"text_checkbox"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click check text checkbox", "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignModulePage.ModifyComponentSeeMoreType')


class AdminCampaignModulePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewComponent(arg):
        '''
        ClickAddNewComponent : Click add new component
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new component
        xpath = Util.GetXpath({"locate":"add_new_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add new component button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePageButton.ClickAddNewComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save component
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save component
        xpath = Util.GetXpath({"locate":"save_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditComponent(arg):
        '''
        ClickEditComponent : Click edit component
                Input argu :
                    type - component type
                    component_name - component name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        component_name = arg["component_name"]

        ##Click edit component
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace("name_to_be_replaced", component_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit component", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePageButton.ClickEditComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDuplicateComponent(arg):
        '''
        ClickDuplicateComponent : Click duplicate component
                Input argu :
                    type - component type
                    component_name - component name
                    post_action - continue / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        component_name = arg["component_name"]
        post_action = arg["post_action"]

        ##Click duplicate component
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace("name_to_be_replaced", component_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click duplicate component", "result": "1"})

        ##Click choose post action (continue / cancel)
        xpath = Util.GetXpath({"locate": post_action})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose post action -> " + post_action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePageButton.ClickDuplicateComponent')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToCampaignModulePage(arg):
        '''
        ClickBackToCampaignModulePage : Click back to campaign module page
                Input argu :
                    post_action - cancel / leave (if no input, then no post action)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        post_action = arg["post_action"]

        ##Click back to campaign module page
        xpath = Util.GetXpath({"locate": "back_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click back to campaign module page button", "result": "1"})

        ##Click choose post action (cancel / leave)
        if post_action:
            xpath = Util.GetXpath({"locate": post_action})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose post action -> " + post_action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePageButton.ClickBackToCampaignModulePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteComponent(arg):
        '''
        ClickDeleteComponent : Click delete component
                Input argu :
                    type - component type
                    component_name - component name
                    post_action - confirm / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        component_name = arg["component_name"]
        post_action = arg["post_action"]

        ##Click delete component
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace("name_to_be_replaced", component_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click delete component", "result": "1"})

            ##Click choose post action (confirm / cancel)
            xpath = Util.GetXpath({"locate": post_action})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click choose post action -> " + post_action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignModulePageButton.ClickDeleteComponent')
