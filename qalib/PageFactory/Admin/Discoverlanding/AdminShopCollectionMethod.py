#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminShopCollectionMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import re
import time

##Import framework common library
import Util
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminShopCollectionCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPageNumber(arg):
        '''
        GoToPageNumber : Go to page number
                Input argu :
                    page_number - last_page / page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_number = arg["page_number"]

        ##Go to last page
        if page_number == "last_page":

            ##Get items per page
            xpath = Util.GetXpath({"locate": "items_per_page"})
            BaseUILogic.GetAndReserveElements({"reservetype": "items_per_page", "method": "xpath", "locate": xpath, "result": "1"})
            items_per_page = int(GlobalAdapter.ShopCollectionE2EVar._ItemsPerPage_)
            dumplogger.info(items_per_page)

            ##Count last page number
            if (int(GlobalAdapter.ShopCollectionE2EVar._RecordsCount_[0]) % items_per_page) > 0:
                last_page_number = (int(GlobalAdapter.ShopCollectionE2EVar._RecordsCount_[0]) / items_per_page) + 1
            else:
                last_page_number = (int(GlobalAdapter.ShopCollectionE2EVar._RecordsCount_[0]) / items_per_page) + 0

            ##Input page number
            xpath = Util.GetXpath({"locate": "input_bar"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": str(last_page_number), "result": "1"})
            time.sleep(1)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(3)

        else:
            ##Input page number
            xpath = Util.GetXpath({"locate": "input_bar"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_number, "result": "1"})
            time.sleep(1)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopCollectionCommon.GoToPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRowCount(arg):
        '''
        CheckRowCount : Check row count of table
                Input argu :
                    row_element - rows in table
                    items_per_page - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        row_element = arg['row_element']
        items_per_page = arg["items_per_page"]

        ##Count number of data row
        xpath = Util.GetXpath({"locate": row_element})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})
            number = GlobalAdapter.GeneralE2EVar._Count_
            dumplogger.info("Selected items per page : %s, rows found on page : %s" % (items_per_page, number))

            ##Check if row number match selected items per page
            if str(number) == items_per_page:
                ret = 1
            else:
                ret = 0
        else:
            ret = 0
            dumplogger.info("No row found on page %s" % (row_element))

        OK(ret, int(arg['result']), 'AdminShopCollectionCommon.CheckRowCount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectNumberPerPage(arg):
        '''
        SelectNumberPerPage : Select number per page
                Input argu :
                    type - shop_collection_list_page / edit_shop_collection / operation_history / item_source
                    number_per_page - 10 / 20 / 50 / 100
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        number_per_page = arg["number_per_page"]

        ##Click number per page drop-down list
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click number per page drop-down list", "result": "1"})
        time.sleep(1)

        ##Click number
        xpath = Util.GetXpath({"locate": "number_per_page"})
        xpath = xpath.replace("page_to_be_replaced", number_per_page)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click number", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminShopCollectionCommon.SelectNumberPerPage')


class AdminShopCollectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click on confirm button
                Input argu :
                    type - leave / yes / submit / ok / delete / redirect
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click on confirm button
        xpath = Util.GetXpath({"locate": "confirm_button"})
        xpath = xpath.replace("string_to_be_replaced", type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on " + type + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click on cancel button
                Input argu :
                    type - cancel / no / close
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click on cancel button
        xpath = Util.GetXpath({"locate": "cancel_button"})
        xpath = xpath.replace("string_to_be_replaced", type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickReset(arg):
        '''
        ClickReset : Click on reset button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on cancel button
        xpath = Util.GetXpath({"locate": "reset_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterBoxSearch(arg):
        '''
        ClickFilterBoxSearch : Click search button on filter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on search button
        xpath = Util.GetXpath({"locate": "search_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickFilterBoxSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterBoxReset(arg):
        '''
        ClickFilterBoxReset : Click reset button on filter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on reset button
        xpath = Util.GetXpath({"locate": "reset_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on reset button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickFilterBoxReset')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageNumber(arg):
        '''
        ClickPageNumber : Click page number
                Input argu :
                    type - shop_collection_list_page / edit_shop_collection / operation_history / item_source
                    page_number - page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        page_number = arg["page_number"]

        ##Click page number
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace("number_to_be_replaced", page_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page number", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickPageNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextPage(arg):
        '''
        ClickNextPage : Click go to next page
                Input argu :
                    type - shop_collection_list_page / edit_shop_collection / operation_history / item_source
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click go to next page button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to next page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickNextPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPrevPage(arg):
        '''
        ClickPrevPage : Click go to previous page
                Input argu :
                    type - shop_collection_list_page / edit_shop_collection / operation_history / item_source
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click go to previous page button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click go to previous page button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickPrevPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCollection(arg):
        '''
        ClickEditCollection : Click edit button
                Input argu :
                    collection_id - edit which shop collection id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickEditCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterIcon(arg):
        '''
        ClickFilterIcon : Click on filter icon
                Input argu :
                    filter_type - shop_collection_id / shop_collection_name / image_status / last_operator
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        filter_type = arg["filter_type"]

        ##Click on filter icon
        xpath = Util.GetXpath({"locate": filter_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickFilterIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToShopCollection(arg):
        '''
        ClickBackToShopCollection : Click back to shop collection button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to shop collection button
        xpath = Util.GetXpath({"locate": "back_to_shop_collection_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to shop collection button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickBackToShopCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click on save button
                Input argu :
                    type - general_save_button / edit_collection_save_button / mass_inline_edit_save_button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click on save button
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteBannerImage(arg):
        '''
        DeleteBannerImage : delete banner image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click trash icon
        xpath = Util.GetXpath({"locate": "trash_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionButton.DeleteBannerImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassEdit(arg):
        '''
        ClickMassEdit : Click on mass edit button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on search button
        xpath = Util.GetXpath({"locate": "mass_edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on mass edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickMassEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplate(arg):
        '''
        ClickDownloadTemplate : Click download template button on mass edit popup window
                Input argu :
                    type - mass_edit_add_new / mass_edit_overwrite / mass_edit_update / mass_remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click download template
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download template", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickDownloadTemplate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteUploadFile(arg):
        '''
        DeleteUploadFile : Click trash icon to delete the upload file
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on delete button
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on trash icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.DeleteUploadFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddMoreShop(arg):
        '''
        ClickAddMoreShop : Click add more shop button on mass edit popup window
                Input argu :
                    click_times - number of times add more button is clicked
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_times = arg["click_times"]

        ##Click on add more button multiple times with shop id
        if click_times:

            for times in range(int(click_times)):
                ##Set shop id
                AdminShopCollectionEditPage.SetManuallyAddShopsAttributes({"shop_number": times + 1, "shop_id": times + 1, "shop_image_hash": "", "rectangle_logo_image_hash": "", "circle_logo_image_hash": "", "shop_text": "", "result": "1"})

                ##Click on add more button
                xpath = Util.GetXpath({"locate": "add_more_shop_button"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add more shop button", "result": "1"})

        ##Click on add more button one times without shop id
        else:
            ##Click on add more button
            xpath = Util.GetXpath({"locate": "add_more_shop_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add more shop button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickAddMoreShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteShop(arg):
        '''
        DeleteShop : Click delete button on mass edit popup window to delete shop
                Input argu :
                    shop_number - shop number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_number = arg["shop_number"]

        ##Click on search button
        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("string_to_be_replaced", shop_number)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.DeleteShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteShopCollection(arg):
        '''
        DeleteShopCollection : Click delete button to delete shop collection
                Input argu :
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Click on delete button
        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("string_to_be_replaced", shop_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on delete button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.DeleteShopCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetColumnDropdown(arg):
        '''
        ClickSetColumnDropdown : Click set column dropdown
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set column dropdown
        xpath = Util.GetXpath({"locate": "set_column_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click set column dropdown", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickSetColumnDropdown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangePosition(arg):
        '''
        ClickChangePosition : Click change position button
                Input argu :
                    shop_id - which shop id to change position
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Click on change position button
        xpath = Util.GetXpath({"locate": "change_position_button"})
        xpath = xpath.replace("string_to_be_replaced", shop_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on change position button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickChangePosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCounter(arg):
        '''
        ClickCounter : Click counter to increase/decrease position number
                Input argu :
                    action - up / down
                    click_times - number to click
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        click_times = arg["click_times"]

        ##Click on counter anticon button
        for time in range(int(click_times)):
            xpath = Util.GetXpath({"locate": action})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on counter anticon " + action + " button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickCounter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClose(arg):
        '''
        ClickClose : Click close button on change position panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on close button
        xpath = Util.GetXpath({"locate": "close_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on close button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickClose')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button
                Input argu :
                    shop_id - which shop id to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Click on edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace("string_to_be_replaced", shop_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteEditShopCollectionImage(arg):
        '''
        DeleteEditShopCollectionImage : delete image in edit shop collection section
                Input argu :
                    type - shop_image / circle_logo_image / rectangle_logo_image
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        if type == "rectangle_logo_image":
            ##Move to element
            xpath = Util.GetXpath({"locate": "rectangle_logo_next"})
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        ##Click trash icon
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionButton.DeleteEditShopCollectionImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCollectionColumnIcon(arg):
        '''
        ClickEditCollectionColumnIcon : Click on column icon
                Input argu :
                    icon_type - shop_id / shop_name / shop_image_status / circle_image_status / rectangle_image_status / shop_text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        ##Click on column icon
        xpath = Util.GetXpath({"locate": icon_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on column icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickEditCollectionColumnIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassInlineEdit(arg):
        '''
        ClickMassInlineEdit : Click mass inline edit button on edit shop collection section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on mass inline edit button
        xpath = Util.GetXpath({"locate": "mass_inline_edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on mass inline edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickMassInlineEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickManuallyUpload(arg):
        '''
        ClickManuallyUpload : Click manually upload on item source section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on manually upload button
        xpath = Util.GetXpath({"locate": "manually_upload_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on manually upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickManuallyUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPreview(arg):
        '''
        ClickPreview : Click preview button on item source section
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on preview button
        xpath = Util.GetXpath({"locate": "preview_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on preview button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickPreview')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApplyToAll(arg):
        '''
        ClickApplyToAll : Click apply to all button in shop text popup panel
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on apply to all button
        xpath = Util.GetXpath({"locate": "aplly_to_all_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on apply to all button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionButton.ClickApplyToAll')


class AdminShopCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShopCollectionIdSorted(arg):
        '''
        CheckShopCollectionIdSorted : Check if shop collection id are sorted in descending order
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id_list = []

        ##Get number of page
        xpath = Util.GetXpath({"locate": "last_page"})
        BaseUILogic.GetAndReserveElements({"reservetype": "count", "method": "xpath", "locate": xpath, "result": "1"})
        page_count = int(GlobalAdapter.GeneralE2EVar._Count_)

        ##Get collection id in each row
        for page_num in range(page_count - 1):
            dumplogger.info("current page : %d" % (page_num))

            xpath = Util.GetXpath({"locate": "row_element"})
            BaseUICore.GetElements({"method": "xpath", "locate": xpath, "mode": "multi", "result": "1"})

            for collection_id in GlobalAdapter.CommonVar._PageElements_:
                collection_id_list.append(collection_id.text)

            xpath = Util.GetXpath({"locate": "next_page_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next page button", "result": "1"})
            time.sleep(5)

        ##Compare each row
        for row_num in range(len(collection_id_list) - 1):
            if int(collection_id_list[row_num]) >= int(collection_id_list[row_num + 1]):
                ret = 1
            else:
                ret = 0
                break

        OK(ret, int(arg['result']), 'AdminShopCollectionPage.CheckShopCollectionIdSorted')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchBox(arg):
        '''
        InputSearchBox : Input search box
                Input argu :
                    search_type - shop_collection_id / shop_collection_name / image_status / last_operator
                    value - value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        value = arg["value"]

        if search_type == "shop_collection_id":

            ##Clear input field
            xpath = Util.GetXpath({"locate": search_type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            if value:
                ##Input value
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

            else:
                ##Input global collection id
                dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._CollectionID_)
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.ShopCollectionE2EVar._CollectionID_[0], "result": "1"})

        elif search_type in ("shop_collection_name", "last_operator"):

            ##Input value
            xpath = Util.GetXpath({"locate": search_type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        elif search_type == "image_status":

            ##Click dropdown list
            xpath = Util.GetXpath({"locate": search_type})
            xpath = xpath.replace("string_to_be_replaced", value)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on dropdown list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionPage.InputSearchBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetIdOfCollection(arg):
        '''
        GetIdOfCollection : Get collection id and store in global
                Input argu :
                    locate_element - element xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        locate_element = arg['locate_element']

        ##Reset collection id list
        GlobalAdapter.ShopCollectionE2EVar._CollectionID_ = []

        ##Get elements and store in global
        xpath = Util.GetXpath({"locate": locate_element})
        BaseUILogic.GetAndReserveElements({"reservetype": "new_shop_collection_id", "method": "xpath", "locate": xpath, "result": "1"})
        dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._CollectionID_)

        OK(ret, int(arg['result']), 'AdminShopCollectionPage.GetIdOfCollection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopCollectionName(arg):
        '''
        ClickShopCollectionName : Click shop collection name on shop collection list page
                Input argu :
                    shop_collection_name - shop collection name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_collection_name = arg['shop_collection_name']

        ##Click on shop collection name text
        xpath = Util.GetXpath({"locate": "shop_collection_name"})
        xpath = xpath.replace("string_to_be_replaced", shop_collection_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on shop collection name text", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionPage.ClickShopCollectionName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckTotalCount(arg):
        '''
        CheckTotalCount : Check whether the total records found equal to the number of collections
                Input argu :
                    items_per_page - number of items per page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        items_per_page = arg['items_per_page']

        ##get page count
        xpath = Util.GetXpath({"locate": "last_page_btn"})
        BaseUILogic.GetAndReserveElements({"reservetype": "number_of_page", "method": "xpath", "locate": xpath, "result": "1"})
        page_count = GlobalAdapter.ShopCollectionE2EVar._NumberOfPage_

        dumplogger.info("Page Count : %s" % (page_count))

        ##Go to last page
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click last page button", "result": "1"})

        ##Get the number of collection on last page
        xpath = Util.GetXpath({"locate": "row_element"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"reservetype": "count", "method": "xpath", "locate": xpath, "result": "1"})
            last_page_row_count = GlobalAdapter.GeneralE2EVar._Count_
            dumplogger.info("Last page records : " + str(last_page_row_count))

            ##Get number of records found
            xpath = Util.GetXpath({"locate": "records_found"})
            BaseUILogic.GetAndReserveElements({"reservetype": "records_count", "method": "xpath", "locate": xpath, "result": "1"})
            records_found = GlobalAdapter.ShopCollectionE2EVar._RecordsCount_[-1]

            ##Check whether the total records found equal to the number of collections
            if int(records_found) == (int(page_count) - 1) * int(items_per_page) + int(last_page_row_count):
                ##Back to first page
                AdminShopCollectionButton.ClickPageNumber({"type": "shop_collection_list_page", "page_number": "1", "result": "1"})
                time.sleep(3)

            else:
                ret = 0
                dumplogger.info("Total records found is not equal to the number of collections")

        else:
            ret = 0
            dumplogger.info("No row found on last page")

        OK(ret, int(arg['result']), 'AdminShopCollectionPage.CheckTotalCount')


class AdminShopCollectionEditPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetShopCollectionAttributes(arg):
        '''
        SetShopCollectionAttributes : Set shop collection attributes
                Input argu :
                    collection_name - collection name
                    feature_product - 1 for enable, 0 for disable
                    random - 1 for random collection name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_name = arg["collection_name"]
        feature_product = arg["feature_product"]
        random = arg['random']

        ##Use random string as collection name
        if random:
            collection_name = XtFunc.GenerateRandomString(20, "characters")

            ##Store input product name to global
            if len(GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_) > 0:
                GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_ = []

            GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_.append(collection_name)
            dumplogger.info(GlobalAdapter.ShopCollectionE2EVar._NewCollectionName_)

        ##Input collection name
        if collection_name:
            xpath = Util.GetXpath({"locate": "collection_name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": collection_name, "message": "Input collection name", "result": "1"})

        ##Click feature product toggle
        if feature_product:
            xpath = Util.GetXpath({"locate": "feature_product"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click feature product toggle", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.SetShopCollectionAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetBannerImage(arg):
        '''
        SetBannerImage : Set banner image
                Input argu :
                    upload_type - upload image by file or image hash
                    icon_image_file - icon image file
                    icon_image_type - icon image type
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        icon_image_file = arg["icon_image_file"]
        icon_image_type = arg["icon_image_type"]
        image_hash = arg["image_hash"]

        ##Upload icon image
        if upload_type in ("file", "image"):
            xpath = Util.GetXpath({"locate": "image_upload_button"})
            BaseUICore.UploadFileWithCSS({"locate": xpath, "action": upload_type, "file_name": icon_image_file, "file_type": icon_image_type, "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":
            xpath = Util.GetXpath({"locate": "input_hash"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            if image_hash:
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
                dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.SetBannerImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckShopCollectionAttributes(arg):
        '''
        CheckShopCollectionAttributes : Check collection attribute
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check header
        xpath = Util.GetXpath({"locate": "header_title"})
        BaseUILogic.GetAndReserveElements({"reservetype": "new_shop_collection_id", "method": "xpath", "locate": xpath, "result": "1"})
        BaseUILogic.GetDiffInReserveElements({"compareType": "new_shop_collection_id", "result": "1"})

        ##Check collection name
        xpath = Util.GetXpath({"locate": "collection_name"})
        BaseUILogic.GetAndReserveElements({"reservetype": "edit_shop_collection_name", "method": "xpath", "locate": xpath, "result": "1"})
        BaseUILogic.GetDiffInReserveElements({"compareType": "new_shop_collection_name", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.CheckShopCollectionAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelecetMassEditType(arg):
        '''
        SelecetMassEditType : Select mass edit type in mass edit popup window
                Input argu :
                    mass_edit_type - mass_edit_shops / mass_remove_shops / manual_add_shops / manual_remove_shops
                    mass_edit_shops_type - add_new / overwrite / update
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mass_edit_type = arg['mass_edit_type']
        mass_edit_shops_type = arg['mass_edit_shops_type']

        ##Select mass edit type
        xpath = Util.GetXpath({"locate": mass_edit_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select mass edit type", "result": "1"})

        if mass_edit_type == "mass_edit_shops":
            xpath = Util.GetXpath({"locate": mass_edit_shops_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select mass edit shop type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.SelecetMassEditType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchUploadCSV(arg):
        '''
        BatchUploadCSV : Upload file csv file
                Input argu :
                    upload_type - add_new / overwrite / update / mass_remove_shops / item_source_manually_upload
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg["upload_type"]
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload csv with collections
        xpath = Util.GetXpath({"locate": upload_type})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.BatchUploadCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetManuallyAddShopsAttributes(arg):
        '''
        SetManuallyAddShopsAttributes : Set manually add shops attributes on mass edit popup window
                Input argu :
                    shop_number - shop number
                    shop_id - shop id
                    shop_image_hash - shop image hash
                    rectangle_logo_image_hash - rectangle logo image hash
                    circle_logo_image_hash - circle logo image hash
                    shop_text - shop text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_number = int(arg["shop_number"])
        shop_id = arg["shop_id"]
        shop_image_hash = arg["shop_image_hash"]
        rectangle_logo_image_hash = arg["rectangle_logo_image_hash"]
        circle_logo_image_hash = arg["circle_logo_image_hash"]
        shop_text = arg["shop_text"]

        shop_number = shop_number - 1

        ##Input shop id
        if shop_id:
            xpath = Util.GetXpath({"locate": "shop_id"})
            xpath = xpath.replace("number_to_be_replaced", str(shop_number))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "Input shop id", "result": "1"})

        ##Input shop image hash
        if shop_image_hash:
            xpath = Util.GetXpath({"locate": "shop_image_hash"})
            xpath = xpath.replace("number_to_be_replaced", str(shop_number))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_image_hash, "message": "Input shop image hash", "result": "1"})

        ##Input rectangle logo image hash
        if rectangle_logo_image_hash:
            xpath = Util.GetXpath({"locate": "rectangle_logo_image_hash"})
            xpath = xpath.replace("number_to_be_replaced", str(shop_number))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rectangle_logo_image_hash, "message": "Input rectangle logo image hash", "result": "1"})

        ##Input circle logo image hash
        if circle_logo_image_hash:
            xpath = Util.GetXpath({"locate": "circle_logo_image_hash"})
            xpath = xpath.replace("number_to_be_replaced", str(shop_number))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": circle_logo_image_hash, "message": "Input circle logo image hash", "result": "1"})

        ##Input shop text
        if shop_text:
            xpath = Util.GetXpath({"locate": "shop_text"})
            xpath = xpath.replace("number_to_be_replaced", str(shop_number))
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_text, "message": "Input shop text", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.SetManuallyAddShopsAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetManuallyRemoveShopsAttributes(arg):
        '''
        SetManuallyRemoveShopsAttributes : Set manually remove shops attributes on mass edit popup window
                Input argu :
                    shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shop_id"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "message": "Input shop id", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.SetManuallyRemoveShopsAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAllColumn(arg):
        '''
        SelectAllColumn : Select all column in set column
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select all column
        xpath = Util.GetXpath({"locate": "unchecked_column"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "count", "result": "1"})

            ##Select all detected unchecked column
            for select_time in range(int(GlobalAdapter.GeneralE2EVar._Count_)):
                xpath = Util.GetXpath({"locate": "unchecked_column"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select column" + str(select_time + 1), "result": "1"})

        else:
            dumplogger.info("No unchecked column exists !!!")

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.SelectAllColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputChangePosition(arg):
        '''
        InputChangePosition : Input position number to change
                Input argu :
                    number - position number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input position number
        xpath = Util.GetXpath({"locate": "position_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.InputChangePosition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditShopCollectionAttributes(arg):
        '''
        EditShopCollectionAttributes : Edit shop collection attributes
                Input argu :
                    shop_id - shop id
                    shop_text - shop text
                    redirection_url - redirection url
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        shop_text = arg["shop_text"]
        redirection_url = arg["redirection_url"]

        ##Input shop text
        if shop_text:
            xpath = Util.GetXpath({"locate": "shop_text"})
            xpath = xpath.replace("string_to_be_replaced", shop_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_text, "message": "Input shop text", "result": "1"})

        ##Input redirection url
        if redirection_url:
            xpath = Util.GetXpath({"locate": "redirection_url"})
            xpath = xpath.replace("string_to_be_replaced", shop_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": redirection_url, "message": "Input redirection url", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.EditShopCollectionAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditShopCollectionImage(arg):
        '''
        EditShopCollectionImage : Edit shop collection image
                Input argu :
                    shop_id - which shop to upload image
                    upload_type - upload image by file or image hash
                    upload_image_type - shop_image / shop_hash / circle_logo_image / circle_logo_hash / rectangle_logo_image / rectangle_logo_hash
                    icon_image_file - icon image file
                    icon_image_type - icon image type
                    image_hash - image hash
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]
        upload_type = arg["upload_type"]
        upload_image_type = arg["upload_image_type"]
        icon_image_file = arg["icon_image_file"]
        icon_image_type = arg["icon_image_type"]
        image_hash = arg["image_hash"]

        ##Upload icon image
        if upload_type == "upload_file":

            if upload_image_type == "rectangle_logo_image":
                ##Move to element
                xpath = Util.GetXpath({"locate": "rectangle_logo_next"})
                BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

            xpath = Util.GetXpath({"locate": upload_image_type})
            xpath = xpath.replace("string_to_be_replaced", shop_id)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "click upload button", "result": "1"})
            time.sleep(3)
            XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": icon_image_file, "file_type": icon_image_type, "result": "1"})
            dumplogger.info("Success upload image.")

        ##Input image hash
        elif upload_type == "image_hash":

            if upload_image_type == "rectangle_logo_hash":
                ##Move to element
                xpath = Util.GetXpath({"locate": "rectangle_logo_next"})
                BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

            xpath = Util.GetXpath({"locate": upload_image_type})
            xpath = xpath.replace("string_to_be_replaced", shop_id)
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_hash, "result": "1"})
            dumplogger.info("Success input image hash.")

        OK(ret, int(arg["result"]), 'AdminShopCollectionEditPage.EditShopCollectionImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchBox(arg):
        '''
        InputSearchBox : Input search box
                Input argu :
                    input_type - shop_id / shop_name / image_status / shop_text
                    value - value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_type = arg["input_type"]
        value = arg["value"]

        if input_type in ("shop_id", "shop_name", "shop_text"):

            ##Clear input field
            xpath = Util.GetXpath({"locate": input_type})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            ##Input value
            if value:
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        elif input_type == "image_status":

            ##Click dropdown list
            xpath = Util.GetXpath({"locate": input_type})
            xpath = xpath.replace("string_to_be_replaced", value)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on dropdown list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.InputSearchBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HoverOnCounter(arg):
        '''
        HoverOnCounter : Hover mouse on counter
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Hover mouse on counter
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.HoverOnCounter')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPageNumber(arg):
        '''
        GoToPageNumber : Go to page number
                Input argu :
                    type - edit_shop_collection / operation_history / item_source
                    page_number - last_page / page number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        page_number = arg["page_number"]

        ##Input page number
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_number, "result": "1"})
        time.sleep(1)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopCollectionEditPage.GoToPageNumber')


class AdminAutoGenerateRulePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAutoGenerateRuleAttributes(arg):
        '''
        SetAutoGenerateRuleAttributes : Set auto generate rule page attributes
                Input argu :
                    name - name
                    description - description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        description = arg["description"]

        ##Input name
        if name:
            xpath = Util.GetXpath({"locate": "name"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "message": "Input name", "result": "1"})

        ##Input description
        if description:
            xpath = Util.GetXpath({"locate": "description"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "message": "Input description", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminAutoGenerateRulePage.SetAutoGenerateRuleAttributes')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectItemSourceType(arg):
        '''
        SelectItemSourceType : Select item source type
                Input argu :
                    type - shop_list_csv / my_brand_membership_shop_list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Select item source type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select item source type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.SelectItemSourceType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadShopListCSV(arg):
        '''
        UploadShopListCSV : Upload shop list csv file
                Input argu :
                    action - file / image
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg["action"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload shop list csv file
        xpath = Util.GetXpath({"locate": "upload_button"})
        BaseUICore.UploadFileWithCSS({"locate": xpath, "action": action, "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.UploadShopListCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToGenerateShopCollectionPageByRuleID(arg):
        '''
        GoToGenerateShopCollectionPageByRuleID : Go to generate shop collection page by rule id
                Input argu :
                    rule_id - rule id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_id = arg["rule_id"]

        ##Get rule_id from Global variable
        if GlobalAdapter.ShopCollectionE2EVar._RuleID_ and not rule_id:
            rule_id = GlobalAdapter.ShopCollectionE2EVar._RuleID_

        url = "https://dl-admin." + GlobalAdapter.UrlVar._Domain_ + "/item-rule-generation?rule_id=" + rule_id + "&collection_type=shop"
        BaseUICore.GotoURL({"url": url, "result": "1"})
        BaseUICore.PageHasLoaded({"result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.GoToGenerateShopCollectionPageByRuleID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputMaxNumberOfShop(arg):
        '''
        InputMaxNumberOfShop : Input max number of shop in the collection
                Input argu :
                    number - max number of shop
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg["number"]

        ##Input number
        xpath = Util.GetXpath({"locate": "max_number_of_shop"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": number, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "tab", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.InputMaxNumberOfShop')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectGeneratingSettingType(arg):
        '''
        SelectGeneratingSettingType : Select generating setting type
                Input argu :
                    type - into_one_collection / by_shops_name_letter / one_off_generating / auto_updating
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Select generating setting type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select generating setting type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.SelectGeneratingSettingType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCronjobsEndTime(arg):
        '''
        InputCronjobsEndTime : Input cronjobs end time
                Input argu :
                    end_time - input end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = arg["end_time"]

        ##Already select time in End time field
        xpath = Util.GetXpath({"locate": "delete_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click delete button to clear end time field
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        if end_time:
            ##Select current date time
            if end_time == "now":
                end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, 0, 0)
            else:
                end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(end_time), 0)

            ##Click select time
            xpath = Util.GetXpath({"locate": "select_time"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select time", "result": "1"})

            ##Input end time
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

            ##Click ok button
            xpath = Util.GetXpath({"locate": "ok_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.InputCronjobsEndTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectUpdateFrequency(arg):
        '''
        SelectUpdateFrequency : Select update frequency
                Input argu :
                    type - daily / half_daily / hourly
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Select update frequency
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select update frequency", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.SelectUpdateFrequency')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGeneratedCollectionID(arg):
        '''
        ClickGeneratedCollectionID : Click generated collection id on generating tasks table
                Input argu :
                    collection_id - collection id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_id = arg['collection_id']

        ##Click on generated collection id
        xpath = Util.GetXpath({"locate": "collection_id"})
        xpath = xpath.replace("id_to_be_replaced", collection_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on generated collection id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.ClickGeneratedCollectionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyFilterInputTime(arg):
        '''
        ModifyFilterInputTime : Modify filter input time
                Input argu :
                    start_time - start time or now
                    end_time - end time or now
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Already select time in time field
        xpath = Util.GetXpath({"locate": "delete_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click delete button to clear time field
            BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "1", "result": "1"})

        ##Select current date time as start time
        if start_time == "now":
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, 0, 0)

        ##Select current date time as end time
        if end_time == "now":
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, 0, 0)

        ##Click start time
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time", "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Click end time
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time", "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        ##Click ok button
        xpath = Util.GetXpath({"locate": "ok_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.ModifyFilterInputTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSearchBox(arg):
        '''
        InputSearchBox : Input search box
                Input argu :
                    search_type - rule_id / rule_name / last_operator
                    value - value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg["search_type"]
        value = arg["value"]

        ##Input value
        xpath = Util.GetXpath({"locate": search_type})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePage.InputSearchBox')


class AdminAutoGenerateRulePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerateShopCollections(arg):
        '''
        ClickGenerateShopCollections : Click on generate shop collection button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on generate shop collection button
        xpath = Util.GetXpath({"locate": "generate_shop_collection_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on generate shop collection button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickGenerateShopCollections')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGeneratingTaskTableIcon(arg):
        '''
        ClickGeneratingTaskTableIcon : Click icon on generating task table
                Input argu :
                    icon_type - create_time / update_time / last_operator
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        ##Click on icon
        xpath = Util.GetXpath({"locate": icon_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on generating task table" + icon_type + "icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickGeneratingTaskTableIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRegenerate(arg):
        '''
        ClickRegenerate : Click regenerate action button
                Input argu :
                    task_id - task id to regenerate
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        task_id = arg["task_id"]

        ##Click on regenerate button
        xpath = Util.GetXpath({"locate": "regenerate_btn"})
        xpath = xpath.replace("id_to_be_replaced", task_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on regenerate action button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickRegenerate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExtendCronjob(arg):
        '''
        ClickExtendCronjob : Click extend cronjob to 3M button
                Input argu :
                    task_id - task id to extend cronjob
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        task_id = arg["task_id"]

        ##Click on extend cronjob button
        xpath = Util.GetXpath({"locate": "extend_cronjob_btn"})
        xpath = xpath.replace("id_to_be_replaced", task_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on extend cronjob to 3M button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickExtendCronjob')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFilterIcon(arg):
        '''
        ClickFilterIcon : Click on filter icon
                Input argu :
                    filter_type - rule_id / rule_name / update_time / create_time / last_operator
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        filter_type = arg["filter_type"]

        ##Click on filter icon
        xpath = Util.GetXpath({"locate": filter_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on filter icon", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickFilterIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerate(arg):
        '''
        ClickGenerate : Click generate button on Auto Generate Collection Rule List
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on generate button
        xpath = Util.GetXpath({"locate": "generate_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on generate button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickGenerate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickView(arg):
        '''
        ClickView : Click view button on Auto Generate Collection Rule List
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on view button
        xpath = Util.GetXpath({"locate": "view_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on view button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickView')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBackToRuleList(arg):
        '''
        ClickBackToRuleList : Click back to rule list button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back to rule list button
        xpath = Util.GetXpath({"locate": "back_to_rule_list_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back to rule list button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickBackToRuleList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveAndGoToGenerate(arg):
        '''
        ClickSaveAndGoToGenerate : Click on save and go to generate button
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click on save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on save and go to generate button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAutoGenerateRulePageButton.ClickSaveAndGoToGenerate')
