﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidFlashSaleMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time

##Import common library
import FrameWorkBase
from Config import dumplogger
import Util
import DecoratorHelper
import Config

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidFlashSaleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        ''' ClickSeeMore : Click see more in flash sale section
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"fs_see_more"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click see more in flash sale section", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCategory(arg):
        ''' ClickCategory : Click flash sale category
                Input argu :
                        category_name - category name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        category_name = arg["category_name"]

        ##Click flash sale category
        xpath = Util.GetXpath({"locate":"fs_cat"})
        xpath_fs_cat = xpath.replace("cat_name_to_be_replace", category_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath_fs_cat, "message":"Click flash sale category", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemindOK(arg):
        ''' ClickRemindOK : Click flash sale remind ok button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click flash sale remind ok button
        xpath = Util.GetXpath({"locate":"remind_ok_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click flash sale remind ok button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickRemindOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOngoingFlashSale(arg):
        ''' ClickOngoingFlashSale : Click ongoing flash sale
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click ongoing flash sale
        xpath = Util.GetXpath({"locate":"ongoing_flash_sale"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ongoing flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickOngoingFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickComingSoonFlashSale(arg):
        ''' ClickComingSoonFlashSale : Click coming soon flash sale
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click coming soon flash sale
        xpath = Util.GetXpath({"locate":"coming_flash_sale2"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click coming soon flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickComingSoonFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextComingSoonFlashSale(arg):
        ''' ClickNextComingSoonFlashSale : Click next coming soon flash sale
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click next coming soon flash sale
        xpath = Util.GetXpath({"locate":"coming_flash_sale3"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next coming soon flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickNextComingSoonFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShareFlashSale(arg):
        ''' ClickShareFlashSale : Click share flash sale
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click share flash sale
        xpath = Util.GetXpath({"locate":"share_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click share flash sale", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickShareFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemindMe(arg):
        ''' ClickRemindMe : Click remind me
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click remind me
        xpath = Util.GetXpath({"locate":"remind_me"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click remind me flash sale product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelRemindMe(arg):
        ''' ClickCancelRemindMe : Cancel remind me
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Cancel remind me
        xpath = Util.GetXpath({"locate":"cancel_remind"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Cancel remind me flash sale product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickCancelRemindMe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleBuyNow(arg):
        ''' ClickFlashSaleBuyNow : Click flash sale buy now
                Input argu :
                    product - product name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        product = arg["product"]

        ##Click product buy now btn
        xpath = Util.GetXpath({"locate":"buy_now_btn"})
        xpath = xpath.replace("product_name_to_be_replace", product)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click buy now", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickFlashSaleBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMegaSaleCategory(arg):
        ''' ClickMegaSaleCategory : Click mega sale category button
                Input argu :
                    category - category
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        category = arg["category"]

        ##Click mega sale category button
        xpath = Util.GetXpath({"locate":"category_button"})
        xpath = xpath.replace("category_name_to_be_replace", category)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click mega sale category button", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickMegaSaleCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleBuyNow(arg):
        ''' ClickFlashSaleBuyNow : Click flash sale buy now
                Input argu : product - product name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        product = arg["product"]

        ##Click product buy now btn
        xpath = Util.GetXpath({"locate":"buy_now_btn"})
        xpath = xpath.replace("product_name_to_be_replace", product)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click buy now", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickFlashSaleBuyNow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTimeInaccurate(arg):
        ''' ClickTimeInaccurate : Click time inaccurate button
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"popup_time_inaccurate"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            ##Click the pop up message of time inaccurate
            xpath = Util.GetXpath({"locate":"ok_button"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click pop up message ok button for time inaccurate", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleButton.ClickTimeInaccurate')

class AndroidFlashSalePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFirstFlashSaleProduct(arg):
        ''' ClickFirstFlashSaleProduct : Click flash sale product to go to flash sale landing page
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click flash sale product
        xpath = Util.GetXpath({"locate":"fs_product"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Goto flash sale page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSalePage.ClickFirstFlashSaleProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleProductByPrice(arg):
        ''' ClickFlashSaleProductByPrice : Click flash sale product by price to go to flash sale landing page
                Input argu :price - price
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        price = arg["price"]

        ##Click flash sale product
        xpath = Util.GetXpath({"locate":"fs_product"})
        xpath = xpath.replace("price_to_be_replace", price)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Goto flash sale page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSalePage.ClickFlashSaleProductByPrice')

class AndroidFlashSaleLandingPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleTimeBar(arg):
        ''' ClickFlashSaleTimeBar : Click time bar on top of flash sale landing page to choose each flash sale
                Input argu : order - order
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        order = arg["order"]

        ##Click flash sale time bar to switch each flash sale
        xpath = Util.GetXpath({"locate":"flash_sale_time_bar"})
        xpath = xpath.replace("order_to_be_replace", order)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click flash sale time bar", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleLandingPage.ClickFlashSaleTimeBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleProduct(arg):
        ''' ClickFlashSaleProduct : Click flash sale product to go to PDP
                Input argu : product_name - product_name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click flash sale product
        xpath = Util.GetXpath({"locate":"fs_product"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click flash sale product", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleLandingPage.ClickFlashSaleProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFlashSaleCategoryDropList(arg):
        ''' ClickFlashSaleCategoryDropList : Click flash sale category drop list
                Input argu : N/A
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click flash sale product
        xpath = Util.GetXpath({"locate":"drop_list"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click flash sale category drop list", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSaleLandingPage.ClickFlashSaleCategoryDropList')

class AndroidFlashSalePageComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        xpath = Util.GetXpath({"locate": button_type})

        ##Click on button
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFlashSalePageComponent.ClickOnButton')
