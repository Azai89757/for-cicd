import unittest
from Config import dumplogger
from FrameWorkBase import *


class VNAPIMallPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIMallPage Precondition =="
        dumplogger.info("== Setup VNAPIMallPage Precondition ==")

    def test_VNAPIMallPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIMallPage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIMallPage Post condition =="
        dumplogger.info("== Setup VNAPIMallPage Post condition ==")
