#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminDragonPayMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import Util
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper

##Inport Web library
from PageFactory.Web import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchDragonPayAdmin(arg):
    '''
    LaunchDragonPayAdmin : Launch dragon pay admin
        Input argu : N/A
        Return code :
            1 - success
            0 - fail
            -1 - error
    '''
    ret = 1
    url = "https://test.dragonpay.ph/AdminWeb/LoginPage.aspx"

    ##Launch dragon pay admin
    BaseUICore.GotoURL({"url":url, "result":"1"})

    ##Input ID
    xpath = Util.GetXpath({"locate": "id_column"})
    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "SHOPEE", "result":"1"})

    ##Input Password
    xpath = Util.GetXpath({"locate": "password_column"})
    BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "!anu?a3RapuY", "result":"1"})

    ##Click login btn
    xpath = Util.GetXpath({"locate": "login_btn"})
    BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login btn", "result":"1"})

    OK(ret, int(arg["result"]), 'LaunchDragonPayAdmin')


class AdminDragonPayCommon:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NavigateToOTCSimulatePage(arg):
        '''
        NavigateToOTCSimulatePage : Navigate to OTC simulate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click admin tab
        xpath = Util.GetXpath({"locate": "admin"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click admin", "result":"1"})

        ##Click otc non bank simulator hyperlink
        xpath = Util.GetXpath({"locate": "otc_hyper_link"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click admin", "result":"1"})

        OK(ret, int(arg["result"]), "AdminDragonPayCommon.NavigateToOTCSimulatePage")


class AdminDragonPayOTCPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SimulateNonBankPayment(arg):
        '''
        SimulateNonBankPayment : Simulate OTC Non-Bank payment
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input ref no
        xpath = Util.GetXpath({"locate": "refno_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._DragonPayRefNo_, "result":"1"})

        ##Input amount
        xpath = Util.GetXpath({"locate": "amount_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._PayableAmount_, "result":"1"})

        ##Click pay btn
        xpath = Util.GetXpath({"locate": "pay_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click login btn", "result":"1"})

        OK(ret, int(arg["result"]), 'AdminDragonPayOTCPage.SimulateNonBankPayment')
