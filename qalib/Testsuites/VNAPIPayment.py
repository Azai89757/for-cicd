import unittest
from Config import dumplogger
from FrameWorkBase import *


class VNAPIPayment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIPayment Precondition =="
        dumplogger.info("== Setup VNAPIPayment Precondition ==")

    def test_VNAPIPayment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIPayment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIPayment Post condition =="
        dumplogger.info("== Setup VNAPIPayment Post condition ==")
