import os
import Config
import commands
import platform
import telnetlib
import time
import re
import logging
import paramiko
import paramiko_expect
import traceback
import configparser
import subprocess
import sys

##Spex agent ip address
spex_agent_server = "10.129.140.79"

##Setup ip address for ssh accessing
wsl_ipaddress = "127.0.0.1"

##SSH port of wsl sshd_config
wsl_port = 2222

cmd = {
    'linux':{'private_key_path':'cat ~/.ssh/private_key','spex_path':'~/spex','ssh_list':'ssh-add -l','add_key':'ssh-add ~/.ssh/private_key','pri_key':'~/.ssh/private_key','ssh_agent_pid':'ps aux | grep ssh'},
    'darwin':{'private_key_path':'cat /var/root/.ssh/private_key','spex_path':'/var/root/spex','ssh_list':'ssh-add -l','add_key':'ssh-add /var/root/.ssh/private_key','pri_key':'/var/root/.ssh/private_key','ssh_agent_pid':'ps aux | grep ssh'}
    }

def IniParser(ini_file):
    '''IniParser: Parse ini file under /data/common
    Input argu: ini file path
    Return:  A 2-dimention dictionary which contains data in ini.'''

    ##dumplogger.info("Enter IniParser")
    config = configparser.ConfigParser()
    dict_config_setting = {}
    try:
        if os.path.isfile(ini_file):
            ##Read ini file
            config.read(ini_file)

            ##Sequence to load all of keys in
            for sub_key in config.keys():

                ##Skip DEFAULT key in ini
                if sub_key != "DEFAULT":

                    ##Declare two-dimensional dictionary
                    dict_config_setting[sub_key] = dict()
                    ##Sequence to load all of config keys and values
                    for sub_item in config.items(sub_key):
                        ##Set config key and value into sub_key(Because config key and value is tuple)
                        dict_config_setting[sub_key][sub_item[0]] = sub_item[1]

            return dict_config_setting
        else:
            ##dumplogger.info("ini file not exist!!!")
            print "ini file not exist!!!"

    except configparser.NoSectionError:
        print "Please check your ini file path!!!"

    except:
        print "Other ini exception error."

class SSH2SpexAgent(object):

    def __init__(self, logname):

        ##Receive response from ssh connection
        self.resp = ""

        ##ssh shell
        self.shell = None

        ##Setup logger
        output_folder = Config._SystemPath_ + Config._OutputFolderName_ + Config.dict_systemslash[Config._platform_]
        self.logger = set_log( output_folder + logname + ".log", logname)

        ##Setup config_file path
        config_file = "data" + Config.dict_systemslash[Config._platform_] + "common" + Config.dict_systemslash[Config._platform_] + "SpexAPIConfig.ini"
        env_config = IniParser(config_file)

        ##Import username/password of WSL, and shopee user account
        self.wsl_username = env_config['WSL_Config']['account']
        self.wsl_password = env_config['WSL_Config']['password']
        self.user = env_config['WSL_Config']['user']

    def send_cmd(self, command):
        try:
            ##add for nat testing
            process = subprocess.Popen(command,shell = True,stdout = subprocess.PIPE,stdin = subprocess.PIPE,stderr = subprocess.PIPE)
            # Send the data and get the output
            self.resp = process.communicate()
            if self.resp[0]:
                return True
            else:
                self.logger.debug(self.resp)
                return False

        except:
            self.logger.exception("send command fail")
            self.logger.debug(self.resp)
            return False

    def private_key_import(self,platform):
        ##Cat the key file
        result = self.send_cmd(cmd[platform]['private_key_path'])
        if result:
            ##Check the key content
            if result_compare("PRIVATE KEY-----", self.resp[0]):
                self.logger.info("User Key Exist")
                return True
            else:
                self.logger.error("[Import private key] Private Key Non Exist Under ~/.ssh/")
                return False
        else:
            self.logger.error(self.resp)

    def add_key_to_ssh_agent(self,platform):
        ##Check ssh-add content enable service
        if self.send_cmd(cmd[platform]['ssh_list']):
            if result_compare("/root/.ssh/private_key (RSA)", self.resp[0]):
                self.logger.info("SSH Agent Key exist")
                return True
            else:
                ##Add key if key not in ssh-agent
                if not self.send_cmd(cmd[platform]['add_key']):
                    if result_compare("Identity added:", self.resp[1]):
                        self.logger.info("SSH Agent Key Added")
                        return True
                    else:
                        self.logger.error("Cannot execute add key")
                        self.logger.debug(self.resp)
                        return False
                else:
                    self.logger.error("Add key failed")
                    self.logger.debug(self.resp)
                    return False
        else:
            self.logger.error("Cannot execute ssh-add -l")
            self.logger.debug(self.resp)
            return False

    def setup_spex_connection(self,platform):
        ##Init sshclient
        sshclient = paramiko.SSHClient()

        ##Automatically adding the hostname and new host key to the local and saved
        sshclient.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        sshclient.connect(wsl_ipaddress, wsl_port, self.wsl_username, self.wsl_password)

        self.logger.info("[setup_spex_connection] Init Script to Spex")
        ##Create ssh connection with socket mapping to Spex-agent server
        spex_cmd = "ssh -i " + cmd[platform]['pri_key'] + " -p 22 -vvvCNL ~/spex/spex.sock:/run/spex/spex.sock -o StreamLocalBindUnlink=yes " + self.user + "@" + spex_agent_server

        try:
            ##Create a client interaction class which will interact with the host
            interact = paramiko_expect.SSHClientInteraction(sshclient, timeout=60000, display=True, buffer_size=65535, tty_width=100, tty_height=24)
            interact.send(spex_cmd)

            ##Tail the output from SSH client
            interact.tail(output_callback=lambda m: self.logger.debug(m))

        except KeyboardInterrupt:
            self.logger.exception('Ctrl+C interruption detected, stopping tail')
            return False
        except:
            self.logger.exception("[setup_spex_connection] Setup Spex connection fail")
            return False

##Use for setup local to spex agent connection with socket mapping.
def connect2spexagent():
    sshconnect = SSH2SpexAgent("SSH2SpexAgent")

    try:
        ##Check private key exist under /var/root/.ssh
        if sshconnect.private_key_import(Config.CheckOSPlatform()):
            sshconnect.logger.info("Private key exist")

            ##Check private key imported in ssh-agent
            if sshconnect.add_key_to_ssh_agent(Config.CheckOSPlatform()):
                sshconnect.logger.info("Private key imported to ssh-agent")

                ##Init ssh connection to spex agent with socket mapping
                sshconnect.setup_spex_connection(Config.CheckOSPlatform())
            else:
                ##Log message when key import failed
                sshconnect.logger.error("Private Key Imported Failed")
        else:
            ##Log message when key not exist
            sshconnect.logger.error("Private key not exist")
    except:
        ##Log message for exception
        sshconnect.logger.exception("[Connect2spex] Connect fail")

def set_log(filename,loggername):
    logger = logging.getLogger(loggername)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - [%(filename)s:%(lineno)d][%(funcName)s]|%(message)s')
    filehandler = logging.FileHandler(filename)
    filehandler.setLevel(logging.DEBUG)
    filehandler.setFormatter(formatter)
    logger.addHandler(filehandler)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(formatter)
    logger.addHandler(console)
    return logger

def result_compare(pattern, text):
    complied_pattern = re.compile(pattern)
    match_result = complied_pattern.search(text)
    if match_result is None:
        return False
    else:
        return True

if __name__ == '__main__':
    connect2spexagent()
