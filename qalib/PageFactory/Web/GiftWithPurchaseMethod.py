﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 GiftWithPurchaseMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper

##Import Web library
import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class GiftWithPurchasePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseFreeItem(arg):
        '''
        ChooseFreeItem : Choose free item in gift with purchase landing page
                Input argu :
                    index - choose product index for free gift item
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Choose product name for free gift
        xpath = Util.GetXpath({"locate": "product"})
        xpath = xpath.replace('replaced_text', index)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose free gift item", "result": "1"})

        ##click complete btn
        xpath_complete_btn = Util.GetXpath({"locate": "complete_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath_complete_btn, "message": "Click complete btn", "result": "1"})

        OK(ret, int(arg['result']), 'GiftWithPurchasePage.ChooseFreeItem')
