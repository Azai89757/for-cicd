import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCLogOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCLogOut Precondition =="
        dumplogger.info("== Setup THPCLogOut Precondition ==")

    def test_THPCLogOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCLogOut-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCLogOut Post condition =="
        dumplogger.info("== Setup THPCLogOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
