#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 DBOrderMethod.py: The def of this file called by XML mainly.
'''

##Import framework common library
import DecoratorHelper
import FrameWorkBase
import Config
from Config import dumplogger
import GlobalAdapter

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class OrderDB:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetOrderData(arg):
        '''
        GetOrderData : Get the data of user's latest order
                Input argu :
                    data_type - checkoutid
                    userid - buyer's userid
                Return :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        data_type = arg["data_type"]
        userid = arg["userid"]

        ##Generate db_name
        db_number = int(userid) % 100000 // 1000 // 5
        db_name = "staging_order_" + str("%02d" % (db_number))

        ##Generate table name
        table_number = int(userid) % 100000 // 1000
        tab_number = int(userid) % 100000
        if data_type in ("checkoutid"):
            table_name = "shopee_checkout_v2_db_" + str("%08d" % (table_number)) + ".checkout_v2_tab_" + str("%08d" % (tab_number))

        ##Generate SQL cmd
        GlobalAdapter.DBVar._SqlCmd_ = "select " + data_type + " from " + table_name + " where userid = " + userid + " order by ctime DESC limit 1"

        ##Access DB to get and store checkout id
        DBCommonMethod.InitialSQL({"db_name":db_name, "result": "1"})
        DBCommonMethod.SendSQLCommandToDB({"method": "select", "result": "1"})
        DBCommonMethod.StoreDataFromSQLResult({"column": "checkoutid", "result": "1"})
        DBCommonMethod.DeInitialSQL({"result": "1"})

        OK(ret, int(arg['result']), 'OrderDB.GetOrderData -> ' + data_type)


class AdminServiceFeeDB:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetRuleUpdateStatus(arg):
        '''
        SetRuleUpdateStatus : Change rule_update_status to 0 in servicefee db
                Input argu :
                    rule_id - set vaule to 1 and get rule id in Global
                    db_file - db file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get commission fee rule ID
        rule_id = arg['rule_id']
        db_file = arg['db_file']
        GlobalAdapter.CommonVar._DynamicCaseData_["rule_id"] = GlobalAdapter.ServiceFeeE2EVar._RuleID_
        ##Define assign list
        assign_list = []

        ##Check whether if need to assign rule id or not
        if int(rule_id):
            assign_list.append({"column": "rule_id", "value_type": "string"})
        else:
            dumplogger.info("Do not need to assign rule id")
        DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_servicefee", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminServiceFeeDB.SetRuleUpdateStatus')
