import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCProductCard Precondition =="
        dumplogger.info("== Setup THPCProductCard Precondition ==")

    def test_THPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductCard-01.xml")

    def test_THPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductCard-02.xml")

    def test_THPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductCard-03.xml")

    def test_THPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductCard-04.xml")

    def test_THPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductCard-05.xml")

    def test_THPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCProductCard-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCProductCard Post condition =="
        dumplogger.info("== Setup THPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
