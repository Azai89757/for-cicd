import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class COPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCheckOut Precondition =="
        dumplogger.info("== Setup COPCCheckOut Precondition ==")

    def test_COPCCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCheckOut-01.xml")

    def test_COPCCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCheckOut-02.xml")

    def test_COPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCheckOut-03.xml")

    # def test_COPCCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CO/COPCCheckOut-04.xml")

    # def test_COPCCheckOut05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CO/COPCCheckOut-05.xml")

    # def test_COPCCheckOut06(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CO/COPCCheckOut-06.xml")

    # def test_COPCCheckOut07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/CO/COPCCheckOut-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCheckOut Post condition =="
        dumplogger.info("== Setup COPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
