import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCMyVoucherPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCMyVoucherPage Precondition =="
        dumplogger.info("== Setup PHPCMyVoucherPage Precondition ==")

    def test_PHPCMyVoucherPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCMyVoucherPage-01.xml")

    def test_PHPCMyVoucherPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCMyVoucherPage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCMyVoucherPage Post condition =="
        dumplogger.info("== Setup PHPCMyVoucherPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
