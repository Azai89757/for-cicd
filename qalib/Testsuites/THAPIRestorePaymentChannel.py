import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THAPIRestorePaymentChannel(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIRestorePaymentChannel Precondition =="
        dumplogger.info("== Setup THAPIRestorePaymentChannel Precondition ==")

    def test_THAPIRestorePaymentChannel01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/THAPIRestorePaymentChannel-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THAPIRestorePaymentChannel Post condition =="
        dumplogger.info("== Setup THAPIRestorePaymentChannel Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
