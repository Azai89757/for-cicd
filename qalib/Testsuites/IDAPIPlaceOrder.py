import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup IDAPIPlaceOrder Precondition ==")

    def test_IDAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup IDAPIPlaceOrder Post condition ==")
