import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCMediaStock(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMediaStock Precondition =="
        dumplogger.info("== Setup IDPCMediaStock Precondition ==")

    def test_IDPCMediaStock01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMediaStock-01.xml")

    def test_IDPCMediaStock02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMediaStock-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMediaStock Post condition =="
        dumplogger.info("== Setup IDPCMediaStock Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
