#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminModuleHeaderMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import time


##Import common library
import XtFunc
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

##Import Admin library
from PageFactory.Admin import AdminCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AdminHeaderTextPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyHeaderTextGeneralInfo(arg):
        '''
        ModifyHeaderTextGeneralInfo : Edit header text general info
                Input argu :
                    title_name - title name
                    description - description
                    adjust_start_time - adjust header start time
                    adjust_end_time - adjust header end time
                    rule_set - rule set to be chosen
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        title_name = arg["title_name"]
        description = arg["description"]
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]
        rule_set = arg["rule_set"]

        ##Input title name
        if title_name:
            AdminHeaderTextComponent.InputToColumn({"column_type": "title_name", "input_content": title_name, "result": "1"})

        ##Input title name
        if description:
            AdminHeaderTextComponent.InputToColumn({"column_type": "description", "input_content": description, "result": "1"})

        ##Input start time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(adjust_start_time), 0)
            xpath = Util.GetXpath({"locate": "start_time_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time text box", "result": "1"})
            time.sleep(1)
            AdminHeaderTextComponent.InputToColumn({"column_type": "start_time", "input_content": time_stamp_start, "result": "1"})
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Input end time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M:%S", 0, int(adjust_end_time), 0)
            xpath = Util.GetXpath({"locate": "end_time_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time text box", "result": "1"})
            time.sleep(1)
            AdminHeaderTextComponent.InputToColumn({"column_type": "end_time", "input_content": time_stamp_end, "result": "1"})
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Choose rule set
        if rule_set:
            xpath = Util.GetXpath({"locate": "rule_set_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click rule set text box", "result": "1"})
            time.sleep(1)
            AdminHeaderTextComponent.InputToColumn({"column_type": "rule_set", "input_content": rule_set, "result": "1"})
            xpath = Util.GetXpath({"locate": "rule_set_input"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextPage.ModifyHeaderTextGeneralInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyHeaderTextConfiguration(arg):
        '''
        ModifyHeaderTextConfiguration : Edit header text configuration
                Input argu :
                    component_name - component name
                    header_text_others - header text other than English / Simplified Chinese
                    header_text_en - header text in English
                    header_text_zh - header text in Simplified Chinese
                    header_upload_type - header image upload type (file / image)
                    header_image_name - upload header image upload name (only for flashsale)
                    header_image_type - upload header image upload type (only for flashsale)
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        component_name = arg["component_name"]
        header_text_others = arg["header_text_others"]
        header_text_en = arg["header_text_en"]
        header_text_zh = arg["header_text_zh"]
        header_upload_type = arg["header_upload_type"]
        header_image_name = arg["header_image_name"]
        header_image_type = arg["header_image_type"]

        ##Choose component name
        if component_name:
            xpath = Util.GetXpath({"locate": "component_box"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click component box", "result": "1"})
            xpath = Util.GetXpath({"locate": "component_name"})
            xpath = xpath.replace("name_to_be_replaced", component_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose component", "result": "1"})

        ##Input header text in language other than English and Simplified Chinese
        if header_text_others:
            AdminHeaderTextComponent.InputToColumn({"column_type": "header_text_others", "input_content": header_text_others, "result": "1"})

        ##Input header text in language in English
        if header_text_en:
            AdminHeaderTextComponent.InputToColumn({"column_type": "header_text_en", "input_content": header_text_en, "result": "1"})

        ##Input header text in language in Simplified Chinese
        if header_text_zh:
            AdminHeaderTextComponent.InputToColumn({"column_type": "header_text_zh", "input_content": header_text_zh, "result": "1"})

        ##Upload image
        if header_upload_type and header_image_name and header_image_type:
            xpath = Util.GetXpath({"locate": "image_upload"})
            BaseUICore.UploadFileWithCSS({"locate": xpath, "action": header_upload_type, "file_name": header_image_name, "file_type": header_image_type, "result": "1"})
            time.sleep(5)

        OK(ret, int(arg['result']), 'AdminHeaderTextPage.ModifyHeaderTextConfiguration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditDefaultHeaderText(arg):
        '''
        EditDefaultHeaderText : Edit default header text
                Input argu :
                    name - component name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit default text", "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextPage.EditDefaultHeaderText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseScheduledHeaderTextAction(arg):
        '''
        ChooseScheduledHeaderTextAction : Choose scheduled header text action
                Input argu :
                    action - edit / delete
                    name - component name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        action = arg["action"]
        name = arg["name"]

        ##Choose action
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose scheduled header text action -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextPage.ChooseScheduledHeaderTextAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseHeaderTextConfigurationAction(arg):
        '''
        ChooseHeaderTextConfigurationAction : Choose header text configuration action
                Input argu :
                    action - edit / delete
                    name - component name
                Return code : 1 - success
                              0 - fail
                              -1 - error
        '''
        ret = 1
        action = arg["action"]
        name = arg["name"]

        ##Click choose action (edit or delete)
        xpath = Util.GetXpath({"locate": action})
        xpath = xpath.replace("name_to_be_replaced", name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose header text configuration action -> " + action, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextPage.ChooseHeaderTextConfigurationAction')


class AdminHeaderTextComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckbox(arg):
        '''
        ClickOnCheckbox : Click any type of checkbox with well defined locator
                Input argu :
                    checkbox_type - details_page_all_product / details_page_option_product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        checkbox_type = arg["checkbox_type"]
        model_id = arg["model_id"]

        ##Click on checkbox
        if checkbox_type == "details_page_all_product":
            xpath = Util.GetXpath({"locate": "details_page_all_product"})

        else:
            xpath = Util.GetXpath({"locate": "details_page_option_product"})
            xpath = xpath.replace("model_id_to_be_replaced", model_id)

        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click check box: %s, which xpath is %s" % (checkbox_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSellerDiscountComponent.ClickOnCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def NavigateToPage(arg):
        '''
        NavigateToPage : Navigate to a page based on page_type
            Input argu :
                page_type - type of page to be navigate to
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click and navigate to page
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Navigate to Page: %s, which xpath is %s" % (page_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextComponent.NavigateToPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - title_name / description / start_time / end_time / rule_set
                input_content - content that input to column
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSection(arg):
        '''
        ClickOnSection : Click any type of section with well defined locator
            Input argu :
                section_type - type of section which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]

        ##Click on section
        xpath = Util.GetXpath({"locate": section_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click section: %s, which xpath is %s" % (section_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AdminHeaderTextComponent.ClickOnSection')
