import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCSignup Precondition =="
        dumplogger.info("== Setup INPCSignup Precondition ==")

    def test_INPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCSignup Post condition =="
        dumplogger.info("== Setup INPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
