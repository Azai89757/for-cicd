#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidListingMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import os
import time

##Import common library
import Util
import Config
import XtFunc
import FrameWorkBase
import DecoratorHelper
import AndroidBaseUILogic
from Config import dumplogger

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidCreateProductPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductName(arg):
        '''
        InputProductName : Input Product Name on add product page
                Input argu :
                    product_name - input product name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Input product name
        xpath = Util.GetXpath({"locate":"add_product_name"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":product_name, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductDescription(arg):
        '''
        InputProductDescription : Input Product description in create product page
                Input argu :
                    product_description - input Product description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_description = arg["product_description"]

        ##Input product description
        xpath = Util.GetXpath({"locate":"add_product_description"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":product_description, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductDescription')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductPrice(arg):
        '''
        InputProductPrice : input Product price in create product page
                Input argu :
                    product_price - input Product price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_price = arg["product_price"]

        ##Input product price
        xpath = Util.GetXpath({"locate":"product_price"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":product_price, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductAmount(arg):
        '''
        InputProductAmount : input Product Amount in create product page
                Input argu :
                    product_amount - input Product Amount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_amount = arg["product_amount"]

        ##Input product amount
        xpath = Util.GetXpath({"locate":"product_amount"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":product_amount, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationValue(arg):
        '''
        InputVariationValue : input variation value in create product page
                Input argu :
                    variation_value - input variation value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        variation_value = arg["variation_value"]

        ##Input variation value
        xpath = Util.GetXpath({"locate":"variation_value_input_field"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click text field", "result": "1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":variation_value, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputVariationValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationOption(arg):
        '''
        InputVariationOption : input variation option in create product page
                Input argu :
                    variation_option - input variation option
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        variation_option = arg["variation_option"]

        ##Input variation option
        xpath = Util.GetXpath({"locate":"variation_option_input_field"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click text field", "result": "1"})
        time.sleep(2)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":variation_option, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputVariationOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductWeight(arg):
        '''
        InputProductWeight : Input product weight in create product page
                Input argu :
                    product_weight - input product weight
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_weight = arg["product_weight"]

        ##Click delivery options
        xpath = Util.GetXpath({"locate":"delivery_option"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})
        time.sleep(5)

        ##Input product weight
        xpath = Util.GetXpath({"locate":"product_weight"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":product_weight, "result":"1"})
        time.sleep(30)

        ## Check SLS option exist
        xpath = Util.GetXpath({"locate":"sls_option"})
        AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"})

        ##Click comfirm button
        xpath = Util.GetXpath({"locate":"comfirm_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click comfirm button", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductWeight')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWeight(arg):
        '''
        InputWeight : Input weight in create product page
                Input argu :
                    weight - input weight
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        weight = arg["weight"]

        ##Input product weight
        xpath = Util.GetXpath({"locate":"weight"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":weight, "result":"1"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputWeight')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWholeSaleMinAmount(arg):
        '''
        InputWholeSaleMinAmount : input Wholesale min amount in create product page
                Input argu :
                    wholesale_min_amount - input Wholesale min amount
                    index - input row index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        wholesale_min_amount = arg["wholesale_min_amount"]
        index = arg["index"]

        ##Input Wholesale amount
        xpath = Util.GetXpath({"locate":"wholesale_min_amount"})
        xpath = xpath.replace("index_to_be_replaced", index)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":wholesale_min_amount, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputWholeSaleMinAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWholeSaleMaxAmount(arg):
        '''
        InputWholeSaleMaxAmount : input Wholesale max amount  in create product page
                Input argu :
                    wholesale_max_amount - input Wholesale max amount
                    index - input row index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        wholesale_max_amount = arg["wholesale_max_amount"]
        index = arg["index"]

        ##Input Wholesale amount
        xpath = Util.GetXpath({"locate":"wholesale_max_amount"})
        xpath = xpath.replace("index_to_be_replaced", index)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":wholesale_max_amount, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputWholeSaleMaxAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputWholeSalePrice(arg):
        '''
        InputWholeSalePrice : input Wholesale price in create product page
                Input argu :
                    wholesale_price - input Wholesale price
                    index - input row index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        wholesale_price = arg["wholesale_price"]
        index = arg["index"]

        ##Input Wholesale price
        xpath = Util.GetXpath({"locate":"wholesale_price"})
        xpath = xpath.replace("index_to_be_replaced", index)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":wholesale_price, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputWholeSalePrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVariationInfo(arg):
        '''
        InputVariationInfo : Input price and stock to target variation blank
                Input argu :
                    color - black / white
                    size - S / M / L
                    price - price you want to set
                    stock - stock you want to set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        color = arg["color"]
        size = arg["size"]
        price = arg["price"]
        stock = arg["stock"]

        ##Input price for variation
        xpath = Util.GetXpath({"locate":"price_field_" + color})
        xpath = xpath.replace("size_to_be_replaced",size)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":price, "result":"1"})
        time.sleep(2)

        ##Input stock for variation
        xpath = Util.GetXpath({"locate":"stock_field_" + color})
        xpath = xpath.replace("size_to_be_replaced",size)
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":stock, "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputVariationInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDateToShipDay(arg):
        '''
        InputDateToShipDay : Input Date To Ship Day in create product page
                Input argu :
                    day - input Date To Ship Day
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        day = arg["day"]

        ##Input Date To Ship Day
        xpath = Util.GetXpath({"locate":"date_to_ship_day"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"0", "result":"1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":day, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputDateToShipDay')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseUploadOption(arg):
        '''
        ChooseUploadOption : Choose upload option after clicking square
                Input argu :
                    upload_type - choose_photo_from_album / choose_video_from_album
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg['upload_type']

        ##Choose option
        xpath = Util.GetXpath({"locate": upload_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose upload option", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.ChooseUploadOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseBrand(arg):
        '''
        ChooseBrand : Choose Brand
                Input argu :
                    brand_name - brand name you want to choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        brand_name = arg['brand_name']

        ##Click brand tab
        xpath = Util.GetXpath({"locate": "brand_tab"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click brand tab", "result":"1"})

        ##Choose target brand
        xpath = Util.GetXpath({"locate": "target_brand"})
        xpath = xpath.replace("brand_to_be_replaced", brand_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click brand tab", "result":"1"})
        time.sleep(5)

        ##Click comfirm button
        xpath = Util.GetXpath({"locate":"save_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click save button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.ChooseBrand')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddPhotoFromAlbum(arg):
        '''
        AddPhotoFromAlbum : Add the specific photo from album
                Input argu :
                    file_name - your .png file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image = arg['file_name']

        ##Choose target photo from album
        dumplogger.info("Prepare to choose image:" + image + ".png")
        time.sleep(5)
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":image, "threshold":"0.9", "is_click":"yes", "result":"1"})
        time.sleep(5)

        ## Click check icon of photo
        xpath = Util.GetXpath({"locate":"photo_checkbox"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            ## Click checkbox to choose target photo
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click photo check icon", "result":"1"})
            time.sleep(5)

            ## Back to album gallary
            AndroidBaseUICore.AndroidKeyboardAction({"actiontype":"back", "result":"1"})
        else:
            ret = 0
            dumplogger.info("Image can not be choose. Maybe it does not meet specifications.")

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.AddPhotoFromAlbum')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddVideoFromAlbum(arg):
        '''
        AddVideoFromAlbum : Add the first video from album
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        xpath = Util.GetXpath({"locate":"video_checkbox"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            ## Click checkbox to choose first video
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Select video", "result":"1"})
            time.sleep(5)
        else:
            ret = 0
            dumplogger.info("Video can not be choose. Maybe it does not meet specifications.")

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.AddVideoFromAlbum')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddColorVariation(arg):
        '''
        AddColorVariation : Click color option to add variation (in variation page)
                Input argu :
                    color - black / white
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        color = arg["color"]

        xpath = Util.GetXpath({"locate":color})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Add color variation: " + color, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.AddColorVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddSizeVariation(arg):
        '''
        AddSizeVariation : Click size option to add variation (in variation page)
                Input argu :
                    size - S / M / L
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        size = arg["size"]

        xpath = Util.GetXpath({"locate":size})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Add size variation: " + size, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.AddSizeVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRepeatText(arg):
        '''
        InputRepeatText : Input repeat text by setting char and times
                Input argu :
                    text - the text you want to input
                    times - times you want to repeat the char
                    target_locate - FE input field name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        target_locate = arg["target_locate"]
        text = arg["text"]
        times = arg["times"]
        input_text = ""

        for loop_count in range(int(times)):
            input_text += text

        ##Input text to target field
        xpath = Util.GetXpath({"locate":target_locate})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":input_text, "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputRepeatText')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSLSOption(arg):
        '''
        CheckSLSOption : Check SLS Option toggle, default status is opened.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check toggle status,if status is off,click SLS Option.
        if XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":"sls_option_off", "threshold":"0.9", "is_click":"no", "passok":"0", "result":"1"}):
            xpath = Util.GetXpath({"locate":"sls_option"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click SLS Option", "result":"1"})
            time.sleep(10)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.CheckSLSOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseTargetCountry(arg):
        '''
        ChooseTargetCountry : Click target country of origin and click save
                Input argu :
                    country - country
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        country = arg["country"]

        ##Click target country
        xpath = Util.GetXpath({"locate":"target_country"})
        xpath = xpath.replace("country_to_be_replace", country)
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})

        time.sleep(3)

        ##Click save button
        xpath = Util.GetXpath({"locate":"save_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click save button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.ChooseTargetCountry')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductManufacturerDetails(arg):
        '''
        InputProductManufacturerDetails : Input product manufacturer details
                Input argu :
                    detail - detail description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        detail = arg["detail"]

        ##Input manufacturer detail
        xpath = Util.GetXpath({"locate":"manufacturer_detail_input"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":detail, "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductManufacturerDetails')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductHSNCode(arg):
        '''
        InputProductHSNCode : Input product HSN code
                Input argu :
                    code - HSN code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        code = arg["code"]

        ##Input HSN code
        xpath = Util.GetXpath({"locate":"hsn_code_input"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":code, "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputProductHSNCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseTaxCode(arg):
        '''
        ChooseTaxCode : Choose tax code
                Input argu :
                    code_type - tax code type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        code_type = arg["code_type"]

        ##Click tax code area
        xpath = Util.GetXpath({"locate":"tax_code_area"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click tax code area", "result":"1"})
        time.sleep(5)

        ##Choose tax code
        xpath = Util.GetXpath({"locate":"tax_code_type"})
        xpath = xpath.replace("code_type_to_be_replace", code_type)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose tax code", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.ChooseTaxCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPackagingSize(arg):
        '''
        InputPackagingSize : Input packaging size
                Input argu :
                    width - package width
                    length - package length
                    height - package height
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        width = arg["width"]
        length = arg["length"]
        height = arg["height"]

        ##Input package width
        xpath = Util.GetXpath({"locate":"width_input"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":width, "result":"1"})

        ##Input package length
        xpath = Util.GetXpath({"locate":"length_input"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":length, "result":"1"})

        ##Input package height
        xpath = Util.GetXpath({"locate":"height_input"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":height, "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPage.InputPackagingSize')


class AndroidCreateProductPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDTSToggle(arg):
        '''
        ClickDTSToggle : Click DTS toggle in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click DTS toggle
        xpath = Util.GetXpath({"locate":"dts_toggle"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickDTSToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShippingFee(arg):
        '''
        ClickShippingFee : Click Shipping Fee in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Shipping Fee
        xpath = Util.GetXpath({"locate":"shipping_fee"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickShippingFee')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAlbumCheckIcon(arg):
        '''
        ClickAlbumCheckIcon : Click check icon after choosing images
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ## Click checkbox to upload target photo
        xpath = Util.GetXpath({"locate":"album_checkicon"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click album check icon", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickAlbumCheckIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVariationOption(arg):
        '''
        ClickVariationOption : Click Variation in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click variation option
        xpath = Util.GetXpath({"locate":"variation_option"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click variation option", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickVariationOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVariationImageToggle(arg):
        '''
        ClickVariationImageToggle : Click Variation image toggle, default status is opened.
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click variation image toggle
        xpath = Util.GetXpath({"locate":"variation_image_toggle"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click variation image toggle", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickVariationImageToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickNextVarSetting(arg):
        '''
        ClickNextVarSetting : Click next button in variation setting page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click next button in variation setting page
        xpath = Util.GetXpath({"locate":"next_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next button in variation setting page", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickNextVarSetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPublish(arg):
        '''
        ClickPublish : Click publish button in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click publish button in create product page
        xpath = Util.GetXpath({"locate":"publish_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click publish button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickPublish')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in create product page
                Input argu :
                    page_type : edit button in specific page ex:size/color
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click edit
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click edit button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        '''
        ClickUpdate : Click update button in edit product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click update button
        xpath = Util.GetXpath({"locate":"update_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click update button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseCategory(arg):
        '''
        ClickChooseCategory : Click Choose Category in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Choose Category
        xpath = Util.GetXpath({"locate":"choose_category"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"up", "isclick":"1", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickChooseCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChooseCountryOfOrigin(arg):
        '''
        ClickChooseCountryOfOrigin : Click Choose country of origin
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click choose country of origin
        xpath = Util.GetXpath({"locate":"choose_country"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickChooseCountryOfOrigin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HandleCategoryPopup(arg):
        '''
        HandleCategoryPopup : Click Category popup handle
                Input argu :
                    handle_action - accept / cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        handle_action = arg['handle_action']

        ## Check if popup message show up
        xpath = Util.GetXpath({"locate":"popup"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            ## If category popup exist, handle it
            xpath = Util.GetXpath({"locate":handle_action})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Handle category popup", "result": "1"})
        else:
            dumplogger.info("Category popup not exist")

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.HandleCategoryPopup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTargetCategory(arg):
        '''
        ClickTargetCategory : Click target Category in create product page
                Input argu :
                    target_category - choose target category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        target_category = arg["target_category"]

        ##Click Choose Category
        xpath = Util.GetXpath({"locate":"target_category"})
        xpath = xpath.replace("target_to_be_replaced", target_category)
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickTargetCategory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickWholesale(arg):
        '''
        ClickWholesale : Click Wholesale in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Wholesale
        xpath = Util.GetXpath({"locate":"wholesale_button"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickWholesale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmitWholesale(arg):
        '''
        ClickSubmitWholesale : Click Submit Wholesale
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Submit Wholesale
        xpath = Util.GetXpath({"locate":"wholesale_submit_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Submit Wholesale", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickSubmitWholesale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmitVariation(arg):
        '''
        ClickSubmitVariation : Click submit variation
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click submit variation
        xpath = Util.GetXpath({"locate":"variation_submit_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Submit variation", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickSubmitVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddWholesale(arg):
        '''
        ClickAddWholesale : Click create wholesale button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create wholesale button
        xpath = Util.GetXpath({"locate":"wholesale_create_button"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Add Wholesale", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickAddWholesale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteImage(arg):
        '''
        ClickDeleteImage : Click to delete first image
                Input argu :
                    index - index of image you want to delete (ex. First image -> index=1)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg["index"]

        ##Click image delete button with index
        xpath = Util.GetXpath({"locate":"first_delete_button"})
        xpath = xpath.replace("index_to_be_replaced", str(int(index)*2-1))
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click delete image button", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickDeleteImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteWholesale(arg):
        '''
        ClickDeleteWholesale : Click Delete Wholesale
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click wholesale button by OpenCV
        XtFunc.MatchImgByOpenCV({"env": Config._EnvType_, "mode": Config._TestCasePlatform_.lower(), "image":"wholesale_delete_icon", "threshold":"0.9", "is_click":"yes", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickDeleteWholesale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click Save in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save
        xpath = Util.GetXpath({"locate":"save"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click save button", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddVariation(arg):
        '''
        ClickAddVariation : Click add variation in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add variation
        xpath = Util.GetXpath({"locate":"add_variation"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add variation button", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickAddVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDone(arg):
        '''
        ClickDone : Click done in create product page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click done
        xpath = Util.GetXpath({"locate":"done"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click done button", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickDone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddVariationOption(arg):
        '''
        ClickAddVariationOption : Click add in create product page
                Input argu :
                    page_type : add button in specific page ex: color/size
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click add
        xpath = Util.GetXpath({"locate":page_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add button", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickAddVariationOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddPhoto(arg):
        '''
        ClickAddPhoto : Click add photo
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate":"add_photo"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click add photo", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickAddPhoto')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCustomizeVariationOption(arg):
        '''
        ClickCustomizeVariationOption : Click customize variation option in create product page
                Input argu :
                    option: option name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        option = arg["option"]

        ##Click add
        xpath = Util.GetXpath({"locate":"option"})
        xpath = xpath.replace("option_to_be_replace", option)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click option", "result":"1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidCreateProductPageButton.ClickCustomizeVariationOption')


class AndroidSellerCenterButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddProduct(arg):
        '''
        ClickAddProduct : Click Add Product in seller page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add product in seller page
        xpath = Util.GetXpath({"locate":"add_product"})
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result":"1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterButton.ClickAddProduct')
