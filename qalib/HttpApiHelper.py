#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 HttpApiHelper.py: The def of this file called by other function.
'''
from Config import apilogger
import requests
import inspect

##For Send HTTP packets to API interface
class APIController():

    def __init__(self):

        apilogger.info("Enter APIController initial")
        ##Initial header
        self.headers = {'content-type': "application/x-www-form-urlencoded",
                        'cache-control': "no-cache",
                        'user-agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36"
                        }

    def ComposeURL(self, http_method, url, args):
        ''' ComposeURL : Compose Url parameters
                Input argu :
                    http_method - get, post, put
                    url - url for http request
                    args - url parameter to be composed
                Return code :
                    payload - payload to be returned
        '''

        apilogger.info("Enter APIController ComposeURL")
        ##Prepare to merge GET parameter from hash
        fullurl = url
        ##Prepare to merge POST parameter from hash
        payload = ""
        ##Get keys in hash
        keys = args.keys()

        ##Compose url for get method
        if http_method == "get":
            ##Sequence to load keys
            for temp in keys:
                if temp == keys[-1]:
                    fullurl = fullurl + temp + "=" + args[temp]
                    apilogger.debug("fullurl = " + fullurl)
                    apilogger.info("fullurl = " + fullurl)
                    return fullurl
                else:
                    fullurl = fullurl + temp + "=" + args[temp] + "&"

        ##Compose url for post/put method
        elif http_method == "post" or http_method == "put":
            ##Sequence to load keys
            for temp in keys:
                if temp == keys[-1]:
                    payload = payload + temp + "=" + args[temp]
                    apilogger.debug("payload = " + payload)
                    return payload
                else:
                    payload = payload + temp + "=" + args[temp] + "&"

    def SendAPIPacket(self, http_method, url, payload=None, files=None, headers=None, session=None, cookies=None, url_param=None):
        ''' SendAPIPacket : Send API Packet
                Input argu :
                    http_method - get, delete, post, put
                    url - url for http request
                    payload - payload for http request
                    headers - headers for http request
                    session - session for http request
                    cookies - cookies for http request
                    url_param - url parameters for http request (update session usage)
                Return code :
                    http response - session / status / text
                    0 - fail
        '''

        apilogger.info("Enter APIController SendAPIPacket")
        response = ""
        request_session = ""

        apilogger.info("Send to api url: %s" % (url))
        apilogger.info("Send header to api: %s" % (headers))
        apilogger.info("Send cookies to api: %s" % (cookies))
        apilogger.info("Send payload to api: %s" % (payload))
        ##HTTP Get
        if http_method == "get":
            ##Use inherit session to send request
            if inspect.isclass(type(session)) and session:
                apilogger.info("Use request.session to send packet")
                response = session.get(url, timeout=300, headers=headers, cookies=cookies)

            ##Send get request directly
            else:
                apilogger.info("Send get request directly")
                response = requests.get(url, timeout=300, headers=headers, cookies=cookies, data=payload)

        ##HTTP Delete
        elif http_method == "delete":
            ##Use inherit session to send request
            if inspect.isclass(type(session)) and session:
                apilogger.info("Use request.session to send packet")
                response = session.delete(url, timeout=300)

            ##Send delete request directly
            else:
                apilogger.info("Send delete request directly")
                response = requests.delete(url, timeout=300, headers=headers, cookies=cookies)

        #HTTP POST
        elif http_method == "post":

            ##Login and update session
            if session == "new_session":
                apilogger.info("Use request.session to send packet")
                request_session = requests.Session()
                request_session.cookies.update(url_param)
                response = request_session.post(url, data=payload, headers=headers)

            ##Use inherit session to send request
            elif inspect.isclass(type(session)) and session:

                ##Check data(payload) is rawdata or json dumps
                if type(payload) is dict or type(payload) is list:
                    apilogger.info("POST data is JSON or List format")
                    apilogger.info("Use request.session to send packet")
                    response = session.post(url, json=payload, headers=headers, cookies=cookies)
                else:
                    apilogger.info("Use request.session to send packet")
                    response = session.post(url, data=payload, headers=headers, cookies=cookies)
            ##Send Post request directly
            else:
                apilogger.info("Send Post request directly")
                response = requests.post(url=url, data=payload, files=files, headers=headers, cookies=cookies, timeout=300)

        #HTTP PUT
        elif http_method == "put":
            ##Check data(payload) is rawdata or json dumps
            if type(payload) is dict or type(payload) is list:
                apilogger.info("PUT data is JSON format")

                ##Use inherit session to send request
                if inspect.isclass(type(session)) and session:
                    apilogger.info("Use request.session to send packet")
                    response = session.put(url, json=payload)
                else:
                    response = requests.put(url=url, data=payload, headers=headers, cookies=cookies, timeout=300)
            else:
                apilogger.info("PUT data is not JSON format")
                ##Use inherit session to send request
                if inspect.isclass(type(session)) and session:
                    apilogger.info("Use request.session to send packet")
                    response = session.put(url, data=payload)

                ##Send Put request directly
                else:
                    apilogger.info("Send Put request directly")
                    response = requests.put(url=url, json=payload, headers=headers, timeout=300)

        else:
            print "!!!!Please check your http_method!!!!"
            apilogger.error("!!!!Please check your http_method!!!!")
            return 1

        apilogger.info("%s -> Response header: %s" % (http_method, response.headers))
        apilogger.info("%s -> Response status code : %d" % (http_method, response.status_code))
        apilogger.info("%s -> Response text: %s" % (http_method, response.text))
        return {"session":request_session, "status":response.status_code, "text":response.text, "cookie":response.cookies.get_dict(), "headers":dict(response.headers)}
