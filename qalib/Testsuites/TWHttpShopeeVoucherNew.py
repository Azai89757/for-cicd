import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpShopeeVoucherNew(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpShopeeVoucherNew Precondition =="
        dumplogger.info("== Setup TWHttpShopeeVoucherNew Precondition ==")

    def test_TWHttpShopeeVoucherNew01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucherNew-01.xml")

    def test_TWHttpShopeeVoucherNew02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucherNew-02.xml")

    def test_TWHttpShopeeVoucherNew03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucherNew-03.xml")

    def test_TWHttpShopeeVoucherNew04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucherNew-04.xml")

    def test_TWHttpShopeeVoucherNew05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpShopeeVoucherNew-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpShopeeVoucherNew Post condition =="
        dumplogger.info("== Setup TWHttpShopeeVoucherNew Post condition ==")
