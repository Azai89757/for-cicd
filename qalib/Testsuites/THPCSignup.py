import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCSignup(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSignup Precondition =="
        dumplogger.info("== Setup THPCSignup Precondition ==")

    def test_THPCSignup01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCSignup-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCSignup Post condition =="
        dumplogger.info("== Setup THPCSignup Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
