import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class VNPCChat(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCChat Precondition =="
        dumplogger.info("== Setup VNPCChat Precondition ==")

    def test_VNPCChat01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCChat-01.xml")

    def test_VNPCChat02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCChat-02.xml")

    def test_VNPCChat03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCChat-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCChat Post condition =="
        dumplogger.info("== Setup VNPCChat Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
