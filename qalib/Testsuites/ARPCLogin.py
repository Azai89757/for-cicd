import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCLogin Precondition =="
        dumplogger.info("== Setup ARPCLogin Precondition ==")

    def test_ARPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCLogin-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCLogin Post condition =="
        dumplogger.info("== Setup ARPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
