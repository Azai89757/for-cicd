import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCProductPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCProductPromotion Precondition =="
        dumplogger.info("== Setup VNPCProductPromotion Precondition ==")

    def test_VNPCProductPromotion01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-01.xml")

    def test_VNPCProductPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-02.xml")

    def test_VNPCProductPromotion03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-03.xml")

    def test_VNPCProductPromotion04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-04.xml")

    def test_VNPCProductPromotion05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-05.xml")

    def test_VNPCProductPromotion06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-06.xml")

    def test_VNPCProductPromotion07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-07.xml")

    def test_VNPCProductPromotion08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-08.xml")

    def test_VNPCProductPromotion09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-09.xml")

    def test_VNPCProductPromotion10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-10.xml")

    def test_VNPCProductPromotion11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-11.xml")

    def test_VNPCProductPromotion12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-12.xml")

    def test_VNPCProductPromotion13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-13.xml")

    def test_VNPCProductPromotion14(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductPromotion-14.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCProductPromotion Post condition =="
        dumplogger.info("== Setup VNPCProductPromotion Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
