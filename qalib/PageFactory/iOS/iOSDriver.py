#!/usr/bin/env python
# -*- coding: utf-8 -*-

##import common library
import requests
import json
import base64
import string
import urllib3
import socket
import traceback

##import Logger
from Logger import InitialDebugLog

##Import common exceptions
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class iOSLocator(By):
    PREDICATE = "predicate string"
    CLASS_CHAIN = "class chain"
    ACCESSIBILITY_ID = "accessibility id"


class WdaCommands:
    """
        Here's the part where to save the command url
        Just use tuple to save (GET/POST, {url_to_replace})
        string starts with '$' in url will be auto replaced with paramater given
        example: $sessionId will auto replace with 'sessionId' value given
        (For more commands please see : https://github.com/appium/WebDriverAgent/tree/master/WebDriverAgentLib/Commands/*Commands.m file)
    """

    ##Driver
    new_session = ("POST", "/session")
    status = ("GET", "/status")
    find_element = ("POST", "/session/$sessionId/element")
    find_elements = ("POST", "/session/$sessionId/elements")
    swipe = ("POST", "/session/$sessionId/wda/dragfromtoforduration")
    delete_session = ("DELETE", "/session/$sessionId")
    get_window_size = ("GET", "/session/$sessionId/window/size")
    screenshot = ("GET", "/screenshot")
    click_coordinate = ("POST", "/session/$sessionId/wda/tap/0")
    set_clipboard = ("POST", "/session/$sessionId/wda/setPasteboard")
    get_clipboard = ("POST", "/session/$sessionId/wda/getPasteboard")
    deactivate_app = ("POST", "/session/$sessionId/wda/deactivateApp")
    terminate_app = ("POST", "/session/$sessionId/wda/apps/terminate")
    activate_app = ("POST", "/session/$sessionId/wda/apps/activate")
    launch_app = ("POST", "/session/$sessionId/wda/apps/launch")
    get_page_source = ("GET", "/source?format=$format")
    long_press_coordinate = ("POST", "/session/$sessionId/wda/touchAndHold")

    ##Alert
    get_alert_text = ("GET", "/session/$sessionId/alert/text")
    accept_alert = ("POST", "/session/$sessionId/alert/accept")
    dismiss_alert = ("POST", "/session/$sessionId/alert/dismiss")
    get_alert_buttons = ("GET", "/session/$sessionId/alert/buttons")

    ##Element
    click = ("POST", "/session/$sessionId/element/$elementId/click")
    clear = ("POST", "/session/$sessionId/element/$elementId/clear")
    send_keys_to_element = ("POST", "/session/$sessionId/element/$elementId/value")
    get_element_screenshot = ("GET", "/session/$sessionId/element/$elemendId/screenshot")
    get_element_text = ("GET", "/session/$sessionId/element/$elementId/text")
    touch_and_hold = ("POST", "/session/$sessionId/wda/element/$elementId/touchAndHold")
    get_element_attribute = ("GET", "/session/$sessionId/element/$elementId/attribute/$name")
    is_element_enabled = ("GET", "/session/$sessionId/element/$elementId/enabled")


class Alert:
    ##Alert control

    def __init__(self, driver):
        self.driver = driver

    def text(self):
        """text : alert text
                Input argu :
                    N/A
                Return :
                    value - alert text
        note : [[FBRoute GET:@"/alert/text"] respondWithTarget:self action:@selector(handleAlertGetTextCommand:)]
        """
        return self.driver.execute(WdaCommands.get_alert_text)["value"]

    def buttons(self):
        """buttons : alert buttons
                Input argu :
                    N/A
                Return :
                    value - alert buttons
        note : [[FBRoute GET:@"/wda/alert/buttons"] respondWithTarget:self action:@selector(handleGetAlertButtonsCommand:)]
        """
        return self.driver.execute(WdaCommands.get_alert_buttons)["value"]

    def accept(self):
        """accept : accept alert
                Input argu :
                    N/A
                Return :
                    N/A
        note : [[FBRoute POST:@"/alert/accept"] respondWithTarget:self action:@selector(handleAlertAcceptCommand:)]
        """
        self.driver.execute(WdaCommands.accept_alert)

    def dismiss(self):
        """dismiss : dismiss alert
                Input argu :
                    N/A
                Return :
                    N/A
        note : [[FBRoute POST:@"/alert/dismiss"] respondWithTarget:self action:@selector(handleAlertDismissCommand:)]
        """
        self.driver.execute(WdaCommands.dismiss_alert)


class Element:
    ##Element control

    def __init__(self, driver, id):
        self.driver = driver
        self._id = id

    def click(self):
        """click : Clicks the element
                Input argu :
                    N/A
                Return :
                    N/A
        note : [[FBRoute POST:@"/element/:uuid/click"] respondWithTarget:self action:@selector(handleClick:)]
        """
        payload = {
            "elementId": self._id
        }
        self.driver.execute(WdaCommands.click, payload)

    def clear(self):
        """clear : Clears the text if it's a text entry element.
                Input argu :
                    N/A
                Return :
                    N/A
        note : [[FBRoute POST:@"/element/:uuid/clear"] respondWithTarget:self action:@selector(handleClear:)]
        """
        payload = {
            "elementId": self._id
        }
        self.driver.execute(WdaCommands.clear, payload)

    def send_keys(self, string):
        """send_keys : Simulate typing into the element
                Input argu :
                    N/A
                Return :
                    N/A
        note : [[FBRoute POST:@"/element/:uuid/value"] respondWithTarget:self action:@selector(handleSetValue:)]
        """
        payload = {
            "elementId": self._id,
            "text": list(string)
        }
        self.driver.execute(WdaCommands.send_keys_to_element, payload)

    def long_press(self, duration):
        """long_press : Long press the element
                Input argu :
                    duration - long press duration (seconds)
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/element/:uuid/touchAndHold"] respondWithTarget:self action:@selector(handleTouchAndHold:)]
        """
        payload = {
            "elementId": self._id,
            "duration": int(duration)
        }
        self.driver.execute(WdaCommands.touch_and_hold, payload)

    @property
    def text(self):
        """text : The text of the element
                Input argu :
                    N/A
                Return :
                    value - text of element
        note : [[FBRoute GET:@"/element/:uuid/text"] respondWithTarget:self action:@selector(handleGetText:)]
        """
        payload = {
            "elementId": self._id
        }
        return self.driver.execute(WdaCommands.get_element_text, payload)["value"]

    @property
    def isEnabled(self):
        """isEnabled : The enable status of element
                Input argu :
                    N/A
                Return :
                    value - element enable status
        """
        return self.driver.execute(WdaCommands.is_element_enabled)["value"]

    def get_attribute(self, attribute):
        """get_attribute : get attribte of an element
                Input argu :
                    attribute - enabled / rect / text / displayed / selected / name / accessible / accessibilityContainer / type / name / rawIdentifier / value / label
                Return :
                    value - attribute value
        note : [[FBRoute GET:@"/element/:uuid/attribute/:name"] respondWithTarget:self action:@selector(handleGetAttribute:)]
        """
        payload = {
            "elementId": self._id,
            "name": attribute
        }
        return self.driver.execute(WdaCommands.get_element_attribute, payload)["value"]

    def screenshot(self, filepath):
        """screenshot : screenshot an element
                Input argu :
                    filepath - file path to save screen shot
                Return :
                    N/A
        note : [[FBRoute GET:@"/element/:uuid/screenshot"] respondWithTarget:self action:@selector(handleElementScreenshot:)]
        """
        ##Get screenshot response from wda
        value = self.execute(WdaCommands.get_element_screenshot)["value"]

        ##Decode response image value
        raw_value = base64.b64decode(value)

        ##Check png header?
        #png_header = b"\x89PNG\r\n\x1a\n"

        ##save it to file
        with open(filepath, 'wb') as image_file:
            image_file.write(raw_value)


class iOSController:

    ##Init element class
    ##Init timeout
    _web_element_cls = Element
    _timeout = socket._GLOBAL_DEFAULT_TIMEOUT

    def __init__(self, host, desiredCaps):
        self.sessionId = ""
        self.deviceUrl = host
        self.JsonHeader = {
            "content-type": "application/json"
        }
        self.ioslogger = InitialDebugLog(logger_name='iOSDriver_QATest', logger_type='RotatingFileHandler')
        self.start_session(host, desiredCaps)

    def start_session(self, host, desiredCaps):
        """start_session : start session with web driver agent
                Input argu :
                    host - web driver agent host
                    desiredCaps - web driver desired capabilities
                Return :
                    N/A
        note : [[FBRoute POST:@"/session"].withoutSession respondWithTarget:self action:@selector(handleCreateSession:)]
        """
        #apilogger.info("Connecting to webdriver agent: %s, with desiredCaps: %s" % (host, desiredCaps))

        ##Create a new session with desiredCaps
        try:
            response = self.execute(WdaCommands.new_session, desiredCaps)
        except:
            print traceback.format_exc()
        ##Save session id
        self.sessionId = response["sessionId"]
        self.ioslogger.info("Create new session -> %s" % (self.sessionId))

    def deactivate_app(self, duration):
        """deactivate_app : deactivate app for seconds
                Input argu :
                    duration - deactivate app time (seconds)
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/deactivateApp"] respondWithTarget:self action:@selector(handleDeactivateAppCommand:)]
        """
        payload = {
            "duration": int(duration)
        }
        self.execute(WdaCommands.deactivate_app, payload)

    def terminate_app(self, bundle_id):
        """terminate_app : terminate an app
                Input argu :
                    bundle_id : bundle id of the app
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/apps/terminate"] respondWithTarget:self action:@selector(handleSessionAppTerminate:)]
        """
        payload = {
            "bundleId": bundle_id
        }
        return self.execute(WdaCommands.terminate_app, payload)

    def activate_app(self, bundle_id):
        """activate_app : activate an app
                Input argu :
                    bundle_id : bundle id of the app
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/apps/activate"] respondWithTarget:self action:@selector(handleSessionAppActivate:)]
        """
        payload = {
            "bundleId": bundle_id
        }
        return self.execute(WdaCommands.activate_app, payload)

    def launch_app(self, config):
        """launch_app : Launch an app
                Input argu :
                    config : launch app config
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/apps/launch"] respondWithTarget:self action:@selector(handleSessionAppLaunch:)],
        """
        return self.execute(WdaCommands.launch_app, config)

    def _wrap_value(self, value):
        """_wrap_value : wrap value for sending request to wda
                Input argu :
                    value - value to wrap
                Return :
                    converted - converted value
        """
        ##If value is dictionary
        if isinstance(value, dict):
            converted = {}
            ##Insert value to its key
            for key, val in value.items():
                converted[key] = self._wrap_value(val)
            return converted
        ##If value is element
        elif isinstance(value, self._web_element_cls):
            ##Return element
            return {'ELEMENT': value.id, 'element-6066-11e4-a52e-4f735466cecf': value.id}
        ##If value is list
        elif isinstance(value, list):
            dumped_list = []
            ##Append each wrapped value to list
            for item in value:
                dumped_list.append(self._wrap_value(item))
            return dumped_list
        else:
            return value

    def _unwrap_value(self, value):
        """_unwrap_value : unwrap value for response get from wda
                Input argu :
                    value - response value to unwrap
                Return :
                    value - unwrapped value
        """
        ##if value is dictionary
        if isinstance(value, dict):
            ##if element contains element related key
            if 'ELEMENT' in value or 'element-6066-11e4-a52e-4f735466cecf' in value:
                wrapped_id = value.get('ELEMENT', None)
                ##return Element
                if wrapped_id:
                    return self.create_web_element(value['ELEMENT'])
                else:
                    return self.create_web_element(value['element-6066-11e4-a52e-4f735466cecf'])
            else:
                ##Iterate all values in dictionary keys
                for key, val in value.items():
                    value[key] = self._unwrap_value(val)
                return value
        ##if value is list
        elif isinstance(value, list):
            dumped_list = []
            ##Append all unwrapped value into dumped list
            for item in value:
                dumped_list.append(self._unwrap_value(item))
            return dumped_list
        else:
            return value

    def execute(self, driver_command, params=None):
        """execute : send command to web driver agent server
                Input argu :
                    driver_command - (request method, api url)
                    params - params to send to server
                Return :
                    response - api response from wda server
        """
        ##Check if params have session id
        if self.sessionId is not None:
            if not params:
                params = {'sessionId': self.sessionId}
            elif 'sessionId' not in params:
                params['sessionId'] = self.sessionId

        ##Handle web driver agent api url
        params = self._wrap_value(params)
        self.ioslogger.info("wrapped params: %s" % (params))
        url_path = string.Template(driver_command[1]).substitute(params)
        final_url = self.deviceUrl + url_path
        self.ioslogger.info("final url -> %s" % (final_url))

        ##Dump params into string and delete sessionId key
        if "sessionId" in params:
            del params["sessionId"]
        body = json.dumps(params)

        ##Don't need to send any data if request method is not POST or PUT
        if body and driver_command[0] != "POST" and driver_command[0] != "PUT":
            body = None

        ##Create request to send to web driver agent
        self.ioslogger.info("Prepare to send requets, method: %s, url: %s, body: %s" % (driver_command[0], final_url, body))
        http = urllib3.PoolManager(timeout=self._timeout)
        response = http.request(driver_command[0], final_url, body=body, headers=self.JsonHeader)

        ##Handle response
        if response.data:
            ##dumps response into json format
            json_response = json.loads(response.data)

            ##Handle if response have error
            #print "Handle response", json_response
            self.error_handle(json_response)

            ##Return formatted response
            self.ioslogger.info("response -> %s" % (self._wrap_value(json_response)))
            return self._unwrap_value(json_response)

    def error_handle(self, response):
        """error_handle : handle when response have error
                Input argu :
                    response - wda server response
                Return code :
                    N/A
        note : it will directly raise correspond error
        """
        ##Check response not none and response value key is not none
        if response is not None and response.get("value") is not None:
            #print "error handle", response.get("value")
            ##Get value of response
            value = response.get("value")
            ##Check if value is dictionary
            if isinstance(value, dict):
                ##value contains error key
                if "error" in value:
                    ##Get error msg and raise correspond error
                    error = value["error"]
                    if error == "no such element":
                        raise NoSuchElementException("No such element, please check your xpath")
                    elif error == "unknown error":
                        raise WebDriverException("eoncounter unknown error")
            else:
                self.ioslogger.info("value is not a dictionary, it won't be handled")

    def status(self):
        """status : Get web driver agent status
                Input argu :
                    N/A
                Return :
                    response dict - web driver agent status dictoinary
        note : [[FBRoute GET:@"/status"].withoutSession respondWithTarget:self action:@selector(handleGetStatus:)]
        """
        return self.execute(WdaCommands.status)

    def get_window_size(self):
        """get_window_size : get window size of device
                Input argu :
                    N/A
                Return :
                    screen_size_dict
        note : [[FBRoute GET:@"/window/size"] respondWithTarget:self action:@selector(handleGetWindowSize:)]
        """
        return self.execute(WdaCommands.get_window_size)["value"]

    def create_web_element(self, elementId):
        """create web element : create web element
                Input argu :
                    elementId - elementId to be create element
                Return :
                    Element - created element
        """
        return self._web_element_cls(self, elementId)

    def find_element(self, using, value):
        """find_element : find element by given strategy
                Input argu :
                    using - method to use to find element
                    value - value of the element
                Return :
                    element - element found by driver
        note : [[FBRoute POST:@"/element"] respondWithTarget:self action:@selector(handleFindElement:)]
        """
        self.ioslogger.info("using %s to find element %s" % (using, value))
        payload = {
            "using": using,
            "value": value
        }
        return self.execute(WdaCommands.find_element, payload)["value"]

    def find_elements(self, using, value):
        """find_elements : find elements by given strategy
                Input argu :
                    using - method to use to find element
                    value - value of the element
                Return :
                    elements - elements found by driver
        note : [[FBRoute POST:@"/elements"] respondWithTarget:self action:@selector(handleFindElements:)]
        """
        self.ioslogger.info("using %s to find elements %s" % (using, value))
        payload = {
            "using": using,
            "value": value
        }
        return self.execute(WdaCommands.find_elements, payload)["value"]

    def find_element_by_xpath(self, locate):
        """find_element_by_xpath : find element using xpath
                Input argu :
                    locate - xpath of the element
                Return :
                    element - element if it was found
        """
        return self.find_element(using=By.XPATH, value=locate)

    def find_elements_by_xpath(self, locate):
        """find_elements_by_xpath : find elements using xpath
                Input argu :
                    locate - xpath of the element
                Return :
                    elements - elements if they were found
        """
        return self.find_elements(using=By.XPATH, value=locate)

    def find_element_by_accessibility_id(self, locate):
        """find_element_by_accessibility_id : find element using accessibility id
                Input argu :
                    locate - id of the element
                Return :
                    element - element if it was found
        """
        return self.find_element(using=iOSLocator.ACCESSIBILITY_ID, value=locate)

    def find_elements_by_accessibility_id(self, locate):
        """find_elements_by_accessibility_id : find elements using accessibility id
                Input argu :
                    locate - id of the element
                Return :
                    element - element if it was found
        """
        return self.find_elements(using=iOSLocator.ACCESSIBILITY_ID, value=locate)

    def click_coordinate(self, x, y):
        """click_coordinate : click coordinate on the screen
                Input argu :
                    x - x point of the screen
                    y - y point of the screen
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/tap/:uuid"] respondWithTarget:self action:@selector(handleTap:)]
        """
        payload = {
            "x": x,
            "y": y
        }
        self.execute(WdaCommands.click_coordinate, payload)

    def swipe(self, x1, y1, x2, y2, duration):
        """swipe : swipe the screen
                Input argu :
                    x1 - original x point on the screen
                    y1 - original y point on the screen
                    x2 - distination x point on the screen
                    y2 - distination y point on the screen
                    duration - start coordinate press duration (seconds)
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/dragfromtoforduration"] respondWithTarget:self action:@selector(handleDragCoordinate:)]
        """
        payload = {
            "fromX": x1,
            "fromY": y1,
            "toX": x2,
            "toY": y2,
            "duration": duration
        }
        self.execute(WdaCommands.swipe, payload)

    def long_press(self, x, y, duration):
        """long_press : long press a coordinate
                Input argu :
                    x - x point on the screen
                    y - y point on the screen
                    duration - seconds of hold time
                Return :
                    N/A
        note : [[FBRoute POST:@"/wda/touchAndHold"] respondWithTarget:self action:@selector(handleTouchAndHoldCoordinate:)]
        """
        payload = {
            "x": x,
            "y": y,
            "duration": duration
        }
        self.execute(WdaCommands.long_press_coordinate, payload)

    def dump_source(self, filepath, format):
        """dump_source : dump current page source
                Input argu :
                    filepath - file path to save dumped source
                    format - dump source format json / xml
                Return :
                    N/A
        note : [[FBRoute GET:@"/source"].withoutSession respondWithTarget:self action:@selector(handleGetSourceCommand:)]
        """
        payload = {
            "format": format
        }
        value = self.execute(WdaCommands.get_page_source, payload)["value"]
        with open(filepath, "wb") as f:
            f.write(value.encode("utf8"))

    def switch_to_alert(self):
        """switch_to_alert
        """
        alert = Alert(self)
        return alert

    def set_clipboard(self, string):
        """
        note : [[FBRoute POST:@"/wda/setPasteboard"] respondWithTarget:self action:@selector(handleSetPasteboard:)]
        """
        payload = {
            "contentType":"plaintext",
            "content": base64.b64encode(string.encode()).decode()
        }
        self.execute(WdaCommands.set_clipboard, payload)["value"]

    def get_clipboard(self):
        """
        note : [[FBRoute POST:@"/wda/getPasteboard"] respondWithTarget:self action:@selector(handleGetPasteboard:)]
        """
        payload = {
            "contentType":"plaintext"
        }
        return self.execute(WdaCommands.get_clipboard, payload)["value"]

    def implicitly_wait(self, timeout):
        self._timeout = timeout

    def get_screenshot_as_file(self, filepath):
        """get_screenshot_as_file : get device screenshot and save it to file
                Input argu :
                    filepath - saved screenshot file path
                Return :
                    N/A
        note : [[FBRoute GET:@"/screenshot"].withoutSession respondWithTarget:self action:@selector(handleGetScreenshot:)]
        """
        value = self.execute(WdaCommands.screenshot)["value"]
        raw_value = base64.b64decode(value)

        ##Check png header?
        #png_header = b"\x89PNG\r\n\x1a\n"

        with open(filepath, 'wb') as image_file:
            image_file.write(raw_value)

    def quit(self):
        """quit : delete current session
                Input argu :
                    N/A
                Return :
                    N/A
        note : [[FBRoute DELETE:@""] respondWithTarget:self action:@selector(handleDeleteSession:)]
        """
        self.execute(WdaCommands.delete_session)
