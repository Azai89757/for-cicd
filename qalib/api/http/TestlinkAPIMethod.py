#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 TestlinkAPIMethod.py: The def of this file called by other function.
'''

##Import framework common library
from Config import dumplogger
import HttpApiHelper


class TestLinkController(HttpApiHelper.APIController):

    @staticmethod
    def GetTestResultReport(arg):
        dumplogger.info("Enter GetTestResultReport")

        ##Intial API Controller
        AP = HttpApiHelper.APIController()

        payload = {
                'tl_login': 'admin',
                'tl_password': 'admin'
                    }

        url_login = "http://10.143.221.116/testlink-1.9.18/login.php?viewer="
        url_get_report = "http://10.143.221.116/testlink-1.9.18/lib/results/resultsGeneral.php?"

        ##Call SendAPIPacket to get login session first
        request_session = AP.SendAPIPacket("post", url_login, session=payload)

        ##Call ComposeURL to completed merge action
        final_url_report = AP.ComposeURL("get", url_get_report, arg)

        ##Call SendAPIPacket to get result report
        response = AP.SendAPIPacket("get", final_url_report, session=request_session)

        return response["text"]
