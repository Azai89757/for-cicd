import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class COPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCoins Precondition =="
        dumplogger.info("== Setup COPCCoins Precondition ==")

    def test_COPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-01.xml")

    def test_COPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-02.xml")

    def test_COPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-03.xml")

    def test_COPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-04.xml")

    def test_COPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-05.xml")

    def test_COPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-06.xml")

    def test_COPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CO/COPCCoins-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COPCCoins Post condition =="
        dumplogger.info("== Setup COPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
