import sys
import json
import os
from collections import OrderedDict
from itertools import chain


def json_compare(filename):
    testcase_output = {}
    other_output = {}

    ##Open file with binary
    with open(filename, 'r') as f:
        raw_data = f.read()
        ##decode with utf-8 and user json.loads to dictionary
        try:
            dict_ = json.loads(raw_data.decode('utf-8'))
        except:
            print "Please check the validation of this json file: %s" % (filename)
            return 1

    if "i18n" in filename:
        for key, value in dict_.iteritems():
            if "-" in key:
                ##save in testcase_output dict
                testcase_output[key] = value
            elif "-" not in key:
                ##save in other_output dict
                other_output[key] = value

        ##Use json.dumps to sort dict and to json
        testcase_output_json = json.dumps(testcase_output, sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False)
        other_output_json = json.dumps(other_output, sort_keys=True, indent=4, separators=(',', ': '), ensure_ascii=False)

        ##Use json.loads to ordered dictionary to keep json order
        testcase_output_dict = json.loads(testcase_output_json, object_pairs_hook=OrderedDict)
        other_output_dict = json.loads(other_output_json, object_pairs_hook=OrderedDict)

        ##Merge 2 ordered dictionary
        output = OrderedDict(chain(testcase_output_dict.items(), other_output_dict.items()))

        ##Use json.dumps to json
        output_json = json.dumps(output, indent=4, separators=(',', ': '), ensure_ascii=False)

        output_json += '\n'
        # same = set(data).intersection(output_json)
        output_json = output_json.encode('utf-8')

        if output_json == raw_data:
            #print "json file: %s is valid." % (filename)
            return 0
        else:
            print "Please sort your json: %s before you commit !!!" % (filename)
            return 1
    else:
        #print "json file: %s is valid." % (filename)
        return 0

##Get file path from input argument
valid_status = 0
file_list = []
file_path = sys.argv[1]
if os.path.isdir(file_path):
    #print file_path, 'is dir'
    ##Create file list for add .json file
    for root, dirs, files in os.walk(file_path):
        for sub_dir in dirs:
            dir_path = os.path.join(root, sub_dir)
            dir_list = os.listdir(dir_path)
            for filename in dir_list:
                if ".json" in filename:
                    if json_compare(os.path.join(dir_path, filename)) == 1:
                        valid_status = 1

sys.exit(valid_status)
