﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminPromotionMethod: The def of this file called by XML mainly.
'''

##Import system library
import os
import csv
import time
import codecs

##Import common library
import Util
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def ChangeCoinsAmountFromDB(arg):
    '''
    ChangeCoinsAmountFromDB : Change Coins Amount From DB
            Input argu :
                db_name - database name from dbconfig.ini
                available_amount - available amount of coins
                db_file - db file name
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    db_name = arg['db_name']
    available_amount = arg["available_amount"]
    db_file = arg["db_file"]

    ##For VN, available_amount rate is 1:100 ; Example:If available_amount is 30 then coins will be 3000
    ##define assign_list
    assign_list = [{"column": "available_amount", "value_type": "number"}]
    GlobalAdapter.CommonVar._DynamicCaseData_["available_amount"] = available_amount

    ##Send SQL command
    DBCommonMethod.SendSQLCommandProcess({"db_name":db_name, "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

    OK(ret, int(arg['result']), 'ChangeCoinsAmountFromDB')


class AdminFlashSaleButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownload(arg):
        '''
        ClickDownload : Click download button in admin flash sale page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download button
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickDownload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickManualConfiguration(arg):
        '''
        ClickManualConfiguration : Click manual configuration button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click manual configuration button
        xpath = Util.GetXpath({"locate": "manual_config_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click manual configuration button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickManualConfiguration')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRecommendationList(arg):
        '''
        ClickRecommendationList : Click recommendation list button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click recommendation list button
        xpath = Util.GetXpath({"locate": "recomm_list_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click recommendation list button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickRecommendationList')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNew(arg):
        '''
        ClickCreateNew : Click create new
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click create new
        xpath = Util.GetXpath({"locate": "create_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickCreateNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductScreening(arg):
        '''
        ClickProductScreening : Click Product Screening
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click product screening button
        xpath = Util.GetXpath({"locate": "product_screening"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Product Screening button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickProductScreening')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisplayManagement(arg):
        '''
        ClickDisplayManagement : Click Display Management
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click display management button
        xpath = Util.GetXpath({"locate": "display_management"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Display Management button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickDisplayManagement')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickManualUploaded(arg):
        '''
        ClickManualUploaded : Click manual uploaded
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Manual Uploaded button
        xpath = Util.GetXpath({"locate": "manual_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Manual Uploaded button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickManualUploaded')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        BaseUICore.CheckButtonClickable({"message":"Wait Confirm", "method":"xpath", "locate":Util.GetXpath({"locate":"confirm_btn"}), "state":"enable", "result": "1"})
        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"confirm_btn"}), "message":"Click Confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditProduct(arg):
        '''
        ClickEditProduct : Edit product
                Input argu :
                    product_short_name - product short name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_short_name = arg["product_short_name"]

        ##Click target product edit button
        xpath = Util.GetXpath({"locate": "product_edit_btn"})
        xpath = xpath.replace("product_short_name_to_be_replace", product_short_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click target product edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickEditProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddModelVariation(arg):
        '''
        ClickAddModelVariation : Click add model variation button
                Input argu :
                    item_id - item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click add model variation button
        xpath = Util.GetXpath({"locate": "add_model_variation_btn"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add model variation button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickAddModelVariation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductSave(arg):
        '''
        ClickProductSave : Click product save button
                Input argu :
                    item_id - item id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSaleButton.ClickProductSave')


class AdminFlashSalePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadBatchChangedFile(arg):
        '''
        UploadBatchChangedFile : Upload batch changed file
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Upload batch changed file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-upload-to-add-0", "element_type": "id", "project": "FlashSale", "action": "file", "file_name": "flashsale_batch_change", "file_type": "csv", "result": "1"})

        ##Click upload button
        BaseUICore.ExecuteScript({"script":"document.getElementById('button-file-upload-0').removeAttribute('disabled')", "result": "1"})
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.UploadBatchChangedFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateFlashSale(arg):
        '''
        CreateFlashSale : Create new flash sale
                Input argu :
                    name - flash sale name
                    nomination - 1 (don't allow seller nomination) / 0 (allow seller nomination)
                    Price_preview - 1 (don't display flashSale price under preview) / 0 (display flashSale price under preview)
                    start_time - flash sale star time, now time stamp add input minute
                    end_time - flash sale end time, now time stamp add input minute
                    description - flash
                    url - flase sale url
                    min_stock - min stock number
                    min_discount : min discount price
                    max_discount : max discount price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        name = arg['name']
        nomination = arg['nomination']
        Price_preview = arg['Price_preview']
        start_time = arg['start_time']
        end_time = arg['end_time']
        description = arg['description']
        url = arg['url']
        min_stock = arg['min_stock']
        min_discount = arg['min_discount']
        max_discount = arg['max_discount']

        ##Input promotion name
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "promotion_name"}), "string": name, "result": "1"})

        ##Click Allow Self Nomination toggle
        if int(nomination):
            BaseUICore.ExecuteScript({"script": "document.getElementById('is_self_nomination').click()", "result": "1"})

        ##Click Display FlashSale Price under Preview toggle
        if int(Price_preview):
            BaseUICore.ExecuteScript({"script": "document.getElementById('is_show_price').click()", "result": "1"})

        ##Input start time
        if start_time:
            ##Get current date time and add minutes on it
            time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
        else:
            ##Use current date time when blank
            time_stamp_start = start_time

        ##Input promotion start date
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "promotion_start_time"}), "string": time_stamp_start, "result": "1"})

        ##Input end time
        if end_time:
            ##Get current date time and add minutes on it
            time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)
        else:
            ##Use current date time when blank
            time_stamp_end = end_time

        ##Input promotion end date
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "promotion_end_time"}), "string": time_stamp_end, "result": "1"})

        ##Input promotion description
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "promotion_description"}), "string": description, "result": "1"})

        ##Upload PC banner
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "pc-banner-picker", "element_type": "id", "project": "FlashSale", "action": "image", "file_name": "1200x230_pc", "file_type": "jpg", "result": "1"})
        time.sleep(3)

        ##Upload APP banner
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "banner-picker", "element_type": "id", "project": "FlashSale", "action": "image", "file_name": "1200x240_app", "file_type": "jpg", "result": "1"})
        time.sleep(3)

        ##Input banner URL
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "landing_page_url"}), "string": url, "result": "1"})

        ##Input mininum stock
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "promotion_min_stock"}), "string": min_stock, "result": "1"})

        ##Input discount value
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "min_discount"}), "string": min_discount, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "max_discount"}), "string": max_discount, "result": "1"})

        ##Nomination Settings
        if int(nomination):
            nomination_start_time = arg['nomination_start_time']
            nomination_end_time = arg['nomination_end_time']
            nomination_condition = arg['nomination_condition']
            nomination_seat = arg['nomination_seat']
            max_dts = arg['max_dts']

            ##Input nomination start time
            if nomination_start_time:
                ##Get current date time and add minutes on it
                time_stamp_start = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(nomination_start_time), 0)
            else:
                ##Use current date time when blank
                time_stamp_start = nomination_start_time
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "nomination_start_time"}), "string": time_stamp_start, "result": "1"})

            ##Input nomination end time
            if nomination_end_time:
                ##Get current date time and add minutes on it
                time_stamp_end = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(nomination_end_time), 0)
            else:
                ##Use current date time when blank
                time_stamp_end = nomination_end_time
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "nomination_end_time"}), "string": time_stamp_end, "result": "1"})

            ##Input Nomination Condition
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "nomination_condition"}), "string": nomination_condition, "result": "1"})

            ##Input Nomination Seat per Seller
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "nomination_seat_per_seller"}), "string": nomination_seat, "result": "1"})

            ##Input Maximun DTS
            BaseUICore.Input({"method": "xpath", "locate": Util.GetXpath({"locate": "max_dts"}), "string": max_dts, "result": "1"})

        ##Wait button clickable then click
        time.sleep(2)
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.CheckButtonClickable({"message": "Wait create button clickable", "method": "xpath", "locate": xpath, "state": "enable", "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminFlashSalePage.CreateFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditFlashSale(arg):
        '''
        ClickEditFlashSale : Click edit flash sale
                Input argu :
                    flash_sale_name - flash sale name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        flash_sale_name = arg["flash_sale_name"]

        ##Click first flash sale edit button
        xpath = Util.GetXpath({"locate": "flashsale_edit_btn"})
        xpath = xpath.replace('replace_name', flash_sale_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ClickEditFlashSale')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProduct(arg):
        '''
        UploadProduct : upload products
                Input argu :
                            csv_file_name : upload csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        csv_file_name = arg["csv_file_name"]

        ##Upload file in input field
        BaseUILogic.UploadFileWithPath({"method": "input", "element":"step_screening_product_upload", "element_type":"id", "project":"FlashSale", "action":"file", "file_name":csv_file_name, "file_type":"csv", "result": "1"})
        time.sleep(3)

        ##Click Upload button
        xpath = Util.GetXpath({"locate": "upload_csv_btn"})
        BaseUICore.Click({"method":"xpath", "locate": xpath, "message":"Click Upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.UploadProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def HandleStagingJSError(arg):
        '''
        HandleStagingJSError : Handle if staging have JavaScript error and popup alert window
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        time.sleep(5)

        ##Error window always at HTML, so excute script tp click OK then close windows
        BaseUICore.ExecuteScript({"script": "var a = document.getElementsByClassName('ui ok button'); a[2].click();", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.HandleStagingJSError')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ApproveProduct(arg):
        '''
        ApproveProduct : Approve target product
                Input argu :
                    product_short_name - product short name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_short_name = arg["product_short_name"]

        ##Click target product approve button
        xpath = Util.GetXpath({"locate": "product_approve_btn"})
        xpath_product_approve_btn = xpath.replace("product_short_name_to_be_replace", product_short_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_product_approve_btn, "message": "Click target product approve button", "result": "1"})

        ##Check popup confirm window display
        xpath = Util.GetXpath({"locate": "popup_confirm_window"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##Click popup approve button
        xpath = Util.GetXpath({"locate": "popup_approve_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click popup approve button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ApproveProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RejectProduct(arg):
        '''
        RejectProduct : Reject target product in product screening
                Input argu :
                    product_short_name - product short name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_short_name = arg["product_short_name"]

        ##Input reject reason dropdown
        xpath = Util.GetXpath({"locate": "product_reject_reason_field"})
        xpath_product_reject_reason_field = xpath.replace("product_short_name_to_be_replace", product_short_name)
        BaseUICore.Input({"method": "xpath", "locate": xpath_product_reject_reason_field, "string": "Out of stock", "result": "1"})

        ##Click target product reject button if it exist
        xpath = Util.GetXpath({"locate": "product_reject_btn"})
        xpath_product_reject_btn = xpath.replace("product_short_name_to_be_replace", product_short_name)
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_product_reject_btn, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath_product_reject_btn, "message": "Click target product reject button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.RejectProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductImage(arg):
        '''
        UploadProductImage : Upload target product image
                Input argu :
                    product_short_name - product short name
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_short_name = arg["product_short_name"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Get product id element xpath
        xpath = Util.GetXpath({"locate": "product_id_element"})
        xpath_product_id_element = xpath.replace("product_short_name_to_be_replace", product_short_name)

        ##Get product id element text
        BaseUICore.GetElements({"method":"xpath", "locate":xpath_product_id_element, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})

        ##Get upload input id
        element_attr_id_string = "img-chooser-" + GlobalAdapter.CommonVar._PageAttributes_

        ##Upload file with path
        BaseUILogic.UploadFileWithPath({"method": "input", "element": element_attr_id_string, "element_type": "id", "project": "FlashSale", "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.UploadProductImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRefreshBtnInFlashSalePage(arg):
        '''
        ClickRefreshBtnInFlashSalePage : Click refresh btn for sync flash sale setting with core server, let flash sale can display on homepage
                Input argu :
                    number - order number of flash sale
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get element of flashsale that need to click refresh btn
        xpath = Util.GetXpath({"locate": "flashsale_status"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "flashsale_status", "result": "1"})

        ##Click each refresh btn
        for each_flashsale_id in GlobalAdapter.PromotionE2EVar._FlashSaleIDList_:
            refresh_btn = Util.GetXpath({"locate": "refresh_btn"})
            xpath = refresh_btn.replace('replace_flashsale_id', each_flashsale_id)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click refresh btn", "result": "1"})

            ##Handle sync success pop up window
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ClickRefreshBtnInFlashSalePage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassOperation(arg):
        '''
        MassOperation : Mass operation in display management
                Input argu :
                    operation_type - move_to_manual_configuration / reject / move_to_recommendation_list / toggle_on_sold_out / toggle_off_sold_out
                    number - all/numbering of product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        operation_type = arg['operation_type']
        number = arg['number']

        ##Check if there has less than one products, if yes, start rejest process, if no, skip reject process
        xpath = Util.GetXpath({"locate": "product_exist"})

        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##If we want to reject all display products
            if number == 'all':
                xpath = Util.GetXpath({"locate": "all_product_checkbox"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select all products", "result": "1"})
            ##If only need to reject one target product
            else:
                xpath = Util.GetXpath({"locate": "target_product_checkbox"})
                xpath = xpath.replace('replace_number', number)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click select numbering of product", "result": "1"})
            time.sleep(1)

            ##Choose actions of reject option
            xpath = Util.GetXpath({"locate": "action_tap"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action tap", "result": "1"})
            time.sleep(2)

            xpath = Util.GetXpath({"locate": operation_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch option", "result": "1"})

            if operation_type == "reject":
                ##Input reject reason "Out of stock"
                time.sleep(2)
                xpath = Util.GetXpath({"locate": "reason_input_box"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Out of stock", "result": "1"})

                ##Click reject button
                time.sleep(2)
                xpath = Util.GetXpath({"locate": "reject_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reject option", "result": "1"})
                time.sleep(5)

            if "sold_out" in operation_type:
                ##Handle popup alert
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        else:
            dumplogger.info('No product is under onging status, skip reject process.')

        OK(ret, int(arg['result']), 'AdminFlashSalePage.MassOperation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMoveToManualButton(arg):
        '''
        ClickMoveToManualButton : Click move product to manual configuration
                Input argu :
                    number - number of product in recommendation list
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        number = arg['number']

        ##Click move product to manual configuration
        xpath = Util.GetXpath({"locate": "move_to_manual_btn"})
        xpath_move_to_manual_btn = xpath.replace("number_to_be_replace", number)
        BaseUICore.Click({"method": "xpath", "locate": xpath_move_to_manual_btn, "message": "Click move product to manual configuration", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ClickMoveToManualButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplayOrderField(arg):
        '''
        InputDisplayOrderField : Input display order field
                Input argu :
                    value - order value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        value = arg['value']

        ##Input display order field
        xpath = Util.GetXpath({"locate": "display_order_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": value, "result": "1"})
        time.sleep(2)

        ##Click white space to save
        xpath = Util.GetXpath({"locate": "white_space"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click white space to save value", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.InputDisplayOrderField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreFlashSalePeriod(arg):
        '''
        RestoreFlashSalePeriod : Restore flashsale start time/end time to current time
                Input argu :
                    db_file - file name of sql command
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        db_file = "flash_sale_info"

        ##Get element of flashsale that need to restore
        xpath = Util.GetXpath({"locate": "flashsale_status"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "flashsale_status", "result": "1"})

        for each_flashsale_id in GlobalAdapter.PromotionE2EVar._FlashSaleIDList_:
            ##Assign each flash sale id to global first
            GlobalAdapter.CommonVar._DynamicCaseData_["promotionid"] = each_flashsale_id

            ##Get current country
            GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

            ##Define assign list
            assign_list = [{"column": "start_time", "value_type": "cur_unix_time"}, {"column": "country", "value_type": "string"}, {"column": "end_time", "value_type": "cur_unix_time"}, {"column": "promotionid", "value_type": "number"}]

            ##Send SQL command process
            DBCommonMethod.SendSQLCommandProcess({"db_name":"staging_promotion", "file_name":db_file, "method":"update", "verify_result":"", "assign_data_list":assign_list, "store_data_list":"", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.RestoreFlashSalePeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseOverlayImage(arg):
        '''
        ChooseOverlayImage : Choose overlay image
                Input argu :
                    item_id - item id which you want to choose
                    value - dropdown value need to select
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]
        value = arg["value"]

        ##Click overlay image select dropdown
        xpath = Util.GetXpath({"locate": "overlay_image_select_dropdown"})
        xpath = xpath.replace("replace_item_id", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click overlay image select dropdown", "result": "1"})
        time.sleep(5)

        ##Click action select dropdown option
        xpath = Util.GetXpath({"locate": "overlay_image_option"}).replace("value_to_be_replace", value)
        xpath = xpath.replace("replace_item_id", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click overlay image value", "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.ChooseOverlayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputModelVariationInfo(arg):
        '''
        InputModelVariationInfo : Input model variation info
                Input argu :
                    item_id - item id
                    model_id - model id
                    promo_stock - promo stock
                    promo_price - promo price
                    rebate_price - rebate price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]
        model_id = arg["model_id"]
        promo_stock = arg["promo_stock"]
        promo_price = arg["promo_price"]
        rebate_price = arg["rebate_price"]

        element_item_id_string = "new-model-for-item-" + item_id

        ##Input model id
        xpath = Util.GetXpath({"locate": "model_id_field"})
        xpath = xpath.replace("item_id_to_be_replace", element_item_id_string)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": model_id, "result": "1"})

        ##Input promo stock
        xpath = Util.GetXpath({"locate": "promo_stock_field_" + Config._TestCaseRegion_.lower()})
        xpath = xpath.replace("item_id_to_be_replace", element_item_id_string)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promo_stock, "result": "1"})

        ##Input promo price
        xpath = Util.GetXpath({"locate": "promo_price_field_" + Config._TestCaseRegion_.lower()})
        xpath = xpath.replace("item_id_to_be_replace", element_item_id_string)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promo_price, "result": "1"})

        ##Input rebate price
        xpath = Util.GetXpath({"locate": "rebate_price_field_" + Config._TestCaseRegion_.lower()})
        xpath = xpath.replace("item_id_to_be_replace", element_item_id_string)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rebate_price, "result": "1"})

        OK(ret, int(arg['result']), 'AdminFlashSalePage.InputModelVariationInfo')


class AdminVoucherDetailPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetAndSavePromotionIDFromUrl(arg):
        '''
        GetAndSavePromotionIDFromUrl : Get Promotion ID from url in voucher edit page
                Input argu :
                    voucher_type - shopee_voucher / seller_voucher / free_shipping_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]

        ##Get promotion id from url
        BaseUICore.GetBrowserCurrentUrl({"result": "1"})
        BaseUILogic.FilterStringToGlobal({"type": "promotion_url", "object": "", "result": "1"})

        ##Save it with voucher code
        if voucher_type == "shopee_voucher":
            GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[0]["id"] = GlobalAdapter.PromotionE2EVar._PromotionIDList_[0]
        elif voucher_type == "seller_voucher":
            GlobalAdapter.PromotionE2EVar._SellerVoucherList_[0]["id"] = GlobalAdapter.PromotionE2EVar._PromotionIDList_[0]
        elif voucher_type == "free_shipping_voucher":
            GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[0]["id"] = GlobalAdapter.PromotionE2EVar._PromotionIDList_[0]
        else:
            dumplogger.error("Input voucher type error, voucher type: %s" % (voucher_type))

        OK(ret, int(arg['result']), 'AdminVoucherDetailPage.GetAndSavePromotionIDFromUrl -> %s' % (GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_))

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditButton(arg):
        '''
        ClickEditButton : Click edit btn in voucher detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        ##Click edit btn
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminVoucherDetailPage.ClickEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductScopeItemIDs(arg):
        '''
        InputProductScopeItemIDs : Input Product Scope Item IDs
                Input argu :
                    items_ids - item ids to input
                    type - including / excluding
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']
        item_ids = arg['item_ids']

        ##Input product item ids
        xpath = Util.GetXpath({"locate":type + "_manual_upload_title"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click manual upload", "result": "1"})

        ##Input manual upload text with item ids
        xpath = Util.GetXpath({"locate":type + "_manual_upload_textarea"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":item_ids, "result": "1"})

        ##Click upload btn
        xpath = Util.GetXpath({"locate":type + "_manual_upload_item_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminVoucherDetailPage.InputProductScopeItemIDs')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLabelIds(arg):
        '''
        InputLabelIds : Input product scope product label IDs
                Input argu :
                    scope_type - including / excluding
                    label_ids - label ids you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope_type = arg['scope_type']
        label_ids = arg['label_ids']

        if scope_type == "including":
            ##Click product scope criteria radio button
            xpath = Util.GetXpath({"locate": "product_scope_crit_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product scope criteria radio button", "result": "1"})

        ##Input label id
        xpath = Util.GetXpath({"locate":scope_type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":label_ids, "result": "1"})

        OK(ret, int(arg['result']), 'AdminVoucherDetailPage.InputLabelIds -> ' + scope_type)


class AdminSellerVoucherButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save btn in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save btn in voucher configuration page
        xpath = Util.GetXpath({"locate":"save_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button in voucher configuration page", "result": "1"})

        OK(ret, int(arg['result']), "AdminSellerVoucherButton.ClickSave")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button in voucher configuration page
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable button in seller voucher list page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click disable button in seller voucher list page
        xpath = Util.GetXpath({"locate": "disable_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable button in seller voucher list page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherButton.ClickDisable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
        ClickUpload : Click upload button in voucher configuration page
                Input argu :
                    type : including / excluding
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click upload button
        xpath = Util.GetXpath({"locate": type + "_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherButton.ClickUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDispatch(arg):
        '''
        ClickDispatch : Click upload user tag & dispatch
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload user tag & dispatch button
        xpath = Util.GetXpath({"locate": "dispatch_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload user tag and dispatch button", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherButton.ClickDispatch')


class AdminSellerVoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNewVoucherButton(arg):
        '''
        ClickCreateNewVoucherButton : Click create new voucher button
                Input argu :
                    reward_type - product_discount, coin_cashback
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reward_type = arg["reward_type"]

        ##Click create new voucher button
        xpath = Util.GetXpath({"locate": "new_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new button", "result": "1"})

        ##Default is set to product discount
        if reward_type != "product_discount":
            xpath = Util.GetXpath({"locate": "voucher_reward"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reward dropdown", "result": "1"})

            xpath = Util.GetXpath({"locate": reward_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reward type" + reward_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherPage.ClickCreateNewVoucherButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherPrefixCode(arg):
        '''
        InputVoucherPrefixCode : Input voucher prefix code
                Input argu :
                    prefix - prefix code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        prefix = arg['prefix']

        ##if no prefix code get, auto generate one
        if not prefix:
            prefix = XtFunc.GenerateRandomString(8, "characters")
            GlobalAdapter.PromotionE2EVar._SellerVoucherList_.append({"code": prefix, "id": ""})

        ##Input prefix code
        xpath = Util.GetXpath({"locate": "prefix_code"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": prefix, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.InputVoucherPrefixCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherBasicRule(arg):
        '''
        InputVoucherBasicRule : Input voucher basic rule
                Input argu :
                    shop_id - shop id
                    quota_type - claim / usage
                    usage_quota - usage quota
                    claim_quota - claim quota
                    start_time - start time
                    end_time - end time
                    more_detail - voucher more detail message
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg['shop_id']
        quota_type = arg['quota_type']
        usage_quota = arg['usage_quota']
        claim_quota = arg['claim_quota']
        start_time = arg['start_time']
        end_time = arg['end_time']
        more_detail = arg['more_detail']

        ##Input shopid
        if shop_id:
            xpath = Util.GetXpath({"locate": "shop_id"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        ##Input voucher name "TWQA"
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "TWQA", "result": "1"})

        ##Select voucher quota type
        if quota_type:
            xpath = Util.GetXpath({"locate": "quota_type_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher quota type dropdown", "result": "1"})
            xpath = Util.GetXpath({"locate": quota_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher quota type dropdown option", "result": "1"})

        ##Input voucher usage quota if you need it
        if usage_quota:
            xpath = Util.GetXpath({"locate": "usage_quota_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": usage_quota, "result": "1"})

        ##Input voucher claim quota
        if claim_quota:
            xpath = Util.GetXpath({"locate": "claim_quota_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": claim_quota, "result": "1"})

        ##Input start time
        if start_time:
            input_start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
            xpath = Util.GetXpath({"locate": "start_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_start_time, "result": "1"})

        ##Input end time
        if end_time:
            input_end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))
            xpath = Util.GetXpath({"locate": "end_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_end_time, "result": "1"})

        ##Input more detail message
        if more_detail:
            xpath = Util.GetXpath({"locate": "more_detail_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": more_detail, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.InputVoucherBasicRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductDiscountInfo(arg):
        '''
        InputProductDiscountInfo : Input product discount info
                Input argu :
                    discount_value - discount value
                    discount_percentage - discount percentage
                    max_amount - max valid voucher amount
                    minimum_basket - minimum basket size
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        discount_value = arg["discount_value"]
        discount_percentage = arg['discount_percentage']
        max_amount = arg['max_amount']
        minimum_basket = arg["minimum_basket"]

        ##Input discount
        if discount_percentage:
            xpath = Util.GetXpath({"locate": "discount_percentage"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": discount_percentage, "result": "1"})

        ##Input discount value
        if discount_value:
            xpath = Util.GetXpath({"locate": "discount_value"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": discount_value, "result": "1"})

        ##Input max valid amount
        if max_amount:
            xpath = Util.GetXpath({"locate": "max_valid_amount"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_amount, "result": "1"})

        ##Input minimum basket
        if minimum_basket:
            xpath = Util.GetXpath({"locate": "minimum_basket"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": minimum_basket, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.InputProductDiscountInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCoinCashbackInfo(arg):
        '''
        InputCoinCashbackInfo : Input coin cashback info
                Input argu :
                    coins_cashback_rate - percent cashback
                    coins_cashback_cap - caps of coins
                    minimum_basket - minimum basket
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        coins_cashback_rate = arg['coins_cashback_rate']
        coins_cashback_cap = arg['coins_cashback_cap']
        minimum_basket = arg["minimum_basket"]

        ##Input coins cashback rate
        xpath = Util.GetXpath({"locate": "coins_cashback_rate"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": coins_cashback_rate, "result": "1"})

        ##Input coins cashback cap
        xpath = Util.GetXpath({"locate": "coins_cashback_cap"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": coins_cashback_cap, "result": "1"})

        ##Input minimum basket
        xpath = Util.GetXpath({"locate": "minimum_basket"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": minimum_basket, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.InputCoinCashbackInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductScope(arg):
        '''
        InputProductScope : Input product scope section
                Input argu :
                    type - any_product / criteria
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click type
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product scope " + type, "result": "1"})

        OK(ret, int(arg['result']), "AdminSellerVoucherPage.InputProductScope")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputUserScope(arg):
        '''
        InputUserScope : Input user scope section
                Input argu :
                    user_tags - user tags entry should have two parts - attribute key and attribute value (example: [attribute key 1, attribute value 1],[attribute key 2, attribute value 2],...)
                    error_title - custom error message title
                    error_content - custom error message content
                    link - custom link
                    link_button_text - custom link button text
                    others - manual_dispatch / shop_members / targeted_shop_buyer
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_tags = arg['user_tags']
        error_title = arg['error_title']
        error_content = arg['error_content']
        link = arg['link']
        link_button_text = arg['link_button_text']
        others = arg['others']

        ##Click All users checkbox
        xpath = Util.GetXpath({"locate": "all_users"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Scope of Users All users checkbox", "result": "1"})

        ##Set up user tags
        if user_tags:
            ##Click by user tags checkbox
            xpath = Util.GetXpath({"locate":"user_tag_checkbox"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click by user tags checkbox", "result": "1"})

            ##Input user tags
            xpath = Util.GetXpath({"locate": "user_tag_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_tags, "result": "1"})

        ##Input message title
        if error_title:
            ##Input message title region
            xpath = Util.GetXpath({"locate": "msg_title_reg_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_title, "result": "1"})

            ##Input message title english
            xpath = Util.GetXpath({"locate": "msg_title_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_title, "result": "1"})

        ##Input message content
        if error_content:
            ##Input message content region
            xpath = Util.GetXpath({"locate": "msg_content_reg_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_content, "result": "1"})

            ##Input message content english
            xpath = Util.GetXpath({"locate": "msg_content_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_content, "result": "1"})

        ##Set up custom link
        if link:
            ##Click custom link radio button
            xpath = Util.GetXpath({"locate":"custom_link_radio_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click custom link radio button", "result": "1"})

            ##Input link
            xpath = Util.GetXpath({"locate": "link_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link, "result": "1"})

        ##Input link button text
        if link_button_text:
            ##Input link button text region
            xpath = Util.GetXpath({"locate": "link_btn_text_reg_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link_button_text, "result": "1"})

            ##Input link button text english
            xpath = Util.GetXpath({"locate": "link_btn_text_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link_button_text, "result": "1"})

        ##Click other radio button in user scope section
        if others:
            xpath = Util.GetXpath({"locate": others})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s" % (others), "result": "1"})

        OK(ret, int(arg['result']), "AdminSellerVoucherPage.InputUserScope")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFEDisplaySetting(arg):
        '''
        InputFEDisplaySetting : Input Seller Voucher FE display setting
                Input argu :
                    first_customised_label_region - first customised label region display text
                    first_customised_label_en - first customised label english display text
                    second_customised_label_region - second customised label region display text
                    second_customised_label_en - second customised label english display text
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        first_customised_label_region = arg['first_customised_label_region']
        first_customised_label_en = arg['first_customised_label_en']
        second_customised_label_region = arg['second_customised_label_region']
        second_customised_label_en = arg['second_customised_label_en']

        ##Default is default link, set to custom link if custom link is given
        if first_customised_label_region or first_customised_label_en:
            ##Click first customised label checkbox
            xpath = Util.GetXpath({"locate": "first_customised_label_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first customised label checkbox", "result": "1"})

            if first_customised_label_region:
                ##Input first customised region (Some country don't have regional language)
                xpath_first_customised_region_field = Util.GetXpath({"locate": "first_customised_region_field"})
                BaseUICore.Input({"method": "xpath", "locate": xpath_first_customised_region_field, "string": first_customised_label_region, "result": "1"})

            ##Input first customised english
            xpath = Util.GetXpath({"locate": "first_customised_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": first_customised_label_en, "result": "1"})

        if second_customised_label_region or second_customised_label_en:
            ##Click second customised label checkbox
            xpath = Util.GetXpath({"locate": "second_customised_label_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click second customised label checkbox", "result": "1"})

            if second_customised_label_region:
                ##Input second customised region (Some country don't have regional language)
                xpath_second_customised_region_field = Util.GetXpath({"locate": "second_customised_region_field"})
                BaseUICore.Input({"method": "xpath", "locate": xpath_second_customised_region_field, "string": second_customised_label_region, "result": "1"})

            ##Input second customised english
            xpath = Util.GetXpath({"locate": "second_customised_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": second_customised_label_en, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.InputFEDisplaySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisplaySetting(arg):
        '''
        ClickDisplaySetting : Choose Seller Voucher display setting
                Input argu :
                    type - default / specific_channel / do_not_display
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click display setting type
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + type, "result": "1"})

        if type == "specific_channel":
            xpath = Util.GetXpath({"locate":"feed_checkbox"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click feed checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.ClickDisplaySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllowedDistributionMethod(arg):
        '''
        ClickAllowedDistributionMethod : Choose Allowed Distribution Method type
                Input argu :
                    type - any / only_dispatch
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Choose Allowed Distribution Method type
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click " + type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.ClickAllowedDistributionMethod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogisticsCheckBox(arg):
        '''
        ClickLogisticsCheckBox : Click logistics checkbox
                Input argu :
                    logistic - for VN region: giao_Nhanh / vnpost / jt_express / grab / nowship / best_express / standard_express_doora /
                                viettel_post / giao_htk / vnpost_nhanh / shopee_express / shopee_24h / ninja_van / shopee_4h /
                                standard_express_lwe / standard_express
                                for MX region: estandar_rapido_product
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        logistic = arg["logistic"]

        ##Click logistics checkbox
        xpath = Util.GetXpath({"locate": logistic})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click logistics checkbox => " + logistic, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherPage.ClickLogisticsCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeviceCheckBox(arg):
        '''
        ClickDeviceCheckBox : Click device checkbox
                Input argu :
                    device - all_devices / iOS / android / web
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        device = arg["device"]

        ##Click device checkbox
        xpath = Util.GetXpath({"locate": device})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click device checkbox => " + device, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminSellerVoucherPage.ClickDeviceCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadGenerateVoucherCSV(arg):
        '''
        UploadGenerateVoucherCSV : Upload csv for mass generate csv
                Input argu :
                    shop_id - shop id for this voucher
                    file_name - file name you want to upload
                    start_time - voucher start time
                    end_time - voucher end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        template_csv_data = []
        voucher_list = []
        shop_id = arg['shop_id']
        file_name = arg['file_name']
        start_time = arg['start_time']
        end_time = arg['end_time']

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Get file path
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"
        new_file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + "temp_" + file_name + ".csv"

        ##Save template csv data for generate new data later
        with open(file_path, 'rb') as csv_file:
            csv_reader = csv.reader(codecs.EncodedFile(csv_file, 'utf8', 'utf_8_sig'))
            ##Read header first
            header = csv_reader.next()
            ##Append template csv data into list
            for row in csv_reader:
                template_csv_data.append(row)

        ##generate voucher code
        for index in range(len(template_csv_data)):
            voucher_list.append({"code": XtFunc.GenerateRandomString(8, "characters"), "id": ""})

        ##Write csv data to upload later
        with open(new_file_path, 'wb') as upload_file:
            writer = csv.writer(upload_file)

            ##write csv header
            writer.writerow(header)

            ##Assemble all
            for index, data in enumerate(template_csv_data):
                ##Replace shop id
                data = ','.join(data).replace(r"{shop_id}", shop_id).split(',')
                ##Replace vcode
                data = ','.join(data).replace((r"{vcode%s}" % str(index + 1)), voucher_list[index]["code"]).split(',')
                ##Replace start_time
                data = ','.join(data).replace(r"{start_time}", start_time).split(',')
                ##Replace end_time
                data = ','.join(data).replace(r"{end_time}", end_time).split(',')
                ##write replaced data
                writer.writerow(data)

        ##Upload file
        xpath = Util.GetXpath({"locate":"upload_file"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_file_path, "result": "1"})

        ##Click upload btn
        xpath = Util.GetXpath({"locate":"upload_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload btn", "result": "1"})

        ##Save voucher code into Global list
        GlobalAdapter.PromotionE2EVar._SellerVoucherList_ = voucher_list

        ##remove temp file
        os.remove(new_file_path)

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.UploadGenerateVoucherCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductScopeFile(arg):
        '''
        UploadProductScopeFile : Upload product scope file
                Input argu :
                    section - including / excluding
                    upload_file_name - upload file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg['section']
        upload_file_name = arg['upload_file_name']
        file_type = arg['file_type']

        ##Click batch import CSV radio button
        xpath = Util.GetXpath({"locate": section + "_batch_import_csv_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch import CSV radio button", "result": "1"})

        ##Click choose file button
        xpath = Util.GetXpath({"locate": section + "_choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})

        ##Upload file with opencv
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": upload_file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.UploadProductScopeFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadDispatchUserTag(arg):
        '''
        UploadDispatchUserTag : Upload Dispatch User Tag
                Input argu :
                    user_tag - user tag (ex: [attribute key, attribute value])
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_tag = arg['user_tag']

        ##Click dispatch on top right
        xpath = Util.GetXpath({"locate": "dispatch_btn_top_right"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dispatch on top right", "result": "1"})

        ##Input user tag
        xpath = Util.GetXpath({"locate": "tag_input"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":user_tag, "result": "1"})

        ##Click dispatch to submit
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dispatch to submit", "result": "1"})

        ##Handle popup
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.UploadDispatchUserTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchVoucher(arg):
        '''
        SearchVoucher : search voucher with voucher name
                Input argu :
                    seller_name - voucher seller name you want to search
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        seller_name = arg["seller_name"]

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##Search voucher with voucher name
        xpath = Util.GetXpath({"locate": "search_voucher_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": seller_name, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerVoucherPage.SearchVoucher')


class AdminShopeeVoucherButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherListEdit(arg):
        '''
        ClickVoucherListEdit : Click edit button in voucher list page
                Input argu :
                    voucher_name - voucher name you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click edit button in voucher list page
        xpath = Util.GetXpath({"locate": "voucher_edit_btn"})
        xpath_voucher_edit_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_voucher_edit_btn, "message": "Click edit button in voucher list page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickVoucherListEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove button", "result": "1"})

        ##Handle pop up
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownload(arg):
        '''
        ClickDownload : Click download button in voucher configuration page
                Input argu :
                    button_type - include_shopid / include_itemid / exclude_shopid / exclude_itemid
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click download button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download button => " + button_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherButton.ClickDownload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button in voucher configuration page
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnable(arg):
        '''
        ClickEnable : Click enable button in admin shopee voucher page
                Input argu :
                    button_type - normal_voucher / generate_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click enable button in admin shopee voucher page
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable button in admin shopee voucher page => " + button_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickEnable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisable(arg):
        '''
        ClickDisable : Click disable button in admin shopee voucher page
                Input argu :
                    button_type - normal_voucher / generate_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click disable button in admin shopee voucher page
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable button in admin shopee voucher page => " + button_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickDisable')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button in voucher generate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button in voucher generate page
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button in voucher generate page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export button in voucher generate page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export button in voucher generate page
        xpath = Util.GetXpath({"locate": "export_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export button in voucher generate page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExportSelectedImages(arg):
        '''
        ClickExportSelectedImages : Click export select image button in image uploader page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click export button in voucher generate page
        xpath = Util.GetXpath({"locate": "export_image_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export select image button in image uploader page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickExportSelectedImages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickViewVouchers(arg):
        '''
        ClickViewVouchers : Click view vouchers button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click view vouchers button in voucher configuration page
        xpath = Util.GetXpath({"locate": "view_vouchers_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view vouchers button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickViewVouchers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGenerateVouchers(arg):
        '''
        ClickGenerateVouchers : Click generate vouchers button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click generate vouchers button in voucher configuration page
        xpath = Util.GetXpath({"locate": "generate_vouchers_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click generate vouchers button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickGenerateVouchers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
        ClickUpload : Click upload button in voucher upload user page
                Input argu :
                    button_type - upload_import_user / upload_remove_user
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click cancel button in voucher generate page
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button in voucher upload user page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBack(arg):
        '''
        ClickBack : Click back button in view vouchers page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click back button in view vouchers page
        xpath = Util.GetXpath({"locate": "back_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click back button in view vouchers page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickBack')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLinkedCheckout(arg):
        '''
        ClickLinkedCheckout : Click linked checkout in view vouchers page
                Input argu :
                    index - link checkout button index
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        index = arg['index']

        ##Click linked checkout in view vouchers page
        xpath = Util.GetXpath({"locate": "linked_checkout_btn"})
        xpath_linked_checkout_btn = xpath.replace("index_to_be_replace", index)
        BaseUICore.Click({"method": "xpath", "locate": xpath_linked_checkout_btn, "message": "Click linked checkout in view vouchers page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickLinkedCheckout')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherConfigEdit(arg):
        '''
        ClickVoucherConfigEdit : Click edit button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit button in voucher configuration page
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherButton.ClickVoucherConfigEdit')


class AdminShopeeVoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadGenerateVoucherCSV(arg):
        '''
        UploadGenerateVoucherCSV : Upload csv for mass generate csv
                Input argu :
                    start_time - start time of voucher
                    end_time - end time of voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        line_count = 0
        template_csv_data = []
        voucher_list = []
        file_name = arg['file_name']
        start_time = arg['start_time']
        end_time = arg['end_time']

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Get file path
        slash = Config.dict_systemslash[Config._platform_]
        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"
        new_file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + "temp_" + file_name + ".csv"

        ##Get template data for generate data later
        with open(file_path, 'rb') as csv_file:
            csv_reader = csv.reader(codecs.EncodedFile(csv_file, 'utf8', 'utf_8_sig'))
            ##Read header first
            header = csv_reader.next()
            ##save tempate csv data to list
            for row in csv_reader:
                template_csv_data.append(row)
                if 'private' in row:
                    dumplogger.info("this public voucher is for private use so no need to manually apply")
                else:
                    line_count += 1

        ##generate voucher code in voucher list
        for index in range(len(template_csv_data)):
            voucher_list.append({"id": "", "code": XtFunc.GenerateRandomString(8, "characters")})

        ##Write csv data to upload later
        with open(new_file_path, 'wb') as upload_file:
            writer = csv.writer(upload_file)

            ##Write header first
            writer.writerow(header)

            for row_index, csv_data_row in enumerate(template_csv_data):
                csv_write_row_data = []

                ##Loop each csv row cell
                for csv_data_cell in csv_data_row:
                    ##Append vcode
                    if csv_data_cell == r"{vcode%s}" % str(row_index + 1):
                        csv_write_row_data.append(voucher_list[row_index]["code"])
                    ##Append start_time
                    elif csv_data_cell == r"{start_time}":
                        csv_write_row_data.append(start_time)
                    ##Append end_time
                    elif csv_data_cell == r"{end_time}":
                        csv_write_row_data.append(end_time)
                    ##Append other csv data
                    else:
                        csv_write_row_data.append(csv_data_cell)

                ##Write csv row
                writer.writerow(csv_write_row_data)

        ##Click mass generate shopee vouchers button
        xpath = Util.GetXpath({"locate": "mass_generate_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass generate shopee vouchers button", "result": "1"})

        ##Upload file
        xpath = Util.GetXpath({"locate":"upload_file"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_file_path, "result": "1"})

        ##Click upload btn
        xpath = Util.GetXpath({"locate":"upload_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click upload btn", "result": "1"})

        ##Only save public voucher code in global voucher list
        GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_ = voucher_list[:line_count]

        ##remove temp file
        os.remove(new_file_path)

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadGenerateVoucherCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadVoucherImage(arg):
        '''
        UploadVoucherImage : Upload voucher image in admin voucher image uploader page
                Input argu :
                    voucher_image_name - voucher image name you want to set up
                    file_name - voucher image file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_image_name = arg['voucher_image_name']
        file_name = arg['file_name']

        ##Click upload image button
        xpath = Util.GetXpath({"locate": "upload_image_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload image button", "result": "1"})

        ##Input voucher image name
        xpath = Util.GetXpath({"locate":"image_name_field"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":voucher_image_name, "result": "1"})

        ##Click choose image file button
        xpath = Util.GetXpath({"locate": "choose_image_file_btn"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

        ##Sleep 5 second to wait upload window is appear
        time.sleep(5)

        ##Upload voucher image file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": file_name, "file_type": "png", "result": "1"})

        ##Sleep 5 second to wait image is shown on FE
        time.sleep(5)

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        ##Handle popup alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadVoucherImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogisticsCheckBox(arg):
        '''
        ClickLogisticsCheckBox : Click logistics checkbox
                Input argu :
                            logistic - for VN region: giao_Nhanh / vnpost / jt_express / grab / nowship / best_express / standard_express_doora /
                                       viettel_post / giao_htk / vnpost_nhanh / shopee_express / shopee_24h / ninja_van / shopee_4h /
                                       standard_express_lwe / standard_express
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        logistic = arg["logistic"]

        ##Click logistics checkbox
        xpath = Util.GetXpath({"locate": logistic})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click logistics checkbox => " + logistic, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickLogisticsCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeviceCheckBox(arg):
        '''
        ClickDeviceCheckBox : Click device checkbox
                Input argu :
                            device - all_devices / iOS / android / web
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        device = arg["device"]

        ##Click device checkbox
        xpath = Util.GetXpath({"locate": device})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click device checkbox => " + device, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickDeviceCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisplayLabelCheckbox(arg):
        '''
        ClickDisplayLabelCheckbox : Click display label checkbox
                Input argu :
                            label_option - new_user / shopee_pay / custom_1 / custom_2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        label_option = arg["label_option"]

        ##Click display label checkbox
        xpath = Util.GetXpath({"locate": label_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click display label checkbox => " + label_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickDisplayLabelCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentMethodCheckbox(arg):
        '''
        ClickPaymentMethodCheckbox : Click payment method checkbox
                Input argu :
                            payment_option - airpay_wallet / airpay_GIRO / credit_card / credit_card_exclude_installment / credit_card_installment
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        payment_option = arg["payment_option"]

        ##Click payment method checkbox
        xpath = Util.GetXpath({"locate": payment_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment method checkbox => " + payment_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickPaymentMethodCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUserValidityCheckBox(arg):
        '''
        ClickUserValidityCheckBox : Click user validity checkbox
                Input argu :
                            valid_option - register_time / order_live / order_cancel / user_phone_number / user_tag
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        valid_option = arg["valid_option"]

        ##Click user validity checkbox
        xpath = Util.GetXpath({"locate": valid_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + valid_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickUserValidityCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickImageUploaderCheckBox(arg):
        '''
        ClickImageUploaderCheckBox : Click image checkbox
                Input argu :
                    image_id - 1/2/3  your image uploader image id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_id = arg["image_id"]

        ##Click image id checkbox
        xpath = Util.GetXpath({"locate": "image_checkbox"})
        xpath = xpath.replace("replace_id", image_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click image id checkbox => " + image_id, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickImageUploaderCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickScopeUserRadioButton(arg):
        '''
        ClickScopeUserRadioButton : Click scope user radio button
                Input argu :
                            user_option - criteria / all_user / upload_user_id / upload_user_tag
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_option = arg["user_option"]

        ##Click scope user radio button
        xpath = Util.GetXpath({"locate": user_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click scope user radio button => " + user_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickScopeUserRadioButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPaymentMethodRadioButton(arg):
        '''
        ClickPaymentMethodRadioButton : Click payment method radio button
                Input argu :
                            method_option - all / specific
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        method_option = arg["method_option"]

        ##Click payment method radio button
        xpath = Util.GetXpath({"locate": method_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click payment method radio button => " + method_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickPaymentMethodRadioButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLogisticPromotionRuleRadioButton(arg):
        '''
        ClickLogisticPromotionRuleRadioButton : Click logistic promotion rule radio button
                Input argu :
                            rule_option - all / work_rule
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        rule_option = arg["rule_option"]

        ##Click logistic promotion rule radio button
        xpath = Util.GetXpath({"locate": rule_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click logistic promotion rule radio button => " + rule_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickLogisticPromotionRuleRadioButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherUILinkRadioButton(arg):
        '''
        ClickVoucherUILinkRadioButton : Click voucher UI link radio button
                Input argu :
                            link_option - default / custom
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_option = arg["link_option"]

        ##Click voucher UI link radio button
        xpath = Util.GetXpath({"locate": link_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher UI link radio button => " + link_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickVoucherUILinkRadioButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherUIHeaderRadioButton(arg):
        '''
        ClickVoucherUIHeaderRadioButton : Click voucher UI header radio button
                Input argu :
                            header_option - default / custom
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        header_option = arg["header_option"]

        ##Click voucher UI header radio button
        xpath = Util.GetXpath({"locate": header_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher UI header radio button => " + header_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickVoucherUIHeaderRadioButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickScopeTagRadioButton(arg):
        '''
        ClickScopeTagRadioButton : Click scope tag radio button
                Input argu :
                            tag_option - no_tag / custom
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tag_option = arg["tag_option"]

        ##Click scope tag radio button
        xpath = Util.GetXpath({"locate": tag_option})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click scope tag radio button => " + tag_option, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickScopeTagRadioButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCustomisedProductScopeTag(arg):
        '''
        InputCustomisedProductScopeTag : Input Customised Product Scope Tag
                Input argu :
                            local_tag - customised tag name in local language
                            english_tag - customised tag name in english
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        local_tag = arg["local_tag"]
        english_tag = arg["english_tag"]

        ##Input local tag
        if local_tag:
            xpath = Util.GetXpath({"locate": "local_tag_block"})
            if Config._TestCaseRegion_ == "MX":
                xpath = xpath.replace("country", "es_mx")
            else:
                xpath = xpath.replace("country", Config._TestCaseRegion_.lower())
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": local_tag, "result": "1"})

        ##Input english tag
        if english_tag:
            xpath = Util.GetXpath({"locate": "english_tag_block"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": english_tag, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.InputCustomisedProductScopeTag')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCustomisedProductScopeTagID(arg):
        '''
        InputCustomisedProductScopeTagID : Input Customised Product Scope Tag id
                Input argu :
                    image_id - customised product scope tag id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_id = arg["image_id"]

        ##Input image id
        xpath = Util.GetXpath({"locate": "product_scope_tags_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_id, "result": "1"})
        time.sleep(3)
        ##kebord press enter
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.InputCustomisedProductScopeTagID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNewVoucherButton(arg):
        '''
        ClickCreateNewVoucherButton : Click create new voucher button
                Input argu : voucher_type - public / private
                             reward_type - product_discount / coin_cashback / free_shipping
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]
        reward_type = arg["reward_type"]

        ##Click create new voucher button
        xpath = Util.GetXpath({"locate": "new_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new button", "result": "1"})

        ##Handle pop up
        #BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        ##Default is set to public
        if voucher_type == "private":
            ##Click type dropdown
            xpath = Util.GetXpath({"locate": "voucher_type"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click type dropdown", "result": "1"})

            ##Click private in dropdown
            xpath = Util.GetXpath({"locate": "type_private"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click voucher type - private", "result": "1"})

        ##Default is set to product discount
        if reward_type != "product_discount":
            xpath = Util.GetXpath({"locate": "voucher_reward"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reward dropdown", "result": "1"})

            xpath = Util.GetXpath({"locate": reward_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reward type" + reward_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickCreateNewVoucherButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click save button in admin voucher page
                Input argu :
                            button_type - normal_voucher / generate_voucher
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']

        ##Click save button in admin voucher page
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button in admin voucher page => " + button_type, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDispatchButton(arg):
        '''
        ClickDispatchButton : Click dispatch button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click dispatch button in voucher configuration page
        xpath = Util.GetXpath({"locate": "dispatch_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dispatch button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickDispatchButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadUserButton(arg):
        '''
        ClickUploadUserButton : Click upload user button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload user button in voucher configuration page
        xpath = Util.GetXpath({"locate": "upload_user_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload user button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickUploadUserButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPopupYesButton(arg):
        '''
        ClickPopupYesButton : Click popup yes button in voucher configuration page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click popup yes button
        xpath = Util.GetXpath({"locate": "popup_yes_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click popup yes button in voucher configuration page", "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.ClickPopupYesButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadFileLink(arg):
        '''
        ClickDownloadFileLink : Click download file link in admin voucher upload user id page
                Input argu :
                        type - error_file / export_file
                        index - index of download error file link
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']
        index = arg['index']

        ##Click download file link in admin voucher upload user id page
        xpath = Util.GetXpath({"locate": type})
        xpath_download_err_file_link = xpath.replace("index_to_be_replace", index)
        BaseUICore.Click({"method": "xpath", "locate": xpath_download_err_file_link, "message": "Click download file link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ClickDownloadFileLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProceed(arg):
        '''
        ClickProceed : Click proceed button if popup category warning after click save
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click proceed button if popup category warning
        xpath = Util.GetXpath({"locate": "proceed_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click proceed button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ClickProceed')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCodingMonkeyButton(arg):
        '''
        ClickCodingMonkeyButton : Click CodingMonkey button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click CodingMonkey button
        xpath = Util.GetXpath({"locate": "coding_monkey_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click codingmonkey button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ClickCodingMonkeyButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductScopeTagButton(arg):
        '''
        ClickProductScopeTagButton : Click Product Scope Tag button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ProductScopeTag button
        xpath = Util.GetXpath({"locate": "product_scope_tag_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Product Scope Tag button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ClickProductScopeTagButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadImageButton(arg):
        '''
        ClickUploadImageButton : Click upload image button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload image button
        xpath = Util.GetXpath({"locate": "upload_image_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Upload Image button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ClickUploadImageButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadTemplateFileButton(arg):
        '''
        ClickDownloadTemplateFileButton : Click download template file button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download template button
        xpath = Util.GetXpath({"locate": "download_template_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download template file button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ClickDownloadTemplateFileButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputValidEndTimeField(arg):
        '''
        InputValidEndTimeField : Input valid end time field
                Input argu :
                            end_time - voucher valid end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        end_time = arg['end_time']

        ##Prepare datetime string
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Input valid end time
        xpath = Util.GetXpath({"locate": "valid_end_time_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.InputValidEndTimeField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLogisticPromotionRuleField(arg):
        '''
        InputLogisticPromotionRuleField : Input logistic promotion rule field
                Input argu :
                            string - string you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        string = arg['string']

        ##Get promotion rule input field XPath
        xpath = Util.GetXpath({"locate": "promotion_rule_field"})

        if string:
            ##Input promotion rule id from XML string
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": string, "result": "1"})
        else:
            ##Input promotion rule id from global _PromotionID_
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PromotionE2EVar._PromotionIDList_[0], "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.InputLogisticPromotionRuleField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFirstDisplayLabelField(arg):
        '''
        InputFirstDisplayLabelField : Input first display label field
                Input argu :
                            string - string you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        string = arg['string']

        ##Input first display label field (Region)
        xpath = Util.GetXpath({"locate": "first_display_label_field_region"})
        xpath = xpath.replace("region_to_be_replaced", Config._TestCaseRegion_.lower())
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": string, "result": "1"})

        ##Input first display label field (EN)
        xpath = Util.GetXpath({"locate": "first_display_label_field_en"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": string, "result": "1"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.InputFirstDisplayLabelField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFreeShippingVoucherInfo(arg):
        '''
        InputFreeShippingVoucherInfo : Input free shipping info
                Input argu :
                    voucher_name - free shipping voucher name
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]

        ##Input name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})

        ##Input T&C landing page product
        xpath = Util.GetXpath({"locate": "usage_term_product"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "For TWQA testing purpose", "result": "1"})

        ##Input T&C landing page more details
        xpath = Util.GetXpath({"locate": "usage_term"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "For TWQA testing purpose", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.InputFreeShippingVoucherInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductDiscountBasicRule(arg):
        '''
        InputProductDiscountBasicRule : Input product discount basic rule
                Input argu :
                    voucher_name - voucher name
                    prefix - voucher prefix code
                    suffix - suffix length
                    max_voucher_usage - max voucher usage
                    discount - discount value
                    minimum - minimum basket
                    start_time - start time
                    end_time - end time
                    discount_percentage - discount percentage
                    max_amount - max valid amount
                    tw_time  - use tw time
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        voucher_name = arg['voucher_name']
        prefix = arg["prefix"]
        suffix = arg["suffix"]
        max_voucher_usage = arg["max_voucher_usage"]
        discount = arg["discount"]
        minimum = arg["minimum"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        discount_percentage = arg['discount_percentage']
        max_amount = arg['max_amount']
        tw_time = arg['tw_time']

        ##Prepare datetime string
        if tw_time == '1':
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time), is_tw_time=1)
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time), is_tw_time=1)
        else:
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Input name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})

        ##Input max voucher usage
        if max_voucher_usage:
            xpath = Util.GetXpath({"locate": "usage_limit"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_voucher_usage, "result": "1"})

        ##Input prefix code
        xpath = Util.GetXpath({"locate": "prefix_code"})
        if not prefix:
            ##if no prefix code get, auto generate one
            prefix = XtFunc.GenerateRandomString(8, "characters")
            GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"id": "", "code": prefix})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": prefix, "result": "1"})

        ##Input suffix length
        if suffix:
            xpath = Util.GetXpath({"locate": "suffix_length"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": suffix, "result": "1"})

        ##Input discount
        if discount_percentage != "0":
            xpath = Util.GetXpath({"locate": "discount_percentage"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": discount_percentage, "result": "1"})

        ##Input discount
        if discount_percentage == "0":
            xpath = Util.GetXpath({"locate": "discount_value"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": discount, "result": "1"})
        else:
            ##Input max valid amount
            xpath = Util.GetXpath({"locate": "max_valid_amount"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_amount, "result": "1"})

        ##Input minimum basket
        xpath = Util.GetXpath({"locate": "minimum_basket"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": minimum, "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.InputProductDiscountBasicRule, Voucher code -> ' + prefix)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputVoucherTimeRule(arg):
        '''
        InputVoucherTimeRule : Input voucher time rule
                Input argu :
                    start_time - start time
                    end_time - end time
                    tw_time - use tw time

                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        tw_time = arg['tw_time']

        ##Prepare datetime string
        if tw_time == '1':
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time), is_tw_time=1)
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time), is_tw_time=1)
        else:
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.InputVoucherTimeRule ')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPublicUserScope(arg):
        '''
        InputPublicUserScope : Input public voucher user scope
                Input argu :
                    type - all_users / register_time / order_live / order_cancel / user_phone_number / user_tag
                    register_start_time - register start time
                    register_end_time - register end time
                    orders_operator - greater / less / equal
                    order_live_number - Number of orders live (to pay, to receive, completed)
                    order_cancel_number - Number of orders cancelled (cancelled, return and refunded)
                    user_tag - user tag (example: [attribute key 1, attribute value 1],[attribute key 2, attribute value 2],...)
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']
        register_start_time = arg['register_start_time']
        register_end_time = arg['register_end_time']
        orders_operator = arg['orders_operator']
        order_live_number = arg['order_live_number']
        order_cancel_number = arg['order_cancel_number']
        user_tag = arg['user_tag']

        if type == "all_users":
            ##Click all user checkbox
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + type, "result": "1"})

        elif type == "register_time":
            ##Click register time checkbox
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + type, "result": "1"})

            ##Input register start time
            xpath = Util.GetXpath({"locate": "register_start_time_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": register_start_time, "result": "1"})

            ##Input register end time
            xpath = Util.GetXpath({"locate": "register_end_time_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": register_end_time, "result": "1"})

        elif type == "order_live":
            ##Click order live checkbox
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + type, "result": "1"})

            ##Click order live operator dropdown
            xpath = Util.GetXpath({"locate": "order_live_operator_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order live operator dropdown", "result": "1"})

            ##Click order live operator dropdown option
            xpath = Util.GetXpath({"locate": orders_operator})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order live operator dropdown option => " + orders_operator, "result": "1"})

            ##Input order live number
            xpath = Util.GetXpath({"locate": "order_live_number_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": order_live_number, "result": "1"})

        elif type == "order_cancel":
            ##Click order cancel checkbox
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + type, "result": "1"})

            ##Click order cancel operator dropdown
            xpath = Util.GetXpath({"locate": "order_cancel_operator_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order cancel operator dropdown", "result": "1"})

            ##Click order cancel operator dropdown option
            xpath = Util.GetXpath({"locate": orders_operator})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click order cancel operator dropdown option => " + orders_operator, "result": "1"})

            ##Input order cancel number
            xpath = Util.GetXpath({"locate": "order_cancel_number_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": order_cancel_number, "result": "1"})

        elif type == "user_phone_number":
            ##Click user phone number checkbox
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + type, "result": "1"})

        elif type == "user_tag":
            ##Click user tag checkbox
            xpath = Util.GetXpath({"locate": type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click user validity checkbox => " + type, "result": "1"})

            ##Input user tag
            xpath = Util.GetXpath({"locate": "user_tag_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_tag, "result": "1"})

        else:
            dumplogger.error("Argument type is not correct!!! Please input one of following options => all_users / register_time / order_live / order_cancel / user_phone_number / user_tag")
            ret = 0

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.InputPublicUserScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDisplaySetting(arg):
        '''
        InputDisplaySetting : Input product discount product FE display setting
            Input argu :
                    custom_link - custom link in shopee voucher display setting
                    new_user_only - display new user only label for this voucher, you can input any string to open it
                    first_customised_label_region - first customised label region display text
                    first_customised_label_en - first customised label english display text
                    second_customised_label_region - second customised label region display text
                    second_customised_label_en - second customised label english display text
            Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        custom_link = arg['custom_link']
        new_user_only = arg['new_user_only']
        first_customised_label_region = arg['first_customised_label_region']
        first_customised_label_en = arg['first_customised_label_en']
        second_customised_label_region = arg['second_customised_label_region']
        second_customised_label_en = arg['second_customised_label_en']

        ##Default is default link, set to custom link if custom link is given
        if custom_link:
            ##Click custom link
            xpath = Util.GetXpath({"locate":"use_link_custom"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click custom link", "result": "1"})

            ##Input custom link
            xpath = Util.GetXpath({"locate":"custom_link"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": custom_link, "result": "1"})

        if new_user_only:
            ##Click new user only checkbox
            xpath = Util.GetXpath({"locate": "new_user_only_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click new user only checkbox", "result": "1"})

        if first_customised_label_region or first_customised_label_en:
            ##Click first customised label checkbox
            xpath = Util.GetXpath({"locate": "first_customised_label_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click first customised label checkbox", "result": "1"})

            if first_customised_label_region:
                ##Input first customised region (Some country don't have regional language)
                xpath = Util.GetXpath({"locate": "first_customised_region_field"})
                if Config._TestCaseRegion_ in ("MX", "CO", "CL", "ES", "AR"):
                    xpath_first_customised_region_field = xpath.replace("region_to_be_replace", "es_" + Config._TestCaseRegion_.lower())
                elif Config._TestCaseRegion_ == "BR":
                    xpath_first_customised_region_field = xpath.replace("region_to_be_replace", "pt_" + Config._TestCaseRegion_.lower())
                elif Config._TestCaseRegion_ == "IN":
                    xpath_first_customised_region_field = xpath.replace("region_to_be_replace", "hi")
                elif Config._TestCaseRegion_ == "PH":
                    xpath_first_customised_region_field = xpath.replace("region_to_be_replace", "zh_cn")
                elif Config._TestCaseRegion_ == "SG":
                    xpath_first_customised_region_field = xpath.replace("region_to_be_replace", "zh_cn")
                    xpath_first_customised_ms_field = xpath.replace("region_to_be_replace", "ms")
                    BaseUICore.Input({"method": "xpath", "locate": xpath_first_customised_ms_field, "string": first_customised_label_region, "result": "1"})
                else:
                    xpath_first_customised_region_field = xpath.replace("region_to_be_replace", Config._TestCaseRegion_.lower())
                BaseUICore.Input({"method": "xpath", "locate": xpath_first_customised_region_field, "string": first_customised_label_region, "result": "1"})

            ##Input first customised english
            xpath = Util.GetXpath({"locate": "first_customised_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": first_customised_label_en, "result": "1"})

        if second_customised_label_region or second_customised_label_en:
            ##Click second customised label checkbox
            xpath = Util.GetXpath({"locate": "second_customised_label_checkbox"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click second customised label checkbox", "result": "1"})

            if second_customised_label_region:
                ##Input second customised region (Some country don't have regional language)
                xpath = Util.GetXpath({"locate": "second_customised_region_field"})
                if Config._TestCaseRegion_ in ("MX", "CO", "CL", "ES"):
                    xpath_second_customised_region_field = xpath.replace("region_to_be_replace", "es_" + Config._TestCaseRegion_.lower())
                else:
                    xpath_second_customised_region_field = xpath.replace("region_to_be_replace", Config._TestCaseRegion_.lower())
                BaseUICore.Input({"method": "xpath", "locate": xpath_second_customised_region_field, "string": second_customised_label_region, "result": "1"})

            ##Input second customised english
            xpath = Util.GetXpath({"locate": "second_customised_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": second_customised_label_en, "result": "1"})

        ##Input product
        xpath = Util.GetXpath({"locate": "product_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Input product", "result": "1"})

        ##Input more details
        xpath = Util.GetXpath({"locate": "more_detail_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "Input more details", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.InputDisplaySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCoinCashbackBasicRule(arg):
        '''
        InputCoinCashbackBasicRule : Input coin cashback voucher info
                Input argu :
                            voucher_name - voucher name
                            prefix - voucher prefix code
                            suffix - voucher suffix length
                            max_voucher_usage - max voucher usage
                            minimum - minimum basket
                            start_time - start time
                            end_time - end time
                            coins_cashback_rate - percent cashback
                            coins_cashback_cap - caps of coins
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']
        prefix = arg['prefix']
        suffix = arg["suffix"]
        max_voucher_usage = arg["max_voucher_usage"]
        minimum = arg["minimum"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        coins_cashback_rate = arg['coins_cashback_rate']
        coins_cashback_cap = arg['coins_cashback_cap']

        ##Prepare datetime string
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Input name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})

        ##Input usage limit
        if max_voucher_usage:
            xpath = Util.GetXpath({"locate": "usage_limit"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_voucher_usage, "result": "1"})

        ##Input prefix code
        xpath = Util.GetXpath({"locate": "prefix_code"})
        if not prefix:
            ##if no prefix code get, auto generate one
            prefix = XtFunc.GenerateRandomString(8, "characters")
            GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_.append({"id": "", "code": prefix})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": prefix, "result": "1"})

        ##Input suffix length
        if suffix:
            xpath = Util.GetXpath({"locate": "suffix_length"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": suffix, "result": "1"})

        ##Input coins cashback rate
        xpath = Util.GetXpath({"locate": "coins_cashback_rate"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": coins_cashback_rate, "result": "1"})

        ##Input coins cashback cap
        xpath = Util.GetXpath({"locate": "coins_cashback_cap"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": coins_cashback_cap, "result": "1"})

        ##Input minimum basket
        xpath = Util.GetXpath({"locate": "minimum_basket"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": minimum, "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.InputCoinCashbackBasicRule, Voucher code -> ' + prefix)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCustomErrorMessage(arg):
        '''
        InputCustomErrorMessage : Input custom error message in user scope section
                Input argu : error_title - custom error message title
                             error_content - custom error message content
                             link - custom link
                             link_button_text - custom link button text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        error_title = arg['error_title']
        error_content = arg['error_content']
        link = arg['link']
        link_button_text = arg['link_button_text']

        ##Input message title
        if error_title:
            if Config._TestCaseRegion_ != "PH":
                ##Input message title region
                xpath = Util.GetXpath({"locate": "msg_title_reg_field"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_title, "result": "1"})

            ##Input message title english
            xpath = Util.GetXpath({"locate": "msg_title_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_title, "result": "1"})

        ##Input message content
        if error_content:
            if Config._TestCaseRegion_ != "PH":
                ##Input message content region
                xpath = Util.GetXpath({"locate": "msg_content_reg_field"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_content, "result": "1"})

            ##Input message content english
            xpath = Util.GetXpath({"locate": "msg_content_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": error_content, "result": "1"})

        ##Set up custom link
        if link:
            ##Click custom link radio button
            xpath = Util.GetXpath({"locate":"custom_link_radio_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click custom link radio button", "result": "1"})

            ##Input link
            xpath = Util.GetXpath({"locate": "link_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link, "result": "1"})

        ##Input link button text
        if link_button_text:
            ##Input link button text region
            xpath = Util.GetXpath({"locate": "link_btn_text_reg_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link_button_text, "result": "1"})

            ##Input link button text english
            xpath = Util.GetXpath({"locate": "link_btn_text_en_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": link_button_text, "result": "1"})

        OK(ret, int(arg['result']), "AdminShopeeVoucherPage.InputCustomErrorMessage")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductScope(arg):
        '''
        InputProductScope : Input product scope section
                Input argu : type - any_product / criteria
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click type
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product scope " + type, "result": "1"})

        OK(ret, int(arg['result']), "AdminShopeeVoucherPage.InputProductScope")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReminderPNSettings(arg):
        '''
        InputReminderPNSettings : Input reminder PN settings
                Input argu :
                    pn_time - push notification will be sent X minutes before the voucher 'Valid From' time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        pn_time = arg['pn_time']

        ##Input PN time
        xpath = Util.GetXpath({"locate":'pn_reminder_field'})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": pn_time, "result": "1"})

        OK(ret, int(arg['result']), "AdminShopeeVoucherPage.InputReminderPNSettings")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GenerateVouchers(arg):
        '''
        GenerateVouchers : Generate shopee vouchers
                Input argu :
                            voucher_code_to_generate - number of voucher code to generate
                            reason - reason string you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_code_to_generate = arg['voucher_code_to_generate']
        reason = arg['reason']

        ##Click generate vouchers button
        xpath = Util.GetXpath({"locate": "generate_vouchers_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click generate vouchers button", "result": "1"})

        ##Input number of voucher code to generate
        xpath = Util.GetXpath({"locate": "voucher_code_to_generate_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_code_to_generate, "result": "1"})

        ##Input reason string
        xpath = Util.GetXpath({"locate": "reason_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": reason, "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})

        ##Handle popup alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        ##Sleep 2 second to wait until generate loading is disappear
        time.sleep(2)

        ##Click view vouchers
        xpath = Util.GetXpath({"locate": "view_vouchers_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click view vouchers", "result": "1"})

        ##Sleep 10 second to wait until all voucher code element is shown
        time.sleep(10)

        ##Get all generate voucher codes and save in global variable
        xpath = Util.GetXpath({"locate": "voucher_codes_text"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "shopee_voucher_code", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.GenerateVouchers')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ExportUserDispatchStatus(arg):
        '''
        ExportUserDispatchStatus : Export user ids and dispatch status
                Input argu :
                            user_ids - user ids seperated with a comma (example: 1001,1002,1003....)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_ids = arg['user_ids']

        ##Click export user ids and dispatch status button
        xpath = Util.GetXpath({"locate": "export_dispatch_status_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click export user ids and dispatch status button", "result": "1"})

        if user_ids:
            ##Click export user id radio button
            xpath = Util.GetXpath({"locate": "export_user_id_radio_btn"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

            ##Input usage limit per user field
            xpath = Util.GetXpath({"locate": "user_ids_input_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_ids, "result": "1"})

        ##Click popup export button
        xpath = Util.GetXpath({"locate": "popup_export_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click popup export button", "result": "1"})

        ##Handle popup alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.ExportUserDispatchStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpSellerAbsorbed(arg):
        '''
        SetUpSellerAbsorbed : Set up shopee voucher seller absorbed
                Input argu :
                            seller_absorbed - yes / no
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        seller_absorbed = arg['seller_absorbed']

        ##Click seller absorbed drop down
        xpath = Util.GetXpath({"locate": "seller_absorbed_drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller absorbed drop down", "result": "1"})

        ##Click seller absorbed drop down option (Yes / No)
        xpath = Util.GetXpath({"locate": seller_absorbed})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click seller absorbed drop down option => %s" % (seller_absorbed), "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpSellerAbsorbed')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpProductScope(arg):
        '''
        SetUpProductScope : Set up shopee voucher product scope
                Input argu :
                            scope_type - including / excluding
                            shop_types - all / mall / prefer , you can input multiple option seperate with comma. example: mall,prefer,....
                            category_names - category name you want to select , you can input multiple option seperate with comma. example: category1,category2,....
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope_type = arg['scope_type']
        shop_types = arg['shop_types']
        category_names = arg['category_names']

        if scope_type == "including":
            ##Click product scope criteria radio button
            xpath = Util.GetXpath({"locate": "voucher_product_scope_crit_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product scope criteria radio button", "result": "1"})

        if shop_types:
            ##Click shop type drop down
            xpath = Util.GetXpath({"locate": scope_type + "_shop_type_drop_down"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop type drop down => %s" % (scope_type), "result": "1"})

            ##Get shop type list split with comma
            shop_type_list = shop_types.split(',')
            for shop_type in shop_type_list:
                ##Click shop type drop down option
                xpath = Util.GetXpath({"locate": scope_type + "_" + shop_type + "_drop_down_option"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click shop type drop down option => %s %s" % (scope_type, shop_type), "result": "1"})

        if category_names:
            ##Click category drop down
            xpath = Util.GetXpath({"locate": scope_type + "_category_drop_down"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click category drop down => %s" % (scope_type), "result": "1"})

            ##Get category name list split with comma
            category_name_list = category_names.split(',')
            for category_name in category_name_list:
                ##Click category name drop down option
                xpath = Util.GetXpath({"locate": scope_type + "_category_drop_down_option"})
                xpath_category_name = xpath.replace("category_name_to_be_replace", category_name)
                BaseUICore.Click({"method": "xpath", "locate": xpath_category_name, "message": "Click category name drop down option => %s" % (scope_type), "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpProductScope')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpUserLimit(arg):
        '''
        SetUpUserLimit : Set up shopee voucher user limit
                Input argu :
                            usage_limit - usage limit per user
                            total_claim_dispatch_limit - total claim/dispatch user limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        usage_limit = arg['usage_limit']
        total_claim_dispatch_limit = arg['total_claim_dispatch_limit']

        ##Input usage limit per user field
        xpath = Util.GetXpath({"locate": "usage_limit_per_user_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": usage_limit, "result": "1"})

        ##Input total claim/dispatch user limit field
        if total_claim_dispatch_limit:
            xpath = Util.GetXpath({"locate": "claim_dispatch_user_limit_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": total_claim_dispatch_limit, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpUserLimit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpDisplaySetting(arg):
        '''
        SetUpDisplaySetting : Set up display setting
                Input argu :
                            message - display setting message
                            url - display setting URL
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        message = arg['message']
        url = arg['url']

        ##Click allow shopping cart checkbox
        xpath = Util.GetXpath({"locate": "shopping_cart_checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click allow shopping cart checkbox", "result": "1"})

        ##Input display setting message
        xpath = Util.GetXpath({"locate": "display_setting_msg_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": message, "result": "1"})

        ##Input display setting url
        xpath = Util.GetXpath({"locate": "display_setting_url_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpDisplaySetting')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpVoucherTimePeroid(arg):
        '''
        SetUpVoucherTimePeroid : Set up voucher time peroid
                Input argu :
                            start_time - voucher active/valid start time
                            end_time - voucher active/valid end time
                            type - active / valid / all
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        type = arg["type"]

        ##Prepare datetime string
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        if type in ("active", "all"):
            ##Input active start time
            xpath = Util.GetXpath({"locate": "start_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

            ##Input active end time
            xpath = Util.GetXpath({"locate": "end_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        if type in ("valid", "all"):
            ##Input valid start time
            xpath = Util.GetXpath({"locate": "valid_start"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

            ##Input valid end time
            xpath = Util.GetXpath({"locate": "valid_end"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpVoucherTimePeroid')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpUserRegisterTimeFromPeroid(arg):
        '''
        SetUpUserRegisterTimeFromPeroid : Set up user register time from peroid
                Input argu :
                            start_time - voucher register start time
                            end_time - voucher register end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input register start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input register end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpUserRegisterTimeFromPeroid')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpPrivateNotification(arg):
        '''
        SetUpPrivateNotification : Set up private notification
                Input argu :
                            action - on / off
                            title - title string you want to input
                            action_require - action require string you want to input
                            push - push string you want to input
                            url - voucher notification url you want to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action = arg['action']
        title = arg['title']
        action_require = arg['action_require']
        push = arg['push']
        url = arg['url']

        ##Click notification action dropdown
        xpath = Util.GetXpath({"locate": "noti_action_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click notification action dropdown", "result": "1"})

        ##Click notification action dropdown option (On / Off)
        xpath = Util.GetXpath({"locate": action})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click notification action dropdown option => " + action, "result": "1"})

        if action == "on":
            ##Input title
            xpath = Util.GetXpath({"locate": "title_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

            ##Input action require
            xpath = Util.GetXpath({"locate": "action_require_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": action_require, "result": "1"})

            ##Input push
            xpath = Util.GetXpath({"locate": "push_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": push, "result": "1"})

            ##Input url
            xpath = Util.GetXpath({"locate": "url_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": url, "result": "1"})

            ##Upload banner
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "banner", "element_type": "name", "project": "FreeShippingVoucher", "action": "image", "file_name": "fsv_banner", "file_type": "png", "result": "1"})
            xpath = Util.GetXpath({"locate": "banner_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "ef7c7c97128a088723302ce33f708278", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SetUpPrivateNotification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpProductCategory(arg):
        '''
        SetUpProductCategory : Set Up Product Category
                Input argu : scope_type - including/excluding
                             category - category
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope_type = arg['scope_type']
        category = arg['category']

        ##old
        ##Click category dropdown
        dropdown_xpath = Util.GetXpath({"locate":"new_category_dropdown"})
        dropdown_xpath = dropdown_xpath.replace('replace_type', scope_type)
        BaseUICore.Click({"method":"xpath", "locate":dropdown_xpath, "message":"Click product category dropdown", "result": "1"})

        ##Check category you want to choose
        category_xpath = Util.GetXpath({"locate":"new_category_check"})
        category_xpath = category_xpath.replace('replace_type', scope_type)
        category_xpath = category_xpath.replace('replace_category', category)
        BaseUICore.Click({"method":"xpath", "locate":category_xpath, "message":"Click product category " + category, "result": "1"})

        ##Fold category dropdown
        BaseUICore.Click({"method":"xpath", "locate":dropdown_xpath, "message":"Click product category dropdown", "result": "1"})

        OK(ret, int(arg['result']), "AdminShopeeVoucherPage.SetUpProductCategory")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetUpShopType(arg):
        '''
        SetUpShopType : Select shop type in scope of product
                Input argu : scope_type - including / excluding
                             shop_type - all / mall / prefer_seller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        scope_type = arg['scope_type']
        shop_type = arg['shop_type']

        ##Click shop type dropdown
        dropdown_xpath = Util.GetXpath({"locate":"shop_type_dropdown"})
        dropdown_xpath = dropdown_xpath.replace('replace_type', scope_type)
        BaseUICore.Click({"method":"xpath", "locate":dropdown_xpath, "message":"Click shop type dropdown", "result": "1"})

        ##Check shop type you want to choose
        type_xpath = Util.GetXpath({"locate":shop_type})
        type_xpath = type_xpath.replace('replace_type', scope_type)
        BaseUICore.Click({"method":"xpath", "locate":type_xpath, "message":"Click shop type " + shop_type, "result": "1"})

        ##Fold shop type dropdown
        BaseUICore.Click({"method":"xpath", "locate":dropdown_xpath, "message":"Click shop type dropdown", "result": "1"})

        OK(ret, int(arg['result']), "AdminShopeeVoucherPage.SetUpShopType")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddCreditCardBinNumber(arg):
        '''
        AddCreditCardBinNumber : Add credit card bin number
                Input argu :
                            bin_number - BIN number you want to add
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        bin_number = arg['bin_number']

        ##Input BIN Number
        xpath = Util.GetXpath({"locate": "bin_number_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": bin_number, "result": "1"})

        ##Click add BIN button
        xpath = Util.GetXpath({"locate": "add_bin_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add BIN button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.AddCreditCardBinNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadUserCsvFile(arg):
        '''
        UploadUserCsvFile : Upload user csv file
                Input argu :
                            file_name - csv file name you want to upload
                            type - add / remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']
        type = arg['type']

        ##Click batch import user ids button
        xpath = Util.GetXpath({"locate": type + "_import_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch import user ids button", "result": "1"})

        ##Sleep 2 second to wait until choose file popup window is appear
        time.sleep(2)

        ##Click choose file button
        xpath = Util.GetXpath({"locate": type + "_choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})

        ##Upload user csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadUserCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCreditCardBinCsvFile(arg):
        '''
        UploadCreditCardBinCsvFile : Upload credit card BIN number csv file
                Input argu :
                            file_name - csv file name you want to upload
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Upload csv file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "import-bin-numbers", "element_type": "id", "project": "ShopeeVoucher", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click import BIN number button
        xpath = Util.GetXpath({"locate": "import_bin_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click import BIN number button", "result": "1"})

        ##If popup alert appear, handle popup alert
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadCreditCardBinCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductScopeCsvFile(arg):
        '''
        UploadProductScopeCsvFile : Upload product scope csv file
                Input argu :
                            upload_file_name - upload csv file name
                            upload_type - shopid / itemid
                            scope_type - including / excluding
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_file_name = arg['upload_file_name']
        upload_type = arg['upload_type']
        scope_type = arg['scope_type']

        if scope_type == "including":
            ##Click product scope criteria radio button
            xpath = Util.GetXpath({"locate": "voucher_product_scope_crit_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product scope criteria radio button", "result": "1"})

        ##Click choose file button
        xpath = Util.GetXpath({"locate": upload_type + "_" + scope_type + "_choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})
        time.sleep(5)
        ##Upload file with opencv
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": upload_file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": upload_type + "_" + scope_type + "_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        ##Handle popup alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadProductScopeCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadGenerateVoucherCsvFile(arg):
        '''
        UploadGenerateVoucherCsvFile : Upload generate voucher csv file
                Input argu :
                            file_name - upload csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Click mass generate shopee vouchers button
        xpath = Util.GetXpath({"locate": "mass_generate_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click mass generate shopee vouchers button", "result": "1"})

        ##Upload csv file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "mass-generate-upload-section-upload-file", "element_type": "id", "project": "ShopeeVoucher", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadGenerateVoucherCsvFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductScopeTagImage(arg):
        '''
        UploadProductScopeTagImage : Upload Product Scope Tag Image file
                Input argu :
                    file_name - upload product scope tag image file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Input name
        xpath = Util.GetXpath({"locate": "add_image_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_name, "result": "1"})

        ##Click choose image file button
        xpath = Util.GetXpath({"locate": "choose_image_file_btn"})
        BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

        ##Sleep 5 second to wait upload window is appear
        time.sleep(5)

        ##Upload voucher image file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "image", "file_name": file_name, "file_type": "png", "result": "1"})

        ##Sleep 5 second to wait image is shown on FE
        time.sleep(5)

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        ##Handle popup alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.UploadProductScopeTagImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchVoucher(arg):
        '''
        SearchVoucher : search voucher with voucher name or status
            Input argu :
                        voucher_name - voucher name you want to search
                        status - voucher status you want to search
            Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]
        status = arg["status"]

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        ##Search voucher with status
        if status:
            xpath = Util.GetXpath({"locate": "voucher_status"})
            BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"text", "selectkey":status.title(), "result": "1"})
            time.sleep(3)

        ##Search voucher with voucher name
        if voucher_name:
            xpath = Util.GetXpath({"locate": "search_voucher_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})
            time.sleep(2)
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.SearchVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetVoucherPromotionID(arg):
        '''
        GetVoucherPromotionID : Get voucher promotion ID and store to global variable
            Input argu :
                        voucher_type - shopee_voucher / free_shipping_voucher
                        voucher_name - voucher name you want to search
            Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]
        voucher_name = arg["voucher_name"]

        ##Check page loading processing is disappear
        xpath = Util.GetXpath({"locate": "pop_up_processing"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        if Config._TestCaseRegion_ == 'TW':
            ##Input voucher name
            xpath = Util.GetXpath({"locate": "voucher_name_search_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})
            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        ##Get voucher promotion ID and store to global variable
        xpath = Util.GetXpath({"locate":"promotion_id_text"})
        xpath_promotion_id_text = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUILogic.GetAndReserveElements({"method":"xpath", "locate":xpath_promotion_id_text, "reservetype": voucher_type + "_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.GetVoucherPromotionID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DisableVoucher(arg):
        '''
        DisableVoucher : Disable voucher
            Input argu : N/A
            Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        ##Click disable button to disable voucher
        xpath = Util.GetXpath({"locate": "disable_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable button to disable voucher", "result": "1"})

        ##Check enable button is exist
        xpath = Util.GetXpath({"locate": "enable_btn"})
        BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.DisableVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RemoveVoucher(arg):
        '''
        RemoveVoucher : remove latest voucher for specific name
                Input argu : name - voucher activity name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        voucher_name = arg['name']

        ##Search voucher using given voucher name
        xpath = Util.GetXpath({"locate": "voucher_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": voucher_name, "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})
        time.sleep(2)

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        ##Click remove button
        xpath = Util.GetXpath({"locate": "remove_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove button", "result": "1"})

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminShopeeVoucherPage.RemoveVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToVoucherDetail(arg):
        '''
        GoToVoucherDetail : Go to voucher detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Search for the voucher TWQA Create
        xpath = Util.GetXpath({"locate": "activity_name_search"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "TWQA", "result": "1"})
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Click first edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(2)

        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.GoToVoucherDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DispatchVoucher(arg):
        '''
        DispatchVoucher : Dispatch voucher in voucher detail page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click dispatch voucher button
        xpath = Util.GetXpath({"locate": "dispatch_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click dispatch button", "result": "1"})

        ##Handle popup alert
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg["result"]), 'AdminShopeeVoucherPage.DispatchVoucher')


class AdminGiftWithPurchasePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchGiftWithPurchaseId(arg):
        '''
        SearchGiftWithPurchaseId : Search gift with purchase id or shop id
                Input argu :
                        type - gift_with_purchase_id, shop_id
                        search_id - use gift with purchase id or shop id to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg["type"]
        search_id = arg["search_id"]

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "gift_with_purchase_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number": "5", "result": "1"})

        ##Input gift with purchase id or shop id to search
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":search_id, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.SearchGiftWithPurchaseId -> ' + search_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToGiftWithPurchaseDetail(arg):
        '''
        GoToGiftWithPurchaseDetail : Go to gift with purchase detail
                Input argu :
                        gift_with_purchase_id - use gift with purchase id to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        gift_with_purchase_id = arg["gift_with_purchase_id"]

        ##Input gift with purchase id and search
        time.sleep(5)
        AdminGiftWithPurchasePage.SearchGiftWithPurchaseId({"type":"gift_with_purchase_id", "search_id":gift_with_purchase_id, "result": "1"})

        ##Click edit btn
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit btn ", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.GoToGiftWithPurchaseDetail -> ' + gift_with_purchase_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateGiftWithPurchase(arg):
        '''
        CreateGiftWithPurchase : Create gift with purchase promotion
                Input argu : start_time - the time of start
                             end_time - the time of end
                             shop_id - shop id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        shop_id = arg['shop_id']

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Click create btn
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create btn", "result": "1"})

        ##Input name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "TWQA", "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        ##Input shop id
        xpath = Util.GetXpath({"locate": "shop_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})

        ##Input x value
        xpath = Util.GetXpath({"locate": "x_value"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "2", "result": "1"})

        ##Input y value
        xpath = Util.GetXpath({"locate": "y_value"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "2", "result": "1"})

        ##Input z value
        xpath = Util.GetXpath({"locate": "z_value"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "5", "result": "1"})

        ##Input regional label
        xpath = Util.GetXpath({"locate": "label_" + Config._TestCaseRegion_.lower()})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "QAAAAAAAAAA", "result": "1"})

        time.sleep(3)

        ##Click naxt btn
        xpath = Util.GetXpath({"locate": "next_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click next btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.CreateGiftWithPurchase')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddItemMainProduct(arg):
        '''
        AddItemMainProduct : Add item main product
                Input argu :
                        shopid - main item shop id
                        itemid - main item item id
                        purchase_limit - main product purchase limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]
        purchase_limit = arg["purchase_limit"]

        ##Click main product add item button
        xpath = Util.GetXpath({"locate": "main_add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main product add item button", "result": "1"})

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})

        ##Input itemid
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": itemid, "message": "Input itemid", "result": "1"})

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit","result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.AddItemMainProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddItemPurchaseWithGiftItem(arg):
        '''
        AddItemPurchaseWithGiftItem : Add purchase with free gift item
                Input argu :
                        shopid - gift item shop id
                        itemid - gift item item id
                        modelid - gift item model id
                        rebate - gidt item rebate
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]
        modelid = arg["modelid"]
        rebate = arg["rebate"]

        ##Click purchase with gift item add item button
        xpath = Util.GetXpath({"locate": "purchase_with_gift_add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click purchase with gift add item button", "result": "1"})

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})

        ##Input itemid
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": itemid, "message": "Input itemid", "result": "1"})

        ##Input modelid
        xpath = Util.GetXpath({"locate": "modelid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": modelid, "message": "Input modelid", "result": "1"})

        ##Input rebate
        xpath = Util.GetXpath({"locate": "rebate_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rebate, "message": "Input rebate", "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.AddItemPurchaseWithGiftItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableButton(arg):
        '''
        ClickEnableButton : Click enable button
                Input argu :
                        item_id - item_id which you want enable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click enable button
        xpath = Util.GetXpath({"locate": "enable_btn"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.ClickEnableButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisableButton(arg):
        '''
        ClickEnableButton : Click enable button
                Input argu :
                        item_id - item_id which you want disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click enable button
        xpath = Util.GetXpath({"locate": "disable_btn"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminGiftWithPurchasePage.ClickDisableButton')


class AdminAddOnDealsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductPoolCSV(arg):
        '''
        UploadProductPoolCSV : Upload csv file to add on deal main item after add on deal create
                Input argu :
                        type - main / addon
                        file_name - file name which you want to uplaod
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        item_type = arg["type"]
        file_name = arg["file_name"]

        slash = Config.dict_systemslash[Config._platform_]

        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

        ##Input csv file path
        xpath = Util.GetXpath({"locate": item_type + "_upload_csv"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})

        ##Click upload btn
        xpath = Util.GetXpath({"locate": item_type + "_upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.UploadProductPoolCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchUploadItemCSV(arg):
        '''
        BatchUploadItemCSV : Batch Upload csv file in add on deal overview page
                Input argu :
                        type - main_batch_upload / sub_batch_upload
                        file_name - file name which want to upload
                Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        type = arg['type']
        file_name = arg["file_name"]

        slash = Config.dict_systemslash[Config._platform_]

        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + ".csv"

        ##Input csv file path
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})

        ##Click upload btn
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.BatchUploadItemCSV')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchAddOnDealTime(arg):
        '''
        SearchAddOnDealTime : Search add on deal time in add on deal page
                Input argu :
                        start_time - use start time to search add on deal
                        end_time - use end time to search add on deal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input add on deal start time to search
        xpath = Util.GetXpath({"locate":"start_time"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string": start_time, "result": "1"})
        time.sleep(2)

        ##Handle pop up
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        ##Input add on deal end time to search
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        time.sleep(2)

        ##Handle pop up
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.SearchAddOnDealTime -> ' + start_time + '~' + end_time)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseAddOnDealStatus(arg):
        '''
        ChooseAddOnDealStatus : Choose add on deal status
                Input argu :
                        status - ongoing / scheduled / end
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg["status"]

        ##Click status drop down list
        xpath = Util.GetXpath({"locate": "drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})
        time.sleep(2)

        ##Choose add on deal status
        xpath = Util.GetXpath({"locate": status})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal status", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ChooseAddOnDealStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDisplayAmount(arg):
        '''
        ChooseDisplayAmount : Choose how many add on deal display in overview page
                Input argu :
                        amount - 25 / 50 / 75 / 100 / 125 / 150 / 175 / 200 / 225 / 250
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        amount = arg["amount"]

        ##Click amount drop down list
        xpath = Util.GetXpath({"locate": "drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})
        time.sleep(2)

        ##Choose add on deal amout
        xpath = Util.GetXpath({"locate": amount})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal amount", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ChooseDisplayAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadFileButton(arg):
        '''
        ClickUploadFileButton : Click upload file button
                Input argu : product_type - main/addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click upload file button
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload file button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickUploadFileButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnableButton(arg):
        '''
        ClickEnableButton : Click enable button
                Input argu :
                        item_id - item_id which you want to enable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click enable button
        xpath = Util.GetXpath({"locate": "enable_btn"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click enable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickEnableButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisableButton(arg):
        '''
        ClickDisableButton : Click disable button
                Input argu :
                        item_id - item_id which you want to disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click disable button
        xpath = Util.GetXpath({"locate": "disable_btn"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickDisableButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckbox(arg):
        '''
        ClickCheckbox : Click checkbox for enable or disable
                Input argu :
                        item_id - item_id which you want to disable
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click disable button
        xpath = Util.GetXpath({"locate": "checkbox"})
        xpath = xpath.replace("item_id_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNewButton(arg):
        '''
        ClickAddNewButton : Click add new button in add on deal overview page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add new button
        xpath = Util.GetXpath({"locate": "add_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickAddNewButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopyButton(arg):
        '''
        ClickCopyButton : Click copy button in add on deal overview page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click copy button
        xpath = Util.GetXpath({"locate": "copy_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click copy button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickCopyButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddItemButton(arg):
        '''
        ClickAddItemButton : Click add item button
                Input argu : product_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click add item button
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add item button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickAddItemButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteItem(arg):
        '''
        ClickDeleteItem : Click delete item btn
                Input argu : product_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click delete item btn
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete item btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickDeleteItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditButton(arg):
        '''
        ClickEditButton : Click edit button
                Input argu :
                        add_on_deal_name - add on deal name which you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        add_on_deal_name = arg["add_on_deal_name"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath_edit_btn = xpath.replace("add_on_deal_name_to_be_replace", add_on_deal_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath_edit_btn, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickEditButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditProductButton(arg):
        '''
        ClickEditProductButton : Click edit product button
                Input argu :
                        item_id - item_id which you want to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]

        ##Click edit button
        xpath = Util.GetXpath({"locate": "edit_btn"})
        edit_xpath = xpath.replace("item_id_name_to_be_replace", item_id)
        BaseUICore.Click({"method": "xpath", "locate": edit_xpath, "message": "Click edit product button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickEditProductButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveButton(arg):
        '''
        ClickSaveButton : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickSaveButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductMassEnableButton(arg):
        '''
        ClickProductMassEnableButton : Click product mass enable button
                Input argu :
                        product_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click main product mass enable button
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main product mass enable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickProductMassEnableButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductMassDisableButton(arg):
        '''
        ClickProductMassDisableButton : Click product mass disable button
                Input argu :
                        product_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click main product mass disable button
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main product mass disable button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickProductMassDisableButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAllProductCheckBox(arg):
        '''
        ClickAllProductCheckBox : Click all product checkBox
                Input argu :
                        product_type - main / addon
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click all product checkBox
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click all product checkBox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickAllProductCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLabelField(arg):
        '''
        InputLabelField : Input label field
                Input argu :
                        text - Input label field text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg['text']
        country = Config._TestCaseRegion_.lower()

        ##Input label field
        xpath = Util.GetXpath({"locate": country + "_label_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "message": "Input label field", "result": "1"})

        if country == 'my':
            ##input en label
            xpath = Util.GetXpath({"locate": "en_label_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "message": "Input label field", "result": "1"})
            time.sleep(3)

            ##input ch label
            xpath = Util.GetXpath({"locate": "ch_label_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "message": "Input label field", "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.InputLabelField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPurchaseLimitField(arg):
        '''
        InputPurchaseLimitField : Input purchase limit field
                Input argu :
                        text - Input purchase limit text
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text = arg['text']

        ##Input purchase limit field
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": text, "message": "Input purchase limit field", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.InputPurchaseLimitField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddItemMainProduct(arg):
        '''
        AddItemMainProduct : Add item main product
                Input argu :
                        shopid - shop id
                        itemid - item id
                        purchase_limit - main product purchase limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]
        purchase_limit = arg["purchase_limit"]

        ##Click main product add item button
        xpath = Util.GetXpath({"locate": "main_add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main product add item button", "result": "1"})
        time.sleep(3)

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})

        ##Input itemid
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": itemid, "message": "Input itemid", "result": "1"})

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit", "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.AddItemMainProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyItemMainProduct(arg):
        '''
        ModifyItemMainProduct : Modify main item product information
                Input argu :
                        purchase_limit - main product purchase limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        purchase_limit = arg["purchase_limit"]

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit","result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ModifyItemMainProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddItemAddOnProduct(arg):
        '''
        AddItemAddOnProduct : Add item add on product
                Input argu :
                        shopid - shop id
                        itemid - item id
                        modelid - model id
                        addon_price - add on product price
                        purchase_limit - add on product purchase limit
                        rebate - rebate
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        shopid = arg["shopid"]
        itemid = arg["itemid"]
        modelid = arg["modelid"]
        addon_price = arg["addon_price"]
        purchase_limit = arg["purchase_limit"]
        rebate = arg["rebate"]

        ##Click add on product add item button
        xpath = Util.GetXpath({"locate": "add_on_add_item_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on product add item button", "result": "1"})

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})

        ##Input itemid
        xpath = Util.GetXpath({"locate": "itemid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": itemid, "message": "Input itemid", "result": "1"})

        ##Input modelid
        xpath = Util.GetXpath({"locate": "modelid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": modelid, "message": "Input modelid", "result": "1"})

        ##Input add on price
        xpath = Util.GetXpath({"locate": "addon_price_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": addon_price, "message": "Input add on price", "result": "1"})

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit", "result": "1"})

        ##Input rebate
        xpath = Util.GetXpath({"locate": "rebate_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": rebate, "message": "Input rebate", "result": "1"})

        ##Click save button
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.AddItemAddOnProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBasicAddOnDealConfig(arg):
        '''
        InputBasicAddOnDealConfig : Input basic add on deal config
                Input argu :
                        name - add on deal name
                        shopid - shop id
                        start_time_delta - start time delta
                        end_time_delta - end time delta
                        purchase_limit - purchase limit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]
        shopid = arg["shopid"]
        start_time_delta = arg["start_time_delta"]
        end_time_delta = arg["end_time_delta"]
        purchase_limit = arg["purchase_limit"]

        ##Input add on deal name
        xpath = Util.GetXpath({"locate": "name_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "message": "Input add on deal name", "result": "1"})

        ##Input shopid
        xpath = Util.GetXpath({"locate": "shopid_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shopid, "message": "Input shopid", "result": "1"})

        ##Input start time with calculate value by start time delta
        start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time_delta), 0)
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_string, "message": "Input start time", "result": "1"})

        ##Input end time with calculate value by end time delta
        end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time_delta), 0)
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time_string, "message": "Input end time", "result": "1"})

        ##Input purchase limit
        xpath = Util.GetXpath({"locate": "purchase_limit_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": purchase_limit, "message": "Input purchase limit", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.InputBasicAddOnDealConfig')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyBasicAddOnDealConfig(arg):
        '''
        ModifyBasicAddOnDealConfig : Modify basic add on deal config
                Input argu :
                        config_type - name / shopid / start_time_delta / end_time_delta / purchase_limit / label
                        config_data - config_data
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        config_type = arg['config_type']
        config_data = arg['config_data']

        ##according to config type to input config data
        xpath = Util.GetXpath({"locate": config_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": config_data, "message": "Modify add on deal config", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ModifyBasicAddOnDealConfig')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyProductConfig(arg):
        '''
        ModifyProductConfig : Modify basic add on deal product config
                Input argu :
                        item_type - main/addon
                        item_id - product id
                        config_type - main_item_purchase_limit / add_on_price / addon_item_purchase_limit
                        config_data - config_data
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        item_type = arg['item_type']
        item_id = arg['item_id']
        config_type = arg['config_type']
        config_data = arg['config_data']

        ##click product edit button
        AdminAddOnDealsPage.ClickEditProductButton({"item_id": item_id, "result": "1"})
        time.sleep(2)

        ##according to config type to input config data
        xpath = Util.GetXpath({"locate": config_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": config_data, "message": "Modify product/item config", "result": "1"})

        ##click save button
        xpath = Util.GetXpath({"locate": item_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ModifyProductConfig')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ResetAddOnDealStatus(arg):
        '''
        ResetAddOnDealStatus : Reset add on deal status
                Input argu :
                        add_on_deal_name - add on deal name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        add_on_deal_name = arg["add_on_deal_name"]

        ##Get add on deal reset target
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath_edit_btn = xpath.replace("add_on_deal_name_to_be_replace", add_on_deal_name)
        ##Check target add on deal exist (status is scheduled), if exist, need to reset it
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath_edit_btn, "passok": "0", "result": "1"}):
            ##Click edit button in target add on deal
            BaseUICore.Click({"method": "xpath", "locate": xpath_edit_btn, "message": "Click edit button", "result": "1"})

            ##Check main product status, if status is enable, need to disable it
            xpath = Util.GetXpath({"locate": "main_product_action_status_enable"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ##Click all main product checkBox
                xpath = Util.GetXpath({"locate": "all_main_checkbox"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click all main product checkBox", "result": "1"})

                ##Click main product mass disable button
                xpath = Util.GetXpath({"locate": "main_mass_disable_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click main product mass disable button", "result": "1"})

                ##Close popup alert
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

            ##Sleep 5 second to wait until mass operation popup window is disappear
            time.sleep(5)

            ##Check add on product status, if status is enable, need to disable it
            xpath = Util.GetXpath({"locate": "addon_product_action_status_enable"})
            if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
                ##Click all add on product checkBox
                xpath = Util.GetXpath({"locate": "all_addon_checkbox"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click all add on product checkBox", "result": "1"})

                ##Click add on product mass disable button
                xpath = Util.GetXpath({"locate": "addon_mass_disable_btn"})
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on product mass disable button", "result": "1"})

                ##Close popup alert
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ResetAddOnDealStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchAddOnDealId(arg):
        '''
        SearchAddOnDealId : Search add on deal id or shop id in bundle deal page
                Input argu :
                        type - add_on_id, shop_id
                        search_id - use addon id or shop id to search addon deal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg["type"]
        search_id = arg["search_id"]

        ##Check table had displayed
        xpath = Util.GetXpath({"locate": "add_on_table"})
        BaseUILogic.CheckElementWithRetry({"locate": xpath, "total_retry_number": "5", "result": "1"})

        ##Input add on deal id or shop id to search
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":search_id, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.SearchAddOnDealId -> ' + search_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToAddOnDealDetail(arg):
        '''
        GoToAddOnDealDetail : Go to add on deal detail
                Input argu :
                        add_on_id - use add_on_id to search for add on deal
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        add_on_id = arg["add_on_id"]

        ##Input add on id and search
        time.sleep(5)
        AdminAddOnDealsPage.SearchAddOnDealId({"type":"add_on_id", "search_id":add_on_id, "result": "1"})

        ##Click edit btn
        time.sleep(5)
        xpath = Util.GetXpath({"locate":"edit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click edit btn ", "result": "1"})
        time.sleep(8)

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.GoToAddOnDealDetail -> ' + add_on_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrders(arg):
        '''
        ClickOrders : Click add on deal orders button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click orders btn
        xpath = Util.GetXpath({"locate": "add_on_orders_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add on orders btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickOrders')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeOrder(arg):
        '''
        ClickChangeOrder : Click change order btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change order btn
        xpath = Util.GetXpath({"locate": "add_on_change_orders_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on change orders btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickChangeOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickChangeProductOrder(arg):
        '''
        ClickChangeProductOrder : Click change product order btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click change product order btn
        xpath = Util.GetXpath({"locate": "change_product_order_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click change product order btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickChangeProductOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUpdateSubItems(arg):
        '''
        ClickBatchUpdateSubItems : Click batch update sub items btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch update sub items btn
        xpath = Util.GetXpath({"locate": "batch_update_sub_items_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch update sub items btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickBatchUpdateSubItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickBatchUpdateMainItems(arg):
        '''
        ClickBatchUpdateMainItems : Click batch update main items btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click batch update main items btn
        xpath = Util.GetXpath({"locate": "batch_update_main_items_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch update mian items btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickBatchUpdateMainItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrdersID(arg):
        '''
        ClickOrdersID : Click add on deal orders id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click orders id
        xpath = Util.GetXpath({"locate": "add_on_orders_id"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add on orders id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickOrdersID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExport(arg):
        '''
        ClickExport : Click export btn
                Input argu : product_type - main / addon / orders
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_type = arg['product_type']

        ##Click export btn
        xpath = Util.GetXpath({"locate": product_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click export btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickExport')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRightArrow(arg):
        '''
        ClickRightArrow : Click right arrow btn to change page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click right arrow btn
        xpath = Util.GetXpath({"locate": "right_arrow_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click right arrow btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminAddOnDealsPage.ClickRightArrow')


class AdminGroupBuyPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCreateNewButton(arg):
        '''
        ClickCreateNewButton : Click creat new group buy button
                Input argu :
                        NA
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click creat new button
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create button", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminGroupBuyPage.ClickCreateNewButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputGroupBuyNameAndPeriod(arg):
        '''
        InputGroupBuyNameAndPeriod : Input group buy name, start time and end time
                Input argu :
                        group_buy_name - name of group buy
                        start_time - group buy start time
                        end_time - group buy end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        group_buy_name = arg["group_buy_name"]
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Get time format
        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Input group buy name
        xpath = Util.GetXpath({"locate": "group_buy_name_block"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": group_buy_name, "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time_block"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time_block"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        time.sleep(5)

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
        time.sleep(10)

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        time.sleep(15)

        OK(ret, int(arg['result']), 'AdminGroupBuyPage.InputGroupBuyNameAndPeriod')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadGroupBuyItems(arg):
        '''
        UploadGroupBuyItems : Upload items for group buy
                Input argu :
                        file_name - csv file of items
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]

        ##Click choose file button
        xpath = Util.GetXpath({"locate": "choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})
        XtFunc.WindowsKeyboardEvent(action="esc")
        time.sleep(3)

        ##Click and upload file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "upload-file", "element_type": "id", "project": "SharingPanel", "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})
        time.sleep(10)

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        time.sleep(10)

        ##Click batch approve btn
        xpath = Util.GetXpath({"locate": "batch_approve_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click batch approve button", "result": "1"})
        time.sleep(15)

        ##Handle pop up
        BaseUILogic.HandleUnknownPopUpWindow({"expected_time": "1", "result": "1"})
        time.sleep(15)

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})
        time.sleep(10)

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminGroupBuyPage.UploadGroupBuyItems')


class AdminPaymentChannelPromotionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateNewPromotion(arg):
        '''
        CreateNewPromotion : Create new payment promotion
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M")
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=20)

        ##Click create new btn
        xpath = Util.GetXpath({"locate": "create_new_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create new button", "result": "1"})

        ##Input name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "TWQA", "result": "1"})

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})

        ##Input monthly limit
        xpath = Util.GetXpath({"locate": "monthly_limit"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "20", "result": "1"})

        ##Input total usage daily limit
        xpath = Util.GetXpath({"locate": "total_usage_daily_limit"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "20", "result": "1"})

        ##Click add bin btn
        xpath = Util.GetXpath({"locate": "add_bin_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add bin btn", "result": "1"})

        ##Input bin number
        xpath = Util.GetXpath({"locate": "bin_number"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "400000", "result": "1"})

        time.sleep(2)

        ##Click bun number pop up submit btn
        xpath = Util.GetXpath({"locate": "popup_submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click bin save btn", "result": "1"})

        ##Click add tier btn
        xpath = Util.GetXpath({"locate": "add_tier_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add tier btn", "result": "1"})

        ##Input min spending
        xpath = Util.GetXpath({"locate": "input_min_spending"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "1000", "result": "1"})

        ##Input discount percentage
        xpath = Util.GetXpath({"locate": "input_discount_percentage"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "50", "result": "1"})

        ##Click pop up submit btn
        xpath = Util.GetXpath({"locate": "popup_submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click tier save btn", "result": "1"})

        ## Click category dropdown
        if Config._TestCaseRegion_ in ("SG", "VN"):
            xpath = Util.GetXpath({"locate": "global_category_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click global category dropdown", "result": "1"})
        else:
            xpath = Util.GetXpath({"locate": "local_category_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click local category dropdown", "result": "1"})

        ## Select all category
        xpath = Util.GetXpath({"locate": "all_category"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click all category", "result": "1"})

        ##Click save btn
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg["result"]), 'AdminPaymentChannelPromotionPage.CreateNewPromotion')


class AdminManualCreditCoinsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddManuaulCreditCoins(arg):
        '''
        AddManuaulCreditCoins : Add coins from admin
                Input argu : user_id - user ID
                             amount - amount
                             title - title
                             reason - reason
                             description - description
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        user_id = arg["user_id"]
        amount = arg["amount"]
        title = arg["title"]
        reason = arg["reason"]
        description = arg["description"]

        ##Click add btn
        xpath = Util.GetXpath({"locate": "add_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add btn", "result": "1"})

        ##Input user ID
        xpath = Util.GetXpath({"locate": "userid_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_id, "result": "1"})

        ##Input amount
        xpath = Util.GetXpath({"locate": "amount_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": amount, "result": "1"})

        ##Input title
        xpath = Util.GetXpath({"locate": "title_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": title, "result": "1"})

        ##Input reason
        xpath = Util.GetXpath({"locate": "reason_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": reason, "result": "1"})

        ##Input description
        xpath = Util.GetXpath({"locate": "description_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": description, "result": "1"})

        ##Click submit btn
        xpath = Util.GetXpath({"locate": "submit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click submit btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.AddManuaulCreditCoins')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeManualCreditCoinsStatus(arg):
        '''
        ChangeManualCreditCoinsStatus : Change manual credit coins status
                Input argu : user_id - user ID
                             action_type - confirm/reject
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        user_id = arg["user_id"]
        action_type = arg["type"]

        ##Click action btn
        xpath = Util.GetXpath({"locate": action_type}).replace("user_id_to_be_replace", user_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.ChangeManualCreditCoinsStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def RestoreManualCreditCoinsStatus(arg):
        '''
        RestoreManualCreditCoinsStatus : Restore manual credit coins status
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click all item checkbox
        xpath = Util.GetXpath({"locate": "all_item_checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click all item checkbox", "result": "1"})

        ##Click mass reject btn
        xpath = Util.GetXpath({"locate": "reject_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click reject btn", "result": "1"})

        ##Handle pop up
        if BaseUILogic.DetectPopupWindow():
            BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.RestoreManualCreditCoinsStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadMassCreditCoinsFile(arg):
        '''
        UploadMassCreditCoinsFile : Upload mass credit coins file
                Input argu : file_name - file name
                             file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload file in input field
        BaseUILogic.UploadFileWithPath({"method": "input", "element":"upload", "element_type":"class", "project":"Coins", "action":"file", "file_name":file_name, "file_type":file_type, "result": "1"})
        time.sleep(3)
        ##Click Upload button
        BaseUICore.Click({"method":"xpath", "locate":Util.GetXpath({"locate":"upload_csv_btn"}), "message":"Click Upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.UploadMassCreditCoinsFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MassChangeManualCreditCoinsStatus(arg):
        '''
        MassChangeManualCreditCoinsStatus : Mass change manual credit coins status
                Input argu : action_type - confirm/reject
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg["type"]

        for click_time in range(1, 6):
            ##Click to select all check box
            xpath = Util.GetXpath({"locate": "checkbox_all"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to check all checkbox", "result": "1"})

            time.sleep(5)

            ##Click action btn
            xpath = Util.GetXpath({"locate": action_type})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action btn", "result": "1"})

            try:
                ##Click ok on popup message
                BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})
                break
            except:
                dumplogger.info("Popup alert don't show up, try to click checkbox again.")

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.MassChangeManualCreditCoinsStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectAllBox(arg):
        '''
        ClickSelectAllBox : Click select all box on manual credit coins page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to select all check box
        xpath = Util.GetXpath({"locate": "checkbox_all"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to check all checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.ClickSelectAllBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMassActionButton(arg):
        '''
        ClickMassActionButton : Click mass action button on manual credit coins page
                Input argu : action_type - confirm/reject
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        action_type = arg["type"]

        ##Click action btn
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.ClickMassActionButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCheckBox(arg):
        '''
        SelectCheckBox : Select singel box on manual credit coins page
                Input argu : order - order need to select, can using space to check more than 1 order
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]

        xpath = Util.GetXpath({"locate": "checkbox_singel"}).replace("order_to_be_replace", order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to check singel checkbox", "result": "1"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.SelectCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeSingelManualCreditCoinsStatus(arg):
        '''
        ChangeSingelManualCreditCoinsStatus : Change singel manual credit coin status
                Input argu : order - order need to check, can using space to check more than 1 order
                            action_type - confirm/reject
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order = arg["order"]
        action_type = arg["type"]

        ##Check more than 1 order
        for order_number in order:
            if order_number != " ":
                time.sleep(3)

                ##Click to select singel check box
                xpath = Util.GetXpath({"locate": "checkbox_singel"}).replace("order_to_be_replace", order_number)
                BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to check singel checkbox", "result": "1"})

        time.sleep(3)

        ##Click action btn
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click action btn", "result": "1"})

        ##Click ok on popup message
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminManualCreditCoinsPage.CheckSingelCreditCoinsOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCoinCreditRecordDate(arg):
        '''
        CheckCoinCreditRecordDate : Check coin credit record date
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        ##Get upload date
        xpath = Util.GetXpath({"locate":"record_date"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        upload_date = GlobalAdapter.CommonVar._PageAttributes_

        ##Get currect time
        current_time = XtFunc.GetCurrentDateTime("%d-%m-%Y %H:%M", 0, 0, 0)
        dumplogger.info("current time: %s" % (current_time))

        ##Only compare date
        if upload_date[:10] == current_time[:10]:
            dumplogger.info("Upload date same with current date.")
        else:
            dumplogger.info("Upload date difference with current date.")
            ret = 0

        OK(ret, int(arg['result']), "AdminManualCreditCoinsPage.CheckCoinCreditRecordDate")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchRecordByUserID(arg):
        '''
        SearchRecordByUserID : Search record by user ID
                Input argu : user_id - user id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        user_id = arg["user_id"]

        ##Input user ID
        xpath = Util.GetXpath({"locate": "userid_input"})

        ##Clean all string first
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_id, "result": "1"})
        time.sleep(2)

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), "AdminManualCreditCoinsPage.SearchRecordByUserID")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchRecordByUsername(arg):
        '''
        SearchRecordByUsername : Search record by username
                Input argu : username - username
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        username = arg["username"]

        ##Input user name
        xpath = Util.GetXpath({"locate": "username_input"})

        ##Clean all string first
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": "", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": username, "result": "1"})
        time.sleep(2)

        ##Click keyboard entry
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), "AdminManualCreditCoinsPage.SearchRecordByUsername")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectRecordStatus(arg):
        '''
        SelectRecordStatus : Select record status
                Input argu : status - all, pending, confirmed, rejected
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        status = arg["status"]

        ##Select status
        if status == "all":
            xpath = Util.GetXpath({"locate": "status_select"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "~~~", "result": "1"})
        elif status == "pending":
            xpath = Util.GetXpath({"locate": "status_select"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "1", "result": "1"})
        elif status == "confirmed":
            xpath = Util.GetXpath({"locate": "status_select"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "2", "result": "1"})
        elif status == "rejected":
            xpath = Util.GetXpath({"locate": "status_select"})
            BaseUICore.SelectDropDownList({"method": "xpath", "locate": xpath, "selecttype": "value", "selectkey": "0", "result": "1"})

        time.sleep(10)

        OK(ret, int(arg['result']), "AdminManualCreditCoinsPage.SelectRecordStatus")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def MoveToTitleInfoIcon(arg):
        '''
        MoveToTitleInfoIcon : Move to title info icon
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Move to title info icon element
        xpath = Util.GetXpath({"locate": "title_info_icon"})
        BaseUICore.MoveToElementCoordinate({"locate": xpath, "is_click": "0", "result": "1"})

        OK(ret, int(arg['result']), "AdminManualCreditCoinsPage.MoveToTitleInfoIcon")


class AdminProductPromotionsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToProductPromotionDetail(arg):
        '''
        GoToProductPromotionDetail : Search promotion id to go to product promotion detail page
                Input argu :
                        promotion_id - use promotion id to search for product promotion
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_id = arg["promotion_id"]

        time.sleep(5)
        ##Input promotion id and search
        AdminProductPromotionsPage.SearchProductPromotionId({"promotion_id": promotion_id, "result": "1"})
        time.sleep(5)

        ##Click target product promotion into detail page
        xpath = Util.GetXpath({"locate": "target_promotion_id"})
        xpath = xpath.replace('replace_seller', promotion_id)
        time.sleep(2)

        ##Click target product promotion id
        xpath = Util.GetXpath({"locate": "promotion_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click target table", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.GoToProductPromotionDetail -> ' + promotion_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPromotionId(arg):
        '''
        ClickPromotionId : Click promotion id to go to product promotion detail page
                Input argu : promotion_name - use promotion name to click promotion id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promotion_name = arg['promotion_name']

        ##Click target product promotion id
        xpath = Util.GetXpath({"locate": "promotion_id"})
        xpath = xpath.replace('replace_promotion_name', promotion_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click target table", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickPromotionId')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToPromotionItemConfirmation(arg):
        '''
        GoToPromotionItemConfirmation : Click Promotion Item Confirmation tab
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click Promotion Item Confirmation tab
        xpath = Util.GetXpath({"locate": "promotion_item_confirmation"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click Promotion Item Confirmation tab", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.GoToPromotionItemConfirmation')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductPromotionId(arg):
        '''
        SearchProductPromotionId : Search product promotion id in product promotion page
                Input argu :
                        promotion_id - use promotion id to search for product promotion
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        promotion_id = arg["promotion_id"]

        ##Input bundle id and search
        xpath = Util.GetXpath({"locate":"promotion"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":promotion_id, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype":"enter", "locate":xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.SearchProductPromotionId -> ' + promotion_id)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductPromotionName(arg):
        '''
        SearchProductPromotionName : Search product promotion name in product promotion page
                Input argu :
                        promotion_name - use promotion name to search for product promotion
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        promotion_name = arg["promotion_name"]

        ##Input product name and search
        xpath = Util.GetXpath({"locate": "promotion_name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promotion_name, "result": "1"})
        time.sleep(3)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.SearchProductPromotionName -> ' + promotion_name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductPromotionType(arg):
        '''
        SearchProductPromotionType : Search product promotion type in product promotion page
                Input argu :
                        promotion_type - all_promotion_type / normal_product_promotion / deep_discount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        promotion_type = arg["promotion_type"]

        ##Click promotion type drop down list
        xpath = Util.GetXpath({"locate": "drop_down_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list", "result": "1"})
        time.sleep(5)

        ##Click promotion type
        xpath = Util.GetXpath({"locate": promotion_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion type", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.SearchProductPromotionType -> ' + promotion_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductPromotionStage(arg):
        '''
        SearchProductPromotionStage : Search product promotion stage in product promotion page
                Input argu :
                        promotion_stage - all_promotion_stage / created / nomination / under_review / live / ended
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        promotion_stage = arg["promotion_stage"]

        ##Click promotion stage drop down list
        xpath = Util.GetXpath({"locate": "drop_down_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list", "result": "1"})
        time.sleep(5)

        ##Click promotion stage
        xpath = Util.GetXpath({"locate": promotion_stage})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion stage", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.SearchProductPromotionStage -> ' + promotion_stage)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductPromotionSource(arg):
        '''
        SearchProductPromotionSource : Search product promotion source in product promotion page
                Input argu :
                        promotion_source - all_promotion_source / admin / cmt
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        promotion_source = arg["promotion_source"]

        ##Click promotion stage drop down list
        xpath = Util.GetXpath({"locate": "drop_down_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list", "result": "1"})
        time.sleep(5)

        ##Click promotion source
        xpath = Util.GetXpath({"locate": promotion_source})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion source", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.SearchProductPromotionStage -> ' + promotion_source)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductPromotionByItem(arg):
        '''
        SearchProductPromotionByItem : Search product promotion by item information in product promotion search area
                Input argu :
                        type - shop_id / item_id / model_id
                        data - data which you want to search
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg["type"]
        data = arg["data"]

        ##Input bundle id and search
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": data, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.SearchProductPromotionByItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search btn in product promotion search area
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search btn
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeDisplayPage(arg):
        '''
        ChangeDisplayPage : Change display page btn in product promotion overview page
                Input argu :
                        page_number - which page number you want to arrive
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        page_number = arg['page_number']

        ##Delete page number
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Input page number
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_number, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ChangeDisplayPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageBar(arg):
        '''
        ClickPageBar : Click page bar to change page number
                Input argu :
                        action_type - first_page / last_page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        action_type = arg['action_type']

        ##Click page bar
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page bar", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickPageBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDisplayAmount(arg):
        '''
        ChooseDisplayAmount : Choose how many add on deal display in overview page
                Input argu :
                        amount - 25 / 50 / 100 / 200
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        amount = arg["amount"]

        ##Click amount drop down list
        xpath = Util.GetXpath({"locate": "drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})
        time.sleep(2)

        ##Choose add on deal amout
        xpath = Util.GetXpath({"locate": amount})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal amount", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ChooseDisplayAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveAndContinue(arg):
        '''
        ClickSaveAndContinue : Click save and continue btn in promotion template page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click target product promotion id into detail page.
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickSaveAndContinue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddProductPromotion(arg):
        '''
        ClickAddProductPromotion : Click add product promotion btn in promotion overview page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add product promotion btn
        xpath = Util.GetXpath({"locate": "add_product_promotion_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add product promotion btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickAddProductPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductPromotionName(arg):
        '''
        InputProductPromotionName : Input product promotion name in promotion template page
                Input argu :
                        name - product promotion name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        name = arg["name"]

        ##Input product promotion name
        xpath = Util.GetXpath({"locate": "name_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "message": "Input product promotion name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.InputProductPromotionName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductPromotionTime(arg):
        '''
        InputProductPromotionTime : Input product promotion start and emd time in promotion template page
                Input argu :
                        start_time - promotion start time
                        end_time - promotion end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time with calculate value by start time delta
        start_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(start_time), 0)
        xpath = Util.GetXpath({"locate": "start_time_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time_string, "message": "Input start time", "result": "1"})

        ##Input end time with calculate value by end time delta
        end_time_string = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, int(end_time), 0)
        xpath = Util.GetXpath({"locate": "end_time_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time_string, "message": "Input end time", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.InputProductPromotionTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductPromotionDiscount(arg):
        '''
        InputProductPromotionDiscount : Input product promotion discount in promotion template page
                Input argu :
                        min_discount_price - min discount price
                        min_discount_price - min discount price
                        min_discount_percent - min discount percent
                        max_discount_percent - max discount percent
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        min_discount_price = arg["min_discount_price"]
        max_discount_price = arg["max_discount_price"]
        min_discount_percent = arg["min_discount_percent"]
        max_discount_percent = arg["max_discount_percent"]

        if min_discount_price or max_discount_price:
            ##delete min discount percent
            xpath = Util.GetXpath({"locate": "min_discount_percent_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            ##delete max discount percent
            xpath = Util.GetXpath({"locate": "max_discount_percent_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            ##Input min discount price
            xpath = Util.GetXpath({"locate": "min_discount_price_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_discount_price,"message": "Modify product prmotion config", "result": "1"})

            ##Input max discount price
            xpath = Util.GetXpath({"locate": "max_discount_price_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_discount_price,"message": "Modify product prmotion config", "result": "1"})

        if min_discount_percent or max_discount_percent:
            ##delete min discount price
            xpath = Util.GetXpath({"locate": "min_discount_price_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            ##delete min discount price
            xpath = Util.GetXpath({"locate": "max_discount_price_field"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})

            ##Input min discount percent
            xpath = Util.GetXpath({"locate": "min_discount_percent_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_discount_percent,"message": "Modify product prmotion config", "result": "1"})

            ##Input max discount percent
            xpath = Util.GetXpath({"locate": "max_discount_percent_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_discount_percent,"message": "Modify product prmotion config", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.InputProductPromotionDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductPromotionRule(arg):
        '''
        InputProductPromotionRule : Input product promotion rule in promotion template page
                Input argu :
                        min_promo_stock - min promo stock
                        max_po_items - PO items % = Sum (Confirmed PO) / Sum (All Confirmed items)
                        min_rebate - min rebate price
                        max_rebate - max rebate price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        min_promo_stock = arg["min_promo_stock"]
        max_po_items = arg["max_po_items"]
        min_rebate = arg["min_rebate"]
        max_rebate = arg["max_rebate"]

        if min_promo_stock:
            ##Input min promo stock
            xpath = Util.GetXpath({"locate": "min_promo_stock_field"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_promo_stock,"message": "Input min promo stock", "result": "1"})

        ##Input max po items percent
        xpath = Util.GetXpath({"locate": "max_po_items_percent_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_po_items, "message": "Input max po items percent","result": "1"})

        ##Input min rebate price
        xpath = Util.GetXpath({"locate": "min_rebate_price_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": min_rebate, "message": "Input min rebate price", "result": "1"})

        ##Input max rebate price
        xpath = Util.GetXpath({"locate": "max_rebate_price_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": max_rebate, "message": "Input max rebate price","result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.InputProductPromotionRule')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyBasicConfig(arg):
        '''
        ModifyBasicConfig : Modify basic add on deal config
                Input argu :
                        config_type - name / start_time / end_time / min_discount_price / max_discount_price / min_discount / max_discount / min_promo_stock / max_po / min_rebate / max_rebate
                        config_data - data to be modified
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        config_type = arg['config_type']
        config_data = arg['config_data']

        ##according to config type to input config data
        xpath = Util.GetXpath({"locate": config_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": config_data, "message": "Modify product prmotion config","result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ModifyBasicConfig')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirmPrice(arg):
        '''
        ClickConfirmPrice : Click confirm price in promotion template page
                Input argu :
                        item_id - use item id to confirm product promotion price
                        type - confirm or reject product promotion price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        item_id = arg["item_id"]
        type = arg['type']

        ##Click select item checkbox
        xpath = Util.GetXpath({"locate": "check_box"})
        xpath = xpath.replace('replace_item_id', item_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item checkbox", "result": "1"})
        time.sleep(3)

        ##Click target product promotion id into detail page.
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm or reject btn", "result": "1"})

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickConfirmPrice')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickItemCheckBox(arg):
        '''
        ClickItemCheckBox : Click item checkbox in promotion template page
                Input argu :
                        type - confirmation_page / exceed_table
                        model_id - choose specific item id checkbox
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        model_id = arg["model_id"]

        ##Click item checkbox
        xpath = Util.GetXpath({"locate": type})
        xpath = xpath.replace('replace_model_id', model_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click item checkbox", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickItemCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePromotionPriceAction(arg):
        '''
        ChoosePromotionPriceAction : Click confirm or reject promotion price in promotion template page
                Input argu :
                        type - confirm or reject product promotion price
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click target product promotion id into detail page.
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm or reject btn", "result": "1"})

        ##Handle pop up
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ChoosePromotionPriceAction')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProduct(arg):
        '''
        UploadProduct : Upload item id to product promotion
                Input argu :
                        file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        file_name = arg["file_name"]

        slash = Config.dict_systemslash[Config._platform_]

        file_path = Config._UICaseDataFilePath_ + slash + Config._TestCaseRegion_ + slash + Config._EnvType_ + slash + Config._TestCasePlatform_ + slash + Config._TestCaseFeature_ + slash + "file" + slash + file_name + "." + "csv"

        ##Upload Shop id list
        xpath = Util.GetXpath({"locate": "upload_csv"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_path, "result": "1"})
        time.sleep(5)

        ##Click upload btn
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.UploadProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def BatchRejectItem(arg):
        '''
        BatchRejectItem : Batch reject items in promotion item confirmation tab
                Input argu :
                        file_name - file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Click choose file button
        xpath = Util.GetXpath({"locate": "choose_file_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click choose file button", "result": "1"})

        ##Upload user csv file
        XtFunc.UploadFileWithWindowsAutoIt({"action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload button
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.BatchRejectItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel btn in product promotion model uploading table page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProceed(arg):
        '''
        ClickProceed : Click proceed btn in product promotion model uploading table page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click proceed btn
        xpath = Util.GetXpath({"locate": "proceed_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click proceed btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickProceed')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadItems(arg):
        '''
        ClickDownloadItems : Click download items btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download items btn
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download items btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickDownloadItems')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeepDiscountToggle(arg):
        '''
        ClickDeepDiscountToggle : Click deep discount toggle
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click deep discount toggle
        xpath = Util.GetXpath({"locate": "deep_discount_toggle"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click deep discount toggle", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickDeepDiscountToggle')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOverlapPromotion(arg):
        '''
        ClickOverlapPromotion : Click overlap promotion id
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click overlap promotion id
        xpath = Util.GetXpath({"locate": "promotion_id"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click overview promotion id", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ClickOverlapPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChoosePromotionType(arg):
        '''
        ChoosePromotionType : Choose promotion type
                Input argu : promo_type - normal_product_promotion / deep_discount
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promo_type = arg['promo_type']

        ##Click promotion type dropdown list
        xpath = Util.GetXpath({"locate": "promotion_type_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion type dropdown list", "result": "1"})
        time.sleep(3)

        ##Choose type
        xpath = Util.GetXpath({"locate": promo_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose promotion type -> " + promo_type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.ChoosePromotionType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetProductPromotionID(arg):
        '''
        GetProductPromotionID : Get the lastest Product Promotion ID
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get promotion id
        time.sleep(2)
        xpath = Util.GetXpath({"locate": "product_promotion_id"})
        BaseUILogic.GetAndReserveElements({"method": "xpath", "locate": xpath, "reservetype": "promotion_id", "result": "1"})

        OK(ret, int(arg['result']), 'AdminProductPromotionsPage.GetProductPromotionID -> %s' % (GlobalAdapter.PromotionE2EVar._PromotionIDList_))


class AdminOverlayImagePoolPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditOverlayImage(arg):
        '''
        ClickEditOverlayImage : Click edit overlay image
                Input argu : data_id - data_id
                Return code : 0 - success
                            1 - fail
                            99 - error
        '''
        ret = 1
        data_id = arg["data_id"]

        ##Click edit
        xpath = Util.GetXpath({"locate": "edit"}).replace("data_id_to_be_replace", data_id)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOverlayImagePoolPage.ClickEditOverlayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputOverlayImageName(arg):
        '''
        InputOverlayImageName : Input overlay image name
                Input argu : name - name
                Return code : 0 - success
                            1 - fail
                            99 - error
        '''
        ret = 1
        name = arg["name"]

        ##Input name
        xpath = Util.GetXpath({"locate": "name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOverlayImagePoolPage.InputOverlayImageName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveOverlayImage(arg):
        '''
        ClickSaveOverlayImage : Click save overlay image
                Input argu : N/A
                Return code : 0 - success
                            1 - fail
                            99 - error
        '''
        ret = 1

        ##Click save
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOverlayImagePoolPage.ClickSaveOverlayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelOverlayImage(arg):
        '''
        ClickCancelOverlayImage : Click cancel overlay image
                Input argu : N/A
                Return code : 0 - success
                            1 - fail
                            99 - error
        '''
        ret = 1

        ##Click cancel
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminOverlayImagePoolPage.ClickCancelOverlayImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadOverlayImage(arg):
        '''
        UploadOverlayImage : Upload overlay image
                Input argu : file_name - file name
                             file_type - file type
                Return code : 0 - success
                            1 - fail
                            99 - error
        '''
        ret = 1
        project = arg["project"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload image
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "input-image-to-add-1", "element_type": "id", "project": project, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminOverlayImagePoolPage.UploadOverlayImage')


class AdminSellerDiscountPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CreateSellerDiscount(arg):
        '''
        CreateSellerDiscount : Create seller discount
                Input argu : start_time - the time of start
                             end_time - the time of end
                             promo_name - the name of promotion
                             file_name - promo csv file name
                             project - your project name

                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]
        project = arg["project"]
        promo_name = arg["promo_name"]
        file_name = arg["file_name"]

        start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
        end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))

        ##Click create btn
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create btn", "result": "1"})
        time.sleep(2)

        ##Input promo name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promo_name, "result": "1"})
        time.sleep(2)

        ##Input start time
        xpath = Util.GetXpath({"locate": "start_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time", "result": "1"})
        time.sleep(2)

        ##Input end time
        xpath = Util.GetXpath({"locate": "end_time"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time", "result": "1"})
        time.sleep(2)

        ## Upload seller discount csv file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "seller-discount-upload-items-file", "element_type": "id", "project": project, "action": "file", "file_name": file_name, "file_type": "csv", "result": "1"})

        ##Click upload btn
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})

        time.sleep(10)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.CreateSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadSellerDiscountFile(arg):
        ''' UploadSellerDiscountFile : Upload seller discount file
                Input argu :
                    file_name - promo file name
                    file_type - promo file type
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ## Upload seller discount file
        BaseUILogic.UploadFileWithPath({"method": "input", "element": "file", "element_type": "name", "project": Config._TestCaseFeature_, "action": "file", "file_name": file_name, "file_type": file_type, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.UploadSellerDiscountFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoTime(arg):
        '''
        InputPromoTime : Input promo time
                Input argu :
                        start_time - the time of start
                        end_time - the time of end
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        start_time = arg["start_time"]
        end_time = arg["end_time"]

        ##Input start time
        if start_time:
            start_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(start_time))
            xpath = Util.GetXpath({"locate": "start_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": start_time, "result": "1"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time", "result": "1"})
            time.sleep(2)

        ##Input end time
        if end_time:
            end_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", minutes=int(end_time))
            xpath = Util.GetXpath({"locate": "end_time"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": end_time, "result": "1"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.InputPromoTime')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoName(arg):
        '''
        InputPromoName : Input promo name
                Input argu :
                        promo_name - the name of promotion
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promo_name = arg["promo_name"]

        ##Input promo name
        xpath = Util.GetXpath({"locate": "name"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promo_name, "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.InputPromoName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditSellerDiscount(arg):
        '''
        ClickEditSellerDiscount : Click edit btn on seller discount overview page
                Input argu :
                        promo_name - promotion name to edit
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promo_name = arg["promo_name"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "edit_btn"})
        xpath = xpath.replace("promo_name", promo_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickEditSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDownloadItem(arg):
        '''
        ClickDownloadItem : Click download item btn
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click download item btn
        xpath = Util.GetXpath({"locate": "download_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click download item btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickDownloadItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveEditPromotion(arg):
        '''
        ClickSaveEditPromotion : Click save btn on promotion edit page
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save btn on promotion edit page
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save btn on promotion edit page", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickSaveEditPromotion')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpload(arg):
        '''
        ClickUpload : Click upload btn on promotion edit page
                Input argu :
                        N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload btn
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickUpload')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchSellerDiscount(arg):
        '''
        SearchSellerDiscount : Search on seller discount overview page
            Input argu :
                user_id - seller id
                username - seller name
                promo_id - promotion id
                promotion_status - scheduled / live / ended / deleted
            Return code : 1 - success
                          0 - fail
                         -1 - error
        '''
        ret = 1
        user_id = arg["user_id"]
        username = arg["username"]
        promo_id = arg["promo_id"]
        promotion_status = arg["promotion_status"]

        BaseUILogic.BrowserRefresh({"message":"Refresh browser", "result": "1"})
        time.sleep(5)

        if user_id or username or promo_id:

            ##Search user ID
            if user_id:
                xpath = Util.GetXpath({"locate": "user_id"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": user_id, "result": "1"})
                time.sleep(3)

            ##Search username
            if username:
                xpath = Util.GetXpath({"locate": "username"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": username, "result": "1"})
                time.sleep(2)

            ##Search promo ID
            if promo_id:
                xpath = Util.GetXpath({"locate": "promo_id"})
                BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promo_id, "result": "1"})
                time.sleep(2)

            BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})
            time.sleep(2)

        ##Click promotion status drop down list
        if promotion_status:
            xpath = Util.GetXpath({"locate": "drop_down_list"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down list", "result": "1"})
            time.sleep(5)

            ##Click promotion status
            xpath = Util.GetXpath({"locate": promotion_status})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click promotion status:" + promotion_status, "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.SearchSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEndSellerDiscount(arg):
        '''
        ClickEndSellerDiscount : Click end btn on seller discount overview page
                Input argu :
                        promo_name - promotion name to end
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promo_name = arg["promo_name"]

        ##Click edit btn
        xpath = Util.GetXpath({"locate": "end_btn"})
        xpath = xpath.replace("promo_name", promo_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end button", "result": "1"})
        BaseUILogic.PopupHandle({"handle_action": "accept", "result": "0"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickEndSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddSellerDiscount(arg):
        '''
        ClickAddSellerDiscount : Click add seller discount btn on seller discount overview page
                Input argu : N/A
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1

        ##Click create btn
        xpath = Util.GetXpath({"locate": "create_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click create btn", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickAddSellerDiscount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchShopID(arg):
        '''
        SearchShopID : Search shop id on seller discount overview page
                Input argu :
                        shop_id - shop id
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        shop_id = arg["shop_id"]

        ##Input shop ID
        xpath = Util.GetXpath({"locate": "shop_id"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": shop_id, "result": "1"})
        time.sleep(3)

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.SearchShopID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeDisplayPage(arg):
        '''
        ChangeDisplayPage : Change display page btn in seller discount overview page
                Input argu :
                        page_number - which page number you want to arrive
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        page_number = arg['page_number']

        ##Delete page number
        xpath = Util.GetXpath({"locate": "input_field"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        time.sleep(2)

        ##Input page number
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": page_number, "result": "1"})
        time.sleep(2)
        BaseUILogic.KeyboardAction({"actiontype": "enter", "locate": xpath, "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ChangeDisplayPage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageBar(arg):
        '''
        ClickPageBar : Click page bar to change page number
                Input argu :
                        action_type - first_page / last_page / previous_page / next_page
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        action_type = arg['action_type']

        ##Click page bar
        xpath = Util.GetXpath({"locate": action_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click page bar", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickPageBar')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseDisplayAmount(arg):
        '''
        ChooseDisplayAmount : Choose how many add on deal display in overview page
                Input argu :
                        amount - 25 / 50 / 100 / 200
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        amount = arg["amount"]

        ##Click amount drop down list
        xpath = Util.GetXpath({"locate": "drop_down"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click drop down", "result": "1"})
        time.sleep(2)

        ##Choose add on deal amout
        xpath = Util.GetXpath({"locate": amount})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add on deal amount", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ChooseDisplayAmount')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductIdLink(arg):
        '''
        ClickProductIdLink : Click product id link
                Input argu :
                        model_id - which product id of model want to click hyperlink
                Return code :
                    1 - success
                    0 -  fail
                    -1 - error
        '''
        ret = 1
        model_id = arg["model_id"]

        ##Click product id link
        xpath = Util.GetXpath({"locate": "product_id_link"})
        xpath = xpath.replace("model_id", model_id)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click product id link", "result": "1"})

        OK(ret, int(arg['result']), 'AdminSellerDiscountPage.ClickProductIdLink')
