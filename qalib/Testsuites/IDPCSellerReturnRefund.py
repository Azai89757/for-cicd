import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCSellerReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSellerReturnRefund Precondition =="
        dumplogger.info("== Setup IDPCSellerReturnRefund Precondition ==")

    def test_IDPCSellerReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerReturnRefund-01.xml")

    def test_IDPCSellerReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerReturnRefund-02.xml")

    def test_IDPCSellerReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCSellerReturnRefund-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCSellerReturnRefund Post condition =="
        dumplogger.info("== Setup IDPCSellerReturnRefund Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
