import unittest
from Config import dumplogger
from FrameWorkBase import *


class BRAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup BRAPIPlaceOrder Precondition ==")

    def test_BRAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/BRAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup BRAPIPlaceOrder Post condition ==")
