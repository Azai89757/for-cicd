import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class MXAndroidCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAndroidCheckOut Precondition =="
        dumplogger.info("== Setup MXAndroidCheckOut Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"mx"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    # def test_MXAndroidCheckOut01(self):

    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidCheckOut-01.xml")

    # def test_MXAndroidCheckOut02(self):

    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidCheckOut-02.xml")

    # def test_MXAndroidCheckOut03(self):

    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidCheckOut-03.xml")

    # def test_MXAndroidCheckOut04(self):

    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidCheckOut-04.xml")

    # def test_MXAndroidCheckOut05(self):

    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/MX/MXAndroidCheckOut-05.xml")

    def test_MXAndroidCheckOut06(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidCheckOut-06.xml")

    def test_MXAndroidCheckOut07(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidCheckOut-07.xml")

    def test_MXAndroidCheckOut08(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidCheckOut-08.xml")

    def test_MXAndroidCheckOut09(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXAndroidCheckOut-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXAndroidCheckOut Post condition =="
        dumplogger.info("== Setup MXAndroidCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
