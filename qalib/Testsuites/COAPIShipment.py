import unittest
from Config import dumplogger
from FrameWorkBase import *


class COAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIShipment Precondition =="
        dumplogger.info("== Setup COAPIShipment Precondition ==")

    def test_COAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/COAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup COAPIShipment Post condition =="
        dumplogger.info("== Setup COAPIShipment Post condition ==")
