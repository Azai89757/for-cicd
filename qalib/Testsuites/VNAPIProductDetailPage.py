import unittest
from Config import dumplogger
from FrameWorkBase import *
import GlobalAdapter


class VNAPIProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIProductDetailPage Precondition =="
        dumplogger.info("== Setup VNAPIProductDetailPage Precondition ==")

    def test_VNAPIProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIProductDetailPage-01.xml")

    def tearDown(self):
        print GlobalAdapter.CommonVar._TestLinkID_
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIProductDetailPage Post condition =="
        dumplogger.info("== Setup VNAPIProductDetailPage Post condition ==")
