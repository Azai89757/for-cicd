import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCMediaStock(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMediaStock Precondition =="
        dumplogger.info("== Setup MYPCMediaStock Precondition ==")

    def test_MYPCMediaStock01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMediaStock-01.xml")

    def test_MYPCMediaStock02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMediaStock-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMediaStock Post condition =="
        dumplogger.info("== Setup MYPCMediaStock Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
