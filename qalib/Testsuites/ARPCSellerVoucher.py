import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCSellerVoucher Precondition =="
        dumplogger.info("== Setup ARPCSellerVoucher Precondition ==")

    def test_ARPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCSellerVoucher-01.xml")

    def test_ARPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCSellerVoucher-02.xml")

    def test_ARPCSellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCSellerVoucher-03.xml")

    def test_ARPCSellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCSellerVoucher Post condition =="
        dumplogger.info("== Setup ARPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver({})
