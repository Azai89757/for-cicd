import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCNotification(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print("== Setup INPCNotification Precondition ==")
        dumplogger.info("== Setup INPCNotification Precondition ==")

    def test_INPCNotification01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-01.xml")

    def test_INPCNotification02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-02.xml")

    def test_INPCNotification03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-03.xml")

    def test_INPCNotification04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-04.xml")

    def test_INPCNotification05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-05.xml")

    def test_INPCNotification06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-06.xml")

    def test_INPCNotification07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-07.xml")

    def test_INPCNotification08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-08.xml")

    def test_INPCNotification09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-09.xml")

    def test_INPCNotification10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-10.xml")

    def test_INPCNotification11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-11.xml")

    def test_INPCNotification12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCNotification-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print("== Setup INPCNotification Post condition ==")
        dumplogger.info("== Setup INPCNotification Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
