import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCReturnRefund(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCReturnRefund Precondition =="
        dumplogger.info("== Setup PHPCReturnRefund Precondition ==")

    def test_PHPCReturnRefund01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCReturnRefund-01.xml")

    def test_PHPCReturnRefund02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCReturnRefund-02.xml")

    def test_PHPCReturnRefund03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCReturnRefund-03.xml")

    def test_PHPCReturnRefund04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCReturnRefund-04.xml")

    def test_PHPCReturnRefund05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCReturnRefund-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCReturnRefund Post condition =="
        dumplogger.info("== Setup PHPCReturnRefund Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
