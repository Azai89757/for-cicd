﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidAddOnDealMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper

##Import Android library
import AndroidBaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidAddOnDealPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductName(arg):
        '''
        ClickProductName : click product name to go to product detail page
                Input argu :
                    product_name - go to specific product detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product name to go to product detail page
        xpath = Util.GetXpath({"locate": "product_name"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click product name", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealPage.ClickProductName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseProductModel(arg):
        '''
        ChooseProductModel : Click to choose product mode on add on deal section page
                Input argu :
                    name - product model name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click add to cart button
        xpath = Util.GetXpath({"locate": "product_mode"}).replace("product_name_for_replace", name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Choose product mode","result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealPage.ChooseProductModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddOnItemCheckbox(arg):
        '''
        ClickAddOnItemCheckbox : Click add on item checkbox
                Input argu :
                    product_name - product name which you will choose
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click add on item checkbox
        xpath = Util.GetXpath({"locate": "addon_item_checkbox"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add on item checkbox", "result": "1"})

        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidAddOnDealPage.ClickAddOnItemCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSelectGifts(arg):
        '''
        ClickSelectGifts : Click select gift to view gwp gift
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click select gift to view gwp gift
        xpath = Util.GetXpath({"locate": "select_gifts"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click select gift", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealPage.ClickSelectGifts')


class AndroidAddOnDealButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductModelDropDown(arg):
        '''
        ClickProductModelDropDown : click product model drop down menu btn
                Input argu :
                    product_name - product name which you will click drop down btn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg['product_name']

        ##Click product model drop down menu btn
        xpath = Util.GetXpath({"locate": "drop_down_menu_btn"})
        xpath = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click drop down menu btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealButton.ClickProductModelDropDown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click add on deal ok btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add on deal ok btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add on deal ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart button on add on deal page
                Input argu :N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click add to cart button
        xpath = Util.GetXpath({"locate": "cart_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click add to cart btn on add on deal page","result": "1"})

        OK(ret, int(arg['result']), 'AndroidAddOnDealButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddQuantity(arg):
        '''
        ClickAddQuantity : Click add quantity btn on add on deal page
                Input argu :
                        click_time - how many quantity you will add
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_time = int(arg["click_time"])

        ##Click add quantity btn
        for quantity in range(click_time):
            xpath = Util.GetXpath({"locate":"add_btn"})
            AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"click add button", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidAddOnDealButton.ClickAddQuantity')
