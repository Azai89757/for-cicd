import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class SGPCCoins(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCCoins Precondition =="
        dumplogger.info("== Setup SGPCCoins Precondition ==")

    def test_SGPCCoins01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-01.xml")

    def test_SGPCCoins02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-02.xml")

    def test_SGPCCoins03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-03.xml")

    def test_SGPCCoins04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-04.xml")

    def test_SGPCCoins05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-05.xml")

    def test_SGPCCoins06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-06.xml")

    def test_SGPCCoins07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCCoins-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCCoins Post condition =="
        dumplogger.info("== Setup SGPCCoins Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
