import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCTransparentBackgroundImage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCTransparentBackgroundImage Precondition =="
        dumplogger.info("== Setup MXPCTransparentBackgroundImage Precondition ==")

    '''def test_MXPCTransparentBackgroundImage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCTransparentBackgroundImage-01.xml")

    def test_MXPCTransparentBackgroundImage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCTransparentBackgroundImage-02.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCTransparentBackgroundImage Post condition =="
        dumplogger.info("== Setup MXPCTransparentBackgroundImage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
