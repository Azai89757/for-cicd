import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCPurchaseWithPurchase(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCPurchaseWithPurchase Precondition =="
        dumplogger.info("== Setup THPCPurchaseWithPurchase Precondition ==")

    def test_THPCPurchaseWithPurchase01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCPurchaseWithPurchase-01.xml")

    def test_THPCPurchaseWithPurchase02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCPurchaseWithPurchase-02.xml")

    def test_THPCPurchaseWithPurchase03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCPurchaseWithPurchase-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCPurchaseWithPurchase Post condition =="
        dumplogger.info("== Setup THPCPurchaseWithPurchase Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
