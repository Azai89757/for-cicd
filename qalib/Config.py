##Import common library
import os
import platform
import socket
import ctypes
import array
import tempfile
import getpass
import datetime
import time
import random
import string


#########################
## Detect os platform is windows/mac/linux
#########################
def CheckOSPlatform():

    if platform.system().lower() == "linux" or platform.system().lower() == "linux2":
        return "linux"
    elif platform.system().lower() == "darwin":
        return "darwin"
    elif platform.system().lower() == "windows":
        return "windows"
#########################

#########################
##System platform definition
#########################
dict_systempath = {"windows":"C:\\", "darwin":"/Library/", "linux":"/var/"}
dict_systemslash = {"windows":"\\", "darwin":"/", "linux":"/"}
_platform_ = CheckOSPlatform()
_SystemPath_ = dict_systempath[CheckOSPlatform()]
_SysDIR_ = os.environ['WINDIR'] if "WINDIR" in os.environ else "/Library"

#########################
##Common definition
#########################
_CurDIR_ = os.getcwd()
_CurDataDIR_ = os.path.realpath(os.path.abspath(os.path.dirname(os.getcwd())) + dict_systemslash[_platform_] + "QATestData")
_CurUser_ = getpass.getuser()
_CurFolderName_ = os.path.basename(os.getcwd())
_CurDateTime_ = datetime.datetime.fromtimestamp(time.time()).strftime("%Y%m%d-%H%M%S")
_CaseStartTime_ = ""
_LOCAL_PATH_ = os.getcwd()
_HostName_ = str(socket.gethostname()).lower()
_EnvPFBConfigINI_ = "EnvironmentConfig.ini"
_XMLOutputPath_ = _SystemPath_ + _CurFolderName_ + dict_systemslash[_platform_] + "xml"
_DataFilePath_ = _CurDataDIR_ + dict_systemslash[_platform_] + "data"
_JSONFilePath_ = _DataFilePath_ + dict_systemslash[_platform_] + "Json"
_UICaseDataFilePath_ = _DataFilePath_ + dict_systemslash[_platform_] + "uidata"
_APICaseDataFilePath_ = _DataFilePath_ + dict_systemslash[_platform_] + "apidata"
_DBCaseDataFilePath_ = _DataFilePath_ + dict_systemslash[_platform_] + "dbdata"
_INIFilePath_ = _DataFilePath_ + dict_systemslash[_platform_] + "common" + dict_systemslash[_platform_] + _EnvPFBConfigINI_
_SpexAPIConfigINI_ = _DataFilePath_ + dict_systemslash[_platform_] + "common" + dict_systemslash[_platform_] + "SpexAPIConfig.ini"
_DBConfigINI_ = _DataFilePath_ + dict_systemslash[_platform_] + "common" + dict_systemslash[_platform_] + "DBConfig.ini"
_ShopeeOSSConnectorINI_ = _DataFilePath_ + dict_systemslash[_platform_] + "common" + dict_systemslash[_platform_] + "ShopeeOSSConfig.ini"
_Exception_ = -1
_EspressoAPKPath_ = _SystemPath_ + "APK\\country.apk"
_EspressoKeyStorePath_ = _SystemPath_ + "APK\\debug.keystore"
_FolderSuffixString_ = '_' + ''.join(random.choice(string.ascii_letters) for x in range(6))
_TimeZone_ = {
    "VN": "Asia/Ho_Chi_Minh",
    "TH": "Asia/Bangkok",
    "PH": "Asia/Manila",
    "MX": "America/Mexico_City",
    "BR": "America/Araguaina",
    "MY": "Asia/Kuala_Lumpur",
    "SG": "Asia/Singapore",
    "CO": "America/Bogota",
    "CL": "America/Santiago",
    "ES": "Europe/Madrid",
    "PL": "Europe/Warsaw",
    "IN": "Asia/Kolkata",
    "FR": "Europe/Paris",
    "TW": "Asia/Taipei",
    "ID": "Asia/Jakarta",
    "AR": "America/Argentina/Buenos_Aires"
}

#########################
##API Related definition
#########################
_SecretKey_ = "2741c3bdf29277e0403040c1d87d1420"
_SpexRegResult_ = False
_SpexInstanceID_ = {}
_SpexConfigKey_ = ""
_SpexInstanceTag = ""
_HttpResponseValue_ = ""
_HttpResponseTextKey_ = ""

#########################
##Testcase Related definition
#########################
_CaseFailList_ = []
_RerunList_ = []
_CaseID_ = None
_TestCaseType_ = ""
_TestCasePlatform_ = ""
_TestCaseRegion_ = ""
_AllTestCaseType_ = []
_Step_ = False
_TestCaseFeature_ = ""
_CaseRelationShip_ = []
_RerunObj_ = None
_CaseRetried_ = 0
_TestLinkID_ = {}
_TestCaseDesc_ = {}

#########################
##DB Related definition
#########################
_DBConfig_ = {}

#########################
##Case Trigger Mode definition
#########################
_Headless_ = False
_i18NJson_ = {
    "TW":[],
    "VN":[],
    "ID":[],
    "TH":[],
    "SG":[],
    "PH":[],
    "MY":[],
    "BR":[],
    "MX":[],
    "CO":[],
    "CL":[],
    "PL":[],
    "ES":[],
    "IN":[],
    "FR":[],
    "AR":[]
}
_BuildNo_ = ""
_SwitchEnv_ = None
_EnvType_ = "staging"
_TriggerCaseMode_ = ""
_PFB_ = {}
_UsingEmulator_ = None
_RemoteSeleniumDriver_ = None

#########################
##Device Info definition
#########################
_DeviceID_ = ""
_AvdName_ = ""
_DeviceNumber_ = 1
_DeviceCPUUsage_ = 0
_DeviceHostIP_ = socket.gethostbyname(socket.gethostname())
'''_MobileDeviceInfo_ = {
    "4d1b1d0a":{
        "device_name":"Mix 2",
        "device_platform_version":""},
    "988a1b38313637515930":{
        "device_name":"Note 8",
        "device_platform_version":""},
    "J5AXB761N272L96":{
        "device_name":"Zenfone 5",
        "device_platform_version":""},
    "b91580c":{
        "device_name":"Mix 5",
        "device_platform_version":""},
    "401fc63f0504":{
        "device_name":"Red Mi Note 4X",
        "device_platform_version":""},
    "f456cc3f4c4d323584ddc1b54e48dac1042b5aca":{
        "device_name":"Iphone10",
        "device_platform_version":""},
    "2bb9623a3f017ece":{
        "device_name":"Samsung Galaxy S9",
        "device_platform_version":""},
    "8B1X11XXE":{
        "device_name":"Google Pixel 3",
        "device_platform_version":""},
    "emulator-5554":{
        "device_name":"emulator-5554",
        "device_platform_version":""},
    "emulator-5556":{
        "device_name":"emulator-5556",
        "device_platform_version":""},
    "emulator-5558":{
        "device_name":"emulator-5558",
        "device_platform_version":""},
    "emulator-5560":{
        "device_name":"emulator-5560",
        "device_platform_version":""},
    "emulator-5562":{
        "device_name":"emulator-5562",
        "device_platform_version":""},
    "emulator-5564":{
        "device_name":"emulator-5564",
        "device_platform_version":""},
    "emulator-5566":{
        "device_name":"emulator-5566",
        "device_platform_version":""},
    "emulator-5568":{
        "device_name":"emulator-5568",
        "device_platform_version":""},
    "emulator-5570":{
        "device_name":"emulator-5570",
        "device_platform_version":""},
    "emulator-5572":{
        "device_name":"emulator-5572",
        "device_platform_version":""},
    "emulator-5574":{
        "device_name":"emulator-5574",
        "device_platform_version":""},
    "emulator-5576":{
        "device_name":"emulator-5576",
        "device_platform_version":""},
    "emulator-5578":{
        "device_name":"emulator-5578",
        "device_platform_version":""},
    "emulator-5580":{
        "device_name":"emulator-5580",
        "device_platform_version":""},
    "emulator-5582":{
        "device_name":"emulator-5582",
        "device_platform_version":""},
    "emulator-5584":{
        "device_name":"emulator-5584",
        "device_platform_version":""},
    "S475Q8ZTGQBA7SI7":{
        "device_name":"OPPO",
        "device_platform_version":""},
    "4EY5T18811001567":{
        "device_name":"huawei",
        "device_platform_version":""},
    "00008020-000D38D60146002E":{
        "device_name":"iPhoneX",
        "device_platform_version":""},
    "c1688908": {
        "device_name": "vivo V15",
        "device_platform_version": ""},
    "SOFY9DQOU8PRIJPJ": {
        "device_name": "vivo Y17",
        "device_platform_version": ""},
    "dcf7444": {
        "device_name": "redmi Note7",
        "device_platform_version": ""},
    "02f3b45633bd2e78d43a128e2d069a4557e1111d":{
        "device_name":"Iphone7",
        "device_platform_version":""},
    "kbi7gmk7eqwwfuv4": {
        "device_name": "redmi Note8",
        "device_platform_version": ""},
    "JCAZHM000545CF9": {
        "device_name": "ASUS M6",
        "device_platform_version": ""},
    "0ff6071f7212": {
        "device_name": "XiaoMi One",
        "device_platform_version": ""},
    "R58MB3TGC6E": {
        "device_name": "Samsung A20",
        "device_platform_version": ""},
    "00008020-001179812ED9002E": {
        "device_name": "iPhone XS",
        "device_platform_version": ""},
    "02f3b45633bd2e78d43a128e2d069a4557e1111d":{
        "device_name":"iPhone7",
        "device_platform_version":"",
        "device_x":"319",
        "device_y":"566"},
    "f456cc3f4c4d323584ddc1b54e48dac1042b5aca":{
        "device_name":"iPhone X",
        "device_platform_version":"",
        "resolution_x": "1125",
        "resolution_y": "2436",
        "device_x":"375",
        "device_y":"810"},
    "3687385f": {
        "device_name": "redmi Note8",
        "device_platform_version": ""},
    "R58N2381SLK": {
        "device_name": "Samsung Galaxy A20",
        "device_platform_version": ""},
    "RF8N603QB3K": {
        "device_name": "Samsung Galaxy A21s",
        "device_platform_version": ""},
    "R58N729NX2N":{
        "device_name":"Samsung Galaxy A51",
        "device_platform_version":""},
    "86d9456b": {
        "device_name": "Vivo 1818",
        "device_platform_version": ""},
    "19ce7536": {
        "device_name": "Redmi Note 8T",
        "device_platform_version": ""},
    "RF8N51Q2NLP": {
        "device_name": "Samsung Galaxy A31",
        "device_platform_version": ""},
    "R58M51AV25J":{
        "device_name": "Samsung Galaxy A50",
        "device_platform_version": ""}
    }
'''
_DesiredCaps_ = {
        ##General
        "automationName": "",
        "platformName": "Android",
        "deviceName": "device1",
        "udid": "",
        "appPackage": "",
        "chromeDriverPort": "5566",
        "systemPort": "8201",
        "normalizeTagNames": True,
        "noReset": True,
        'newCommandTimeout': '180000',
        'platformVersion': '',
        'appActivity': 'com.shopee.app.ui.home.HomeActivity_',
        'autoWebviewTimeout': '40000',
        # "app": "C:\\APK\\country.apk",
        'adbExecTimeout': '40000',
        'appWaitDuration': '30000'

        ##espresso
        # "showGradleLog": "",
        # "useKeystore": "",
        # "keystorePath": "",
        # "keystorePassword": "",
        # "keyAlias": "",
        # "keyPassword": "",
        # "app": "",
        # "forceEspressoRebuild": "",
        # "appWaitActivity": "",
        # "espressoBuildConfig": "",
        # "espressoServerLaunchTimeout":""

        ##uiautomator2
        # 'unicodeKeyboard': True,
        # 'resetKeyboard': True,
        # 'wdaStartupRetries': '4',
        # 'iosInstallPause': '8000',
        # 'wdaStartupRetryInterval': '20000',
        # 'uiautomator2ServerLaunchTimeout': '180000',
        # 'uiautomator2ServerReadTimeout': '360000'
        }

_AdbCommands_ = {
        'ExpandNotification': ' shell cmd statusbar expand-notifications',
        'CollapseNotification': ' shell cmd statusbar collapse',
        'GetDeviceInfo': ' shell getprop ro.build.version.release',
        'GetPackageName': ' shell pm path ',
        'GetShopeeVersion': ' shell ',
        'CloseAnimation': ' shell am broadcast -a io.appium.settings.data_connection --es setstatus disable',
        'KeyboardInput': ' shell input keyevent ',
        'CleanAppData': ' shell pm clear ',
        'Install': ' install -r ',
        'Uninstall': ' uninstall ',
        'Intent': ' shell am start -a ',
        'IntentUrl': ' shell am start -a "android.intent.action.VIEW" -d "',
        'LaunchAPP': ' shell am start -n ',
        'StopAPP': ' shell am force-stop ',
        'SetIME': ' shell ime set ',
        'EnableIME': ' shell ime enable ',
        'CheckIME': ' shell ime list -s',
        'GetFirstIME': ' shell "ime list -s | head -n1"',
        'GrantAPPAccess': ' shell pm grant ',
        'GrantAndroidAccessType':{
                                    'Set_Animation_Scale':' android.permission.SET_ANIMATION_SCALE',
                                    'Camera':' android.permission.CAMERA',
                                    'SMS':' android.permission.READ_SMS',
                                    'Write_External_Storage':' android.permission.WRITE_EXTERNAL_STORAGE',
                                    'Read_External_Storage':' android.permission.READ_EXTERNAL_STORAGE',
                                    'Access_Fine_Location':' android.permission.ACCESS_FINE_LOCATION',
                                    'Access_Coarse_Location':' android.permission.ACCESS_COARSE_LOCATION',
                                    'Access_Background_Location':' android.permission.ACCESS_BACKGROUND_LOCATION',
                                    'Audio':' android.permission.RECORD_AUDIO',
                                    'Contacts':' android.permission.READ_CONTACTS',
                                    'Alert':' android.permission.SYSTEM_ALERT_WINDOW',
                                    'Write_Calendar':' android.permission.WRITE_CALENDAR',
                                    'Read_Calendar':' android.permission.READ_CALENDAR'
                                    },
        'SetAirplaneMode': ' shell settings put global airplane_mode_on ',
        'SetHttpProxy': ' shell settings put global http_proxy ',
        'AddContact': ' shell am start -a android.intent.action.INSERT -t vnd.android.cursor.dir/contact -e ',
        'DeleteContact': ' shell pm clear com.android.providers.contacts ',
        'OpenAPPfromBackground': ' shell monkey -p ',
        'ChangeDeviceTime': ' shell su 0 toybox date ',
        'RevokeAPPAccess': ' shell pm revoke ',
        'DumpDOMTree': ' shell uiautomator dump',
        'CopyFile': ' pull ',
        'Dumpsys': ' shell dumpsys ',
        'DumpsysActivity': ' shell dumpsys activity top ',
        'DumpsysConnectivity': ' shell dumpsys connectivity ',
        'Swipe': ' shell input swipe ',
        'SetMobileNetwork': ' shell svc data ',
        'GetDeviceCpuMem': ' shell top -n 1 | grep --line-buffered "cpu\|com.shopee.',
        'Permission': ' root',
        'ClickCoordinates': ' shell input tap '
        }

_iOSDesiredCaps_ = {
    "capabilities": {
        "alwaysMatch": {
            "bundleId": "com.beeasy.shopee.sg",
        }
    }
}

_AppiumDevicePort_ = {
    'chrome_driver_port':10000,
    'appium_client_port':11000,
    'system_port':12000
}

#########################
##Log Related definition
#########################
_OutputFolderName_ = "QATest_Output"
_OutputFolderPath_ = _SystemPath_ + _OutputFolderName_ + dict_systemslash[_platform_] + _CurDateTime_.split("-")[0] + dict_systemslash[_platform_] + _HostName_ + dict_systemslash[_platform_] + _CurDateTime_.split("-")[1] + _FolderSuffixString_
_NULL_ = open(os.devnull, "wb")
_LogFile_ = _OutputFolderPath_ + dict_systemslash[_platform_] + "QATest.log"
_LogFolder_ = "log"
_LogServerIP_ = "192.168.66.248"
_LogServerPort_ = 80
_LogServerPath_ = "/Loger.php"
_LogServerTimeout_ = 3
_LogCurDateTime_ = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
_LogDB_ = 1

#########################
##Selenium Driver definition
#########################
_ChromeDriverEXE_ = {"linux":"chromedriver_linux64_", "darwin":"chromedriver_mac64_", "windows":"chromedriver_win32_"}
_ChromeDriverVersion_ = "102"
_IEDriverVersion_ = "3.14"
_DriverBinaryPath_ = _CurDataDIR_ + dict_systemslash[_platform_] + "bin"
_ChromeDriver_ = _DriverBinaryPath_ + dict_systemslash[_platform_] + _ChromeDriverEXE_[CheckOSPlatform()] + _ChromeDriverVersion_
_InstantWaitingPeriod_ = 5
_ShortWaitingPeriod_ = 10
_AverageWaitingPeriod_ = 15
_GeneralWaitingPeriod_ = 25
_LongWaitingPeriod_ = 60
_SeleniumGridUrl_ = {"1": "10.45.32.120:4444", "2": "10.45.32.121:4444", "3": "10.45.32.122:4444", "4": "10.45.32.123:4444", "5": "10.45.32.124:4444"}

#########################
##DB white list definition
#########################
_WLForAutoLogDB_ = [
    "LICHUNGde-Mac-mini.local",
    "twqa-test-host1",
    "twqa-test-host2",
    "twqa-test-host3",
    "twqa-test-host4",
    "twqa-test-host5",
    "twqa-test-host6",
    "twqa-test-hosta",
    "twqa-test-hostb",
    "twqa-test-hostc",
    "twqa-test-hostd",
    "twqa-test-hoste",
    "twqa-test-hostf",
    "twqa-test-hostg",
    "twqa-test-hosth",
    "twqa-test-hosti",
    "twqa-test-hostj",
    "twqa-test-hostk"
    ]
_WLForAutoFineTuneDB_ = ["SHNB-TERRY_TSAI", "SHNB-JESS_CHU", "SHNB-JEAN_WEI", "SHNB-2306", "SHNB-2335", "SHNB-JOKER_LU", "SHNB-DANIEL_CHUANG"]
_PSIDForAutoFineTuneDB_ = ["1096", "1709", "1755", "2306", "2335", "2590", "2558"]

#########################
##Proxy Server definition
#########################
_AnyProxyPath_ = _SystemPath_ + dict_systemslash[_platform_] + "anyproxy" + dict_systemslash[_platform_] + "bin" + dict_systemslash[_platform_] + "anyproxy"
_EnableProxy_ = False
_ProxyInfo_ = {
    "port":8000,
    "webport":9000
}

#########################
##Local information
#########################
def GetWinSysInfo():

    dict_winsysinfo = {}

    _bit_ = None
    key = None

    ##x86 or x64
    arch = platform.architecture()[0]
    _bit_ = "x86" if "32" in arch else "x64"

    ##Search corresponding registry by x86/x64
    if (platform.architecture()[0] == '64bit'):
        key = OpenKey(HKEY_LOCAL_MACHINE, r'SOFTWARE\Microsoft\Windows NT\CurrentVersion', 0, KEY_QUERY_VALUE | KEY_WOW64_64KEY)
    else:
        key = OpenKey(HKEY_LOCAL_MACHINE, r'SOFTWARE\Microsoft\Windows NT\CurrentVersion', 0, KEY_QUERY_VALUE)

    ##Query win system info from registry
    dict_winsysinfo["os_platform"], dict_winsysinfo["os_productname"] = QueryValueEx(key, 'ProductName')

    ##Hostname
    dict_winsysinfo["os_hostname"] = str(socket.gethostname())

    ##Win IE temporary folder
    dict_winsysinfo["ie_tmpfolder"] = tempfile.gettempdir().strip('temp') + 'Microsoft\\Windows\\Temporary Internet Files\\'

    return dict_winsysinfo
#########################

#########################
##Associate functions
#########################
def getFileInfo(filename, info):
    """
    Extract information from a file.
    """
    try:
        ##Get size needed for buffer (0 if no info)
        size = ctypes.windll.version.GetFileVersionInfoSizeA(filename, None)
        #print size

        ##If no info in file -> empty string
        if not size:
            return ''

        ##Create buffer
        res = create_string_buffer(size)
        ##Load file informations into buffer res
        ctypes.windll.version.GetFileVersionInfoA(filename, None, size, res)
        rf = c_uint()
        lf = c_uint()

        ##Look for codepages
        ctypes.windll.version.VerQueryValueA(res, '\\VarFileInfo\\Translation',byref(rf), byref(lf))
        ##If no codepage -> empty string
        if not l.value:
            return ''

        ##Take the first codepage (what else ?)
        codepages = array.array('H', string_at(rf.value, lf.value))
        codepage = tuple(codepages[:2].tolist())

        ##Extract information
        ctypes.windll.version.VerQueryValueA(res, ('\\StringFileInfo\\%04x%04x\\' + info) % codepage, byref(rf), byref(lf))
        ver = string_at(rf.value, lf.value)
        ver = ver.split(".")
        if len(ver) > 3:
            ver = ver[0] + "." + ver[1] + "-" + ver[3].strip('\x00')
        elif len(ver) == 1:
            ver = ver[0].strip('\x00')
        print "Driver version: %s" % (ver)
        return ver

    except Exception, err:
        print "Exception raised, message:"
        print err
        if err[0] == 2:
            print "It looks like driver is not properly installed, exit.."
            exit()
        exit()
#########################

#########################
##Import splib first
#########################
if _platform_ is "darwin" or _platform_ is "linux":
    import splib

#########################
##Debug Log Initial
#########################
def CheckAndCreateDebugFolder():
    ''' CheckAndCreateDebugFolder : Check debug folder existance and create one if don't
            Input argu :
                N/A
            Return code:
                N/A
            Note: None
    '''

    ##If log output folder not exist, created !!
    if os.path.exists(_OutputFolderPath_):
        print _OutputFolderPath_ + "path exist"
    else:
        ##Execute command to create log folder by current date
        if _platform_ is "darwin" or _platform_ is "linux":
            command = "mkdir -p " + _OutputFolderPath_
        else:
            command = "mkdir " + _OutputFolderPath_
        os.system(command)

##Initial dumplogger and apilogger
from Logger import InitialDebugLog
CheckAndCreateDebugFolder()
dumplogger = InitialDebugLog(logger_name='Dump_QATest', logger_type='RotatingFileHandler')
apilogger = InitialDebugLog(logger_name='Api_QATest', logger_type='RotatingFileHandler')
android_performance_logger = InitialDebugLog(logger_name='Android_Performance', logger_type='RotatingFileHandler')
chrome_logger = InitialDebugLog(logger_name='Chrome_Recorder', logger_type='RotatingFileHandler')
