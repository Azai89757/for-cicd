import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpSearch(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSearch Precondition =="
        dumplogger.info("== Setup TWHttpSearch Precondition ==")

    def test_TWHttpSearch01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSearch-01.xml")

    def test_TWHttpSearch02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSearch-02.xml")

    # def test_TWHttpSearch03(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/api/http/TWHttpSearch-03.xml")

    def test_TWHttpSearch04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSearch-04.xml")

    # def test_TWHttpSearch05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/api/http/TWHttpSearch-05.xml")

    def test_TWHttpSearch06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSearch-06.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSearch Post condition =="
        dumplogger.info("== Setup TWHttpSearch Post condition ==")
