﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminIBankingMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import common library
import Util
import Config
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore

##import db library
from db import DBCommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def LaunchIBankingAdmin(arg):
    '''
    LaunchIBankingAdmin : go to ibanking admin
        Input argu : N/A
        Return code :
            1 - success
            0 - fail
            -1 - error
    '''

    ret = 1
    cyberpay_cookie = {}

    if 'Cyberpay' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:

        ##Using sql command to get cookie name and value in specific env and country
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "ibanking", "method":"select", "verify_result":"", "assign_data_list":"", "store_data_list":store_list, "result":"1"})

        ##Store cookie name and value
        cyberpay_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        cyberpay_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        cyberpay_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['Cyberpay'] = [cyberpay_cookie]

    ##Go to ibanking admin
    url = "https://test-integration.cyberpay." + Config._TestCaseRegion_.lower() + "/provider/ibanking_txn/"
    dumplogger.info("url = %s" % (url))

    BaseUICore.GotoURL({"url":url, "result":"1"})
    time.sleep(5)

    ##Set cookies to login
    BaseUICore.SetBrowserCookie({"storage_type":"Cyberpay", "result":"1"})
    time.sleep(5)

    ##Go to ibanking admin
    BaseUICore.GotoURL({"url":url, "result":"1"})
    BaseUICore.PageHasLoaded({"result":"1"})

    OK(ret, int(arg['result']), 'LaunchIBankingAdmin')


class AdminIBankingTransactionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToIBankingTransactionDetail(arg):
        '''
        GoToIBankingTransactionDetail : Search ibanking transaction
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Input partner ref field
        xpath = Util.GetXpath({"locate": "partner_ref"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PaymentE2EVar._SPMChannelRef_, "result":"1"})

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result":"1"})
        time.sleep(5)

        ##Click detail link
        xpath = Util.GetXpath({"locate":"detail"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click detail", "result":"1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AdminIBankingTransactionPage.GoToIBankingTransactionDetail')
