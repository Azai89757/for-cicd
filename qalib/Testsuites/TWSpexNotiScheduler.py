import unittest
from Config import dumplogger
from FrameWorkBase import *
from api.spex import SpexAPICore
import Config


class TWSpexNotiScheduler(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexNotiScheduler Precondition =="
        dumplogger.info("== Setup TWSpexNotiScheduler Precondition ==")

        ##Tail log file to grep "Local connections to /run/spex/spex.sock:-2 forwarded to remote address /run/spex/spex.sock"
        ##If Spex InitResult is True, will not call RegisterToSpex() again
        if SpexAPICore.InspectSpexConnection():
            ##Register to Spex with specific type of spex configuration for the first time
            if not Config._SpexRegResult_:
                if SpexAPICore.GetSpexConfig('NotiScheduler_Staging'):
                    Config._SpexRegResult_ = SpexAPICore.RegisterToSpex()

                    ##Check outcome after register
                    if Config._SpexRegResult_:
                        print "== Setup TWSpexNotiScheduler Register Success =="
                        dumplogger.info("== Setup TWSpexNotiScheduler Register Success ==")

                    else:
                        print ("Spex Register Failed!!!")
                        dumplogger.error("Spex Register Failed")
                        ##Directly Leave Setup
                        self.skipTest("")
                else:
                    dumplogger.error("Get Spex config from ini failed!!!")
                    self.skipTest("")
            else:
                dumplogger.info("Spex is already registered.")

        else:
            print "== Setup TWSpexNotiScheduler Precondition Fail by No Spex Agent Connection =="
            dumplogger.error("== Setup TWSpexNotiScheduler Precondition Fail by No Spex Agent Connection ==")
            ##Directly Leave Setup
            self.skipTest("")

    def test_TWSpexNotiScheduler01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-01.xml")

    def test_TWSpexNotiScheduler02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-02.xml")

    def test_TWSpexNotiScheduler03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-03.xml")

    def test_TWSpexNotiScheduler04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-04.xml")

    def test_TWSpexNotiScheduler05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-05.xml")

    def test_TWSpexNotiScheduler06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-06.xml")

    def test_TWSpexNotiScheduler07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/spex/TWSpexNotiScheduler-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWSpexNotiScheduler Post condition =="
        dumplogger.info("== Setup TWSpexNotiScheduler Post condition ==")
