import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCMicrosite Precondition =="
        dumplogger.info("== Setup ESPCMicrosite Precondition ==")

    def test_ESPCMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-01.xml")

    def test_ESPCMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-02.xml")

    def test_ESPCMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-03.xml")

    def test_ESPCMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-04.xml")

    def test_ESPCMicrosite05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-05.xml")

    def test_ESPCMicrosite06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-06.xml")

    def test_ESPCMicrosite07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-07.xml")

    def test_ESPCMicrosite08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-08.xml")

    def test_ESPCMicrosite09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCMicrosite-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCMicrosite Post condition =="
        dumplogger.info("== Setup ESPCMicrosite Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
