import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCHomePage Precondition =="
        dumplogger.info("== Setup MXPCHomePage Precondition ==")

    def test_MXPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCHomePage-01.xml")

    def test_MXPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCHomePage-02.xml")

    '''def test_MXPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCHomePage Post condition =="
        dumplogger.info("== Setup MXPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
