import unittest
from Config import dumplogger
from FrameWorkBase import *


class PHAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup PHAPIPlaceOrder Precondition ==")

    def test_PHAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/PHAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup PHAPIPlaceOrder Post condition ==")
