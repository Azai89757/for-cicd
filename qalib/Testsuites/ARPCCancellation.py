import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ARPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCCancellation Precondition =="
        dumplogger.info("== Setup ARPCCancellation Precondition ==")

    # def test_ARPCCancellation01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCancellation-01.xml")

    # def test_ARPCCancellation02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCancellation-02.xml")

    def test_ARPCCancellation03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCCancellation-03.xml")

    def test_ARPCCancellation04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/AR/ARPCCancellation-04.xml")

    # def test_ARPCCancellation05(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/AR/ARPCCancellation-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ARPCCancellation Post condition =="
        dumplogger.info("== Setup ARPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
