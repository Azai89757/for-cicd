import unittest
from Config import dumplogger
from FrameWorkBase import *


class BRAPIShipment(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRAPIShipment Precondition =="
        dumplogger.info("== Setup BRAPIShipment Precondition ==")

    def test_BRAPIShipment01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/BRAPIShipment-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRAPIShipment Post condition =="
        dumplogger.info("== Setup BRAPIShipment Post condition ==")
