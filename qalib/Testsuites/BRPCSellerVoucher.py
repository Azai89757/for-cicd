import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCSellerVoucher(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSellerVoucher Precondition =="
        dumplogger.info("== Setup BRPCSellerVoucher Precondition ==")

    def test_BRPCSellerVoucher01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerVoucher-01.xml")

    def test_BRPCSellerVoucher02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerVoucher-02.xml")

    def test_BRPCSellerVoucher03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerVoucher-03.xml")

    def test_BRPCSellerVoucher04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCSellerVoucher-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCSellerVoucher Post condition =="
        dumplogger.info("== Setup BRPCSellerVoucher Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
