import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCLogin Precondition =="
        dumplogger.info("== Setup MYPCLogin Precondition ==")

    def test_MYPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCLogin-01.xml")

    def test_MYPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCLogin Post condition =="
        dumplogger.info("== Setup MYPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
