import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDHttpHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpHomePage Precondition =="
        dumplogger.info("== Setup IDHttpHomePage Precondition ==")

    def test_IDHttpHomePage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpHomePage-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpHomePage Post condition =="
        dumplogger.info("== Setup IDHttpHomePage Post condition ==")
