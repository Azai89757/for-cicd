import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCProductCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCProductCollection Precondition =="
        dumplogger.info("== Setup MXPCProductCollection Precondition ==")

    def test_MXPCProductCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-01.xml")

    def test_MXPCProductCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-02.xml")

    def test_MXPCProductCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-03.xml")

    def test_MXPCProductCollection04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-04.xml")

    def test_MXPCProductCollection05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-05.xml")

    def test_MXPCProductCollection06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-06.xml")

    def test_MXPCProductCollection07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCProductCollection-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCProductCollection Post condition =="
        dumplogger.info("== Setup MXPCProductCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
