import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class INAndroidCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INAndroidCheckOut Precondition =="
        dumplogger.info("== Setup INAndroidCheckOut Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"in"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_INAndroidCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-01.xml")

    # def test_INAndroidCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/IN/INAndroidCheckOut-02.xml")

    def test_INAndroidCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-03.xml")

    def test_INAndroidCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-04.xml")

    def test_INAndroidCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-05.xml")

    def test_INAndroidCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-06.xml")

    def test_INAndroidCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-07.xml")

    def test_INAndroidCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-08.xml")

    # def test_INAndroidCheckOut09(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/IN/INAndroidCheckOut-09.xml")

    def test_INAndroidCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-10.xml")

    def test_INAndroidCheckOut11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INAndroidCheckOut-11.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INAndroidCheckOut Post condition =="
        dumplogger.info("== Setup INAndroidCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
