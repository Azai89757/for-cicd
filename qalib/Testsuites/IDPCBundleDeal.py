import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCBundleDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCBundleDeal Precondition =="
        dumplogger.info("== Setup IDPCBundleDeal Precondition ==")

    def test_IDPCBundleDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCBundleDeal-01.xml")

    def test_IDPCBundleDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCBundleDeal-02.xml")

    def test_IDPCBundleDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCBundleDeal-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCBundleDeal Post condition =="
        dumplogger.info("== Setup IDPCBundleDeal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
