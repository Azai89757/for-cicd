import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class FRPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCCart Precondition =="
        dumplogger.info("== Setup FRPCCart Precondition ==")

    def test_FRPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCart-01.xml")

    def test_FRPCCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCart-02.xml")

    def test_FRPCCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCart-03.xml")

    def test_FRPCCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/FR/FRPCCart-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup FRPCCart Post condition =="
        dumplogger.info("== Setup FRPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
