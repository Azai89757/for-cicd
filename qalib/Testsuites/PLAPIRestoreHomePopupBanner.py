import unittest
from Config import dumplogger
from FrameWorkBase import *


class PLAPIRestoreHomePopupBanner(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAPIRestoreHomePopupBanner Precondition =="
        dumplogger.info("== Setup PLAPIRestoreHomePopupBanner Precondition ==")

    def test_PLAPIRestoreHomePopupBanner01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/PLAPIRestoreHomePopupBanner-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLAPIRestoreHomePopupBanner Post condition =="
        dumplogger.info("== Setup PLAPIRestoreHomePopupBanner Post condition ==")
