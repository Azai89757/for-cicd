#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 PageTemplatesMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

@DecoratorHelper.FuncRecorder
def GoToProductLabelPage(arg):
    '''
    GoToProductLabelPage : Go to Product label page by label id
            Input argu :
                label_id - product label id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    label_id = arg["label_id"]

    url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/l/product_label/" + label_id
    dumplogger.info("url = " + url)

    ##Go to url
    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(2)

    OK(ret, int(arg['result']), 'GoToProductLabelPage')

@DecoratorHelper.FuncRecorder
def GoToPersonalizedCollectionLandingPage(arg):
    '''
    GoToPersonalizedCollectionLandingPage : Go to Personalized collection page by collection id
            Input argu :
                collection_id - collection id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    collection_id = arg["collection_id"]

    url = "https://" + GlobalAdapter.UrlVar._Domain_ + "/l/personalized_collection/" + collection_id
    dumplogger.info("url = " + url)

    ##Go to url
    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(2)

    OK(ret, int(arg['result']), 'GoToPersonalizedCollectionLandingPage')


class ProductLabelPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductSortTab(arg):
        '''
        SelectProductSortTab : Click product sort tab
                Input argu :
                        sort_tab - popular / latest / top_sales / price_high_to_low / price_low_to_high
                Return code : 1 - success
                           0 - fail
                            -1 - error
        '''
        ret = 1
        sort_tab = arg["sort_tab"]
        xpath = Util.GetXpath({"locate":sort_tab})

        if sort_tab in ("price_high_to_low", "price_low_to_high"):
            xpath_drop_down = Util.GetXpath({"locate":"price_drop_down_list"})

            ##Select tab
            BaseUICore.Move2ElementAndClick({"method":"xpath", "locate":xpath_drop_down, "locatehidden":xpath, "message":"Choose price sort tab from price drop down list", "result": "1"})
        else:
            ##Click sort tab
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click sort tab", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPage.SelectProductSortTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickPageController(arg):
        '''
        ClickPageController : Click next page or previous page button
                Input argu :
                    type - next / previous
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click next page or previous page button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click %s page button" % (type), "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'ProductLabelPage.ClickPageController')


class ShopCollectionPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShopCollectionCard(arg):
        '''
        ClickShopCollectionCard : Click shop collection card
                Input argu :
                    card_order - the order of the cards on the page
                    layout - 1 or 2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_order = arg["card_order"]
        layout = arg["layout"]

        ##Click collection card
        xpath = Util.GetXpath({"locate": "layout_" + layout})
        xpath = xpath.replace("string_to_be_replaced", card_order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click collection card", "result": "1"})

        OK(ret, int(arg['result']), 'ShopCollectionPage.ClickShopCollectionCard')


class ShopCollectionButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickFollow(arg):
        '''
        ClickFollow : Click follow button at shop collection page
                Input argu :
                    card_order - the order of the cards on the page
                    layout - 1 or 2
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        card_order = arg["card_order"]
        layout = arg["layout"]

        ##Click follow button
        xpath = Util.GetXpath({"locate": "layout_" + layout})
        xpath = xpath.replace("string_to_be_replaced", card_order)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click follow button", "result": "1"})

        OK(ret, int(arg['result']), 'ShopCollectionButton.ClickFollow')


class AllCampaignPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeCampaignTab(arg):
        '''
        ChangeCampaignTab : Change campaign tab
                Input argu :
                    tab_name - campaign tab name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        tab_name = arg["tab_name"]
        xpath = Util.GetXpath({"locate":"tab"})
        xpath = xpath.replace("string_to_be_replaced", tab_name)

        ##Hover on element
        BaseUICore.MoveToElementCoordinate({"locate": Util.GetXpath({"locate":"see_all"}), "is_click":"0", "result": "1"})

        ##Click sort tab
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click campaign tab", "result": "1"})

        OK(ret, int(arg['result']), 'AllCampaignPage.ChangeCampaignTab')


class PageTemplatesComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
            Input argu :
                button_type - type of button which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'PageTemplatesComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnLabel(arg):
        '''
        ClickOnLabel : Click any type of label with well defined locator
            Input argu :
                label_type - type of label which defined by caller
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        label_type = arg["label_type"]

        ##Click on label
        xpath = Util.GetXpath({"locate": label_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click label: %s, which xpath is %s" % (label_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'PageTemplatesComponent.ClickOnLabel')
