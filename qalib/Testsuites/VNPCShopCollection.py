import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCShopCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopCollection Precondition =="
        dumplogger.info("== Setup VNPCShopCollection Precondition ==")

    def test_VNPCShopCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopCollection-01.xml")

    def test_VNPCShopCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopCollection-02.xml")

    def test_VNPCShopCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCShopCollection-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCShopCollection Post condition =="
        dumplogger.info("== Setup VNPCShopCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
