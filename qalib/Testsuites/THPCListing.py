import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCListing Precondition =="
        dumplogger.info("== Setup THPCListing Precondition ==")

    def test_THPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCListing-01.xml")

    def test_THPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCListing-02.xml")

    def test_THPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCListing-03.xml")

    def test_THPCListing04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCListing-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCListing Post condition =="
        dumplogger.info("== Setup THPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
