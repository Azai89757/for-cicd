import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCShopCollection(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCShopCollection Precondition =="
        dumplogger.info("== Setup MYPCShopCollection Precondition ==")

    def test_MYPCShopCollection01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCShopCollection-01.xml")

    def test_MYPCShopCollection02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCShopCollection-02.xml")

    def test_MYPCShopCollection03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCShopCollection-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCShopCollection Post condition =="
        dumplogger.info("== Setup MYPCShopCollection Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
