﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 BundleDealMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import FrameWorkBase
import DecoratorHelper

##Import Web library
import BaseUICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class BundleDealPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCollectionItem(arg):
        '''
        ClickCollectionItem : Click collection item in product detail page
                Input argu :
                    collection_item - click collection item name to go to product detail page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        collection_item = arg['collection_item']

        ##Click collection item in product detail page
        xpath = Util.GetXpath({"locate": "product_name"})
        xpath = xpath.replace('replaced_text', collection_item)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click collection item", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealPage.ClickCollectionItem')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStickTopProduct(arg):
        '''
        ClickStickTopProduct : Click stick top product
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click stick top product
        xpath = Util.GetXpath({"locate": "stick_top_product_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click stick top product", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealPage.ClickStickTopProduct')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectProductModel(arg):
        '''
        SelectProductModel : select product model to add specific item to cart
                Input argu :
                    model_name - add specific model to cart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        model_name = arg['model_name']

        ##Select product model
        xpath = Util.GetXpath({"locate": "model_name_btn"})
        xpath = xpath.replace("model_name_to_be_replace", model_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click model name btn", "result": "1"})
        time.sleep(3)

        ##Click add to cart
        xpath = Util.GetXpath({"locate": "add_to_cart_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'BundleDealPage.SelectProductModel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CloseExclamationMarkMessage(arg):
        '''
        CloseExclamationMarkMessage : Close message pop from exclamation mark
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click close button pop from exclamation mark
        xpath = Util.GetXpath({"locate": "close_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Close message pop from exclamation mark", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealPage.CloseExclamationMarkMessage')


class BundleDealButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeAll(arg):
        '''
        ClickSeeAll : Click see all btn in pdp
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click see all btn
        xpath = Util.GetXpath({"locate":"see_all_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see all btn", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealButton.ClickSeeAll')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddToCart(arg):
        '''
        ClickAddToCart : Click add to cart btn for specific item
                Input argu :
                    product_name - add specific product to cart
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click add to cart
        xpath = Util.GetXpath({"locate": "add_to_cart_btn"})
        xpath = xpath.replace("replace_product_name", product_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart", "result": "1"})
        time.sleep(10)

        xpath = Util.GetXpath({"locate": "popup_addtocart_window"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0","result": "1"}):
            xpath = Util.GetXpath({"locate": "popup_windos_addtocart_btn"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add to cart in popup window", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealButton.ClickAddToCart')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteIcon(arg):
        '''
        ClickDeleteIcon : Click delete icon btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete icon btn
        xpath = Util.GetXpath({"locate": "delete_icon_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete icon btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'BundleDealButton.ClickDeleteIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel btn
        xpath = Util.GetXpath({"locate": "cancel_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel btn", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click remove btn
        xpath = Util.GetXpath({"locate": "remove_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove btn", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickExclamationMark(arg):
        '''
        ClickExclamationMark : Click exclamation mark btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click exclamation mark btn
        xpath = Util.GetXpath({"locate": "exclamation_mark_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": " Click exclamation mark btn", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealButton.ClickExclamationMark')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        '''
        ClickOk : Click ok btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok btn
        xpath = Util.GetXpath({"locate": "ok_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok btn", "result": "1"})

        OK(ret, int(arg['result']), 'BundleDealButton.ClickOk')
