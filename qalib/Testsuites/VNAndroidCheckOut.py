import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNAndroidCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidCheckOut Precondition =="
        dumplogger.info("== Setup VNAndroidCheckOut Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"vn"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_VNAndroidCheckOut01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-01.xml")

    def test_VNAndroidCheckOut02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-02.xml")

    def test_VNAndroidCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-03.xml")

    def test_VNAndroidCheckOut04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-04.xml")

    def test_VNAndroidCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-05.xml")

    def test_VNAndroidCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-06.xml")

    def test_VNAndroidCheckOut07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-07.xml")

    def test_VNAndroidCheckOut08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-08.xml")

    def test_VNAndroidCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-09.xml")

    def test_VNAndroidCheckOut10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-10.xml")

    def test_VNAndroidCheckOut11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-11.xml")

    def test_VNAndroidCheckOut12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-12.xml")

    def test_VNAndroidCheckOut13(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidCheckOut-13.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidCheckOut Post condition =="
        dumplogger.info("== Setup VNAndroidCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
