import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCOrder Precondition =="
        dumplogger.info("== Setup INPCOrder Precondition ==")

    def test_INPCOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-01.xml")

    def test_INPCOrder02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-02.xml")

    def test_INPCOrder03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-03.xml")

    def test_INPCOrder04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-04.xml")

    def test_INPCOrder05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-05.xml")

    def test_INPCOrder06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-06.xml")

    def test_INPCOrder07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-07.xml")

    def test_INPCOrder08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-08.xml")

    def test_INPCOrder09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-09.xml")

    def test_INPCOrder10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCOrder-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCOrder Post condition =="
        dumplogger.info("== Setup INPCOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
