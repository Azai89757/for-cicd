import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCCart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCCart Precondition =="
        dumplogger.info("== Setup TWPCCart Precondition ==")

    def test_TWPCCart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCart-01.xml")

    def test_TWPCCart02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCart-02.xml")

    def test_TWPCCart03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCart-03.xml")

    def test_TWPCCart04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCart-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCCart Post condition =="
        dumplogger.info("== Setup TWPCCart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
