import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNAndroidMyVoucherPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidMyVoucherPage Precondition =="
        dumplogger.info("== Setup VNAndroidMyVoucherPage Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses Espresso driver
        Config._DesiredCaps_['automationName'] = "Espresso"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"vn"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_VNAndroidMyVoucherPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-01.xml")

    def test_VNAndroidMyVoucherPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-02.xml")

    def test_VNAndroidMyVoucherPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-03.xml")

    def test_VNAndroidMyVoucherPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-04.xml")

    def test_VNAndroidMyVoucherPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-05.xml")

    def test_VNAndroidMyVoucherPage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-06.xml")

    def test_VNAndroidMyVoucherPage07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNAndroidMyVoucherPage-07.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAndroidMyVoucherPage Post condition =="
        dumplogger.info("== Setup VNAndroidMyVoucherPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        #Kill Appium
        KillAppium({"isFail":"0"})
