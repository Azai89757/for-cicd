﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidFormMethod.py: The def of this file called by XML mainly.
'''

##Import common library
import time
import FrameWorkBase
import Util
import XtFunc
import AndroidBaseUICore
import AndroidBaseUILogic
from Config import dumplogger
import DecoratorHelper


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AndroidFormButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        ''' ClickSubmit : Click submit button to submit form
                    Input argu : N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click submit button
        xpath = Util.GetXpath({"locate":"submit_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click submit button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddress(arg):
        ''' ClickAddress : Click address field in form to redirect to address setting page
                    Input argu :
                            question - question title
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        question = arg['question']

        ##Click answer field and redirect to choose address page
        xpath = Util.GetXpath({"locate":"address_field"})
        xpath = xpath.replace("question_to_be_replaced", question)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click address field", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDate(arg):
        ''' ClickDate : Click date field in form to redirect to date setting page
                    Input argu :
                            question - question title
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        question = arg['question']

        ##Click answer field and redirect to choose date page
        xpath = Util.GetXpath({"locate":"date_field"})
        xpath = xpath.replace("question_to_be_replaced", question)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click date field", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickDate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        ''' ClickOK : Click OK button
                    Input argu :
                            N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click ok button
        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click ok button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCheckbox(arg):
        ''' ClickCheckbox : Click checkbox for checkbox question
                    Input argu :
                            question - question title
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        question = arg['question']

        ##Click chekbox
        xpath = Util.GetXpath({"locate":"checkbox"})
        xpath = xpath.replace("question_to_be_replaced", question)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click checkbox", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickCheckbox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOption(arg):
        ''' ClickOption : Click option for sc/mc question
                    Input argu :
                            option - option name
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        option = arg['option']

        ##Click option
        xpath = Util.GetXpath({"locate":"option"})
        xpath = xpath.replace("option_to_be_replaced", option)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click option", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRedirect(arg):
        ''' ClickRedirect : Click the button to redirect to button url
                    Input argu :
                            button_text - the button text you set in BE
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        button_text = arg['button_text']

        ##Click button
        xpath = Util.GetXpath({"locate":"redirect_btn"})
        xpath = xpath.replace("button_text_to_be_replaced", button_text)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickRedirect')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        ''' ClickEdit : Click the button to edit your submission
                    Input argu :
                            N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click edit button
        xpath = Util.GetXpath({"locate":"edit_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click edit button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendOther(arg):
        ''' ClickSendOther : Click the button to send other submissions
                    Input argu :
                            N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click send other button
        xpath = Util.GetXpath({"locate":"send_other_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click send other button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickSendOther')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUnsubscribe(arg):
        ''' ClickUnsubscribe : Click the button to unsubscribe
                    Input argu :
                            N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click unsubscribe button
        xpath = Util.GetXpath({"locate":"unsubscribe_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click unsubscribe button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickUnsubscribe')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        ''' ClickConfirm : Click the button to confirm
                    Input argu :
                            N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMaybeNot(arg):
        ''' ClickMaybeNot : Click maybe not button
                    Input argu :
                            N/A
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1

        ##Click maybe not btn
        xpath = Util.GetXpath({"locate":"maybe_not_btn"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click maybe not button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickMaybeNot')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUplaod(arg):
        ''' ClickUplaod : Click the button to upload file
                    Input argu :
                            question - question title
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        question = arg['question']

        ##Click upload button
        xpath = Util.GetXpath({"locate":"upload_btn"})
        xpath = xpath.replace("question_to_be_replaced", question)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click upload file button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AndroidFormButton.ClickUplaod')

class AndroidFormPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAnswer(arg):
        ''' InputAnswer : Input answer for free/phone/email question
                    Input argu :
                            question - question title
                            answer - answer
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        question = arg['question']
        answer = arg['answer']

        ##Input answer
        xpath = Util.GetXpath({"locate":"answer"})
        xpath = xpath.replace("question_to_be_replaced", question)
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click answer field", "result": "1"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":answer, "result": "1"})

        ##Click other field to make answer field complete
        xpath = Util.GetXpath({"locate":"others"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click others field", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFormPage.InputAnswer')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAddressDetail(arg):
        ''' InputAddressDetail : Input address detail
                    Input argu :
                            postal_code - postal code
                            city - city
                            address - address
                    Return code : 1 - success
                                0 - fail
                                -1 - error
        '''
        ret = 1
        postal_code = arg['postal_code']
        city = arg['city']
        address = arg['address']

        ##Input postal code
        xpath = Util.GetXpath({"locate":"postal_code"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":postal_code, "result": "1"})

        ##Input city
        xpath = Util.GetXpath({"locate":"city"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":city, "result": "1"})

        ##Input address
        xpath = Util.GetXpath({"locate":"address"})
        AndroidBaseUICore.AndroidInput({"method":"xpath", "locate":xpath, "string":address, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidFormPage.InputAddressDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddFileFromCamera(arg):
        ''' AddFileFromCamera : Add the specific photo from album
                Input argu : NA
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1

        ##Click upload from camera btn
        xpath = Util.GetXpath({"locate":"camera"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click camera btn", "result": "1"})
        time.sleep(8)

        ##Click grant camera access btn
        xpath = Util.GetXpath({"locate":"allow_camera_btn"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click grant camera access btn", "result": "1"})
            time.sleep(5)

        ##Click capture_camera btn
        xpath = Util.GetXpath({"locate":"capture_camera"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click capture_camera btn", "result": "1"})
        time.sleep(5)

        ##Click use_photo btn
        xpath = Util.GetXpath({"locate":"use_photo"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click use_photo btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidFormPage.AddFileFromCamera')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddFileFromSystemFolder(arg):
        ''' AddFileFromSystemFolder : Add the specific photo from album
                Input argu : file_name - your file name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Click upload from system folder btn
        xpath = Util.GetXpath({"locate":"system_folder"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click system_folder btn", "result": "1"})
        time.sleep(2)

        ##Click grant camera access btn
        xpath = Util.GetXpath({"locate":"allow_camera_btn"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click grant camera access btn", "result": "1"})
            time.sleep(3)

        ##Check directory is "download" instead of "Recent"
        xpath = Util.GetXpath({"locate":"recent_folder"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            xpath = Util.GetXpath({"locate":"roots_list"})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click roots list btn", "result": "1"})
            time.sleep(3)
            xpath = Util.GetXpath({"locate":"download_folder"})
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click download folder", "result": "1"})
            time.sleep(3)

        ##Click pdf file
        xpath = Util.GetXpath({"locate":"pdf_file"})
        xpath = xpath.replace("file_name_to_be_replaced", file_name)
        AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate":xpath, "direction":"down", "isclick":"1", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidFormPage.AddFileFromSystemFolder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddFileFromPhoto(arg):
        ''' AddFileFromPhoto : Add the specific photo from album
                Input argu : file_name - your file name
                Return code : 1 - success
                            0 - fail
                            -1 - error
        '''
        ret = 1
        file_name = arg['file_name']

        ##Click upload from photo btn
        xpath = Util.GetXpath({"locate":"photo"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click photo btn", "result": "1"})
        time.sleep(2)

        ##Click grant camera access btn
        xpath = Util.GetXpath({"locate":"allow_camera_btn"})
        if AndroidBaseUICore.AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok":"0", "result":"1"}):
            AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click grant camera access btn", "result": "1"})
            time.sleep(3)

        ## Click photo tp upload
        XtFunc.MatchImgByOpenCV({"env":"staging", "mode":Config._TestCasePlatform_.lower(), "image":file_name, "threshold":"0.9", "is_click":"yes", "result":"1"})
        time.sleep(3)

        ##Click check btn
        xpath = Util.GetXpath({"locate":"check"})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click photo btn", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'AndroidFormPage.AddFileFromPhoto')
