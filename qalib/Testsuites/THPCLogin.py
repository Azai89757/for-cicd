import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class THPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCLogin Precondition =="
        dumplogger.info("== Setup THPCLogin Precondition ==")

    def test_THPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCLogin-01.xml")

    def test_THPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TH/THPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup THPCLogin Post condition =="
        dumplogger.info("== Setup THPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
