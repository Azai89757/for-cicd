import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCHomePage Precondition =="
        dumplogger.info("== Setup TWPCHomePage Precondition ==")

    def test_TWPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomePage-01.xml")

    def test_TWPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomePage-02.xml")

    '''def test_TWPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCHomePage Post condition =="
        dumplogger.info("== Setup TWPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
