import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCRecommendationTK(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCRecommendationTK Precondition =="
        dumplogger.info("== Setup IDPCRecommendationTK Precondition ==")

    def test_IDPCRecommendationTK01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCRecommendationTK-01.xml")

    def test_IDPCRecommendationTK02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCRecommendationTK-02.xml")

    def test_IDPCRecommendationTK03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCRecommendationTK-03.xml")

    def test_IDPCRecommendationTK04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCRecommendationTK-04.xml")

    def test_IDPCRecommendationTK05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCRecommendationTK-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCRecommendationTK Post condition =="
        dumplogger.info("== Setup IDPCRecommendationTK Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
