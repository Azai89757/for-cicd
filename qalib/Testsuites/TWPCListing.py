import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCListing(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCListing Precondition =="
        dumplogger.info("== Setup TWPCListing Precondition ==")

    def test_TWPCListing01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCListing-01.xml")

    def test_TWPCListing02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCListing-02.xml")

    def test_TWPCListing03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCListing-03.xml")

    def test_TWPCListing04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCListing-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCListing Post condition =="
        dumplogger.info("== Setup TWPCListing Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
