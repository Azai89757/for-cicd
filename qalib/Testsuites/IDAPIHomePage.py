import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDAPIHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIHomePage Precondition =="
        dumplogger.info("== Setup IDAPIHomePage Precondition ==")

    def test_IDAPIHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIHomePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIHomePage Post condition =="
        dumplogger.info("== Setup IDAPIHomePage Post condition ==")
