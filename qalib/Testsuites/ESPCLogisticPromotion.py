import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class ESPCLogisticPromotion(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCLogisticPromotion Precondition =="
        dumplogger.info("== Setup ESPCLogisticPromotion Precondition ==")

    def test_ESPCLogisticPromotion01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCLogisticPromotion-01.xml")

    def test_ESPCLogisticPromotion02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCLogisticPromotion-02.xml")

    def test_ESPCLogisticPromotion03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCLogisticPromotion-03.xml")

    def test_ESPCLogisticPromotion04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESPCLogisticPromotion-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESPCLogisticPromotion Post condition =="
        dumplogger.info("== Setup ESPCLogisticPromotion Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
