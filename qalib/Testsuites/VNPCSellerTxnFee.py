import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCSellerTxnFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSellerTxnFee Precondition =="
        dumplogger.info("== Setup VNPCSellerTxnFee Precondition ==")

    def test_VNPCSellerTxnFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerTxnFee-01.xml")

    def test_VNPCSellerTxnFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCSellerTxnFee-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCSellerTxnFee Post condition =="
        dumplogger.info("== Setup VNPCSellerTxnFee Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
