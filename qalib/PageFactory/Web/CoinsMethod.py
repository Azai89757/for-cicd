﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
CoinsMethod.py: The def of this file called by XML mainly.
'''

##Import python library
import time

##Import framework common library
import Util
import Config
import XtFunc
import GlobalAdapter
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Import Web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class CoinsPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckCoinCreditDate(arg):
        '''
        CheckCoinCreditDate : Check coin credit date
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get credut date text
        xpath = Util.GetXpath({"locate":"credit_date"})
        BaseUICore.GetElements({"method":"xpath", "locate":xpath, "mode": "single", "result": "1"})
        BaseUICore.GetAttributes({"attrtype":"text", "mode":"single", "result": "1"})
        dumplogger.info("credit_date on coins page: %s" % (GlobalAdapter.CommonVar._PageAttributes_))
        credit_date = GlobalAdapter.CommonVar._PageAttributes_

        if Config._TestCaseRegion_ == "VN":
            current_time = XtFunc.GetCurrentDateTime("%H:%M %d-%m-%Y", 0, 0, 0)
            dumplogger.info("os current time: %s" % (current_time))

            ##Only compare date
            if credit_date[6:] == current_time[6:]:
                dumplogger.info("Credit date same with current date.")
            else:
                dumplogger.info("Credit date difference with current date.")
                ret = 0

        elif Config._TestCaseRegion_ == "TW":
            current_time = XtFunc.GetCurrentDateTime("%Y-%m-%d %H:%M", 0, 0, 0)
            dumplogger.info("os current time: %s" % (current_time))

            ##Only compare date
            if credit_date[:10] == current_time[:10]:
                dumplogger.info("Credit date same with current date.")
            else:
                dumplogger.info("Credit date difference with current date.")
                ret = 0

        OK(ret, int(arg['result']), "CoinsPage.CheckCoinCreditDate")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoCoinSubTab(arg):
        '''
        GotoCoinSubTab : Go to coin sub tab
                Input argu :
                    type - all_history, earning, spending
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##switch coin sub tab
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab on coin page ->" + type, "result": "1"})

        OK(ret, int(arg['result']), "CoinsPage.GotoCoinSubTab")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GotoCoinExpiredPage(arg):
        '''
        GotoCoinExpiredPage : Go to coin expired page from coin page
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click coins expired date
        xpath = Util.GetXpath({"locate": "coins_expired"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click coins expired date", "result": "1"})

        OK(ret, int(arg['result']), "CoinsPage.GotoCoinExpiredPage")
