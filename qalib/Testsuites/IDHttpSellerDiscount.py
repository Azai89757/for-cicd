import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDHttpSellerDiscount(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSellerDiscount Precondition =="
        dumplogger.info("== Setup IDHttpSellerDiscount Precondition ==")

    def test_IDHttpSellerDiscount01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-01.xml")

    def test_IDHttpSellerDiscount02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-02.xml")

    def test_IDHttpSellerDiscount03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-03.xml")

    def test_IDHttpSellerDiscount04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-04.xml")

    def test_IDHttpSellerDiscount05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-05.xml")

    def test_IDHttpSellerDiscount06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-06.xml")

    def test_IDHttpSellerDiscount07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-07.xml")

    def test_IDHttpSellerDiscount08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDHttpSellerDiscount-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDHttpSellerDiscount Post condition =="
        dumplogger.info("== Setup IDHttpSellerDiscount Post condition ==")
