#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 KibanaAPIMethod.py: The def of this file called by XML mainly.
'''
##Import system library
import json

##Import common library
import XtFunc
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import api library
from api import APICommonMethod
import HttpAPICore


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class KibanaAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def KibanaAccountLogin(arg):
        '''
        KibanaAccountLogin : Login Kibana account
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Reset global value
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session": "new", "json_data": "common/kibana_login", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": "kibana_login", "username": "qa_tracker_vd", "password": "qnqGF@Pu6XlPbLSa", "result": "1"})

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        ##Check API response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        OK(ret, int(arg['result']), 'KibanaAPI.KibanaAccountLogin')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GetTrackerData(arg):
        '''
        GetTrackerData : Get Tracker data form Kibana
                Input argu :
                    platform = ios_app / android_app / pc_web
                    operation = impression / click / view / ...
                    page_type = seller_center_sidebar / home / shopping_cart / ...
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        platform = arg['platform']
        operation = arg['operation']
        page_type = arg['page_type']

        ##Assign filter into payload
        filter_list = []
        if platform:
            filter_list.append({"match_phrase": {"platform": platform}})

        if operation:
            filter_list.append({"match_phrase": {"operation": operation}})

        if page_type:
            filter_list.append({"match_phrase": {"page_type": page_type}})

        for filter in filter_list:
            GlobalAdapter.APIVar._HttpPayload_["params"]["body"]["query"]["bool"]["filter"].append(filter)

        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_
        APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "country", "target": "country", "time_deviation": "", "result": "1"})

        ##Change payload format to json string
        APICommonMethod.ChangePayloadDictToStr({"result": "1"})

        ##Send request
        HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        ##Check api response code
        APICommonMethod.CheckAPIResponseCode({"result": "1"})

        res_t = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Secound es api won't do if first es api response text not contain 'id'
        if "id" in res_t.keys():
            ##Get response id
            APICommonMethod.StoreDataFromAPIResponse({"column": "id", "result": "1"})
            dumplogger.info("Get response id: {}".format(GlobalAdapter.CommonVar._DynamicCaseData_["id"]))

            ##Set API data(2nd es api with Kibana)
            HttpAPICore.GetAndSetHttpAPIData({"collection": "kibana_es2", "result": "1"})

            ##Replace id into payload
            APICommonMethod.AssignDataToAPIRequest({"assign_to": "payload", "source": "id", "target": "id", "time_deviation": "", "result": "1"})

            ##Send request
            HttpAPICore.SendHttpRequest({"http_method": "post", "result": "1"})

        OK(ret, int(arg['result']), 'KibanaAPI.GetTrackerData')
