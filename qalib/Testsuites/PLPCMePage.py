import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCMePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCMePage Precondition =="
        dumplogger.info("== Setup PLPCMePage Precondition ==")

    def test_PLPCMePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCMePage-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCMePage Post condition =="
        dumplogger.info("== Setup PLPCMePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
