﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 SellerCenterAPIMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time
import json
import datetime

##Import framework common library
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import API library
import HttpAPICore
import HttpAPILogic
from api import APICommonMethod


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class SellerCenterLoginAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def Login(arg):
        '''
        Login : Login account by API
                Input argu :
                    username - account username
                    password - account password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        username = arg["username"]
        password = arg["password"]

        ##Reset global value
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"new", "json_data":"common/sclogin", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": Config._TestCaseRegion_.lower() + "_" + Config._EnvType_, "username":username, "password":password, "result": "1"})

        ##Send http request with retry method
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        ##Deinit api
        HttpAPICore.DeInitialHttpAPI({"result":"1"})

        OK(ret, int(arg['result']), 'SellerCenterLoginAPI.Login')


class SellerCenterOrderAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForAcceptReturnRefund(arg):
        '''
        AssignDataForAcceptReturnRefund : Assign API data for seller accept return refund
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Following region use drc (dispute resolution center) flow, need to assign return sn data only
            if Config._TestCaseRegion_ in ("VN", "TH", "PH", "SG", "MY", "CO", "CL", "PL", "ES", "FR", "AR", "ID"):
                ##Insert return sn into accept return/refund api payload
                GlobalAdapter.APIVar._HttpPayload_["message"]["return_id"] = int(GlobalAdapter.OrderE2EVar._ReturnID_)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['message']['return_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["message"]["return_id"]))

            ##Following region use non-drc (non dispute resolution center) flow, need to assign return id and return sn
            elif Config._TestCaseRegion_ in ("MX", "BR", "TW"):
                ##Insert return id and return sn into accept return/refund api payload
                GlobalAdapter.APIVar._HttpPayload_["return_id"] = int(GlobalAdapter.OrderE2EVar._ReturnID_)
                GlobalAdapter.APIVar._HttpPayload_["return_sn"] = GlobalAdapter.OrderE2EVar._ReturnSN_
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['return_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["return_id"]))
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['return_sn'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["return_sn"]))

            else:
                dumplogger.error("Please setting correct test case region!!!")
                ret = 0

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForAcceptReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForDisputeReturnRefund(arg):
        '''
        AssignDataForDisputeReturnRefund : Assign API data for seller dispute return refund
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Following region use drc (dispute resolution center) flow, need to assign return id data only
            if Config._TestCaseRegion_ in ("VN", "PH", "SG", "MY", "CO", "CL", "PL", "ES", "FR", "AR", "TH", "ID"):
                ##Insert return id into dispute return/refund api payload
                GlobalAdapter.APIVar._HttpPayload_["return_id"] = int(GlobalAdapter.OrderE2EVar._ReturnID_)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['return_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["return_id"]))

            ##Following region use non-drc (non dispute resolution center) flow, need to assign return id and return sn
            elif Config._TestCaseRegion_ in ("MX", "BR"):
                ##Insert return id and return sn into dispute return/refund api payload
                GlobalAdapter.APIVar._HttpPayload_["return_id"] = int(GlobalAdapter.OrderE2EVar._ReturnID_)
                GlobalAdapter.APIVar._HttpPayload_["return_sn"] = GlobalAdapter.OrderE2EVar._ReturnSN_
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['return_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["return_id"]))
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['return_sn'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["return_sn"]))

            ##Dump json to string
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForDisputeReturnRefund')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForArrangeShipping(arg):
        '''
        AssignDataForArrangeShipping : Assign order_id / pickup_time / forder_id for seller arrange shipping API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        try:
            ##Insert order id and pickup time into  api payload
            GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
            GlobalAdapter.APIVar._HttpPayload_["forder_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['order_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["order_id"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['forder_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["forder_id"]))

            ##Input shipping trace number for self delivery in TW
            if Config._TestCaseRegion_ == "TW" and "shipping_trace_no" in GlobalAdapter.APIVar._HttpPayload_:
                GlobalAdapter.APIVar._HttpPayload_["shipping_trace_no"] = XtFunc.GenerateRandomString(9, "characters")
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['shipping_trace_no'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["shipping_trace_no"]))

            ##If "pickup_time" in payload, give pick up time
            if "pickup_time" in GlobalAdapter.APIVar._HttpPayload_:
                GlobalAdapter.APIVar._HttpPayload_["pickup_time"] = int(time.time()) + 86400
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['pickup_time'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["pickup_time"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForArrangeShipping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForCancelOrder(arg):
        '''
        AssignDataForCancelOrder : Assign order_id for seller reject cancel request API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        try:
            ##Insert order id into api payload
            GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['order_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["order_id"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForCancelOrder')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForGiveBuyerRating(arg):
        '''
        AssignDataForGiveBuyerRating : assign order_id  for seller giving buyer rating API
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''

        ret = 1

        try:
            ##Insert order id into api payload
            GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['order_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["order_id"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForGiveBuyerRating')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForIndomaret(arg):
        '''
        AssignDataForIndomaret : Assign Indomaret ID for payout
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Assign indomaret id
            GlobalAdapter.APIVar._HttpPayload_ = GlobalAdapter.APIVar._HttpPayload_.replace("payment_code_to_be_replace", GlobalAdapter.PaymentE2EVar._IndomaretID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_ = %s" % (GlobalAdapter.APIVar._HttpPayload_))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForIndomaret')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForCounterReturnRefund(arg):
        '''
        AssignDataForCounterReturnRefund : Assign API data for seller counter return refund
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Following region use drc (dispute resolution center) flow, need to assign return id data only
            if Config._TestCaseRegion_ in ("VN", "PH", "SG", "MY", "CO", "CL", "PL", "ES", "FR", "AR", "TW", "ID"):
                ##Insert return id into counter return/refund api payload
                GlobalAdapter.APIVar._HttpPayload_["message"]["offer"]["return_id"] = int(GlobalAdapter.OrderE2EVar._ReturnID_)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['message']['offer']['return_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["message"]["offer"]["return_id"]))

            ##Following region use non-drc (non dispute resolution center) flow, need to assign return id and return sn
            elif Config._TestCaseRegion_ in ("MX", "BR"):
                ##Insert return id and return sn into counter return/refund api payload
                GlobalAdapter.APIVar._HttpPayload_["message"]["offer"]["return_id"] = int(GlobalAdapter.OrderE2EVar._ReturnID_)
                GlobalAdapter.APIVar._HttpPayload_["message"]["offer"]["return_sn"] = int(GlobalAdapter.OrderE2EVar._ReturnSN_)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['message']['offer']['return_id'] = %d" % (GlobalAdapter.APIVar._HttpPayload_["message"]["offer"]["return_id"]))
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['message']['offer']['return_sn'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["message"]["offer"]["return_sn"]))

            ##Dump json to string
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOrderAPI.AssignDataForCounterReturnRefund')


class SellerCenterProductAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyProductSetting(arg):
        '''
        ModifyProductSetting : Modify Product Setting by sending api payload
                Input argu :
                    collection - api payload collection to be sent
                return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        collection = arg['collection']

        ##Initial Http Api
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"", "result": "1"})

        ##Load Api data to be sent
        HttpAPICore.GetAndSetHttpAPIData({"collection":collection, "result": "1"})

        ##Send request with retry
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"post"})

        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterProductAPI.ModifyProductSetting')


class SellerCenterPaymentAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForUpdatePaymentSetting(arg):
        '''
        AssignDataForUpdatePaymentSetting : Assign data for update payment setting
                Input argu :
                    status - 1 (turn toggle on)
                             0 (turn toggle off)
                    method - creditcard/installment
                return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status = arg['status']
        method = arg['method']

        try:
            ##Change update payment setting status
            if method == "creditcard":
                GlobalAdapter.APIVar._HttpPayload_["creditcard_payment_enabled"] = int(status)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['creditcard_payment_enabled'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["creditcard_payment_enabled"]))

            elif method == "installment":
                GlobalAdapter.APIVar._HttpPayload_["installment_status"] = int(status)
                dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['installment_status'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["installment_status"]))

            GlobalAdapter.APIVar._HttpHeaders_['cookie'] = GlobalAdapter.APIVar._HttpHeaders_['cookie'].replace("SPC_SC_TK_TS_to_be_replaced", GlobalAdapter.APIVar._APIResponse_['cookie']['SPC_SC_TK_TS'])
            GlobalAdapter.APIVar._HttpHeaders_['cookie'] = GlobalAdapter.APIVar._HttpHeaders_['cookie'].replace("SPC_EC_to_be_replaced", GlobalAdapter.APIVar._APIResponse_['cookie']['SPC_EC'])

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterPaymentAPI.AssignDataForUpdatePaymentSetting')


class SellerCenterOMSAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForRepushArrangeShipment(arg):
        '''
        AssignDataForRepushArrangeShipment : Assign order ID to payload
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Assign order id
            GlobalAdapter.APIVar._HttpUrlParameter_["order_id"] = GlobalAdapter.OrderE2EVar._OrderID_
            dumplogger.info("GlobalAdapter.APIVar._HttpUrlParameter_['order_id'] = %s" % (GlobalAdapter.APIVar._HttpUrlParameter_["order_id"]))

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.AssignDataForRepushArrangeShipment')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForOMSShipping(arg):
        '''
        AssignDataForOMSShipping : Assign order ID to payload (for OMS only)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Assign order id
            GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['order_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["order_id"]))

            ##Assign OFG id
            GlobalAdapter.APIVar._HttpPayload_["ofg_id"] = int(GlobalAdapter.OrderE2EVar._OFGID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['ofg_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["ofg_id"]))

            ##Assign pickup time to 0 if channel id is following. Pickup time 0 means using drop off method to arrange shipment, it is stable method to arrange shipment
            if GlobalAdapter.APIVar._HttpPayload_["channel_id"] in (50010, 50018, 50012, 10012, 20021, 20011, 2000, 40018, 19108, 50023, 130001, 140002, 80014, 90001, 100001, 120001):
                GlobalAdapter.APIVar._HttpPayload_["pickup_time"] = 0
            ##Assign pickup time (time now + 4 days) if today is Friday. Because some logistic channel can't arrange shipment in holiday
            elif datetime.date.today().weekday() == 4:
                GlobalAdapter.APIVar._HttpPayload_["pickup_time"] = int(time.time()) + 345600
            ##Assign pickup time (time now + 1 day)
            else:
                GlobalAdapter.APIVar._HttpPayload_["pickup_time"] = int(time.time()) + 90000

            ##Assign shipping traceno if channel id is "70015"
            if GlobalAdapter.APIVar._HttpPayload_["channel_id"] != 70015:
                GlobalAdapter.APIVar._HttpPayload_["shipping_traceno"] = XtFunc.GenerateRandomString(9, "characters")

            ##Dump json to string
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.AssignDataForOMSShipping')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForGetFOrderID(arg):
        '''
        AssignDataForGetFOrderID : Assign order ID to payload (for OMS only)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Assign order id
            GlobalAdapter.APIVar._HttpPayload_["order_id"] = int(GlobalAdapter.OrderE2EVar._OrderID_)
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['order_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["order_id"]))

            ##Dump json to string
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.AssignDataForGetFOrderID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AssignDataForUpdateLogistic(arg):
        '''
        AssignDataForUpdateLogistic : Assign forder ID and system time to payload
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        try:
            ##Assign forder id and system time
            GlobalAdapter.APIVar._HttpPayload_["forder_id"] = int(GlobalAdapter.CommonVar._DynamicCaseData_["forder_id"])
            GlobalAdapter.APIVar._HttpPayload_["system_time"] = int(time.time())
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['forder_id'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["forder_id"]))
            dumplogger.info("GlobalAdapter.APIVar._HttpPayload_['system_time'] = %s" % (GlobalAdapter.APIVar._HttpPayload_["system_time"]))

            ##Dump json to string
            GlobalAdapter.APIVar._HttpPayload_ = json.dumps(GlobalAdapter.APIVar._HttpPayload_)

        except KeyError:
            dumplogger.exception("Encounter Key Error!!!")
            ret = -1

        except:
            dumplogger.exception("Encounter Other Exception!!!!")
            ret = -1

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.AssignDataForUpdateLogistic')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeOrderLogisticToRequestCreated(arg):
        '''
        ChangeOrderLogisticToRequestCreated : Change order logistic to request created
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Init order logistic to request create
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"init_forder", "result":"1"})
        SellerCenterOMSAPI.AssignDataForOMSShipping({"result":"1"})
        HttpAPICore.SendHttpRequest({"http_method":"post", "result":"1"})

        ##Save time stamp for issue report
        GlobalAdapter.OrderE2EVar._OrderTimestampDict_["init_forder"] = GlobalAdapter.APIVar._RequestTimestamp_
        XtFunc.SaveCaseInfoForIssueReport({"info_key": "order_timestamp", "info_value": "", "result": "1"})
        dumplogger.info("Order related timestamp: " + str(GlobalAdapter.OrderE2EVar._OrderTimestampDict_))

        ##Check response
        APICommonMethod.CheckAPIResponseCode({"result":"1"})
        APICommonMethod.CheckAPIResponseText({"result": "1"})

        ##Deinit api
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.ChangeOrderLogisticToRequestCreated')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeOrderLogisticStatus(arg):
        '''
        ChangeOrderLogisticStatus : Change order logistic status
                Input argu :
                    logistic_status - pickup_done / pickup_retry / delivery_done / lost_parcel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        logistic_status = arg["logistic_status"]

        ##Get forder id
        HttpAPICore.InitialHttpAPI({"session":"unused", "json_data":"", "result":"1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection":"get_forder_id", "result":"1"})
        SellerCenterOMSAPI.AssignDataForGetFOrderID({"result":"1"})
        HttpAPICore.SendHttpRequest({"http_method":"get", "result":"1"})
        APICommonMethod.CheckAPIResponseCode({"result":"1"})
        APICommonMethod.StoreDataFromAPIResponse({"column":"forder_id", "result":"1"})

        ##Change order logistic status
        HttpAPICore.GetAndSetHttpAPIData({"collection":logistic_status, "result":"1"})
        SellerCenterOMSAPI.AssignDataForUpdateLogistic({"result":"1"})
        HttpAPICore.SendHttpRequest({"http_method":"post", "result":"1"})

        ##Save time stamp for issue report
        action = "change_to_" + logistic_status
        GlobalAdapter.OrderE2EVar._OrderTimestampDict_[action] = GlobalAdapter.APIVar._RequestTimestamp_
        XtFunc.SaveCaseInfoForIssueReport({"info_key": "order_timestamp", "info_value": "", "result": "1"})
        dumplogger.info("Order related timestamp: " + str(GlobalAdapter.OrderE2EVar._OrderTimestampDict_))

        ##Check response
        APICommonMethod.CheckAPIResponseCode({"result":"1"})
        APICommonMethod.CheckAPIResponseText({"result":"1"})

        ##Deinit api
        HttpAPICore.DeInitialHttpAPI({"result":"1"})

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.ChangeOrderLogisticStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyOrderToDeliveryDone(arg):
        '''
        ModifyOrderToDeliveryDone : Modify order logistic status to delivery done
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Init order logistic to request create
        SellerCenterOMSAPI.ChangeOrderLogisticToRequestCreated({"result": "1"})

        ##Sleep 20 second to wait logistic request create is complete
        time.sleep(20)

        ##Change order logistic status to pickup done
        SellerCenterOMSAPI.ChangeOrderLogisticStatus({"logistic_status": "pickup_done", "result": "1"})

        ##Sleep 10 second to wait logistic pickup done is complete
        time.sleep(10)

        ##Change order logistic status to delivery done
        SellerCenterOMSAPI.ChangeOrderLogisticStatus({"logistic_status": "delivery_done", "result": "1"})

        OK(ret, int(arg['result']), 'SellerCenterOMSAPI.ModifyOrderToDeliveryDone')


class SellerCenterLogisticAPI:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckSellerLogisticStatus(arg):
        '''
        CheckSellerLogisticStatus : Logistic check by API response
                Input argu :
                    channel_id - channel ID you want to check
                    product_id - product you want to check
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 0
        product_id = arg['product_id']
        target_channel_id = int(arg['channel_id'])

        ##Send request to get logistic list
        HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"common/SellerCenter/logistic_checker", "result": "1"})
        HttpAPICore.GetAndSetHttpAPIData({"collection": Config._TestCaseRegion_.lower() + "_" + Config._EnvType_, "result": "1"})
        ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"get"})
        HttpAPICore.DeInitialHttpAPI({"result": "1"})

        ##Save response as json type
        response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

        ##Check target channel in seller channel list one by one
        for channel in response["data"]["list"]:

            if channel["channel_id"] == target_channel_id:
                ##Check target channel enabled
                if channel["enabled"]:
                    ret = 1
                    dumplogger.info("Seller logistic channel %i is enabled" % (target_channel_id))

                    ##Set collection key for product list
                    GlobalAdapter.CommonVar._DynamicCaseData_["product_id"] = product_id

                    ##Initial Http Api
                    HttpAPICore.InitialHttpAPI({"session":"inherit", "json_data":"common/SellerCenter/logistic_checker", "result": "1"})
                    HttpAPICore.GetAndSetHttpAPIData({"collection": Config._TestCaseRegion_.lower() + "_" + Config._EnvType_ + "_product", "result": "1"})

                    ##Set payload value from global
                    APICommonMethod.AssignDataToAPIRequest({"assign_to": "query_string_parameters", "source": "product_id", "target": "product_id", "time_deviation": "", "result": "1"})

                    ##Assign shopid and itemid from argument to global
                    ret = HttpAPILogic.SendHttpRequestWithRetry({"http_method":"get"})
                    HttpAPICore.DeInitialHttpAPI({"result": "1"})

                    ##Save response as json type
                    response = json.loads(GlobalAdapter.APIVar._APIResponse_["text"])

                    ##Check target channel in seller channel list one by one
                    for channel in response["data"]["list"]:

                        if channel["channel_id"] == target_channel_id:
                            ##Check target channel enabled
                            if channel["enabled"]:
                                dumplogger.info("Seller and item logistic channels both are opened !!")
                            else:
                                ret = 0
                                dumplogger.error("Item %s logistic channel %i is disabled" % (product_id, target_channel_id))
                            break
                    else:
                        ret = 0
                        dumplogger.error("Item didn't find this logistic, but seller's logistic does. This may be a bug !!")
                else:
                    dumplogger.error("Seller logistic channel %i is disabled" % (target_channel_id))
                break
        else:
            dumplogger.error("Channel doesn't existed, please go OPS to check channel %i turn to whitelist mode or not" % (target_channel_id))

        OK(ret, int(arg['result']), 'SellerCenterLogisticAPI.CheckSellerLogisticStatus')
