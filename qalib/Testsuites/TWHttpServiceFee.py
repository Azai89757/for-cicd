import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpServiceFee(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpServiceFee Precondition =="
        dumplogger.info("== Setup TWHttpServiceFee Precondition ==")

    def test_TWHttpServiceFee01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-01.xml")

    def test_TWHttpServiceFee02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-02.xml")

    def test_TWHttpServiceFee03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-03.xml")

    def test_TWHttpServiceFee04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-04.xml")

    def test_TWHttpServiceFee05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-05.xml")

    def test_TWHttpServiceFee06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-06.xml")

    def test_TWHttpServiceFee07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-07.xml")

    def test_TWHttpServiceFee08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-08.xml")

    def test_TWHttpServiceFee09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-09.xml")

    def test_TWHttpServiceFee10(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-10.xml")

    def test_TWHttpServiceFee11(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-11.xml")

    def test_TWHttpServiceFee12(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpServiceFee-12.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpServiceFee Post condition =="
        dumplogger.info("== Setup TWHttpServiceFee Post condition ==")
