import unittest
from Config import dumplogger
from FrameWorkBase import *


class IDAPIRestoreFraud(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIRestoreFraud Precondition =="
        dumplogger.info("== Setup IDAPIRestoreFraud Post condition")

    def test_IDAPIRestoreFraud001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/IDAPIRestoreFraud-001.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAPIRestoreFraud Post condition =="
        dumplogger.info("== Setup IDAPIRestoreFraud Post condition")
