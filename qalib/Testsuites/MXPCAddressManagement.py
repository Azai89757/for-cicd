import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MXPCAddressManagement(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCAddressManagement Precondition =="
        dumplogger.info("== Setup MXPCAddressManagement Precondition ==")

    def test_MXPCAddressManagement01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MX/MXPCAddressManagement-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MXPCAddressManagement Post condition =="
        dumplogger.info("== Setup MXPCAddressManagement Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
