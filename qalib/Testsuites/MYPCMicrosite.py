import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class MYPCMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMicrosite Precondition =="
        dumplogger.info("== Setup MYPCMicrosite Precondition ==")

    def test_MYPCMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-01.xml")

    def test_MYPCMicrosite02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-02.xml")

    def test_MYPCMicrosite03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-03.xml")

    def test_MYPCMicrosite04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-04.xml")

    def test_MYPCMicrosite05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-05.xml")

    def test_MYPCMicrosite06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-06.xml")

    def test_MYPCMicrosite07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-07.xml")

    def test_MYPCMicrosite08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-08.xml")

    def test_MYPCMicrosite09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCMicrosite-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCMicrosite Post condition =="
        dumplogger.info("== Setup MYPCMicrosite Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
