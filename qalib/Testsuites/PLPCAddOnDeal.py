import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PLPCAddOnDeal(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCAddOnDeal Precondition =="
        dumplogger.info("== Setup PLPCAddOnDeal Precondition ==")

    def test_PLPCAddOnDeal01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCAddOnDeal-01.xml")

    def test_PLPCAddOnDeal02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCAddOnDeal-02.xml")

    def test_PLPCAddOnDeal03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PL/PLPCAddOnDeal-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PLPCAddOnDeal Post condition =="
        dumplogger.info("== Setup PLPCAddOnDeal Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
