import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class SGPCLogin(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCLogin Precondition =="
        dumplogger.info("== Setup SGPCLogin Precondition ==")

    def test_SGPCLogin01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCLogin-01.xml")

    def test_SGPCLogin02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/SG/SGPCLogin-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup SGPCLogin Post condition =="
        dumplogger.info("== Setup SGPCLogin Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
