import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class BRPCTransparentBackgroundImage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCTransparentBackgroundImage Precondition =="
        dumplogger.info("== Setup BRPCTransparentBackgroundImage Precondition ==")

    def test_BRPCTransparentBackgroundImage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCTransparentBackgroundImage-01.xml")

    def test_BRPCTransparentBackgroundImage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/BR/BRPCTransparentBackgroundImage-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup BRPCTransparentBackgroundImage Post condition =="
        dumplogger.info("== Setup BRPCTransparentBackgroundImage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
