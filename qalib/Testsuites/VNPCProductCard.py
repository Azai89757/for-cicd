import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class VNPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCProductCard Precondition =="
        dumplogger.info("== Setup VNPCProductCard Precondition ==")

    def test_VNPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-01.xml")

    def test_VNPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-02.xml")

    def test_VNPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-03.xml")

    def test_VNPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-04.xml")

    def test_VNPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-05.xml")

    def test_VNPCProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-06.xml")

    def test_VNPCProductCard07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-07.xml")

    def test_VNPCProductCard08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNPCProductCard-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNPCProductCard Post condition =="
        dumplogger.info("== Setup VNPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
