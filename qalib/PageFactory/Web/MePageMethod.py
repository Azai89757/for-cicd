﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 MePageMethod.py: The def of this file called by XML mainly.
'''

##import python library
import re
import time
import datetime

##Import framework common library
import Util
import Config
import XtFunc
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class MyAddressPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetAddressAsDefault(arg):
        '''
        SetAddressAsDefault : Click set address as default
                Input argu :
                    address_name - address name to be set
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        address_name = arg['address_name']

        ##Locate edit address page
        xpath = Util.GetXpath({"locate": "edit_address_page"})

        ##Check edit address page exists or not
        if BaseUICore.CheckElementExist({"method":"xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click detail address field to move to default address button to appear
            xpath = Util.GetXpath({"locate": "detail_address_field"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click detail address field", "result": "1"})

            ##Click to set default address in edit address popup window
            xpath = Util.GetXpath({"locate": "default_check_button"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to set default address", "result": "1"})

        else:
            ##Click to set default address in address page
            xpath = Util.GetXpath({"locate": "default_button"})
            xpath = xpath.replace("name_to_be_replaced", address_name)
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to set default address", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.SetAddressAsDefault')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteAddress(arg):
        '''
        DeleteAddress : Click to delete address
                Input argu :
                    address_name - address name to be delete
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        address_name = arg['address_name']

        ##Locate delete address button
        xpath = Util.GetXpath({"locate": "label_user_address_card_delete"})
        xpath = xpath.replace("replace_name", address_name)

        ##check deleted button exist or not
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to delete address", "result": "1"})

            ##Click confirm button on pop up
            xpath = Util.GetXpath({"locate": "delete_confirm"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.DeleteAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddAddress(arg):
        '''
        ClickAddAddress : Click to add address
                Input argu :
                    address_type - convenient_store / home
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        address_type = arg["address_type"]
        ret = 1

        xpath = Util.GetXpath({"locate": address_type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click to add address", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.ClickAddAddress -> ' + address_type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputInfoAndChooseStore(arg):
        '''
        InputInfoAndChooseStore : Input name  + phone number to new a convenient store address
                Input argu :
                    name - name
                    phone - phone number
                    store - 7-11 / 全家 / 萊爾富
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg["name"]
        phone = arg["phone"]
        store = arg["store"]
        ret = 1

        if name:
            ##Input name
            xpath = Util.GetXpath({"locate":"name"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        if phone:
            ##Input phone
            xpath = Util.GetXpath({"locate":"phone"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        if store:
            ##Choose a convenience store type
            xpath = Util.GetXpath({"locate":"convenient_store"})
            xpath = xpath.replace('replaced_text', store)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click to add an convenient store", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputInfoAndChooseStore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditAddressDetail(arg):
        '''
        EditAddressDetail : to edit or add a address
                Input argu :
                    state - state name
                    city - city name
                    area - area name
                    town - town name
                    name - user real name
                    phone - phone number
                    postcode - postcode
                    streetname - street name
                    scenario - print the scenario
                    label - home/work
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        state = arg['state']
        city = arg["city"]
        area = arg["area"]
        town = arg["town"]
        name = arg["name"]
        phone = arg["phone"]
        postcode = arg["postcode"]
        streetname = arg["streetname"]
        scenario = arg["scenario"]
        label = arg["label"]
        ret = 1

        dumplogger.info(scenario)
        if state:
            ##Choose State
            xpath = Util.GetXpath({"locate":"state_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click city drop down menu", "result": "1"})

            xpath = Util.GetXpath({"locate":"state_drop_down_edit"}).replace('state_to_be_replaced', state)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a city", "result": "1"})

        if city:
            ##Choose City
            xpath = Util.GetXpath({"locate":"city_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click city drop down menu", "result": "1"})

            xpath = Util.GetXpath({"locate":"choose_city"}).replace('city_to_be_replaced', city)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a city", "result": "1"})

        if area:
            ##Choose district
            xpath = Util.GetXpath({"locate":"area_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click area drop down menu", "result": "1"})

            xpath = Util.GetXpath({"locate":"area_drop_down_edit"}).replace('area_to_be_replaced', area)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a area", "result": "1"})

        if town:
            ##Choose district
            xpath = Util.GetXpath({"locate":"town_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click town drop down menu", "result": "1"})

            xpath = Util.GetXpath({"locate":"town_drop_down_edit"}).replace('town_to_be_replaced', town)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a town", "result": "1"})

        if postcode:
            if Config._TestCaseRegion_ in ("TW", "PH"):
                ##Insert post code
                xpath = Util.GetXpath({"locate":"postcode_input_column"})
                BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
                BaseUICore.Input({"method":"xpath", "locate":xpath, "string":postcode, "result": "1"})

            else:
                ##Chose post code
                xpath = Util.GetXpath({"locate":"postcode_drop_down"})
                BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click post code drop down menu", "result": "1"})
                xpath = Util.GetXpath({"locate":"choose_post_code"}).replace('post_code_to_be_replaced', postcode)
                BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a post code", "result": "1"})

        ##Input Name
        xpath = Util.GetXpath({"locate":"name_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        ##Input phone number
        xpath = Util.GetXpath({"locate":"phone_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        ##Input address
        xpath = Util.GetXpath({"locate":"address_input_column"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":streetname, "result": "1"})

        ##Choose label
        xpath = Util.GetXpath({"locate":"label_address_label_" + label})
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click label type", "result": "1"})

        if Config._TestCaseRegion_ == "ID":
            ##Click address input column and it will go down to add location btn(had been gotten in last GetXpath)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click address input column", "result": "1"})

            ##Click OK btn to avoid the address popup
            xpath = Util.GetXpath({"locate":"submit_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit", "result": "1"})

            ##Click add location btn
            xpath = Util.GetXpath({"locate":"add_location_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add location btn", "result": "1"})

            ##Click confirm btn
            xpath = Util.GetXpath({"locate":"confirm_btn"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm btn", "result": "1"})

        if "cancel" in scenario:
            ##Click cancel
            xpath = Util.GetXpath({"locate":"cancel_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel", "result": "1"})

        elif "suspend" in scenario:
            ##Do nothing and suspend in edit address page
            dumplogger.info("Do nothing and suspend in edit address page")

        else:
            ##Click submit
            xpath = Util.GetXpath({"locate":"submit_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'MyAddressPage.EditAddressDetail')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDivisionSearch(arg):
        '''
        ClickDivisionSearch : click division search in address page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click division search
        xpath = Util.GetXpath({"locate": "division_search"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click division search block", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.ClickDivisionSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputState(arg):
        '''
        InputState : input state in address page
                Input argu :
                    state - state name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        state = arg['state']
        ret = 1

        ##Click State
        xpath = Util.GetXpath({"locate":"state_drop_down_edit"})
        xpath = xpath.replace('state_to_be_replaced', state)
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a state", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputState')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputCity(arg):
        '''
        InputCity : input city in address page
                Input argu :
                    city - city name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        city = arg['city']
        ret = 1

        if Config._TestCaseRegion_ in ("PL"):
            ##Click city list
            locate = Util.GetXpath({"locate":"city_drop_down"})
            BaseUICore.Click({"method":"xpath", "locate":locate, "message":"Click city list", "result": "1"})

        ##Click City
        xpath = Util.GetXpath({"locate":"choose_city"})
        xpath = xpath.replace('city_to_be_replaced', city)
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a city", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputCity')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputArea(arg):
        '''
        InputArea : input area in address page
                Input argu :
                    area - area name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        area = arg['area']
        ret = 1

        if Config._TestCaseRegion_ in ("MY"):
            ##Input Area
            xpath = Util.GetXpath({"locate":"area_input_column"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":area, "result": "1"})
            time.sleep(3)

        ##Click Area
        xpath = Util.GetXpath({"locate":"area_drop_down_edit"})
        xpath = xpath.replace('area_to_be_replaced', area)
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a area", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputArea')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputTown(arg):
        '''
        InputTown : input town in address page
                Input argu :
                    town - town name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        town = arg['town']
        ret = 1

        ##Choose district
        xpath = Util.GetXpath({"locate":"town_drop_down"})

        if Config._TestCaseRegion_ in ("MX", "BR", "CO"):
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":town, "result": "1"})

        else:
            ##Click Town
            xpath = Util.GetXpath({"locate":"town_drop_down_edit"})
            xpath = xpath.replace('town_to_be_replaced', town)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a town", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputTown')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPostcode(arg):
        '''
        InputPostcode : input postcode in address page
                Input argu :
                    postcode - postcode number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        postcode = arg['postcode']
        ret = 1

        if Config._TestCaseRegion_ in ("TH", "ID"):

            ##Click post code
            xpath = Util.GetXpath({"locate":"choose_post_code"})
            xpath = xpath.replace('post_code_to_be_replaced', postcode)
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Choose a post code", "result": "1"})
        else:
            ##Insert post code
            xpath = Util.GetXpath({"locate":"postcode_input_column"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":postcode, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputPostcode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAddress(arg):
        '''
        InputAddress : input address in address page
                Input argu :
                    address - address name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        address = arg['address']
        ret = 1

        ##Input address
        xpath = Util.GetXpath({"locate": "address_input_column"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": address, "result": "1"})

        ##Press tab to hide detail address recommandation menu
        if Config._TestCaseRegion_ in ("VN"):
            BaseUICore.SendKeyboardEvent({"locate": xpath, "sendtype": "tab", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputDetailInfo(arg):
        '''
        InputDetailInfo : input detail address information in address page
                Input argu :
                    info - address information
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        info = arg['info']
        ret = 1

        ##Input address detail information
        xpath = Util.GetXpath({"locate":"address_info_input_column"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":info, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputDetailInfo')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputStreetNumber(arg):
        '''
        InputStreetNumber : input street number of address in address page
                Input argu :
                    number - street number of address
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        number = arg['number']
        ret = 1

        ##Input street number of address
        xpath = Util.GetXpath({"locate":"street_number"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":number, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputStreetNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputName(arg):
        '''
        InputName : input address in address page
                Input argu :
                    name - username in address page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        name = arg['name']
        ret = 1

        ##Input Name
        xpath = Util.GetXpath({"locate":"name_input_column"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":name, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhone(arg):
        '''
        InputPhone : input phone in address page
                Input argu :
                    phone - phone number in address page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        phone = arg['phone']
        ret = 1

        ##Input phone number
        xpath = Util.GetXpath({"locate":"phone_input_column"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputPhone')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputNeighborhood(arg):
        '''
        InputNeighborhood : input neighborhood in address page
                Input argu :
                    neighborhood - neighborhood in address page
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        neighborhood = arg['neighborhood']
        ret = 1

        ##Input Neighborhood
        xpath = Util.GetXpath({"locate":"neighborhood_input_column"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":neighborhood, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputNeighborhood')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputLabel(arg):
        '''
        InputLabel : input label in address page
                Input argu :
                    label - home / work
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        label = arg['label']
        ret = 1

        ##Choose label
        xpath = Util.GetXpath({"locate":"label_address_label_" + label})
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click label type", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.InputLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : click submit in address page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click title for refresh GEO lacation box
        if Config._TestCaseRegion_ in ("MY", "VN"):
            xpath = Util.GetXpath({"locate":"address_title_text"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click address title", "result": "1"})

        ##Click submit
        xpath = Util.GetXpath({"locate":"submit_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def Set711AsAddress(arg):
        '''
        Set711AsAddress : In third pary (7-11) page, set a fixed store address
                Input argu :
                    storename711 - store name of 7-11
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        storename711 = arg["storename711"]

        ##Use name to search shop
        xpath = Util.GetXpath({"locate":"search_branch_name"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Use name to search shop", "result": "1"})

        ##Input shop name to search for match
        BaseUICore.SwitchToFrame({"frame":"//*[@id='frmMain']", "result": "1"})
        xpath = Util.GetXpath({"locate":"branch_name"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":storename711, "result": "1"})

        xpath = Util.GetXpath({"locate":"send_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click Search", "result": "1"})

        ##Click matched shop name
        xpath = Util.GetXpath({"locate":"click_shop"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click matched shop", "result": "1"})
        BaseUICore.SwitchToOriginalFrame({"result": "1"})

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"confirm_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        ##Click accept button
        xpath = Util.GetXpath({"locate":"accept_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click accept button", "result": "1"})

        ##Click submit button
        xpath = Util.GetXpath({"locate":"submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.Set711AsAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetHiLifeAsAddress(arg):
        '''
        SetHiLifeAsAddress : In third pary (Hi-Life) page, set a fixed store address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select City
        xpath = Util.GetXpath({"locate":"city"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"value", "selectkey":"01", "result": "1"})

        ##Select Section
        xpath = Util.GetXpath({"locate":"area"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"value", "selectkey":"100", "result": "1"})

        ##Select Street Name
        xpath = Util.GetXpath({"locate":"street"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"value", "selectkey":"八德路", "result": "1"})

        ##Click search
        xpath = Util.GetXpath({"locate":"search_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose a store", "result": "1"})

        ##Select Which store
        xpath = Util.GetXpath({"locate":"select_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose a store", "result": "1"})

        ##Confirm chosen store
        xpath = Util.GetXpath({"locate":"submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.SetHiLifeAsAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SetOKAsAddress(arg):
        '''
        SetOKAsAddress : In third pary (OK Mart) page, set a fixed store address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select City
        xpath = Util.GetXpath({"locate":"city"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"value", "selectkey":"台北市", "result": "1"})

        ##Select Section
        xpath = Util.GetXpath({"locate":"area"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"value", "selectkey":"萬華區", "result": "1"})

        ##Select Street Name
        xpath = Util.GetXpath({"locate":"street"})
        BaseUICore.SelectDropDownList({"method":"xpath", "locate":xpath, "selecttype":"value", "selectkey":"萬大路", "result": "1"})

        ##Click search
        xpath = Util.GetXpath({"locate":"search_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose a store", "result": "1"})

        ##Select Which store
        xpath = Util.GetXpath({"locate":"select_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Choose a store", "result": "1"})

        ##Confirm chosen store
        xpath = Util.GetXpath({"locate":"submit_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.SetOKAsAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchAddress(arg):
        '''
        SearchAddress : Input address to geo map search box and select searc result
                Input argu :
                    address - search address keyword
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        address = arg["address"]

        ##Input address to geo map search box
        xpath = Util.GetXpath({"locate":"search_block"})
        BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":address, "result": "1"})

        ##Select search result
        xpath = Util.GetXpath({"locate":"search_result"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click search result", "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressPage.SearchAddress')


class PersonalInformationPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPassword(arg):
        '''
        InputPassword : Input password
                Input argu :
                    password - password
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        password = arg["password"]
        ret = 1

        ##Click to edit email addrss
        xpath = Util.GetXpath({"locate":"password_column"})
        BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":password, "result": "1"})

        OK(ret, int(arg['result']), 'PersonalInformationPage.InputPassword')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputEmailAddress(arg):
        '''
        InputEmailAddress : Input email address
                Input argu :
                    email - email address / if need auto generate, input "auto_generate"
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        email = arg["email"]
        ret = 1

        ##Auto generate set to 1
        if email == "auto_generate":
            email = "id_user_" + XtFunc.GenerateRandomString(6, "numbers") + "@test.com"

        ##Input address
        xpath = Util.GetXpath({"locate":"email_column"})
        #BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click input email address button", "result": "1"})
        BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":email, "result": "1"})

        OK(ret, int(arg['result']), 'PersonalInformationPage.InputEmailAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputName(arg):
        '''
        InputName : Edit user name
                Input argu :
                    type - normal (nickname) / user (for account login) / shop (shop name)
                    name - user name / if need auto generate, input "auto_generate"
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        type = arg["type"]
        name = arg["name"]
        ret = 1

        ##Auto generate set to 1
        if name == "auto_generate":
            name = "id_user_" + XtFunc.GenerateRandomString(6, "numbers")

        ##Edit name
        xpath = Util.GetXpath({"locate": type})
        BaseUILogic.KeyboardAction({"locate": xpath, "actiontype": "delete", "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})

        OK(ret, int(arg['result']), 'PersonalInformationPage.InputName ->' + name)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputBankNumber(arg):
        '''
        InputBankNumber : Input bank number in edit phone number page
                Input argu :
                    bank - bank
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        bank = arg["bank"]
        ret = 1

        ##Input bank number
        xpath = Util.GetXpath({"locate":"bank_column"})
        BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":bank, "result": "1"})

        ##Click confirm button
        MePageButton.ClickConfirm({"type": "bank_account_confirm", "result": "1"})

        OK(ret, int(arg['result']), 'PersonalInformationPage.InputBankNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPhoneNumber(arg):
        '''
        InputPhoneNumber : Input phone number in edit phone number page
                Input argu :
                    phone - phone number
                    verify_code - otp verification number
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        phone = arg["phone"]
        verify_code = arg["verify_code"]
        ret = 1

        ##Input phone number
        xpath = Util.GetXpath({"locate":"phone_column"})
        BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":phone, "result": "1"})
        MePageButton.ClickConfirm({"type": "label_user_phone_confirm", "result": "1"})

        if verify_code:
            ##Click Sending verification
            ##Now verificcation is closed, so mark the next code
            #MePageButton.ClickSendVerification({"result": "1"})

            ##Input otp number
            xpath = Util.GetXpath({"locate":"otp_column"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click OTP field", "result": "1"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":verify_code, "result": "1"})

            MePageButton.ClickConfirm({"type": "label_user_phone_confirm", "result": "1"})

        OK(ret, int(arg['result']), 'PersonalInformationPage.InputPhoneNumber')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeBirthday(arg):
        '''
        ChangeBirthday : Change birthday
                Input argu :
                    date_type - now/past/future/default/YYYY-MM-DD
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        date_type = arg["date_type"]
        ret = 1

        ##Get today month time in different region
        month_mapping = {
            "ID": {
                "1":"Januari",
                "2":"Februari",
                "3":"Maret",
                "4":"April",
                "5":"Mei",
                "6":"Juni",
                "7":"Juli",
                "8":"Agustus",
                "9":"September",
                "10":"Oktober",
                "11":"November",
                "12":"Desember"
            },
            "TH": {
                "1":"มกราคม",
                "2":"กุมภาพันธ์",
                "3":"มีนาคม",
                "4":"เมษายน",
                "5":"พฤษภาคม",
                "6":"มิถุนายน",
                "7":"กรกฎาคม",
                "8":"สิงหาคม",
                "9":"กันยายน",
                "10":"ตุลาคม",
                "11":"พฤศจิกายน",
                "12":"ธันวาคม"
            },
            "TW": {
                "1":u"一月",
                "2":u"二月",
                "3":u"三月",
                "4":u"四月",
                "5":u"五月",
                "6":u"六月",
                "7":u"七月",
                "8":u"八月",
                "9":u"九月",
                "10":u"十月",
                "11":u"十一月",
                "12":u"十二月"
            },
            "VN": {
                "1":"Tháng 1",
                "2":"Tháng 2",
                "3":"Tháng 3",
                "4":"Tháng 4",
                "5":"Tháng 5",
                "6":"Tháng 6",
                "7":"Tháng 7",
                "8":"Tháng 8",
                "9":"Tháng 9",
                "10":"Tháng 10",
                "11":"Tháng 11",
                "12":"Tháng 12",
            },
            "MX": {
                "1":"Enero",
                "2":"Febrero",
                "3":"Marzo",
                "4":"Abril",
                "5":"Mayo",
                "6":"Junio",
                "7":"Julio",
                "8":"Agosto",
                "9":"Septiembre",
                "10":"Octubre",
                "11":"Noviembre",
                "12":"Diciembre",
            }
        }

        if date_type == "now":
            ## Get date time now
            time = datetime.datetime.now()

        elif date_type == "past":
            ## Get date time now and make time to be past
            time = datetime.datetime.now() - datetime.timedelta(days=1)

        elif date_type == "future":
            ## Get date time now and make time to be future
            time = datetime.datetime.now() + datetime.timedelta(days=1)

        elif date_type == "default":
            time = datetime.datetime.now() - datetime.timedelta(days=450)

        else:
            time = datetime.datetime(int(date_type.split('-')[0]), int(date_type.split('-')[1]), int(date_type.split('-')[2]))

        ##Get today day time
        day = str(time.day)

        ##Get today month time
        month = month_mapping[Config._TestCaseRegion_][str(time.month)]

        ##Get today year time
        year = str(time.year)

        xpath_choose_date = Util.GetXpath({"locate":"choose_day"})

        ##Change day
        xpath = Util.GetXpath({"locate":"choose_day_list"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click which day", "result": "1"})
        xpath_choose_day = xpath_choose_date.replace('date_to_be_choosed', day)
        BaseUICore.Click({"method":"xpath", "locate":xpath_choose_day, "message":"Choose a day", "result": "1"})

        ##Change month
        xpath = Util.GetXpath({"locate":"choose_month_list"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click which month", "result": "1"})
        xpath_choose_month = xpath_choose_date.replace('date_to_be_choosed', month)
        BaseUICore.Click({"method":"xpath", "locate":xpath_choose_month, "message":"Choose a month", "result": "1"})

        ##Change year
        xpath = Util.GetXpath({"locate":"choose_year_list"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click which year", "result": "1"})
        xpath_choose_year = xpath_choose_date.replace('date_to_be_choosed', year)
        BaseUICore.Click({"method":"xpath", "locate":xpath_choose_year, "message":"Choose a year", "result": "1"})

        OK(ret, int(arg['result']), 'PersonalInformationPage.ChangeBirthday')


class ChangePasswordPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def EditPassword(arg):
        '''
        EditPassword : Input old password, new password (twice), and verfication code to change current password
                Input argu :
                    old_password - old password
                    new_password - new password
                    confirm_new_password - new password for confirmation
                    verification_code - verification code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        old_password = arg["old_password"]
        new_password = arg["new_password"]
        confirm_new_password = arg["confirm_new_password"]
        verification_code = arg["verification_code"]
        ret = 1

        if old_password:
            ##Input old password
            xpath = Util.GetXpath({"locate":"old_password"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":old_password, "result": "1"})

        if new_password:
            ##Input new password
            xpath = Util.GetXpath({"locate":"new_password"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":new_password, "result": "1"})

        if confirm_new_password:
            ##Input new password in confirm colum
            xpath = Util.GetXpath({"locate":"confirm_new_password"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":confirm_new_password, "result": "1"})

        if verification_code:
            ##Input verification code
            xpath = Util.GetXpath({"locate":"vcode_input_column"})
            BaseUILogic.KeyboardAction({"locate":xpath, "actiontype":"delete", "result": "1"})
            BaseUICore.Input({"method":"xpath", "locate":xpath, "string":verification_code, "result": "1"})

        OK(ret, int(arg['result']), 'ChangePasswordPage.EditPassword')


class MePageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSubmit(arg):
        '''
        ClickSubmit : Click submit button
                Input argu :
                    type - page type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click submit button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click submit button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickSubmit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button
                Input argu :
                    type - password_confirm / label_confirm
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click confirm button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickConfirm')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save button
        xpath = Util.GetXpath({"locate":"label_save"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save button", "result": "1"})

        time.sleep(10)

        OK(ret, int(arg['result']), 'MePageButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel button
                Input argu :
                    type - order_address_label_cancel / label_cancel
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']

        ##Click cancel button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMainPage(arg):
        '''
        ClickMainPage : Go to main page of metab
                Input argu :
                    type - label_user_page_my_account / label_user_page_my_purchase / label_user_page_notifications / label_user_page_my_voucher / label_user_page_my_shopee_coins
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Go to main page of metab
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Go to main page of metab", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickMainPage -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def GoToSubPage(arg):
        '''
        GoToSubPage : Go to sub page of metab
                Input argu :
                    type - label_user_page_profile / label_user_page_banks_and_cards / label_user_page_addresses / label_user_page_change_password
                           label_user_page_order_updates / label_user_page_promotions / label_user_page_list_updates / label_user_page_activity
                           label_user_page_flashsale_updates / label_user_page_paid_ads / label_user_page_rating_updates / label_user_page_wallet_updates / label_user_page_shopee_updates
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Go to sub page
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Go to sub page", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.GoToSubPage -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click to edit address / email / phone
                Input argu :
                    type - edit_address / edit_email / edit_phone
                    address_name - address name (for edit_address)
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]
        address_name = arg["address_name"]

        xpath = Util.GetXpath({"locate": type})

        ## Replace address name in xpath template
        if type == "edit_address":
            xpath = xpath.replace("address_name_to_be_replaced", address_name)

        ##Click to edit address / email / phone
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click to edit ->" + type, "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickComplete(arg):
        '''
        ClickComplete : Click complete button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click complete button
        xpath = Util.GetXpath({"locate":"complete_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click complete button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickComplete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChangeGender(arg):
        '''
        ChangeGender : Change gender
                Input argu :
                    type - male / female / others
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Change Gender
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Change Gender", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ChangeGender -> ' + type)

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOk(arg):
        '''
        ClickOk : Click ok button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click confirm button
        xpath = Util.GetXpath({"locate":"label_user_profile_ok"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click ok button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickOk')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancelButton(arg):
        '''
        ClickCancelButton : Click cancel button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click cancel button
        xpath = Util.GetXpath({"locate":"cancel_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickCancelButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSetDefaultAddress(arg):
        '''
        ClickSetDefaultAddress : Click set default address
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click set as default check box
        xpath = Util.GetXpath({"locate":"default_checkbox"})
        BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click set as default check box", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickSetDefaultAddress')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGeoMap(arg):
        '''
        ClickGeoMap : Click GEO
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click geo map box
        xpath = Util.GetXpath({"locate":"geo_block"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click geo block", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickGeoMap')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEnlargeInGeoMap(arg):
        '''
        ClickEnlargeInGeoMap : Click enlarge button in GEO map
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click enlarge button
        xpath = Util.GetXpath({"locate":"enlarge_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click enlarge button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickEnlargeGeoMap')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickZoomOutInGeoMap(arg):
        '''
        ClickZoomOutInGeoMap : Click zoom out button in GEO map
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click zoom out button
        xpath = Util.GetXpath({"locate":"zoomout_button"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click zoom out button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickZoomOutGeoMap')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSendVerification(arg):
        '''
        ClickSendVerification : Click send verification button in edit phone number page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click send verification button
        xpath = Util.GetXpath({"locate":"label_user_phone_send"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click send verification button", "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickSendVerification')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddress(arg):
        '''
        ClickAddress : Click address that want to change
                Input argu :
                    address_name - address name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        address_name = arg['address_name']
        ret = 1

        ##Click address
        xpath = Util.GetXpath({"locate": "address_name"})
        xpath = xpath.replace("address_name_to_replaced", address_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click address -> %s" % (address_name), "result": "1"})

        OK(ret, int(arg['result']), 'MePageButton.ClickAddress')

class MyAddressComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button according to arguments
                Input argu :
                    button_type - type of button
                    button_text - text of button which to be replaced in xpath
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg['button_type']
        button_text = arg["button_text"]

        ##Click any type of button according to arguments
        xpath = Util.GetXpath({"locate":button_type})

        if button_text:
            xpath = xpath.replace("string_to_be_replace", button_text)

            ##Click on button
            BaseUICore.Click({"method":"javascript", "locate":xpath, "message":"Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        else:
            ##Click on button
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
            Input argu :
                column_type - type of column
                input_content - input content
            Return code :
                1 - success
                0 - fail
                -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'MyAddressComponent.InputToColumn')
