#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminProductLabelMethod.py: The def of this file called by XML mainly.
'''

##Import system library
import time

##Import common library
import DecoratorHelper
import FrameWorkBase
import Config
import Util
from Config import dumplogger
import GlobalAdapter

##Import Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic

##import db library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def LaunchProductLabelAdmin(arg):
    '''
    LaunchProductLabelAdmin : Go to product label admin page
            input Argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    db_file = "admin_cookie_productLabel"
    env = Config._EnvType_.lower()
    ProductLabel_admin_cookie = {}

    ##Get BE url and go to BE
    url = "https://productlabel.ssp.staging.shopee.io/"
    BaseUICore.GotoURL({"url": url, "result": "1"})
    time.sleep(3)

    ##If cookie is not stored for the first time, get cookie from db
    if 'ProductLabel_admin' not in GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]:
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Using sql command to get cookie name and value in specific env
        store_list = ["cookie_name", "cookie_value", "cookie_domain", "cookie_expired_time"]
        assign_list = [{"column": "env", "value_type": "string"}]
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name":db_file, "method":"select", "verify_result":"", "assign_data_list":assign_list, "store_data_list":store_list, "result": "1"})

        ##Store cookie name and value
        ProductLabel_admin_cookie['name'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"]
        ProductLabel_admin_cookie['value'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]
        ProductLabel_admin_cookie['domain'] = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_domain"]
        GlobalAdapter.CommonVar._BrowserCookie_[Config._TestCaseRegion_]['ProductLabel_admin'] = [ProductLabel_admin_cookie]

    ##Go to Product label admin
    BaseUICore.SetBrowserCookie({"storage_type":"ProductLabel_admin", "result": "1"})
    time.sleep(3)
    BaseUICore.GotoURL({"url": url, "result": "1"})
    BaseUICore.PageHasLoaded({"result": "1"})

    OK(ret, int(arg['result']), 'LaunchProductLabelAdmin')

class ProductLabelPlatformTab:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickProductLabelSubTab(arg):
        '''
        ClickProductLabelSubTab : Click subtab
                Input argu :
                    subtab - product_label/item_label_listing
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        subtab = arg['subtab']

        ##Get subtab xpath and click
        xpath = Util.GetXpath({"locate": subtab})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click sub tab in product label admin", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformTab.ClickProductLabelSubTab')


class ProductLabelPlatformButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAdd(arg):
        '''
        ClickAdd : Click add button in itemlabel>listing
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get add button xpath and click
        xpath = Util.GetXpath({"locate": "add_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformButton.ClickAdd')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove button in itemlabel>listing
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get remove button xpath and click
        xpath = Util.GetXpath({"locate": "remove_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click remove", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button in search result
                Input argu :
                    product_label_id - input product label id
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        input_content = arg['product_label_id']

        ##Get edit button xpath and click
        xpath = Util.GetXpath({"locate": "edit"})
        xpath = xpath.replace('replace_labelid', input_content)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product edit button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformButton.ClickEdit')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickStatus(arg):
        '''
        ClickStatus : Click active/inactive in edit product label page
                Input argu :
                    status_type - active/inactive
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        status_type = arg['status_type']

        ##Get active/inactive button xpath
        xpath = Util.GetXpath({"locate": status_type})

        ##Click active/inactive button
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click active/inactive button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformButton.ClickStatus')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUpdate(arg):
        '''
        ClickUpdate : Click update button in edit product label page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get update button xpath and click
        xpath = Util.GetXpath({"locate": "update"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click update button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformButton.ClickUpdate')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickConfirm(arg):
        '''
        ClickConfirm : Click confirm button in popup window when change product label status to inactive
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get confirm button xpath and click
        xpath = Util.GetXpath({"locate": "confirm"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click confirm button", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformButton.ClickConfirm')


class ProductLabelPlatformPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchProductLabelByID(arg):
        '''
        SearchProductLabelByID : Input product label id to search product label in product label page
                Input argu :
                    search_type - active/inactive
                    input_content - search keyword
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        search_type = arg['search_type']
        input_content = arg['input_content']

        ##Search product label
        xpath = Util.GetXpath({"locate": 'search_input'})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        ##Click search button
        button = Util.GetXpath({"locate": 'search_button'})
        BaseUICore.Click({"method": "xpath", "locate": button, "message": "Click search", "result": "1"})

        ##Select search type and click active/inactive button
        button = Util.GetXpath({"locate": search_type})
        BaseUICore.Click({"method": "xpath", "locate": button, "message": "Click active", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformPage.SearchProductLabelByID')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputProductLabelName(arg):
        '''
        InputProductLabelName : Input product label name in add product label to item page when add/remove item from product label
                Input argu :
                    productlabel_name - product label name
                    input_content - search keyword
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        productlabel_name = arg['productlabel_name']

        ##Click the input cube
        xpath = Util.GetXpath({"locate": 'input_cube'})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click input cube", "result": "1"})
        time.sleep(1)

        ##Input the product label name
        xpath = Util.GetXpath({"locate": 'input_productlabel_name'})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":productlabel_name, "result": "1"})
        time.sleep(1)

        ##Click product label name in dropdown list
        xpath = Util.GetXpath({"locate": "productlabel_dropdown"})
        xpath = xpath.replace('replace_by_productlabel',productlabel_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click product label name in dropdown list", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformPage.InputProductLabelName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectCountry(arg):
        '''
        SelectCountry : Select admin country
                Input argu :
                    country - country name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        country = arg['country']

        #Click country dropdown button
        xpath = Util.GetXpath({"locate": "country_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click country dropdown button", "result": "1"})

        #Click country in dropdown list
        xpath = Util.GetXpath({"locate": country})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select country in product label platform", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformPage.SelectCountry')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadProductLabelCsv(arg):
        '''
        UploadProductLabelCsv : Upload csv when add/remove Product Label to Item
                Input argu :
                    upload_type - add/remove
                    upload_csv - upload csv file name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        upload_type = arg['upload_type']
        upload_csv = arg['upload_csv']
        local_xpath = Util.GetXpath({"locate": "upload_csv"})

        if upload_csv:
            ##Upload seller shopsids csv file
            BaseUILogic.UploadFileWithPath({"method": "xpath", "locate":local_xpath, "element": "import_sellerids", "element_type": "xpath", "project": Config._TestCaseFeature_, "action": "file", "file_name": upload_csv, "file_type": "csv", "result": "1"})
            time.sleep(5)

        else:
            ##Upload seller shopsids non csv file
            BaseUILogic.UploadFileWithPath({"method": "input", "element": "file", "element_type": "xpath", "project": Config._TestCaseFeature_, "action": "file", "file_name": "non_csv_servicefee_rule", "file_type": "txt", "result": "1"})
            time.sleep(5)

        ##Get add/remove button xpath
        xpath = Util.GetXpath({"locate": upload_type})

        ##Click add/remove button xpath
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add", "result": "1"})

        OK(ret, int(arg['result']), 'ProductLabelPlatformPage.UploadProductLabelCsv -> ' + upload_csv)
