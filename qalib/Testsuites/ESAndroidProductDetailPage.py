import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *

class ESAndroidProductDetailPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESAndroidProductDetailPage Precondition =="
        dumplogger.info("== Setup ESAndroidProductDetailPage Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Disable wifi module by command only if proxy server is not used
            if not Config._EnableProxy_:
                ##Disable wifi module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc wifi disable"
                dumplogger.info("Disable emulator wifi module: %s" % (cmd))
                os.system(cmd)

                ##Enable mobile data module by command
                cmd = "adb -s " + Config._DeviceID_ + " shell svc data enable"
                dumplogger.info("Enable emulator mobile data module: %s" % (cmd))
                os.system(cmd)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"es"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_ESAndroidProductDetailPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESAndroidProductDetailPage-01.xml")

    def test_ESAndroidProductDetailPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESAndroidProductDetailPage-02.xml")

    def test_ESAndroidProductDetailPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESAndroidProductDetailPage-03.xml")

    def test_ESAndroidProductDetailPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ES/ESAndroidProductDetailPage-04.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESAndroidProductDetailPage Post condition =="
        dumplogger.info("== Setup ESAndroidProductDetailPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
