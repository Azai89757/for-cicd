import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCMallPage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMallPage Precondition =="
        dumplogger.info("== Setup IDPCMallPage Precondition ==")

    def test_IDPCMallPage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMallPage-01.xml")

    def test_IDPCMallPage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMallPage-02.xml")

    def test_IDPCMallPage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMallPage-03.xml")

    def test_IDPCMallPage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMallPage-04.xml")

    def test_IDPCMallPage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCMallPage-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCMallPage Post condition =="
        dumplogger.info("== Setup IDPCMallPage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
