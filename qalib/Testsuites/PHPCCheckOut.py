import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class PHPCCheckOut(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCCheckOut Precondition =="
        dumplogger.info("== Setup PHPCCheckOut Precondition ==")

    # def test_PHPCCheckOut01(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCCheckOut-01.xml")

    # def test_PHPCCheckOut02(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCCheckOut-02.xml")

    def test_PHPCCheckOut03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCheckOut-03.xml")

    # def test_PHPCCheckOut04(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCCheckOut-04.xml")

    def test_PHPCCheckOut05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCheckOut-05.xml")

    def test_PHPCCheckOut06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCheckOut-06.xml")

    # def test_PHPCCheckOut07(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCCheckOut-07.xml")

    # def test_PHPCCheckOut08(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCCheckOut-08.xml")

    def test_PHPCCheckOut09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCCheckOut-09.xml")

    # def test_PHPCCheckOut10(self):
    #     if isMyCase(self.id()) is False:
    #         return
    #     XMLParser("/xml/ui/PH/PHPCCheckOut-10.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCCheckOut Post condition =="
        dumplogger.info("== Setup PHPCCheckOut Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
