import unittest
from Config import dumplogger
from FrameWorkBase import *


class VNAPIMicrosite(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIMicrosite Precondition =="
        dumplogger.info("== Setup VNAPIMicrosite Precondition ==")

    def test_VNAPIMicrosite01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/VNAPIMicrosite-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNAPIMicrosite Post condition =="
        dumplogger.info("== Setup VNAPIMicrosite Post condition ==")
