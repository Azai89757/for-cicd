﻿
#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
AndroidBaseUICore.py: The def of this file called by XML mainly.
'''

##Import system library
import os
import re
import time
import shutil
import datetime
import traceback

##Import framework common library
import Util
import XtFunc
import Parser
import Config
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger
from Config import _GeneralWaitingPeriod_, _OutputFolderPath_, dict_systemslash, _platform_

##Import Android library
import AndroidBaseUILogic

##Import selenium related library
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import InvalidElementStateException


_AndroidDriver_ = None

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

def SendADBCommand(arg):
    '''
    SendADBCommand : Send ADB command to client device
            Input argu :
                command_type : Please choose a command type from Config._AdbCommands_
                adb_arg : Arguments for the adb command
                is_return : 1 (Return cmd result) / 0 (Not Return cmd result)
            Return code :
                cmd_result - command output result
    '''
    dumplogger.info("Enter SendADBCommand")

    command_type = arg["command_type"]
    adb_arg = arg["adb_arg"]
    is_return = arg["is_return"]

    ##Send ADB Command
    try:
        command = 'adb -s ' + Config._DeviceID_ + Config._AdbCommands_[command_type]
        if adb_arg:
            command += adb_arg
        cmd_result = os.popen(command).read()
        dumplogger.info(command)
    except:
        dumplogger.exception("adb.exe command failed, please check your command_type is correct")
        print traceback.print_exc()

    if int(is_return):
        return cmd_result

def GetAndroidDeviceInfo(deviceid):
    '''
    GetAndroidDeviceInfo : Get android device name/platform version
            Input argu : N/A
            Return code :
                output - device information
    '''
    dumplogger.info("Enter GetAndroidDeviceInfo")
    dumplogger.info("deviceid = %s" % (deviceid))

    ##Send adb Command
    output = SendADBCommand({"command_type": "GetDeviceInfo", "adb_arg": "", "is_return": "1", "result": "1"})

    ##Get cmd result
    dumplogger.info("output = " + output.strip())
    return output.strip()

def ChangeDeviceTime(arg):
    '''
    ChangeDeviceTime : Change time on Android device
            Input argu :
                days - days / minutes / seconds
            Return code : N/A
    '''
    days = int(arg["days"])
    minutes = int(arg["minutes"])
    seconds = int(arg["seconds"])

    dumplogger.info("Enter ChangeDeviceTime")
    ##Set specific time in certain formate
    set_date = XtFunc.GetCurrentDateTime("%m%d%H%M%Y.00", days, minutes, seconds)

    ##Send adb Command
    output = SendADBCommand({"command_type": "ChangeDeviceTime", "adb_arg": set_date, "is_return": "1", "result": "1"})

    ##Get cmd result
    dumplogger.info("output = " + output.strip())

def GetMobileDeviceId(platform):
    '''
    GetAndroidDeviceId : Get android device id
            Input argu :
                platform - android
            Return code :
                device_id_group - last device id
    '''
    dumplogger.info("Enter GetMobileDeviceId")
    dumplogger.info("platform = " + platform)

    if platform == "android":
        cmd = "adb devices"
        dumplogger.info("cmd = " + cmd)

        ##Get cmd result
        output = os.popen(cmd).read()
        dumplogger.info("output = " + output)

        ##Split output result by line
        try:
            adb_device_list = output.split("\n")

            device_id_group = []
            for each_line in range(1, len(adb_device_list)):
                ##Parser device id for android
                match = re.search('(\S+)(\s+)(device)', adb_device_list[each_line], re.IGNORECASE)
                if match:
                    device_id_group.append(match.group(1))
                else:
                    dumplogger.info("Do not found the device id !!")

            ##return latest device id
            dumplogger.exception("Get device id : %s" % (device_id_group[-1]))
            if "TWQA_" in Config._AvdName_:
                return "emulator-" + str(int(Config._AvdName_.split("_")[-1]) * 2 + 5552)
            else:
                return device_id_group[-1]

        ##Handle index error
        except IndexError:
            dumplogger.exception("Encounter Index Error!!!")
            print "Encounter Index error, please check Config._DeviceNumber_ and adb device list in debug log first."

        ##Handle unknown error
        except:
            dumplogger.exception("Encounter Unknown Error!!!")
            print "Encounter Unknown error, please check Config._DeviceNumber_ and adb device list in debug log first."

    else:
        dumplogger.info("Please check your mobile platform")
        print "Please check your mobile platform"

def CheckMobileDeviceConnected(platform, deviceid):
    '''
    CheckMobileDeviceConnected : Check mobile device connected
            Input argu :
                platform - android
                deviceid - mobile device id
            Return code :
                PASS - get device id successfully
                FAIL - get device id failed
    '''
    dumplogger.info("Enter CheckMobileDeviceConnected")
    dumplogger.info("platform = %s" % (platform))

    ##For android
    if platform == "android":

        ##adb command to get devices list
        cmd = 'adb devices'
        cmd_result = os.popen(cmd).read()
        dumplogger.info("cmd_result = %s" % (cmd_result))

        ##Check deviceid is equal to cmd return result(cmd_result)
        if deviceid in cmd_result:
            dumplogger.info("Get device id - > PASS")
            return 'PASS'
        else:
            dumplogger.info("Get device id - > FAIL")
            return 'FAIL'
    else:
        dumplogger.info("Platform not defined, please check again !!")
        print "Platform not defined, please check again !!"

def CheckMobileAppInstall(platform, deviceid, pkgname):
    '''
    CheckMobileAppInstall : Check android device connected
            Input argu :
                platform - android
                deviceid - mobile device id
                pkgname - app name
            Return code :
                version - app version in device
                FAIL - get app version failed
    '''

    dumplogger.info("Enter CheckMobileAppInstall")
    dumplogger.info("platform = %s" % (platform))
    dumplogger.info("deviceid = %s" % (deviceid))
    dumplogger.info("pkgname = %s" % (pkgname))

    ##For android
    if platform == "android":

        ##adb command to get package(App) name
        cmd_result = SendADBCommand({"command_type": "GetPackageName", "adb_arg": pkgname, "is_return": "1", "result": "1"})
        dumplogger.info("cmd_result = %s" % (cmd_result))

        ##Check package name is equal to cmd return result(cmd_result)
        if pkgname in cmd_result:
            adb_arg = '"dumpsys package com.shopee.' + Config._TestCaseRegion_.lower() + '.int | grep versionName"'
            adb_arg = adb_arg.replace("com.shopee.tw.int", pkgname)
            dumplogger.info(adb_arg)
            cmd_result = SendADBCommand({"command_type": "GetShopeeVersion", "adb_arg": adb_arg, "is_return": "1", "result": "1"})
            version = 'PASS, ' + cmd_result.strip()
            dumplogger.info("version = %s" % (version))
            return version
        else:
            dumplogger.info("FAIL, Not found App installed")
            return 'FAIL'
    else:
        dumplogger.info('Platform not defined.')
        print 'Platform not defined.'

@DecoratorHelper.FuncRecorder
def StartAppium(arg):
    '''
    StartAppium : Start Appium with new command prompt
            Input argu : None
            Return code : N/A
    '''

    log_file = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + "appium_log.log"
    chrome_driver_port = Config._AppiumDevicePort_["chrome_driver_port"] + int(Config._DeviceNumber_)
    appium_client_port = Config._AppiumDevicePort_["appium_client_port"] + int(Config._DeviceNumber_)

    ##Determine launch method based on platform and device number
    if Config._platform_ == 'windows':
        launch_appium_cmd = 'start /MIN cmd.exe @cmd /k appium -p ' + str(chrome_driver_port) + ' -bp ' + str(appium_client_port) + ' --log ' + log_file + ' --session-override --relaxed-security'

    elif Config._platform_ == 'darwin':
        launch_appium_cmd = 'open -a Terminal -n; osascript -e \'tell app "Terminal"\n do script \"appium -p ' + str(chrome_driver_port) + ' -bp ' + str(appium_client_port) + ' --log ' + log_file + '\"\n end tell\''

    ##Launch new command prompt for appium
    dumplogger.info(launch_appium_cmd)
    os.system(launch_appium_cmd)

    ##Get process ID
    GlobalAdapter.CommonVar._ProcessID_["appium"] = FrameWorkBase.PNameQueryProcess("cmd", launch_appium_cmd)
    dumplogger.info("pid: %s" % (GlobalAdapter.CommonVar._ProcessID_["appium"]))
    if GlobalAdapter.CommonVar._ProcessID_["appium"] == -1:
        print "Get appium process ID failed!!!!!!!!"
        dumplogger.info("Get appium process ID failed!!!!!!!!")

    ##Wait for appium initial
    time.sleep(5)

@DecoratorHelper.FuncRecorder
def KillAppium(arg):
    '''
    KillAppium : Kill Appium process
            Input argu :
                isFail - To know whether it's intent by failure case or not. 1 - default (Case not failed), 0 - case failed
            Return code : N/A
    '''
    isFail = arg["isFail"]

    ##For rerun process, output case folder will be different
    if Config._RerunList_ and Parser._CASE_['id']:
        output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Parser._CASE_['id'].split("-")[0] + "_rerun" + Config.dict_systemslash[Config._platform_]
    else:
        output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Kill process by process id
    FrameWorkBase.KillProcessByPID(GlobalAdapter.CommonVar._ProcessID_["appium"])

    ##Create log folder if not exist
    if os.path.exists(output_casename_folder):
        dumplogger.info(output_casename_folder + "path exist")
    else:
        command = "mkdir " + output_casename_folder
        os.system(command)
        dumplogger.info("command = " + command)

    ##Rename Appium log name if failed
    appium_log_name_before = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + "appium_log.log"
    if isFail == '0':
        shutil.move(appium_log_name_before, output_casename_folder)
        dumplogger.info("All cases are success.")

    else:
        appium_log_name_after = output_casename_folder + Parser._CASE_['id'] + "_appium_log.log"

        try:
            shutil.move(appium_log_name_before, appium_log_name_after)
            dumplogger.info("Rename Appium log name->" + appium_log_name_after)
        except:
            dumplogger.exception("Move log file failed. Please check your permission.")
            print "Move log file failed. Please check your permission."

    ##Reset CommonVar parameter
    GlobalAdapter.CommonVar._ProcessID_["appium"] = ""
    dumplogger.info('Clear GlobalAdapter.CommonVar._ProcessID_["appium"] list')

@DecoratorHelper.FuncRecorder
def AndroidSwitchEnv(arg):
    '''
    AndroidSwitchEnv : Android switch env
            Input argu : N/A
            Return code : N/A
    '''

    dumplogger.info("Parser._CASE_['failskip'] : %s" % Parser._CASE_['failskip'])

    ##Define output case folder first
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Record current timestamp first
    cur_time = datetime.datetime.fromtimestamp(time.time()).strftime("%H%M%S")

    forbidden_zone_page = Util.GetXpath({"locate": "forbidden_zone_page"})
    if AndroidCheckElementExist({"method": "xpath", "locate": forbidden_zone_page, "passok": "0", "result": "1"}):
        ##Click back key to go to home page
        AndroidKeyboardAction({"actiontype": "back", "result": "1"})

    try:
        ##Proceed to switch environment when case is at initial
        if Parser._CASE_['failskip'] == 0:
            ##Long press under bar to switch env
            xpath = Util.GetXpath({"locate":"long_press_btn"})
            AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})
            time.sleep(2)

            ##Check env in shopee Forbidden Zone, and switch to desired env if necessary
            xpath = Util.GetXpath({"locate":"current_env_text"})
            current_env = _AndroidDriver_.find_element_by_xpath(xpath).text
            match_env = re.search('(\()(\w+)(\))', current_env, re.IGNORECASE)

            if match_env:
                ##Stored function name from inline code
                current_env = match_env.group(2)
                dumplogger.info('Current env : %s !' % (current_env))
            else:
                dumplogger.info("Don't find environment !!")

            ##Get switch env name which is what you would like to switch
            switch_env = (Config._EnvType_).capitalize()

            ##Check current env is as same as switch env, if not, switch to what you want to switch of env in ini file
            if current_env != switch_env:
                dumplogger.info("Current Environment: %s, start to switch..." % (current_env))
                ##Click current env title
                xpath = Util.GetXpath({"locate":"env_title"})
                AndroidClick({"method":"xpath", "locate":xpath, "message":"Click current env name", "result": "1"})
                time.sleep(5)

                ##Click target switch env name
                xpath = Util.GetXpath({"locate":"change_env_text"})
                xpath = xpath.replace('replace_env', switch_env)
                AndroidClick({"method":"xpath", "locate":xpath, "message":"Choose target env", "result": "1"})
                print "Switch env to %s !!!\n" % (switch_env)
                dumplogger.info('Switch env to %s !' % (switch_env))

            else:
                dumplogger.info("Current Environment: %s, don't need to switch!" % (current_env))
                ##Click back key to go to home page
                AndroidKeyboardAction({"actiontype":"back", "result": "1"})

    except NoSuchElementException:
        dumplogger.exception("Encounter NoSuchElementException!!!")
        _AndroidDriver_.get_screenshot_as_file(output_casename_folder + cur_time + "_swit_chenv_failed.png")
    except:
        dumplogger.exception("Encounter Unknown Error!!!")
        _AndroidDriver_.get_screenshot_as_file(output_casename_folder + cur_time + "_switchenv_failed.png")

@DecoratorHelper.FuncRecorder
def AndroidCloseBanner(arg):
    '''
    AndroidCloseBanner : Android handle banner with retry
            Input argu :
                banner_type - home / mall
            Return code : N/A
    '''
    ret = 0
    banner_type = arg["banner_type"]

    ##Loop 3 times
    for retry_times in range(1, 4):
        ##Close banner by clicking specific coordinates
        if banner_type == "mall":
            ret = AndroidClickCoordinates({"x_cor": 15, "y_cor": 80})
        elif banner_type == "home":
            print "Handle close home banner!!!\n"
            dumplogger.info("Handle close home banner!!!")
            ret = AndroidClickCoordinates({"x_cor": 960, "y_cor": 2100})
            ret = AndroidClickCoordinates({"x_cor": 960, "y_cor": 2100})
            ret = AndroidClickCoordinates({"x_cor": 100, "y_cor": 2100})
        if ret == 1:
            break
        else:
            time.sleep(5)
            dumplogger.info("%d times trying to handle banner failed..." % (retry_times))

def AndroidHandleLiveBundle():
    '''
    AndroidHandleLiveBundle : Android handle live bundle issue
            Input argu : N/A
            Return code : N/A
    '''

    ##Loop to retry 3 times
    for retry_times in range(1, 4):
        ##Use adb command to check current page
        dumpsys_cur_page = SendADBCommand({"command_type": "Dumpsys", "adb_arg": "window windows | find \"gr=CENTER sim={adjust=pan forwardNavigation\"", "is_return": "1", "result": "1"})
        dumplogger.info("print dumpsys activity")
        dumplogger.info(dumpsys_cur_page)

        ##Check current page type is language initial page
        if dumpsys_cur_page:
            ##Wait for page loaded in case app crash
            time.sleep(15)

            ##In case page still in forbidden zone
            AndroidKeyboardAction({"actiontype": "back", "result": "1"})

            ##Live bundle issue alert popup, click reload bundle
            xpath = Util.GetXpath({"locate":"close_popup"})
            AndroidClick({"method":"xpath", "locate":xpath, "message":"Close reload bundle", "result": "1"})

            ##Wait for bundle reload
            time.sleep(10)

            ##Clean install bundle
            #AndroidCleanInstallBundle({"result": "1"})

            ##Substitute clean install bundle to reset feature branch
            AndroidResetFeatureBranch({"result": "1"})

        else:

            ##Live bundle issue alert not show, skip action
            dumplogger.info("Check loading bundle not exist")
            break

def AndroidSelectLanguage():
    '''
    AndroidSelectLanguage : Android Select Default language when first lauch APP
            Input argu : N/A
            Return code : N/A
    '''

    ##Retry 3 times, total 4 times
    for retry_times in range(4):

        ##Use adb command to check current page
        dumpsys_cur_page = SendADBCommand({"command_type": "Dumpsys", "adb_arg": "window windows | find \"mObscuringWindow\"", "is_return": "1", "result": "1"})
        dumplogger.info("print dumpsys activity")
        dumplogger.info(dumpsys_cur_page.strip())

        ##Check current page type is language initial page
        if "tutorial.LanguageInitActivity" in dumpsys_cur_page.strip():

            ##Select local language as test language
            xpath = Util.GetXpath({"locate":"language"})
            AndroidClick({"method":"xpath", "locate":xpath, "message":"Click language", "result": "1"})

            ##Click confirm button after select language
            xpath = Util.GetXpath({"locate":"ok_btn"})
            AndroidClick({"method":"xpath", "locate":xpath, "message":"Click language page ok btn", "result": "1"})
            time.sleep(3)
            break

        elif ("mObscuringWindow=null" in dumpsys_cur_page.strip()) or ("ImageWallpaper" in dumpsys_cur_page.strip()):
            ##Launched APP have not done, wait and retry
            dumplogger.info("Launch APP not completed in %d times, retry again." % (retry_times+1))
            time.sleep(10)

        else:
            dumplogger.info("Don't need to initiate language, just pass on!")
            break

def AndroidHandleTutorial():
    '''
    AndroidHandleTutorial : Android Handle Tutorial
            Input argu : N/A
            Return code : N/A
    '''

    ##Retry 3 times, total 4 times
    for retry_times in range(4):

        ##Use adb command to check current page
        dumpsys_cur_page = SendADBCommand({"command_type": "Dumpsys", "adb_arg": "window windows | find \"mObscuringWindow\"", "is_return": "1", "result": "1"})
        dumplogger.info("print dumpsys activity")
        dumplogger.info(dumpsys_cur_page.strip())

        ##Check current page type is language initial page
        if "tutorial.TutorialActivity" in dumpsys_cur_page.strip():
            ##Swipe to close fullscreen immersive notice window
            SendADBCommand({"command_type": "Swipe", "adb_arg": "540 50 540 800", "is_return": "1", "result": "1"})

            ##Use android back to close tutorial
            AndroidKeyboardAction({"actiontype":"back", "result": "1"})
            break

        elif ("mObscuringWindow=null" in dumpsys_cur_page.strip()) or ("ImageWallpaper" in dumpsys_cur_page.strip()):
            ##Launched APP have not done, wait and retry
            dumplogger.info("Launch APP not completed in %d times, retry again." % (retry_times+1))
            time.sleep(10)

        else:
            dumplogger.info("Don't need to go through tutorial, just pass on!")
            break

def AndroidInitialDriver(arg):
    '''
    AndroidInitialDriver : Initial Android app driver
            Input argu :
                browser - ie, ff, chrome
            Return code :
                1 - success
                0 - fail
                -1 - error
                        key - keyword
    '''
    print "Prepare enter AndroidInitialDriver!!!\n"
    dumplogger.info('Prepare enter AndroidInitialDriver')

    ##Reconnect to LTE if not connected
    AndroidCheckAndReconnectLTE()

    ##Send Adb command to set Permission
    SendADBCommand({"command_type":"Permission", "adb_arg":"", "is_return": "0", "result": "1"})
    time.sleep(3)

    ##Set Up Appium Desired Caps by Driver
    AndroidSetUpAppiumDesiredCaps()

    ##Establish connection with appium
    AndroidConnectWithAppium()

    ##Get RN Transify Key
    ##20210701 Skip get rn transify key
    #for platform in GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_]:
    #    if 'rn' in platform:
    #        transify_key_name = platform

    #if not GlobalAdapter.RegionalJson._TransferKeyMapping_[Config._TestCaseRegion_][transify_key_name]:
    #    ##Store i18N Json file name by test country for the first time
    #    if transify_key_name not in Config._i18NJson_[Config._TestCaseRegion_]:
    #        Config._i18NJson_[Config._TestCaseRegion_].append(transify_key_name)

    #    ##Get Transify Key
    #    ret = Util.GetAndroidRNTransifyKey(env, transify_key_name)
    #    dumplogger.info("Get Android transify key result: %d" % (ret))
    #else:
    #    dumplogger.info("Android transify key has been loaded!!!")

    ##Reconnect to wifi (Mobile network no need to reconnnect)
    # AndroidCheckAndReconnectWifi()

    ##Initiate default language
    # AndroidSelectLanguage()

    ##Handle first open APP tutorial
    # AndroidHandleTutorial()

    ##Handle error
    # AndroidHandleWrongBundleError({"result": "1"})

    ##Close cookies message in specific region
    # if Config._TestCaseRegion_ in ("PL", "ES"):
    #     AndroidCloseCookiesPopUp({"result": "1"})

    ##Deal with UAT error (if env already staging)
    #AndroidCloseUATError({"result": "1"})

    ##Handle first open APP popup banner, avoid app crash initially
    # time.sleep(20)
    # AndroidCloseBanner({"banner_type":"home"})

    ##Handle dismiss alert
    #AndroidHandleUnknownError({"result": "1"})

    ##Switch environment
    # AndroidSwitchEnv({})

    ##Handle live bundle popup
    # time.sleep(10)
    # AndroidHandleLiveBundle()

    ##Deal with UAT error (if env not staging)
    #AndroidCloseUATError({"result": "1"})

    ##Close RNLogger pop Up
    # AndroidCloseRNLoggerPopUp()

    ##Setting Android PFB version if needed
    try:

        ##If emulator is Espresso then check its PFB and install master-automation
        if "Espresso" in Config._DesiredCaps_.values():
            Config._PFB_["Android"] = {}
            Config._PFB_["Android"]["pfb_name"] = "master-automation"
            AndroidSetPFB({"is_restore":"", "result": "1"})

        elif not Config._PFB_["Android"]["pfb_name"]:
            AndroidSetPFB({"is_restore":"", "result": "1"})

        else:
            dumplogger.info('No need to executing of choose PFB version process.')

    except KeyError:
        dumplogger.info("Not in golden case mode!")
    except:
        dumplogger.exception("Please check error messages...")

    dumplogger.info('Prepare leave AndroidInitialDriver')


def AndroidSetUpAppiumDesiredCaps():
    '''
    AndroidSetUpAppiumDesiredCaps : Set Up Appium Desired Caps by Driver
            Input argu : N/A
            Return code : N/A
    '''
    dumplogger.info('Prepare Enter AndroidSetUpAppiumDesiredCaps')

    ##Set Up Appium Desired Caps by Driver
    try:
        ##get appium port number based on which device
        chrome_driver_port = Config._AppiumDevicePort_["chrome_driver_port"] + int(Config._DeviceNumber_)
        system_port = Config._AppiumDevicePort_["system_port"] + int(Config._DeviceNumber_)

        country = Config._TestCaseRegion_.lower()

        ##Assign appium capabillity
        Config._DesiredCaps_['udid'] = Config._DeviceID_
        Config._DesiredCaps_['chromeDriverPort'] = str(chrome_driver_port)
        Config._DesiredCaps_['systemPort'] = system_port
        Config._DesiredCaps_['deviceName'] = Config._DeviceID_
        Config._DesiredCaps_['platformVersion'] = GetAndroidDeviceInfo(Config._DeviceID_)

        if "Espresso" in Config._DesiredCaps_.values():
            print "Initial driver with Espresso!!! Will set up Espresso Desired Capabilities!!!"
            dumplogger.info("Initial driver with Espresso!!! Will set up Espresso Desired Capabilities!!!.")
            Config._DesiredCaps_['appPackage'] = 'com.shopee.' + country + '.debug'
            Config._DesiredCaps_['app'] = Config._EspressoAPKPath_.replace('country', Config._TestCaseRegion_.lower())
            Config._DesiredCaps_['showGradleLog'] = False
            Config._DesiredCaps_['useKeystore'] = True
            Config._DesiredCaps_['keystorePath'] = Config._EspressoKeyStorePath_
            Config._DesiredCaps_['keystorePassword'] = "android"
            Config._DesiredCaps_['keyAlias'] = "androiddebugkey"
            Config._DesiredCaps_['keyPassword'] = "android"
            Config._DesiredCaps_['forceEspressoRebuild'] = True
            Config._DesiredCaps_['appWaitActivity'] = "com.shopee.app.ui.*"
            Config._DesiredCaps_['espressoBuildConfig'] = "{ \"additionalAppDependencies\": [ \"androidx.lifecycle:lifecycle-extensions:2.0.0\", \"androidx.lifecycle:lifecycle-common:2.0.0\", \"androidx.annotation:annotation:1.1.0\"]}"
            Config._DesiredCaps_['espressoServerLaunchTimeout'] = "600000"
        else:
            print "Initial driver with uiautomator2!!! Will set up uiautomator2 Desired Capabilities!!!"
            dumplogger.info("Initial driver with uiautomator2!!! Will set up uiautomator2 Desired Capabilities!!!.")
            Config._DesiredCaps_['automationName'] = "UiAutomator2"
            Config._DesiredCaps_['appWaitActivity'] = 'com.shopee.app.ui.*'
            Config._DesiredCaps_['appPackage'] = 'com.shopee.' + country + '.int'
            Config._DesiredCaps_['unicodeKeyboard'] = True
            Config._DesiredCaps_['resetKeyboard'] = True
            Config._DesiredCaps_['wdaStartupRetries'] = '4'
            Config._DesiredCaps_['iosInstallPause'] = '8000'
            Config._DesiredCaps_['wdaStartupRetryInterval'] = '20000'
            Config._DesiredCaps_['uiautomator2ServerLaunchTimeout'] = '180000'
            Config._DesiredCaps_['uiautomator2ServerReadTimeout'] = '360000'

        dumplogger.info("Start to grant permissions and clear residual data in phone.")
        ##Grant app permission & Clear residual data only when case starts, don't need to do this action when teardown
        if Parser._CASE_['failskip'] == 0:
            ##Grant app permission
            AndroidGrantAppPermission({"permission_type":"default", "result": "1"})
            ##Clear residual appium data in phone
            AndroidUninstallAppiumPackage({"result": "1"})

    except:
        print "Set up appium desired caps error!!! Please check debug log for further information."
        dumplogger.exception("Set up appium desired caps error!!! Please check debug log for further information.")
        print traceback.print_exc()

    dumplogger.info('Prepare leave AndroidSetUpAppiumDesiredCaps')

def AndroidConnectWithAppium():
    '''
    AndroidConnectWithAppium : Establish connection with appium
            Input argu : N/A
            Return code : N/A
    '''
    dumplogger.info('Prepare Enter AndroidConnectWithAppium')
    dumplogger.info(Config._DesiredCaps_)
    ##Establish connection with appium
    try:
        ##Check device cpu usage is stable
        AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

        webdriver_url = 'http://localhost:' + Config._DesiredCaps_['chromeDriverPort'] + '/wd/hub'
        driver = webdriver.Remote(webdriver_url, Config._DesiredCaps_)

        ##Check device cpu usage is stable
        AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

        global _AndroidDriver_
        _AndroidDriver_ = driver
        print "Appium Driver has connected!!!"

    except:
        print "Connect With Appium driver error!!! Please check debug log for further information."
        dumplogger.exception("Connect With Appium driver error!!! Please check debug log for further information.")
        dumpsys_activity = SendADBCommand({"command_type": "DumpsysActivity", "adb_arg": "", "is_return": "1", "result": "1"})
        dumplogger.exception("print dumpsys activity")
        dumplogger.exception(dumpsys_activity)

    dumplogger.info('Prepare leave AndroidConnectWithAppium')

def AppiumConnectionChecker():
    '''
    AppiumConnectionChecker : Check appium connection, if disconnect will retry
            Input argu : N/A
            Return code : N/A
    '''
    try:
        if "com.shopee" not in _AndroidDriver_.current_activity:
            dumplogger.info("The activity is : %s ", str(_AndroidDriver_.current_activity))

            ##Because feature "Notification" has to check noti out of shopee app
            if ("Notification" in Config._TestCaseFeature_) or ("UnifiedLink" in Config._TestCaseFeature_):
                dumplogger.info("Notification pass ok in re-connection appium")
            else:
                dumplogger.info("Prepare re-connection appium")
                AndroidConnectWithAppium()
    except AttributeError:
        dumplogger.info("Encounter Attribute Error and pass.")
    except WebDriverException:
        dumplogger.exception("Encounter WebDriver Error.")
    except:
        dumplogger.exception("Encounter unknown issue, please check exception.")
        AndroidConnectWithAppium()

@DecoratorHelper.FuncRecorder
def AndroidDeInitialDriver(arg):
    '''
    AndroidDeInitialDriver : deinitial Android driver
            Input argu : N/A
            Return code : N/A
    '''

    ##Declare _AndroidDriver_ in AndroidDeInitialDriver to adjust it's attr
    global _AndroidDriver_

    try:
        ##Quit android driver
        ##Because of Encountering WebDriverException: unknown server-side error of socket hang up
        ##Try to replace driver.quit() to driver.close() to handle this issue
        _AndroidDriver_.quit()
        _AndroidDriver_ = ""

        ##After appium upgrade, close could not be run after quit
        ##So comment _AndroidDriver_.close() and keep observation
        #_AndroidDriver_.close()

    except AttributeError:
        print "Please initial Android driver first"
        dumplogger.info("Please initial Android driver first")

    except:
        dumplogger.info("Please check error message on cmd")
        dumplogger.exception('Got exception error')

@DecoratorHelper.FuncRecorder
def AndroidInput(arg):
    '''
    AndroidInput : Input any strings in Android app
            Input argu :
                locate - Position in Android app
                string - Input strings
                method - xpath, css, id
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    locate = arg['locate']
    string = arg['string']
    method = arg['method']
    ret = 1

    dumplogger.info("locate = %s" % (locate))
    dumplogger.info("string = %s" % (string))
    dumplogger.info("method = %s" % (method))

    ##Detect path to find element by method
    try:
        if method == "xpath":
            WebDriverWait(_AndroidDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).clear()
            time.sleep(1)
            WebDriverWait(_AndroidDriver_, _GeneralWaitingPeriod_).until(EC.presence_of_element_located((By.XPATH, (locate)))).send_keys(string)
            message = "Input %s" % (str(string))
        else:
            message = "Please check method."
            dumplogger.debug(message)
    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        ret = 0
    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    time.sleep(0.5)
    OK(ret, int(arg['result']), 'AndroidInput->' + message)

@DecoratorHelper.FuncRecorder
def AndroidKeyboardAction(arg):
    '''
    AndroidKeyboardAction : Determine kinds of keyboard action used on Android.
            Input argu :
                actiontype - which action on android device, back/home/delete/enter/paste/select_all
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    actiontype = arg["actiontype"]
    ret = 1

    time.sleep(3)
    if actiontype == 'back':
        _AndroidDriver_.press_keycode(4)
    elif actiontype == 'home':
        _AndroidDriver_.press_keycode(3)
    elif actiontype == 'delete':
        _AndroidDriver_.press_keycode(67)
    elif actiontype == 'enter':
        _AndroidDriver_.press_keycode(66)
    elif actiontype == 'paste':
        _AndroidDriver_.press_keycode(279)
    elif actiontype == 'select_all':
        _AndroidDriver_.press_keycode(29, 28672)

    OK(ret, int(arg['result']), 'AndroidKeyboardAction->' + actiontype)

@DecoratorHelper.FuncRecorder
def AndroidKeyInNumbers(arg):
    '''
    AndroidKeyInNumbers : Use adb command to send keyboard numbers to device
            Input argu :
                number - the number to be input by adb command
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    number = arg["number"]
    ret = 1
    key = ""

    time.sleep(3)
    if number in ('1', '2', '3', '4', '5', '6', '7', '8', '9'):
        key = str(int(number)+7)
    else:
        key = ""
        dumplogger.debug("Please specify the right number to be input!!!")

    #Send adb command if there is key
    if key:
        SendADBCommand({"command_type":"KeyboardInput", "adb_arg": key, "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidKeyInNumbers->' + number)

@DecoratorHelper.FuncRecorder
def AndroidClick(arg):
    '''
    AndroidClick : Click any content in Android
            Input argu :
                locate - Position in Android
                method - method of locate element
                message - message of action
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg['locate']
    method = arg['method']
    message = arg['message']
    ret = 1

    try:
        if method == "xpath":
            WebDriverWait(_AndroidDriver_, _GeneralWaitingPeriod_).until(EC.element_to_be_clickable((By.XPATH, (locate)))).click()
        else:
            dumplogger.debug("Please check method!!")
            ret = 0

    except TimeoutException:
        message = "Cannot find element in %s seconds. Element xpath => %s" % (_GeneralWaitingPeriod_, locate)
        dumplogger.exception(message)
        #AndroidGetElementTreeAndCompare({"locate": locate})
        ret = 0
    except WebDriverException:
        message = "Please see the exception message, or check your element xpath => %s" % (locate)
        dumplogger.exception(message)
        ret = 0
    except:
        message = "Encounter unknown exception."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'AndroidClick->' + message)

def AndroidClickCoordinates(arg):
    '''
    AndroidClickCoordinates : Click coordiantes on Android device
            Input argu :
                x_cor - X coordinate
                y_cor - Y coordinate
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    dumplogger.info('Prepare enter AndroidClickCoordinates')
    AppiumConnectionChecker()
    x_cor = arg['x_cor']
    y_cor = arg['y_cor']
    ret = 1

    ##Define output case folder first
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Record current timestamp first
    cur_time = datetime.datetime.fromtimestamp(time.time()).strftime("%H%M%S")

    ##Click coordinates of touch action
    try:
        actions = TouchAction(_AndroidDriver_)
        if Config._DesiredCaps_['automationName'] == "Espresso":
            ##Expresso uses adb command to click by coordinates
            dumplogger.info("Expresso click by coordinates")
            coordinates_command = str(x_cor) + " " + str(y_cor)
            SendADBCommand({"command_type": "ClickCoordinates", "adb_arg": coordinates_command, "is_return": "1", "result": "1"})
        else:
            ##UIAutomator uses appium command to click by coordinates
            dumplogger.info("UIAutomator click by coordinates")
            actions.tap(x=x_cor,y=y_cor).perform()

    ##handle InvalidElementStateException error
    except InvalidElementStateException:
        ret = -1
        dumplogger.exception('Encounter InvalidElementStateException, at current time stamp: %s' % (cur_time))
        _AndroidDriver_.get_screenshot_as_file(output_casename_folder + cur_time + "_click_coor_failed.png")

    ##handle unknown error
    except:
        ret = -1
        dumplogger.exception('Encounter unknown error, at current time stamp: %s' % (cur_time))
        _AndroidDriver_.get_screenshot_as_file(output_casename_folder + cur_time + "_click_coor_failed.png")

    dumplogger.info('Prepare leave AndroidClickCoordinates')
    return ret

@DecoratorHelper.FuncRecorder
def AndroidGetElements(arg):
    '''
    AndroidGetElements : Get any elements in Android
            Input argu :
                locate - Position in android
                mode - single, multi
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg["locate"]
    mode = arg["mode"]
    message = ""
    ret = 1

    ##Get Android elements by method
    try:
        ##For test the efficiency of WebDriverWait, we annotate implicitly_wait
        if mode == "single":
            element = WebDriverWait(_AndroidDriver_, 30).until(EC.presence_of_element_located((By.XPATH, (locate))))
            GlobalAdapter.CommonVar._PageElements_ = element

        elif mode == "multi":
            element = WebDriverWait(_AndroidDriver_, 30).until(EC.presence_of_element_located((By.XPATH, (locate))))
            GlobalAdapter.CommonVar._PageElements_ = element

    except TimeoutException:
        message = "Cannot find element in 30 seconds. Element xpath => %s" % (locate)
        dumplogger.error(message)
        ret = 0

    except IndexError:
        message = "Index error"
        dumplogger.exception(message)
        ret = -1

    except NoSuchElementException:
        message = "Get elements failed"
        dumplogger.exception(message)
        #AndroidGetElementTreeAndCompare({"locate": locate})
        ret = -1

    OK(ret, int(arg['result']), 'AndroidGetElements->' + message)

@DecoratorHelper.FuncRecorder
def AndroidParseElements(arg):
    '''
    AndroidParseElements : Parse any elements in android
            Input argu :
                keyword - the keyword want to check
                isfuzzy - output message
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    isfuzzy = arg["isfuzzy"]

    ret = 0
    if GlobalAdapter.CommonVar._PageAttributes_ is None:
        dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = None")
        message = "Page attribute is empty"
        ret = 0

    elif "\n" in GlobalAdapter.CommonVar._PageAttributes_:
        dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = " + GlobalAdapter.CommonVar._PageAttributes_)

        if arg['keyword'] in GlobalAdapter.CommonVar._PageAttributes_:
            message = "Keyword found"
            ret = 1

        else:
            message = "Keyword not found"
            ret = 0
    else:
        ##Check string is match
        try:
            dumplogger.info("GlobalAdapter.CommonVar._PageAttributes_ = " + GlobalAdapter.CommonVar._PageAttributes_)

            if arg['isfuzzy'] == "1":
                if arg['keyword'] in GlobalAdapter.CommonVar._PageAttributes_:
                    message = "Keyword found"
                    ret = 1
                else:
                    message = "Keyword not found"
                    ret = 0
            elif arg['isfuzzy'] == "0":
                if arg['keyword'] == GlobalAdapter.CommonVar._PageAttributes_.strip():
                    message = "Keyword fully matched"
                    ret = 1
                else:
                    message = "Keyword not matched"
                    ret = 0
            else:
                message = "Check isfuzzy."
                ret = 0
        except KeyError:
            message = "Encounter keyError."
            dumplogger.exception(message)
        except:
            message = "Encounter unknown error."
            dumplogger.exception(message)

    OK(ret, int(arg['result']), 'AndroidParseElements->' + message)

def AndroidRelativeSwipe(arg):
    '''
    AndroidRelativeSwipe : Swipe the page by corridinate
            Input argu :
                locate_x - initial cooridinate of x-axis
                locate_y - initial cooridinate of y-axis
                movex - degree of movement of x-axis
                movey - degree of movement of y-axis
            Addition Argu :
                times - swipe time

            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    locate_x = int(arg["locate_x"])
    locate_y = int(arg["locate_y"])
    movex = int(arg["movex"])
    movey = int(arg["movey"])
    message = ""
    ret = 1

    if "times" in arg:
        times = arg['times']
    else:
        times = "1"

    try:
        for swipe_times in range(int(times)):
            if Config._DesiredCaps_['automationName'] == "Espresso":
                ##Expresso uses adb command to swipe (since Expressp does not support appium swipe command)
                dumplogger.info("Expresso swipe")
                swipe_command = str(locate_x) + " " + str(locate_y) + " " + str(movex) + " " + str(movey)
                SendADBCommand({"command_type": "Swipe", "adb_arg": swipe_command, "is_return": "1", "result": "1"})
            else:
                ##UIAutomator uses appium command to swipe
                dumplogger.info("UIAutomator swipe")
                _AndroidDriver_.swipe(locate_x, locate_y, movex, movey)
                dumplogger.info("Swipe times: %d" % (swipe_times))
            dumplogger.info("Swipe times: %d" % (swipe_times))
    except WebDriverException:
        message = "Encounter webdriver error."
        dumplogger.exception(message)
        ret = -1
    except:
        message = "Encounter unknown error."
        dumplogger.exception(message)
        ret = -1

    OK(ret, int(arg['result']), 'AndroidRelativeSwipe->' + message)

@DecoratorHelper.FuncRecorder
def AndroidGetAttributes(arg):
    '''
    AndroidGetAttributes : Get element attribute from _PageElements_
            Input argu :
                attrtype - text, location, count, get_attribute
                attribute - attribute for get_attribute
                mode - single, multi
                message - output message
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    GlobalAdapter.CommonVar._PageAttributes_ = ""
    GlobalAdapter.CommonVar._PageAttributesList_ = []
    attrtype = arg["attrtype"]
    message = ""
    ret = 1

    try:
        attribute = arg["attribute"]
    except KeyError:
        attribute = ""
        dumplogger.info('KeyError - attribute default is 0')
    except:
        attribute = ""
        dumplogger.info('Other error - attribute default is 0')
    finally:
        dumplogger.info("attribute = %s" % (attribute))

    try:
        mode = arg["mode"]
    except KeyError:
        mode = "single"
        dumplogger.info('KeyError - mode default is 0')
    except:
        mode = "single"
        dumplogger.info('Other error - mode default is 0')
    finally:
        dumplogger.info("mode = %s" % (mode))

    try:
        if mode == "single":
            if attrtype == 'text':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.text
            elif attrtype == 'location':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.location
            elif attrtype == 'size':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.size
            elif attrtype == 'get_attribute':
                GlobalAdapter.CommonVar._PageAttributes_ = GlobalAdapter.CommonVar._PageElements_.get_attribute(attribute)

        elif mode == "multi":
            for tmp in GlobalAdapter.CommonVar._PageElements_:
                if attrtype == 'text':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.text)
                elif attrtype == 'location':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.location)
                elif attrtype == 'get_attribute':
                    GlobalAdapter.CommonVar._PageAttributesList_.append(tmp.get_attribute(attribute))
            if attrtype == 'count':
                GlobalAdapter.CommonVar._PageAttributes_ = len(GlobalAdapter.CommonVar._PageElements_)

    except NoSuchElementException:
        message = "Get PageAttributes failed"
        dumplogger.exception(message)
        ret = 0

    finally:
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributes_)
        dumplogger.info(GlobalAdapter.CommonVar._PageAttributesList_)

    OK(ret, int(arg['result']), 'AndroidGetAttributes->' + message)

def AndroidCheckElementExist(arg):
    '''
    AndroidCheckElementExist : Check the element exist or not
            Input argu :
                locate - Position in web
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ##
    ##DO NOT use decoratorHelper on this function since this function will return value
    ##
    dumplogger.info("Enter AndroidCheckElementExist")
    AppiumConnectionChecker()

    locate = arg["locate"]
    message = "Exist"
    ret = 1

    try:
        passok = arg["passok"]
    except KeyError:
        passok = "1"
        dumplogger.info('KeyError - passok default is 1')
    except:
        passok = "1"
        dumplogger.info('Other error - passok default is 1')
    finally:
        dumplogger.info("passok = %s" % (passok))

    ##For test the efficiency of WebDriverWait, we annotate implicitly_wait
    try:
        WebDriverWait(_AndroidDriver_, 30).until(EC.presence_of_element_located((By.XPATH, (locate))))
    except TimeoutException:
        ret = 0
        message = "Cannot find element in 30 seconds. Element xpath => %s" % (locate)
        dumplogger.exception(message)
    except NoSuchElementException:
        ret = 0
        message = "Nonexistent. Element xpath => %s" % (locate)
        dumplogger.exception(message)
        #AndroidGetElementTreeAndCompare({"locate": locate})

    if passok == "1":
        OK(ret, int(arg['result']), 'AndroidCheckElementExist->' + message)
    else:
        return ret

@DecoratorHelper.FuncRecorder
def AndroidLongPress(arg):
    '''
    AndroidLongPress : Long press on an element
            Input argu :
                locate - location of the element
                sec - press for how long
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    locate = arg["locate"]
    sec = arg["sec"]
    ret = 1
    message = ""

    action = TouchAction(_AndroidDriver_)
    ##Define output case folder first
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Record current timestamp first
    cur_time = datetime.datetime.fromtimestamp(time.time()).strftime("%H%M%S")

    try:
        action.long_press(_AndroidDriver_.find_element_by_xpath(locate), duration=int(sec)*1000).release().perform()

    except NoSuchElementException:
        message = "No Such Element."
        _AndroidDriver_.get_screenshot_as_file(output_casename_folder + cur_time + "_nosuch_element_exception_failed.png")
        dumplogger.info(message)
        #AndroidGetElementTreeAndCompare({"locate": locate})
    except:
        message = "Encounter unexpected error."
        _AndroidDriver_.get_screenshot_as_file(output_casename_folder + cur_time + "_click_long_press_failed.png")
        dumplogger.exception(message)

    OK(ret, int(arg['result']), 'AndroidLongPress->' + message)

@DecoratorHelper.FuncRecorder
def AndroidCleanAPPData(arg):
    '''
    AndroidCleanAPPData : Clean Shopee APP Data and re-open shopee app again
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Send Adb command to clean app data
    SendADBCommand({"command_type": "CleanAppData", "adb_arg": Config._DesiredCaps_['appPackage'], "is_return": "0", "result": "1"})

    ##Re-open shopee APP again
    AndroidCloseShopeeAPP({"result": "1"})
    AndroidGrantAppPermission({"permission_type":"default", "result": "1"})
    AndroidOpenShopeeAPP({"result": "1"})
    AndroidSelectLanguage()
    AndroidHandleTutorial()

    ##Close cookies message in specific region
    if Config._TestCaseRegion_ in ("PL", "ES"):
        AndroidCloseCookiesPopUp({"result": "1"})

    ##Close banner and switch env
    time.sleep(20)
    AndroidCloseBanner({"banner_type":"home"})

    ##Change env
    AndroidSwitchEnv({})

    ##Check app launch
    AndroidCheckAppLaunch({"result": "1"})

    ##Handle bundle
    AndroidHandleLiveBundle()

    ##Close RNLogger pop Up
    time.sleep(20)
    AndroidCloseRNLoggerPopUp()

    if Config._TestCaseRegion_ in ("PH"):
        time.sleep(60)

    ##Deal with UAT error
    #AndroidCloseUATError({"result": "1"})
    if Config._SwitchEnv_:
        AndroidSetPFB({"is_restore":"", "result": "1"})
    else:
        dumplogger.info('No need to executing of choose PFB version process.')

    ##Handle bundle
    AndroidHandleLiveBundle()

    OK(ret, int(arg['result']), 'AndroidCleanAPPData')

@DecoratorHelper.FuncRecorder
def AndroidCloseUATError(arg):
    '''
    AndroidCloseUATError : Close UAT error popup message if there is one, since UAT error message may intercept btn press in page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Detect uat error title message
    xpath = Util.GetXpath({"locate":"uat_error_title"})
    if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):

        message_xpath = Util.GetXpath({"locate": "network_request_message"})
        if AndroidCheckElementExist({"method": "xpath", "locate": message_xpath, "passok": "0", "result": "1"}):
            AndroidSetProxyNetwork({"action_type": "disable", "result": "1"})
            time.sleep(10)
            AndroidSetProxyNetwork({"action_type": "enable", "result": "1"})
            time.sleep(10)

        xpath = Util.GetXpath({"locate":"ok_btn"})
        AndroidClick({"method":"xpath", "locate":xpath, "message":"Click ok btn of uat error message.", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidCloseUATError')

@DecoratorHelper.FuncRecorder
def AndroidCloseContextualizeForbidden(arg):
    '''
    AndroidCloseContextualizeForbidden : Close contextualize forbidden toggle in forbidden zone
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    time.sleep(10)
    AndroidCloseBanner({"banner_type":"home"})

    ##Long press under bar to forbidden zone
    xpath = Util.GetXpath({"locate":"long_press_btn"})
    AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})
    time.sleep(2)

    ##Move to native feature test section and click
    AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "3", "result": "1"})
    xpath = Util.GetXpath({"locate":"native_feature_test_section"})
    AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "1", "result": "1"})
    time.sleep(5)

    ##Move to contextualizeforbidden section
    AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "15", "result": "1"})
    xpath = Util.GetXpath({"locate": "contextualize_forbidden_section"})
    AndroidBaseUILogic.AndroidMoveAndCheckElement({"locate": xpath, "direction": "down", "isclick": "0", "result": "1"})
    time.sleep(5)

    ##If contextualizeforbidden toggle is open status, and click toggle to close
    xpath = Util.GetXpath({"locate":"contextualize_forbidden_toggle_open"})
    if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
        AndroidClick({"method":"xpath", "locate":xpath, "message":"Press contextualize forbidden toggle", "result": "1"})
        time.sleep(30)
    else:
        AndroidKeyboardAction({"actiontype": "back", "result": "1"})
        AndroidKeyboardAction({"actiontype": "back", "result": "1"})
        AndroidKeyboardAction({"actiontype": "back", "result": "1"})
        dumplogger.info('No need to close contextualize forbidden toggle. Back to homepage.')

    OK(ret, int(arg['result']), 'AndroidCloseContextualizeForbidden')

@DecoratorHelper.FuncRecorder
def AndroidCleanInstallBundle(arg):
    '''
    AndroidCleanInstallBundle : Clean install bundle in forbidden zone
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Disable device proxy to reload bundle
    AndroidSetProxyNetwork({"action_type": "disable", "result": "1"})
    time.sleep(5)

    ##Close banner
    AndroidCloseBanner({"banner_type":"home"})

    ##Long press under bar to forbidden zone
    xpath = Util.GetXpath({"locate":"long_press_btn"})
    AndroidLongPress({"locate":xpath, "sec":"2", "result": "1"})
    time.sleep(2)

    ##Press clean install bundle btn
    AndroidBaseUILogic.AndroidRelativeMove({"direction": "down", "times": "1", "result": "1"})
    for retry_time in range(3):
        xpath = Util.GetXpath({"locate":"clean_bundle_btn"})
        AndroidClick({"method":"xpath", "locate":xpath, "message":"Press clean install bundle btn", "result": "1"})
        time.sleep(10)

        ##If there's socket confirm popup, click ok
        xpath = Util.GetXpath({"locate":"socket_confirm"})
        if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AndroidClick({"method":"xpath", "locate":xpath, "message":"Press OK btn", "result": "1"})
            time.sleep(20)

        ##If page is still in PFB page, press back and repress "Reset Feature Branch"
        xpath = Util.GetXpath({"locate":"pfb_page_title"})
        if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            time.sleep(20)
            AndroidKeyboardAction({"actiontype":"back", "result": "1"})
            time.sleep(3)
        else:
            ret = 1
            break

    ##Enable device proxy
    AndroidSetProxyNetwork({"action_type": "enable", "result": "1"})

    ##Wait app reactive
    time.sleep(25)

    ##Check device cpu usage is stable
    AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

    OK(ret, int(arg['result']), 'AndroidCleanInstallBundle')

@DecoratorHelper.FuncRecorder
def AndroidResetFeatureBranch(arg):
    '''
    AndroidResetFeatureBranch : Reset feature branch in forbidden zone
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 0

    ##Disable device proxy to reload bundle
    AndroidSetProxyNetwork({"action_type": "disable", "result": "1"})
    time.sleep(5)

    ##Close banner
    AndroidCloseBanner({"banner_type":"home"})

    ##Long press under bar to forbidden zone
    xpath = Util.GetXpath({"locate":"long_press_btn"})
    AndroidLongPress({"locate":xpath, "sec":"2", "result": "1"})
    time.sleep(2)

    ##Press reset feature branch btn
    for retry_time in range(3):
        xpath = Util.GetXpath({"locate":"reset_feature_btn"})
        AndroidClick({"method":"xpath", "locate":xpath, "message":"Press reset feature branch btn", "result": "1"})
        time.sleep(10)

        ##If there's socket confirm popup, click ok
        xpath = Util.GetXpath({"locate":"socket_confirm"})
        if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            AndroidClick({"method":"xpath", "locate":xpath, "message":"Press OK btn", "result": "1"})
            time.sleep(20)

        ##If page is still in PFB page, press back and repress "Reset Feature Branch"
        xpath = Util.GetXpath({"locate":"pfb_page_title"})
        if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            time.sleep(20)
            AndroidKeyboardAction({"actiontype":"back", "result": "1"})
            time.sleep(3)
        else:
            ret = 1
            break

    ##Enable device proxy
    AndroidSetProxyNetwork({"action_type": "enable", "result": "1"})

    ##Wait app reactive
    time.sleep(20)

    ##Check device cpu usage is stable
    AndroidCheckDeviceCPUUsage({"max_cpu_usage": "100"})

    OK(ret, int(arg['result']), 'AndroidResetFeatureBranch')

@DecoratorHelper.FuncRecorder
def AndroidHandleUnknownError(arg):
    '''
    AndroidHandleUnknownError : Handle unknown error in home page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    time.sleep(5)

    ##Check error text exist or not
    xpath = Util.GetXpath({"locate": "dismiss_btn"})

    for retry_times in range(1, 5):
        if AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            ##Click dismiss button
            xpath = Util.GetXpath({"locate": "dismiss_btn"})
            AndroidClick({"method": "xpath", "locate": xpath, "message": "click dismiss", "result": "1"})
            time.sleep(3)
        else:
            dumplogger.info("No more dismiss button to click, will leave the loop directly.")
            break

    OK(ret, int(arg['result']), 'AndroidHandleUnknownError')

@DecoratorHelper.FuncRecorder
def AndroidHandleWrongBundleError(arg):
    '''
    AndroidHandleWrongBundleError : Handle wrong bundle error in home page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    time.sleep(5)

    ##Check error text exist or not and close it
    xpath = Util.GetXpath({"locate": "close_error"})
    if AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
        AndroidClick({"method": "xpath", "locate": xpath, "message": "Click close error", "result": "1"})
        time.sleep(3)

    OK(ret, int(arg['result']), 'AndroidHandleWrongBundleError')

@DecoratorHelper.FuncRecorder
def AndroidHandleUnknownWarning(arg):
    '''
    AndroidHandleUnknownWarning : Handle Warning in seller page
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    time.sleep(5)

    ##Check error text exist or not
    xpath = Util.GetXpath({"locate":"dismiss_btn"})
    if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
        ##Click dismiss button
        xpath = Util.GetXpath({"locate":"dismiss_btn"})
        AndroidClick({"method":"xpath", "locate":xpath, "message":"click dismiss", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidHandleUnknownWarning')

@DecoratorHelper.FuncRecorder
def AndroidGrantAppPermission(arg):
    '''
    AndroidGrantAppPermission : Grant all Shopee APP need permissions
            Input argu :
                permission_type - default / location_only / except_location
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    permission_type = arg["permission_type"]

    if permission_type == "default":
        ## Grant all the permission in Config._AdbCommands_['GrantAndroidAccessType']
        for each_permission_key, each_permission_type in Config._AdbCommands_['GrantAndroidAccessType'].iteritems():

            ##For default permission type, will grant permission
            adb_arg = Config._DesiredCaps_['appPackage'] + each_permission_type
            SendADBCommand({"command_type": "GrantAPPAccess", "adb_arg": adb_arg, "is_return": "0", "result": "1"})

    elif permission_type == "except_location":

        ##Revoke location permission type
        adb_arg = Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Access_Fine_Location"]
        SendADBCommand({"command_type": "RevokeAPPAccess", "adb_arg": adb_arg, "is_return": "0", "result": "1"})
        adb_arg = Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Access_Coarse_Location"]
        SendADBCommand({"command_type": "RevokeAPPAccess", "adb_arg": adb_arg, "is_return": "0", "result": "1"})
        adb_arg = Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Access_Background_Location"]
        SendADBCommand({"command_type": "RevokeAPPAccess", "adb_arg": adb_arg, "is_return": "0", "result": "1"})

    elif permission_type == "location_only":

        #For default permission type, will skip to grant location permission
        for each_permission_key, each_permission_type in Config._AdbCommands_['GrantAndroidAccessType'].iteritems():
            if each_permission_key not in ("Access_Fine_Location", "Access_Coarse_Location", "Access_Background_Location"):
                adb_arg = Config._DesiredCaps_['appPackage'] + each_permission_type
                SendADBCommand({"command_type": "RevokeAPPAccess", "adb_arg": adb_arg, "is_return": "0", "result": "1"})

            else:
                adb_arg = Config._DesiredCaps_['appPackage'] + each_permission_type
                SendADBCommand({"command_type": "GrantAPPAccess", "adb_arg": adb_arg, "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidGrantAppPermission')

@DecoratorHelper.FuncRecorder
def AndroidUninstallAppiumPackage(arg):
    '''
    AndroidUninstallAppiumPackage : Uninstall appium related packages in device for restore env
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    packages = ['io.appium.uiautomator2.server', 'io.appium.uiautomator2.server.test', 'io.appium.settings', 'io.appium.unlock']

    for each_command in packages:
        ##Use adb command to uninstall appium packages
        SendADBCommand({"command_type": "Uninstall", "adb_arg": each_command, "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidUninstallAppiumPackage')

@DecoratorHelper.FuncRecorder
def AndroidOpenShopeeAPP(arg):
    '''
    AndroidOpenShopeeAPP : Open Shopee APP
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Check device cpu usage is stable
    AndroidCheckDeviceCPUUsage({"max_cpu_usage": "70"})

    ##Use adb command to launch shopee
    app_pacakge = Config._DesiredCaps_['appPackage'] + '/' + Config._DesiredCaps_['appActivity']
    SendADBCommand({"command_type": "LaunchAPP", "adb_arg": app_pacakge, "is_return": "0", "result": "1"})

    ##Sleep 5s to wait adb launch shopee is active
    time.sleep(5)

    ##Check device cpu usage is stable
    AndroidCheckDeviceCPUUsage({"max_cpu_usage": "70"})

    OK(ret, int(arg['result']), 'AndroidOpenShopeeAPP')

@DecoratorHelper.FuncRecorder
def AndroidCloseShopeeAPP(arg):
    '''
    AndroidCloseShopeeAPP : Close Shopee APP
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Use adb command to close APP
    SendADBCommand({"command_type": "StopAPP", "adb_arg": Config._DesiredCaps_['appPackage'], "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidCloseShopeeAPP')

@DecoratorHelper.FuncRecorder
def AndroidSetAirplaneMode(arg):
    '''
    AndroidSetAirplaneMode : Set Airplane Mode
            Input argu :
                action_type - open / close
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    action_type = arg["action_type"]

    if action_type == "open":
        ##Use adb command to open Airplane Mode
        SendADBCommand({"command_type": "SetAirplaneMode", "adb_arg": "1", "is_return": "0", "result": "1"})

    elif action_type == "close":
        ##Use adb command to close Airplane Mode
        SendADBCommand({"command_type": "SetAirplaneMode", "adb_arg": "0", "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidSetAirplaneMode -> ' + action_type)

@DecoratorHelper.FuncRecorder
def AndroidSetProxyNetwork(arg):
    '''
    AndroidSetProxyNetwork : Set up android proxy network
            Input argu :
                action_type - enable / disable
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    action_type = arg["action_type"]

    ##Check device use proxy or not
    if Config._EnableProxy_:

        ##Open device proxy
        if action_type == "enable":
            ##Get proxy ip and port. Example: 192.168.0.1:8001
            proxy_ip_port = "%s:%s" % (Config._DeviceHostIP_, Config._ProxyInfo_["port"] + int(Config._DeviceNumber_))
            dumplogger.info("Proxy ip:port => %s" % (proxy_ip_port))

            ##Use adb command to set http proxy. ADB command example => adb shell settings put global http_proxy 192.168.0.1:8001
            SendADBCommand({"command_type": "SetHttpProxy", "adb_arg": proxy_ip_port, "is_return": "0", "result": "1"})

        ##Close device proxy
        elif action_type == "disable":
            ##Use adb command to remove http proxy. ADB command example => adb shell settings put global http_proxy :0
            SendADBCommand({"command_type": "SetHttpProxy", "adb_arg": ":0", "is_return": "0", "result": "1"})

        else:
            ret = 0
            dumplogger.error("Please check your action type is enable or disable!!!")
    else:
        dumplogger.info("Device no need to set up proxy")

    OK(ret, int(arg['result']), 'AndroidSetProxyNetwork -> ' + action_type)

@DecoratorHelper.FuncRecorder
def AndroidSetWifi(arg):
    '''
    AndroidSetWifi : Set Wifi
            Input argu :
                action_type - enable / disable
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    action_type = arg["action_type"]

    ##Use adb command to enable/disable wifi
    SendADBCommand({"command_type":"SetNetwork", "adb_arg":"wifi " + action_type, "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidSetWifi -> ' + action_type)

@DecoratorHelper.FuncRecorder
def AndroidSetLTE(arg):
    '''
    AndroidSetLTE : Set LTE
            Input argu :
                action_type - enable / disable
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    action_type = arg["action_type"]

    ##Use adb command to enable/disable LTE
    SendADBCommand({"command_type":"SetMobileNetwork", "adb_arg": action_type, "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidSetLTE -> ' + action_type)

@DecoratorHelper.FuncRecorder
def OpenShopeeAPPFromBackground(arg):
    '''
    OpenShopeeAPPFromBackground : Open the shopee app in the background and lauch from the same screen
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Use adb command to open the shopee app in the background and lauch from the same screen
    SendADBCommand({"command_type":"OpenAPPfromBackground", "adb_arg":Config._DesiredCaps_['appPackage'] + ' 1', "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'OpenShopeeAPPFromBackground')

@DecoratorHelper.FuncRecorder
def SetShopeeAPPStoragePermission(arg):
    '''
    SetShopeeAPPStoragePermission : Open the shopee app storage permission
            Input argu :
                action_type - grant / revoke
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    action_type = arg["action_type"]

    if action_type == "grant":

        ##Use adb command to grant shopee app storage permission
        SendADBCommand({"command_type":"GrantAPPAccess", "adb_arg":Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Write_External_Storage"], "is_return": "0", "result": "1"})
        SendADBCommand({"command_type":"GrantAPPAccess", "adb_arg":Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Read_External_Storage"], "is_return": "0", "result": "1"})

    elif action_type == "revoke":

        ##Use adb command to revoke shopee app storage permission
        SendADBCommand({"command_type":"RevokeAPPAccess", "adb_arg":Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Write_External_Storage"], "is_return": "0", "result": "1"})
        SendADBCommand({"command_type":"RevokeAPPAccess", "adb_arg":Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Read_External_Storage"], "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'SetShopeeAPPStoragePermission')

@DecoratorHelper.FuncRecorder
def SetShopeeAPPContactPermission(arg):
    '''
    SetShopeeAPPContactPermission : Open the shopee app contact permission
            Input argu :
                action_type - grant / revoke
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1
    action_type = arg["action_type"]

    if action_type == "grant":

        ##Use adb command to grant shopee app contact permission
        SendADBCommand({"command_type":"GrantAPPAccess", "adb_arg":Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Contacts"], "is_return": "0", "result": "1"})

    elif action_type == "revoke":

        ##Use adb command to revoke shopee app contact permission
        SendADBCommand({"command_type":"RevokeAPPAccess", "adb_arg":Config._DesiredCaps_['appPackage'] + Config._AdbCommands_['GrantAndroidAccessType']["Contacts"], "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'SetShopeeAPPContactPermission')

@DecoratorHelper.FuncRecorder
def AndroidClickDeviceHomeButton(arg):
    '''
    AndroidClickDeviceHomeButton : Back to the home
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Use adb command to back to the home
    SendADBCommand({"command_type":"KeyboardInput", "adb_arg":"KEYCODE_HOME", "is_return": "0", "result": "1"})

    OK(ret, int(arg['result']), 'AndroidClickDeviceHomeButton')

@DecoratorHelper.FuncRecorder
def AndroidSetPFB(arg):
    '''
    AndroidSetPFB : Switch Android PFB version
            Input argu:
                is_restore - reset feature branch to default
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    is_restore = arg['is_restore']
    ret = 1

    ##Get selected pfb name from ini file
    pfb_version_name = Config._PFB_["Android"]["pfb_name"].lower()
    dumplogger.info("Parser._CASE_['failskip'] : %s" % Parser._CASE_['failskip'])
    ##Proceed of switch PFB version when case initial
    if Parser._CASE_['failskip'] == 0:
        ##Long press under bar to switch PFB version
        xpath = Util.GetXpath({"locate":"long_press_btn"})
        AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        ##Get current pfb name
        xpath = Util.GetXpath({"locate":"current_pfb_name"})
        current_pfb_name = _AndroidDriver_.find_element_by_xpath(xpath).text
        if current_pfb_name != pfb_version_name:
            ##Press Feature Branch List tab for choosing PFB version
            xpath = Util.GetXpath({"locate":"feature_branch_list"})
            AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Feature Branch List tab", "result": "1"})
            time.sleep(5)

            ##Input selected PFB version name from ini file
            xpath = Util.GetXpath({"locate":"search_bar"})
            AndroidInput({"method":"xpath", "locate":xpath, "string":pfb_version_name, "result": "1"})
            time.sleep(2)

            ##Check selected PFB version name is exist or not, if exist, choose seleted PFB version name
            xpath = Util.GetXpath({"locate":"check_pfb_exist"})
            xpath = xpath.replace('replace_pfb_name', pfb_version_name)

            if AndroidCheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
                dumplogger.info('Select PFB name: %s', pfb_version_name)
                AndroidClick({"method":"xpath", "locate":xpath, "message":"Click PFB name to close keyboard", "result": "1"})
                AndroidClick({"method":"xpath", "locate":xpath, "message":"Click selected pfb name", "result": "1"})
                time.sleep(45)
                AndroidCloseShopeeAPP({"result": "1"})
                time.sleep(5)
                AndroidOpenShopeeAPP({"result": "1"})
            else:
                dumplogger.info('Please do make sure your PFB version name is correct in ini file!')
                ret = 0
        else:
            dumplogger.info('No need to switch other PFB version. Back to previus page.')
            AndroidKeyboardAction({"actiontype":"back", "result": "1"})

    ##Reset feature branch for special testing purpose, whatever case pass or fail
    if is_restore:
        ##Long press under bar to reset PFB version
        xpath = Util.GetXpath({"locate":"long_press_btn"})
        AndroidLongPress({"locate":xpath, "sec":"3", "result": "1"})

        ##Click to reset feature branch.
        dumplogger.info('Click Reset Feature Branch.')
        xpath = Util.GetXpath({"locate":"reset_pfb"})
        AndroidClick({"method":"xpath", "locate":xpath, "message":"Click Reset Feature Branch", "result": "1"})
        time.sleep(5)
    else:
        dumplogger.info('No need to reset PFB.')

    OK(ret, int(arg['result']), 'AndroidSetPFB')

@DecoratorHelper.FuncRecorder
def AndroidStartRecording(arg):
    '''
    AndroidStartRecording : Start recording mobile video using adb command
            Input Argu: N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    ret = 1
    message = ""

    ##Get current date time as file name
    GlobalAdapter.CommonVar._ScreenshotFilename_["android"] = Parser._CASE_['id'] + "_" + XtFunc.GetCurrentDateTime("%Y_%m_%d_%H%M", 0, 0, 0, 1)

    ##Setup recording command
    record_video_cmd = 'start /MIN cmd.exe @cmd /k adb -s ' + Config._DeviceID_ + ' shell "'

    ##Get screenshot and store to device SD card
    for record_time in range(10):
        record_video_cmd += 'screenrecord  --time-limit 180 /sdcard/' + GlobalAdapter.CommonVar._ScreenshotFilename_["android"] + "_" + str(record_time) + ".mp4" + '; '
    record_video_cmd += '"'

    try:
        ##Launch new command prompt for adb recording
        dumplogger.info(record_video_cmd)
        os.system(record_video_cmd)

        ##Get process ID
        GlobalAdapter.CommonVar._ProcessID_["android_screenshot"] = FrameWorkBase.PNameQueryProcess("cmd", str(record_video_cmd))
        dumplogger.info("pid: %s" % (GlobalAdapter.CommonVar._ProcessID_["android_screenshot"]))
        if GlobalAdapter.CommonVar._ProcessID_["android_screenshot"] == -1:
            ret = 0
            dumplogger.info("Get process ID Failed!!!!!!!!")

        ##Wait for driver initial
        time.sleep(5)

    ##Fail to record, but won't let framework stop
    except WindowsError:
        message = "Confront PID error."
        dumplogger.exception(message)
    except:
        message = "Unknown exception."
        dumplogger.exception(message)

    OK(ret, int(arg['result']), 'AndroidStartRecording->' + message)

@DecoratorHelper.FuncRecorder
def AndroidEndRecording(arg):
    '''
    AndroidEndRecording : Stop recording mobile video
            Input Argu: N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''

    ret = 1

    ##Output folder
    output_casename_folder = Config._OutputFolderPath_ + Config.dict_systemslash[Config._platform_] + Config._TestCaseType_ + Config.dict_systemslash[Config._platform_]

    ##Create log folder if not exist
    if os.path.exists(output_casename_folder):
        dumplogger.info(output_casename_folder + "path exist")
    else:
        command = "mkdir " + output_casename_folder
        os.system(command)
        dumplogger.info("command = " + command)

    ##Kill the process
    FrameWorkBase.KillProcessByPID(GlobalAdapter.CommonVar._ProcessID_["android_screenshot"])
    time.sleep(5)

    ##Pull video from mobile SD card and remove the video
    for record_time in range(10):
        #pull_recorded_video = 'start /MIN cmd.exe @cmd /c adb -s ' + Config._DeviceID_ + " pull /sdcard/" + GlobalAdapter.CommonVar._ScreenshotFilename_["android"] + "_" + str(record_time) + ".mp4"
        pull_recorded_video = 'adb -s ' + Config._DeviceID_ + " pull /sdcard/" + GlobalAdapter.CommonVar._ScreenshotFilename_["android"] + "_" + str(record_time) + ".mp4"
        pull_recorded_video += " " + output_casename_folder + "/" + GlobalAdapter.CommonVar._ScreenshotFilename_["android"] + "_" + str(record_time) + ".mp4"
        #remove_recorded_video = 'start /MIN cmd.exe @cmd /c adb -s ' + Config._DeviceID_ + " shell rm /sdcard/" + GlobalAdapter.CommonVar._ScreenshotFilename_["android"] + "_" + str(record_time) + ".mp4"
        remove_recorded_video = 'adb -s ' + Config._DeviceID_ + " shell rm /sdcard/" + GlobalAdapter.CommonVar._ScreenshotFilename_["android"] + "_" + str(record_time) + ".mp4"

        ##Pull video from mobile SD card to pc path
        dumplogger.info(pull_recorded_video)
        os.system(pull_recorded_video)

        ##Remove video from SD card
        dumplogger.info(remove_recorded_video)
        os.system(remove_recorded_video)
        time.sleep(2)

    ##Reset global variable anyway
    GlobalAdapter.CommonVar._ScreenshotFilename_["android"] = ""
    dumplogger.info('Clear GlobalAdapter.CommonVar._ScreenshotFilename_["android"]')
    GlobalAdapter.CommonVar._ProcessID_["android_screenshot"] = ""
    dumplogger.info('Clear GlobalAdapter.CommonVar._ProcessID_["android_screenshot"] list')

    OK(ret, int(arg['result']), 'AndroidEndRecording')

@DecoratorHelper.FuncRecorder
def AndroidGetElementTreeAndCompare(arg):
    '''
    AndroidGetElementTreeAndCompare : Get Android Element Tree And Compare
            Input argu :
                locate - locate of xpath
            Return code : N/A
    '''
    locate = arg["locate"]
    dump_file = _OutputFolderPath_ + dict_systemslash[_platform_] + 'android_elem_tree.xml'

    ##Dump DOM Tree first
    SendADBCommand({"command_type":"DumpDOMTree", "adb_arg": "", "is_return": "0", "result":"1"})

    ##Pull xml file from Android Device
    SendADBCommand({"command_type":"CopyFile", "adb_arg":"/sdcard/window_dump.xml " + dump_file, "is_return": "0", "result":"1"})

    ##Find target locate in DOM tree
    if not XtFunc.ParseElementTreeAndFind({"dom_source":dump_file, "target":locate}):
        dumplogger.info("Element xpath not found in android dump of current page!!!")

def AndroidGetNetworkInfo():
    '''
    AndroidGetNetworkInfo : Get Android Network Information
            Input argu : N/A
            Return code : N/A
    '''

    ##Send adb Command
    output = SendADBCommand({"command_type":"DumpsysConnectivity", "adb_arg":"", "is_return":"1", "result":"1"})

    ##Get cmd result
    dumplogger.info("Android network information = " + output.strip())

def AndroidCheckDeviceCPUUsage(arg):
    '''
    AndroidCheckDeviceCPUUsage : Check android device cpu usage is stable
            Input argu :
                max_cpu_usage - max cpu usage you want to check (float)
            Return code : N/A
    '''
    ##Do not add @DecoratorHelper.FuncRecorder!!!. Because if case fail, will trigger AppiumConnectionChecker and occur maximum recursion depth exceeded
    max_cpu_usage = arg["max_cpu_usage"]
    dumplogger.info("Begin check CPU usage")

    try:
        check_count = 0
        ##Check count < 96 (96*5=480. Upper limit wait 480 second)
        while check_count < 96:
            dumplogger.info("Device cpu usage: %s" % (Config._DeviceCPUUsage_))

            ##Check device cpu rate < max cpu usage
            if float(Config._DeviceCPUUsage_) < float(max_cpu_usage):
                dumplogger.info("Check CPU pass")
                break
            else:
                dumplogger.info("Device cpu usage check count: %s" % (check_count))

            ##Check android device performance info per 5 second
            check_count = check_count + 1
            time.sleep(5)

    except IndexError:
        dumplogger.exception('Performance info response data index is not correct!!!')

    except:
        dumplogger.exception("Encounter Other Exception !!!!")

def AndroidGetDevicePerformanceInfo():
    '''
    AndroidGetDevicePerformanceInfo : Get device performance information (CPU / MEM)
            Input argu : N/A
            Return code :
                Dictionary object - performance info dictionary object => e.g. {"total_used_cpu": "20.0", "total_used_mem": "10.0"}
                None - no any device performance info
    '''
    ##Prepare performance info dictionary object
    performance_info = {
        "device_user_cpu": "0",
        "device_sys_cpu": "0",
        "app_cpu": "0",
        "app_mem": "0"
    }

    try:
        ##Send adb command to get device shopee app performance info => e.g. "adb shell top -d 5 | grep --line-buffered com.shopee.vn"
        command = 'adb -s ' + Config._DeviceID_ + Config._AdbCommands_["GetDeviceCpuMem"] + Config._TestCaseRegion_.lower()
        performance_output = os.popen(command).read()

        ##Filter device performance CPU message by regex
        ##Regex group 1 => Device used user CPU (%)
        ##Regex group 2 => Device used sys CPU (%)
        device_performance_regex_result = re.search(r"\s+(\S+)%user[\S\s]+\s+(\S+)%sys", performance_output)

        ##Save device performance info (user CPU%, sys CPU%)
        if device_performance_regex_result:
            performance_info["device_user_cpu"] = device_performance_regex_result.group(1)
            performance_info["device_sys_cpu"] = device_performance_regex_result.group(2)

        ##Filter shopee app CPU / RAM message by regex
        ##Regex group 1 => Shopee app used CPU (%)
        ##Regex group 2 => Shopee app used MEM (%)
        shopee_app_performance_regex_result = re.search(r"\s+(\S+)\s+(\S+)\s+\S+\s+com\.shopee\.", performance_output)

        ##Save shopee app performance info (CPU%, MEM%)
        if shopee_app_performance_regex_result:
            performance_info["app_cpu"] = shopee_app_performance_regex_result.group(1)
            performance_info["app_mem"] = shopee_app_performance_regex_result.group(2)

        return performance_info

    except WindowsError:
        dumplogger.exception('Occur windows execute command Exception !!!!')
        return None

    except:
        dumplogger.exception('Encounter Other Error !!!!')
        return None

def AndroidCheckAndReconnectWifi():
    '''
    AndroidCheckAndReconnectWifi : Check android device wifi connection, try to connect when without connection
            Input argu : N/A
            Return code : N/A
    '''

    ##Check to use proxy or not
    if Config._EnableProxy_:
        ##Send adb Command
        dump_wifi_connection = SendADBCommand({"command_type":"DumpsysConnectivity", "adb_arg":"| find \"WIFI[], state: CONNECTED/CONNECTED\"", "is_return":"1", "result":"1"})

        ##Get cmd result
        dumplogger.info("Android network connection = " + dump_wifi_connection.strip())

        ##If not connect to Wifi
        if not dump_wifi_connection:

            ##Connect to wifi
            AndroidSetWifi({"action_type": "enable", "result": "1"})

def AndroidCheckAndReconnectLTE():
    '''
    AndroidCheckAndReconnectLTE : Check android device LTE connection, try to connect when without connection
            Input argu : N/A
            Return code : N/A
    '''

    ##Check to use proxy or not
    if Config._EnableProxy_:
        ##Send adb Command
        dump_wifi_connection = SendADBCommand({"command_type":"DumpsysConnectivity", "adb_arg":"| find \"MOBILE[LTE], state: CONNECTED/CONNECTED\"", "is_return":"1", "result":"1"})

        ##Get cmd result
        dumplogger.info("Android network connection = " + dump_wifi_connection.strip())

        ##If not connect to Wifi
        if not dump_wifi_connection:

            ##Connect to wifi
            AndroidSetLTE({"action_type": "enable", "result": "1"})

@DecoratorHelper.FuncRecorder
def AndroidCheckAppLaunch(arg):
    '''
    AndroidCheckAppLaunch : Check app launch
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Check the current staying screen
    dumpsys_cur_activity = SendADBCommand({"command_type": "Dumpsys", "adb_arg": "|findStr mFocusedApp", "is_return": "1", "result": "1"})
    dumplogger.info('dumpsys_cur_activity -> ' + dumpsys_cur_activity)

    ##Check current screen is staying in shopee app or not
    if (Config._DesiredCaps_['appPackage'] + '/' + Config._DesiredCaps_['appActivity']) in dumpsys_cur_activity:
        dumplogger.info('App exists in current screen!')
    else:
        AndroidOpenShopeeAPP({"result": "1"})
        dumplogger.debug('App needs to be restarted due to crash or other reason!')

    OK(ret, int(arg['result']), 'AndroidCheckAppLaunch')

def AndroidCloseRNLoggerPopUp():
    '''
    AndroidCloseRNLoggerPopUp : Close RNLogger pop up
            Input argu : N/A
            Return code : N/A
    '''

    ##Go to feature toggle page by adb
    print "Enter feature toggle page!!!\n"
    adb_arg = Config._DesiredCaps_['appPackage'] + "/com.shopee.app.ui.setting.ForbiddenZone.ConfigFeatureToggleActivity_"
    SendADBCommand({"command_type":"LaunchAPP", "adb_arg": adb_arg, "is_return": "0", "result": "1"})
    time.sleep(3)

    ##Input rn_error_popup to search feature toggle
    xpath = Util.GetXpath({"locate": "search_feature_toggle"})
    AndroidInput({"method": "xpath", "locate": xpath, "string": "rn_error_popup", "result": "1"})
    time.sleep(3)

    ##Check rn_error_popup toggle is close.
    xpath = Util.GetXpath({"locate": "rn_error_popup_toggle_close"})
    if AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
        dumplogger.info('No need to close rn_error_popup forbidden toggle. Back to previous page.')
    else:
        ##If rn_error_popup toggle is open, then click toggle to close.
        xpath = Util.GetXpath({"locate": "rn_error_popup_toggle_open"})
        AndroidClick({"method": "xpath", "locate": xpath, "message": "Press rn_error_popup toggle", "result": "1"})
        xpath = Util.GetXpath({"locate": "rn_error_popup_toggle_close"})
        AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"})
        dumplogger.info('Close rn_error_popup forbidden toggle. Back to previous page.')

    ##Return to previous page
    AndroidKeyboardAction({"actiontype": "back", "result": "1"})
    AndroidKeyboardAction({"actiontype": "back", "result": "1"})

def AndroidGetAndStorgeCurrentImage(arg):
    '''
    AndroidGetAndStorgeCurrentImage : Storge current Android emulator devices screenshot and execute function name
            Input argu :
                img - Which variable need storge to (ex: Start_img or Retry_Start_img)
                function_name - exectued function name
                function_status - normal function / retry function
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    img = arg["img"]
    function_name = arg["function_name"]
    function_status = arg["function_status"]
    function_filter = ("MatchImgByOpenCV", "HorizontalMoveAndCheckElement", "HorizontalMoveElementToPosition")

    ##Only support When "Android" in function name
    if "Android" in function_name or function_name in function_filter and _AndroidDriver_:
        ##Process funciton name
        if len(function_name.split('(')[0].split('.')) > 2:
            retry_line_number = function_name.split(')')[1]
            texts = function_name.split('(')[0].split('.')
            function_name = texts[1] + "." + texts[2] + '#' + retry_line_number
        else:
            function_name = function_name.split('(')[0]
        try:
            img_base64 = _AndroidDriver_.get_screenshot_as_base64()
            image = XtFunc.DecodeImage({"image":img_base64})

            ##Append image and function name to GlobalAdapter variable
            img.append(image)
            function_status.append(function_name)

            ##pop previous 3 steps image and function name when image list length > 3
            if len(img) > 3:
                img.pop(0)
                function_status.pop(0)

            dumplogger.info("Start image : %d", len(GlobalAdapter.CommonVar._Start_Img_))
            dumplogger.info("Function name:" + str(GlobalAdapter.CommonVar._Function_Name_)[1:-1])
            dumplogger.info("Retry Start image : %d", len(GlobalAdapter.CommonVar._Retry_Start_Img_))
            dumplogger.info("Retry Function name:" + str(GlobalAdapter.CommonVar._Retry_Function_Name_)[1:-1])
        except WebDriverException:
            dumplogger.exception("Encounter Webdriver Error!")

        except:
            dumplogger.exception("Encounter unknow exception")

def AndroidCloseCookiesPopUp(arg):
    '''
    AndroidCloseCookiesPopUp : Close cookies pop up
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Close cookies message
    xpath = Util.GetXpath({"locate": "cookies_btn"})
    if AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
        AndroidClick({"method": "xpath", "locate": xpath, "message": "Close cookies message", "result": "1"})
        dumplogger.info('Close cookie popup window.')

    OK(ret, int(arg['result']), 'AndroidCloseCookiesPopUp')

def AndroidClosePDPAPopup(arg):
    '''
    AndroidClosePDPAPopup : Close PDPA consent popup
            Input argu : N/A
            Return code :
                1 - success
                0 - fail
                -1 - error
    '''
    ret = 1

    ##Close PDPA consent popup
    xpath = Util.GetXpath({"locate": "accept_pdpa_consent_btn"})
    if AndroidCheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
        AndroidClick({"method": "xpath", "locate": xpath, "message": "Close PDPA consent popup", "result": "1"})
        dumplogger.info('Close PDPA consent popup window.')

    OK(ret, int(arg['result']), 'AndroidClosePDPAPopup')
