#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminCampaignUploadPageMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import common library
import Util
import XtFunc
import FrameWorkBase
import DecoratorHelper
from Config import dumplogger

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminCampaignUploadPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SearchCampaignLabelByName(arg):
        '''
        SearchCampaignLabelByName : Search campaign label by name
                Input argu :
                    name - name of campaign label
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        name = arg["name"]

        ##Click search icon
        xpath = Util.GetXpath({"locate": "search_icon"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click on search icon", "result": "1"})
        time.sleep(3)

        ##Input search name
        xpath = Util.GetXpath({"locate": "name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": name, "result": "1"})
        time.sleep(3)

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.SearchCampaignLabelByName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ModifyCampaignLabel(arg):
        '''
        ModifyCampaignLabel : Modify campaign label content
                Input argu :
                    label_name - name of campaign label
                    adjust_start_time - adjust start time
                    adjust_end_time - adjust end time
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        label_name = arg["label_name"]
        adjust_start_time = arg["adjust_start_time"]
        adjust_end_time = arg["adjust_end_time"]

        ##Input label name
        if label_name:
            xpath = Util.GetXpath({"locate": "label_name_input"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": label_name, "result": "1"})
            time.sleep(3)

        ##Input Start Time
        if adjust_start_time:
            ##Get current date time and add minutes on it
            time_stamp_start_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_start_time), 0)
            time_stamp_start_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_start_time), 0)

            ##Modify start date
            xpath = Util.GetXpath({"locate": "start_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start date dropdown", "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "start_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

            ##Modify start time
            xpath = Util.GetXpath({"locate": "start_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click start time dropdown", "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "start_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_start_time, "result": "1"})
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        else:
            dumplogger.info("Do not setting start time.")

        ##Input End Time
        if adjust_end_time:
            ##Get current date time and add minutes on it
            time_stamp_end_date = XtFunc.GetCurrentDateTime("%Y-%m-%d", 0, int(adjust_end_time), 0)
            time_stamp_end_time = XtFunc.GetCurrentDateTime("%H:%M:%S", 0, int(adjust_end_time), 0)

            ##Modify end date
            xpath = Util.GetXpath({"locate": "end_date_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end date dropdown", "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "end_date_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_date, "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})

            ##Modify end time
            xpath = Util.GetXpath({"locate": "end_time_dropdown"})
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click end time dropdown", "result": "1"})
            time.sleep(2)
            xpath = Util.GetXpath({"locate": "end_time_input"})
            BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": time_stamp_end_time, "result": "1"})
            xpath = Util.GetXpath({"locate": "section_label"})
            BaseUICore.MoveToElementCoordinate({"locate":xpath, "is_click":"1", "result": "1"})
        else:
            dumplogger.info("Do not setting end time.")

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.ModifyCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ChooseLabelBannerType(arg):
        '''
        ChooseLabelBannerType : Choose campaign label banner type
                Input argu :
                    banner_type - banner type (home_carousel / category / home_popup / mall_carousel / skinny / homepage_mall / digital_product_carousel / mobile_browser / mall_popup / microsite + "(size)")
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        banner_type = arg["banner_type"]

        ##Click banner type dropdown
        xpath = Util.GetXpath({"locate": "banner_type_dropdown"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click banner type dropdown", "result": "1"})
        time.sleep(3)

        ##Choose banner type
        xpath = Util.GetXpath({"locate": "banner_type_content"})
        xpath = xpath.replace("type_to_be_replaced", banner_type)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Choose banner type -> " + banner_type, "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.ChooseLabelBannerType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SwitchCampaignLabelSection(arg):
        '''
        SwitchCampaignLabelSection : Switch campaign label section
                Input argu :
                    section - active / disabled
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section = arg["section"]

        ##Switch section to active / disabled
        xpath = Util.GetXpath({"locate": section})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Switch to section -> " + section, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.SwitchCampaignLabelSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAllCampaignLabel(arg):
        '''
        SelectAllCampaignLabel : Select all campaign label
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Select all campaign label
        xpath = Util.GetXpath({"locate": "select_all_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select all campaign label", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.SelectAllCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeselectAllCampaignLabel(arg):
        '''
        DeselectAllCampaignLabel : Deselect all campaign label
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Deselect all campaign label
        xpath = Util.GetXpath({"locate": "deselect_all_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Deselect all campaign label", "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.DeselectAllCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadCampaignLabelImage(arg):
        '''
        UploadCampaignLabelImage : Upload campaign label image
                Input argu :
                    mode - pc / mobile
                    file_name - file name
                    file_type - file type
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        mode = arg["mode"]
        file_name = arg["file_name"]
        file_type = arg["file_type"]

        ##Upload image
        xpath = Util.GetXpath({"locate": mode})
        BaseUICore.UploadFileWithCSS({"locate":xpath, "action": "image", "file_name": file_name, "file_type": file_type, "result": "1"})

        OK(ret, int(arg['result']), 'AdminCampaignUploadPage.UploadCampaignLabelImage')


class AdminCampaignUploadPageButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEditCampaignLabel(arg):
        '''
        ClickEditCampaignLabel : Click edit btn of campaign label
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click edit
        xpath = Util.GetXpath({"locate": "edit_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUploadPageButton.ClickEditCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisableCampaignLabel(arg):
        '''
        ClickDisableCampaignLabel : Click disable btn of campaign label
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click disabled
        xpath = Util.GetXpath({"locate": "disable_btn"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click disable btn", "result": "1"})
            time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUploadPageButton.ClickDisableCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSaveCampaignLabel(arg):
        '''
        ClickSaveCampaignLabel : Click save btn after editting campaign label
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click save btn after editting campaign label
        xpath = Util.GetXpath({"locate": "save_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save campaign label", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUploadPageButton.ClickSaveCampaignLabel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUploadCampaignLabel(arg):
        '''
        ClickUploadCampaignLabel : Click upload camapaign label btn
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click upload campaign label btn
        xpath = Util.GetXpath({"locate": "upload_btn"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click upload btn", "result": "1"})
        time.sleep(3)

        OK(ret, int(arg['result']), 'AdminCampaignUploadPageButton.ClickUploadCampaignLabel')
