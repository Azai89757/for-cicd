#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 DBCommonMethod.py: The def of this file called by other function.
'''
##Import system library
from sqlite3 import OperationalError
import time
import re
import os
import json

##Import common library
from Config import dumplogger
import DecoratorHelper
import GlobalAdapter
import FrameWorkBase
import DBHelper
import Config
import XtFunc
import Parser
import Util

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


@DecoratorHelper.FuncRecorder
def InitialSQL(arg):
    ''' InitialSQL : Initial for SQL Command
            Input argu :
                db_name - db name to initial
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1
    db_name = arg["db_name"]

    ##Decide whether db location is shopee / qa db
    if db_name == "qa":
        env = db_name + "_info"
    elif db_name == "dev_shopee_order_accounting_settlement_db":
        ##For STS Refactor need to use another DB account
        env = "test_order_info"
    else:
        env = Config._EnvType_.lower() + "_info"

    ##Parse data from DBConfig.ini
    db_config = Util.IniParser(Config._DBConfigINI_)

    ##Error Handle
    if Util.IniParser(Config._DBConfigINI_):

        ##Store data to config variables
        Config._DBConfig_["host"] = db_config[db_name]["host"]
        Config._DBConfig_["port"] = db_config[db_name]["port"]
        Config._DBConfig_["user"] = db_config[env]["user"]
        Config._DBConfig_["pwd"] = db_config[env]["pwd"]

        ##Default charset is utf8, if you need different charset, please specify it in DBConfig.ini
        if "charset" in db_config[db_name]:
            Config._DBConfig_["charset"] = db_config[db_name]["charset"]
        else:
            Config._DBConfig_["charset"] = "utf8"

        dumplogger.info("DB config: %s" % (Config._DBConfig_))

    ##Error Handle if Parser DBConfig return False
    else:
        dumplogger.error("DBConfig.ini content error!")
        ret = 0

    OK(ret, int(arg['result']), 'InitialSQL -> ' + db_name)

@DecoratorHelper.FuncRecorder
def GetAndSetSQLData(arg):
    ''' GetAndSetSQLData : Get and Set SQL data for json and sql file
            Input argu :
                file_name - file name for different db data files
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1
    file_name = arg["file_name"]
    slash = Config.dict_systemslash[Config._platform_]
    regex = r"(.+)(limit)(\s+)(\d+)"

    ##admin file is saved at common folder
    if "admin_cookie" in file_name or file_name == "rundeck_cookie" or file_name == "cmt_cookie":
        db_file = Config._DBCaseDataFilePath_ + slash + "common" + slash + file_name
    elif file_name == "spm_info":
        db_file = Config._DBCaseDataFilePath_ + slash + "common" + slash + "spm" + slash + file_name + "_" + Config._TestCaseRegion_.lower()
    elif file_name == "flash_sale_info":
        db_file = Config._DBCaseDataFilePath_ + slash + "common" + slash + "flash_sale" + slash + file_name + "_" + Config._TestCaseRegion_.lower()
    else:
        ##db_file should be at \data\dbdata\tcjson\$country\$caseid\$caseid-$file_name
        db_file = Config._DBCaseDataFilePath_ + slash + "tcjson" + slash + Config._TestCaseRegion_ + slash + Config._TestCaseFeature_ + slash + Parser._CASE_['id'] + "-" + file_name

    ##Get json file to load data
    if os.path.exists(db_file + ".json"):
        XtFunc.GetJSONContentFromFile({"jsonfile":db_file, "result": "1"})
    else:
        dumplogger.info("Not such file exist: %s.json, please double check it if you need." % (db_file))

    ##Open sql file first and check if cmd is valid
    if os.path.exists(db_file + ".sql"):
        with open(db_file + ".sql", "rb") as sql:
            sql_content = sql.readlines()
            ##To avoid multiple sql command in one .sql file, there is only one line can be wrote in .sql file
            if len(sql_content) == 1:
                GlobalAdapter.DBVar._SqlCmd_ = sql_content[0]
                dumplogger.info("SQL CMD : %s" % (GlobalAdapter.DBVar._SqlCmd_))

                ##For SELECT, filter if limit in sql command, or if limit <= 10
                match = re.search(regex, sql_content[0], re.MULTILINE | re.IGNORECASE)
                if match:
                    if int(match.group(4)) > 10:
                        ##limit > 10, fail
                        print "The Limit of SQL command:%s is > 10, please make it less !!!!!!!" % (sql_content[0])
                        dumplogger.error("The Limit of SQL command: %s is > 10, please make it less !!!!!!!" % (sql_content[0]))
                        ret = 0
                    else:
                        ##limit <= 10, pass
                        dumplogger.info("The Limit of SQL command: %s is <= 10." % (sql_content[0]))
                elif "update" in sql_content[0].lower():
                    ##Update command will skip the check
                    dumplogger.info("SQL command: %s is Update method, no need to check limit." % (sql_content[0]))
                elif "insert" in sql_content[0].lower():
                    ##Insert command will skip the check
                    dumplogger.info("SQL command: %s is Insert method, no need to check limit." % (sql_content[0]))
                else:
                    ##no limit in sql, fail
                    print "There is no Limit in SQL command:%s, please add it in your sql file!!!" % (sql_content[0])
                    dumplogger.error("There is no Limit in SQL command:%s, please add it in your sql file!!!" % (sql_content[0]))
                    ret = 0
            else:
                print "Only one line can put inside your .sql file, please check!!!"
                dumplogger.error("Only one line can put inside your .sql file, please check!!!")
                ret = 0
    else:
        dumplogger.info("Not such file exist: %s.sql, please double check it again!!!" % (db_file))
        ret = 0

    OK(ret, int(arg['result']), 'GetAndSetSQLData -> ' + file_name)

@DecoratorHelper.FuncRecorder
def DeInitialSQL(arg):
    ''' DeInitialSQL : De-initial for SQL Command
            Input argu :
                N/A
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1

    ##Restore global variables
    Config._DBConfig_ = {}
    GlobalAdapter.CommonVar._JSONRawDataFromFile_ = ""
    GlobalAdapter.DBVar._SqlCmd_ = ""
    GlobalAdapter.DBVar._DBResult_ = ""
    GlobalAdapter.DBVar._DBTableTitle_ = []

    OK(ret, int(arg['result']), 'DeInitialSQL')

@DecoratorHelper.FuncRecorder
def SendSQLCommandToDB(arg):
    ''' SendSQLCommandToDB : Send sql command to shopee DB
            Input argu :
                method - select/update
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1
    method = arg["method"]
    message = ""

    try:
        ##Connect to DB
        db_connection = DBHelper.MySQLController(Config._DBConfig_)

        ##Execute sql by .sql file, for query sql data only
        if method == "select" and method in GlobalAdapter.DBVar._SqlCmd_.lower():
            ##Execute sql file
            GlobalAdapter.DBVar._DBResult_ = db_connection.Execute(GlobalAdapter.DBVar._SqlCmd_)
            message = "SQL Execute Command : %s" % (GlobalAdapter.DBVar._SqlCmd_)
            dumplogger.info(message)
            dumplogger.info("SQL Result : %s" % str(GlobalAdapter.DBVar._DBResult_))

            ##Store title of this table
            for table_name in db_connection.GetTitle():
                GlobalAdapter.DBVar._DBTableTitle_.append(table_name[0])
            dumplogger.info("DB Table Tile : %s" % (GlobalAdapter.DBVar._DBTableTitle_))
            '''
            GlobalAdapter.DBVar._DBTableTitle_ should be ->
            [u'promotionid', u'name', u'url', u'start_time', u'end_time', ... ]
            '''

        ##Execute sql by .sql file, for update sql data only
        elif method == "update" and method in GlobalAdapter.DBVar._SqlCmd_.lower():
            ##Execute sql file
            GlobalAdapter.DBVar._DBResult_ = db_connection.Update(GlobalAdapter.DBVar._SqlCmd_)
            message = "SQL Update Command : %s" % (GlobalAdapter.DBVar._SqlCmd_)
            dumplogger.info(message)
            dumplogger.info("SQL Result : %s" % str(GlobalAdapter.DBVar._DBResult_))

        ##Execute sql by .sql file, for insert sql data only
        elif method == "insert" and method in GlobalAdapter.DBVar._SqlCmd_.lower():
            ##Execute sql file
            GlobalAdapter.DBVar._DBResult_ = db_connection.Insert(GlobalAdapter.DBVar._SqlCmd_, None)
            message = "SQL Insert Command : %s" % (GlobalAdapter.DBVar._SqlCmd_)
            dumplogger.info(message)
            dumplogger.info("SQL Result : %s" % str(GlobalAdapter.DBVar._DBResult_))

        else:
            message = "Please check if sql_method is select/update/insert and matches your .sql file !!!"
            dumplogger.error("Please check if sql_method is select/update/insert and matches your .sql file !!!")
            ret = 0

    except OperationalError:
        dumplogger.exception("Connect to Service Fee SQL server failed, please check the root cause with manual team.")
        ret = -1

    except:
        dumplogger.exception("Encounter unexpected error, please check the log to analyze root cause.")
        ret = -1

    OK(ret, int(arg['result']), 'SendSQLCommandToDB -> ' + message)

def TransferSQLResult():
    ''' TransformSQLResult : Transform SQL result from tuple to dict
                Input argu :
                    N/A
                Return code :
                    rlt_dict - sql result as dict format
                    False - unexpected situation in data type / data structure: fail
    '''
    rlt_dict = {}

    '''
    GlobalAdapter.DBVar._DBResult_ should be ->
    ((123456, u'voucher1', 'http://google.com', 20200904-101310, 20200904-10420),
    (654321, u'voucher2', 'http://google1.com', 20200904-101311, 20200904-10421))
    '''
    try:
        ##Get every record of sql result
        for rlt_count in range(len(GlobalAdapter.DBVar._DBResult_)):
            rlt_dict[rlt_count+1] = {}
            ##To assemble sql result with the title of db table
            for rlt_index in range(len(GlobalAdapter.DBVar._DBResult_[rlt_count])):
                rlt_dict[rlt_count+1][GlobalAdapter.DBVar._DBTableTitle_[rlt_index]] = GlobalAdapter.DBVar._DBResult_[rlt_count][rlt_index]

        '''
        rlt_dict should be ->
            "1":{
                "promotionid": "123456",
                "name": "voucher1",
                "url": "http://google.com",
                "start_time": "20200904-101310",
                "end_time": "20200904-10420"
            },
            "2":{
                "promotionid": "654321",
                "name": "voucher2",
                "url": "http://google1.com",
                "start_time": "20200904-101311",
                "end_time": "20200904-10421"
            }
        '''
        dumplogger.info("Execute SQL result after transform to dictionary: %s" % (rlt_dict))
        return rlt_dict

    ##Hanle error
    except IndexError:
        dumplogger.exception("Encounter IndexError !!!")
        return False
    except KeyError:
        dumplogger.exception("Encounter KeyError !!!")
        return False
    except:
        dumplogger.exception("Encounter Other Error !!!")
        return False

@DecoratorHelper.FuncRecorder
def GetAndCheckSQLResult(arg):
    ''' GetAndCheckSQLResult : Check and verify sql result
                Input argu :
                    N/A
                Return code :
                    1 - success
                    0 - sql data not match : fail
                   -1 - unexpected situation in data type / data structure: fail
    '''
    ret = 1

    try:
        ##Transfer SQL result to dict first
        if type(GlobalAdapter.DBVar._DBResult_) is not dict:
            GlobalAdapter.DBVar._DBResult_ = TransferSQLResult()

        ##Compare result with data defined in json file
        dumplogger.info(GlobalAdapter.DBVar._DBResult_)
        if GlobalAdapter.DBVar._DBResult_:
            for expected_seq, expected_data in GlobalAdapter.CommonVar._JSONRawDataFromFile_.items():
                ##Check if data sequence number in result first
                if int(expected_seq) in GlobalAdapter.DBVar._DBResult_.keys():
                    for column_title, db_record in expected_data.items():
                        if column_title in GlobalAdapter.DBVar._DBResult_[int(expected_seq)]:
                            ##Compare sql result and expected result for every column
                            if str(db_record) == str(GlobalAdapter.DBVar._DBResult_[int(expected_seq)][column_title]):
                                dumplogger.info("record: %s of %s match with sql result: %s" % (str(db_record), column_title, str(GlobalAdapter.DBVar._DBResult_[int(expected_seq)][column_title])))
                            else:
                                dumplogger.error("record: %s of %s NOT match with sql result: %s" % (str(db_record), column_title, str(GlobalAdapter.DBVar._DBResult_[int(expected_seq)][column_title])))
                                ret = 0
                        else:
                            dumplogger.error("Column tile: %s not found in sql result!!!!")
                            ret = 0
                else:
                    dumplogger.error("Please check your data sequence starts from 1 and data is correct...")
                    ret = 0
        else:
            dumplogger.error("Unexpected GlobalAdapter.DBVar._DBResult_ ... please check the debug log !!!")
            ret = 0

    ##Hanle error
    except IndexError:
        dumplogger.exception("Encounter IndexError !!!")
        ret = -1
    except KeyError:
        dumplogger.exception("Encounter KeyError !!!")
        ret = -1
    except:
        dumplogger.exception("Encounter Other Error !!!")
        ret = -1

    OK(ret, int(arg['result']), 'GetAndCheckSQLResult')

@DecoratorHelper.FuncRecorder
def StoreDataFromSQLResult(arg):
    ''' StoreDataFromSQLResult : Store data from sql result of specific value
                Input argu :
                    column - value of data need to be stored into global
                Return code :
                    1 - success
                    0 - sql data not match : fail
                   -1 - unexpected situation in data type / data structure: fail
    '''
    column = arg["column"]
    ret = 1

    try:
        ##Transfer SQL result to dict first
        if type(GlobalAdapter.DBVar._DBResult_) is not dict:
            GlobalAdapter.DBVar._DBResult_ = TransferSQLResult()

        ##Store data to global, will always get the first result
        dumplogger.info(GlobalAdapter.DBVar._DBResult_)
        if GlobalAdapter.DBVar._DBResult_:
            if column in GlobalAdapter.DBVar._DBResult_[1]:
                ##GlobalAdapter.CommonVar._DynamicCaseData_ should be a str/int
                GlobalAdapter.CommonVar._DynamicCaseData_[column] = GlobalAdapter.DBVar._DBResult_[1][column]
            else:
                dumplogger.error("Second Layer Key: %s not found in %s!!!" % (column, GlobalAdapter.DBVar._DBResult_[1]))
                ret = 0
        else:
            dumplogger.error("Unexpected GlobalAdapter.DBVar._DBResult_ ... please check the debug log !!!")
            ret = 0

    ##Hanle error
    except IndexError:
        dumplogger.exception("Encounter IndexError !!!")
        ret = -1
    except KeyError:
        dumplogger.exception("Encounter KeyError !!!")
        ret = -1
    except:
        dumplogger.exception("Encounter Other Error !!!")
        ret = -1

    OK(ret, int(arg['result']), 'StoreDataFromSQLResult -> ' + str(GlobalAdapter.CommonVar._DynamicCaseData_))

@DecoratorHelper.FuncRecorder
def AssignDataToSQLCmd(arg):
    ''' AssignDataToSQLCmd : Assign data to sql command
            Input argu :
                source_column - source data of where clause which its value is dynamic
                target_column - target data of where clause which its value is dynamic
                value_type - number / string / cur_unix_time / stored_time
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1
    source_column = arg["source_column"]
    target_column = arg["target_column"]
    value_type = arg["value_type"]
    regex = r"(" + target_column + r")(\W+)(data_to_be_replaced)"

    try:
        ##wrap value with value_type first
        if value_type == "string":
            GlobalAdapter.CommonVar._DynamicCaseData_[target_column] = "'" + str(GlobalAdapter.CommonVar._DynamicCaseData_[source_column]) + "'"
        elif value_type == "number":
            GlobalAdapter.CommonVar._DynamicCaseData_[target_column] = str(GlobalAdapter.CommonVar._DynamicCaseData_[source_column])
        elif value_type == "cur_unix_time":
            GlobalAdapter.CommonVar._DynamicCaseData_[target_column] = str(int(time.time()))
        elif value_type == "stored_time":
            GlobalAdapter.CommonVar._DynamicCaseData_[target_column] = str(GlobalAdapter.TrackerVar._CurUnixTime_)

        ##Check if pattern could be found in SQL command
        match = re.search(regex, GlobalAdapter.DBVar._SqlCmd_, re.MULTILINE | re.IGNORECASE)
        if match:
            ##Replace dynamic value
            GlobalAdapter.DBVar._SqlCmd_ = re.sub(regex, target_column + "=" + GlobalAdapter.CommonVar._DynamicCaseData_[target_column], GlobalAdapter.DBVar._SqlCmd_, re.MULTILINE | re.IGNORECASE)
            dumplogger.info("SQL command: %s" % (GlobalAdapter.DBVar._SqlCmd_))
        ##If encounter error during replace process, case will fail.
        else:
            ##Replace dynamic value
            ##If its insert command do not have column name in front of "data_to_be_replaced"
            if "insert" in GlobalAdapter.DBVar._SqlCmd_.lower():
                GlobalAdapter.DBVar._SqlCmd_ = re.sub(r"data_to_be_replaced", GlobalAdapter.CommonVar._DynamicCaseData_[target_column], GlobalAdapter.DBVar._SqlCmd_, re.MULTILINE | re.IGNORECASE)
            else:
                dumplogger.error("SQL command not valid... please go check your .sql command: %s" % (GlobalAdapter.DBVar._SqlCmd_))
                ret = 0

    ##Hanle error
    except KeyError:
        dumplogger.exception("Encounter KeyError !!!")
        ret = -1
    except:
        dumplogger.exception("Encounter Other Error !!!")
        ret = -1

    OK(ret, int(arg['result']), 'AssignDataToSQLCmd -> ' + GlobalAdapter.DBVar._SqlCmd_)

@DecoratorHelper.FuncRecorder
def SendSQLCommandProcess(arg):
    ''' SendSQLCommandProcess : Total process for send a SQL command (Automation QA usage only)
            Input argu :
                db_name - name of the db defined in DBConfig.ini
                file_name - sql/json data file name for SQL command
                method - select / update / insert
                verify_result - 1 (verify) / empty (no verify)
                assign_data_list - data to be assigned before sending SQL command EX.[{"column": "id", "value_type": "number"}, ...]
                store_data_list - data to be stored after sending SQL command EX.['acid', 'promotionid', 'time', ...]
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1
    db_name = arg["db_name"]
    file_name = arg["file_name"]
    method = arg["method"]
    verify_result = arg["verify_result"]
    assign_data_list = arg["assign_data_list"]
    store_data_list = arg["store_data_list"]

    ##Initial SQL
    InitialSQL({"db_name":db_name, "result": "1"})

    ##Get and set sql related data (.sql and .json): \data\dbdata\tcjson\TW\TWSpexSearch-007001\TWSpexSearch-007001-voucher1
    GetAndSetSQLData({"file_name":file_name, "result": "1"})

    if assign_data_list:
        for each_assign_data in assign_data_list:
            AssignDataToSQLCmd({"source_column":each_assign_data["column"], "target_column":each_assign_data["column"], "value_type":each_assign_data["value_type"], "result": "1"})
    else:
        dumplogger.info("No need to call AssignDataToSQLCmd !!!")

    ##Send SQL command to DB
    SendSQLCommandToDB({"method":method, "result": "1"})

    ##Call GetandCheckSQLResult / StoreDataFromSQLResult only for select method
    if method == "select":
        ##verify sql result to global if needed
        if verify_result:
            GetAndCheckSQLResult({"result": "1"})
        else:
            dumplogger.info("No need to call GetAndCheckSQLResult !!!")

        ##Store sql result to global if needed
        if store_data_list:
            for each_store_data in store_data_list:
                StoreDataFromSQLResult({"column":each_store_data, "result": "1"})
        else:
            dumplogger.info("No need to call StoreDataFromSQLResult !!!")
    else:
        dumplogger.info("No need to call GetAndCheckSQLResult!")

    ##De-Initial SQL
    DeInitialSQL({"result": "1"})

    OK(ret, int(arg['result']), 'SendSQLCommandProcess -> ' + GlobalAdapter.DBVar._SqlCmd_)

@DecoratorHelper.FuncRecorder
def AssignTableNameToSQLCmd(arg):
    ''' AssignTableNameToSQLCmd : Assign table name to sql command
            Input argu :
                table_prefix - table prefix name
                column - target data of where clause which its value is dynamic
            Return code :
                1 - success
                0 - fail
               -1 - error
    '''
    ret = 1
    table_prefix = arg["table_prefix"]
    column = arg["column"]
    regex = r"table_to_be_replaced"

    try:
        ##Check if pattern could be found in SQL command
        match = re.search(regex, GlobalAdapter.DBVar._SqlCmd_, re.MULTILINE | re.IGNORECASE)
        if match:

            column_value = int(GlobalAdapter.CommonVar._DynamicCaseData_[column])
            partition_id = column_value % 1000

            ##zfill will ensure partition_id in string format have correct numbers count
            table_name = table_prefix + "_00000" + str(partition_id).zfill(3)
            GlobalAdapter.DBVar._SqlCmd_ = re.sub(regex, table_name, GlobalAdapter.DBVar._SqlCmd_, re.MULTILINE | re.IGNORECASE)
            dumplogger.info("SQL command: %s" % (GlobalAdapter.DBVar._SqlCmd_))

        ##If encounter error during replace process, case will fail.
        else:
            dumplogger.error("SQL command not valid... please go check your .sql command: %s" % (GlobalAdapter.DBVar._SqlCmd_))
            ret = 0

    ##Hanle error
    except KeyError:
        dumplogger.exception("Encounter KeyError !!!")
        ret = -1
    except:
        dumplogger.exception("Encounter Other Error !!!")
        ret = -1

    OK(ret, int(arg['result']), 'AssignTableNameToSQLCmd -> ' + GlobalAdapter.DBVar._SqlCmd_)
