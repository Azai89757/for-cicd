import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class CLPCProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCProductCard Precondition =="
        dumplogger.info("== Setup CLPCProductCard Precondition ==")

    def test_CLPCProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCard-01.xml")

    def test_CLPCProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCard-02.xml")

    def test_CLPCProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCard-03.xml")

    def test_CLPCProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCard-04.xml")

    def test_CLPCProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/CL/CLPCProductCard-05.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLPCProductCard Post condition =="
        dumplogger.info("== Setup CLPCProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
