import unittest
from Config import dumplogger
from FrameWorkBase import *


class TEST(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TEST Precondition =="

    def test_TEST001(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("xml/TEST-001.xml")

    def test_TEST002(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/TEST-002.xml")

    def test_TEST003(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/TEST-003.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TEST Post condition =="
