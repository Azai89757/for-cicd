import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCCancellation(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCCancellation Precondition =="
        dumplogger.info("== Setup TWPCCancellation Precondition ==")

    def test_TWPCCancellation01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCancellation-01.xml")

    def test_TWPCCancellation02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCCancellation-02.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCCancellation Post condition =="
        dumplogger.info("== Setup TWPCCancellation Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
