#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 VoucherMethod.py: The def of this file called by XML mainly.
'''

##import python library
import time

##Import framework common library
import Util
import FrameWorkBase
import GlobalAdapter
import DecoratorHelper
from Config import dumplogger

##Import web library
import BaseUICore
import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class VoucherWalletPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ApplyVoucherCode(arg):
        '''
        ApplyVoucherCode : Apply voucher code
                Input argu :
                    code - voucher code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        code = arg['voucher_code']

        input_field = Util.GetXpath({"locate":"input_voucher_field"})
        apply_btn = Util.GetXpath({"locate":"apply_btn"})
        if code:
            ##Input voucher code
            BaseUICore.Input({"method":"xpath", "locate":input_field, "string":code, "result": "1"})

            ##Click apply
            BaseUICore.Click({"method":"xpath", "locate":apply_btn, "message":"Click apply button", "result": "1"})
        else:
            ##Loop to enter all voucher code
            for voucher in GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[:2]:
                ##Input voucher code
                BaseUICore.Input({"method":"xpath", "locate":input_field, "string":voucher["code"], "result": "1"})

                ##Click apply
                BaseUICore.Click({"method":"xpath", "locate":apply_btn, "message":"Click apply button", "result": "1"})

                time.sleep(5)

                BaseUILogic.BrowserRefresh({"message":"Refresh page", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherWalletPage.ApplyVoucherCode')


class VoucherWalletButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUseVoucher(arg):
        '''
        ClickUseVoucher : Click use btn in voucher card
                Input argu :
                    voucher_message - message of voucher to be used
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_message = arg['voucher_message']

        ##Click voucher use btn
        xpath = Util.GetXpath({"locate":"label_use"})
        xpath = xpath.replace("replace_voucher_message", voucher_message)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click use btn", "result": "1"})

        OK(ret, int(arg['result']), "VoucherWalletButton.ClickUseVoucher")


class VoucherPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputPromoCode(arg):
        '''
        InputPromoCode : Goto cart page and input voucher
                Input argu :
                    promocode - voucher code
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        code = arg["promocode"]

        xpath = Util.GetXpath({"locate": "label_platform_voucher_input_placeholder"})
        ##Enter promo code
        if code:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": code, "result": "1"})
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[-1]["code"], "result": "1"})

        ##Click promo code button
        xpath = Util.GetXpath({"locate": "label_voucher_input_apply"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click apply button", "result": "1"})

        time.sleep(5)

        ##Click voucher radio button if voucher is exist
        xpath = Util.GetXpath({"locate": "voucher_radio_button"})
        if BaseUICore.CheckElementExist({"method": "xpath", "locate": xpath, "passok": "0", "result": "1"}) and code:
            xpath = Util.GetXpath({"locate": "voucher_radio_button"})
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click voucher radio button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherPage.InputPromoCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClaimVoucher(arg):
        '''
        ClaimVoucher : Input voucher code and click save to claim voucher
                Input argu :
                    voucher_type - shopee_voucher / seller_voucher / free_shipping_voucher / dp_voucher / offline_payment_voucher
                    voucher_code - voucher code you want to input
                    vouchers_index - if you want to input suffix voucher code, you need to give index of list variable "_VoucherCode_" to get one of suffix voucher codes
                    part_of_voucher_code_length - part of voucher code you want to input
                    extra_voucher_code - extra voucher code you want to extra input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_type = arg["voucher_type"]
        voucher_code = arg['voucher_code']
        vouchers_index = arg['vouchers_index']
        part_of_voucher_code_length = arg['part_of_voucher_code_length']
        extra_voucher_code = arg['extra_voucher_code']

        ##If voucher_code argument is empty, get voucher code from global variable
        if not voucher_code:
            ##Get voucher code from global list variable "_ShopeeVoucherInfo_"
            if voucher_type == "shopee_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._ShopeeVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_SellerVoucherInfo_"
            elif voucher_type == "seller_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._SellerVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_FreeShippingVoucherList_"
            elif voucher_type == "free_shipping_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._FreeShippingVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_DPVoucherList_"
            elif voucher_type == "dp_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._DPVoucherList_[int(vouchers_index)]["code"]

            ##Get voucher code from global list variable "_OfflinePaymentVoucherList_"
            elif voucher_type == "offline_payment_voucher":
                voucher_code = GlobalAdapter.PromotionE2EVar._OfflinePaymentVoucherList_[int(vouchers_index)]["code"]
            else:
                dumplogger.error("Voucher type string is not correct")

        ##Get part of voucher code
        if part_of_voucher_code_length:
            voucher_code = voucher_code[:int(part_of_voucher_code_length)]

        ##Add extra voucher code string in voucher_code
        voucher_code = voucher_code + extra_voucher_code

        ##Log voucher code
        dumplogger.info("Input voucher code => '%s' to claim voucher" % (voucher_code))

        ##Input voucher code
        xpath = Util.GetXpath({"locate":"voucher_code_field"})
        BaseUILogic.KeyboardAction({"actiontype":"delete", "locate":xpath, "result": "1"})
        BaseUICore.Input({"method":"xpath", "locate":xpath, "string":voucher_code, "result": "1"})

        time.sleep(2)

        ##Click save voucher button
        xpath = Util.GetXpath({"locate":"save_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click save voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherPage.ClaimVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputSellerPromoCode(arg):
        '''
        InputSellerPromoCode : Input promo code in seller voucher drawer
                Input argu :
                    promocode - promo code to input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        promocode = arg["promocode"]

        xpath = Util.GetXpath({"locate": "label_shop_voucher_input_placeholder"})
        ##Enter promo code
        if promocode:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": promocode, "result": "1"})
        else:
            BaseUICore.Input({"method": "xpath", "locate": xpath, "string": GlobalAdapter.PromotionE2EVar._SellerVoucherList_[0]["code"], "result": "1"})

        ##Click promo code button
        xpath = Util.GetXpath({"locate": "label_voucher_input_apply"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"click apply button", "result": "1"})

        time.sleep(5)

        OK(ret, int(arg['result']), 'VoucherPage.InputSellerPromoCode')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UnselectPreselectedVoucher(arg):
        '''
        UnselectPreselectedVoucher : Unselect preselect voucher
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        xpath = Util.GetXpath({"locate": "selected_radio"})
        ##Enter promo code
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click radio button", "result": "1"})
        time.sleep(5)

        OK(ret, int(arg['result']), 'VoucherPage.UnselectPreselectedVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def CheckRandomVoucherLabelText(arg):
        '''
        CheckRandomVoucherLabelText : Check random voucher label text in any voucher page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Check random voucher label text in any voucher page
        time.sleep(2)
        xpath = Util.GetXpath({"locate":"random_voucher_label_txt"})
        xpath = xpath.replace("random_text_to_be_replaced", GlobalAdapter.PromotionE2EVar._CustomisedLabel_)
        if BaseUICore.CheckElementExist({"method":"xpath", "locate":xpath, "passok": "0", "result": "1"}):
            dumplogger.info("Voucher label text match!")
        else:
            dumplogger.info("Voucher label text not match!")
            ret = 0

        OK(ret, int(arg['result']), 'VoucherPage.CheckRandomVoucherLabelText')

class VoucherButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickClaim(arg):
        '''
        ClickClaim : Click voucher claim button
                Input argu :
                    page_type - pdp / shop_page / voucher_drawer
                    voucher_name - voucher name to want to claim
                    voucher_discount - if page_type is pdp or voucher_drawer, use this argument to locate element
                    minimum_basket_size - if page_type is pdp or voucher_drawer, use this argument to locate element
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]
        voucher_name = arg["voucher_name"]

        if page_type in ('voucher_drawer'):

            voucher_discount = arg["voucher_discount"]
            minimum_basket_size = arg["minimum_basket_size"]

            ##Click voucher claim button
            xpath = Util.GetXpath({"locate":page_type})
            xpath = xpath.replace("title_1_to_be_replace", voucher_discount)
            xpath = xpath.replace("title_2_to_be_replace", minimum_basket_size)
            BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher claim button", "result": "1"})

        else:

            ##Click voucher claim button
            xpath = Util.GetXpath({"locate":page_type})
            xpath_claim_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
            BaseUICore.Click({"method":"xpath", "locate":xpath_claim_btn, "message":"Click voucher claim button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickClaim')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click voucher cancel button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click voucher cancel button
        xpath = Util.GetXpath({"locate":"cancel_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher cancel button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickCancel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddMore(arg):
        '''
        ClickAddMore : Click add more btn
                Input argu :
                    voucher_message - message of voucher to click add btn
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']
        voucher_message = arg['voucher_message']

        ##Click add more btn
        xpath = Util.GetXpath({"locate":type})
        xpath = xpath.replace("replaced_voucher_message", voucher_message)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click add more btn", "result": "1"})

        OK(ret, int(arg['result']), "VoucherButton.ClickAddMore")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickGoShopping(arg):
        '''
        ClickGoShopping : Click go shopping btn
                Input argu :
                    voucher_message - message of voucher to click go shopping
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg['type']
        voucher_message = arg['voucher_message']

        ##Click go shopping btn
        xpath = Util.GetXpath({"locate":type})
        xpath = xpath.replace("replace_voucher_message", voucher_message)
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click go shopping", "result": "1"})

        OK(ret, int(arg['result']), "VoucherButton.ClickGoShopping")

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOK(arg):
        '''
        ClickOK : Click ok button in voucher selection page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click ok button in voucher selection page
        xpath = Util.GetXpath({"locate": "voucher_label_ok"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click ok button in voucher selection page", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickOK')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTabOnTopPanel(arg):
        '''
        ClickTabOnTopPanel : Click voucher tab button on top panel
                Input argu :
                        type - label_used_voucher / label_valid_voucher / label_invalid_voucher
                Return code :
                        1 - success
                        0 - fail
                        -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Click voucher tab button
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher tab button => " + type, "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickTabOnTopPanel')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickHistory(arg):
        '''
        ClickHistory : Click history button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click history button
        xpath = Util.GetXpath({"locate":"voucher_wallet_link_view_history_voucher"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click history button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickHistory')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucher(arg):
        '''
        ClickVoucher : Click one of vouchers in voucher seletion page
                Input argu :
                    voucher_name - voucher name you want to click
                    button_type - display_text / radio_button
                    voucher_discount - voucher discount
                    minimum_basket_size - voucher minimum basket size
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']
        button_type = arg['button_type']
        voucher_discount = arg['voucher_discount']
        minimum_basket_size = arg['minimum_basket_size']

        ##Get voucher xpath
        xpath = Util.GetXpath({"locate":button_type})
        xpath_voucher_radio_btn = xpath.replace("title_1_to_be_replace", voucher_discount)
        xpath_voucher_radio_btn = xpath_voucher_radio_btn.replace("title_2_to_be_replace", minimum_basket_size)
        xpath_voucher_radio_btn = xpath_voucher_radio_btn.replace("voucher_name_to_be_replace", voucher_name)

        ##Click voucher in voucher seletion page
        BaseUICore.Click({"method":"javascript", "locate":xpath_voucher_radio_btn, "message":"Click one of vouchers in voucher seletion page", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickVoucher')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherIcon(arg):
        '''
        ClickVoucherIcon : Click voucher icon in voucher card
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click one of vouchers in voucher seletion page
        xpath = Util.GetXpath({"locate":"voucher_icon"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher icon in voucher card", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickVoucherIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickLink(arg):
        '''
        ClickLink : Click voucher link button
                Input argu :
                    link_name - voucher link name button you want to click it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        link_name = arg['link_name']

        ##Click voucher link button
        xpath = Util.GetXpath({"locate":"link_name_btn"})
        xpath_link_name_btn = xpath.replace("link_name_to_be_replace", link_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_link_name_btn, "message":"Click voucher link button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickLink')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowMore(arg):
        '''
        ClickShowMore : Click show more button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show more button
        xpath = Util.GetXpath({"locate":"voucher_wallet_checkout_label_show_more_fsv"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click show more button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickShowMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickShowLess(arg):
        '''
        ClickShowLess : Click show less button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click show less button
        xpath = Util.GetXpath({"locate":"voucher_wallet_checkout_label_show_less_fsv"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click show less button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickShowLess')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTermCondition(arg):
        '''
        ClickTermCondition : Click vouhcer terms and conditions button
                Input argu :
                    voucher_name - voucher name you want to click it terms and conditions button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click vouhcer terms and conditions button
        xpath = Util.GetXpath({"locate":"label_voucher_tnc"})
        xpath_voucher_tnc = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_voucher_tnc, "message":"Click vouhcer terms and conditions button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickTermCondition')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickUse(arg):
        '''
        ClickUse : Click use voucher button
                Input argu :
                    voucher_name - voucher name you want to use it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]

        ##Click use voucher button
        xpath = Util.GetXpath({"locate":"label_use"})
        xpath_use_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_use_btn, "message":"Click use voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickUse')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickApply(arg):
        '''
        ClickApply : Click apply voucher button
                Input argu :
                    voucher_name - voucher name you want to apply it
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg["voucher_name"]

        ##Click use voucher button
        xpath = Util.GetXpath({"locate":"apply_btn"})
        xpath_apply_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_apply_btn, "message":"Click apply voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickApply')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRemove(arg):
        '''
        ClickRemove : Click remove voucher button
                Input argu :
                    voucher_name - voucher name you want to remove
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        voucher_name = arg['voucher_name']

        ##Click remove voucher button
        xpath = Util.GetXpath({"locate":"remove_btn"})
        xpath_remove_btn = xpath.replace("voucher_name_to_be_replace", voucher_name)
        BaseUICore.Click({"method":"xpath", "locate":xpath_remove_btn, "message":"Click remove voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickRemove')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherInputField(arg):
        '''
        ClickVoucherInputField : Click voucher input field in voucher seletion page
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click voucher input field
        xpath = Util.GetXpath({"locate":"voucher_input_field"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher input field", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickVoucherInputField')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickVoucherCarouselArrow(arg):
        '''
        ClickVoucherCarouselArrow : Click voucher carousel arrow in shopee mart
                Input argu :
                    type - left/right
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        type = arg['type']

        ##Click voucher carousel arrow
        xpath = Util.GetXpath({"locate":type})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click voucher carousel arrow", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickVoucherCarouselArrow')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMyVoucherPageTab(arg):
        '''
        ClickMyVoucherPageTab : Switch MyVoucherPage Tab
                Input argu :
                    tab_name - voucher tab you want to switch
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_name = arg['tab_name']

        ##Click MyVoucherPageTab button
        xpath = Util.GetXpath({"locate":tab_name})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click my voucher page tab button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickMyVoucherPageTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTCPageUse(arg):
        '''
        ClickTCPageUse : Click T&C use voucher button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click use voucher button
        xpath = Util.GetXpath({"locate":"use_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click use T&C page voucher button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickTCPageUse')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSeeMore(arg):
        '''
        ClickSeeMore : Click see more button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click partner voucher see more button
        xpath = Util.GetXpath({"locate":"see_more_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click see more button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickSeeMore')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCopy(arg):
        '''
        ClickCopy : Click partner voucher copy button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click partner voucher copy button
        xpath = Util.GetXpath({"locate":"copy_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click copy button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickCopy')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickMarkAsUsed(arg):
        '''
        ClickMarkAsUsed : Click partner voucher mark as used button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click partner voucher mark as used button
        xpath = Util.GetXpath({"locate":"mark_btn"})
        BaseUICore.Click({"method":"xpath", "locate":xpath, "message":"Click mark as used button", "result": "1"})

        OK(ret, int(arg['result']), 'VoucherButton.ClickMarkAsUsed')
