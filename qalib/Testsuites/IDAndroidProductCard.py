import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Android.AndroidBaseUICore import *
from PageFactory.Web.BaseUICore import *


class IDAndroidProductCard(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAndroidProductCard Precondition =="
        dumplogger.info("== Setup IDAndroidProductCard Precondition ==")

        packagename = "com.shopee." + Config._TestCaseRegion_.lower() + ".int"
        platform = Config._TestCasePlatform_.lower()

        ##This feature uses UiAutomator2 driver
        Config._DesiredCaps_['automationName'] = "UiAutomator2"

        ##Get device id for the first time only
        if not Config._DeviceID_:
            Config._DeviceID_ = GetMobileDeviceId("android")

        ##Get device info by device id
        if Config._DeviceID_:
            device_info = GetAndroidDeviceInfo(Config._DeviceID_)

            ##Start Appium
            StartAppium({})

            ##Call function from AndroidBaseUICore
            print "Device connection check: " + CheckMobileDeviceConnected(platform, Config._DeviceID_)
            print "App installed check: " + CheckMobileAppInstall(platform, Config._DeviceID_, packagename)
            print "Get device version: " + device_info

            ##Call function from AndroidBaseUICore
            AndroidInitialDriver({"env":"staging", "country":"id"})

        else:
            dumplogger.error("Please check your Android device id !!!!")
            print "Please check your Android device id !!!!"

            ##Directly Leave Setup
            self.skipTest("")

    def test_IDAndroidProductCard01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-01.xml")

    def test_IDAndroidProductCard02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-02.xml")

    def test_IDAndroidProductCard03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-03.xml")

    def test_IDAndroidProductCard04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-04.xml")

    def test_IDAndroidProductCard05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-05.xml")

    def test_IDAndroidProductCard06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-06.xml")

    def test_IDAndroidProductCard07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-07.xml")

    def test_IDAndroidProductCard08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-08.xml")

    def test_IDAndroidProductCard09(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDAndroidProductCard-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDAndroidProductCard Post condition =="
        dumplogger.info("== Setup IDAndroidProductCard Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill Android driver
        AndroidDeInitialDriver({})
        ##Kill Appium
        KillAppium({"isFail":"0"})
