import unittest
from Config import dumplogger
from FrameWorkBase import *


class CLAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup CLAPIPlaceOrder Precondition ==")

    def test_CLAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/CLAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup CLAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup CLAPIPlaceOrder Post condition ==")
