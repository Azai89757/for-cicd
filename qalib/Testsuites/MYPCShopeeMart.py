import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class MYPCShopeeMart(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCShopeeMart Precondition =="
        dumplogger.info("== Setup MYPCShopeeMart Precondition ==")

    def test_MYPCShopeeMart01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/MY/MYPCShopeeMart-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup MYPCShopeeMart Post condition =="
        dumplogger.info("== Setup MYPCShopeeMart Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
