import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class TWPCTax(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCTax Precondition =="
        dumplogger.info("== Setup TWPCTax Precondition ==")

    def test_TWPCTax01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCTax-01.xml")

    def test_TWPCTax02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCTax-02.xml")

    def test_TWPCTax03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/TW/TWPCTax-03.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWPCTax Post condition =="
        dumplogger.info("== Setup TWPCTax Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
