import unittest
from Config import dumplogger
from FrameWorkBase import *


class ESAPIPlaceOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESAPIPlaceOrder Precondition =="
        dumplogger.info("== Setup ESAPIPlaceOrder Precondition ==")

    def test_ESAPIPlaceOrder01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/ESAPIPlaceOrder-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup ESAPIPlaceOrder Post condition =="
        dumplogger.info("== Setup ESAPIPlaceOrder Post condition ==")
