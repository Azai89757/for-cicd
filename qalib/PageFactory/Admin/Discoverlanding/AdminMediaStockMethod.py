#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AdminMediaStockMethod.py: The def of this file called by XML mainly.
'''

#Import system library
import time

##Import common library
import DecoratorHelper
import FrameWorkBase
import Util

##Inport Web library
from PageFactory.Web import BaseUICore
from PageFactory.Web import BaseUILogic


def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)


class AdminMediaStockCommonButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddNew(arg):
        '''
        ClickAddNew : Click add new button
                Input argu :
                    page_type : attributes / attribute_group
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click add new button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockCommonButton.ClickAddNew')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSave(arg):
        '''
        ClickSave : Click save button
                Input argu :
                    page_type : attributes / attribute_group / single_image / multiple_image
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click save button
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click save button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockCommonButton.ClickSave')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickCancel(arg):
        '''
        ClickCancel : Click cancel to new attributes
                Input argu :
                    page_type : attributes / attribute_group / single_image / multiple_image
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        page_type = arg["page_type"]

        ##Click cancel to new attributes
        xpath = Util.GetXpath({"locate": page_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click cancel", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockCommonButton.ClickCancel')


class AdminMediaStockAttributesButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickYes(arg):
        '''
        ClickYes : Click yes to create new attributes
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click yes to create new attributes
        xpath = Util.GetXpath({"locate": "yes_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click yes to create new attributes", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesButton.ClickYes')


class AdminMediaStockAttributesPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAttributesName(arg):
        '''
        InputAttributesName : Input attributes name
                Input argu :
                    attributes_name - attributes name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attributes_name = arg["attributes_name"]

        ##Input attributes name
        xpath = Util.GetXpath({"locate": "new_attributes_name_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": attributes_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesPage.InputAttributesName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectValueType(arg):
        '''
        SelectValueType : Select value type
                Input argu :
                    type - dropdown / integer / string
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        type = arg["type"]

        ##Select value type
        xpath = Util.GetXpath({"locate": type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select value type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesPage.SelectValueType')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputValuePool(arg):
        '''
        InputValuePool : Input value pool
                Input argu :
                    relevant_values - relevant values
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        relevant_values = arg["relevant_values"]

        ##Input value pool
        xpath = Util.GetXpath({"locate": "value_pool_input"})
        BaseUILogic.KeyboardAction({"actiontype": "delete", "locate": xpath, "result": "1"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": relevant_values, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesPage.InputValuePool')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteAttribute(arg):
        '''
        DeleteAttribute : Delete attribute by name in attribute list
                Input argu :
                    attribute_name - attribute name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_name = arg["attribute_name"]

        ##Click delete attribute by name in attribute list
        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("string_to_be_replaced", attribute_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete attribute by name in attribute list", "result": "1"})

        ##Confirm to delete
        xpath = Util.GetXpath({"locate": "confirm_to_delete"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Confirm to delete attribute by ID in attribute list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesPage.DeleteAttribute')


class AdminMediaStockAttributesGroupButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRecommendedDimensions(arg):
        '''
        ClickRecommendedDimensions : Click checkbox of check recommended dimensions on upload
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click checkbox of check recommended dimensions on upload
        xpath = Util.GetXpath({"locate": "checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox of check recommended dimensions on upload", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupButton.ClickRecommendedDimensions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickRecommendedImageFileSize(arg):
        '''
        ClickRecommendedImageFileSize : Click checkbox of Check recommended image file size on upload
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click checkbox of Check recommended image file size on upload
        xpath = Util.GetXpath({"locate": "checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox of Check recommended image file size on upload", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupButton.ClickRecommendedImageFileSize')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickTransparentBackgroundImage(arg):
        '''
        ClickTransparentBackgroundImage : Click checkbox of transparent background image
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click checkbox of transparent background image
        xpath = Util.GetXpath({"locate": "checkbox"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click checkbox of transparent background image", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupButton.ClickTransparentBackgroundImage')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDelete(arg):
        '''
        ClickDelete : Click delete selected attribute pool
                Input argu :
                    attribute_pool_name = attribute Group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_pool_name = arg["attribute_pool_name"]

        ##Click delete selected attribute pool
        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("string_to_be_replaced", attribute_pool_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click delete selected attribute pool", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupButton.ClickDelete')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddAttributePool(arg):
        '''
        AddAttributePool : Add attribute pool with first item in dropdown list(need input correct attribute pool name first)
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Add attribute pool with first item in dropdown list
        xpath = Util.GetXpath({"locate": "attribute_pool_first_item"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Add attribute pool with first item in dropdown list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupButton.AddAttributePool')


class AdminMediaStockAttributesGroupPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAttributeGroupName(arg):
        '''
        InputAttributeGroupName : Input attribute group name in new group page
                Input argu :
                    attribute_group_name - attribute Group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_group_name = arg["attribute_group_name"]

        ##Input attribute group name in new group page
        xpath = Util.GetXpath({"locate": "new_attribute_group_name_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": attribute_group_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupPage.InputAttributeGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRecommendedImageDimensions(arg):
        '''
        InputRecommendedImageDimensions : Input recommended image dimensions
                Input argu :
                    width - width value input
                    height - height value input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        width = arg["width"]
        height = arg["height"]

        ##Input recommended image dimensions width
        xpath = Util.GetXpath({"locate": "width_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": width, "result": "1"})

        ##Input recommended image dimensions height
        xpath = Util.GetXpath({"locate": "height_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": height, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupPage.InputRecommendedImageDimensions')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputRecommendedImageFileSize(arg):
        '''
        InputRecommendedImageFileSize : Input recommended image file size
                Input argu :
                    file_size - file size value input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        file_size = arg["file_size"]

        ##Input recommended image file size
        xpath = Util.GetXpath({"locate": "file_size_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": file_size, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupPage.InputRecommendedImageFileSize')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAttributePool(arg):
        '''
        InputAttributePool : Input attribute pool
                Input argu :
                    attribute_pool - attribute pool value input
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_pool = arg["attribute_pool"]

        ##Input attribute pool
        xpath = Util.GetXpath({"locate": "attribute_pool_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": attribute_pool, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupPage.InputAttributePool')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def DeleteAttributGroup(arg):
        '''
        DeleteAttributGroup : Delete attribute group by name in attribute groups list
                Input argu :
                    attribute_group_name - attribute group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_group_name = arg["attribute_group_name"]

        ##Click delete attribute by name in attribute list
        xpath = Util.GetXpath({"locate": "delete_button"})
        xpath = xpath.replace("string_to_be_replaced", attribute_group_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete attribute group by name in attribute groups list", "result": "1"})

        ##Confirm to delete
        xpath = Util.GetXpath({"locate": "confirm_to_delete"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Confirm to delete attribute by ID in attribute list", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockAttributesGroupPage.DeleteAttributGroup')


class AdminMediaStockImagesListButton:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickSearch(arg):
        '''
        ClickSearch : Click search button
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click search button
        xpath = Util.GetXpath({"locate": "search_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click search button", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListButton.ClickSearch')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickAddAttribute(arg):
        '''
        ClickAddAttribute : Click add attribute button in images page
                Input argu :
                    click_times - click times with add attribute button
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        click_times = arg["click_times"]

        ##Click add attribute button in images page
        xpath = Util.GetXpath({"locate": "add_attribute"})
        for times in range(int(click_times)):
            BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add attribute button in images page", "result": "1"})
            time.sleep(2)

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListButton.ClickAddAttribute')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDeleteAttribute(arg):
        '''
        ClickDeleteAttribute : Delete attribute
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click delete attribute by name in attribute list
        xpath = Util.GetXpath({"locate": "delete_button"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Delete attribute", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListButton.ClickDeleteAttribute')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickEdit(arg):
        '''
        ClickEdit : Click edit button by reference name
            Input argu :
                    reference_name - reference name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reference_name = arg["reference_name"]

        ##Click edit button by reference name
        xpath = Util.GetXpath({"locate": "edit_button"})
        xpath = xpath.replace("string_to_be_replaced", reference_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click edit button by reference name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListButton.ClickEdit')


class AdminMediaStockImagesListPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageAttributeGroupName(arg):
        '''
        InputImageAttributeGroupName : Input image attribute group name
                Input argu :
                    image_attribute_group_name - image attribute group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_attribute_group_name = arg["image_attribute_group_name"]

        ##Input image attribute group name
        xpath = Util.GetXpath({"locate": "image_attribute_group_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_attribute_group_name, "result": "1"})
        time.sleep(2)
        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "Input image attribute group name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListPage.InputImageAttributeGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageAttributeName(arg):
        '''
        InputImageAttributeName : Input image attribute name
                Input argu :
                    image_attribute_name - image attribute name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_attribute_name = arg["image_attribute_name"]

        ##Input image attribute name
        xpath = Util.GetXpath({"locate": "image_attribute_group_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_attribute_name, "result": "1"})
        time.sleep(2)
        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "Input image attribute name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListPage.InputImageAttributeName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputImageAttributeValue(arg):
        '''
        InputImageAttributeValue : Input image attribute value
                Input argu :
                    image_attribute_value - image attribute value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_attribute_value = arg["image_attribute_value"]

        ##Input image attribute value
        xpath = Util.GetXpath({"locate": "image_attribute_value_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_attribute_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListPage.InputImageAttributeValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def AddNewImage(arg):
        '''
        AddNewImage : add new image
            Input argu :
                    new_image_type - Single Image / Multiple Images
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        new_image_type = arg["new_image_type"]

        ##Click add new image
        xpath = Util.GetXpath({"locate": "add_new_image"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click add new image", "result": "1"})

        xpath = Util.GetXpath({"locate": new_image_type})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminMediaStockImagesListPage.AddNewImage')


class AdminNewSingleImagePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReferenceName(arg):
        '''
        InputReferenceName : Input reference name
                Input argu :
                    reference_name - reference name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reference_name = arg["reference_name"]

        ##Input image attribute value
        xpath = Util.GetXpath({"locate": "reference_name_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": reference_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSingleImagePage.InputReferenceName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAttributeGroupName(arg):
        '''
        InputAttributeGroupName : Input image attribute group name
                Input argu :
                    image_attribute_group_name - image attribute group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_attribute_group_name = arg["image_attribute_group_name"]

        ##Input image attribute group name
        xpath = Util.GetXpath({"locate": "image_attribute_group_input"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": image_attribute_group_name, "result": "1"})
        time.sleep(2)
        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "Input image attribute group name", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSingleImagePage.InputAttributeGroupName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadImagePreview(arg):
        '''
        UploadImagePreview : Upload image for image preview
                Input argu :
                    image_name - image name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_name = arg["image_name"]

        ##Upload image for image preview
        BaseUILogic.UploadFileWithPath({"method": "input", "project": "MediaStock", "action": "image", "file_name": image_name, "file_type": "png", "element": "file", "element_type": "type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSingleImagePage.UploadImagePreview')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputAttributePoolValue(arg):
        '''
        InputAttributePoolValue : Input image attribute pool value to matched attribute pool name
                Input argu :
                    attribute_pool_name - image attribute pool name
                    attribute_pool_value - image attribute pool value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_pool_name = arg["attribute_pool_name"]
        attribute_pool_value = arg["attribute_pool_value"]

        ##Input image attribute group name
        xpath = Util.GetXpath({"locate": "pool_value"})
        xpath = xpath.replace("string_to_be_replaced", attribute_pool_name)
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": attribute_pool_value, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSingleImagePage.InputAttributePoolValue')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAttributePoolDropdownValue(arg):
        '''
        SelectAttributePoolDropdownValue : Select dropdown attribute pool value to matched attribute pool name
                Input argu :
                    attribute_pool_name - image attribute pool name
                    attribute_pool_value - image attribute pool value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_pool_name = arg["attribute_pool_name"]
        attribute_pool_value = arg["attribute_pool_value"]

        ##Click attribute dropdown list
        xpath = Util.GetXpath({"locate": "pool_dropdown_list"})
        xpath = xpath.replace("string_to_be_replaced", attribute_pool_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click attribute dropdown list", "result": "1"})

        ##Select key
        xpath = Util.GetXpath({"locate": "value_to_select"})
        xpath = xpath.replace("string_to_be_replaced", attribute_pool_value)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select key", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewSingleImagePage.SelectAttributePoolDropdownValue')


class AdminNewMultipleImagePage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadZipFolderOfImages(arg):
        '''
        UploadZipFolderOfImages : Upload zip folder of images
                Input argu :
                    image_zip_folder_name - image zip folder name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        image_zip_folder_name = arg["image_zip_folder_name"]

        ##Upload zip folder of images
        BaseUILogic.UploadFileWithPath({"method": "input", "project": "MediaStock", "action": "file", "file_name": image_zip_folder_name, "file_type": "zip", "element": "file", "element_type": "type", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewMultipleImagePage.UploadZipFolderOfImages')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputReferenceName(arg):
        '''
        InputReferenceName : Input reference name
                Input argu :
                    reference_name - reference name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reference_name = arg["reference_name"]

        ##Input image attribute group name
        xpath = Util.GetXpath({"locate": "reference_name_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": reference_name, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewMultipleImagePage.InputReferenceName')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAttributeGroup(arg):
        '''
        SelectAttributeGroup : Select attribute group
                Input argu :
                    attribute_group_name - attribute group name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_group_name = arg["attribute_group_name"]

        ##Click attribute group list
        xpath = Util.GetXpath({"locate": "attribute_group_list"})
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click attribute group dropdowon list", "result": "1"})

        ##Select key
        xpath = Util.GetXpath({"locate": "value_to_select"})
        xpath = xpath.replace("string_to_be_replaced", attribute_group_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Select key", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewMultipleImagePage.SelectAttributeGroup')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputFieldNumberOfAttributesInFile(arg):
        '''
        InputFieldNumberOfAttributesInFile : Input field No. of attributes in filename
                Input argu :
                    field_number - field No.
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        field_number = arg["field_number"]

        ##Input image attribute group name
        xpath = Util.GetXpath({"locate": "attribute_no_field"})
        BaseUICore.Input({"method": "xpath", "locate": xpath, "string": field_number, "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewMultipleImagePage.InputFieldNumberOfAttributesInFile')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def SelectAttributePoolValue(arg):
        '''
        SelectAttributePoolValue : Select attribute pool value
                Input argu :
                    attribute_name - attribute name
                    attribute_value - attribute value
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        attribute_name = arg["attribute_name"]
        attribute_value = arg["attribute_value"]

        ##Click attribute group list
        xpath = Util.GetXpath({"locate": "attribute_vlaue_list"})
        xpath = xpath.replace("string_to_be_replaced", attribute_name)
        BaseUICore.Click({"method": "xpath", "locate": xpath, "message": "Click attribute value dropdowon list", "result": "1"})
        time.sleep(5)

        ##Select key
        for index in range(1, int(attribute_value)):
            BaseUICore.SendSeleniumKeyEvent({"action_type": "down", "string": "", "result": "1"})

        BaseUICore.SendSeleniumKeyEvent({"action_type": "enter", "string": "", "result": "1"})

        OK(ret, int(arg['result']), 'AdminNewMultipleImagePage.SelectAttributePoolValue')
