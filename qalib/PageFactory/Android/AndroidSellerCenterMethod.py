#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 AndroidSellerCenterMethod.py: The def of this file called by XML mainly.
'''

##import python common library
import time

##Import common library
import Util
import FrameWorkBase
import DecoratorHelper

##Import Android library
import AndroidBaseUICore
import AndroidBaseUILogic

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class AndroidSellerCenterPage:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOrderListTab(arg):
        '''
        ClickOrderListTab : Click order list tab in seller center page
                Input argu :
                    order_status - shipping / completed / cancellation / return_refund / unprocessed / processed
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        order_status = arg["order_status"]

        ##Click order list tab in seller center page
        xpath = Util.GetXpath({"locate":order_status})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click order list tab in seller center page => %s" % (order_status), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterPage.ClickOrderListTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickDisputeReasonOption(arg):
        '''
        ClickDisputeReasonOption : Click dispute reason option
                Input argu :
                    reason_option - shipped_the_item_and_have_proof
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        reason_option = arg["reason_option"]

        ##Click dispute reason option
        xpath = Util.GetXpath({"locate":reason_option})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click dispute reason option => %s" % (reason_option), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterPage.ClickDisputeReasonOption')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def UploadPhoto(arg):
        '''
        UploadPhoto : Upload photo
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Click to upload image
        xpath = Util.GetXpath({"locate":"add_photo"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click to upload image", "result": "1"})
        time.sleep(2)

        ##Click to upload from album
        xpath = Util.GetXpath({"locate":"from_album"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click upload from album", "result": "1"})
        time.sleep(2)

        ##Click first image
        xpath = Util.GetXpath({"locate":"image"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click first image", "result": "1"})
        time.sleep(2)

        ##Click next
        xpath = Util.GetXpath({"locate":"next"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click next", "result": "1"})
        time.sleep(2)

        OK(ret, int(arg['result']), 'AndroidSellerCenterPage.UploadPhoto')


class AndroidSellerCenterComponent:

    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnButton(arg):
        '''
        ClickOnButton : Click any type of button with well defined locator
                Input argu :
                    button_type - type of button which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        button_type = arg["button_type"]

        ##Click on button
        xpath = Util.GetXpath({"locate": button_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click button: %s, which xpath is %s" % (button_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.ClickOnButton')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnIcon(arg):
        '''
        ClickOnIcon : Click any type of icon with well defined locator
                Input argu :
                    icon_type - type of icon which defined by caller
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        icon_type = arg["icon_type"]

        ##Click on icon
        xpath = Util.GetXpath({"locate": icon_type})
        AndroidBaseUICore.AndroidClick({"method": "xpath", "locate": xpath, "message": "Click icon: %s, which xpath is %s" % (icon_type, xpath), "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.ClickOnIcon')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputToColumn(arg):
        '''
        InputToColumn : Input any type of culumn based on arguments
                Input argu :
                    column_type - type of column
                    input_content - input content
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        column_type = arg["column_type"]
        input_content = arg["input_content"]

        ##Input something to column
        xpath = Util.GetXpath({"locate": column_type})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": xpath, "string": input_content, "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.InputToColumn')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnTab(arg):
        '''
        ClickOnTab : Click tab on seller center page
                Input argu :
                    tab_type - my_shop_to_ship / my_shop_cancelled / my_shop_return / my_shop_review /
                            my_sale_unpaid / my_sale_to_ship / my_sale_shipping / my_sale_completed / my_sale_cancellation / my_sale_return_refund
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click tab on seller center page
        xpath = Util.GetXpath({"locate":tab_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click tab on seller center page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.ClickOnTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSubTab(arg):
        '''
        ClickOnSubTab : Click sub tab on my sales page
                Input argu :
                    tab_type - all / all_requests / to_process / processed / unprocessed / to_respond / cancelled /
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        tab_type = arg["tab_type"]

        ##Click sub tab on my sales page
        xpath = Util.GetXpath({"locate":tab_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click sub tab on my sales page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.ClickOnSubTab')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnSection(arg):
        '''
        ClickOnSection : Click section on seller center page
                Input argu :
                    section_type - odp_income_details / odp_buyer_payment / odp_buyer_info / odp_select_cancel_reason / odp_cancel_reason_out_of_stock
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        section_type = arg["section_type"]

        ##Click section on seller center page
        xpath = Util.GetXpath({"locate":section_type})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":xpath, "message":"Click section on seller center page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.ClickOnSection')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def ClickOnCheckBox(arg):
        '''
        ClickOnCheckBox : Click cancel check box on seller center page
                Input argu :
                    checkbox_type - product_name
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        product_name = arg["product_name"]

        ##Click check box on seller center page
        xpath = Util.GetXpath({"locate":"product_name_checkbox"})
        product_name_checkbox = xpath.replace("product_name_to_be_replace", product_name)
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":product_name_checkbox, "message":"Click check box on seller center page", "result": "1"})

        OK(ret, int(arg['result']), 'AndroidSellerCenterComponent.ClickOnCheckBox')

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def InputInTextField(arg):
        '''
        InputInTextField : Input street name
                Input argu :
                    message - type text
                    text_field_type - odp_add_note
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1
        text_field_type = arg["text_field_type"]
        message = arg["message"]

        ##Input text field
        locate = Util.GetXpath({"locate": text_field_type})
        AndroidBaseUICore.AndroidInput({"method": "xpath", "locate": locate, "string":message, "result": "1"})
        AndroidBaseUILogic.AndroidRelativeMove({"direction":"down", "times":"1", "result": "1"})

        locate = Util.GetXpath({"locate": "odp_submit_btn"})
        AndroidBaseUICore.AndroidClick({"method":"xpath", "locate":locate, "message":"Click Submit button", "result": "1"})

        OK(ret, int(arg["result"]), 'AndroidSellerCenterComponent.InputInTextField')
