import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class IDPCUnifiedLink(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCUnifiedLink Precondition =="
        dumplogger.info("== Setup IDPCUnifiedLink Precondition ==")

    def test_IDPCUnifiedLink01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/ID/IDPCUnifiedLink-01.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup IDPCUnifiedLink Post condition =="
        dumplogger.info("== Setup IDPCUnifiedLink Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
