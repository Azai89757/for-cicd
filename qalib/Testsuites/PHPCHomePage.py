import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *

class PHPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCHomePage Precondition =="
        dumplogger.info("== Setup PHPCHomePage Precondition ==")

    def test_PHPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-01.xml")

    def test_PHPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-02.xml")

    '''def test_PHPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-03.xml")'''

    '''def test_PHPCHomePage04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-04.xml")

    def test_PHPCHomePage05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-05.xml")

    def test_PHPCHomePage06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-06.xml")'''

    '''def test_PHPCHomePage07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/PH/PHPCHomePage-07.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup PHPCHomePage Post condition =="
        dumplogger.info("== Setup PHPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
