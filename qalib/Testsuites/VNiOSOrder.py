import unittest
import Config
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.iOS.iOSBaseUICore import *
from PageFactory.Web.BaseUICore import *


class VNiOSOrder(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSOrder Precondition =="
        dumplogger.info("== Setup VNiOSOrder Precondition ==")

        ##Get device id and device t type
        Config._DeviceID_, device_type = GetiOSDeviceId()
        print "Device id : %s" % (Config._DeviceID_)
        print "Device type :", device_type

        ##Get device info by device id
        if Config._DeviceID_:
            #Config._MobileDeviceInfo_[Config._DeviceID_] = {}
            #Config._MobileDeviceInfo_[Config._DeviceID_]["device_name"] = device_type
            ##Build WDA to iOS device
            iOSBuildWDAToDevice({"device_type":device_type, "uuid":Config._DeviceID_})

            ##Call function from iOSBaseUICore
            iOSInitialDriver()
        else:
            dumplogger.error("Please check your iOS device id !!!!")
            print "Please check your iOS device id !!!!"
            ##Directly Leave Setup
            self.skipTest("")

    def test_VNiOSOrder01(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-01.xml")

    def test_VNiOSOrder02(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-02.xml")

    def test_VNiOSOrder03(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-03.xml")

    def test_VNiOSOrder04(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-04.xml")

    def test_VNiOSOrder05(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-05.xml")

    def test_VNiOSOrder06(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-06.xml")

    def test_VNiOSOrder07(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-07.xml")

    def test_VNiOSOrder08(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-08.xml")

    def test_VNiOSOrder09(self):

        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/VN/VNiOSOrder-09.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup VNiOSOrder Post condition =="
        dumplogger.info("== Setup VNiOSOrder Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
        ##Kill iOS driver
        iOSDeInitialDriver({})
        ##Kill Appium
        iOSKillXcodeBuild({"isFail":"0"})
