import unittest
from Config import dumplogger
from FrameWorkBase import *
from PageFactory.Web.BaseUICore import *


class INPCHomePage(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCHomePage Precondition =="
        dumplogger.info("== Setup INPCHomePage Precondition ==")

    def test_INPCHomePage01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCHomePage-01.xml")

    def test_INPCHomePage02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCHomePage-02.xml")

    '''def test_INPCHomePage03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/ui/IN/INPCHomePage-03.xml")'''

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup INPCHomePage Post condition =="
        dumplogger.info("== Setup INPCHomePage Post condition ==")

        ##Kill Web driver
        DeInitialWebDriver("kill_browser")
