#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
 RCMD AdminAPIMethod.py: The def of this file called by XML mainly.
 RCMD Admin: Daily Discover Admin
'''

##Import common library
import DecoratorHelper
import FrameWorkBase
import GlobalAdapter
import Config

##Import DB library
from db import DBCommonMethod

def OK(src, dst, desc=""):
    return FrameWorkBase.OK(src, dst, desc)

class RCMDAdminAPI:
    def __IsForTest__(self):
        pass

    @staticmethod
    @DecoratorHelper.FuncRecorder
    def StoreRCMDAdminCookie(arg):
        '''
        StoreRCMDAdminCookie : Assign RCMD Admin cookie to API header
                Input argu : N/A
                Return code :
                    1 - success
                    0 - fail
                    -1 - error
        '''
        ret = 1

        ##Get current env
        GlobalAdapter.CommonVar._DynamicCaseData_["env"] = Config._EnvType_

        ##Get current country
        GlobalAdapter.CommonVar._DynamicCaseData_["country"] = Config._TestCaseRegion_

        ##Get cookie name and value from DB and store to common var
        store_list = ["cookie_name", "cookie_value", "cookie_domain"]
        assign_list = [{"column": "env", "value_type": "string"},{"column": "country", "value_type": "string"}]

        ##Send SQL to get RCMD admin cookie
        DBCommonMethod.SendSQLCommandProcess({"db_name":"qa", "file_name": "admin_cookie_rcmd", "method":"select", "verify_result":"", "assign_data_list": assign_list, "store_data_list":store_list, "result": "1"})

        ##Build cookie to API header
        rcmd_cookie = GlobalAdapter.CommonVar._DynamicCaseData_["cookie_name"] + "=" + GlobalAdapter.CommonVar._DynamicCaseData_["cookie_value"]

        ##Assign cookie to API request header
        GlobalAdapter.APIVar._HttpHeaders_["cookie"] = rcmd_cookie

        OK(ret, int(arg['result']), 'RCMDAdminAPI.StoreRCMDAdminCookie')
