import unittest
from Config import dumplogger
from FrameWorkBase import *


class TWHttpSellerDiscountNew(unittest.TestCase):

    def setUp(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerDiscountNew Precondition =="
        dumplogger.info("== Setup TWHttpSellerDiscountNew Precondition ==")

    def test_TWHttpSellerDiscountNew01(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-01.xml")

    def test_TWHttpSellerDiscountNew02(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-02.xml")

    def test_TWHttpSellerDiscountNew03(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-03.xml")

    def test_TWHttpSellerDiscountNew04(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-04.xml")

    def test_TWHttpSellerDiscountNew05(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-05.xml")

    def test_TWHttpSellerDiscountNew06(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-06.xml")

    def test_TWHttpSellerDiscountNew07(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-07.xml")

    def test_TWHttpSellerDiscountNew08(self):
        if isMyCase(self.id()) is False:
            return
        XMLParser("/xml/api/http/TWHttpSellerDiscountNew-08.xml")

    def tearDown(self):
        if isMyCase(self.id()) is False:
            return
        print "== Setup TWHttpSellerDiscountNew Post condition =="
        dumplogger.info("== Setup TWHttpSellerDiscountNew Post condition ==")
